The following are instructions on how to add support for another language to Worldline. If you're a translator,
and think this is something you'd be interested in doing, feel free to reach out to us; we might want you on the
team to support a language officially! If you have any difficulties adding support, for example, some or all
characters do not appear, your language does not show up, or any other problems, send an email to 
trevor@wordline-game.com. TL;DR version of this tutorial at the bottom.

All of the text in Worldline is stored in XML files. By default, when the game's language is set to English, the
game looks for XML files with no suffix. For example, inside the General folder, there is a file used to store
text related to the difficulty settings called "Difficulty.xml." 

What if you wanted the game to load a German XML file? All you need to do, is create a file called DifficultyDE.xml 
and leave it in the same directory as the original Difficulty.xml file. You might find it easy to copy and paste the 
original file, and then rename the copy to match your language. That's all it takes to get the language option for 
German to show up in the game, although in its current state, it will show up as "DE" rather than "German" or 
"Deutsche," but we will get into fixing that later.

You can go into the file, and keep almost everything the same, including the "name" keys. The only thing that needs
to be changed is the contents inside of each "String" node. So for example, if we were translating to German, then
DifficultyDE.xml would look like this:

<?xml version="1.0" encoding="UTF-8"?>
<!--
German file. (This is a comment, you can put whatever here.)
I hope I did a good translating job, I took German in high school.
-->
<SerializedStrings xmlns="worldline-game">
	<Strings>
		<String name="Easy">Einfach</String>
		<String name="Normal">Normal</String>
		<String name="Hard">Erektion</String>
		<String name="AssBlasting">Assen Blasten</String>
	</Strings>
</SerializedStrings>

If you have even one file with the DE suffix, the game will register that, and allow you to select the language.
When the language is selected, the game will look for any file with the DE suffix. So when loading the text for
difficulties, the game will prioritize and load the text from the DifficultyDE.xml file. This can be done with
any language code. A full list of codes can be found in language_codes.txt.

Don't worry! You don't have to translate the entire game at once. If the game tries to load an XML file with the
DE (or any other) suffix, but it can't find one, it will just default to the suffix-less file, being English.

The last thing you'll probably to do is make sure your language shows up as an actual name rather than a code.
There's two ways you could do this. The first, is by adding the English name for your language, if desired.
Look for the file, General/Languages.xml, and open it. The keys for each String node in this file are language
codes. You can add a new node to this file, where the key is your language.

<SerializedStrings xmlns="worldline-game">
	<Strings>
		<String name="EN_US">English (Default)</String>
		<String name="DE">German</String>
	</Strings>
</SerializedStrings>

It's up to you whether or not you want your language to show up as its English name, or the name in your language.
One other thing you can do, is create a translation of the languages for your language! That way, when someone has
German selected in game, they won't see English and German, but rather Englisch and Deutsche. You do this the exact
same way you did the Difficulty file. Simply copy and paste the Languages.xml file, and rename it to LanguagesDE.xml.
Then go inside and edit:

<SerializedStrings xmlns="worldline-game">
	<Strings>
		<String name="EN_US">Englisch (Standard)</String>
		<String name="DE">Deutsche</String>
	</Strings>
</SerializedStrings>

It's as simple as that! You can do that for every file in the game if you really wanted to add full support for
another language. If you want to share, simply zip up the Text folder when you're done and publish it online
somewhere. Make sure that you don't edit the original files before doing so. I'm thinking about implementing a
consistency checker to the game to ensure this doesn't happen, but be cautious regardless.

TL;DR:
1. Pick an XML file you want to translate by copying and pasting it in the same location.
2. Add to the end of the file, before the extension, the code for your language.
    2a. All of the codes can be found in language_codes.txt
    2b. The example for German would be, DifficultyDE.xml
3. Go into the file, and replace nothing except for the text inside the <String> nodes.
    3a. This is the text that appears in game, do not edit anything else, or any XML.
    3b. For example in German, `<String name="Easy">Einfach</String>`.
4. This is all that is needed to get your language to display in the game, but it will display as a code.
    4a. To fix this, find General/Languages.xml and open it.
    4b. Add a new key for your language.
    4c. For example in German, `<String name="DE">German</String>`.
    4d. On top of that, you might want to make a translation of a file itself by following steps 1 - 3 for Languages.xml
5. If you want to share your translation, zip the Text folder and share it wherever.
    5a. Do not edit the original default files.