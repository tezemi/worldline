﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced 'SeperateSpecular' with 'SeparateSpecular'

Shader "FX/VertexLit ShowThrough"
{
    Properties
    {
        _Color ("Main Color", Color) = (1, 1, 1, 1)
        _MainTex ("Base (RGB)", 2D) = "white" { }
        _OccludeColor ("Occlusion Color", Color) = (0, 0, 1, 1)
        _OutlineColor ("Ountline Color", Color) = (0, 0, 0, 0)
        _Outline ("_Outline", Range(0, 0.5)) = 0
    }
    SubShader
    {
        Tags { "Queue" = "Geometry+5" }
        // occluded pass
        Pass
        {
            ZWrite Off
            Blend One Zero
            ZTest Greater
            Color[_OccludeColor]
        }
        // Vertex lights
        Pass
        {
            Tags { "LightMode" = "Vertex" }
            ZWrite On
            Lighting On
            SeparateSpecular On
            Material
            {
                Diffuse[_Color]
                Ambient[_Color]
                // Emission [_PPLAmbient]
            }
            
            
            SetTexture[_MainTex]
            {
                ConstantColor[_Color]
                Combine texture * primary DOUBLE, texture * constant
            }
        }
        Pass
        {
            Cull Front
            CGPROGRAM
            
            #pragma vertex vert
            #pragma fragment frag
            #include "UnityCG.cginc"
            
            struct v2f
            {
                float4 pos: SV_POSITION;
            };
            
            float _Outline;
            float4 _OutlineColor;
            
            float4 vert(appdata_base v): SV_POSITION
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                float3 normal = mul((float3x3) UNITY_MATRIX_MV, v.normal);
                normal.x *= UNITY_MATRIX_P[0][0];
                normal.y *= UNITY_MATRIX_P[1][1];
                o.pos.xy += normal.xy * _Outline;
                return o.pos;
            }
            
            half4 frag(v2f i): COLOR
            {
                return _OutlineColor;
            }
            
            ENDCG
            
        }
    }
    FallBack "Diffuse", 1
}

