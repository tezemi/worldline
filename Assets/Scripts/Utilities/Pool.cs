﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Utilities
{
    public static class Pool
    {
        public static Dictionary<string, List<GameObject>> ObjectPool { get; } = new Dictionary<string, List<GameObject>>();

        public static bool Has(string poolName)
        {
            if (ObjectPool.ContainsKey(poolName))
            {
                ObjectPool[poolName].RemoveAll(n => n == null);

                return ObjectPool[poolName].Count > 0;
            }

            return false;
        }

        public static GameObject Get(string poolName)
        {
            if (ObjectPool.ContainsKey(poolName))
            {
                if (ObjectPool[poolName].Count > 0)
                {
                    ObjectPool[poolName].RemoveAll(n => n == null);
                    GameObject obj = ObjectPool[poolName][0];
                    obj.hideFlags = HideFlags.None;
                    ObjectPool[poolName].Remove(obj);
                    foreach (IPoolable p in obj.GetComponents<IPoolable>())
                    {
                        p.Reuse();
                    }

                    return obj;
                }
            }

            return null;
        }

        /// <summary>
        /// In general, things that implement IResurrectable (are able to be resurrected from death) should
        /// wait for the buffer.
        /// </summary>
        public static void Add(GameObject obj, string poolName, bool buffer)
        {
            Timing.RunCoroutine(WaitForBuffer());            

            IEnumerator<float> WaitForBuffer()
            {
                yield return Timing.WaitForSeconds(buffer ? DeathTimeOperator.DeathTimeDuration : 0f);

                if (obj == null)
                {
                    yield break;
                }

                if (ObjectPool.ContainsKey(poolName))
                {
                    ObjectPool[poolName].Add(obj);
                }
                else
                {
                    ObjectPool.Add(poolName, new List<GameObject> { obj });
                }

                obj.hideFlags = HideFlags.HideInHierarchy;
            }
        }
    }
}
