// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;

namespace Worldline.Utilities
{
    public class ObservableList<T> : List<T>
    {
        public List<Action<T>> OnAdd { get; } = new List<Action<T>>();
        public List<Action<T>> OnRemove { get; } = new List<Action<T>>();

        public ObservableList()
        {
            
        }

        public ObservableList(IEnumerable<T> collection) : base(collection)
        {
            
        }

        public new void Add(T item)
        {
            base.Add(item);
            foreach (Action<T> a in OnAdd)
            {
                a.Invoke(item);
            }
        }   

        public new void Remove(T item)
        {
            base.Remove(item);
            foreach (Action<T> a in OnRemove)
            {
                a.Invoke(item);
            }
        }
    }
}
