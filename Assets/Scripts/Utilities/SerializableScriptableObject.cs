﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Utilities
{
    /// <summary>
    /// This is used to create ScriptableObjects which store a reference 
    /// to their asset path, allowing the path to be serialized as binary
    /// and then loaded into the game again.
    /// </summary>
    public abstract class SerializableScriptableObject : ScriptableObject
    {
        [HideInInspector]
        public string AssetPath;
    }
}
