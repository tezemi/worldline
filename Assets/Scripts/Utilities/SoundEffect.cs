﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.GameState;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Utilities
{
    [HasDefaultState]
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(SoundEffectTimeOperator))]
    public class SoundEffect : MonoBehaviour, ICanRessurect, IPoolable
    {
        public static List<SoundEffect> AllSoundEffects { get; protected set; } = new List<SoundEffect>();
        public bool EffectedByTimeEngine = true;
        public bool SuppressBGM;
        public bool DestroyOnEnd = true;
        public float SuppressionAmount = 0.5f;
        public bool Dead { get; set; }
        public bool Resurrected { get; set; }
        /// <summary>
        /// When this isn't null, the SoundEffect's TimeOperator will take on 
        /// the effect of the supplied one.
        /// </summary>
        public TimeOperator Parent { get; set; }
        public AudioSource AudioSource { get; protected set; }
        public bool IsPlaying => AudioSource.isPlaying;
        public const bool UsePooling = false;
        public const string SoundEffectPool = "SoundEffectPool";
        protected SoundEffectTimeOperator SoundEffectTimeOperator { get; set; }
        private string _soundName; // For debugging purposes.

        protected virtual void Awake()
        {
            AudioSource = GetComponent<AudioSource>();
            SoundEffectTimeOperator = GetComponent<SoundEffectTimeOperator>();
            GetComponent<Recorder>().SnapshotType = typeof(SoundEffectSnapshot);
            AllSoundEffects.Add(this);
        }

        protected virtual void OnEnable()
        {
            AudioSource.outputAudioMixerGroup = GlobalStorage.Get().SoundEffectsAudioMixerGroup;
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void OnDestroy()
        {
            AllSoundEffects.Remove(this);
        }

        protected virtual void Update()
        {
            if (Parent == null) return;

            if (SoundEffectTimeOperator.CurrentTimeEffect != Parent.CurrentTimeEffect &&
            Parent.CurrentTimeEffect != TimeEffect.Record && Parent.CurrentTimeEffect != TimeEffect.Reset)
            {
                GetComponent<SoundEffectTimeOperator>().CurrentTimeEffect = Parent.CurrentTimeEffect;
            }
        }

        protected IEnumerator<float> Cleanup()
        {
            yield return Timing.WaitUntilDone(new WaitUntil(() => !AudioSource.isPlaying));
            Die();
        }

        protected IEnumerator<float> Suppress()
        {
            BackgroundMusicOperator.Main.FadeTo(0.1f);
            //BackgroundMusicOperator.Main.Volume -= SuppressionAmount;
            yield return Timing.WaitUntilDone(new WaitUntil(() => !AudioSource.isPlaying));
            //BackgroundMusicOperator.Main.Volume += SuppressionAmount;
            BackgroundMusicOperator.Main.FadeTo(1f);
        }

        public void Play()
        {
            if (AudioSource.clip == null)
            {
                Debug.LogWarning("Tried to play null sound. Sound not played", gameObject);
                return;
            }

            _soundName = AudioSource.clip.name;
            AudioSource.Play();
            if (SuppressBGM)
            {
                Timing.RunCoroutine(Suppress(), Segment.FixedUpdate, GetInstanceID().ToString());
            }

            if (DestroyOnEnd)
            {
                Timing.RunCoroutine(Cleanup(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
        }

        public void Stop()
        {
            try
            {
                AudioSource.Stop();
            }
            catch (MissingReferenceException e)
            {
                Debug.Log($"Null AudioSource when trying to stop {_soundName}.{Environment.NewLine}{e}");
            }
            catch (NullReferenceException e)
            {
                Debug.Log($"Null AudioSource when trying to stop {_soundName}.{Environment.NewLine}{e}");
            }
        }

        public void Die()
        {
            if (Resurrected) return;
            
            gameObject.SetActive(false);
            if (EffectedByTimeEngine)
            {
                Dead = true;
                if (Parent != null)
                {
                    bool shouldAlign = Parent.GetComponent<IHasAlignment>() != null;
                    if (shouldAlign)
                    {
                        DeathTimeOperator.Handle(gameObject, Parent.GetComponent<IHasAlignment>().Alignment, false);
                    }

                    //Pool.Add(gameObject, SoundEffectPool, shouldAlign);
                    Destroy(gameObject);
                }
            }
            else
            {
                //Pool.Add(gameObject, SoundEffectPool, false);
                Destroy(gameObject);
            }
        }

        public void Resurrect()
        {
            Dead = false;
            Resurrected = true;
            gameObject.SetActive(true);
            AudioSource.Play();
            AudioSource.time = AudioSource.clip.length - 0.1f;
            Timing.RunCoroutine(Clean(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> Clean()
            {
                yield return Timing.WaitForSeconds(TimeOperator.StandardResurrectionInvincibilityTime);
                Resurrected = false;
            }
        }

        public void Reuse()
        {
            EffectedByTimeEngine = true;
            SuppressBGM = false;
            DestroyOnEnd = true;
            SuppressionAmount = 0.5f;
            Dead = false;
            Resurrected = false;
            Parent = null;
            AudioSource.clip = null;
            AudioSource.loop = false;
            AudioSource.mute = false;
            AudioSource.pitch = 1f;
            AudioSource.volume = 1f;
        }

        public static void StopAllSounds()
        {
            foreach (SoundEffect sound in FindObjectsOfType<SoundEffect>())
            {
                sound.Stop();
            }
        }

        public static SoundEffect CreateSound(AudioClip clip)
        {
            GameObject gameObject;
            if (Pool.Has(SoundEffectPool))
            {
                //gameObject = Pool.Get(SoundEffectPool);
                gameObject = new GameObject("Sound Effect", typeof(SoundEffect));
            }
            else
            {
                gameObject = new GameObject("Sound Effect", typeof(SoundEffect));
            }

            gameObject.SetActive(true);
            gameObject.GetComponent<SoundEffect>().AudioSource.clip = clip;
            if (clip != null)
            {
                gameObject.name = $"'{gameObject.GetComponent<SoundEffect>().AudioSource.clip.name}' Sound Effect";
            }

            return gameObject.GetComponent<SoundEffect>();
        }

        public static SoundEffect PlaySound(AudioClip clip)
        {
            SoundEffect effect = CreateSound(clip);
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, TimeOperator parent)
        {
            SoundEffect effect = CreateSound(clip);
            effect.Parent = parent;
            effect.EffectedByTimeEngine = true;
            effect.Play();

            return effect;
        }
        
        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine)
        {
            SoundEffect effect = CreateSound(clip);
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, float pitch, TimeOperator parent)
        {
            SoundEffect effect = CreateSound(clip);
            effect.AudioSource.pitch = pitch;
            effect.Parent = parent;
            effect.EffectedByTimeEngine = true;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, TimeOperator parent, float volume)
        {
            SoundEffect effect = CreateSound(clip);
            effect.Parent = parent;
            effect.EffectedByTimeEngine = true;
            effect.AudioSource.volume = volume;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, float pitch)
        {
            SoundEffect effect = CreateSound(clip);
            effect.AudioSource.pitch = pitch;
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, TimeOperator parent, float volume, float pitch)
        {
            SoundEffect effect = CreateSound(clip);
            effect.Parent = parent;
            effect.EffectedByTimeEngine = true;
            effect.AudioSource.volume = volume;
            effect.AudioSource.pitch = pitch;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, float pitch, float volume)
        {
            SoundEffect effect = CreateSound(clip);
            effect.AudioSource.pitch = pitch;
            effect.AudioSource.volume = volume;
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.Play();

            return effect;
        }
        
        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, bool suppressBGM, float suppressionAmount = 0.5f)
        {
            SoundEffect effect = CreateSound(clip);
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.SuppressionAmount = suppressionAmount;
            effect.SuppressBGM = suppressBGM;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, bool suppressBGM, float suppressionAmount, bool loop)
        {
            SoundEffect effect = CreateSound(clip);
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.SuppressionAmount = suppressionAmount;
            effect.SuppressBGM = suppressBGM;
            effect.AudioSource.loop = loop;
            effect.Play();

            return effect;
        }

        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, bool suppressBGM, float suppressionAmount, float volume)
        {
            SoundEffect effect = CreateSound(clip);
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.SuppressionAmount = suppressionAmount;
            effect.SuppressBGM = suppressBGM;
            effect.AudioSource.volume = volume;
            effect.Play();

            return effect;
        }
        
        public static SoundEffect PlaySound(AudioClip clip, bool effectedByTimeEngine, bool suppressBGM, float suppressionAmount, bool loop, bool dontDestroyOnLevelChange)
        {
            SoundEffect effect = CreateSound(clip);
            effect.EffectedByTimeEngine = effectedByTimeEngine;
            effect.SuppressionAmount = suppressionAmount;
            effect.SuppressBGM = suppressBGM;
            effect.AudioSource.loop = loop;
            effect.Play();
            if (dontDestroyOnLevelChange)
            {
                DontDestroyOnLoad(effect);
                effect.DestroyOnSceneLoad();
            }

            return effect;
        }

        [SetsUpDefaults]
        private static void SetupDefaults()
        {
            AllSoundEffects = new List<SoundEffect>();
        }
    }
}