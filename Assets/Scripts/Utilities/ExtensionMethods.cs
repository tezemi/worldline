﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.TimeEngine;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using Worldline.Physics;

namespace Worldline.Utilities
{
    /// <summary>
    /// Static class that contains all of the ExtensionMethod used in 
    /// Worldline.
    /// </summary>
    public static class ExtensionMethods
    {
        /// <summary>
        /// Objects inside this List will be destroyed when the scene is changed 
        /// from a non-level scene to a level scene or vice-versa, even if the
        /// GameObject is marked with Don't Destroy On Load. Call the
        /// DestroyOnSceneLoad method to indicate a GameObject should stick around
        /// between levels but not other types of scene changes.
        /// </summary>
        public static List<GameObject> DestroyOnSceneLoads = new List<GameObject>();
        /// <summary>
        /// This is the default scale of the canvas. Usually extension methods 
        /// involving the canvas will attempt to get the scale of the canvas
        /// instead of using this, but if any methods are run before the canvas
        /// is loaded this will be used instead.
        /// </summary>
        public const float CanvasScale = 1.6f;

        /// <summary>
        /// Causes the AudioSource to fade out of the current track and into the
        /// specified one. This method can only be called one a time, do not fade
        /// two AudioSources at the same time.
        /// </summary>
        /// <param name="source">The AudioSource.</param>
        /// <param name="volume">The volume of the track that is being faded into.</param>
        /// <param name="speed">The amount each frame to lower the volume.</param>
        /// <param name="clip">The new clip to fade into, leave null if you want the AudioSource to fade out.</param>
        public static void Fade(this AudioSource source, float volume, float speed = .02f, AudioClip clip = null)
        {
            Timing.RunCoroutine(FadeAudio(), Segment.Update, "ExtensionMethodsFadeAudioSource");

            IEnumerator<float> FadeAudio()
            {
                // Fade out
                while (source.volume > 0f)
                {
                    source.volume -= speed;
                    if (source.volume < 0f)
                    {
                        source.volume = 0f;
                        
                    }

                    yield return Timing.WaitForOneFrame;
                }

                // Set the audio source's new clip
                source.clip = clip;
                if (clip != null)
                {
                    if (!source.isPlaying)          // Play the clip
                    {
                        source.Play();
                    }

                    while (source.volume < volume)  // Fade the music in
                    {
                        source.volume += speed;
                        if (source.volume > volume)
                        {
                            source.volume = volume;
                        }

                        yield return Timing.WaitForOneFrame;
                    }
                }
            }
        }

        /// <summary>
        /// If an AudioSource is fading between two tracks using the Fade() extension
        /// method, calling this will cause the fade to cancel. This does not set the
        /// volume or track of the fade, so if this is called during a point where
        /// the volume is low, the track's volume will remain that way.
        /// </summary>
        /// <param name="source">The AudioSource.</param>
        public static void CancelAudioFade(this AudioSource source)
        {
            Timing.KillCoroutines("ExtensionMethodsFadeAudioSource");
        }

        /// <summary>
        /// Sets the lossy (global) scale of a transform by detaching it from it's parent,
        /// setting the scale, and then re-attaching to its parent.
        /// </summary>
        /// <param name="transform">The transform.</param>
        /// <param name="lossyScale">The scale that will be set in global world units.</param>
        public static void SetLossyScale(this Transform transform, Vector3 lossyScale)
        {
            Transform parent = transform.parent;
            transform.parent = null;
            transform.localScale = lossyScale;
            transform.parent = parent;
        }

        /// <summary>
        /// Blends the speicifed colors with other supplied colors.
        /// </summary>
        /// <param name="color">The original color.</param>
        /// <param name="aColors">The colors to blend with.</param>
        /// <returns>The new blended color.</returns>
        public static Color BlendColors(this Color color, params Color[] aColors)
        {
            Color result = color;
            foreach (Color c in aColors)
            { 
                result += c;
            }

            result /= aColors.Length + 1;

            return result;
        }

        /// <summary>
        /// Makes the object target be destroyed when the scene changes between a scene
        /// and a non-level scene. For example, from a level to the title screen.
        /// </summary>
        /// <param name="monoBehaviour">The GameObject to clean on scene change.</param>
        public static void DestroyOnSceneLoad(this MonoBehaviour monoBehaviour)
        {
            DestroyOnSceneLoads.RemoveAll(o => o == null);
            DestroyOnSceneLoads.Add(monoBehaviour.gameObject);
        }

        /// <summary>
        /// Makes the object target be destroyed when the scene changes between a scene
        /// and a non-level scene. For example, from a level to the title screen.
        /// </summary>
        /// <param name="gameObject">The GameObject to clean on scene change.</param>
        public static void DestroyOnSceneLoad(this GameObject gameObject)
        {
            DestroyOnSceneLoads.RemoveAll(o => o == null);
            DestroyOnSceneLoads.Add(gameObject);
        }

        /// <summary>
        /// Checks to see if a reference to an array is null, or if it isn't 
        /// null, checks to see if the array is empty.
        /// </summary>
        /// <param name="array">The array to check.</param>
        /// <returns>True if the array is null or empty.</returns>
        public static bool IsNullOrEmpty(this Array array)
        {
            return array == null || array.Length == 0;
        }

        /// <summary>
        /// Gets the midpoint position between all provided GameObjects.
        /// </summary>
        /// <param name="monoBehaviours">The MonoBehaviors whose GameObjects we want the midpoint of.</param>
        /// <returns>The midpoint of all GameObjects.</returns>
        public static Vector3 GetMidpoint<T>(this List<T> monoBehaviours) where T : MonoBehaviour
        {
            if (monoBehaviours.Count == 0)
            {
                return Vector3.zero;
            }

            if (monoBehaviours.Count == 1)
            {
                return monoBehaviours[1].transform.position;
            }

            Bounds bounds = new Bounds(monoBehaviours[0].transform.position, Vector3.zero);
            foreach (T monoBehaviour in monoBehaviours)
            {
                bounds.Encapsulate(monoBehaviour.transform.position);
            }

            return bounds.center;
        }

        /// <summary>
        /// Version of the Bounds.contains method that works with <see cref="Vector2"/>s 
        /// and not <see cref="Vector3"/>s.
        /// </summary>
        /// <param name="bounds">The <see cref="Bounds"/>.</param>
        /// <param name="position">The point to check.</param>
        /// <returns>Whether or not position is inside bounds.</returns>
        public static bool ContainsVector2(this Bounds bounds, Vector2 position)
        {
            if (position.x >= bounds.min.x && position.x <= bounds.max.x && position.y >= bounds.min.y && position.y <= bounds.max.y)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Gets a Bounds that represent the UI element's bounding box in world
        /// units.
        /// </summary>
        /// <param name="graphic">The UI element to get the Bounds for.</param>
        /// <returns>The Bounds that represent the UI's box.</returns>
        public static Bounds GetWorldBounds(this Graphic graphic)
        {
            return GetWorldBounds(graphic.rectTransform);
        }

        /// <summary>
        /// Gets a Bounds that represent the UI element's bounding box in world
        /// units.
        /// </summary>
        /// <param name="rectTransform">The UI element to get the Bounds for.</param>
        /// <returns>The Bounds that represent the UI's box.</returns>
        public static Bounds GetWorldBounds(this RectTransform rectTransform)
        {
            float realCanvasScale = MainCanvasController.Main == null ? CanvasScale :
                MainCanvasController.Main.GetComponent<RectTransform>().localScale.x;

            return new Bounds
            (
                rectTransform.position,
                new Vector3
                (
                    rectTransform.sizeDelta.x * realCanvasScale,
                    rectTransform.sizeDelta.y * realCanvasScale,
                    0.0f
                )
            );
        }

        /// <summary>
        /// Gets the bounds of a UI element in canvas space.
        /// </summary>
        /// <param name="graphic">The UI element to get the bounds for.</param>
        /// <returns>The bounds of the UI element.</returns>
        public static Bounds GetAnchoredBounds(this Graphic graphic)
        {
            return GetAnchoredBounds(graphic.rectTransform);
        }

        /// <summary>
        /// Gets the bounds of a UI element in canvas space.
        /// </summary>
        /// <param name="rectTransform">The UI element to get the bounds for.</param>
        /// <returns>The bounds of the UI element.</returns>
        public static Bounds GetAnchoredBounds(this RectTransform rectTransform)
        {
            return new Bounds
            (
                rectTransform.anchoredPosition,
                new Vector3
                (
                    rectTransform.sizeDelta.x,
                    rectTransform.sizeDelta.y,
                    0.0f
                )
            );
        }

        /// <summary>
        /// Gets the world bounds of a 2D camera.
        /// </summary>
        /// <param name="camera">The camera to get the bounds for.</param>
        /// <returns>The bounds of the camera's view within the world.</returns>
        public static Bounds OrthographicBounds(this Camera camera)
        {
            float screenAspect = Screen.width / (float)Screen.height;
            float cameraHeight = camera.orthographicSize * 2;
            Bounds bounds = new Bounds(camera.transform.position, new Vector3(cameraHeight * screenAspect, cameraHeight, 0f));

            return bounds;
        }

        /// <summary>
        /// Gets a <see cref="Collider2D"/> on the <see cref="GameObject"/> 
        /// that is not a trigger. Returns null if there are no 
        /// <see cref="Collider2D"/>s.
        /// </summary>
        /// <param name="monoBehaviour">The <see cref="MonoBehaviour"/>
        /// whose <see cref="GameObject"/> to get a 
        /// <see cref="Collider2D"/> on.</param>
        /// <returns>A non-trigger <see cref="Collider2D"/> if there is 
        /// one.</returns>
        public static Collider2D GetSolidCollider(this MonoBehaviour monoBehaviour)
        {
            foreach (Collider2D collider2D in monoBehaviour.GetComponents<Collider2D>())
            {
                if (!collider2D.isTrigger)
                {
                    return collider2D;
                }
            }

            return null;
        }

        public static bool IsSolidTop(this Collider2D other)
        {
            return other.GetComponent<SolidTop>() != null;
        }

        /// <summary>
        /// Whether or not the mouse cursor is inside a UI element or not.
        /// </summary>
        /// <param name="graphic">The UI element to check.</param>
        /// <returns>True if the mouse is inside the element.</returns>
        public static bool MouseInside(this Graphic graphic)
        {
            return graphic.GetWorldBounds().Contains(InputManager.mousePosition);
        }

        /// <summary>
        /// Whether or not the mouse cursor is inside a UI element or not.
        /// </summary>
        /// <param name="rectTransform">The UI element to check.</param>
        /// <returns>True if the mouse is inside the element.</returns>
        public static bool MouseInside(this RectTransform rectTransform)
        {
            return rectTransform.GetWorldBounds().Contains(InputManager.mousePosition);
        }

        /// <summary>
        /// Returns an IEnumerable with all base types of a type.
        /// </summary>
        /// <param name="type">The type to get all base classes for.</param>
        /// <returns>All of the base types.</returns>
        public static IEnumerable<Type> GetBaseTypes(this Type type)
        {
            if (type.BaseType == null) return type.GetInterfaces();

            return Enumerable.Repeat(type.BaseType, 1)
            .Concat(type.GetInterfaces())
            .Concat(type.GetInterfaces().SelectMany(GetBaseTypes))
            .Concat(type.BaseType.GetBaseTypes());
        }

        /// <summary>
        /// Waits for the specified amount of time, but takes into account the current time
        /// effect of the attached TimeOperator. Will throw an error if called on a MonoBehavior
        /// whose GameObject does not have a TimeOperator attached to it.
        /// </summary>
        /// <param name="monoBehaviour">The MonoBehavior.</param>
        /// <param name="seconds">The amount in seconds to wait.</param>
        /// <returns>IEnumerator which can be used by MEC coroutine.</returns>
        public static IEnumerator<float> WaitForSecondsScaled(this MonoBehaviour monoBehaviour, float seconds)
        {
            if (monoBehaviour == null) yield break;

            TimeOperator timeOperator = monoBehaviour.GetComponent<TimeOperator>();
            if (timeOperator == null)
            {
                timeOperator = monoBehaviour.GetComponentInParent<TimeOperator>();
            }

            for (float f = 0f; f < seconds; f += Time.fixedDeltaTime * GetModifier(timeOperator))
            {
                if (monoBehaviour == null) yield break;
                
                yield return Timing.WaitUntilDone
                (
                    new WaitUntil
                    (
                        () => timeOperator.CurrentTimeEffect != TimeEffect.Pause &&
                              timeOperator.CurrentTimeEffect != TimeEffect.Rewind
                    )
                );
                yield return Timing.WaitForOneFrame;
                if (f < 0)
                {
                    yield break;
                }
            }

            float GetModifier(TimeOperator t)
            {
                if (monoBehaviour == null)
                {
                    return 0f;
                }

                float modifier;
                switch (t.CurrentTimeEffect)
                {
                    case TimeEffect.Slow:
                        modifier = TimeOperator.SlowScale;
                        break;
                    case TimeEffect.FastForward:
                        modifier = TimeOperator.FastForwardScale;
                        break;
                    default:
                        modifier = 1f;
                        break;
                }

                return modifier;
            }
        }

        /// <summary>
        /// Waits for the specified amount of time, but takes into account the current time
        /// effect of the attached TimeOperator. Will throw an error if called on a MonoBehavior
        /// whose GameObject does not have a TimeOperator attached to it. 
        /// </summary>
        /// <param name="monoBehaviour">The MonoBehavior.</param>
        /// <param name="seconds">The amount of seconds to wait.</param>
        /// <returns>The number of seconds to wait, should be used by an MEC coroutine.</returns>
        public static float WaitForFixedSecondsScaled(this MonoBehaviour monoBehaviour, float seconds)
        {
            return Timing.WaitUntilDone
            (
                Timing.RunCoroutine
                (
                    monoBehaviour.WaitForSecondsScaled(seconds), 
                    Segment.FixedUpdate, 
                    monoBehaviour.GetInstanceID().ToString()
                )
            );
        }
    }
}
