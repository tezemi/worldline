﻿using System;
using System.Reflection;
using System.Linq.Expressions;
using System.Collections.Generic;

namespace Worldline.Utilities
{
    /// <summary>
    /// Used to create instances of classes from their Types without the 
    /// use of generics which Unity doesn't support. Faster than using
    /// Activator.CreateInstance().
    /// </summary>
    public static class ObjectCreator
    {
        public static Dictionary<Type, Creator> CachedCreators { get; } = new Dictionary<Type, Creator>();
        public delegate object Creator(params object[] args);

        /// <summary>
        /// Creates a Creator, a delegate that can create instances of an 
        /// object.
        /// </summary>
        /// <param name="t">The type of the object.</param>
        /// <returns>The newly created Creator.</returns>
        public static Creator GetCreator(Type t)
        {
            ConstructorInfo[] constructors = t.GetConstructors();
            if (constructors.Length >= 0)
            {
                Creator compiled;
                ConstructorInfo constructor = constructors[0];
                ParameterInfo[] paramsInfo = constructor.GetParameters();
                ParameterExpression param = Expression.Parameter(typeof(object[]), "args");
                if (paramsInfo.Length > 0)
                {                    
                    Expression[] argsExpressions = new Expression[paramsInfo.Length];
                    for (int i = 0; i < paramsInfo.Length; i++)
                    {
                        Expression index = Expression.Constant(i);
                        Type paramType = paramsInfo[i].ParameterType;
                        Expression paramAccessorExp = Expression.ArrayIndex(param, index);
                        Expression paramCastExp = Expression.Convert(paramAccessorExp, paramType);
                        argsExpressions[i] = paramCastExp;
                    }

                    NewExpression newExpression = Expression.New(constructor, argsExpressions);
                    LambdaExpression lambda = Expression.Lambda(typeof(Creator), newExpression, param);
                    compiled = (Creator)lambda.Compile();                    
                }
                else
                {
                    NewExpression newExpression = Expression.New(constructor);
                    LambdaExpression lambda = Expression.Lambda(typeof(Creator), newExpression, param);
                    compiled = (Creator)lambda.Compile();
                }

                return compiled;
            }

            return null;
        }

        /// <summary>
        /// Creates an instance of a type from its Creator.
        /// </summary>
        /// <param name="t">The type of the object.</param>
        /// <param name="args">Any arguments that need to be passed into the 
        /// constructor.</param>
        /// <returns>The instance of the object.</returns>
        public static object CreateInstance(Type t, params object[] args)
        {
            Creator creator;
            if (CachedCreators.ContainsKey(t))
            {
                creator = CachedCreators[t];
            }
            else
            {
                creator = GetCreator(t);
                CachedCreators.Add(t, creator);
            }           

            return creator(args);
        }
    }
}
