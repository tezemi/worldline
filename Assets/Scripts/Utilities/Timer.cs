// Copyright (c) 2019 Destin Hebner
using System;
using System.Reflection;
using Worldline.TimeEngine;
using ProtoBuf;
using UnityEngine;

namespace Worldline.Utilities
{    
    public class Timer : MonoBehaviour
    {
        public float Delay;
        public int Iterations = 1;
        public bool DestroyOnEnd;
        public bool DestroyGameObjectOnEnd;
        public bool EffectedByTimescale;
        public bool OnlyEffectedByRewind;
        /// <summary>
        /// If true, this timer will destroy itself when it is targeting a null game object, or a game object 
        /// that isn't the game object it is attached to.
        /// </summary>
        public bool TargetSpecificGameObject;
        /// <summary>
        /// Timers that are important can be effected by time controls, but won't be destroyed. (On rewind, record, ect.)
        /// </summary>
        public bool Important = true;
        public float SerializedTimeElapsed;
        public Action Elapsed { get; set; }
        public Action ElaspedReverse { get; set; }
        public int CurrentIterations { get;  set; }
        public float ActualTime { get; set; }
        /// <summary>
        /// Used be the serializer. Do not set if this timer should not be serialized.
        /// </summary>
        public string UniqueName { get; set; }
        /// <summary>
        /// Used by the serializer to determine the elapsed class.
        /// </summary>
        public string ElapsedClassName { get; set; }
        /// <summary>
        /// Used by the serializer to find the event method in the elapsed class.
        /// </summary>
        public string ElapsedMethodName { get; set; }

        public static Timer CreateNewTimer(GameObject gameObject, float delay, int iterations, bool startOn = true, bool boolDestroyOnEnd = true)
        {
            Timer timer = gameObject.AddComponent<Timer>();
            timer.Delay = delay;
            timer.Iterations = iterations;
            timer.DestroyOnEnd = boolDestroyOnEnd;
            timer.enabled = startOn;

            return timer;
        }

        protected void FixedUpdate()
        {
            // Clean up timers that don't target this game object
            // This can happen when creating clones
            if (TargetSpecificGameObject && (Elapsed?.Target == null || Elapsed.Target is MonoBehaviour target && (target.gameObject == null || target.gameObject != gameObject)))
            {
                Destroy(this);
            }

            SerializedTimeElapsed = ActualTime;

            if (GetComponent<TimeOperator>() != null && EffectedByTimescale && !OnlyEffectedByRewind)
            {
                switch (GetComponent<TimeOperator>().CurrentTimeEffect)
                {
                    case TimeEffect.Pause:
                        break;
                    case TimeEffect.Slow:
                        ActualTime += Time.fixedDeltaTime * 0.5f;
                        break;
                    case TimeEffect.Rewind:
                        ActualTime += -Time.fixedDeltaTime;
                        break;
                    case TimeEffect.FastForward:
                        ActualTime += Time.fixedDeltaTime * 2f;
                        break;
                    case TimeEffect.Record:
                        break;
                    case TimeEffect.Reset:
                        break;
                    case TimeEffect.None:
                        ActualTime += Time.fixedDeltaTime;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else if (GetComponent<TimeOperator>() != null && EffectedByTimescale && OnlyEffectedByRewind && GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
            {
                ActualTime += -Time.fixedDeltaTime;
            }
            else
            {
                ActualTime += Time.fixedDeltaTime;
            }

            if (ActualTime >= Delay) 
            {                
                ActualTime = 0;
                CurrentIterations++;
                InvokeElapsed();
            }

            if (ActualTime < 0)
            {
                ActualTime = Delay;
                CurrentIterations--;
                ElaspedReverse?.Invoke();
            }

            if (Iterations != 0 && CurrentIterations == Iterations)
            {
                CurrentIterations = 0;
                if (DestroyOnEnd) Destroy(this);
                if (DestroyGameObjectOnEnd) Destroy(gameObject);
                enabled = false;                
            }
        }

        public void StartTimer()
        {
            ActualTime = 0;
            CurrentIterations = 0;
            enabled = true;
        }

        /// <summary>
        /// End the timer early, calling the elapsed method.
        /// </summary>
        public void EndTimer()
        {
            ActualTime = Delay;
            CurrentIterations = Iterations;
            InvokeElapsed();
            enabled = false;
        }

        private void InvokeElapsed()
        {
            if (ElapsedClassName != null && ElapsedMethodName != null)
            {
                Type t = Type.GetType(ElapsedClassName);
                if (t == null) throw new ArgumentException("Could not find timer event type.");
                object c = Activator.CreateInstance(t);
                MethodInfo mi = t.GetMethod(ElapsedMethodName, BindingFlags.Static | BindingFlags.NonPublic);
                if (mi == null) throw new MissingMethodException("Method could not be found for timer event. Did you make it static you asshole?", ElapsedMethodName);
                mi.Invoke(c, null);
            }
            else
            {
                Elapsed?.Invoke();
            }
        }

        [ProtoContract]
        public class TimerData
        {
            [ProtoMember(1)]
            private float _delay;
            [ProtoMember(2)]
            private int _iterations = 1;
            [ProtoMember(3)]
            private bool _destroyOnEnd;
            [ProtoMember(4)]
            private bool _destroyGameObjectOnEnd;
            [ProtoMember(5)]
            private bool _effectedByTimescale = true;
            [ProtoMember(6)]
            private bool _important;
            [ProtoMember(7)]
            private int _currentIterations;
            [ProtoMember(8)]
            private float _actualTime;
            [ProtoMember(9)]
            private string _elapsedClassName;
            [ProtoMember(10)]
            private string _elapsedMethodName;
            [ProtoMember(11)]
            private bool _enabled;
            [ProtoMember(12)]
            public string Name;

            public TimerData GetTimerData(Timer t)
            {
                _delay = t.Delay;
                _iterations = t.Iterations;
                _destroyOnEnd = t.DestroyOnEnd;
                _destroyGameObjectOnEnd = t.DestroyGameObjectOnEnd;
                _effectedByTimescale = t.EffectedByTimescale;
                _important = t.Important;
                _currentIterations = t.CurrentIterations;
                _actualTime = t.ActualTime;
                _elapsedClassName = t.ElapsedClassName;
                _elapsedMethodName = t.ElapsedMethodName;
                _enabled = t.enabled;
                Name = t.UniqueName ?? throw new ArgumentException("Timer not properly set up to be serialized.");

                return this;
            }

            public TimerData SetTimerData(Timer t)
            {
                t.Delay = _delay;
                t.Iterations = _iterations;
                t.DestroyOnEnd = _destroyOnEnd;
                t.DestroyGameObjectOnEnd = _destroyGameObjectOnEnd;
                t.EffectedByTimescale = _effectedByTimescale;
                t.Important = _important;
                t.CurrentIterations = _currentIterations;
                t.ActualTime = _actualTime;
                t.ElapsedClassName = _elapsedClassName;
                t.ElapsedMethodName = _elapsedMethodName;
                t.enabled = _enabled;
                t.UniqueName = Name;

                return this;
            }
        }
    }
}
