﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Utilities
{
    /// <summary>
    /// Static class that contains math utility methods used for Worldline.
    /// </summary>
    public static class MathUtilities
    {
        /// <summary>
        /// Returns what a value should be within another range of values.
        /// </summary>
        /// <param name="input">The value we are converting.</param>
        /// <param name="inputLow">The lowest possible input the value could be.</param>
        /// <param name="inputHigh">The highest possible input the value could be.</param>
        /// <param name="outputHigh">The highest possible output value after conversion.</param>
        /// <param name="outputLow">The lowest possible output value after conversion.</param>
        /// <returns></returns>
        public static float GetRange(float input, float inputLow, float inputHigh, float outputHigh, float outputLow)
        {
            return (input - inputLow) / (inputHigh - inputLow) * (outputHigh - outputLow) + outputLow;
        }

        /// <summary>
        /// Returns a random float between the specified range using the specified 
        /// weight. When the weight is equal to one, it will be more likely to return
        /// something closer to the maximum value than the minimum.
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <param name="weight"></param>
        /// <returns></returns>
        public static float MaxWeightedRandomRange(float minValue, float maxValue, float weight = 1f)
        {
            float x = Random.Range(minValue, maxValue);
            float y = Random.Range(0f, weight);

            return maxValue - x * y;
        }

        /// <summary>
        /// Returns a vector containing the absolute value from X, Y, and Z.
        /// </summary>
        /// <param name="vector2">The vector to get the absolute value for</param>
        /// <returns>The absolute vector.</returns>
        public static Vector2 Abs(Vector2 vector2)
        {
            return new Vector2(Mathf.Abs(vector2.x), Mathf.Abs(vector2.y));
        }

        /// <summary>
        /// Returns a vector containing the absolute value from X, Y, and Z.
        /// </summary>
        /// <param name="vector3">The vector to get the absolute value for</param>
        /// <returns>The absolute vector.</returns>
        public static Vector3 Abs(Vector3 vector3)
        {
            return new Vector3(Mathf.Abs(vector3.x), Mathf.Abs(vector3.y), Mathf.Abs(vector3.z));
        }
    }
}
