﻿using System.Xml.Serialization;
using System.Collections.Generic;
using Worldline.GUI.Printing;
using TeamUtility.IO;

namespace Worldline.Utilities
{
    /// <summary>
    /// Represents a node containing dialog and dialog metadata in an XML 
    /// document. Can be implicitly converted to a string to get the value
    /// inside the node.
    /// </summary>
    public class StringNode
    {
        /// <summary>
        /// The name of the node. Usually a reference to its purpose in the 
        /// game, such as, "PauseMenuExit."
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }
        /// <summary>
        /// The name of the optional PortraitBundle to load. This is typically 
        /// the name of the character, like "Greg."
        /// </summary>
        [XmlAttribute("bundle")]
        public string Bundle { get; set; }
        /// <summary>
        /// The name of the portrait as it is stored within the bundle. This is 
        /// not the name of a sprite, but usually an emotion, like "Happy" or 
        /// "Screaming."
        /// </summary>
        [XmlAttribute("portrait")]
        public string Portrait { get; set; }
        /// <summary>
        /// The original raw value of the text inside the node, with no modifications.
        /// </summary>
        [XmlText]
        public string OriginalValue { get; set; }
        /// <summary>
        /// The actual text within the StringNode. This is the raw text, 
        /// markup and all, excluding any input tags, which get replaced
        /// automatically.
        /// </summary>
        public string Value
        {
            get
            {
                string working = OriginalValue;
                AdvancedText.RemoveMarkup(working, out List<MarkupTag> markupTags);
                foreach (MarkupTag tag in markupTags)
                {
                    if (tag.Type == TagType.Input)
                    {
                        string tagBody = tag.Body;
                        tagBody = tagBody.Replace("input=", "");
                        tagBody = tagBody.Replace("\"", "");
                        string inputString = InputManager.GetAxisConfiguration(PlayerID.One, tagBody).positive.ToString();
                        working = working.Replace(tag.Text, GetPrettyInput(inputString));
                    }
                }

                return working;
            }
            set
            {
                OriginalValue = value;
            }
        }

        public override string ToString()
        {
            return Value;
        }

        public static string GetPrettyInput(string input)
        {
            switch (input)
            {
                case "Alpha1":
                    return "1";
                case "Alpha2":
                    return "2";
                case "Alpha3":
                    return "3";
                case "Alpha4":
                    return "4";
                case "Alpha5":
                    return "5";
                case "Alpha6":
                    return "6";
                case "Alpha7":
                    return "7";
                case "Alpha8":
                    return "8";
                case "Alpha9":
                    return "9";
                case "Alpha0":
                    return "0";
                case "Mouse0":
                    return "Left Click";
                case "Mouse1":
                    return "Right Click";
                case "Mouse2":
                    return "Middle Mouse";
                case "Mouse3":
                    return "Scroll Wheel Up";
                case "Mouse4":
                    return "Scroll Wheel Down";
                case "LeftControl":
                    return "Left Control";
                case "RightControl":
                    return "Right Control";
                case "LeftShift":
                    return "Left Shift";
                case "RightShift":
                    return "Right Shift";
            }

            return input;
        }

        public static implicit operator string(StringNode stringNode)
        {
            return stringNode.Value;
        }
    }
}
