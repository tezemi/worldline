﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.Utilities
{
    public interface IPoolable
    {
        void Reuse();
    }
}
