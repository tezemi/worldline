﻿using System.IO;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Worldline.GameState;
using UnityEngine;

namespace Worldline.Utilities
{
    /// <summary>
    /// Represents an XML document containing dialog. Each node of dialog 
    /// is stored as a StringNode in the Strings array.
    /// </summary>
    [XmlRoot(ElementName = "SerializedStrings", Namespace = "worldline-game")]
    public class SerializedStrings
    {
        /// <summary>
        /// Contains all of the string nodes in an XML document.
        /// </summary>
        [XmlArray("Strings"), XmlArrayItem("String")]
        public StringNode[] Strings { get; set; }
        /// <summary>
        /// Holds the previously deserialized XML document. This is done as 
        /// text is often loaded from XML contiguously, and there is no need
        /// to serialize the same document over and over.
        /// </summary>
        [XmlIgnore]
        private static (string, SerializedStrings) _currentStrings;

        /// <summary>
        /// While instances of SerializedStrings can be created, it is only 
        /// done so internally for caching purposes. Externally, this class
        /// might as well be considered static.
        /// </summary>
        private SerializedStrings() { }

        /// <summary>
        /// Deserializes and returns an XML document holding game text. The 
        /// path to the document should start from Assets/Data/Text/, and it
        /// should not contain the .xml extension. If the game uses a language
        /// other than english, this will automatically try to find an load
        /// alternative language files.
        /// </summary>
        /// <param name="path">The path to the document relative to the text folder.</param>
        /// <returns>The XML document in its deserializes format.</returns>
        public static SerializedStrings Deserialize(string path)
        {
            SerializedStrings strings = null;
            string file = $"{Application.dataPath}/Data/Text/{path}";
            string englishFile = file;
            if (GameOptions.LoadedGameOptions.Language != Language.EN_US)
            {
                file += GameOptions.LoadedGameOptions.Language;
            }

            if (GameOptions.LoadedGameOptions.Language != Language.EN_US && !File.Exists($"{file}.xml"))
            {   // If the game is set to another language but it can't find the file, default to english.
                file = englishFile;
            }

            file += ".xml";
            if (File.Exists(file))
            {
                using (FileStream fileStream = new FileStream(file, FileMode.Open))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(SerializedStrings));
                    strings = (SerializedStrings)xmlSerializer.Deserialize(fileStream);
                }
            }

            return strings;
        }

        /// <summary>
        /// Gets a StringNode deserialized from an XML document. The name of 
        /// the document must be specified, as well as the name of the node. 
        /// If no node or document is found, this returns a new node that
        /// contains error information.
        /// </summary>
        /// <param name="xmlDocumentName">The name of the XML document to 
        /// load.</param>
        /// <param name="nodeName">The name of the node within the document's 
        /// 'Strings' element.</param>
        /// <returns>A deserialized StringNode.</returns>
        public static StringNode GetString(string xmlDocumentName, string nodeName)
        {
            if (_currentStrings.Item1 != xmlDocumentName)
            {
                _currentStrings = (xmlDocumentName, Deserialize(xmlDocumentName));
            }

            StringNode node = _currentStrings.Item2.Strings.FirstOrDefault(n => n.Name == nodeName) ?? new StringNode
            {
                Bundle = null,
                Name = "NoNode",
                Portrait = "",
                Value = $"StringNode {nodeName} not loaded."
            };

            if (CheatController.IsActive(Cheat.Censorship))
            {
                Dictionary<string, string> grawlixes = new Dictionary<string, string>
                {
                    { "ass", "@$#" },
                    { "hell", "@#$%" },
                    { "damn", "$%#@" },
                    { "shit", "&#$@" },
                    { "dick", "$%!@" },
                    { "fuck", "$#@&" },
                    { "bitch", "$#@&%" },
                    { "fucker", "$#@&%#" },
                    { "asshole", "@$#&*$#" },
                    { "fucking", "$#@&%#!" }
                };

                foreach (string key in grawlixes.Keys)
                {
                    if (node.Value.ToLower().Contains(key))
                    {
                        node.Value = Regex.Replace(node.Value, $"(?i){key}+", grawlixes[key]);
                    }
                }
            }

            return node;
        }

        /// <summary>
        /// Same as the GetString method, but if the specified node can not be 
        /// found, this will return null rather than an error-specific StringNode.
        /// </summary>
        /// <param name="xmlDocumentName">The name of the XML document to load from.</param>
        /// <param name="nodeName">The name of the node to search for.</param>
        /// <returns>The found node, or null if it was not found.</returns>
        public static StringNode GetStringOrNull(string xmlDocumentName, string nodeName)
        {
            if (_currentStrings.Item1 != xmlDocumentName)
            {
                _currentStrings = (xmlDocumentName, Deserialize(xmlDocumentName));
            }

            StringNode node = _currentStrings.Item2.Strings.FirstOrDefault(n => n.Name == nodeName);

            return node;
        }

        public static StringNode GetAny()
        {
            StringNode node = new StringNode
            {
                Bundle = null,
                Name = "NoNode",
                Portrait = "",
                Value = "STR ERROR"
            };

            if (_currentStrings.Item2 != null)
            {
                node = _currentStrings.Item2.Strings[Random.Range(0, _currentStrings.Item2.Strings.Length)];
            }

            return node;
        }
    }
}
