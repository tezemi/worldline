﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Utilities
{
    public class EdgeColliderSnapper : MonoBehaviour
    {
        public bool DisableSnapping;

        protected virtual void OnDrawGizmos()
        {
            if (DisableSnapping) return;

            foreach (EdgeCollider2D edgeCollider2D in GetComponents<EdgeCollider2D>())
            {
                Vector2[] adjustedPoints = new Vector2[edgeCollider2D.points.Length];
                for (var i = 0; i < edgeCollider2D.points.Length; i++)
                {
                    Vector2 point = edgeCollider2D.points[i];
                    point.x = Mathf.RoundToInt(point.x);
                    point.y = Mathf.RoundToInt(point.y);
                    adjustedPoints[i] = point;
                }

                edgeCollider2D.points = adjustedPoints;
            }
        }
    }
}
