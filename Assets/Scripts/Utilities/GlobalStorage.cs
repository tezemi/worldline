﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.Audio;

namespace Worldline.Utilities
{
    /// <summary>
    /// This is a ScriptableObject and one of the only serialized assets in
    /// the project that resides inside of the Resources folder. This
    /// ScriptableObject contains data that is required and present
    /// throughout the lifespan of the game, and because it is inside the
    /// resources folder, it can be accessed from anywhere, even scenes with
    /// no other serialization within them. Due to the fact that is data is
    /// always loaded into memory, this should be used to store information
    /// sparingly, and only when absolutely required.
    /// </summary>
    [CreateAssetMenu(fileName = "Global Storage", menuName = "Data/GlobalStorage")]
    public class GlobalStorage : ScriptableObject
    {
        public AudioMixerGroup BackgroundMusicAudioMixerGroup;
        public AudioMixerGroup SoundEffectsAudioMixerGroup;

        public static GlobalStorage Get()
        {
            return Resources.Load<GlobalStorage>("Global Storage");
        }
    }
}
