﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Utilities
{
    /// <summary>
    /// Keeps a GameObject locked in place regardless of where its parent 
    /// moves.
    /// </summary>
    public class TransformLocker : MonoBehaviour
    {
        public bool LockPosition;
        public bool LockScale;
        public bool LockRotation;
        protected Vector3 StartPosition { get; set; }
        protected Vector3 StartScale { get; set; }
        protected Quaternion StartRotation { get; set; }

        protected virtual void Awake()
        {
            StartPosition = transform.position;
            StartScale = transform.lossyScale;
            StartRotation = transform.rotation;
        }

        protected virtual void Update()
        {
            if (LockPosition)
            {
                transform.position = StartPosition;
            }

            if (LockScale)
            {
                transform.SetLossyScale(StartScale);
            }

            if (LockRotation)
            {
                transform.rotation = StartRotation;
            }
        }
    }
}
