// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using System.IO;
using MEC;
using Worldline.GUI;
using Worldline.Items;
using Worldline.Level;
using Worldline.Player;
using Worldline.GUI.HUD;
using Worldline.GameState;
using Worldline.Player.Weapons;
using Worldline.Player.Upgrades;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using Worldline.Corruption;

namespace Worldline.Utilities
{
    /// <summary>
    /// Component used for overall debugging, with a simple debugging menu.
    /// Press 'B' to open the debugging menu, and 'ESCAPE' to close the
    /// menu. You can type the name of a level into the input field and press
    /// 'RETURN' to go to that level. Press '.' while the menu is open to
    /// provide the player with weapons and upgrades. Press 'H' while the
    /// menu is open to turn off or turn on the HUD.
    /// </summary>
    public class DebugUtils : MonoBehaviour
    {
        public static DebugUtils Main;
        public Text Fps;
        public Text DeltaTime;
        public Text HealMeter;
        public Upgrade WallJumpUpgrade;
        public Upgrade DashUpgrade;
        public Upgrade InfiniteJumpUpgrade;
        public Weapon DualPistols;
        public Weapon NullSphereLauncher;
        public Weapon[] Weapons;
        public Upgrade[] Upgrades;
        public Item[] Items;
        public InputField InputField;
        private bool _active;
        private int? _resIndex;
        private float _deltaTime;

        /// <summary>
        /// Whether or not the debugging menu is on. If this is set to true, the
        /// elements of the debugging menu will be enabled, and the InputField
        /// will be focused on. Setting this to false causes the menu elements
        /// to close.
        /// </summary>
        public bool Active
        {
            get => _active;
            set
            {
                Fps.enabled = value;
                DeltaTime.enabled = value;
                HealMeter.enabled = value;
                InputField.gameObject.SetActive(value);
                _active = value;
            }
        }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            Timing.RunCoroutine(UpdateTick(), Segment.Update);
        }

        protected IEnumerator<float> UpdateTick()
        {
            while (Application.isPlaying)
            {
                yield return Timing.WaitForOneFrame;

                if (Application.targetFrameRate != 60)
                {
                    Application.targetFrameRate = 60;
                }

                // Takes a screenshot
                if (InputManager.GetKeyDown(KeyCode.F12) && Debug.isDebugBuild)
                {
                    Directory.CreateDirectory($"{Application.persistentDataPath}/Screenshots/");
                    ScreenCapture.CaptureScreenshot($"{Application.persistentDataPath}/Screenshots/Worldline-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Year}-{DateTime.Now.Millisecond}.png");
                }

                if (InputManager.GetKey(KeyCode.Space))
                {
                    if (InputManager.GetKeyDown(KeyCode.Period))
                    {
                        Corrupter.Intensity += 10f;
                    }
                    else if (InputManager.GetKeyDown(KeyCode.Comma))
                    {
                        Corrupter.Intensity -= 10f;
                    }

                    if (!_resIndex.HasValue)
                    {
                        _resIndex = GameOptions.LoadedGameOptions.ResolutionIndex;
                    }

                    if (InputManager.GetKeyDown(KeyCode.LeftArrow))
                    {
                        _resIndex -= 1;
                        if (_resIndex < 0)
                        {
                            _resIndex = Screen.resolutions.Length - 1;
                        }

                        Screen.SetResolution(Screen.resolutions[_resIndex.Value].width, Screen.resolutions[_resIndex.Value].height, true);
                    }
                    else if (InputManager.GetKeyDown(KeyCode.Comma))
                    {
                        _resIndex += 1;
                        if (_resIndex >= Screen.resolutions.Length)
                        {
                            _resIndex = 0;
                        }

                        Corrupter.Intensity -= 10f;
                    }
                }

                // This opens up the debug menu
                if (!Debug.isDebugBuild) yield break;

                if (InputManager.GetKeyDown(KeyCode.C))
                {
                    HUDController.Main.CorruptionElements.SetActive(true);
                }

                if (InputManager.GetKeyDown(KeyCode.B) && !Active)
                {
                    Active = true;
                }

                // This closes the debug menu
                if (InputManager.GetKeyDown(KeyCode.Escape) && Active)
                {
                    Active = false;
                }

                if (Active)
                {
                    // Gives the player some weapons and upgrades
                    if (InputManager.GetKeyDown(KeyCode.Period))
                    {
                        GiveAllWeaponsAndUpgrades();
                    }

                    if (InputManager.GetKeyDown(KeyCode.U))
                    {
                        string upgrades = string.Empty;
                        foreach (Upgrade u in MainPlayerOperator.MainPlayerComponent.Upgrades)
                        {
                            upgrades += $"{u.name}, ";
                        }

                        Debug.Log($"Upgrades: {upgrades}");
                        string inventory = string.Empty;
                        foreach (Item i in Item.Inventory)
                        {
                            inventory += $"{i.name}, ";
                        }

                        Debug.Log($"Inventory: {inventory}");

                        InputField.Select();
                    }

                    if (InputManager.GetKeyDown(KeyCode.Keypad0))
                    {
                        SaveFile.LoadedSaveFile.Difficulty = Difficulty.Easy;
                    }

                    if (InputManager.GetKeyDown(KeyCode.Keypad1))
                    {
                        SaveFile.LoadedSaveFile.Difficulty = Difficulty.Normal;
                    }

                    if (InputManager.GetKeyDown(KeyCode.Keypad2))
                    {
                        SaveFile.LoadedSaveFile.Difficulty = Difficulty.Hard;
                    }

                    if (InputManager.GetKeyDown(KeyCode.Alpha9))
                    {
                        Debug.Log("Difficulty changed to AssBlasting.");
                        SaveFile.LoadedSaveFile.Difficulty = Difficulty.AssBlasting;
                    }

                    // Disables to HUD
                    if (InputManager.GetKeyDown(KeyCode.H))
                    {
                        MainCanvasController.Main.gameObject.SetActive(!MainCanvasController.Main.gameObject.activeSelf);
                    }

                    // Sets the target frame rate to 30.
                    if (InputManager.GetKeyDown(KeyCode.LeftBracket))
                    {
                        Application.targetFrameRate = 30;
                    }

                    // Sets frame rate to 60.
                    if (InputManager.GetKeyDown(KeyCode.RightBracket))
                    {
                        Application.targetFrameRate = 60;
                    }

                    // Sets frame rate to 120.
                    if (InputManager.GetKeyDown(KeyCode.Backslash))
                    {
                        Application.targetFrameRate = 120;
                    }

                    _deltaTime = 1.0f / Time.deltaTime;
                    if (_deltaTime >= 59) Fps.color = new Color(1, 1, 1);
                    if (_deltaTime < 59 && _deltaTime >= 60) Fps.color = new Color(.5f, .5f, 0);
                    if (_deltaTime < 29) Fps.color = new Color(1, 0, 0);

                    Fps.text = "FPS: " + Math.Truncate(_deltaTime);
                    DeltaTime.text = "Delta Time: " + Time.deltaTime;
                    if (PlayerWeaponOperator.Main != null && PlayerWeaponOperator.Main.isActiveAndEnabled && PlayerWeaponOperator.Main.HasWeapon<Shotgun>())
                    {
                        HealMeter.text = PlayerWeaponOperator.Main.GetWeapon<Shotgun>().BulletsLoaded.ToString();
                    }

                    if (InputField.text != string.Empty && Input.GetKeyDown(KeyCode.Return))
                    {
                        LevelLoader.Main.LoadLevel(InputField.text, true);
                        InputField.text = string.Empty;
                    }

                    DeltaTime.color = Time.deltaTime <= 0.02f ? Color.white : Color.red;
                }
            }
        }

        public void GiveAllWeaponsAndUpgrades()
        {
            MainPlayerOperator.MainPlayerComponent.MaxHealth = 15;
            MainPlayerOperator.MainPlayerComponent.Health = MainPlayerOperator.MainPlayerComponent.MaxHealth;
            PlayerWeaponOperator.Main.gameObject.SetActive(true);
            foreach (Weapon w in Weapons)
            {
                PlayerWeaponOperator.Main.GiveWeapon(w);
            }

            foreach (Upgrade u in Upgrades)
            {
                MainPlayerOperator.MainPlayerComponent.GiveUpgrade(u);
            }

            foreach (Item i in Items)
            {
                Item.GiveItem(i);
            }
        }
    }
}

