﻿using System.Linq;
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Corruption.SpriteCorruption;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Corruption
{
    /// <summary>
    /// Base abtract class for Corrupters which would effect graphical 
    /// elements of the game. This corrupter creates and populates a
    /// static pool of corrupted sprites that inheritors can use. This
    /// is done so corrupted graphics are not created while the game
    /// is running, which is very expensive.
    /// </summary>
    [HasDefaultState]
    public abstract class GraphicsCorrupter : Corrupter
    {
        /// <summary>
        /// While there can be multiple GraphicsCorrupters, only one can be the 
        /// one that generates the static Sprite data.
        /// </summary>
        public static GraphicsCorrupter MainGraphicsCorrupter { get; private set; }
        /// <summary>
        /// The main SpriteCorruptionTool this GraphicsCorrupter uses for 
        /// generating corrupted looking sprites.
        /// </summary>
        public static SpriteCorruptionTool SpriteCorruptionTool { get; } = new SpriteCorruptionTool();
        /// <summary>
        /// All the game's Sprites currently loaded into memory. This gets 
        /// updated every level, and at random intervals while the game is
        /// being played.
        /// </summary>
        public static Sprite[] SpriteResources { get; private set; }
        /// <summary>
        /// This the keys for this Dictionary will provide values that are 
        /// SpriteCorruptionReferences an object that can hold many different
        /// corrupted looking versions of a single sprite. It can also be 
        /// used to return the preferred index of those sprites.
        /// </summary>
        public static Dictionary<Sprite, SpriteCorruptionReference> CorruptedSpriteResources { get; private set; }
        /// <summary>
        /// Sprites specified here will always have a corrupted variant made.
        /// </summary>
        public Sprite[] AlwaysCorrupt;
        /// <summary>
        /// The maximum possible number of passes the SpriteCorruptionTool could 
        /// generate a sprite with.
        /// </summary>
        public const int MaxPasses = 4;
        /// <summary>
        /// The maximum amount of corrupted Sprites the game will allow before 
        /// it starts discarding old Sprites.
        /// </summary>
        public const int MaxSpritePoolSize = 300;
        /// <summary>
        /// The lowest possible Intensity that could be used while generating
        /// corrupted sprites.
        /// </summary>
        public const float MinIntensity = 0.15f;
        /// <summary>
        /// The maximum size, in pixels, a sprite could be to be eligible for 
        /// corruption. If an input sprite excedes this size, then the Corrupter
        /// will pick another random, smaller sprite.
        /// </summary>
        public const float MaxSpriteSize = 250f;
        private static readonly HashSet<Sprite> _corruptedSprites = new HashSet<Sprite>();

        protected override void OnEnable()
        {
            base.OnEnable();
            if (GameOptions.LoadedGameOptions.PhotosensitiveMode)
            {
                enabled = false;
            }
        }

        protected override void OnLevelLoaded()
        {
            base.OnLevelLoaded();
            if (MainGraphicsCorrupter == null)   // We only want one graphics corrupter to configure corrupted sprites
            {
                MainGraphicsCorrupter = this;
            }
            
            if (MainGraphicsCorrupter != this)
            {
                return;
            }

            float timeBeforeLoad = Time.realtimeSinceStartup;
            
            if (CorruptedSpriteResources == null)
            {
                CorruptedSpriteResources = new Dictionary<Sprite, SpriteCorruptionReference>();
            }
            
            SpriteResources = Resources.FindObjectsOfTypeAll<Sprite>();
            SpriteCorruptionTool.SpriteResources = SpriteResources;
            foreach (Sprite sprite in SpriteResources)
            {
                if (sprite == null || _corruptedSprites.Contains(sprite)) continue;

                // Sprites that have variants get corrupted less often
                if (CorruptedSpriteResources.ContainsKey(sprite) && Random.value < 1f / (CorruptedSpriteResources[sprite].CorruptedSprites.Count * 2f))
                {
                    continue;
                }

                GenerateSprite(sprite, true, Random.Range(1, 4), Random.value);
            }

            // Removes Sprites from top of cache if amount exceeds maximum
            if (CorruptedSpriteResources.Count > MaxSpritePoolSize)
            {
                List<Sprite> spritesToUncache = new List<Sprite>();
                int count = 0;
                foreach (Sprite key in CorruptedSpriteResources.Keys)
                {
                    spritesToUncache.Add(key);
                    if (count++ >= CorruptedSpriteResources.Count - MaxSpritePoolSize)
                    {
                        break;
                    }
                }

                foreach (Sprite key in spritesToUncache)
                {
                    CorruptedSpriteResources.Remove(key);
                }
            }

            Resources.UnloadUnusedAssets();
            float timeAfterLoad = Time.realtimeSinceStartup;
            Debug.Log($"It took {timeAfterLoad - timeBeforeLoad} to generate the sprite cache for {GetInstanceID()}.");

            void GenerateSprite(Sprite sprite, bool genNewOptions, int maxPasses, float maxIntensity)
            {
                if (genNewOptions)
                {
                    SpriteCorruptionTool.GenerateRandomOptionsWeighted(maxPasses, maxIntensity);
                }

                // If sprite is really big, just get another sprite
                while (sprite.rect.size.x > MaxSpriteSize || sprite.rect.size.y > MaxSpriteSize)
                {
                    sprite = GetRandomWeightedSprite(sprite);
                }

                Sprite corruptedSprite = SpriteCorruptionTool.GetCorruptedSprite(sprite);
                _corruptedSprites.Add(corruptedSprite);
                if (CorruptedSpriteResources.ContainsKey(sprite))
                {
                    CorruptedSpriteResources[sprite].CorruptedSprites.Add(corruptedSprite);
                }
                else
                {
                    CorruptedSpriteResources.Add(sprite, new SpriteCorruptionReference(corruptedSprite));
                }
            }

            Sprite GetRandomWeightedSprite(Sprite weight)
            {
                int weightedIndex = SpriteResources.ToList().IndexOf(weight);
                int incrementor = Random.value < 0.5f ? 1 : -1;
                for (; ; weightedIndex += incrementor)
                {
                    if (weightedIndex >= SpriteResources.Length)
                    {
                        weightedIndex = 0;
                    }
                    else if (weightedIndex < 0)
                    {
                        weightedIndex = SpriteResources.Length - 1;
                    }

                    if (Random.value < MaxSpritePoolSize / 10000f)
                    {
                        break;
                    }
                }

                return SpriteResources[weightedIndex];
            }
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private void SetupDefaults()
        {
            SpriteResources = null;            
            CorruptedSpriteResources = null;
        }
    }
}
