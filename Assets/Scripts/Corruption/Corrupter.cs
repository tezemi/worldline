﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Worldline.Utilities;
using Worldline.GameState;
using MEC;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.Corruption
{
    /// <summary>
    /// The Corrupter is the base class for different types of corrupters,
    /// componenets that make use of CorruptionTools to generate corrupted
    /// content, and then use that content within the world. This is the
    /// main implementation of corruption as a mechanic.
    /// </summary>
    [HasDefaultState]
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    public abstract class Corrupter : MonoBehaviour
    {
        public static List<Corrupter> AllCorrupters { get; } = new List<Corrupter>();
        [Tooltip("Used to debug the intensity of corruption.")]
        public float DebugIntensity;
        public float MaxTickTime = MaxPossibleTickTime;
        public float MinTickTime { get; set; } = MinPossibleTickTime;
        public const float MaxPossibleTickTime = 10f;
        public const float MinPossibleTickTime = 0.02f;
        public const float TimeLossPerUncorruptedTick = 2f;
        public const float IntensityChangeToTick = 0.15f;
        private float _debugIntensity;
        private float _intensityLastTick;
        private static float _intensity;

        /// <summary>
        /// The intensity of corruption between zero and one. Higher values 
        /// indicate that more corruption should be used and should be more
        /// intense.
        /// </summary>
        public static float Intensity
        {
            get => _intensity;
            set
            {
                if (value < 0)
                {
                    value = 0;
                }
                else if (value > 1f)
                {
                    value = 1f;
                }

                _intensity = value;
            }
        }

        protected virtual void Awake()
        {
            Intensity = 0f;
            DontDestroyOnLoad(gameObject);
            this.DestroyOnSceneLoad();
            AllCorrupters.Add(this);
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(CallTick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnLevelLoaded()
        {
            Intensity = 0f;
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            if (_debugIntensity != DebugIntensity)
            {
                _debugIntensity = DebugIntensity;
                Intensity = _debugIntensity;
            }

            if (_debugIntensity != Intensity)
            {
                _debugIntensity = Intensity;
                DebugIntensity = _debugIntensity;
            }

            if (Mathf.Abs(Intensity - _intensityLastTick) > IntensityChangeToTick)
            {
                RandomTick();
                _intensityLastTick = Intensity;
            }
        }

        /// <summary>
        /// Gets called completely randomly between MinTickTime and MaxTickTime. 
        /// Can be called at any point. Implementations should cause some sort
        /// of corrupting behavior, and cleanup behavior, depending on the 
        /// intensity of corruption.
        /// </summary>
        public abstract void RandomTick();
        
        /// <summary>
        /// While this Corrupter is enabled, this coroutine will call RandomTick 
        /// between MinTickTime and MaxTickTime.
        /// </summary>
        private IEnumerator<float> CallTick()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Random.Range(MinTickTime, MaxTickTime));
                RandomTick();
                _intensityLastTick = Intensity;
                if (Intensity <= 0f && MaxTickTime - TimeLossPerUncorruptedTick >= TimeLossPerUncorruptedTick)
                {
                    MaxTickTime -= TimeLossPerUncorruptedTick;
                }
                else if (Intensity > 0f)
                {
                    MaxTickTime = MaxPossibleTickTime;
                }
            }
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            Debug.Log("Reset Intensity");
            Intensity = 0f;
            AllCorrupters.Clear();
        }

        public static void ForceUpdate()
        {
            foreach (Corrupter c in AllCorrupters)
            {
                c.RandomTick();
            }
        }
    }
}
