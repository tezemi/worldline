﻿
namespace Worldline.Corruption.SoundEffectCorruption
{
    public enum SoundCorruptionType
    {
        ChangePitch,
        ChangeVolume,
        ChangeSpeed,
        Loop,
        Skip,
        CutOut,
        Crackle,
        ReplaceSound
    }
}
