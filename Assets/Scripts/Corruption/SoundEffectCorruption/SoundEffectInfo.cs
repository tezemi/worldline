﻿using UnityEngine;

namespace Worldline.Corruption.SoundEffectCorruption
{
    public class SoundEffectInfo
    {
        public bool Loop { get; set; }
        public float Pitch { get; set; }
        public float Volume { get; set; }
        public float Time { get; set; }
        public AudioClip AudioClip { get; set; }

        public SoundEffectInfo(bool loop, float pitch, float volume, float time, AudioClip audioClip)
        {
            Pitch = pitch;
            Volume = volume;
            Loop = loop;
            Time = time;
            AudioClip = audioClip;
        }
    }
}
