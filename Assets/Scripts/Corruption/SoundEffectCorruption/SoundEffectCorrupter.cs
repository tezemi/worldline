﻿using System;
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Utilities;
using JetBrains.Annotations;
using MEC;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.SoundEffectCorruption
{
    public class SoundEffectCorrupter : Corrupter
    {
        public static SoundEffectCorrupter MainSoundEffectCorrupter { get; private set; }
        public static SoundCorruptionTool SoundCorruptionTool { get; protected set; }
        public static AudioClip[] AudioClipResources { get; protected set; }
        public HashSet<SoundEffect> IgnoredSoundEffects { get; protected set; } = new HashSet<SoundEffect>();
        public Dictionary<SoundEffect, SoundEffectInfo> CorruptedSoundEffects { get; protected set; } = new Dictionary<SoundEffect, SoundEffectInfo>();
        public AudioMixerGroup CorruptedAudioMixerGroup;
        private readonly HashSet<SoundEffect> _cleanedSoundEffects = new HashSet<SoundEffect>();

        public static readonly Dictionary<SoundCorruptionType, float> SoundCorruptionTypeChances = new Dictionary<SoundCorruptionType, float>
        {
            { SoundCorruptionType.ChangePitch, 0.35f },
            { SoundCorruptionType.ChangeSpeed, 0.5f },
            { SoundCorruptionType.ChangeVolume, 0.25f },
            { SoundCorruptionType.ReplaceSound, 0.1f },
            { SoundCorruptionType.CutOut, 0.65f },
            { SoundCorruptionType.Crackle, 0.65f },
            { SoundCorruptionType.Loop, 0.35f },
            { SoundCorruptionType.Skip, 0.65f }
        };

        protected override void Awake()
        {
            if (MainSoundEffectCorrupter != null && MainSoundEffectCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainSoundEffectCorrupter = this;
            SoundCorruptionTool = new SoundCorruptionTool(CorruptedAudioMixerGroup);
            base.Awake();
        }

        protected override void OnLevelLoaded()
        {
            AudioClipResources = Resources.FindObjectsOfTypeAll<AudioClip>();
            base.OnLevelLoaded();
        }

        protected virtual void LateUpdate()
        {
            if (Intensity > 0.35f)
            {
                for (int i = 0; i < SoundEffect.AllSoundEffects.Count; i++)
                {
                    SoundEffect soundEffect = SoundEffect.AllSoundEffects[i];
                    if (IgnoredSoundEffects.Contains(soundEffect) || CorruptedSoundEffects.ContainsKey(soundEffect)) continue;

                    if (Random.value < Intensity)
                    {
                        foreach (SoundCorruptionType type in Enum.GetValues(typeof(SoundCorruptionType)))
                        {
                            SoundCorruptionTool[type].Enabled = Random.value < SoundCorruptionTypeChances[type];
                            SoundCorruptionTool[type].Index = Random.Range(0, AudioClipResources.Length);
                            SoundCorruptionTool[type].Intensity = Intensity;
                        }

                        if (SoundCorruptionTool[SoundCorruptionType.ChangeSpeed].Enabled)
                        {
                            SoundCorruptionTool[SoundCorruptionType.ChangePitch].Enabled = false;
                        }

                        if (Intensity < 0.5f)
                        {
                            SoundCorruptionTool[SoundCorruptionType.ReplaceSound].Enabled = false;
                        }

                        SoundCorruptionTool.Intensity = Intensity;
                        SoundCorruptionTool.AudioClipResources = AudioClipResources;

                        var sfxInfo = new SoundEffectInfo
                        (
                            soundEffect.AudioSource.loop, 
                            soundEffect.AudioSource.pitch, 
                            soundEffect.AudioSource.volume, soundEffect.AudioSource.time, 
                            soundEffect.AudioSource.clip
                        );

                        SoundCorruptionTool.CorruptSound(soundEffect, true);

                        CorruptedSoundEffects.Add(soundEffect, sfxInfo);
                    }
                    else
                    {
                        IgnoredSoundEffects.Add(soundEffect);
                    }
                }
            }
        }

        public override void RandomTick()
        {
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {

        }

        public void Clean(float chance)
        {
            foreach (SoundEffect soundEffect in CorruptedSoundEffects.Keys)
            {
                if (soundEffect != null && soundEffect.AudioSource != null && soundEffect.IsPlaying && Random.value < chance)
                {
                    Timing.KillCoroutines(soundEffect.AudioSource.GetInstanceID().ToString());
                    soundEffect.AudioSource.pitch = CorruptedSoundEffects[soundEffect].Pitch;
                    soundEffect.AudioSource.volume = CorruptedSoundEffects[soundEffect].Volume;
                    soundEffect.AudioSource.loop = CorruptedSoundEffects[soundEffect].Loop;
                    soundEffect.AudioSource.clip = CorruptedSoundEffects[soundEffect].AudioClip;
                }

                _cleanedSoundEffects.Add(soundEffect);
            }

            foreach (SoundEffect sfx in _cleanedSoundEffects)
            {
                CorruptedSoundEffects.Remove(sfx);
            }

            _cleanedSoundEffects.Clear();
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            MainSoundEffectCorrupter = null;
            SoundCorruptionTool = null;
        }
    }
}
