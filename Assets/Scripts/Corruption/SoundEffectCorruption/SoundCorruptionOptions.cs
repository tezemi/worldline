﻿using System;

namespace Worldline.Corruption.SoundEffectCorruption
{
    [Serializable]
    public class SoundCorruptionOptions
    {
        public bool Enabled;
        public int Index;
        public float Intensity;
    }
}
