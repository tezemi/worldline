﻿using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.Audio;

namespace Worldline.Corruption.SoundEffectCorruption
{
    public class SoundCorruptionDebugger : MonoBehaviour
    {
        public AudioClip TestSound;
        public AudioMixerGroup CorruptedAudioMixerGroup;
        public List<SoundCorruptionType> CorruptionTypes;
        public List<SoundCorruptionOptions> SoundCorruptionOptions;
        public SoundCorruptionTool SoundCorruptionTool { get; set; }

        protected virtual void Awake()
        {
            SoundCorruptionTool = new SoundCorruptionTool(CorruptedAudioMixerGroup);
        }

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                int index = 0;
                foreach (SoundCorruptionType type in CorruptionTypes)
                {
                    SoundCorruptionTool[type] = SoundCorruptionOptions[index++];
                }

                if (TestSound != null)
                {
                    var sound = SoundEffect.PlaySound(TestSound);
                    SoundCorruptionTool.CorruptSound(sound, true);
                }
                else
                {
                    var soundResources = Resources.FindObjectsOfTypeAll<AudioClip>();
                    var audioClip = soundResources[Random.Range(0, soundResources.Length)];
                    var sound = SoundEffect.CreateSound(audioClip);
                    SoundCorruptionTool.CorruptSound(sound, true);
                }
            }
        }
    }
}
