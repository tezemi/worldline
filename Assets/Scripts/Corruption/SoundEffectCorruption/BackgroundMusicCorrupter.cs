﻿using System;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.Audio;
using MEC;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.SoundEffectCorruption
{
    public class BackgroundMusicCorrupter : Corrupter
    {
        public static BackgroundMusicCorrupter MainBackgroundMusicCorrupter { get; private set; }
        public static SoundCorruptionTool SoundCorruptionTool { get; protected set; }
        public AudioMixerGroup CorruptedAudioMixerGroup;
        public float[] TrackTimes;
        public Dictionary<AudioSource, SoundEffectInfo> CorruptedTracks { get; protected set; } = new Dictionary<AudioSource, SoundEffectInfo>();
        private readonly List<AudioSource> _cleanedTracks = new List<AudioSource>();

        protected override void Awake()
        {
            if (MainBackgroundMusicCorrupter != null && MainBackgroundMusicCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainBackgroundMusicCorrupter = this;
            SoundCorruptionTool = new SoundCorruptionTool(CorruptedAudioMixerGroup);
            base.Awake();
        }

        protected override void Update()
        {
            base.Update();
            if (TrackTimes == null || TrackTimes.Length != BackgroundMusicOperator.Main.ExtraAudioSources.Count)
            {
                TrackTimes = new float[BackgroundMusicOperator.Main.ExtraAudioSources.Count];
            }
            
            for (int i = 0; i < BackgroundMusicOperator.Main.ExtraAudioSources.Count; i++)
            {
                TrackTimes[i] = BackgroundMusicOperator.Main.ExtraAudioSources[i].time;
            }
        }

        public override void RandomTick()
        {
            Corrupt();
            Clean(1f - Intensity);
        }

        public void Corrupt()
        {
            if (Intensity < 0.5f) return;

            foreach (SoundCorruptionType type in Enum.GetValues(typeof(SoundCorruptionType)))
            {
                SoundCorruptionTool[type].Enabled = Random.value < SoundEffectCorrupter.SoundCorruptionTypeChances[type];
                SoundCorruptionTool[type].Index = Random.Range(0, SoundEffectCorrupter.AudioClipResources.Length);
                SoundCorruptionTool[type].Intensity = Intensity;
            }

            if (SoundCorruptionTool[SoundCorruptionType.ChangeSpeed].Enabled)
            {
                SoundCorruptionTool[SoundCorruptionType.ChangePitch].Enabled = false;
            }

            if (Intensity < 0.5f)
            {
                SoundCorruptionTool[SoundCorruptionType.ReplaceSound].Enabled = false;
            }

            SoundCorruptionTool.Intensity = Intensity;
            SoundCorruptionTool.AudioClipResources = SoundEffectCorrupter.AudioClipResources;

            foreach (AudioSource audioSource in BackgroundMusicOperator.Main.ExtraAudioSources)
            {
                if (!audioSource.enabled || audioSource.clip == null) continue;

                if (!CorruptedTracks.ContainsKey(audioSource) && Random.value < Intensity * 1.25f)
                {
                    var sfxInfo = new SoundEffectInfo
                    (
                        audioSource.loop,
                        audioSource.pitch,
                        audioSource.volume,
                        audioSource.time,
                        audioSource.clip
                    );

                    CorruptedTracks.Add(audioSource, sfxInfo);
                    SoundCorruptionTool.CorruptSound(audioSource, false);
                }
            }
        }

        public void Clean(float intensity)
        {
            foreach (AudioSource audioSource in CorruptedTracks.Keys)
            {
                if (Random.value < intensity)
                {
                    audioSource.mute = false;
                    audioSource.loop = CorruptedTracks[audioSource].Loop;
                    audioSource.pitch = CorruptedTracks[audioSource].Pitch;
                    audioSource.volume = CorruptedTracks[audioSource].Volume;
                    if (audioSource.clip != CorruptedTracks[audioSource].AudioClip)
                    {
                        audioSource.clip = CorruptedTracks[audioSource].AudioClip;
                    }

                    audioSource.outputAudioMixerGroup = GlobalStorage.Get().BackgroundMusicAudioMixerGroup;
                    audioSource.time = BackgroundMusicOperator.Main.ExtraAudioSources[0].time;
                    Timing.KillCoroutines(audioSource.GetInstanceID().ToString());
                    _cleanedTracks.Add(audioSource);
                }
            }

            foreach (AudioSource source in _cleanedTracks)
            {
                CorruptedTracks.Remove(source);
            }

            _cleanedTracks.Clear();

            foreach (AudioSource audioSource in BackgroundMusicOperator.Main.ExtraAudioSources)
            {
                if (Math.Abs(audioSource.time - BackgroundMusicOperator.Main.ExtraAudioSources[0].time) > 0.05f)
                {
                    audioSource.time = BackgroundMusicOperator.Main.ExtraAudioSources[0].time;
                }

                if (!audioSource.isPlaying)
                {
                    audioSource.Play();
                }
            }
        }
    }
}
