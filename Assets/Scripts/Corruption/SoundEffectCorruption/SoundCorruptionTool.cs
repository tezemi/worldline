﻿using System;
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.SoundEffectCorruption
{
    /// <summary>
    /// A corrupter used to player versions of sounds that are corrupted. 
    /// Modifies pitch, volume, speed, looping, replaces the clip, and
    /// applies a skipping effect.
    /// </summary>
    public class SoundCorruptionTool : CorruptionTool<SoundCorruptionType, SoundCorruptionOptions>
    {
        public float Intensity { get; set; } = 0.5f;
        public AudioMixerGroup CorruptedAudioMixerGroup { get; set; }
        public AudioClip[] AudioClipResources { get; set; }
        public const float MaxVolume = 1.5f;

        public SoundCorruptionTool(AudioMixerGroup corruptedAudioMixerGroup)
        {
            CorruptedAudioMixerGroup = corruptedAudioMixerGroup;
        }

        public void CorruptSound(AudioSource sound, bool disableAfter)
        {
            if (sound == null || sound.clip == null) return;

            float originalLength = sound.clip.length;
            foreach (SoundCorruptionType type in (SoundCorruptionType[])Enum.GetValues(typeof(SoundCorruptionType)))
            {
                if (!Options[type].Enabled) continue;

                var options = Options[type];
                switch (type)
                {
                    case SoundCorruptionType.ChangePitch:
                        sound.outputAudioMixerGroup = CorruptedAudioMixerGroup;
                        GlobalStorage.Get().SoundEffectsAudioMixerGroup.audioMixer.GetFloat("volume", out float volume);
                        CorruptedAudioMixerGroup.audioMixer.SetFloat("volume", volume);
                        CorruptedAudioMixerGroup.audioMixer.SetFloat("pitch", 1f + Random.Range(0f, options.Intensity) * (Random.value < 0.5f ? 1f : -1f));
                        break;
                    case SoundCorruptionType.ChangeVolume:
                        sound.volume = 1f + Random.Range(0f, options.Intensity) * (Random.value < 0.5f ? 1f : -1f);
                        if (sound.volume > MaxVolume)
                        {
                            sound.volume = MaxVolume;
                        }

                        break;
                    case SoundCorruptionType.ChangeSpeed:
                        sound.pitch = 1f + Random.Range(0f, options.Intensity) * (Random.value < 0.5f ? 1f : -1f);
                        break;
                    case SoundCorruptionType.Loop:
                        sound.loop = true;
                        break;
                    case SoundCorruptionType.ReplaceSound:
                        if (sound.isActiveAndEnabled)
                        {
                            var audio = Resources.FindObjectsOfTypeAll<AudioClip>();        // TODO: Get rid of this
                            sound.clip = audio[options.Index];
                            sound.Play();
                        }

                        break;
                }
            }

            if (Options[SoundCorruptionType.Skip].Enabled)
            {
                Timing.RunCoroutine(PerformSkippingEffect(), Segment.FixedUpdate, sound.GetInstanceID().ToString());

                IEnumerator<float> PerformSkippingEffect()
                {
                    int count = 0;
                    int times = (int)(Options[SoundCorruptionType.Skip].Intensity * 10f);
                    while (count < times)
                    {
                        yield return Timing.WaitForSeconds
                        (
                            Random.Range(sound.time, sound.clip.length - 0.02f)
                        );

                        if (sound != null && sound != null)
                        {
                            sound.time = Random.Range(0f, sound.clip.length);
                            if (!sound.isPlaying)
                            {
                                sound.Play();
                            }
                        }
                        else
                        {
                            yield break;
                        }

                        count++;
                    }
                }
            }

            if (Options[SoundCorruptionType.CutOut].Enabled)
            {
                Timing.RunCoroutine(PerformCutOut(), Segment.FixedUpdate, sound.GetInstanceID().ToString());

                IEnumerator<float> PerformCutOut()
                {
                    while (sound != null && sound != null && sound.isPlaying)
                    {
                        if (Random.value < Intensity)
                        {
                            sound.mute = !sound.mute;
                        }

                        yield return Timing.WaitForSeconds(Random.Range(0.05f, 0.1f));
                    }
                }
            }

            if (Options[SoundCorruptionType.Crackle].Enabled)
            {
                Timing.RunCoroutine(PerformCrackle(), Segment.FixedUpdate, sound.GetInstanceID().ToString());

                IEnumerator<float> PerformCrackle()
                {
                    while (sound != null && sound != null && sound.isPlaying)
                    {
                        sound.mute = !sound.mute;

                        yield return Timing.WaitForOneFrame;
                    }
                }
            }

            if (disableAfter)
            {
                Timing.RunCoroutine(DestroyAfterWhile(), Segment.FixedUpdate);
            }

            IEnumerator<float> DestroyAfterWhile()
            {
                yield return Timing.WaitForSeconds(Random.Range(originalLength, originalLength * (Intensity + 1f)));
                if (sound != null && sound != null)
                {
                    sound.Stop();
                }
            }
        }

        public void CorruptSound(SoundEffect sound, bool disableAfter)
        {
            CorruptSound(sound.AudioSource, disableAfter);
        }
    }
}
