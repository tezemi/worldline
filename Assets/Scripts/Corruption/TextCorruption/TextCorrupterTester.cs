﻿using System.Collections.Generic;
using Worldline.GUI;
using UnityEngine;

namespace Worldline.Corruption.TextCorruption
{
    public class TextCorrupterTester : MonoBehaviour
    {
        [Tooltip("Different types of corruption to apply.")]
        public List<TextCorruptionType> CorruptionTypes;
        [Tooltip("Options corresponding to each of the above types.")]
        public List<TextCorruptionOptions> SpriteCorruptionOptions;
        public TextCorruptionTool TextCorruptionTool { get; set; } = new TextCorruptionTool();
        public string OriginalText { get; protected set; }
        protected IRendersText Text { get; set; }

        protected virtual void Awake()
        {
            Text = GetComponent<IRendersText>();
            OriginalText = Text.Text;
        }

        protected virtual void UpdateTextCorrupterOptions()
        {
            if (CorruptionTypes.Count != SpriteCorruptionOptions.Count)
            {
                Debug.LogWarning("Text corruption types list must be the same size as the options list.", gameObject);
                return;
            }

            TextCorruptionTool.ResetCorrupter();
            int index = 0;
            foreach (TextCorruptionType type in CorruptionTypes)
            {
                TextCorruptionTool[type] = SpriteCorruptionOptions[index++];
            }
        }

        public virtual void ApplyCorruptedText()
        {
            UpdateTextCorrupterOptions();
            TextCorruptionTool.ApplyCorruptedText(Text);
        }

        public virtual void ResetText()
        {
            Text.Text = OriginalText;
        }
    }
}
