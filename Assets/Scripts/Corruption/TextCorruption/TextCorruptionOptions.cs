﻿using System;

namespace Worldline.Corruption.TextCorruption
{
    [Serializable]
    public class TextCorruptionOptions
    {
        public bool Enabled;
        public int Priority;
        public int Passes = 1;
        public float Intensity;
    }
}
