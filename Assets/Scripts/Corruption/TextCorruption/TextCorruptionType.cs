﻿
namespace Worldline.Corruption.TextCorruption
{
    public enum TextCorruptionType
    {
        /// <summary>
        /// Replaces a random amount of characters with either other characters 
        /// or random graphics depending on the intensity.
        /// </summary>
        ReplaceRandomCharacters,
        /// <summary>
        /// Replaces all instances of characters with either other characters 
        /// or random graphics depending on the intensity.
        /// </summary>
        ReplaceAllOfCharacter,
        /// <summary>
        /// Replaces certain parts of all of the string with some other dialog.
        /// </summary>
        ReplaceMessage,
        /// <summary>
        /// Replaces characters with corrupted sprites. If the intensity is high, 
        /// each character will be associated with a different corrupted sprite.
        /// </summary>
        ReplaceCharactersWithGraphics
    }
}
