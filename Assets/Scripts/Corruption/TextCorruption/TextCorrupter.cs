﻿using System.Collections.Generic;
using Worldline.GUI;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.GUI.Printing;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.TextCorruption
{
    public class TextCorrupter : Corrupter
    {
        public static TextCorrupter MainTextCorrupter { get; private set; }
        public static TextCorruptionTool TextCorruptionTool { get; protected set; } = new TextCorruptionTool();
        public static List<IRendersText> TextInWorld { get; protected set; }
        public static Dictionary<IRendersText, (string, string)> CorruptedTextRenderers { get; protected set; } = new Dictionary<IRendersText, (string, string)>();
        private readonly List<IRendersText> _cleanedText = new List<IRendersText>();
        
        protected override void Awake()
        {
            if (MainTextCorrupter != null && MainTextCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainTextCorrupter = this;
            base.Awake();
        }

        protected override void OnLevelLoaded()
        {
            // Can't pass IRendersText into FindObjectsOfType, so we have to do
            // it like this
            TextInWorld = new List<IRendersText>();
            TextInWorld.AddRange(FindObjectsOfType<TextRenderer>());
            TextInWorld.AddRange(FindObjectsOfType<AdvancedText>());
            base.OnLevelLoaded();
        }

        protected virtual void LateUpdate()
        {
            foreach (IRendersText text in CorruptedTextRenderers.Keys)
            {
                if (text.Text == CorruptedTextRenderers[text].Item1)
                {
                    text.Text = CorruptedTextRenderers[text].Item2;
                    if (text.FindComponent<TextShadow>() != null)
                    {
                        text.FindComponent<TextShadow>().ShadowText.text = text.Text;
                    }
                }
            }
        }

        public override void RandomTick()
        {
            TextInWorld = new List<IRendersText>();
            TextInWorld.AddRange(FindObjectsOfType<TextRenderer>());
            TextInWorld.AddRange(FindObjectsOfType<AdvancedText>());
            TextCorruptionTool.GenerateRandomOptions(1);
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {
            foreach (IRendersText text in TextInWorld)
            {
                if (Random.value < chance)
                {
                    string originalText = CorruptedTextRenderers.ContainsKey(text) ? CorruptedTextRenderers[text].Item1 : text.Text;
                    TextCorruptionTool.ApplyCorruptedText(text);
                    string corruptedText = text.Text;
                    if (!CorruptedTextRenderers.ContainsKey(text))
                    {
                        CorruptedTextRenderers.Add(text, (originalText, corruptedText));    
                    }
                    else
                    {
                        var tuple = CorruptedTextRenderers[text];
                        CorruptedTextRenderers[text] = (tuple.Item1, corruptedText);
                    }
                }
            }
        }

        public void Clean(float chance)
        {
            for (int i = 0; i < TextCorruptionTool.CreatedGameObjects.Count; i++)
            {
                GameObject obj = TextCorruptionTool.CreatedGameObjects[i];
                if (Random.value < chance)
                {
                    obj.SetActive(false);
                    Pool.Add(obj, TextCorruptionTool.PoolName, false);
                    TextCorruptionTool.CreatedGameObjects.Remove(obj);
                }
            }

            foreach (IRendersText text in CorruptedTextRenderers.Keys)
            {
                try
                {
                    if (Random.value < chance)
                    {
                        text.Text = CorruptedTextRenderers[text].Item1;
                        _cleanedText.Add(text);
                    }
                }
                catch (MissingReferenceException)
                {
                    _cleanedText.Add(text);
                }
            }

            foreach (IRendersText text in _cleanedText)
            {
                CorruptedTextRenderers.Remove(text);
            }

            _cleanedText.Clear();
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            MainTextCorrupter = null;
            TextInWorld = null;
            CorruptedTextRenderers = new Dictionary<IRendersText, (string, string)>();
        }
    }
}
