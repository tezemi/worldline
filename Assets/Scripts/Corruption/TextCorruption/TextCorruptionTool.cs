﻿using System;
using System.Linq;
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.Utilities;
using Worldline.GUI.Printing;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.TextCorruption
{
    public class TextCorruptionTool : CorruptionTool<TextCorruptionType, TextCorruptionOptions>
    {
        public List<GameObject> CreatedGameObjects { get; protected set; } = new List<GameObject>();
        public const int AsciiStart = 32;
        public const int AsciiEnd = 126;
        public const string PoolName = "TextCorruptionToolGameObjectPool";

        public static readonly Dictionary<TextCorruptionType, float> TypeChances = new Dictionary<TextCorruptionType, float>
        {
            { TextCorruptionType.ReplaceAllOfCharacter, 0.5f },
            { TextCorruptionType.ReplaceRandomCharacters, 0.1f },
            { TextCorruptionType.ReplaceCharactersWithGraphics, 0.25f },
            { TextCorruptionType.ReplaceMessage, 0.75f }
        };

        public void ApplyCorruptedText(IRendersText text)
        {
            var originalStringLength = text.Text.Length;
            var lineBreakPositions = new List<int>();
            for (int i = 0; i < text.Text.Length; i++)
            {
                if (text.Text[i] == '\n')
                {
                    lineBreakPositions.Add(i);
                }
            }

            var sortedOptions = Options.OrderByDescending(kvp => kvp.Value.Priority).ToDictionary(x => x.Key, x => x.Value);
            foreach (TextCorruptionType type in sortedOptions.Keys)
            {
                TextCorruptionOptions options = Options[type];
                if (!options.Enabled) continue;

                for (int pass = 0; pass < options.Passes; pass++)
                {
                    switch (type)
                    {
                        case TextCorruptionType.ReplaceRandomCharacters:
                            for (int i = 0; i < text.Text.Length; i++)
                            {
                                if (Random.value < options.Intensity)
                                {
                                    text.Text = text.Text.Remove(i, 1);
                                    text.Text = text.Text.Insert(i, ((char) Random.Range(AsciiStart, AsciiEnd)).ToString());
                                }
                            }

                            break;
                        case TextCorruptionType.ReplaceAllOfCharacter:
                            for (int i = 0; i < text.Text.Length; i++)
                            {
                                if (Random.value < options.Intensity)
                                {
                                    text.Text = text.Text.Replace(text.Text[i],(char) Random.Range(AsciiStart, AsciiEnd));
                                }
                            }

                            break;
                        case TextCorruptionType.ReplaceMessage:
                            string newMessage = SerializedStrings.GetAny();
                            int substringIndex = Random.Range(0, newMessage.Length);
                            int substringLength = Random.Range(1, newMessage.Length - substringIndex);
                            string randomSubstring = newMessage.Substring
                            (
                                substringIndex,
                                substringLength
                            );
                            
                            int indexInOriginal = Random.Range(0, text.Text.Length);
                            string final = text.Text;
                            int k = 0;
                            for (int j = indexInOriginal; j < indexInOriginal + substringLength; j++)
                            {
                                if (j < final.Length)
                                {
                                    final = final.Remove(j, 1);
                                    final = final.Insert(j, randomSubstring[k++].ToString());
                                }
                                else
                                {
                                    final += randomSubstring[k++].ToString();
                                }
                            }

                            text.Text = final;
                            break;
                        case TextCorruptionType.ReplaceCharactersWithGraphics:
                            if (text is AdvancedText advancedText && !string.IsNullOrEmpty(text.Text))
                            {
                                const float intensityNeededToAssociate = 0.5f;
                                var letterToGraphic = new Dictionary<char, Sprite>();
                                var sprites = GraphicsCorrupter.SpriteResources;
                                
                                getCorruptedSprite:
                                var sprite = sprites[Random.Range(0, sprites.Length)];
                                if (!GraphicsCorrupter.CorruptedSpriteResources.ContainsKey(sprite))
                                {
                                    goto getCorruptedSprite;
                                }

                                Sprite corruptedSprite = GraphicsCorrupter.CorruptedSpriteResources[sprite].SpriteAtIndex;
                                if (options.Intensity > 0.5f) // Associate different chars with different graphics
                                {
                                    foreach (char c in advancedText.Message)
                                    {
                                        if (!letterToGraphic.ContainsKey(c))
                                        {
                                            letterToGraphic.Add(c, corruptedSprite);
                                        }
                                    }
                                }
                                
                                bool stretchSize = Random.value < intensityNeededToAssociate;
                                foreach (Text t in advancedText.Characters)
                                {
                                    if (string.IsNullOrEmpty(t.text) || Random.value > options.Intensity) continue;

                                    GameObject imageGameObject;
                                    if (Pool.Has(PoolName))
                                    {
                                        imageGameObject = Pool.Get(PoolName);
                                    }
                                    else
                                    {
                                        imageGameObject = new GameObject("Image for Text", typeof(Image));
                                    }
                                    
                                    Image image = imageGameObject.GetComponent<Image>();
                                    image.rectTransform.SetParent(t.rectTransform);
                                    image.rectTransform.sizeDelta = t.rectTransform.sizeDelta + (stretchSize ? new Vector2(0f, t.rectTransform.sizeDelta.y) : Vector2.zero);
                                    image.rectTransform.anchoredPosition = t.rectTransform.anchoredPosition + new Vector2(0f, t.rectTransform.sizeDelta.y / 2);
                                    if (options.Intensity > intensityNeededToAssociate & letterToGraphic.ContainsKey(t.text[0]))
                                    {
                                        image.sprite = letterToGraphic[t.text[0]];
                                    }
                                    else
                                    {
                                        image.sprite = corruptedSprite;
                                    }
                                    
                                    image.type = Image.Type.Tiled;
                                    imageGameObject.SetActive(true);
                                    CreatedGameObjects.Add(imageGameObject);
                                }
                            }

                            break;
                    }
                }
            }

            if (text.FindComponent<TextPrinter>() != null)
            {
                int debugCount = 0;
                while (text.Text.Length < originalStringLength && debugCount++ < 100)
                {
                    text.Text += " ";
                }

                debugCount = 0;
                while (text.Text.Length > originalStringLength && debugCount++ < 100)
                {
                    text.Text = text.Text.Remove(text.Text.Length - 1);
                }
            }

            foreach (int i in lineBreakPositions)
            {
                text.Text = text.Text.Remove(i, 1);
                text.Text = text.Text.Insert(i, "\n");
            }
        }

        public virtual void GenerateRandomOptions(int maxPasses, int? seed = null)
        {
            if (seed != null)
            {
                Random.InitState(seed.Value);
            }

            var corruptionTypes = Enum.GetValues(typeof(TextCorruptionType));
            foreach (TextCorruptionType type in corruptionTypes)
            {
                this[type] = new TextCorruptionOptions
                {
                    Enabled = Random.value < TypeChances[type],
                    Intensity = Random.value,
                    Passes = Random.Range(1, maxPasses + 1),
                    Priority = Random.Range(0, corruptionTypes.Length)
                };
            }
        }
    }
}
