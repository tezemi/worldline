﻿
namespace Worldline.Corruption.GameObjectCorruption
{
    public enum PositionCorruptionType
    {
        Move,
        Slide,
        JumpBack,
        Flicker
    }
}
