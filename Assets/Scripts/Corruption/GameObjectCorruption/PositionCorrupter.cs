﻿using System;
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Combat.Projectiles;
using MEC;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.EventSystems;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.GameObjectCorruption
{
    public class PositionCorrupter : Corrupter
    {
        public Dictionary<GameObject, PositionInfo> CorruptedPositions { get; protected set; } = new Dictionary<GameObject, PositionInfo>();
        private readonly List<GameObject> _keys = new List<GameObject>();
        private readonly List<GameObject> _cleanedGameObjects = new List<GameObject>();

        protected virtual void FixedUpdate()
        {
            foreach (GameObject obj in _keys)
            {
                CorruptedPositions[obj] = new PositionInfo
                {
                    OriginalPosition = CorruptedPositions[obj].OriginalPosition,
                    CorruptedPosition = CorruptedPositions[obj].CorruptedPosition,
                    PositionLastFrame = transform.position
                };
            }
        }

        public override void RandomTick()
        {
            // essence, particles, non-essential NPCs, UI
            // Essence wells, tilemaps, bullets
            // combat-related npcs, save points, doors
            // the player, collisions
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }
       
        public void Corrupt(float chance)
        {
            foreach (GameObject obj in FindObjectsOfType<GameObject>())
            {
                if (CorruptedPositions.ContainsKey(obj)) continue;

                if (Random.value < Intensity - 0.65f)
                {
                    if (Intensity > 0.45f &&
                    (obj.GetComponent<UIBehaviour>() != null ||
                    obj.GetComponent<ParticleEffect>() != null ||
                    obj.GetComponent<EssenceDroplet>() != null))
                    {
                        CorruptPosition(obj);
                    }
                    else if (Intensity > 0.65f &&
                    (obj.GetComponent<EssenceWell>() != null ||
                    obj.GetComponent<Projectile>() != null ||
                    obj.GetComponent<Tilemap>() != null &&
                    LevelController.Main.PhysicsTilemap != obj.GetComponent<Tilemap>() && 
                    obj.GetComponent<TilemapSlider>() == null))
                    {
                        CorruptPosition(obj);
                    }
                    else if (Intensity > 0.75f &&
                    (obj.GetComponent<EssenceWell>() != null ||
                    obj.GetComponent<Projectile>() != null ||
                    obj.GetComponent<Combatant>() != null &&
                    obj.GetComponent<PlayerOperator>() == null))
                    {
                        CorruptPosition(obj);
                    }
                    else if (Intensity >= 1f &&
                    (obj.GetComponent<Collider2D>() != null &&
                    obj.CompareTag("Surface") ||
                    obj.GetComponent<PlayerOperator>() != null &&
                    obj.GetComponent<PlayerOperator>() == null))
                    {
                        CorruptPosition(obj);
                    }
                }
            }

            void CorruptPosition(GameObject obj)
            {
                CorruptedPositions.Add(obj, new PositionInfo { OriginalPosition = obj.transform.position, PositionLastFrame = obj.transform.position });
                _keys.Add(obj);
                var types = (PositionCorruptionType[])Enum.GetValues(typeof(PositionCorruptionType));
                var type = types[Random.Range(0, types.Length)];
                type = PositionCorruptionType.Move;
                float movementScale = obj.GetComponentInParent<MainCanvasController>() != null ? Random.Range(10f, 100f) : Random.Range(1f, 10f);
                switch (type)
                {
                    case PositionCorruptionType.Move:
                    {
                        obj.transform.position += new Vector3((Random.value > 0.5f ? -1f : 1f) * Intensity * movementScale, (Random.value > 0.5f ? -1f : 1f) * Intensity * movementScale, 0f);
                        CorruptedPositions[obj] = new PositionInfo
                        {
                            OriginalPosition = CorruptedPositions[obj].OriginalPosition,
                            CorruptedPosition = obj.transform.position,
                            PositionLastFrame = CorruptedPositions[obj].PositionLastFrame
                        };

                        break;
                    }
                    case PositionCorruptionType.Slide:
                    {
                        bool x = Random.value > 0.5f;
                        bool y = Random.value > 0.5f;
                        int ticks = 0;
                        int maxTicks = Random.Range(10, (int)(Intensity * 200));
                        float speed = Random.Range(0f, Intensity / 100f);
                        Timing.RunCoroutine(Slide(), Segment.FixedUpdate, $"PositionCorrupter{obj.GetInstanceID()}");

                        IEnumerator<float> Slide()
                        {
                            while (isActiveAndEnabled && ticks++ < maxTicks)
                            {
                                obj.transform.position += new Vector3(x ? speed : 0f, y ? speed : 0f, 0f);
                                yield return Timing.WaitForOneFrame;
                            }
                        }

                        break;
                    }
                    case PositionCorruptionType.JumpBack:
                    {
                        int ticks = 0;
                        int maxTicks = Random.Range(10, (int)(Intensity * 200));
                        float jumpTime = Random.Range(0.02f, 2f);
                        Vector3 startPoint = obj.transform.position;
                        Timing.RunCoroutine(Jump(), Segment.FixedUpdate, $"PositionCorrupter{obj.GetInstanceID()}");

                        IEnumerator<float> Jump()
                        {
                            while (isActiveAndEnabled && ticks++ < maxTicks)
                            {
                                obj.transform.position = startPoint;
                                yield return Timing.WaitForSeconds(jumpTime);
                            }
                        }

                        break;
                    }
                    case PositionCorruptionType.Flicker:
                    {
                        bool jumpToEnd = true;
                        int ticks = 0;
                        int maxTicks = Random.Range(10, (int)(Intensity * 200));
                        float jumpTime = Random.Range(0.02f, 2f);
                        Vector3 startPoint = obj.transform.position;
                        Vector3 endPoint = obj.transform.position += new Vector3((Random.value > 0.5f ? -1f : 1f) * Intensity * 10f, (Random.value > 0.5f ? -1f : 1f) * Intensity * 10f, 0f);
                        Timing.RunCoroutine(Flicker(), Segment.FixedUpdate, $"PositionCorrupter{obj.GetInstanceID()}");

                        IEnumerator<float> Flicker()
                        {
                            while (isActiveAndEnabled && ticks++ < maxTicks)
                            {
                                if (jumpToEnd)
                                {
                                    obj.transform.position = endPoint;
                                }
                                else
                                {
                                    obj.transform.position = startPoint;
                                }

                                yield return Timing.WaitForSeconds(jumpTime);
                            }
                        }

                        break;
                    }
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void Clean(float chance)
        {
            foreach (GameObject obj in CorruptedPositions.Keys)
            {
                try
                {
                    if (Random.value < chance)
                    {
                        Timing.KillCoroutines($"PositionCorrupter{obj.GetInstanceID()}");
                        obj.transform.position = CorruptedPositions[obj].OriginalPosition + (obj.transform.position - CorruptedPositions[obj].CorruptedPosition);
                        _cleanedGameObjects.Add(obj);
                    }
                }
                catch (MissingReferenceException)
                {
                    _cleanedGameObjects.Add(obj);
                }
            }
            
            foreach (GameObject obj in _cleanedGameObjects)
            {
                CorruptedPositions.Remove(obj);
                _keys.Remove(obj);
            }

            _cleanedGameObjects.Clear();
        }
    }
}
