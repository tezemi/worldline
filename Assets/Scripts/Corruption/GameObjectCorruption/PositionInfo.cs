﻿using UnityEngine;

namespace Worldline.Corruption.GameObjectCorruption
{
    public struct PositionInfo
    {
        public Vector3 OriginalPosition;
        public Vector3 CorruptedPosition;
        public Vector3 PositionLastFrame;
    }
}
