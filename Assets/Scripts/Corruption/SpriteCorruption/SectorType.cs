﻿
namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// An enum used to specify a type of sector generation. When sprites 
    /// get corrupted, sectors of the sprite are picked to be corrupted in
    /// order to simulate 16-bit era corruption. This enumeration defines
    /// different types of patterns to use when choosing sectors.
    /// </summary>
    public enum SectorType
    {
        /// <summary>
        /// Sectors are picked completely randomly.
        /// </summary>
        Random,
        /// <summary>
        /// Sectors are more likely to be in large in groups.
        /// </summary>
        Clustered,
        /// <summary>
        /// Only sectors on the left or right side of the sprite will be picked.
        /// </summary>
        Horizontal,
        /// <summary>
        /// Only sectors on the top or bottom half of the sprite will be picked.
        /// </summary>
        Vertical,
        /// <summary>
        /// Sectors on the inside of the sprite are more likely to be picked.
        /// </summary>
        InnerWeight,
        /// <summary>
        /// Sectors on the outside of the sprite are more likely to be picked.
        /// </summary>
        OuterWeight,
        /// <summary>
        /// Sectors are picked by randomly picking columns of sectors.
        /// </summary>
        Columns,
        /// <summary>
        /// Sectors are picked by randomly picking rows of sectors.
        /// </summary>
        Rows,
        /// <summary>
        /// All sectors will be used.
        /// </summary>
        All,
        /// <summary>
        /// No sectors will be used.
        /// </summary>
        None
    }
}
