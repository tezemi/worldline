﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// A enum that represents a different type of visual corruption on a 
    /// sprite.
    /// </summary>
    public enum SpriteCorruptionType
    {
        /// <summary>
        /// Mirrors the sprite horizontally.
        /// </summary>
        FlipXSectors,
        /// <summary>
        /// Mirrors the sprite vertically.
        /// </summary>
        FlipYSectors,
        /// <summary>
        /// Inverts the colors of the sprite.
        /// </summary>
        Invert,
        /// <summary>
        /// Replaces colors on the sprite with other colors.
        /// </summary>
        PaletteSwap,
        /// <summary>
        /// Adds random pixels to the sprite.
        /// </summary>
        ClownVomit,
        /// <summary>
        /// Replaces colors with black or white.
        /// </summary>
        BitColors,
        /// <summary>
        /// Moves sectors on the sprite around.
        /// </summary>
        MoveSectors,
        /// <summary>
        /// Replaces sectors on the sprite with sectors from other sprites.
        /// </summary>
        ReplaceSectors,
        /// <summary>
        /// Simply replaces the specified sectors from the sprite with transparent 
        /// pixels.
        /// </summary>
        RemoveSectors,
        /// <summary>
        /// Replaces sectors on a sprite by replacing them with offset pixels, 
        /// resulting in a tearing effect.
        /// </summary>
        Tearing,
        /// <summary>
        /// Selects a random sector and repeats it in all other specified sectors.
        /// </summary>
        RepeatSectors,
        /// <summary>
        /// Replaces specified sectors with sectors from another sprite at a randomly 
        /// specified offset.
        /// </summary>
        OffsetSectors,
        /// <summary>
        /// Rotates the pixels inside the sectors 90 degrees.
        /// </summary>
        RotateSectors
    }
}
