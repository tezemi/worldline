﻿using JetBrains.Annotations;
using Worldline.Visuals;
using Worldline.GameState;
using UnityEngine;

namespace Worldline.Corruption.SpriteCorruption
{
    [HasDefaultState]
    public class BackgroundCorrupter : Corrupter
    {        
        public static BackgroundCorrupter MainBackgroundCorrupter { get; private set; }
        public static SpriteCorruptionTool SpriteCorruptionTool { get; } = new SpriteCorruptionTool();
        public Sprite[] CorruptedBackgrounds { get; set; } = new Sprite[NumberOfCorruptedBackgrounds];
        public bool BackgroundIsCorrupted { get; protected set; }
        public const int NumberOfCorruptedBackgrounds = 1;
        private static Sprite[] _previouslyCorruptedSprites;

        protected override void Awake()
        {
            if (MainBackgroundCorrupter != null && MainBackgroundCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainBackgroundCorrupter = this;
            base.Awake();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (GameOptions.LoadedGameOptions.PhotosensitiveMode)
            {
                enabled = false;
            }
        }

        protected override void OnLevelLoaded()
        {
            base.OnLevelLoaded();
            if (_previouslyCorruptedSprites != null && _previouslyCorruptedSprites[0] == BackgroundController.Main.BackgroundSprites[0]) return;

            for (int i = 0; i < NumberOfCorruptedBackgrounds; i++)
            {
                SpriteCorruptionTool.GenerateRandomOptionsWeighted(2, 1f);
                CorruptedBackgrounds[i] = SpriteCorruptionTool.GetCorruptedSprite(BackgroundController.Main.BackgroundSprites[Random.Range(0, BackgroundController.Main.BackgroundSprites.Length)]);
            }

            _previouslyCorruptedSprites = BackgroundController.Main.BackgroundSprites;
        }

        public override void RandomTick()
        {
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {
            if (Random.value < chance && chance >= 0.5f)
            {
                BackgroundController.Main.BackgroundSprites = new [] { CorruptedBackgrounds[Random.Range(0, CorruptedBackgrounds.Length)] };
                BackgroundIsCorrupted = true;
            }
        }

        public void Clean(float chance)
        {
            if (Random.value < chance && _previouslyCorruptedSprites != null && BackgroundIsCorrupted)
            {
                BackgroundController.Main.BackgroundSprites = _previouslyCorruptedSprites;
                BackgroundIsCorrupted = false;
            }
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            MainBackgroundCorrupter = null;
            _previouslyCorruptedSprites = null;
        }
    }
}
