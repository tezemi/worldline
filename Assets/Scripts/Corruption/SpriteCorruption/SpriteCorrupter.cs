﻿using System.Linq;
using System.Collections.Generic;
using Worldline.GameState;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// This is the main implementation of sprite corruption during the 
    /// game's corruption mechanic. When Intensity is high, this will find
    /// sprites that have corrupted versions cached, and will randomly
    /// select sprites in the world to be replaced by one of their corrupted
    /// counterparts. When Intensity is low, these replacements will be
    /// cleaned. This is similar to the TileMapCorrupter, and the UICorrupter.
    /// </summary>
    [HasDefaultState]
    public class SpriteCorrupter : GraphicsCorrupter
    {
        public static SpriteCorrupter MainSpriteCorrupter { get; private set; }
        public static List<SpriteRenderer> RenderersInWorld { get; private set; }
        public HashSet<Sprite> CorruptedReferences { get; private set; } = new HashSet<Sprite>();
        public Dictionary<SpriteRenderer, Sprite> CorruptedRenderers { get; private set; } = new Dictionary<SpriteRenderer, Sprite>();
        public const float ChanceToChangeIndexModifier = 0.75f;
        private readonly List<Sprite> _cleanedSprites = new List<Sprite>();
        private readonly List<SpriteRenderer> _cleanedRenderers = new List<SpriteRenderer>();
        private int _lastRootCount;

        protected override void Awake()
        {
            if (MainSpriteCorrupter != null && MainSpriteCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainSpriteCorrupter = this;
            base.Awake();
        }

        protected override void OnLevelLoaded()
        {
            RenderersInWorld = FindObjectsOfType<SpriteRenderer>().ToList();
            base.OnLevelLoaded();
        }

        protected virtual void LateUpdate()
        {
            if (RenderersInWorld == null) return;   // Level is probably loading in

            foreach (SpriteRenderer spriteRenderer in RenderersInWorld)
            {
                if (spriteRenderer == null || spriteRenderer.sprite == null || !CorruptedSpriteResources.ContainsKey(spriteRenderer.sprite)) continue;
                                
                if (CorruptedReferences.Contains(spriteRenderer.sprite) &&
                spriteRenderer.sprite != CorruptedSpriteResources[spriteRenderer.sprite].SpriteAtIndex)
                {                    
                    if (CorruptedReferences.Contains(spriteRenderer.sprite) && !CorruptedRenderers.ContainsKey(spriteRenderer))
                    {
                        CorruptedRenderers.Add(spriteRenderer, spriteRenderer.sprite);
                    }

                    if (Random.value < Intensity * ChanceToChangeIndexModifier)
                    {
                        CorruptedSpriteResources[spriteRenderer.sprite].Index = 
                        Random.Range(0, CorruptedSpriteResources[spriteRenderer.sprite].CorruptedSprites.Count);
                    }

                    spriteRenderer.sprite = CorruptedSpriteResources[spriteRenderer.sprite].SpriteAtIndex;
                }
            }

            FindSpriteRenderers();
        }

        protected virtual void FindSpriteRenderers()
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                if (scene.isLoaded)
                {
                    if (scene.rootCount > _lastRootCount)
                    {
                        var newRenderers = scene.GetRootGameObjects().Where
                        (
                            g => g.GetComponent<SpriteRenderer>() != null &&
                            !RenderersInWorld.Contains(g.GetComponent<SpriteRenderer>())
                        ).Select(o => o.GetComponent<SpriteRenderer>());

                        RenderersInWorld.AddRange(newRenderers);
                        _lastRootCount = scene.rootCount;
                    }
                }
            }
        }

        public override void RandomTick()
        {
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {
            foreach (Sprite sprite in CorruptedSpriteResources.Keys)
            {
                if (Random.value < chance)
                {
                    CorruptedReferences.Add(sprite);
                }
            }
        }

        public void Clean(float chance)
        {
            foreach (SpriteRenderer spriteRenderer in CorruptedRenderers.Keys)
            {
                if (spriteRenderer == null) continue;

                if (Random.value < chance)
                {
                    CorruptedReferences.Remove(CorruptedRenderers[spriteRenderer]);
                    spriteRenderer.sprite = CorruptedRenderers[spriteRenderer];
                    _cleanedRenderers.Add(spriteRenderer);
                }
            }

            foreach (SpriteRenderer spriteRenderer in _cleanedRenderers)
            {
                CorruptedRenderers.Remove(spriteRenderer);
            }
            
            foreach (Sprite sprite in CorruptedReferences)
            {
                if (Random.value < chance)
                {
                    _cleanedSprites.Add(sprite);
                }
            }

            foreach (Sprite sprite in _cleanedSprites)
            {
                CorruptedReferences.Remove(sprite);
            }

            _cleanedSprites.Clear();
            _cleanedRenderers.Clear();
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private void SetupDefaults()
        {
            RenderersInWorld = null;
            CorruptedReferences = null;
            CorruptedRenderers = null;
        }
    }
}
