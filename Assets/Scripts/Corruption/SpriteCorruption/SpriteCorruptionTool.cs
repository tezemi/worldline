﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections.Generic;
using JetBrains.Annotations;
using Worldline.Utilities;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// Class for creating corrupted looking versions of sprites. Instances 
    /// of SpriteCorruptionTool contain an Options dictionary, which is used to
    /// define different SpriteCorruptionOptions and associate them with
    /// different SpriteCorruptionTypes. The instance can then be used
    /// to generate corrupted sprites using the GetCorruptedSprite method.
    /// Depending on the specified options, sprite generation can often be
    /// expensive, and it is best if sprites generated in batches are made
    /// at load time and cached for use later.
    /// </summary>
    public class SpriteCorruptionTool : CorruptionTool<SpriteCorruptionType, SpriteCorruptionOptions>
    {
        /// <summary>
        /// All sprites that can be used pulling in random sprites for corruption. 
        /// If this is null, this tool will default to always calling 
        /// Resources.FindAllOfType, which is very expensive. It is best if this 
        /// is set by the code using this tool to ensure the corrupter is using
        /// up-to-date sprites that it doesn't have to load every corruption.
        /// </summary>
        [CanBeNull]
        public Sprite[] SpriteResources { get; set; }
        /// <summary>
        /// How big the sectors of sprites are. This is used to simulate 16-bit 
        /// era graphics by only working with small sectors of a sprite instead
        /// of the entire sprite itself.
        /// </summary>
        public const int SizeOfSectors = 16;
        /// <summary>
        /// Defines at what alpha a color is considered to be transparent.
        /// </summary>
        public const float TransparentAlpha = 0.1f;
        /// <summary>
        /// A multiplier used to determine how likely sectors will be generated 
        /// on the inside/outside of sprites.
        /// </summary>
        public const float InnerOuterWeight = 1.5f;
        /// <summary>
        /// How likely a sector won't be randomly selected when Horizontal is
        /// used.
        /// </summary>
        public const float HorizontalWeight = 2f;
        /// <summary>
        /// When generating sectors using the cluster method, this is the 
        /// likelihood that the next sector will be used if the current
        /// sector is used.
        /// </summary>
        public const float ClusterHighModifier = 2f;
        /// <summary>
        /// When generating sectors using the cluster method, this is the 
        /// likelihood that the next sector will not be used if the current
        /// sector is not used.
        /// </summary>
        public const float ClusterLowModifier = 0.25f;

        /// <summary>
        /// Returns a version of the specified sprite to look corrupted. Depending 
        /// on the amount of corruption, this can be expensive, so caching corrupted
        /// sprites during loading times is recommended.
        /// </summary>
        /// <param name="originalSprite">The original sprite to be corrupted.</param>
        /// <returns>A new sprite that has been corrupted.</returns>
        public Sprite GetCorruptedSprite(Sprite originalSprite)
        {
            try
            {
                int rectX = Mathf.FloorToInt(originalSprite.rect.x);
                int rectY = Mathf.FloorToInt(originalSprite.rect.y);
                int width = Mathf.FloorToInt(originalSprite.rect.width);
                int height = Mathf.FloorToInt(originalSprite.rect.height);
                Texture2D texture = new Texture2D(width, height);
                Color[] pixels = originalSprite.texture.GetPixels(rectX, rectY, width, height);

                // The working texture is the texture that is painted on.
                // It starts off exactly like the input texture, but
                // corruption is applied each loop.
                Texture2D workingTexture = new Texture2D(width, height);
                workingTexture.SetPixels(pixels);
                workingTexture.Apply();

                // Perform corruptions to the sprite based on enabled types
                var sortedOptions = Options.OrderByDescending(kvp => kvp.Value.Priority).ToDictionary(x => x.Key, x => x.Value);
                foreach (SpriteCorruptionType type in sortedOptions.Keys)
                {
                    SpriteCorruptionOptions options = Options[type];
                    if (!options.Enabled) continue;

                    // Here, a different method is called for each type of corruption 
                    // that accepts a new texture to paint on, an original texture to 
                    // use as a base, usually the sectors that should be affected, and
                    // sometimes the options if it needs to reference those at all.
                    for (int i = 0; i < options.Passes; i++)
                    {
                        List<SpriteSector> sectors = GetRandomSectors(options.PercentageOfSectors, width, height, options.SectorType);
                        switch (type)
                        {
                            case SpriteCorruptionType.FlipXSectors:
                                ApplyFlipXSectors(texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.FlipYSectors:
                                ApplyFlipYSectors(texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.Invert:
                                ApplyInvert(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.PaletteSwap:
                                ApplyPaletteSwap(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.ClownVomit:
                                ApplyClownVomit(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.BitColors:
                                ApplyBitColors(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.MoveSectors:
                                ApplyMoveSectors(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.ReplaceSectors:
                                ApplyReplaceSectors(originalSprite, texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.RemoveSectors:
                                ApplyRemoveSectors(texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.Tearing:
                                ApplyTearing(texture, workingTexture, sectors, options);
                                break;
                            case SpriteCorruptionType.RepeatSectors:
                                ApplyRepeatSectors(texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.OffsetSectors:
                                ApplyOffsetSectors(texture, workingTexture, sectors);
                                break;
                            case SpriteCorruptionType.RotateSectors:
                                ApplyRotateSectors(texture, workingTexture, sectors);
                                break;
                        }

                        Graphics.CopyTexture(texture, workingTexture); // Update working texture to reflect changes
                    }
                }

                texture.filterMode = FilterMode.Point;  // Everything in the game has this
                texture.Apply();                        // Apply any changes made to the texture

                // Sets the correct pivot point based on the original sprite
                Bounds bounds = originalSprite.bounds;
                var pivotX = -bounds.center.x / bounds.extents.x / 2 + 0.5f;
                var pivotY = -bounds.center.y / bounds.extents.y / 2 + 0.5f;

                // Create the new sprite
                Sprite newSprite = Sprite.Create 
                (
                    texture,
                    new Rect(0, 0, width, height),
                    new Vector2(pivotX, pivotY),
                    32
                );

                newSprite.name = $"Corrupted {originalSprite} {newSprite.GetInstanceID()}";

                return newSprite;
            }
            catch (UnityException e)
            {
                // 99 to 100% of the time, this is going to be the ReadWriteNotEnabledException
                Debug.LogWarning($"Could not draw corrupted sprite for {originalSprite.name}. Make sure read/write is enabled.{Environment.NewLine}{e}");

                return originalSprite;
            }
            catch (NullReferenceException e)
            {
                Debug.LogWarning($"Can not draw null texture or sprite. There must be a null texture somewhere in the scene.{Environment.NewLine}{e}");

                return originalSprite;
            }
        }

        /// <summary>
        /// Flips the pixels horizontally in the specified sectors, using the 
        /// original as a base and drawing on the new.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use a base.</param>
        /// <param name="sectors">The sectors where the pixels will get flipped.</param>
        protected void ApplyFlipXSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            foreach (SpriteSector sector in sectors)
            {
                for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                {
                    int oppositePixel = sector.XExtents.Item2;
                    for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(oppositePixel--, y));
                    }
                }
            }
        }

        /// <summary>
        /// Flips the pixels vertically in the specified sectors.
        /// </summary>
        /// <param name="newTexture"></param>
        /// <param name="originalTexture"></param>
        /// <param name="sectors"></param>
        protected void ApplyFlipYSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            foreach (SpriteSector sector in sectors)
            {
                for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                {
                    int oppositePixel = sector.YExtents.Item2;
                    for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, oppositePixel--));
                    }
                }
            }
        }

        /// <summary>
        /// Draws inverted pixels onto newTexture using the pixels from 
        /// originalTexture, only inside specified sectors.
        /// </summary>
        /// <param name="newTexture">The new texture to draw on.</param>
        /// <param name="originalTexture">The original texture to be used as a base.</param>
        /// <param name="sectors">The sectors of the sprite to invert.</param>
        /// <param name="options">The options for this type of corruption.</param>
        protected void ApplyInvert(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            for (int x = 0; x < newTexture.width; x++)
            {
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                        y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    if (pixelIsInsideSector)
                    {
                        Color pixel = originalTexture.GetPixel(x, y);
                        Color invertedColor = new Color
                        (
                            1f - pixel.r,
                            1f - pixel.g,
                            1f - pixel.b,
                            options.AffectsTransparency ? 1f - pixel.a : pixel.a
                        );

                        newTexture.SetPixel(x, y, invertedColor);
                    }
                    else
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Draws pixels within specified clusters to have a random palette swap 
        /// onto the new texture, based on the original.
        /// </summary>
        /// <param name="newTexture">The new texture to draw on.</param>
        /// <param name="originalTexture">The original texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw within.</param>
        /// <param name="options">The options for this type of corruption.</param>
        protected void ApplyPaletteSwap(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            Dictionary<Color, Color> colorMap = new Dictionary<Color, Color>();
            for (int x = 0; x < newTexture.width; x++)
            {
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                        y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    Color pixel = originalTexture.GetPixel(x, y);
                    if (!pixelIsInsideSector || pixel.a < TransparentAlpha && !options.AffectsTransparency)
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                    }
                    else
                    {
                        if (!colorMap.ContainsKey(pixel))
                        {
                            colorMap.Add(pixel, Random.ColorHSV());
                        }

                        newTexture.SetPixel(x, y, colorMap[pixel]);
                    }
                }
            }
        }

        /// <summary>
        /// Replaces pixels in sectors with one of two randomly generated colors.
        /// </summary>
        /// <param name="newTexture">The new texture to draw on.</param>
        /// <param name="originalTexture">The original texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw within.</param>
        /// <param name="options">The options for this type of corruption.</param>
        protected void ApplyBitColors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            int colorIndex = 0;
            int numberOfColors = (int)(options.Intensity * 10f);
            if (numberOfColors == 0)
            {
                numberOfColors = Random.Range(1, 10);
            }

            Color[] randomColors = new Color[numberOfColors];
            for (int i = 0; i < randomColors.Length; i++)
            {
                randomColors[i] = GetRandom16BitColor();
            }
            
            Dictionary<Color, Color> colorMap = new Dictionary<Color, Color>();
            for (int x = 0; x < newTexture.width; x++)
            {
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                        y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    Color pixel = originalTexture.GetPixel(x, y);
                    if (!pixelIsInsideSector || pixel.a < TransparentAlpha)
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                    }
                    else
                    {
                        if (!colorMap.ContainsKey(pixel))
                        {
                            colorMap.Add(pixel, randomColors[colorIndex]);
                            if (options.UseClustering)
                            {
                                colorIndex = Random.Range(0, randomColors.Length);
                            }
                            else if (++colorIndex >= randomColors.Length - 1)
                            {
                                colorIndex = 0;
                            }
                        }

                        newTexture.SetPixel(x, y, colorMap[pixel]);
                    }
                }
            }
        }

        /// <summary>
        /// Randomly selects sectors to move into another sector.
        /// </summary>
        /// <param name="newTexture">The new texture to draw on.</param>
        /// <param name="originalTexture">The original texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw within.</param>
        /// <param name="options">The options for this type of corruption.</param>
        protected void ApplyMoveSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            Color[,] sectorToMove = null;
            foreach (SpriteSector sector in sectors)
            {
                if (Random.value < options.Intensity && sectorToMove == null)
                {
                    sectorToMove = new Color[SizeOfSectors, SizeOfSectors];
                    int yIndex = 0;
                    for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                    {
                        int xIndex = 0;
                        for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                        {
                            sectorToMove[xIndex, yIndex] = originalTexture.GetPixel(x, y);
                            xIndex++;
                        }

                        yIndex++;
                    }
                }
                else if (Random.value < options.Intensity && sectorToMove != null)
                {
                    int yIndex = 0;
                    for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                    {
                        int xIndex = 0;
                        for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                        {
                            newTexture.SetPixel(x, y, sectorToMove[xIndex, yIndex]);
                            xIndex++;
                        }

                        yIndex++;
                    }

                    sectorToMove = null;
                }
            }
        }

        /// <summary>
        /// Applies a clown vomit affect to the specified texture based on the 
        /// original.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use as a base.</param>
        /// <param name="sectors">The sectors to apply clown vomit.</param>
        /// <param name="options">The options for this type of corruption.</param>
        protected void ApplyClownVomit(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            for (int x = 0; x < newTexture.width; x++)
            {
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                        y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    Color pixel = originalTexture.GetPixel(x, y);
                    if (pixelIsInsideSector && Random.value < options.Intensity && 
                    (pixel.a > TransparentAlpha || options.AffectsTransparency))
                    {
                        if (options.UseClustering && x > 1 && y > 1 && Random.value < options.ClusteringWeight)
                        {
                            newTexture.SetPixel(x, y, newTexture.GetPixel(x - Random.Range(0, 2), y - Random.Range(0, 2)));
                        }
                        else
                        {
                            newTexture.SetPixel(x, y, Random.ColorHSV());
                        }
                    }
                    else
                    {
                        newTexture.SetPixel(x, y, pixel);
                    }
                }
            }
        }

        /// <summary>
        /// Replaced the pixels in the specified sectors with transparent ones, 
        /// essentially removing them.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use a base.</param>
        /// <param name="sectors">The sectors where the pixels will get removed.</param>
        protected void ApplyRemoveSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            foreach (SpriteSector sector in sectors)
            {
                for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                {
                    for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                    {
                        newTexture.SetPixel(x, y, Color.clear);
                    }
                }
            }
        }

        /// <summary>
        /// Applies a horizontal tearing effect by reusing pixels across a wide 
        /// area of a sector.
        /// </summary>
        /// <param name="newTexture"></param>
        /// <param name="originalTexture"></param>
        /// <param name="sectors"></param>
        /// <param name="options">The options for this type of corruption.</param> 
        protected void ApplyTearing(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors, SpriteCorruptionOptions options)
        {
            int delayedX = 0;
            int delay = (int)(options.Intensity * 10f) + 1;
            for (int x = 0; x < newTexture.width; x++)
            {
                int delayedY = 0;
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                        y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    if (x % delay == 1)
                    {
                        delayedX++;
                    }

                    if (y % delay == 1)
                    {
                        delayedY++;
                    }

                    if (pixelIsInsideSector)
                    {
                        Color pixel = originalTexture.GetPixel(delayedX, delayedY);
                        newTexture.SetPixel(x, y, pixel);
                    }
                    else
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Replaces sectors with a single random sprite.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw on.</param>
        protected void ApplyReplaceSectors(Sprite originalSprite, Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            var ds = SpriteResources ?? Resources.FindObjectsOfTypeAll<Sprite>();
            tryAgain:
            int weightedIndex = ds.ToList().IndexOf(originalSprite);
            int incrementor = Random.value < 0.5f ? 1 : -1;
            for (; ; weightedIndex += incrementor)
            {
                if (weightedIndex >= ds.Length)
                {
                    weightedIndex = 0;
                }
                else if (weightedIndex < 0)
                {
                    weightedIndex = ds.Length - 1;
                }

                if (Random.value < 0.05f)
                {
                    break;
                }
            }

            var otherTexture = ds[weightedIndex].texture;
            if (otherTexture == null)
            {
                goto tryAgain;
            }

            try
            {
                otherTexture.GetPixel(0, 0);
            }
            catch (UnityException)
            {
                goto tryAgain;
            }

            foreach (SpriteSector sector in sectors)
            {
                for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                {
                    for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                    {
                        newTexture.SetPixel(x, y, otherTexture.GetPixel(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Selects a random sector and repeats it in all sectors.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw on.</param>
        protected void ApplyRepeatSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            Color[,] sectorToCopy = new Color[SizeOfSectors, SizeOfSectors];
            for (int i = 0; i < sectors.Count; i++)
            {
                SpriteSector sector = sectors[i];
                if (Random.value < (float) i / sectors.Count)
                {
                    int xCount = 0;
                    for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                    {
                        int yCount = 0;
                        for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                        {
                            sectorToCopy[xCount, yCount++] = originalTexture.GetPixel(x, y);
                        }

                        xCount++;
                    }
                }
            }

            foreach (SpriteSector sector in sectors)
            {
                int xCount = 0;
                for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                {
                    int yCount = 0;
                    for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                    {
                        newTexture.SetPixel(x, y, sectorToCopy[xCount, yCount++]);
                    }

                    xCount++;
                }
            }
        }

        /// <summary>
        /// Selects a random sprite and uses its location in memory to apply
        /// sectors.
        /// </summary>
        /// <param name="newTexture">The texture to draw on.</param>
        /// <param name="originalTexture">The texture to use as a base.</param>
        /// <param name="sectors">The sectors to draw on.</param>
        protected void ApplyOffsetSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            newTexture.SetPixels(originalTexture.GetPixels());
            newTexture.Apply();
            int textureIndex = -1;
            var ds = SpriteResources ?? Resources.FindObjectsOfTypeAll<Sprite>();
            tryAgain:
            for (int i = Random.Range(0, ds.Length); i < ds.Length; i++)
            {
                try
                {
                    Sprite sprite = ds[i];
                    Texture2D texture2D = sprite.texture;
                    if (texture2D == newTexture || texture2D == originalTexture) continue;

                    textureIndex = i;
                    texture2D.GetPixel(0, 0);
                    break;
                }
                catch (UnityException)
                {
                    goto tryAgain;
                }
            }

            if (textureIndex == -1) return;

            int offset = 0;
            foreach (SpriteSector sector in sectors)
            {
                for (int y = sector.YExtents.Item1; y < sector.YExtents.Item2; y++)
                {
                    for (int x = sector.XExtents.Item1; x < sector.XExtents.Item2; x++)
                    {
                        try
                        {
                            if (ds[textureIndex + offset].texture.width <= x || ds[textureIndex + offset].texture.width <= y)
                            {
                                offset++;
                            }

                            if (textureIndex + offset >= ds.Length)
                            {
                                offset = 0;
                                textureIndex -= sectors.Count - 1;
                            }

                            newTexture.SetPixel(x, y, ds[textureIndex + offset].texture.GetPixel(x, y));
                        }
                        catch (UnityException)
                        {
                            offset = 0;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Applies a rotation to the specified sectors by 90 degrees.
        /// </summary>
        /// <param name="newTexture"></param>
        /// <param name="originalTexture"></param>
        /// <param name="sectors"></param>
        protected void ApplyRotateSectors(Texture2D newTexture, Texture2D originalTexture, List<SpriteSector> sectors)
        {
            for (int x = 0; x < newTexture.width; x++)
            {
                for (int y = 0; y < newTexture.height; y++)
                {
                    bool pixelIsInsideSector = false;
                    foreach (SpriteSector spriteSector in sectors)
                    {
                        if (x >= spriteSector.XExtents.Item1 && x <= spriteSector.XExtents.Item2 &&
                            y >= spriteSector.YExtents.Item1 && y <= spriteSector.YExtents.Item2)
                        {
                            pixelIsInsideSector = true;
                            break;
                        }
                    }

                    if (pixelIsInsideSector)
                    {
                        Color pixel = originalTexture.GetPixel
                        (
                            y < originalTexture.width ? y : originalTexture.width,
                            x < originalTexture.height ? x : originalTexture.height
                        );

                        newTexture.SetPixel(x, y, pixel);
                    }
                    else
                    {
                        newTexture.SetPixel(x, y, originalTexture.GetPixel(x, y));
                    }
                }
            }
        }

        /// <summary>
        /// Applies random options equally distributed across all settings.
        /// </summary>
        /// <param name="maxPasses">The maximum number of passes a type of corruption could occur.</param>
        /// <param name="seed">An optional seed to use during generation.</param>
        public virtual void GenerateRandomOptions(int maxPasses, int? seed = null)
        {
            if (seed != null)
            {
                Random.InitState(seed.Value);
            }

            var corruptionTypes = Enum.GetValues(typeof(SpriteCorruptionType));
            var sectorTypes = (SectorType[])Enum.GetValues(typeof(SectorType));
            foreach (SpriteCorruptionType type in corruptionTypes)
            {
                this[type] = new SpriteCorruptionOptions
                {
                    Enabled = Random.value < 0.5f,
                    AffectsTransparency = Random.value < 0.5f,
                    ClusteringWeight = Random.value,
                    Intensity = Random.value,
                    Passes = Random.Range(1, maxPasses + 1),
                    PercentageOfSectors = Random.value,
                    Priority = Random.Range(0, corruptionTypes.Length),
                    SectorType = sectorTypes[Random.Range(0, sectorTypes.Length)],
                    UseClustering = Random.value < 0.5f
                };
            }
        }

        /// <summary>
        /// Applies random options weighted to prefer particular settings for 
        /// the game's corruption mechanic.
        /// </summary>
        /// <param name="maxPasses">The maximum number of passes that could 
        /// possibly be performed on a sprite.</param>
        /// <param name="maxIntensity">The highest the intensity option could possibly be.</param> 
        /// <param name="seed">An optional seed to use during RNG.</param> 
        public virtual void GenerateRandomOptionsWeighted(int maxPasses, float maxIntensity, int? seed = null)
        {
            if (seed != null)
            {
                Random.InitState(seed.Value);
            }

            var corruptionTypes = Enum.GetValues(typeof(SpriteCorruptionType));
            var sectorTypes = (SectorType[])Enum.GetValues(typeof(SectorType));
            foreach (SpriteCorruptionType type in corruptionTypes)
            {
                this[type] = new SpriteCorruptionOptions
                {
                    Enabled = Random.value < (type is SpriteCorruptionType.ReplaceSectors ? 0.85f : 0.25f), // Increased chance to bring in random sprites
                    AffectsTransparency = Random.value < 0.25f,
                    ClusteringWeight = Random.value,
                    Intensity = Random.value,
                    Passes = Random.Range(1, maxPasses + 1),
                    PercentageOfSectors = Random.value,
                    Priority = type is SpriteCorruptionType.ReplaceSectors ? corruptionTypes.Length : Random.Range(0, corruptionTypes.Length), // Random sprites increase priority 
                    SectorType = sectorTypes[Random.Range(0, sectorTypes.Length)],
                    UseClustering = Random.value < (type is SpriteCorruptionType.ReplaceSectors ? 0.5f : 0.15f)
                };
            }
        }

        /// <summary>
        /// Creates a returns a list of randomly generated sectors. Sectors are 
        /// just minimum and maximum bounds on a sprite.
        /// </summary>
        /// <param name="percentageOfSectors">What percentage of the sprite sectors 
        /// should be generated for. 1 = 100% of the sprite, and 0 = 0% of the sprite.</param>
        /// <param name="sizeX">The width of the sprite.</param>
        /// <param name="sizeY">The height of the sprite.</param>
        /// <param name="sectorType">The type of random sectors to get. This usually
        /// refers to applying weight to the random generation.</param>
        /// <returns></returns>
        public static List<SpriteSector> GetRandomSectors(float percentageOfSectors, int sizeX, int sizeY, SectorType sectorType)
        {
            List<SpriteSector> sectors = new List<SpriteSector>();
            switch (sectorType)
            {
                case SectorType.Random:
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            if (Random.value < percentageOfSectors)
                            {
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.Clustered:
                    float chanceModifier = ClusterLowModifier;
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            if (Random.value < percentageOfSectors * chanceModifier)
                            {
                                chanceModifier = ClusterHighModifier;
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                            else
                            {
                                chanceModifier = ClusterLowModifier;
                            }
                        }
                    }

                    break;
                case SectorType.Horizontal:
                    int midPointX = sizeX / 2;
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            float multi = 1f;
                            if (x > midPointX)
                            {
                                multi = HorizontalWeight;
                            }

                            if (Random.value < percentageOfSectors * multi)
                            {
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.Vertical:
                    int midPointY = sizeY / 2;
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            float multi = 1f;
                            if (y > midPointY)
                            {
                                multi = HorizontalWeight;
                            }

                            if (Random.value < percentageOfSectors * multi)
                            {
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.InnerWeight:
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            float distanceFromCenter = Mathf.Abs((x + y) / 2f - (sizeX + sizeY) / 2f);
                            float inRange = MathUtilities.GetRange
                            (
                                distanceFromCenter,
                                0f,
                                (sizeX + sizeY) / 2f,
                                InnerOuterWeight,
                                1f
                            );

                            if (Random.value < percentageOfSectors * inRange)
                            {
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.OuterWeight:
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            float distanceFromCenter = Mathf.Abs((x + y) / 2f - (sizeX + sizeY) / 2f);
                            float inRange = MathUtilities.GetRange
                            (
                                distanceFromCenter,
                                0f,
                                (sizeX + sizeY) / 2f,
                                InnerOuterWeight,
                                1f
                            );

                            if (Random.value * inRange < percentageOfSectors)
                            {
                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.Columns:
                    List<int> columnsToAffect = new List<int>();
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            if (Random.value < percentageOfSectors && y == 0 || columnsToAffect.Contains(x))
                            {
                                if (!columnsToAffect.Contains(x))
                                {
                                    columnsToAffect.Add(x);
                                }

                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.Rows:
                    List<int> rowsToAffect = new List<int>();
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        {
                            if (Random.value < percentageOfSectors && x == 0 || rowsToAffect.Contains(y))
                            {
                                if (!rowsToAffect.Contains(y))
                                {
                                    rowsToAffect.Add(y);
                                }

                                sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                            }
                        }
                    }

                    break;
                case SectorType.All:
                    for (int x = 0; x < sizeX + SizeOfSectors; x += SizeOfSectors)
                    {
                        for (int y = 0; y < sizeY + SizeOfSectors; y += SizeOfSectors)
                        { 
                            sectors.Add(new SpriteSector((x, x + SizeOfSectors), (y, y + SizeOfSectors)));
                        }
                    }

                    break;
                case SectorType.None:
                    break;
            }

            return sectors;
        }

        /// <summary>
        /// Generates a random color that will commonly be found on a 16-bit 
        /// palette. Avoids colors with any transparency, prefers solid shades.
        /// </summary>
        /// <returns>The random color.</returns>
        public static Color GetRandom16BitColor()
        {
            return new Color
            (
                Random.value < 0.5f ? 1f : 0f,
                Random.value < 0.5f ? 1f : 0f,
                Random.value < 0.5f ? 1f : 0f,
                1f
            );
        }
    }
}
