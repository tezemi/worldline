﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.Corruption.SpriteCorruption
{ 
    public class UICorrupter : GraphicsCorrupter
    {
        public static UICorrupter MainUICorrupter { get; private set; }
        public static List<Image> ImagesInWorld { get; protected set; }
        public HashSet<Sprite> CorruptedReferences { get; protected set; } = new HashSet<Sprite>();
        public Dictionary<Image, Sprite> CorruptedImages { get; protected set; } = new Dictionary<Image, Sprite>();
        private int _lastRootCount;
        private readonly List<Sprite> _cleanedSprites = new List<Sprite>();
        private readonly List<Image> _cleanedImages = new List<Image>();
        
        protected override void Awake()
        {
            if (MainUICorrupter != null && MainUICorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainUICorrupter = this;
            base.Awake();
        }

        protected override void OnLevelLoaded()
        {
            ImagesInWorld = FindObjectsOfType<Image>().ToList();
            base.OnLevelLoaded();
        }

        protected virtual void LateUpdate()
        {
            if (ImagesInWorld == null) return;

            foreach (Image image in ImagesInWorld)
            {
                if (image == null || image.sprite == null || !CorruptedSpriteResources.ContainsKey(image.sprite)) continue;

                if (CorruptedReferences.Contains(image.sprite) &&
                image.sprite != CorruptedSpriteResources[image.sprite].SpriteAtIndex)
                {
                    if (CorruptedReferences.Contains(image.sprite) && !CorruptedImages.ContainsKey(image))
                    {
                        CorruptedImages.Add(image, image.sprite);
                    }

                    if (Random.value < 0.05f)
                    {
                        CorruptedSpriteResources[image.sprite].Index =
                        Random.Range(0, CorruptedSpriteResources[image.sprite].CorruptedSprites.Count);
                    }

                    image.sprite = CorruptedSpriteResources[image.sprite].SpriteAtIndex;
                }
            }

            ImagesInWorld = FindObjectsOfType<Image>().ToList();
        }

        public override void RandomTick()
        {
            ImagesInWorld = FindObjectsOfType<Image>().ToList();
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {
            foreach (Sprite sprite in CorruptedSpriteResources.Keys)
            {
                if (Random.value < chance)
                {
                    CorruptedReferences.Add(sprite);
                }
            }
        }

        public void Clean(float chance)
        {
            foreach (Image image in CorruptedImages.Keys)
            {
                if (image == null) continue;

                if (Random.value < chance)
                {
                    CorruptedReferences.Remove(CorruptedImages[image]);
                    image.sprite = null;
                    Canvas.ForceUpdateCanvases();
                    image.sprite = CorruptedImages[image];
                    Canvas.ForceUpdateCanvases();
                    _cleanedImages.Add(image);
                }
            }

            foreach (Image image in _cleanedImages)
            {
                CorruptedImages.Remove(image);
            }

            foreach (Sprite sprite in CorruptedReferences)
            {
                if (Random.value < chance)
                {
                    _cleanedSprites.Add(sprite);
                }
            }

            foreach (Sprite sprite in _cleanedSprites)
            {
                CorruptedReferences.Remove(sprite);
            }

            _cleanedSprites.Clear();
            _cleanedImages.Clear();
        }
    }
}
