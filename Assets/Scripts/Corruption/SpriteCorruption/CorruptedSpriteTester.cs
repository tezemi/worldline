﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// This is a component used for testing the SpriteCorruptionTool. Attach
    /// it to a GameObject with a SpriteRenderer, and you will be able to
    /// configure SpriteCorruptionOptions from the editor.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class CorruptedSpriteTester : MonoBehaviour
    {
        [Tooltip("Different types of corruption to apply.")]
        public List<SpriteCorruptionType> CorruptionTypes;
        [Tooltip("Options corresponding to each of the above types.")]
        public List<SpriteCorruptionOptions> SpriteCorruptionOptions;
        public SpriteCorruptionTool SpriteCorruptionTool { get; set; } = new SpriteCorruptionTool();
        public Sprite OriginalSprite { get; protected set; }
        public Sprite CorruptedSprite { get; protected set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        
        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            OriginalSprite = SpriteRenderer.sprite;
        }

        protected virtual void UpdateSpriteCorrupterOptions()
        {
            if (CorruptionTypes.Count != SpriteCorruptionOptions.Count)
            {
                Debug.LogWarning("Sprite corruption types list must be the same size as the options list.", gameObject);
                return;
            }

            SpriteCorruptionTool.ResetCorrupter();
            int index = 0;
            foreach (SpriteCorruptionType type in CorruptionTypes)
            {
                SpriteCorruptionTool[type] = SpriteCorruptionOptions[index++];
            }
        }

        public virtual void ApplyCorruptedSprite()
        {
            UpdateSpriteCorrupterOptions();
            CorruptedSprite = SpriteCorruptionTool.GetCorruptedSprite(OriginalSprite);
            SpriteRenderer.sprite = CorruptedSprite;
        }

        public virtual void ResetSprite()
        {
            SpriteRenderer.sprite = OriginalSprite;
        }

        public virtual void SaveSpriteAsPNG()
        {
            File.WriteAllBytes($"{Application.persistentDataPath}/{SpriteRenderer.sprite.name}-{DateTime.Now.Millisecond}.png", SpriteRenderer.sprite.texture.EncodeToPNG());
        }
    }
}
