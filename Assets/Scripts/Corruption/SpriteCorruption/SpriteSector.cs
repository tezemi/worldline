﻿
namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// A SpriteSectors represents the X and Y bounds of a section of a 
    /// sprite, where XExtends is a minimum and maximum X, and YExtends
    /// is a minimum and maximum Y. Usually, the bounds of sectors are
    /// 16 by 16 pixels big, to simulate the effect of a 16-bit era game,
    /// but the bounds can be specified as any size.
    /// </summary>
    public struct SpriteSector
    {
        public (int, int) XExtents;
        public (int, int) YExtents;

        public SpriteSector((int, int) xExtents, (int, int) yExtents)
        {
            XExtents = xExtents;
            YExtents = yExtents;
        }
    }
}
