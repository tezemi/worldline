﻿using System;

namespace Worldline.Corruption.SpriteCorruption
{
    /// <summary>
    /// Represents options to use for a specific type of corruption.
    /// </summary>
    [Serializable]
    public class SpriteCorruptionOptions
    {
        /// <summary>
        /// Whether or not this corruption type should even occur.
        /// </summary>
        public bool Enabled;
        /// <summary>
        /// Whether or not the corruption should effect transparent pixels. 
        /// Depending on the type of corruption, this may do nothing.
        /// </summary>
        public bool AffectsTransparency;
        /// <summary>
        /// Whether or not the type of corruption should use clustering if applicable.
        /// </summary>
        public bool UseClustering;
        /// <summary>
        /// The priority defines what order corrupted sprites will be generated.
        /// Types with a higher priority get generated first.
        /// </summary>
        public int Priority;
        /// <summary>
        /// The number of times this type of corruption will run.
        /// </summary>
        public int Passes = 1;
        /// <summary>
        /// The amount of sectors to possibly affect from zero to one. If SectorType 
        /// is None or All, this does nothing.
        /// </summary>
        public float PercentageOfSectors;
        /// <summary>
        /// The intensity of the corruption. Depending on the corruption, this
        /// may have no effect.
        /// </summary>
        public float Intensity;
        /// <summary>
        /// If clustering is enabled, this is the amount of clustering that will occur.
        /// </summary>
        public float ClusteringWeight;
        /// <summary>
        /// The type of sectoring to use.
        /// </summary>
        public SectorType SectorType;
    }
}
