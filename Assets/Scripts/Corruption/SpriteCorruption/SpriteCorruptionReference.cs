﻿using System.Collections.Generic;
using UnityEngine;

namespace Worldline.Corruption.SpriteCorruption
{
    public class SpriteCorruptionReference
    {
        public int Index { get; set; }
        public List<Sprite> CorruptedSprites { get; protected set; }
        public Sprite SpriteAtIndex => CorruptedSprites[Index];

        public SpriteCorruptionReference(Sprite firstSprite)
        {
            CorruptedSprites = new List<Sprite> { firstSprite };
        }
    }
}
