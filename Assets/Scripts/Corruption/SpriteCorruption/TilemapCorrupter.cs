﻿using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.GameState;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.Corruption.SpriteCorruption
{
    [HasDefaultState]
    public class TilemapCorrupter : GraphicsCorrupter
    {
        public static Sprite NullTileSprite { get; set; }
        public static TilemapCorrupter MainTilemapCorrupter { get; set; }
        public static Tilemap[] TilemapsInWorld { get; set; }
        public static readonly Stack<Tile> TileCache = new Stack<Tile>();
        public static Dictionary<(Vector3Int, Tilemap), Sprite> CorruptedTiles { get; private set; } = new Dictionary<(Vector3Int, Tilemap), Sprite>();
        public const int MinTilesToCorrupt = 2;
        public const int MaxTilesToCorrupt = 6;
        public const int TileCacheSize = 300;
        private readonly List<(Vector3Int, Tilemap)> _cleanedTiles = new List<(Vector3Int, Tilemap)>();
        
        protected override void Awake()
        {
            if (MainTilemapCorrupter != null && MainTilemapCorrupter != this)
            {
                Destroy(gameObject);
                return;
            }

            MainTilemapCorrupter = this;
            base.Awake();

            if (TileCache.Count < TileCacheSize)
            {
                for (int i = 0; i < TileCacheSize; i++)
                {
                    var tile = ScriptableObject.CreateInstance<Tile>();
                    TileCache.Push(tile);
                }
            }
        }

        protected override void OnLevelLoaded()
        {
            TilemapsInWorld = FindObjectsOfType<Tilemap>();
            base.OnLevelLoaded();
        }

        public override void RandomTick()
        {
            Corrupt(Intensity);
            Clean(1f - Intensity);
        }

        public void Corrupt(float chance)
        {
            if (TilemapsInWorld == null) return;

            foreach (Tilemap tilemap in TilemapsInWorld)
            {
                if (tilemap == null || tilemap.GetComponent<TilemapCollider2D>() != null || chance <= 0) continue;

                Bounds cameraBounds = CameraOperator.ActiveCamera.CameraComponent.OrthographicBounds();
                int startTileX = Mathf.FloorToInt(Random.Range(cameraBounds.min.x, cameraBounds.max.x));
                int startTileY = Mathf.FloorToInt(Random.Range(cameraBounds.min.y, cameraBounds.max.y));
                int endTileX = startTileX + Random.Range(MinTilesToCorrupt, MaxTilesToCorrupt) * (Random.value < 0.5f ? 1 : -1);
                int endTileY = startTileY + Random.Range(MinTilesToCorrupt, MaxTilesToCorrupt) * (Random.value < 0.5f ? 1 : -1);
                for (int x = startTileX; x != endTileX; x += endTileX > startTileX ? 1 : -1)
                {
                    for (int y = startTileY; y != endTileY; y += endTileY > startTileY ? 1 : -1)
                    {
                        Vector3Int position = new Vector3Int(x, y, 0);
                        TileBase tileBase = tilemap.GetTile(position);
                        if (Random.value < chance)
                        {
                            if (tileBase == null || !(tileBase is Tile))
                            {
                                if (!CorruptedTiles.ContainsKey((position, tilemap)))
                                {
                                    CorruptedTiles.Add((position, tilemap), null);
                                }

                                // For null tiles, we use a specific sprite that is always stored here
                                // If no sprite is loaded, find a valid random sprite to use
                                while (NullTileSprite == null)
                                {
                                    Sprite template = SpriteResources[Random.Range(0, SpriteResources.Length)];
                                    if (CorruptedSpriteResources.ContainsKey(template))
                                    {
                                        NullTileSprite = CorruptedSpriteResources[template].SpriteAtIndex;
                                    }
                                }

                                Tile corruptedTile = TileCache.Count > 0 ? TileCache.Pop() : ScriptableObject.CreateInstance<Tile>();
                                corruptedTile.sprite = NullTileSprite;
                                tilemap.SetTile(position, corruptedTile);
                            }
                            else if (tileBase is Tile tile && !CorruptedTiles.ContainsKey((position, tilemap)))
                            {
                                NullTileSprite = tile.sprite;
                                CorruptedTiles.Add((position, tilemap), tile.sprite);
                                Tile corruptedTile = TileCache.Count > 0 ? TileCache.Pop() : ScriptableObject.CreateInstance<Tile>();
                                if (CorruptedSpriteResources.ContainsKey(tile.sprite))
                                {
                                    corruptedTile.sprite = CorruptedSpriteResources[tile.sprite].SpriteAtIndex;
                                }
                                else
                                {
                                    corruptedTile.sprite = null;
                                    while (corruptedTile.sprite == null)
                                    {
                                        Sprite template = SpriteResources[Random.Range(0, SpriteResources.Length)];
                                        if (CorruptedSpriteResources.ContainsKey(template))
                                        {
                                            corruptedTile.sprite = CorruptedSpriteResources[template].SpriteAtIndex;
                                        }
                                    }
                                }
                                
                                tilemap.SetTile(position, corruptedTile);
                            }
                        }                   
                    }
                }
            }
        }

        public void Clean(float chance)
        {
            foreach ((Vector3Int, Tilemap) tilePosition in CorruptedTiles.Keys)
            {
                if (tilePosition.Item2 == null)
                {
                    _cleanedTiles.Add(tilePosition);
                }
                else if (Random.value < chance)
                {
                    if (CorruptedTiles[tilePosition] == null)
                    {
                        Tile oldTile = tilePosition.Item2.GetTile<Tile>(tilePosition.Item1);
                        TileCache.Push(oldTile);
                        tilePosition.Item2.SetTile(tilePosition.Item1, null);
                    }
                    else
                    {

                        Tile originalTile = TileCache.Count > 0 ? TileCache.Pop() : ScriptableObject.CreateInstance<Tile>();
                        originalTile.sprite = CorruptedTiles[tilePosition];
                        tilePosition.Item2.SetTile(tilePosition.Item1, originalTile);
                    }

                    _cleanedTiles.Add(tilePosition);
                }
            }

            foreach ((Vector3Int, Tilemap) tile in _cleanedTiles)
            {
                CorruptedTiles.Remove(tile);
            }

            _cleanedTiles.Clear();
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            TilemapsInWorld = null;
            CorruptedTiles = new Dictionary<(Vector3Int, Tilemap), Sprite>();
        }
    }
}
