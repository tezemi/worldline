﻿using System;
using System.Collections.Generic;

namespace Worldline.Corruption
{
    /// <summary>
    /// Base abstract class for corruption tools. Implements associating 
    /// different types of corruption with option classes for which settings
    /// can be applied. Corruption tools are classes which can receive some
    /// sort of input, and spit a "corrupted version" back out using the
    /// specified options.
    /// </summary>
    /// <typeparam name="T">The types of corruption, usually an enum.</typeparam>
    /// <typeparam name="U">An options class for applying settings.</typeparam>
    public abstract class CorruptionTool<T, U> where T : struct, IConvertible where U : new()
    {
        public Dictionary<T, U> Options { get; protected set; }

        public U this[T type]
        {
            get => Options[type];
            set => Options[type] = value;
        }

        protected CorruptionTool()
        {
            Options = new Dictionary<T, U>();
            foreach (T type in Enum.GetValues(typeof(T)))
            {
                Options.Add(type, new U());
            }
        }

        protected CorruptionTool(Dictionary<T, U> options)
        {
            Options = new Dictionary<T, U>();
            foreach (T type in Enum.GetValues(typeof(T)))
            {
                Options.Add(type, new U());
            }

            foreach (T spriteCorruptionType in options.Keys)
            {
                Options[spriteCorruptionType] = options[spriteCorruptionType];
            }
        }

        public void ResetCorrupter()
        {
            Options = new Dictionary<T, U>();
            foreach (T type in Enum.GetValues(typeof(T)))
            {
                Options.Add(type, new U());
            }
        }
    }
}
