﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// AnimationOperator for particle effects. Calls the Die() method on
    /// the attached ParticleEffect when the animation is finished playing.
    /// </summary>
    public class ParticleAnimationOperator : AnimationOperator
    {
        protected override void Step()
        {
            if (CurrentFrame >= Sprites.Length && Sprites.Length > 1)
            {
                Timing.RunCoroutine(Die(), Segment.FixedUpdate);
            }

            IEnumerator<float> Die()
            {
                if (TimeOperator != null)
                {
                    yield return this.WaitForFixedSecondsScaled(Delay);
                }
                else
                {
                    yield return Timing.WaitForSeconds(Delay);
                }

                if (GetComponent<ParticleEffect>() != null)
                {
                    GetComponent<ParticleEffect>().Die();
                }
            }
        }

        public void ResetAnimator()
        {
            CurrentFrame = 0;
            GetComponent<SpriteRenderer>().sprite = Sprites[0];
        }
    }
}

