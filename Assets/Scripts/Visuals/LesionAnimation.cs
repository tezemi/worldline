﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Visuals
{
    public class LesionAnimation : MonoBehaviour
    {
        public int RotationIntervalMin = 60;
        public int RotationIntervalMax = 120;
        public float RotationSpeed = 0.02f;
        public int FramesTilNextRotation { get; protected set; }
        public Quaternion RotationToMoveTo { get; protected set; }
        private int _frameCount;


        protected virtual void Update()
        {
            transform.rotation = Quaternion.Lerp(transform.rotation, RotationToMoveTo, RotationSpeed);
            if (_frameCount++ >= FramesTilNextRotation)
            {
                RotationToMoveTo = Random.rotation;
                FramesTilNextRotation = Random.Range(RotationIntervalMin, RotationIntervalMax);
                _frameCount = 0;
            }
        }
    }
}
