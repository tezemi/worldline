﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Visuals
{
    public class ConfettiSpawnPoint : MonoBehaviour
    {             
        public int ConfettiAmount = 30;
        public float MaxDistance = 2.6f;
        public GameObject ConfettiPrefab;
        public int ConfettiCount { get; protected set; }
        public List<GameObject> SpawnedConfetti { get; protected set; } = new List<GameObject>();
        public const float MinDelay = 0.1f;
        public const float MaxDelay = 0.3f;

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.T))
            {
                ItsPartyTime();
            }

            for (var i = 0; i < SpawnedConfetti.Count; i++)
            {
                GameObject confetti = SpawnedConfetti[i];
                if (confetti == null) continue;
                
                if (confetti.GetComponent<Confetti>().Dead)
                {
                    SpawnedConfetti.Remove(confetti);
                    ConfettiCount++;
                }
            }
        }

        public void ItsPartyTime()
        {
            ConfettiCount = 0;
            CoroutineTimer coroutine = new CoroutineTimer(GetComponent<TimeOperator>(), Spawn);
            coroutine.StartTimer
            (
                () => Random.Range(MinDelay, MaxDelay), 
                () => isActiveAndEnabled && ConfettiCount < ConfettiAmount
            );
        }

        private void Spawn()
        {
            GameObject confetti = Instantiate(ConfettiPrefab);
            confetti.transform.position = transform.position + new Vector3(Random.Range(0, MaxDistance), 0, 0);
            SpawnedConfetti.Add(confetti);
        }
    }
}
