﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.Visuals
{
    public enum CameraMode
    {
        Lock,
        Float,
        FloatVertical,
        Lerp,
        Static
    }
}
