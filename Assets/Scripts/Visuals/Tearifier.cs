﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.TimeEngine;
using UnityEngine;
using UnityEngine.UI;
using Worldline.GameState;
using Random = UnityEngine.Random;

namespace Worldline.Visuals
{
    /// <summary>
    /// This name is just too great to refactor into something more professional.
    /// When attached to a GameObject with a SpriteRenderer, it will ensure every
    /// rendered Sprite will appear using the standard look for tears. Besides
    /// it's obvious use for all Tear enemies, this can be attached to any
    /// GameObject to give it a tear-like appearance. The modified Sprites are
    /// set in the LateUpdate(), so any MonoBehaviours that set sprites in the
    /// LateUpdate() will break this.
    /// </summary>
    public class Tearifier : MonoBehaviour
    {
        /// <summary>
        /// Every time a new color is mapped, ColorIndex is used to choose the
        /// output color, and then is incremented. This reduces the chances of
        /// two pixels next to each other within a sprite will share the same
        /// color.
        /// </summary>
        public static int ColorIndex { get; set; }
        /// <summary>
        /// This contains a list of all of the generated sprites. Mainly used 
        /// to compare a currently active sprite to a generated one to see if
        /// a sprite should be generated. The values are the original sprites.
        /// </summary>
        public static Dictionary<Sprite, Sprite> TearifiedSprites { get; protected set; } = new Dictionary<Sprite, Sprite>();
        /// <summary>
        /// This contains a cache of every sprite that is generated so a new one 
        /// does not have to be generated for each GameObject. The key of this
        /// dictionary is the input/original sprite, and the value is another 
        /// dictionary where the key is a TimeEffect, and the value is a sprite
        /// based on the color of that TimeEffect.
        /// </summary>
        public static Dictionary<Sprite, Dictionary<TimeEffect, Sprite[]>> SpriteCache { get; protected set; } = new Dictionary<Sprite, Dictionary<TimeEffect, Sprite[]>>();
        /// <summary>
        /// All generated color maps. A color map is an association where an 
        /// original color has a corresponding new color. Color maps are 
        /// generated differently for each TimeEffect.
        /// </summary>
        public static Dictionary<TimeEffect, Dictionary<int, Dictionary<Color, Color>>> ColorMaps { get; protected set; } = new Dictionary<TimeEffect, Dictionary<int, Dictionary<Color, Color>>>();
        /// <summary>
        /// Every sprite has AmountOfVariants tearified versions. This is the 
        /// index in the array of different variants that should be used.
        /// </summary>
        public int CurrentVariant { get; set; }
        /// <summary>
        /// Change this to change the color maps and color scheme used for generating 
        /// sprites.
        /// </summary>
        public TimeEffect TimeEffect { get; set; } = TimeEffect.None;
        /// <summary>
        /// Whether or not this Tearifier is currently flickering. Flickering 
        /// is simply when the CurrentVariant is changed each frame resulting
        /// in a flickering effect.
        /// </summary>
        public bool Flickering { get; protected set; }
        /// <summary>
        /// If not Flickering, this is the amount of frames between the last 
        /// flicker. If Flickering, then this is the amount of frames before
        /// the flickering started.
        /// </summary>
        public int FrameCount { get; protected set; }
        /// <summary>
        /// The number of frames before this Tearifier will flicker. Randomly 
        /// assigned each Flicker to be between FrameCountMin and FrameCountMax.
        /// </summary>
        public int FramesBeforeFlicker { get; protected set; } = 175;
        /// <summary>
        /// The amount of frames to spend Flickering. Randomly assigned each 
        /// flicker to be between ChangeFrameMin and ChangeFrameMax.
        /// </summary>
        public int FramesToFlicker { get; protected set; }
        /// <summary>
        /// The number of variants for each type of sprite.
        /// </summary>
        public const int AmountOfVariants = 6;
        /// <summary>
        /// The minimum number of frames before Flickering.
        /// </summary>
        public const int FrameCountMin = 60;
        /// <summary>
        /// The maximum number of frames before Flickering.
        /// </summary>
        public const int FrameCountMax = 520;
        /// <summary>
        /// The minimum number of frames to spend Flickering.
        /// </summary>
        public const int ChangeFrameMin = 60;
        /// <summary>
        /// The maximum number of frames to spend Flickering.
        /// </summary>
        public const int ChangeFrameMax = 120;
        /// <summary>
        /// The amount of variance in color shade to apply to a sprite when it 
        /// is generated using a TimeEffect. Increase this number to make sprite
        /// color more varied, or decrease it to make color more similar.
        /// </summary>
        public const float TimeEffectShadeAmount = 0.55f;
        protected IImbueable Imbueable { get; set; }
        protected Image Image { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        
        protected virtual void Awake()
        {
            Image = GetComponent<Image>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            if (GetComponent<IImbueable>() != null)
            {
                Imbueable = GetComponent<IImbueable>();
                TimeEffect = Imbueable.Imbuement;
            }
        }

        protected virtual void LateUpdate()
        {
            if (Imbueable != null && TimeEffect != Imbueable.Imbuement)
            {
                TimeEffect = Imbueable.Imbuement;
                UpdateSprite();
            }

            if (SpriteRenderer != null)
            {
                if (!TearifiedSprites.ContainsKey(SpriteRenderer.sprite))
                {
                    UpdateSprite();
                }
            }

            if (Image != null)
            {
                if (!TearifiedSprites.ContainsKey(Image.sprite))
                {
                    UpdateSprite();
                }
            }

            if (!GameOptions.LoadedGameOptions.PhotosensitiveMode)
            {
                if (!Flickering && FrameCount >= FramesBeforeFlicker)
                {
                    Flickering = true;
                    FrameCount = 0;
                    FramesBeforeFlicker = Random.Range(FrameCountMin, FrameCountMax);
                    FramesToFlicker = Random.Range(ChangeFrameMin, ChangeFrameMax);
                }

                if (Flickering && FrameCount < FramesToFlicker)
                {
                    CurrentVariant = Random.Range(0, AmountOfVariants);
                    UpdateSprite();
                }

                if (Flickering && FrameCount >= FramesToFlicker)
                {
                    Flickering = false;
                    FrameCount = 0;
                }
            }

            FrameCount++;
        }

        /// <summary>
        /// Update sprite forces the tearifier to change the current sprite to 
        /// a tearified version. If the current sprite is already a tearified
        /// sprite, it will use the original. If nothing has changed, for example,
        /// the current variant, or the time effect, then this will result in no
        /// change.
        /// </summary>
        public void UpdateSprite()
        {
            if (SpriteRenderer != null)
            {
                SpriteRenderer.sprite = GetTearifiedSprite
                (
                    SpriteRenderer.sprite,
                    ColorMaps.ContainsKey(TimeEffect) && ColorMaps[TimeEffect].ContainsKey(CurrentVariant)
                        ? ColorMaps[TimeEffect][CurrentVariant]
                        : new Dictionary<Color, Color>(),
                    TimeEffect,
                    CurrentVariant
                );
            }

            if (Image != null)
            {
                Image.sprite = GetTearifiedSprite
                (
                    Image.sprite,
                    ColorMaps.ContainsKey(TimeEffect) && ColorMaps[TimeEffect].ContainsKey(CurrentVariant)
                        ? ColorMaps[TimeEffect][CurrentVariant]
                        : new Dictionary<Color, Color>(),
                    TimeEffect,
                    CurrentVariant
                );
            }
        }

        /// <summary>
        /// Forces this Tearifier to start flickering, which it does normally 
        /// every few frames or so. Flickering is simply when the CurrentVariant
        /// is changed every frame, forcing the sprite to update to a new
        /// variation of its current sprite.
        /// </summary>
        public void Flicker()
        {
            Flickering = false;
            FrameCount = FramesBeforeFlicker;
        }

        /// <summary>
        /// Returns a tearified version of a sprite. Kind of expensive if the 
        /// sprite has not been cached. If the sprite has been cached, it will
        /// return a tearified sprite from the cache which in much faster.
        /// </summary>
        /// <param name="original">The original sprite to be tearified.</param>
        /// <param name="colorMap">A color map to use. A color map is an association between two colors.</param>
        /// <param name="timeEffect">The time effect to generate a color scheme based on.</param>
        /// <param name="variant">Each sprite has 0 to AmountOfVariants variants. This is that index.</param>
        /// <returns>The new tearified sprite.</returns>
        public static Sprite GetTearifiedSprite(Sprite original, Dictionary<Color, Color> colorMap, TimeEffect timeEffect, int variant)
        {
            if (TearifiedSprites.ContainsKey(original)) // If this is tearifying an already tearified sprite...
            {
                original = TearifiedSprites[original];  // Get the original sprite, and tearify that instead.
            }

            if (SpriteCache.ContainsKey(original))                          // If a sprite has already been cached...
            {
                if (SpriteCache[original].ContainsKey(timeEffect))          // With this time effect...
                {
                    if (SpriteCache[original][timeEffect] != null)          // And the specified variant...
                    {
                        if (SpriteCache[original][timeEffect][variant] != null)
                        {
                            return SpriteCache[original][timeEffect][variant];  // Use that one
                        }
                    }
                }
            }

            try
            {
                int rectX = Mathf.FloorToInt(original.rect.x);
                int rectY = Mathf.FloorToInt(original.rect.y);
                int width = Mathf.FloorToInt(original.rect.width);
                int height = Mathf.FloorToInt(original.rect.height);
                Texture2D texture = new Texture2D(width, height);
                Color[] pixels = original.texture.GetPixels(rectX, rectY, width, height);

                // Loop over every pixel...
                for (int i = 0; i < pixels.Length; i++)
                {
                    // If it isn't transparent...
                    if (pixels[i].a > 0.1f && pixels[i].r > 0.1f && pixels[i].g > 0.1f && pixels[i].b > 0.1f)
                    {
                        // If this color hasn't been used before...
                        if (!colorMap.ContainsKey(pixels[i]))
                        {
                            // If this is using any time effect that isn't fast-forward...
                            if (timeEffect != TimeEffect.None && timeEffect != TimeEffect.FastForward)
                            {
                                // Associate this with a color based on the time effect...
                                Color normColor = TimeOperator.EffectColors[timeEffect];
                                Color newColor;
                                switch (timeEffect) // Generate a new shade of the color
                                {
                                    case TimeEffect.Pause:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b
                                        );

                                        break;
                                    case TimeEffect.Slow:
                                        newColor = new Color
                                        (
                                            normColor.r,
                                            normColor.g,
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Rewind:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g,
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Record:
                                        newColor = new Color
                                        (
                                            normColor.r,
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Reset:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(nameof(timeEffect), timeEffect, null);
                                }

                                colorMap.Add(pixels[i], newColor);
                            }
                            else // Otherwise...
                            {
                                // Associate this color with a random color based on the current color index
                                colorMap.Add(pixels[i], TimeOperator.EffectColors[(TimeEffect)ColorIndex]);
                                if (++ColorIndex > 4)
                                {
                                    ColorIndex = 0;
                                }
                            }
                        }

                        // Apply the color to the pixel based on the color map
                        pixels[i] = colorMap[pixels[i]];
                    }
                }

                texture.filterMode = FilterMode.Point;  // Everything in the game has this
                texture.SetPixels(pixels);              // Apply the new pixels
                texture.Apply();

                // Sets the correct pivot point based on the original sprite
                Bounds bounds = original.bounds;
                var pivotX = -bounds.center.x / bounds.extents.x / 2 + 0.5f;
                var pivotY = -bounds.center.y / bounds.extents.y / 2 + 0.5f;

                Sprite newSprite = Sprite.Create // Create the new sprite
                (
                    texture,
                    new Rect(0, 0, width, height),
                    new Vector2(pivotX, pivotY),
                    32
                );

                newSprite.name = $"{original} Tearified with {timeEffect} Variant {variant}";
                TearifiedSprites.Add(newSprite, original);
                if (ColorMaps.ContainsKey(timeEffect))
                {
                    if (ColorMaps[timeEffect].ContainsKey(variant))
                    {
                        ColorMaps[timeEffect][variant] = colorMap;
                    }
                    else
                    {
                        ColorMaps[timeEffect].Add(variant, colorMap);
                    }
                }
                else
                {
                    ColorMaps.Add(timeEffect, new Dictionary<int, Dictionary<Color, Color>> {{variant, colorMap}});
                }

                if (SpriteCache.ContainsKey(original))  // If the cache already contains the original...
                {
                    if (SpriteCache[original].ContainsKey(timeEffect)) // And the time effect...
                    {
                        if (SpriteCache[original][timeEffect] != null)
                        {
                            SpriteCache[original][timeEffect][variant] = newSprite;
                        }
                    }
                    else
                    {
                        SpriteCache[original].Add(timeEffect, new Sprite[AmountOfVariants]);   // Add a new map to the dictionary
                        SpriteCache[original][timeEffect][variant] = newSprite;
                    }
                }
                else
                {
                    // Otherwise add the original to the cache with a new dictionary 
                    SpriteCache.Add(original, new Dictionary<TimeEffect, Sprite[]> {{timeEffect, new Sprite[AmountOfVariants]}});
                    SpriteCache[original][timeEffect][variant] = newSprite;
                }

                return newSprite;
            }
            catch (UnityException e)
            {
                // 99 to 100% of the time, this is going to be the ReadWriteNotEnabledException
                Debug.LogWarning($"Could not draw Tearified sprite for {original.name}. Make sure read/write is enabled.{Environment.NewLine}{e}");

                return original;
            }
        }
    }
}
