﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.Level;
using Worldline.Utilities;
using Worldline.Visuals.Transitions;
using MEC;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Worldline.Visuals
{
    /// <summary>
    /// Allows for the transition between scenes using an effect, such as
    /// fading between two scenes, and should always be used over
    /// SceneManager.LoadScene(). This is not used to transition between
    /// levels, as it cleans up all scene contents, including protected
    /// GameObjects inside of DontDestroyOnLoad. Use LevelLoader to change
    /// between levels instead.
    /// </summary>
    public class SceneTransitionOperator : MonoBehaviour
    {
        public static SceneTransitionOperator Main;
        public Transition Transition;
        public bool Transitioning { get; protected set; }
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual IEnumerator<float> DoTransition(string scene, bool onlyTransitionEnd)
        {
            Transitioning = true;
            Transition = Instantiate(Transition);

            if (!onlyTransitionEnd)
            {
                Transition.StartTransition();
                yield return Timing.WaitUntilDone(new WaitUntil(() => Transition.SafeToLoad));
            }
            
            if (LevelController.Main != null)
            {
                foreach (Transform tilemap in LevelController.Main.transform)    // Disables entities, NPCs, ect, so they stop running their scripts
                {
                    foreach (Transform obj in tilemap.transform)
                    {
                        obj.gameObject.SetActive(false);
                    }
                }
            }

            yield return Timing.WaitForOneFrame;
            ExtensionMethods.DestroyOnSceneLoads.RemoveAll(n => n == null);
            foreach (GameObject obj in ExtensionMethods.DestroyOnSceneLoads)
            {
                Destroy(obj);
            }

            ExtensionMethods.DestroyOnSceneLoads = new List<GameObject>();
            yield return Timing.WaitUntilDone(SceneManager.LoadSceneAsync(scene));
            Transition.EndTransition();
            PersistentCanvasController.Main.ShowLoadingText = false;
        }

        public void LoadScene(string scene)
        {
            if (Transitioning)
            {
                throw new Exception("Can not start new transition when currently transitioning.");
            }

            Timing.RunCoroutine(DoTransition(scene, false), Segment.FixedUpdate);
        }

        public void LoadSceneTransitionEnd(string scene)
        {
            if (Transitioning)
            {
                throw new Exception("Can not start new transition when currently transitioning.");
            }

            Timing.RunCoroutine(DoTransition(scene, true), Segment.FixedUpdate);
        }
    }
}
