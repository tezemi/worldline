﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(MovementOperator))]
    [RequireComponent(typeof(VisualTimeOperator))]
    public class Confetti : MonoBehaviour, ICanRessurect
    {
        public float DeathTime = 4f;
        public float CurrentHorizontalSpeed = 1.5f;
        public Sprite[] ConfettiSprites;
        public Color[] ConfettiColors;
        public bool Dead { get; set; }
        public bool Resurrected { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        protected VisualTimeOperator VisualTimeOperator { get; set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            MovementOperator = GetComponent<MovementOperator>();
            VisualTimeOperator = GetComponent<VisualTimeOperator>();
            SpriteRenderer.sprite = ConfettiSprites[Random.Range(0, ConfettiSprites.Length)];
            SpriteRenderer.color = ConfettiColors[Random.Range(0, ConfettiColors.Length)];
            SpriteRenderer.flipX = Random.Range(0, 2) == 0;
            SpriteRenderer.flipY = Random.Range(0, 2) == 0;
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Move(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(Destroy(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        private IEnumerator<float> Move()
        {
            start:
            MovementOperator.Speed = new Vector2(CurrentHorizontalSpeed = -CurrentHorizontalSpeed, MovementOperator.Speed.y);
            yield return this.WaitForFixedSecondsScaled(0.5f);
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        private IEnumerator<float> Destroy()
        {
            yield return this.WaitForFixedSecondsScaled(DeathTime);
            startFadeOut:
            yield return this.WaitForFixedSecondsScaled(0.02f);
            GetComponent<SpriteRenderer>().color -= new Color(0f, 0f, 0f, 0.02f);
            if (isActiveAndEnabled && SpriteRenderer.color.a > 0f)
            {
                goto startFadeOut;
            }

            Die();
        }


        public void Die()
        {
            Dead = true;
            gameObject.SetActive(false);
            DeathTimeOperator.Handle(gameObject, CombatAlignment.Neutral, true);
        }

        public void Resurrect()
        {
            Dead = false;
            Resurrected = true;
            gameObject.SetActive(true);
            Timing.RunCoroutine(DisableInvincibility(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> DisableInvincibility()
            {
                yield return Timing.WaitForSeconds(TimeOperator.StandardResurrectionInvincibilityTime);
                Resurrected = false;
            }
        }
    }
}
