﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// Currently designed for use by Tears, this is a component that creates
	/// a death animation for a GameObject. By calling the static method Collapse()
	/// with a Sprite and position, you can create a death animation using that
	/// Sprite at that position in the world.
    /// </summary>
    public class CollapseAnimation : MonoBehaviour, ICanRessurect, IPoolable
    {
        public const string PoolName = "CollapseAnimationPool";
        public bool Resurrected { get; set; }
        public bool Dead { get; set; }
        public CombatAlignment CombatAlignment { get; set; }
        public bool RunDieMethodOnOutOfMemory { get; } = false;
        private float _delay;
        private float _delayRampDown;
        private float _shakeIntensity;
        
        /// <summary>
        /// Implementation from IResurrectable, sets the GameObject to inactive,
        /// creates a DeathTimeOperator for the GameObject, and adds it to a
        /// pool of Collapsers with a buffer.
        /// </summary>
        public void Die()
        {
            gameObject.SetActive(false);
            DeathTimeOperator.Handle(gameObject, CombatAlignment, false);
            Pool.Add(gameObject, PoolName, true);
        }

        /// <summary>
        /// Implementation from IResurrectable, gets called if the DeathTimeOperator
        /// brings the GameObject back to life. Sets the GameObject to active, and
        /// then waits until the CollapseAnimation is done rewinding before allowing it to
        /// die again by setting Resurrected to false. Restarts the animation.
        /// </summary>
        public void Resurrect()
        {
            Dead = false;
            Resurrected = true;
            gameObject.SetActive(true);
            Timing.RunCoroutine(Wait(), Segment.Update, GetInstanceID().ToString());

            IEnumerator<float> Wait()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() => GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.None));
                Resurrected = false;
                StartCoroutine(Animate(_delay, _delayRampDown, _shakeIntensity, 0));
            }
        }

        public void Reuse()
        {
            Resurrected = false;
            Dead = false;
            _delay = 0;
            _delayRampDown = 0;
            _shakeIntensity = 0;
        }

        private IEnumerator<float> Animate(float delay = 0.05f, float delayRampDown = 1000f, float shakeIntensity = 0.05f, int skipTicks = 10)
        {
            _delay = delay;
            _delayRampDown = delayRampDown;
            _shakeIntensity = shakeIntensity;
            Vector3 startPosition = transform.position;
            Sprite startSprite = GetComponent<SpriteRenderer>().sprite;
            Texture2D texture2D = new Texture2D((int)startSprite.rect.width, (int)startSprite.rect.height);
            texture2D.filterMode = FilterMode.Point;
            texture2D.SetPixels(startSprite.texture.GetPixels((int)startSprite.textureRect.x, (int)startSprite.textureRect.y, (int)startSprite.textureRect.width, (int)startSprite.textureRect.height));
            int yTop = 0;
            int yBottom = texture2D.height;
            int ticks = 0;

            start:
            yield return this.WaitForFixedSecondsScaled(delay - ticks / delayRampDown);
            if (ticks >= skipTicks)
            {
                for (int x = 0; x < texture2D.width; x++)
                {
                    texture2D.SetPixel(x, yTop, Color.clear);
                    texture2D.SetPixel(x, yBottom, Color.clear);
                }

                yTop++;
                yBottom--;
                texture2D.Apply();
                Texture2D textureCopy = new Texture2D(texture2D.width, texture2D.height);
                Graphics.CopyTexture(texture2D, textureCopy);
                textureCopy.filterMode = FilterMode.Point;
                textureCopy.Apply();
                Bounds bounds = startSprite.bounds;
                var pivotX = -bounds.center.x / bounds.extents.x / 2 + 0.5f;
                var pivotY = -bounds.center.y / bounds.extents.y / 2 + 0.5f;
                Sprite newSprite = Sprite.Create
                (
                    textureCopy,
                    new Rect(0, 0, texture2D.width, texture2D.height),
                    new Vector2(pivotX, pivotY),
                    32
                );

                newSprite.name = "Collapsed " + ticks;
                GetComponent<SpriteRenderer>().sprite = newSprite;
            }

            transform.position = startPosition + new Vector3(Random.Range(-shakeIntensity, shakeIntensity), 0f, 0f);
            ticks++;
            if (yTop <= yBottom)
            {
                goto start;
            }

            Die();
        }

        /// <summary>
        /// Creates a death animation using the Sprite at the position in the
        /// world. With a faction provided, this animation can be effected by
        /// the player's or enemies' time effects. The last few parameters
        /// manipulate animation speed.
        /// </summary>
        /// <param name="sprite">The Sprite to create the animation with.</param>
        /// <param name="position">The position in the world the animation will occur.</param>
        /// <param name="combatAlignment">The faction of the animation for time effect purposes.</param>
        /// <param name="delay">The starting delay of the animation.</param>
        /// <param name="delayRampDown">How much to decrease the animation speed each tick. Set to zero for same speed.</param>
        /// <param name="shakeIntensity">The intensity of the shake in world units.</param>  
        /// <param name="skipTicks">The amount of ticks before a new sprite is created.</param>
        /// <returns></returns>
        public static GameObject Collapse(Sprite sprite, Vector3 position, CombatAlignment combatAlignment, float delay = 0.05f, float delayRampDown = 1000f, float shakeIntensity = 0.05f, int skipTicks = 10)
        {
            GameObject collapser;
            if (Pool.Has(PoolName))
            {
                collapser = Pool.Get(PoolName);
            }
            else
            {
                collapser = new GameObject("CollapseAnimation", typeof(SpriteRenderer), typeof(CollapseAnimation), typeof(VisualTimeOperator), typeof(Recorder));
            }

            collapser.SetActive(true);
            collapser.GetComponent<SpriteRenderer>().sprite = sprite;
            collapser.transform.position = position;
            collapser.GetComponent<Recorder>().SnapshotType = typeof(VisualSnapshot);
            collapser.GetComponent<CollapseAnimation>().CombatAlignment = combatAlignment;
            Timing.RunCoroutine(collapser.GetComponent<CollapseAnimation>().Animate
            (
                delay, delayRampDown, shakeIntensity, skipTicks), 
                Segment.FixedUpdate,
                collapser.GetInstanceID().ToString()
            );

            return collapser;
        }
    }
}
