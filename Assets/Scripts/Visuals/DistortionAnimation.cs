﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// A component that will create a distortion effect when attached to
    /// a GameObject with a SpriteRenderer.
    /// </summary>
    public class DistortionAnimation : MonoBehaviour
    {
        public float Intensity = 0.5f;
        public float MinDelay = 0.05f;
        public float MaxDelay = 0.15f;
        public List<GameObject> Distortions { get; protected set; } = new List<GameObject>();
        public const string DistortionPool = "DistortionAnimationPool";
        protected SpriteRenderer SpriteRenderer { get; set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Distort(), Segment.FixedUpdate);
        }

        protected virtual void Update()
        {
            Distortions.RemoveAll(n => n == null);
        }

        protected virtual void OnDisable()
        {
            foreach (GameObject distortion in Distortions)
            {
                if (distortion == null || distortion.GetComponent<SpriteRenderer>() == null)
                {
                    continue;
                }

                Timing.RunCoroutine(FadeOut(distortion.GetComponent<SpriteRenderer>()), Segment.FixedUpdate);
            }
        }

        /// <summary>
        /// The main tick for this component. Runs every random seconds between
        /// MinDelay and MaxDelay. Creates a GameObject that copies the Sprite
        /// currently on the SpriteRenderer, and applies some offset as well
        /// as fade. Then calls a coroutine which fades the sprite out, and pools
        /// it.
        /// </summary>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> Distort()
        {
            start:
            yield return Timing.WaitForSeconds(Random.Range(MinDelay, MaxDelay));
            if (this == null) yield break;

            GameObject distortSpriteGameObject; 
            if (Pool.Has(DistortionPool))
            {
                distortSpriteGameObject = Pool.Get(DistortionPool);
            }
            else
            {
                distortSpriteGameObject = new GameObject("Distortion", typeof(SpriteRenderer)) { hideFlags = HideFlags.HideInHierarchy };
            }

            distortSpriteGameObject.SetActive(true);
            distortSpriteGameObject.transform.localScale = transform.localScale;
            distortSpriteGameObject.transform.rotation = transform.rotation;
            distortSpriteGameObject.GetComponent<SpriteRenderer>().sprite = SpriteRenderer.sprite;
            distortSpriteGameObject.GetComponent<SpriteRenderer>().flipX = SpriteRenderer.flipX;
            distortSpriteGameObject.GetComponent<SpriteRenderer>().flipY = SpriteRenderer.flipY;
            distortSpriteGameObject.GetComponent<SpriteRenderer>().color = SpriteRenderer.color;
            distortSpriteGameObject.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, Random.Range(0.25f, 0.75f));
            distortSpriteGameObject.transform.position = transform.position + new Vector3(Random.Range(-Intensity, Intensity), Random.Range(-Intensity, Intensity), 0.15f);
            Distortions.Add(distortSpriteGameObject);
            Timing.RunCoroutine(FadeOut(distortSpriteGameObject.GetComponent<SpriteRenderer>()), Segment.FixedUpdate);
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        /// <summary>
        /// Fades the provided Sprite on the SpriteRenderer out and then pools
        /// the GameObject.
        /// </summary>
        /// <param name="spriteRenderer">The SpriteRenderer to fade out.</param>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> FadeOut(SpriteRenderer spriteRenderer)
        {
            bool on = true;
            start:
            yield return Timing.WaitForOneFrame;
            if (spriteRenderer == null)
            {
                yield break;
            }

            spriteRenderer.color += new Color(0, 0, 0, on ? -0.04f : 0.02f);
            if (spriteRenderer.color.a <= 0)
            {
                spriteRenderer.gameObject.SetActive(false);
                Pool.Add(spriteRenderer.gameObject, DistortionPool, false);
                yield break;
            }

            on = !on;
            goto start;
        }
    }
}
