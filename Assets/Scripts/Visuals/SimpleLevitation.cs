﻿// Copyright (c) 2019 Destin Hebner
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// When attached to a GameObject will cause the GameObject to float up 
    /// and down using a specified speed and distance. Optionally, this can
    /// work with time effects if a TimeOperator is attached.
    /// TODO: Some time effects look wonky.
    /// </summary>
    public class SimpleLevitation : MonoBehaviour
    {
        public float Speed = 1.5f;
        public float Distance = 0.25f;
        public Vector3 StartPosition { get; protected set; }
        private float _time;

        protected virtual void Awake()
        {
            StartPosition = transform.position;
        }

        protected virtual void FixedUpdate()
        {
            float finalSpeed = Speed;
            if (GetComponent<TimeOperator>() != null)
            {
                if (GetComponent<TimeOperator>().ShouldHold) return;

                if (GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Slow)
                {
                    finalSpeed = Speed * TimeOperator.SlowScale;
                }
                else if (GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.FastForward)
                {
                    finalSpeed = Speed * TimeOperator.FastForwardScale;
                }
            }

            transform.position = new Vector3
            (
                transform.position.x, 
                StartPosition.y + Mathf.Sin(_time * finalSpeed) * Distance,
                transform.position.z
            );

            _time += Time.fixedDeltaTime;
        }
    }
}
