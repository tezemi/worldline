﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// When attached to a GameObject, this will create a trail of sprites
    /// at the GameObject's position that fade out at a provided rate. This
    /// can be used to create a fun effect for tears or time engine effects,
    /// and can also be used to make something look like it's moving really
    /// fast.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class TrailAnimation : MonoBehaviour
    {
        public float Length = 0.005f;
        public float FadeRate = 0.05f;
        public const string PoolName = "TrailAnimationPool";
        protected  SpriteRenderer SpriteRenderer { get; set; }
        protected List<SpriteRenderer> Trails { get; set; } = new List<SpriteRenderer>();
        private Vector3? _positionLastTick;

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            _positionLastTick = null;
            Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> Tick()
        {
            start:
            if (_positionLastTick != null)
            {
                GameObject fadeGameObject;
                if (Pool.Has(PoolName))
                {
                    fadeGameObject = Pool.Get(PoolName);
                }
                else
                {
                    fadeGameObject = new GameObject("Fade Game Object", typeof(SpriteRenderer));
                }

                fadeGameObject.SetActive(true);
                SpriteRenderer fadeRenderer = fadeGameObject.GetComponent<SpriteRenderer>();
                fadeRenderer.sprite = SpriteRenderer.sprite;
                fadeRenderer.flipX = SpriteRenderer.flipX;
                fadeRenderer.flipY = SpriteRenderer.flipY;
                fadeRenderer.color = SpriteRenderer.color;
                fadeRenderer.transform.position = _positionLastTick.Value + new Vector3(0f, 0f, 0.15f);
                fadeRenderer.transform.rotation = transform.rotation;
                fadeRenderer.transform.localScale = transform.localScale;
                Timing.RunCoroutine(Fade(fadeRenderer), Segment.FixedUpdate, GetInstanceID().ToString());
                Trails.Add(fadeRenderer);
            }

            _positionLastTick = transform.position;
            yield return Timing.WaitForSeconds(Length);
            if (this != null && isActiveAndEnabled)
            {
                goto start;
            }
        }

        protected virtual IEnumerator<float> Fade(SpriteRenderer fadeRenderer)
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (fadeRenderer == null)
            {
                yield break;
            }

            fadeRenderer.color -= new Color(0f, 0f, 0f, FadeRate);
            if (fadeRenderer.color.a > 0f)
            {
                goto start;
            }

            Trails.Remove(fadeRenderer);
            fadeRenderer.gameObject.SetActive(true);
            Pool.Add(fadeRenderer.gameObject, PoolName, false);
        }

        public static TrailAnimation Create(GameObject gameObject, float length = 0.01f, float fadeRate = 0.05f)
        {
            TrailAnimation t = gameObject.AddComponent<TrailAnimation>();
            t.Length = length;
            t.FadeRate = fadeRate;

            return t;
        }
    }
}
