// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.GameState;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Visuals
{
    /// <summary>
    /// A singleton class responsible for controlling the game's active camera
    /// and giving it game functionality, as well as providing camera-related
    /// information.
    /// </summary>
    public class CameraOperator : MonoBehaviour
    {
        public static CameraOperator ActiveCamera { get; set; }
        public bool DisableLeftBound;
        public bool DisableRightBound;
        public bool DisableTopBound;
        public bool DisableBottomBound;
        public Sprite Fader;
        public SpriteRenderer Overlay;
        public CameraMode Mode = CameraMode.Lock;
        public GameObject Track;
        public bool DisableBounds { get; set; }
        public float LerpSpeed { get; set; } = 0.08f;
        public float HorizontalOffset { get; set; } = 2;
        public Vector3 Focus { get; set; }
        public bool FadedOut { get; protected set; }
        public float Hoffset { get; protected set; }
        public float Voffset { get;  protected set; }
        public Camera CameraComponent { get; private set; }
        public float Speed { get; set; } = 1.25f;
        public const float CameraBoundsScale = 1.78f;
        public const float VerticalOnScreenPadding = 0.10f;
        public const float HorizontalOnScreenPadding = 0f;
        protected Direction? Facing;
        protected const float Scale = 128;
        protected const float UpperOffset = 1.25f;
        protected const float LowerOffset = 1.25f;
        protected const float OffsetSpeed = .035f;
        protected const float VerticalOffsetRange = 6f;
        private bool _disableHorizontalOffset;
        private GameObject _fader;
        private Vector3 _offset;
        
        public bool DisableHorizontalOffset
        {
            get => _disableHorizontalOffset;
            set
            {
                if (value)
                {
                    Facing = null;
                }

                _disableHorizontalOffset = value;
            }
        }

        protected virtual void Awake()
		{
            if (ActiveCamera == null)
            {
                ActiveCamera = this;    
            }

            CameraComponent = GetComponent<Camera>();
		    _fader = new GameObject("Fader", typeof(SpriteRenderer));
		    _fader.GetComponent<SpriteRenderer>().sprite = Fader;
		    _fader.transform.localScale = new Vector3(15, 15, 1);
		    _fader.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        }

        protected virtual void OnEnable()
        {
            if (CheatController.IsActive(Cheat.Thirdperson))
            {
                CameraComponent.orthographic = false;
            }
        }
        
        protected virtual void Update()
        {
            // Offset Horizontal
            if (Track != null && Track.GetComponent<MainPlayerOperator>() != null)
            {
                if (PlayerWeaponOperator.Main.isActiveAndEnabled)
                {
                    Vector2 mousePos = CameraComponent.ScreenToWorldPoint(Input.mousePosition);
                    Facing = mousePos.x < MainPlayerOperator.MainPlayerComponent.transform.position.x ? Direction.Left : Direction.Right;
                    Voffset = Mathf.Lerp(Voffset, (mousePos.y - MainPlayerOperator.MainPlayerObject.transform.position.y) / VerticalOffsetRange, OffsetSpeed);
                    if (Voffset > UpperOffset)
                    {
                        Voffset = UpperOffset;
                    }

                    if (Voffset < -LowerOffset)
                    {
                        Voffset = -LowerOffset;
                    }
                }
                else
                {
                    if (InputManager.GetButtonDown("right"))
                    {
                        Facing = Direction.Right;
                    }
                    else if (InputManager.GetButtonDown("left"))
                    {
                        Facing = Direction.Left;
                    }
                    else if (InputManager.GetButtonDown("right") && InputManager.GetButtonDown("left"))
                    {
                        Facing = null;
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
		{
            // Fader
            if (_fader != null) _fader.transform.position = transform.position + new Vector3(0, 0, .5f);

            // Tracking | Set Track to null if you want to track a vector.
		    if (Mode != CameraMode.Static)
		    {
		        if (Track != null && Facing == null)
		        {
		            Hoffset = Mathf.Lerp(Hoffset, 0f, OffsetSpeed);
		            Focus = Track.transform.position + new Vector3(Hoffset, Voffset);
                }
		        else if (Track != null && Facing == Direction.Right && !_disableHorizontalOffset)
		        {
		            Hoffset = Mathf.Lerp(Hoffset, _disableHorizontalOffset ? 0f : HorizontalOffset, OffsetSpeed);
		            Focus = Track.transform.position + new Vector3(Hoffset, Voffset);
		        }
		        else if (Track != null && Facing == Direction.Left && !_disableHorizontalOffset)
		        {
		            Hoffset = Mathf.Lerp(Hoffset, _disableHorizontalOffset ? 0f : -HorizontalOffset, OffsetSpeed);
		            Focus = Track.transform.position + new Vector3(Hoffset, Voffset);
		        }
		    }

		    // Camera Mode
            switch (Mode)
            {
                case CameraMode.Float:
                    if (transform.position != Focus)
                    {
                        transform.position = new Vector3(Vector3.MoveTowards(transform.position, Focus, Speed).x, Vector3.MoveTowards(transform.position, Focus, Speed).y, transform.position.z) + _offset;
                    }
                    break;
                case CameraMode.Lock:
                    if (transform.position != Focus)
                    {
                        transform.position = new Vector3(Focus.x, Focus.y, transform.position.z) + _offset;
                    }

                    break;
                case CameraMode.FloatVertical:
                    transform.position = new Vector3(Focus.x, transform.position.y, transform.position.z) + _offset;
                    if (Math.Abs(transform.position.y - Focus.y) > .3f)
                    {
                        transform.position = new Vector3(Focus.x, Vector3.MoveTowards(transform.position, Focus, Speed).y, transform.position.z) + _offset;
                    }
                    break;
                case CameraMode.Lerp:
                    transform.position = Vector3.Lerp(transform.position, new Vector3(Focus.x, Focus.y, transform.position.z), LerpSpeed) + _offset;
                    break;
                case CameraMode.Static:
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // Enable/Disable Cameras
            CameraComponent.enabled = this == ActiveCamera;

            AdjustWithLevelSize();
        }

        /// <summary>
        /// Adjusts the camera's position if the camera's edges lie outside of
        /// the level bounds. This gets called every frame in the Update() method.
        /// </summary>
        public void AdjustWithLevelSize()
        {
            if (DisableBounds)
            {
                return;
            }

            // Horizontal
            float width = 2f * CameraComponent.orthographicSize * CameraComponent.aspect;
            if (!DisableRightBound && transform.position.x >= LevelController.Main.MapWidthMax - width / 2f)
            {
                transform.position = new Vector3(LevelController.Main.MapWidthMax - width / 2f - Mathf.Abs(_offset.x), transform.position.y, transform.position.z);
            }
            else if (!DisableLeftBound && transform.position.x <= LevelController.Main.MapWidthMin + width / 2f)
            {
                transform.position = new Vector3(LevelController.Main.MapWidthMin + width / 2f + Mathf.Abs(_offset.x), transform.position.y, transform.position.z);
            }

            // Vertical
            if (!DisableTopBound && transform.position.y >= LevelController.Main.MapHeightMax - CameraComponent.orthographicSize)
            {
                transform.position = new Vector3(transform.position.x, LevelController.Main.MapHeightMax - CameraComponent.orthographicSize - Mathf.Abs(_offset.y), transform.position.z);
            }
            else if (!DisableBottomBound && transform.position.y <= LevelController.Main.MapHeightMin + CameraComponent.orthographicSize)
            {
                transform.position = new Vector3(transform.position.x, LevelController.Main.MapHeightMin + CameraComponent.orthographicSize + Mathf.Abs(_offset.x), transform.position.z) ;
            }
        }

        /// <summary>
        /// Causes the camera to fade into and fade out of white using the specific
        /// delay to determine the speed of the effect. Gets used during the save
        /// point event.
        /// </summary>
        /// <param name="delay">The speed of the effect.</param>
        public void Fade(float delay)
        {
            if (!FadedOut)
            {
                Timing.RunCoroutine(DoFade(), Segment.FixedUpdate, GetInstanceID().ToString());

                IEnumerator<float> DoFade()
                {
                    start:
                    yield return Timing.WaitForSeconds(0.02f);
                    _fader.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, _fader.GetComponent<SpriteRenderer>().color.a + .01f);
                    if (Math.Abs(_fader.GetComponent<SpriteRenderer>().color.a - 1) < .01f)
                    {
                        FadedOut = true;
                    }
                    else
                    {
                        goto start;
                    }
                }
            }
            else
            {
                Timing.RunCoroutine(DoFade(), Segment.FixedUpdate, GetInstanceID().ToString());

                IEnumerator<float> DoFade()
                {
                    start:
                    yield return Timing.WaitForSeconds(0.02f);
                    _fader.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, _fader.GetComponent<SpriteRenderer>().color.a - .01f);
                    if (Math.Abs(_fader.GetComponent<SpriteRenderer>().color.a) < .01f)
                    {
                        FadedOut = false;
                    }
                    else
                    {
                        goto start;
                    }
                }
            }
        }

        /// <summary>
        /// Returns true if a game object is within the camera's bounds.
        /// </summary>
        /// <param name="gameObjectInWorld">The game object to check.</param>
        /// <returns>Whether or not the game object is on the screen.</returns>
        public bool OnScreen(GameObject gameObjectInWorld)
        {
            return gameObjectInWorld != null && OnScreen(gameObjectInWorld.transform.position);
        }

        /// <summary>
        /// Returns true if a point is within the camera's bounds.
        /// </summary>
        /// <param name="point">The point to check.</param>
        /// <returns>Whether or not the point is on the screen.</returns>
        public bool OnScreen(Vector3 point)
        {
            if (CameraComponent == null)
            {
                return false;
            }

            Vector2 viewportPoint = CameraComponent.WorldToViewportPoint(point);
            return viewportPoint.x > -HorizontalOnScreenPadding && viewportPoint.x < 1f + HorizontalOnScreenPadding && viewportPoint.y > -VerticalOnScreenPadding && viewportPoint.y < 1f + VerticalOnScreenPadding;
        }

        /// <summary>
        /// Causes the camera to perform a shaking effect.
        /// </summary>
        /// <param name="intensity">The maximum units the camera can travel during the shake.</param>
        /// <param name="delay">The delay between each time the camera changes position.</param>
        /// <param name="time">The amount of time to shake the camera for.</param>
        public void Shake(float intensity, float delay, float time)
        {
            float actualTime = 0f;
            Timing.RunCoroutine(DoShake(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> DoShake()
            {
                while (actualTime < time)
                {
                    yield return Timing.WaitForSeconds(delay);
                    _offset = new Vector3(Random.Range(-intensity, intensity), Random.Range(-intensity, intensity), 0f);
                    actualTime += delay;
                }

                _offset = Vector3.zero;
            }
        }
	}
}
