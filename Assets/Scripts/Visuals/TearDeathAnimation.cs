﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Utilities;
using Worldline.Corruption.SpriteCorruption;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// Performs an animation for a specified GameObject using the Play method 
    /// that gives it the appearance of dying using a corrupted animation. 
    /// Generating the extra sprites used during the animation is kind of 
    /// expensive, so death sprites should be cached during load times in order
    /// to avoid generating them on-the-fly. A warning will be shown if an
    /// animation is played for an un-cached sprite.
    /// TODO: Cleanup
    /// </summary>
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(VisualTimeOperator))]
    public class TearDeathAnimation : MonoBehaviour, IPoolable, ICanRessurect, IHasAlignment
    {
        public static Dictionary<Sprite, Sprite[]> SpriteCache { get; protected set; } = new Dictionary<Sprite, Sprite[]>();
        public int AnimationIndex { get; set; }
        public CombatAlignment Alignment { get; set; }
        public const int AmountOfVariants = 8;
        public const float FlickerDelay = 0.1f;
        public const string PoolName = "TearDeathAnimationPool";
        private Sprite _deathSprite;
        private GameObject _objPlayingFor;

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        public void Reuse()
        {
            transform.localScale = new Vector3(1f, 1f, 1f);
            GetComponent<Recorder>().ResetRecorder();
            AnimationIndex = 0;
        }

        public void Resurrect()
        {
            AnimationIndex = 0;
            gameObject.SetActive(true);
            Timing.RunCoroutine
            (
                Flicker(),
                Segment.FixedUpdate,
                GetInstanceID().ToString()
            );

            Timing.RunCoroutine
            (
                Shake(),
                Segment.FixedUpdate,
                GetInstanceID().ToString()
            );
        }

        protected virtual IEnumerator<float> Flicker()
        {
            for (; AnimationIndex < AmountOfVariants; AnimationIndex++)
            {
                yield return this.WaitForFixedSecondsScaled(FlickerDelay);
                if (!SpriteCache.ContainsKey(_deathSprite))
                {
                    Debug.LogWarning($"{_deathSprite.name} was not cached. I bet that felt good.");
                    AddSpriteToCache(_deathSprite);
                }

                if (_objPlayingFor != null && _objPlayingFor.activeSelf)
                {
                    break;
                }

                GetComponent<SpriteRenderer>().sprite = SpriteCache[_deathSprite][AnimationIndex];
            }

            gameObject.SetActive(false);
            Pool.Add(gameObject, PoolName, true);
            DeathTimeOperator.Handle(gameObject, Alignment, false);
        }

        protected virtual IEnumerator<float> Shake()
        {
            bool shakeLeft = true;
            start:
            transform.position += new Vector3(shakeLeft ? 0.075f : -0.075f, 0, 0);
            shakeLeft = !shakeLeft;
            yield return this.WaitForFixedSecondsScaled(Time.fixedDeltaTime);
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        /// <summary>
        /// Adds the variants of sprites to the cache. This is expensive and 
        /// should be done during loading times.
        /// </summary>
        /// <param name="sprite">The sprite that needs to be cached.</param>
        /// <param name="force">If a sprite has already been cached, and this 
        /// is true, it will be replaced. Good if you want to generate some
        /// new sprites for a animation during a  level load.</param>
        public static void AddSpriteToCache(Sprite sprite, bool force = false)
        {
            if (SpriteCache.ContainsKey(sprite) && !force) return;

            Sprite[] variants = new Sprite[AmountOfVariants];
            SpriteCorruptionTool spriteCorruptionTool = new SpriteCorruptionTool();
            spriteCorruptionTool.Options[SpriteCorruptionType.ClownVomit] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.25f,
                Intensity = Random.Range(0f, 0.75f),
                AffectsTransparency = Random.value < 0.5f,
                PercentageOfSectors = Random.Range(0f, 1f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.Invert] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.75f,
                PercentageOfSectors = Random.Range(0.5f, 1f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.PaletteSwap] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.75f,
                PercentageOfSectors = Random.Range(0.25f, 1f),
                Priority = -1
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.Tearing] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.15f,
                PercentageOfSectors = Random.Range(0.25f, 1f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.FlipXSectors] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.5f,
                PercentageOfSectors = Random.Range(0.2f, 1f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.FlipYSectors] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.5f,
                PercentageOfSectors = Random.Range(0.2f, 1f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.BitColors] = new SpriteCorruptionOptions
            {
                //Enabled = Random.value < 0.25f,
                Enabled = true,
                PercentageOfSectors = Random.Range(0.25f, 1f),
                UseClustering = Random.value < 0.5f,
                Priority = -2
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.MoveSectors] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.5f,
                PercentageOfSectors = Random.Range(0.15f, 0.5f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.RepeatSectors] = new SpriteCorruptionOptions
            {
                Enabled = Random.value < 0.25f,
                PercentageOfSectors = Random.Range(0.15f, 0.5f)
            };

            spriteCorruptionTool.Options[SpriteCorruptionType.RemoveSectors] = new SpriteCorruptionOptions
            {
                Enabled = true,
                PercentageOfSectors = 0f,
                SectorType = SectorType.InnerWeight,
                Priority = -2
            };

            for (int i = 0; i < variants.Length; i++)
            {
                variants[i] = spriteCorruptionTool.GetCorruptedSprite(sprite);
                spriteCorruptionTool.Options[SpriteCorruptionType.RemoveSectors].PercentageOfSectors += 1f / AmountOfVariants;
            }

            if (!SpriteCache.ContainsKey(sprite))
            {
                SpriteCache.Add(sprite, variants);
            }
            else
            {
                SpriteCache[sprite] = variants;
            }
        }

        /// <summary>
        /// Plays the animation for the specified GameObject by dynamically 
        /// creating the animation object at its position.
        /// </summary>
        /// <param name="obj">The GameObject to play the animation for.</param>
        /// <param name="sprite">The sprite to be used in the animation.</param>
        /// <param name="alignment">The alignment of the animation.</param>
        /// <returns>The newly created animation component.</returns>
        public static TearDeathAnimation Play(GameObject obj, Sprite sprite, CombatAlignment alignment)
        {
            GameObject animationGameObject;
            if (Pool.Has(PoolName))
            {
                animationGameObject = Pool.Get(PoolName);
            }
            else
            {
                animationGameObject = new GameObject
                (
                    "Tear Death Animation",
                    typeof(TearDeathAnimation),
                    typeof(VisualTimeOperator),
                    typeof(Recorder)
                );
            }

            // Don't want animation to be affected by player's essence if tear is dying 
            // while being rewound, or it creates an effect where the tear dies and then
            // comes back to life instantly.
            if (obj.GetComponent<TimeOperator>() != null &&
            obj.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
            {
                animationGameObject.GetComponent<TimeOperator>().enabled = false;
            }

            animationGameObject.SetActive(true);
            animationGameObject.transform.position = obj.transform.position;
            animationGameObject.transform.localScale = obj.transform.localScale;
            animationGameObject.transform.rotation = obj.transform.rotation;
            animationGameObject.GetComponent<Recorder>().SnapshotType = typeof(TearDeathAnimationSnapshot);
            SpriteRenderer spriteRenderer = animationGameObject.GetComponent<SpriteRenderer>();
            spriteRenderer.sprite = sprite;
            spriteRenderer.flipX = obj.GetComponent<SpriteRenderer>().flipX;
            spriteRenderer.flipY = obj.GetComponent<SpriteRenderer>().flipY;
            TearDeathAnimation tearDeathAnimation = animationGameObject.GetComponent<TearDeathAnimation>();
            tearDeathAnimation._deathSprite = sprite;
            tearDeathAnimation._objPlayingFor = obj;
            tearDeathAnimation.Alignment = alignment;
            Timing.RunCoroutine
            (
                tearDeathAnimation.Flicker(), 
                Segment.FixedUpdate, 
                tearDeathAnimation.GetInstanceID().ToString()
            );

            Timing.RunCoroutine
            (
                tearDeathAnimation.Shake(),
                Segment.FixedUpdate,
                tearDeathAnimation.GetInstanceID().ToString()
            );

            return tearDeathAnimation;
        }
    }
}
