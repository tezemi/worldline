﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level.Obstacles;
using Worldline.Level;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    public class BlobStar : MonoBehaviour
    {
        public Sprite[] SpriteSetA;
        public Sprite[] SpriteSetB;
        protected SpriteRenderer SpriteRenderer { get; set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            float randomValue = Random.value;
            GetComponent<AnimationOperator>().Sprites = randomValue < 0.5 ? SpriteSetA : SpriteSetB;
            GetComponent<Rigidbody2D>().gravityScale = Random.value < 0.5f ?
            -GetComponent<Rigidbody2D>().gravityScale : GetComponent<Rigidbody2D>().gravityScale;
            Timing.RunCoroutine(Kill(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> Kill()
        {
            while (SpriteRenderer.color.a > 0f)
            {
                yield return Timing.WaitForSeconds(0.5f);
                SpriteRenderer.color -= new Color(0f, 0f, 0f, 0.25f);
            }

            Pool.Add(gameObject, EssenceBlob.PoolName, false);
            gameObject.SetActive(false);
        }
    }
}
