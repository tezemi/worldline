// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.Level;
using Worldline.Player;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// Singleton component responsible for all background related behavior.
    /// each level should have at least one BackgroundOperator game object,
    /// which is where the background and background's behavior should be set
    /// for that level. Multiple BackgroundOperators can be used to implement
    /// separate background layers, each with their own functionality.
    /// </summary>
    [Obsolete("Use BackgroundController instead.")]
    public class BackgroundOperator : MonoBehaviour 
	{
        public static BackgroundOperator Main;
        public bool StaticX;
        public bool StaticY;
        public float ScrollSpeed = 0.02f;
        public Vector3 Offset;
        public Vector3 CustomScale = new Vector3(1f, 1f, 1f);
	    public BackgroundBehavior BackgroundBehavior;
        public Sprite[] BackgroundSprites;
        public GameObject[] LoopRoll { get; set; }
	    public float BackgroundDist { get; protected set; } = 20f;
        public const float UndersizeScale = 4f;
        public const float PanLimit = 42;
        protected SpriteRenderer BackgroundRenderer { get; set; }
        protected AnimationOperator AnimationOperator { get; set; }
        private int _currentBg;
        private float? _maxPanPos;
        private float? _minPanPos;
	    private float? _staticXPos;
        private float? _staticYPos;
        private Sprite[] _oldBackgroundSprites;
        private BackgroundBehavior _oldBackgroundBehavior;

	    protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
            
            BackgroundRenderer = GetComponent<SpriteRenderer>();
            AnimationOperator = GetComponent<AnimationOperator>();
            BackgroundDist = transform.localPosition.z;
        }

	    protected virtual void Start()
        {
            if (CameraOperator.ActiveCamera != null)
            {
                transform.SetParent(CameraOperator.ActiveCamera.transform);
            }

            _oldBackgroundSprites = BackgroundSprites;
            _oldBackgroundBehavior = BackgroundBehavior;
            UpdateBackgroundSprites(_oldBackgroundSprites);
            UpdateBackgroundBehavior(_oldBackgroundBehavior);
        }

	    protected virtual void Update()
        {
            if (_oldBackgroundSprites != BackgroundSprites)
            {
                _oldBackgroundSprites = BackgroundSprites;
                UpdateBackgroundSprites(_oldBackgroundSprites);
            }

            if (_oldBackgroundBehavior != BackgroundBehavior)
            {
                _oldBackgroundBehavior = BackgroundBehavior;
                UpdateBackgroundBehavior(_oldBackgroundBehavior);
            }

            if (StaticX)
            {
                if (_staticXPos == null)
                {
                    transform.localPosition = Offset;
                    _staticXPos = transform.position.x;
                }

                transform.position = new Vector3((float)_staticXPos, transform.position.y, transform.position.z);
            }
            else
            {
                _staticXPos = null;
            }

            if (StaticY)
            {
                if (_staticYPos == null)
                {
                    transform.localPosition = Offset;
                    _staticYPos = transform.position.y;
                }

                transform.position = new Vector3(transform.position.x, (float)_staticYPos, transform.position.z);
            }
            else
            {
                _staticYPos = null;
            }

            if (BackgroundBehavior == BackgroundBehavior.Loop || BackgroundBehavior == BackgroundBehavior.Scroll)
            {
                LoopRoll[0].transform.position = new Vector3(LoopRoll[0].transform.position.x, CameraOperator.ActiveCamera.transform.position.y, LoopRoll[0].transform.position.z);
                LoopRoll[1].transform.position = new Vector3(LoopRoll[1].transform.position.x, CameraOperator.ActiveCamera.transform.position.y, LoopRoll[1].transform.position.z);
                LoopRoll[2].transform.position = new Vector3(LoopRoll[2].transform.position.x, CameraOperator.ActiveCamera.transform.position.y, LoopRoll[2].transform.position.z);
            }
        }

        protected virtual void FixedUpdate()
        {
            switch (BackgroundBehavior)
            {
                case BackgroundBehavior.Static:
                    transform.localPosition = new Vector3(0f, 0f, BackgroundDist) + Offset;
                    break;
                case BackgroundBehavior.Pan:
                    //Result := ((Input - InputLow) / (InputHigh - InputLow)) * (OutputHigh - OutputLow) + OutputLow;
                    float inputLow = LevelController.Main.MapWidthMax - CameraOperator.ActiveCamera.CameraComponent.orthographicSize * CameraOperator.CameraBoundsScale;
                    float inputHigh = LevelController.Main.MapWidthMin + CameraOperator.ActiveCamera.CameraComponent.orthographicSize * CameraOperator.CameraBoundsScale;
                    float camDist = CameraOperator.ActiveCamera.transform.position.x;
                    float newValue = (camDist - inputLow) / (inputHigh - inputLow) * (_maxPanPos.Value - _minPanPos.Value) + _minPanPos.Value;
                    transform.localPosition = new Vector3(newValue, 0f, BackgroundDist) + Offset;
                    break;
                case BackgroundBehavior.Loop:
                    DetectLoopRollUpdate();
                    break;
                case BackgroundBehavior.Scroll: // TODO: Creates lines between loop rolls.
                    LoopRoll[0].transform.position += new Vector3(ScrollSpeed, 0f, 0f);
                    LoopRoll[1].transform.position += new Vector3(ScrollSpeed, 0f, 0f);
                    LoopRoll[2].transform.position += new Vector3(ScrollSpeed, 0f, 0f);
                    DetectLoopRollUpdate();

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            transform.localScale = CustomScale;
        }

        /// <summary>
        /// Checks to see if the current loop roll is at a position relative to
        /// the camera where it would be good to adjust the loop roll. If it is,
        /// then it will make the proper adjustment.
        /// </summary>
	    protected void DetectLoopRollUpdate()
        {
            float xPos = MainPlayerOperator.MainPlayerObject != null ? MainPlayerOperator.MainPlayerObject.transform.position.x : PlayerStartPosition.Main.transform.position.x;
            if (xPos > LoopRoll[1].GetComponent<SpriteRenderer>().bounds.max.x)
            {
                UpdateLoopRoll(true);
            }
            else if (xPos < LoopRoll[1].GetComponent<SpriteRenderer>().bounds.min.x)
            {
                UpdateLoopRoll(false);
            }
        }

        /// <summary>
        /// This gets called when the BackgroundBehavior field gets changed, as well as when
        /// this component activates. Sets the new type of behavior, and cleans up the old one.
        /// </summary>
        /// <param name="value">The new type of behavior.</param>
	    protected virtual void UpdateBackgroundBehavior(BackgroundBehavior value)
        {
            switch (value)
            {
                case BackgroundBehavior.Static:
                    transform.SetParent(CameraOperator.ActiveCamera.transform);
                    break;
                case BackgroundBehavior.Pan:
                    transform.SetParent(CameraOperator.ActiveCamera.transform);
                    if (LevelController.Main.MapWidth < 42)
                    {
                        BackgroundBehavior = BackgroundBehavior.Static;
                    }

                    break;
                case BackgroundBehavior.Loop:
                case BackgroundBehavior.Scroll:
                    transform.SetParent(null, true);
                    transform.position = new Vector3
                    (
                        BackgroundSprites[0].bounds.size.x / 2f,
                        CameraOperator.ActiveCamera.transform.position.y,
                        transform.position.z
                    ) + Offset;

                    LoopRoll = new[]
                    {
                        new GameObject("Loop Partner A", typeof(SpriteRenderer), typeof(AnimationOperator)),
                        gameObject,
                        new GameObject("Loop Partner B", typeof(SpriteRenderer), typeof(AnimationOperator))
                    };

                    LoopRoll[0].transform.position = LoopRoll[1].transform.position - new Vector3(BackgroundSprites[0].bounds.size.x, 0f, 0f);
                    LoopRoll[2].transform.position = LoopRoll[1].transform.position + new Vector3(BackgroundSprites[0].bounds.size.x, 0f, 0f);

                    foreach (GameObject g in LoopRoll)
                    {
                        if (g == this) continue;

                        g.SetActive(true);
                        g.GetComponent<SpriteRenderer>().sprite = GetComponent<SpriteRenderer>().sprite;
                        g.GetComponent<AnimationOperator>().Sprites = GetComponent<AnimationOperator>().Sprites;
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(value), value, null);
            }
        }

        /// <summary>
        /// Gets called when the background sprites are changed, and also when the component
        /// starts up. This sets the sprite operator and animator to use the new sprites, and
        /// makes adjustments to this component based on the size of the new sprites.
        /// </summary>
        /// <param name="value">The new sprites to use.</param>
	    protected virtual void UpdateBackgroundSprites(Sprite[] value)
	    {
            if (value.Length <= 0)
            {
                return;
            }

	        BackgroundRenderer.sprite = value[0];
	        float height = 2f * CameraOperator.ActiveCamera.CameraComponent.orthographicSize;
	        float width = height * CameraOperator.ActiveCamera.CameraComponent.aspect;
	        if (value[0].bounds.size.x < width || value[0].bounds.size.y < height)
	        {
	            float newScale = (width - value[0].bounds.size.x) / UndersizeScale;
	            transform.localScale = new Vector3(newScale, newScale, 1f);
	        }
	        else
	        {
	            transform.localScale = new Vector3(1f, 1f, 1f);
	        }

	        float backgroundWidth = value[0].bounds.size.x * (CustomScale.x == 1f ? transform.localScale.x : CustomScale.x);
	        float difference = (backgroundWidth - width) / 2f;
	        transform.position = new Vector3(CameraOperator.ActiveCamera.transform.position.x - difference, transform.position.y, transform.position.z);
            if (_minPanPos == null || _maxPanPos == null)
            {
                _minPanPos = transform.localPosition.x;
                _maxPanPos = -_minPanPos;
            }

	        AnimationOperator.Sprites = value;
        }

        /// <summary>
        /// Adjusts the object's inside the loop roll to move forward or backward. A "loop
        /// roll" can be thought of as an array buffer with three elements, the first, which
        /// is a SpriteRenderer GameObject on the left, the second, which is the center, and
        /// the third, which is on the right. This is used to looping and scrolling backgrounds.
        /// As the player moves through the level to the right, eventually you must take the
        /// background on the left, and put it on the right, the previous right background
        /// naturally becomes the center, and eventually the left as the player continues.
        /// The same happens the other way around when the player moves to the left.
        /// </summary>
        /// <param name="forward">If true, the backgrounds will be adjusted as if the player is moving to the right through the level.</param>
	    public virtual void UpdateLoopRoll(bool forward)
	    {
	        if (BackgroundBehavior != BackgroundBehavior.Loop && BackgroundBehavior != BackgroundBehavior.Scroll) return;

	        GameObject loopRollLeft = LoopRoll[0];
	        GameObject loopRollCenter = LoopRoll[1];
	        GameObject loopRollRight = LoopRoll[2];
            if (forward)
            {
                LoopRoll[0] = loopRollCenter;
                LoopRoll[1] = loopRollRight;
                LoopRoll[2] = loopRollLeft;
            }
            else
            {
                LoopRoll[0] = loopRollRight;
                LoopRoll[1] = loopRollLeft;
                LoopRoll[2] = loopRollCenter;
            }

	        LoopRoll[0].transform.position = LoopRoll[1].transform.position - new Vector3(BackgroundSprites[0].bounds.size.x, 0f, 0f);
	        LoopRoll[2].transform.position = LoopRoll[1].transform.position + new Vector3(BackgroundSprites[0].bounds.size.x, 0f, 0f);
        }
    }
}
 