﻿using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{ 
    public class WispEffect : MonoBehaviour
    {
        public int MaxWisps = 25;
        public float MaxScale = 2f;
        public float WispAlpha = 0.5f;
        public float MaxDistance = 16f;
        public float Frequency = 0.02f;
        public Stack<GameObject> AvailableWisps { get; protected set; } = new Stack<GameObject>();
        public HashSet<GameObject> CreatedWisps { get; protected set; } = new HashSet<GameObject>();
        public Dictionary<SpriteRenderer, GameObject> EffectedGameObjects { get; protected set; } = new Dictionary<SpriteRenderer, GameObject>();
        private readonly List<SpriteRenderer> _cleanedObjects = new List<SpriteRenderer>();

        protected virtual void Awake()
        {
            for (int i = 0; i < MaxWisps; i++)
            {
                GameObject wisp = new GameObject($"Wisp {i}", typeof(SpriteRenderer));
                wisp.SetActive(false);
                CreatedWisps.Add(wisp);
                AvailableWisps.Push(wisp);
            }
        }

        protected virtual void FixedUpdate()
        {
            foreach (GameCharacter character in FindObjectsOfType<GameCharacter>()) //todo: no
            {
                if (CreatedWisps.Contains(character.gameObject)) continue;

                var spriteRenderer = character.GetComponent<SpriteRenderer>();
                var distance = Vector2.Distance(transform.position, spriteRenderer.transform.position);
                if (!EffectedGameObjects.ContainsKey(spriteRenderer) && AvailableWisps.Count > 0 && distance < MaxDistance)
                {
                    EffectedGameObjects.Add(spriteRenderer, AvailableWisps.Pop());
                }
                else if (EffectedGameObjects.ContainsKey(spriteRenderer) && distance > MaxDistance)
                {
                    var wisp = EffectedGameObjects[spriteRenderer];
                    wisp.SetActive(false);
                    AvailableWisps.Push(wisp);
                    _cleanedObjects.Add(spriteRenderer);
                }
            }

            foreach (SpriteRenderer spriteRenderer in _cleanedObjects)
            {
                EffectedGameObjects.Remove(spriteRenderer);
            }

            _cleanedObjects.Clear();
        }

        protected virtual IEnumerator<float> Tick()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Frequency);
                foreach (SpriteRenderer spriteRenderer in EffectedGameObjects.Keys)
                {
                    GameObject wisp = EffectedGameObjects[spriteRenderer];
                    wisp.SetActive(true);

                    wisp.transform.position = spriteRenderer.transform.position + new Vector3(0f, 0f, 0.01f);
                    wisp.GetComponent<SpriteRenderer>().sprite = spriteRenderer.sprite;
                    wisp.GetComponent<SpriteRenderer>().flipX = spriteRenderer.flipX;
                    wisp.GetComponent<SpriteRenderer>().flipY = spriteRenderer.flipY;
                    wisp.GetComponent<SpriteRenderer>().color = spriteRenderer.color - new Color(0f, 0f, 0f, WispAlpha);

                    float dist = Vector2.Distance(spriteRenderer.transform.position, transform.position);
                    const float minScale = 1f;
                    float maxScale = MathUtilities.GetRange(dist, MaxDistance, 0f, 2f, 1f);
                    wisp.transform.localScale = new Vector3(Random.Range(minScale, maxScale), Random.Range(minScale, maxScale), 1f);
                }
            }
        }
    }
}
