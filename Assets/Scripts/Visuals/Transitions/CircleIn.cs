﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using UnityEngine;
using Worldline.Level;
using Worldline.Player;
using Object = UnityEngine.Object;

namespace Worldline.Visuals.Transitions
{
    /// <summary>
    /// TODO: Fix
    /// </summary>
    public class CircleIn : Transition
    {
        public Sprite TransitionSprite;
        public readonly Vector3 StartSize = new Vector2(93f, 93f);
        public readonly Vector3 EndSize = new Vector2(44f, 44f);
        public const float DistanceFromCamera = 0.5f;
        public const float Speed = 0.03f;
        private GameObject _transitionObject;

        protected virtual IEnumerator TransitionInto(float speed)
        {
            yield return new WaitForFixedUpdate();
            _transitionObject.transform.position = new Vector3(MainPlayerOperator.MainPlayerObject.transform.position.x, MainPlayerOperator.MainPlayerObject.transform.position.y, _transitionObject.transform.position.z);
            _transitionObject.transform.GetComponent<SpriteRenderer>().size = Vector2.MoveTowards(_transitionObject.transform.GetComponent<SpriteRenderer>().size, EndSize, speed);
            if (_transitionObject.transform.GetComponent<SpriteRenderer>().size.x > EndSize.x)
            {
                speed += Speed;
                LevelController.Main.StartCoroutine(TransitionInto(speed));
            }
            else
            {
                SafeToLoad = true;
            }
        }

        protected virtual IEnumerator TransitionOut(float speed)
        {
            yield return new WaitForFixedUpdate();
            _transitionObject.transform.position = new Vector3(MainPlayerOperator.MainPlayerObject.transform.position.x, MainPlayerOperator.MainPlayerObject.transform.position.y, _transitionObject.transform.position.z);
            _transitionObject.transform.GetComponent<SpriteRenderer>().size = Vector2.MoveTowards(_transitionObject.transform.GetComponent<SpriteRenderer>().size, StartSize, speed);
            if (_transitionObject.transform.GetComponent<SpriteRenderer>().size.x < StartSize.x)
            {
                speed += Speed;
                LevelController.Main.StartCoroutine(TransitionOut(speed));
            }
            else
            {
                Active = false;
                Object.Destroy(_transitionObject);
            }
        }

        public override void StartTransition()
        {
            Active = true;
            _transitionObject = new GameObject("Transition", typeof(SpriteRenderer));
            _transitionObject.transform.position = new Vector3(MainPlayerOperator.MainPlayerObject.transform.position.x, MainPlayerOperator.MainPlayerObject.transform.position.y, CameraOperator.ActiveCamera.transform.position.z)  + new Vector3(0, 0, DistanceFromCamera);
            _transitionObject.GetComponent<SpriteRenderer>().drawMode = SpriteDrawMode.Sliced;
            _transitionObject.GetComponent<SpriteRenderer>().sprite = TransitionSprite;
            _transitionObject.transform.GetComponent<SpriteRenderer>().size = StartSize;
            Object.DontDestroyOnLoad(_transitionObject);
            LevelController.Main.StartCoroutine(TransitionInto(0));
        }

        public override void ForceUpdate()
        {
            _transitionObject.transform.SetParent(CameraOperator.ActiveCamera.transform);
            _transitionObject.transform.localPosition = new Vector3(0f, 0f, DistanceFromCamera);
        }

        public override void EndTransition()
        {
            SafeToLoad = false;
            LevelController.Main.StartCoroutine(TransitionOut(0));
        }
    }
}
