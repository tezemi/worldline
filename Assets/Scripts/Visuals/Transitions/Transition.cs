﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Visuals.Transitions
{
    public abstract class Transition : ScriptableObject
    {
        public bool Active { get; set; }
        public bool SafeToLoad { get; set; }

        public virtual void ForceUpdate() { }
        public abstract void StartTransition();
        public abstract void EndTransition();
    }
}
