﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.GUI;
using MEC;
using UnityEngine;

namespace Worldline.Visuals.Transitions
{
    /// <summary>
    /// Uses the game's main overlay to perform a sliding effect when changing scenes.
    /// </summary>
    [CreateAssetMenu(fileName = "Simple Slide", menuName = "Data/Level Transitions/Simple Slide")]
    public class SimpleSlide : Transition
    {
        public float MaxSpeed = 20f;
        public float MinSpeed = 5f;
        public float SpeedDecay = 0.35f;
        public Color SliderColor = Color.black;
        public const float OffScreenPosition = 500f;
       
        protected virtual IEnumerator<float> TransitionInto()
        {
            float speed = MaxSpeed;
            start:
            yield return Timing.WaitForOneFrame;
            if (Math.Abs(PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition.y) > MinSpeed)
            {
                PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition = Vector2.MoveTowards
                (
                    PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition,
                    Vector2.zero,
                    speed -= speed > MinSpeed ? SpeedDecay : 0f
                );

                goto start;
            }

            PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition = new Vector2(0f, 0f);
            SafeToLoad = true;
        }

        protected virtual IEnumerator<float> TransitionOut()
        {
            float speed = MinSpeed;
            start:
            yield return Timing.WaitForOneFrame;
            if (PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition.y > -OffScreenPosition)
            {
                PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition = Vector2.MoveTowards
                (
                    PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition,
                    new Vector2(0f, -OffScreenPosition), 
                    speed += speed < MaxSpeed ? SpeedDecay : 0f
                );

                goto start;
            }

            PersistentCanvasController.Main.Overlay.color = Color.clear;
            PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition = new Vector2(0f, 0f);
            Active = false;
        }

        public override void StartTransition()
        {
            Active = true;
            PersistentCanvasController.Main.Overlay.color = SliderColor;
            PersistentCanvasController.Main.Overlay.rectTransform.anchoredPosition = new Vector2(0f, OffScreenPosition);
            Timing.RunCoroutine(TransitionInto(), Segment.FixedUpdate);
        }

        public override void EndTransition()
        {
            SafeToLoad = false;
            Timing.RunCoroutine(TransitionOut(), Segment.FixedUpdate);
        }        
    }
}
