﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GUI;
using MEC;
using UnityEngine;

namespace Worldline.Visuals.Transitions
{
    [CreateAssetMenu(fileName = "Fade In Fade Out", menuName = "Data/Level Transitions/Fade In Fade Out")]
    public class FadeInFadeOut : Transition
    {
        public float FadeSpeed = 0.02f;
        public Color FadeColor = Color.black;

        protected virtual IEnumerator<float> TransitionInto()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (PersistentCanvasController.Main.Overlay.color.a < 1f)
            {
                PersistentCanvasController.Main.Overlay.color += new Color(0f, 0f, 0f, FadeSpeed);
                goto start;
            }

            SafeToLoad = true;
        }

        protected virtual IEnumerator<float> TransitionOut()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (PersistentCanvasController.Main.Overlay.color.a > 0f)
            {
                PersistentCanvasController.Main.Overlay.color -= new Color(0f, 0f, 0f, FadeSpeed);
                goto start;
            }

            Active = false;
        }

        public override void StartTransition()
        {
            Active = true;
            PersistentCanvasController.Main.Overlay.color = FadeColor - new Color(0f, 0f, 0f, 1f);
            Timing.RunCoroutine(TransitionInto(), Segment.FixedUpdate);
        }

        public override void EndTransition()
        {
            SafeToLoad = false;
            Timing.RunCoroutine(TransitionOut(), Segment.FixedUpdate);
        }
    }
}
