﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Corruption.SpriteCorruption;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    public class CorruptionSpawn : MonoBehaviour
    {
        public float TimeToSpawn = 5f;
        public Sprite[] CorruptedSprites { get; set; } = new Sprite[10];
        public bool FadingIn { get; protected set; }
        public bool InitSpawn { get; protected set; } = true;
        private GameObject _corruptedSpawn;

        protected virtual void OnEnable()
        {
            if (InitSpawn)
            {
                Timing.RunCoroutine(FlashSprites(), Segment.FixedUpdate, GetInstanceID().ToString());
                InitSpawn = false;
            }
        }

        protected virtual void OnDestroy()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> FlashSprites()
        {
            // Sets up sprites and an object to put them on.
            for (int j = 0; j < CorruptedSprites.Length; j++)
            {
                Sprite tearifiedSprite = GetComponent<SpriteRenderer>().sprite;
            }

            _corruptedSpawn = new GameObject("Corrupted Spawn", typeof(SpriteRenderer), typeof(DistortionAnimation));
            _corruptedSpawn.transform.position = transform.position;
            gameObject.SetActive(false);
            FadingIn = true;
            Timing.RunCoroutine(FadeIn(), Segment.FixedUpdate, GetInstanceID().ToString()); // Fades this thing in.

            // The main loop where the sprite is changed.
            start:
            yield return Timing.WaitForSeconds(Random.Range(0.05f, 0.5f));
            _corruptedSpawn.GetComponent<SpriteRenderer>().sprite = CorruptedSprites[Random.Range(0, CorruptedSprites.Length)];
            if (FadingIn)
            {
                goto start;
            }

            gameObject.SetActive(true);
            Destroy(_corruptedSpawn);
        }

        protected virtual IEnumerator<float> FadeIn()
        {
            _corruptedSpawn.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0.05f);
            start:
            yield return Timing.WaitForSeconds(TimeToSpawn * Time.deltaTime);
            _corruptedSpawn.GetComponent<SpriteRenderer>().color += new Color(1f, 1f, 1f, 0.02f);
            if (_corruptedSpawn.GetComponent<SpriteRenderer>().color.a < 1f)
            {
                goto start;
            }

            FadingIn = false;
        }
    }
}
