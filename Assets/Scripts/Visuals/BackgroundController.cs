﻿using Worldline.Level;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.Visuals
{
    public class BackgroundController : MonoBehaviour
    {
        public static BackgroundController Main { get; set; }
        public bool LockVerticalPosition;
        public BackgroundBehavior BackgroundBehavior;
        public Sprite[] BackgroundSprites;
        public const float PanSpeed = 5f;
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected AnimationOperator AnimationOperator { get; set; }
        private float? _verticalPos;
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            TimeOperator = GetComponent<TimeOperator>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            AnimationOperator = GetComponent<AnimationOperator>();
        }

        protected virtual void Update()
        {
            if (!TimeOperator.ShouldHold && AnimationOperator.Sprites != BackgroundSprites)
            {
                AnimationOperator.Sprites = BackgroundSprites;
            }

            if (LockVerticalPosition && !_verticalPos.HasValue)
            {
                _verticalPos = transform.position.y;
            }
            
            switch (BackgroundBehavior)
            {
                case BackgroundBehavior.Static:
                    transform.localPosition = new Vector3(0f, 0f, transform.localPosition.z);
                    break;
                case BackgroundBehavior.Pan:
                    float minPossiblePosition = LevelController.Main.MapWidthMin + SpriteRenderer.bounds.size.x / 2f;
                    float maxPossiblePosition = LevelController.Main.MapWidthMax - SpriteRenderer.bounds.size.x / 2f;

                    float height = 2f * CameraOperator.ActiveCamera.CameraComponent.orthographicSize;
                    float width = height * CameraOperator.ActiveCamera.CameraComponent.aspect;

                    float xPos = MathUtilities.GetRange
                    (
                        CameraOperator.ActiveCamera.transform.position.x, 
                        LevelController.Main.MapWidthMin + width / 2f, 
                        LevelController.Main.MapWidthMax - width / 2f,
                        maxPossiblePosition, 
                        minPossiblePosition
                    );

                    transform.position = Vector3.MoveTowards(transform.position, new Vector3(xPos, transform.parent.position.y, transform.position.z), PanSpeed);
                    break;
                case BackgroundBehavior.Loop:
                    // TODO
                    break;
                case BackgroundBehavior.Scroll:
                    // TODO
                    break;
            }

            if (LockVerticalPosition && _verticalPos.HasValue)
            {
                transform.position = new Vector3(transform.position.x, _verticalPos.Value, transform.position.z);
            }
        }

        public class BackgroundControllerSnapshot : Snapshot
        {
            public int Frame { get; set; }
            public Sprite Sprite { get; set; }
            
            public override void Record(GameObject gameObject)
            {
                Frame = gameObject.GetComponent<AnimationOperator>().CurrentFrame;
                Sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
            }

            public override void Replay(GameObject gameObject)
            {
                gameObject.GetComponent<AnimationOperator>().CurrentFrame = Frame;
                gameObject.GetComponent<SpriteRenderer>().sprite = Sprite;
            }
        }
    }
}
