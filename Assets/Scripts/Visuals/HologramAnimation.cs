﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Worldline.Visuals
{
    public class HologramAnimation : MonoBehaviour
    {
        public float FlickerMin = 0.65f;
        public float FlickerMax = 0.95f;
        public float FlickerDelay = 0.025f;
        protected SpriteRenderer SpriteRenderer { get; set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> Tick()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(FlickerDelay);
                SpriteRenderer.color = new Color
                (
                    SpriteRenderer.color.r, 
                    SpriteRenderer.color.g, 
                    SpriteRenderer.color.b,
                    Random.Range(FlickerMin, FlickerMax)
                );
            }
        }
    }
}
