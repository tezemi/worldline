// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;
using UnityEngine.Serialization;

namespace Worldline.Visuals
{
    /// <summary>
    /// Component that handles simple sprite animation.
    /// </summary>
    [DisableOnPlayback]
    [DisableOnRewinding]
    [RequireComponent(typeof(SpriteRenderer))]
	public class AnimationOperator : MonoBehaviour 
	{
        [FormerlySerializedAs("Speed")]
        public float Delay;
        public bool DisableAnimationReset { get; set; }
        public int CurrentFrame { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
	    private float _delay;

        [SerializeField]
        private Sprite[] _sprites;
        public Sprite[] Sprites
        {
            get => _sprites;
            set
            {
                CurrentFrame = 0;
                _sprites = value;
            }
        }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            if (GetComponent<TimeOperator>() != null)
            {
                TimeOperator = GetComponent<TimeOperator>();
            }
        }

	    protected virtual void OnEnable()
	    {
	        _delay = Delay;
	        Timing.RunCoroutine(DoAnimation(), Segment.FixedUpdate, GetInstanceID().ToString());
	    }

	    protected virtual void OnDisable()
	    {
	        Timing.KillCoroutines(GetInstanceID().ToString());
	    }

	    protected virtual void Update()
	    {
	        if (_delay != Delay && !DisableAnimationReset)
	        {
	            _delay = Delay;
	            Timing.KillCoroutines(GetInstanceID().ToString());
	            Timing.RunCoroutine(DoAnimation(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
	    }

        /// <summary>
        /// Forces the animation to go to the next frame.
        /// </summary>
	    public virtual void ForceUpdate()
	    {
	        GetComponent<SpriteRenderer>().sprite = _sprites[0];
	    }

        /// <summary>
        /// Gets called after every animation frame. Does nothing by default, 
        /// but can be overridden to add behavior.
        /// </summary>
        protected virtual void Step() { }

        /// <summary>
        /// This is the main animation loop. Moves to the next sprite every tick.
        /// </summary>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> DoAnimation()
        {
            start:
            if (TimeOperator != null)
            {
                yield return this.WaitForFixedSecondsScaled(Delay);
            }
            else
            {
                yield return Timing.WaitForSeconds(Delay);
            }

            if (CurrentFrame > _sprites.Length - 1)
            {
                CurrentFrame = 0;
            }

            if (_sprites.Length <= 0)
            {
                yield break;
            }
            
            SpriteRenderer.sprite = _sprites.Length > 1 ? _sprites[CurrentFrame++] : _sprites[0];
            Step();
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }
    }
}
