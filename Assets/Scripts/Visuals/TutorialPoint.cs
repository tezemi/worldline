﻿// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.GUI;
using Worldline.Utilities;
using Worldline.GameState;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Worldline.Visuals
{
    public class TutorialPoint : TrackedMonoBehaviour
    {
        public bool SayHold;
        public string Input;
        public string Input2;
        public string ResultLocation;
        public float StartOffset = -5f;
        public Transform WorldCanvas;
        public GameObject AlphaNumericPrefab;
        public GameObject BigKeyPrefab;
        public GameObject ArrowKeyPrefab;
        public GameObject EnterKeyPrefab;
        public GameObject MouseButtonPrefab;
        public GameObject MultiButtonInput;
        public GameObject TextPrefab;
        public GameObject TutorialInstance { get; protected set; }
        public const float HoverOffset = 1f;

        protected virtual void Awake()
        {
            if (Fired)
            {
                enabled = true;
            }
        }

        protected virtual void OnEnable()
        {
            FireID();
            string pressText = SayHold ? 
                SerializedStrings.GetString("Sim/TutorialPopups", "Hold0") :
                SerializedStrings.GetString("Sim/TutorialPopups", "Press0");

            if (ResultLocation == "Shoot0")
            {
                if (Environment.UserName.Contains("Vinny") ||
                Environment.UserName.Contains("vinny") ||
                Environment.UserName.Contains("Vine") ||
                Environment.UserName.Contains("vine") ||
                Environment.UserName.Contains("Vinesauce") ||
                Environment.UserName.Contains("vinesauce") ||
                Environment.UserName.Contains("Vin") ||
                Environment.UserName.Contains("vin") || 
                Random.value < 0.001f)
                {
                    ResultLocation = "Shoot1";
                }
            }

            string resultText = SerializedStrings.GetString("Sim/TutorialPopups", ResultLocation);
            AxisConfiguration axisConfiguration = InputManager.GetAxisConfiguration(PlayerID.One, Input);
            if (axisConfiguration.positive == KeyCode.Mouse3 || axisConfiguration.positive == KeyCode.Mouse4)
            {
                pressText = SerializedStrings.GetString("Sim/TutorialPopups", "Scroll0");
            }
            
            if (!string.IsNullOrEmpty(Input2) && !IsMouseButton(axisConfiguration.positive))
            {
                AxisConfiguration axisConfiguration2 = InputManager.GetAxisConfiguration(PlayerID.One, Input2);
                TutorialInstance = Instantiate(MultiButtonInput);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint = transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
                Text inputText = TutorialInstance.GetComponent<TutorialBox>().InputText;
                if (inputText != null)
                {
                    inputText.text = GetNiceName(axisConfiguration.positive);
                }

                Text inputText2 = TutorialInstance.GetComponent<MultiButtonBox>().Input2;
                if (inputText2 != null)
                {
                    inputText2.text = GetNiceName(axisConfiguration2.positive);
                }
            }
            else if (IsSmallKey(axisConfiguration.positive))
            {
                TutorialInstance = Instantiate(AlphaNumericPrefab);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint = transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
                Text inputText = TutorialInstance.GetComponent<TutorialBox>().InputText;
                if (inputText != null)
                {
                    inputText.text = GetNiceName(axisConfiguration.positive);
                }
            }
            else if (IsArrowKey(axisConfiguration.positive))
            {
                TutorialInstance = Instantiate(ArrowKeyPrefab);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint = transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
                Sprite arrowIcon;
                if (axisConfiguration.positive == KeyCode.UpArrow)
                {
                    arrowIcon = TutorialInstance.GetComponent<ArrowKeyBox>().KeyUp;
                }
                else if (axisConfiguration.positive == KeyCode.DownArrow)
                {
                    arrowIcon = TutorialInstance.GetComponent<ArrowKeyBox>().KeyDown;
                }
                else if (axisConfiguration.positive == KeyCode.LeftArrow)
                {
                    arrowIcon = TutorialInstance.GetComponent<ArrowKeyBox>().KeyLeft;
                }
                else
                {
                    arrowIcon = TutorialInstance.GetComponent<ArrowKeyBox>().KeyRight;
                }

                TutorialInstance.GetComponent<TutorialBox>().Icon.sprite = arrowIcon;
            }
            else if (axisConfiguration.positive == KeyCode.Return)
            {
                TutorialInstance = Instantiate(EnterKeyPrefab);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint =
                transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
            }
            else if (IsMouseButton(axisConfiguration.positive))
            {
                TutorialInstance = Instantiate(MouseButtonPrefab);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint =
                transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
                Sprite buttonIcon;
                if (axisConfiguration.positive == KeyCode.Mouse0)
                {
                    buttonIcon = TutorialInstance.GetComponent<MouseButtonBox>().MouseZero;
                }
                else if (axisConfiguration.positive == KeyCode.Mouse1)
                {
                    buttonIcon = TutorialInstance.GetComponent<MouseButtonBox>().MouseOne;
                }
                else
                {
                    buttonIcon = TutorialInstance.GetComponent<MouseButtonBox>().MouseTwo;
                }

                TutorialInstance.GetComponent<TutorialBox>().Icon.sprite = buttonIcon;
            }
            else // If is isn't any of the above, we just assume it's a large key.
            {
                TutorialInstance = Instantiate(BigKeyPrefab);
                TutorialInstance.transform.SetParent(WorldCanvas);
                TutorialInstance.GetComponent<TutorialBox>().PressText.text = pressText;
                TutorialInstance.GetComponent<TutorialBox>().ResultText.text = resultText;
                TutorialInstance.GetComponent<TutorialBox>().HoverPoint = transform.position + new Vector3(0f, HoverOffset, 0f);
                TutorialInstance.transform.position = transform.position + new Vector3(0f, StartOffset, 0f);
                Text inputText = TutorialInstance.GetComponent<TutorialBox>().InputText;
                if (inputText != null)
                {
                    inputText.text = GetNiceName(axisConfiguration.positive);
                }
            }
        }

        public static bool IsSmallKey(KeyCode key)
        {
            return (int)key >= 33 && (int)key <= 272 || (int)key >= 277 && (int)key <= 302 ||
                   (int)key >= 309 && (int)key <= 318 || key  == KeyCode.Escape;
        }

        public static bool IsArrowKey(KeyCode key)
        {
            return key == KeyCode.UpArrow || key == KeyCode.DownArrow || 
                   key == KeyCode.LeftArrow || key == KeyCode.RightArrow;
        }

        public static bool IsMouseButton(KeyCode key)
        {
            return (int)key >= 323 && (int)key <= 329;
        }

        public static string GetNiceName(KeyCode key)
        {
            switch (key)
            {
                case KeyCode.Alpha0:
                case KeyCode.Keypad0:
                    return "0";
                case KeyCode.Alpha1:
                case KeyCode.Keypad1:
                    return "1";
                case KeyCode.Alpha2:
                case KeyCode.Keypad2:
                    return "2";
                case KeyCode.Alpha3:
                case KeyCode.Keypad3:
                    return "3";
                case KeyCode.Alpha4:
                case KeyCode.Keypad4:
                    return "4";
                case KeyCode.Alpha5:
                case KeyCode.Keypad5:
                    return "5";
                case KeyCode.Alpha6:
                case KeyCode.Keypad6:
                    return "6";
                case KeyCode.Alpha7:
                case KeyCode.Keypad7:
                    return "7";
                case KeyCode.Alpha8:
                case KeyCode.Keypad8:
                    return "8";
                case KeyCode.Alpha9:
                case KeyCode.Keypad9:
                    return "9";
                case KeyCode.Escape:
                    return "ESC";
                case KeyCode.RightControl:
                case KeyCode.LeftControl:
                    return "Ctrl";
                case KeyCode.RightAlt:
                case KeyCode.LeftAlt:
                    return "Alt";
                case KeyCode.LeftShift:
                    return "Left Shift";
                case KeyCode.RightShift:
                    return "Right Shift";
                case KeyCode.Mouse0:        // These get replaced with icons
                    return "Left Click";    // Still nice to have these for other places
                case KeyCode.Mouse1:
                    return "Right Click";
                case KeyCode.Mouse2:
                    return "Scroll Wheel Click";
                case KeyCode.Mouse3:
                    return "Scroll Wheel";
                case KeyCode.Mouse4:
                    return "Scroll Wheel";
            }

            return key.ToString(); // If no name is set explicitly, just assume ToString is nice enough.
        }
    }
}
