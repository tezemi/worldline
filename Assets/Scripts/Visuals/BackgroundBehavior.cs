﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.Visuals
{
    public enum BackgroundBehavior
    {
        Static,
        Pan,
        Loop,
        Scroll
    }
}
