﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Visuals
{
    public class LesionAnimationOperator : AnimationOperator
    {
        protected override void Step()
        {
            transform.rotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
        }
    }
}
