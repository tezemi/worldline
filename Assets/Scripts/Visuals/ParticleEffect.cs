﻿// Copyright (c) 2019 Destin Hebner

using System.Collections.Generic;
using MEC;
using Worldline.Combat;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.Visuals
{
    /// <summary>
    /// Allows you create an animated particle, which should consist of a
    /// series of sprites to be played out before the particle dies. The
    /// particle can be effected by time engine effects, and can be rewound
    /// and bound to a certain faction.
    /// </summary>
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(VisualTimeOperator))]
    [RequireComponent(typeof(ParticleAnimationOperator))]
    public class ParticleEffect : MonoBehaviour, ICanRessurect, IHasAlignment, IPoolable
    {
        public float Delay = .08f;
        public Sprite[] Sprites;
        public bool Resurrected { get; set; }
        public bool Dead { get; set; }
        public CombatAlignment Alignment { get; set; }
        public bool RunDieMethodOnOutOfMemory { get; } = true;
        public const string PoolName = "ParticlePool";
        protected ParticleAnimationOperator ParticleAnimator { get; set; }

        private void Awake()
        {
            ParticleAnimator = GetComponent<ParticleAnimationOperator>();
            ParticleAnimator.enabled = false;
        }

        /// <summary>
        /// Plays and enabled the particle's animation.
        /// </summary>
        public void Play()
        {
            ParticleAnimator.Sprites = Sprites;
            ParticleAnimator.Delay = Delay;
            ParticleAnimator.enabled = true;
        }

        /// <summary>
        /// Kills this particle, pooling it with a buffer allowing for resurrection.
        /// </summary>
        public void Die()
        {
            Dead = true;
            gameObject.SetActive(false);
            DeathTimeOperator.Handle(gameObject, Alignment, false);
            Pool.Add(gameObject, PoolName, true);
        }

        /// <summary>
        /// Resurrects this particle.
        /// </summary>
        public void Resurrect()
        {
            Dead = false;
            gameObject.SetActive(true);
            GetComponent<ParticleAnimationOperator>().enabled = false;
            Timing.RunCoroutine(ReplayAnimationWhenFree(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> ReplayAnimationWhenFree()
            {
                yield return Timing.WaitUntilDone(() => !GetComponent<TimeOperator>().ShouldHold);
                GetComponent<ParticleAnimationOperator>().enabled = true;
            }
        }

        /// <summary>
        /// Resets some information about this particle for use within the pool.
        /// </summary>
        public void Reuse()
        {
            Resurrected = false;
            Dead = false;
            GetComponent<ParticleAnimationOperator>().ResetAnimator();
            gameObject.SetActive(true);
        }

        /// <summary>
        /// This is the best way to create a particle effect. Automatically reuses
        /// pooled particles, and pools particles that are dead.
        /// </summary>
        /// <param name="frames">The sprites to be played in the animation.</param>
        /// <param name="position">The position of the particle within the world.</param>
        /// <param name="combatAlignment">The faction of this particle.</param>
        /// <param name="delay">The delay of the animation speed.</param>
        /// <returns></returns>
        public static ParticleEffect Create(Sprite[] frames, Vector3 position, CombatAlignment combatAlignment, float delay = .08f)
        {
            GameObject particle;
            if (Pool.Has(PoolName))
            {
                particle = Pool.Get(PoolName);
            }
            else
            {
                particle = new GameObject("Particle", typeof(ParticleEffect));
            }
            
            ParticleEffect effect = particle.GetComponent<ParticleEffect>();
            effect.transform.localScale = new Vector3(1f, 1f, 1f);
            effect.GetComponent<SpriteRenderer>().color = Color.white;
            effect.GetComponent<SpriteRenderer>().sprite = frames[0];
            effect.GetComponent<Recorder>().SnapshotType = typeof(VisualSnapshot);
            effect.Sprites = frames;
            effect.Delay = delay;
            effect.Alignment = combatAlignment;
            effect.transform.position = position;
            effect.Play();

            return effect;
        }
    }
}