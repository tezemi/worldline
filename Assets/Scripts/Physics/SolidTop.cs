// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// Attach to a <see cref="GameObject"/> with a flat <see cref="EdgeCollider2D"/> 
    /// in order to allow other <see cref="GameObject"/>s with 
    /// <see cref="MovementOperator"/>s to be able to pass through from the bottom of
    /// the surface, but have the surface remain solid on top.
    /// </summary>
    public class SolidTop : MonoBehaviour
    {
        /// <summary>
        /// All <see cref="MovementOperator"/>s stored in this List will always ignore
        /// collision with this <see cref="SolidTop"/>.
        /// </summary>
        public List<MovementOperator> IgnoreAlways { get; } = new List<MovementOperator>();
        /// <summary>
        /// The distance needed to the <see cref="SolidTop"/> before it will even check to
        /// see if it should collide with another <see cref="GameObject"/>. This helps
        /// performance, as the <see cref="SolidTop"/> won't check every single 
        /// <see cref="GameObject"/>, only nearby ones.
        /// </summary>
        public const float DistanceToActivate = 2f;
        /// <summary>
        /// The attached <see cref="EdgeCollider2D"/>.
        /// </summary>
        protected EdgeCollider2D Collider;
        private Vector2 _positionLastTick;

        protected virtual void Awake()
        {
            Collider = GetComponent<EdgeCollider2D>();
        }

        protected virtual void FixedUpdate()
        {
            if (LevelController.Main.Paused) return;    // Saves on CPU

            foreach (MovementOperator movementOperator in MovementOperator.AllMovementOperators)
            {
                var otherCollider = movementOperator.GetSolidCollider();
                if (otherCollider == null)
                {
                    continue;
                }

                if (otherCollider.bounds.max.x < Collider.bounds.min.x - DistanceToActivate || // Also saves CPU
                otherCollider.bounds.min.x > Collider.bounds.max.x + DistanceToActivate)
                {
                    continue;
                }

                float distanceMoved = Mathf.Abs(((Vector2)transform.position - _positionLastTick).y);
                if (otherCollider.bounds.min.y > Collider.bounds.max.y - distanceMoved && !IgnoreAlways.Contains(movementOperator))
                {
                    Physics2D.queriesHitTriggers = false;   // Check for solid above object
                    Vector3 leftStart = otherCollider.bounds.max - new Vector3(otherCollider.bounds.size.x, 0f, 0f);
                    Vector3 rightStart = otherCollider.bounds.max;
                    RaycastHit2D leftHit = Physics2D.Raycast(leftStart, Vector2.up, 0.05f, movementOperator.GroundDetectionLayerMask);
                    RaycastHit2D rightHit = Physics2D.Raycast(rightStart, Vector2.up, 0.05f, movementOperator.GroundDetectionLayerMask);
                    Physics2D.queriesHitTriggers = true;
                    if (leftHit || rightHit)
                    {
                        Physics2D.IgnoreCollision(otherCollider, Collider, true); // If ceiling, fall through
                    }
                    else
                    {
                        Physics2D.IgnoreCollision(otherCollider, Collider, false); // If not, stay solid
                    }

                    Physics2D.IgnoreCollision(otherCollider, Collider, false);
                }
                else
                {
                    Physics2D.IgnoreCollision(otherCollider, Collider, true);
                }
            }

            _positionLastTick = transform.position;
        }
    }
}
