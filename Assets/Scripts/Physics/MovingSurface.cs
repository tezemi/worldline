﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.TimeEngine;
using Worldline.Player.Upgrades;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// When attached to a GameObject with a Collider, if a GameObject with 
    /// a MovementOperator rests on the collider, the GameObject will move
    /// with the collider until it leaves it.
    /// </summary>
    public class MovingSurface : MonoBehaviour
    {
        [Tooltip("If true, this MovingSurface will force GameObject that collide with it to become grounded. Good for edge colliders.")]
        public bool GroundGameObjects;
        public bool AllowWallJumping;
        /// <summary>
        /// All of the GameObjects that have MovementOperators touching this 
        /// surface. GameObjects in this list are not necessarily being actively
        /// moved by this MovingSolid.
        /// </summary>
        public List<GameObject> MovementOperatorsTouching { get; protected set; } = new List<GameObject>();
        protected Vector3 PositionLastFrame { get; set; }
       
        protected virtual void OnCollisionEnter2D(Collision2D other)
        {
            if (other == null || other.gameObject.GetComponent<MovementOperator>() == null || !other.gameObject.GetComponent<MovementOperator>().CanBeGrounded) return;

            // Not if the player is not a clone, or is not under certain time effects!
            if (other.gameObject.GetComponent<TimeOperator>() == null || !other.gameObject.GetComponent<TimeOperator>().ShouldHold)
            {
                MovementOperatorsTouching.Add(other.gameObject);
                if (GroundGameObjects)
                {
                    other.gameObject.GetComponent<MovementOperator>().Ground(0.02f);
                }

                UpdateGameObjectPositions();
            }
        }

        protected virtual void OnCollisionExit2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<MovementOperator>() != null && MovementOperatorsTouching.Contains(other.gameObject))
            {
                MovementOperatorsTouching.Remove(other.gameObject);
            }
        }

        [UsedImplicitly]
        protected virtual void OnReplaySnapshot()
        {
            UpdateGameObjectPositions();
        }

        /// <summary>
        /// Updates all of the GameObjects currently resting on this platform 
        /// to move with it if any movement changes have been made between the 
        /// current frame and the last.
        /// </summary>
        public virtual void UpdateGameObjectPositions()
        {
            foreach (GameObject g in MovementOperatorsTouching)
            {
                bool wallJumping = AllowWallJumping && IsWallJumping(g);
                if (!wallJumping && (!IsOnTop(g) || IsJumping(g) || IsWallJumping(g))) continue;

                g.transform.position += transform.position - PositionLastFrame;
                if (IsPlayer(g) && CameraOperator.ActiveCamera.Track == g)
                {
                    CameraOperator.ActiveCamera.SendMessageUpwards
                    (
                        "FixedUpdate",
                        SendMessageOptions.DontRequireReceiver
                    );
                }
            }

            PositionLastFrame = transform.position;
        }

        /// <summary>
        /// Checks to see if the specified GameObject is not grounded.
        /// </summary>
        /// <param name="obj">The GameObject to check.</param>
        /// <returns>True if GameObject isn't on the ground.</returns>
        protected bool IsGrounded(GameObject obj)
        {
            return obj.GetComponent<MovementOperator>().Grounded;
        }

        /// <summary>
        /// Checkes to see if the specified GameObject is jumping.
        /// </summary>
        /// <param name="obj">The GameObject to check.</param>
        /// <returns>True if the GameObject is jumping.</returns>
        protected bool IsJumping(GameObject obj)
        {
            return obj.GetComponent<MovementOperator>().Jumping;
        }

        /// <summary>
        /// Checks to see if the specified GameObject is not actually on top of 
        /// this solid (although they may be touching it.)
        /// </summary>
        /// <param name="obj">The GameObject to check.</param>
        /// <returns>True if the GameObject is not actually on top of this solid.</returns>
        protected bool IsOnTop(GameObject obj)
        {
            float objMin = obj.GetComponent<Collider2D>().bounds.min.y + (transform.position.y - PositionLastFrame.y); // Account for the fact that the GameObject might be moved upward
            float myMax = GetComponent<CompositeCollider2D>() != null ? // Ensures we don't pick a TilemapCollider.
            GetComponent<CompositeCollider2D>().bounds.max.y : GetComponent<Collider2D>().bounds.max.y;

            return objMin > myMax;
        }

        /// <summary>
        /// Checks to see if the GameObject has the wall jump module, and if they 
        /// do, checks to see if they are wall jumping. 
        /// </summary>
        /// <param name="obj">The GameObject to check.</param>
        /// <returns>True if the GameObject is wall jumping on this solid.</returns>
        protected bool IsWallJumping(GameObject obj)
        {
            if (obj.GetComponent<PlayerOperator>() == null) return false;

            PlayerOperator playerOperator = obj.GetComponent<PlayerOperator>();
            if (playerOperator == null)
            {
                return false;
            }

            return !playerOperator.GetComponent<MovementOperator>().Grounded &&
                   playerOperator.GetUpgrade<WallJumpUpgrade>() != null &&
                   playerOperator.GetUpgrade<WallJumpUpgrade>().OnWall;
        }

        /// <summary>
        /// Checks to see if the GameObject is a player.
        /// </summary>
        /// <param name="obj">The GameObject to check.</param>
        /// <returns>True if the GameObject is a player.</returns>
        protected bool IsPlayer(GameObject obj)
        {
            return obj.GetComponent<PlayerOperator>() == null;
        }
    }
}
