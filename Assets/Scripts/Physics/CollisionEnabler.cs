﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// When attached to a GameObject with any type of 2D collision, this
    /// will disable the collision on Awake(), but will re-enable the
    /// collision after waiting from Delay. Good for enemies who need to
    /// spawn from behind walls.
    /// </summary>
    public class CollisionEnabler : MonoBehaviour
    {
        public float Delay = 1f;

        protected virtual void Awake()
        {
            foreach (Collider2D col in GetComponents<Collider2D>())
            {
                col.enabled = false;
            }

            Timing.RunCoroutine(Wait(), Segment.FixedUpdate);
        }

        /// <summary>
        /// Waits for the specified Delay, and then enables all the collision
        /// on the GameObject.
        /// </summary>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> Wait()
        {
            yield return Timing.WaitForSeconds(Delay);
            foreach (Collider2D col in GetComponents<Collider2D>())
            {
                col.enabled = true;
            }
        }
    }
}
