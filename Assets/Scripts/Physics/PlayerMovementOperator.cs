// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// A MovementOperator specific to player GameObjects. Mostly responsible 
    /// for handling the player's controlled jumping.
    /// </summary>
    public class PlayerMovementOperator : MovementOperator
    {
        [Header("Player Movement Operator")]
        [Tooltip("The sound that plays when the player jumps.")]
        public AudioClip JumpSound;
        [Tooltip("The sound that plays when the player lands.")]
        public AudioClip LandSound;
        /// <summary>
        /// Set true to make the player jump, if they can. If this is false 
        /// while the player is jumping, the jump will end early. This is used
        /// to achieve an effect where the player won't jump as high if the
        /// key is not held for as long.
        /// </summary>
        public bool ShouldJump { get; set; }
        /// <summary>
        /// This is true if the player is getting crushed by a surface and 
        /// their position is being corrected.
        /// </summary>
        public bool CorrectingCollision { get; protected set; }
        public static PlayerMovementOperator Main { get; protected set; }
        protected PlayerOperator PlayerOperator { get; set; }
        private bool _groundedLastFrame;

        protected override void Awake()
        {
            base.Awake();
            PlayerOperator = GetComponent<PlayerOperator>();
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void Update()
        {
            if (PlayerOperator.Hold) return;

            if (!PlayerOperator.DisableMovementInput)
            {
                // If they press the jump button, and they are on the ground, then init the jump.
                if (ShouldJump && !Jumping)
                {
                    Jump();
                }
            }
        }

        protected override void FixedUpdate()
        {
            SlopeCheck();
            base.FixedUpdate();
            
            if (Jumping && !ShouldJump)
            {
                EndJump();
            }

            if (CheatController.IsActive(Cheat.ThiccWynne) && Grounded && !_groundedLastFrame)
            {
                CameraOperator.ActiveCamera.Shake(0.25f, 0.05f, 0.5f);
                SoundEffect.PlaySound(LandSound, GetComponent<TimeOperator>());
            }

            _groundedLastFrame = Grounded;
        }
    }
}
