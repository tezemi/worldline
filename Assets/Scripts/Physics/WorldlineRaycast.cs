﻿using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// Represents an origin and direction or destination of a raycast. Can 
    /// be used to both draw a raycast or linecast, and also debug the cast.
    /// </summary>
    public struct WorldlineRaycast
    {
        /// <summary>
        /// The LayerMask(s) this Raycast will detect.
        /// </summary>
        public int LayerMask;
        /// <summary>
        /// The distance of the raycast, assuming the cast is a raycast and 
        /// not a linecast.
        /// </summary>
        public float Distance;
        /// <summary>
        /// The origin of the raycast.
        /// </summary>
        public Vector2 Origin;
        /// <summary>
        /// Either the direction of the raycast, or the destination of the linecast.
        /// </summary>
        public Vector2 Other;

        public Vector2 Direction
        {
            get
            {
                return (Other - Origin).normalized;
            }
        }

        public WorldlineRaycast(Vector2 origin, Vector2 other, float distance, int layerMask)
        {
            LayerMask = layerMask;
            Distance = distance;
            Origin = origin;
            Other = other;
        }

        public void DebugDrawRay(Color color, float duration)
        {
            Debug.DrawLine(Origin, Origin + Other, color, duration);
        }

        public void DebugDrawLine(Color color, float duration)
        {
            Debug.DrawLine(Origin, Other, color, duration);
        }

        public RaycastHit2D Raycast()
        {
            return Physics2D.Raycast(Origin, Other, Distance, LayerMask);
        }

        public RaycastHit2D Linecast()
        {
            return Physics2D.Linecast(Origin, Other, LayerMask);
        }

        public RaycastHit2D[] LinecastAll()
        {
            return Physics2D.LinecastAll(Origin, Other, LayerMask);
        }

        public static void DebugDrawPoint(Vector2 point, Color color, float size, float duration)
        {
            Debug.DrawLine(point - new Vector2(size, size), point + new Vector2(size, size), color, duration);
            Debug.DrawLine(point - new Vector2(-size, size), point - new Vector2(size, -size), color, duration);
        }
    }
}
