// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Worldline.TimeEngine;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.Physics
{
    /// <summary>
    /// Handles basic physics movements. This componenet essentially acts 
    /// as a way to interface with a GameObject and apply game-specific
    /// movement to it. If the GameObject has a TimeOperator, this will
    /// also handle time-specific movements.
    /// </summary>
    [DisableOnPlayback]
    [DisableOnRewinding]
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    public class MovementOperator : MonoBehaviour
    {
        [Header("Movement Operator")]
        [Tooltip("Whether or not this GameObject can be grounded. False is the GameObject flies.")]
        public bool CanBeGrounded = true;
        [Tooltip("If true, the game will render the raycasts in the scene view for surface detection.")]
        public bool DebugSurfaceDetection;
        [Tooltip("Render debugging tools for ceiling detection.")]
        public bool DebugCeilingDetection;
        [Tooltip("Render debugging tools for detecting surface angles.")]
        public bool DebugAngleCollection;
        [Tooltip("Render debugging tools for detecting sloped surfaces.")]
        public bool DebugSlopeDetection;
        [Tooltip("The amount of frames spent jumping.")]
        public int JumpHeight = 13;
        [Tooltip("The amount of velocity applied to this GameObject upwards when Jump() is called.")]
        public float JumpPower = 9.0f;
        [Tooltip("The amount of velocity changed each frame when the speed has changed.")]
        public float Acceleration = 0.5f;
        [Tooltip("The acceleration gets subtracted by this when not grounded.")]
        public float JumpMobilityLoss = 0.2f;
        [Tooltip("Defines what collisions should be counted as surfaces.")]
        public LayerMask GroundDetectionLayerMask;
        [Tooltip("Defines what other collisions should be ignored.")]
        public string[] IgnoreCollisions = { "Item", "NPC", "Player" };
        [Tooltip("Defines what other triggers should be ignored.")]
        public string[] IgnoreTriggers;
        /// <summary>
        /// If true, this GameObject will not be affected by speed.
        /// </summary>
        public bool Hold { get; set; }
        /// <summary>
        /// Whether or not the GameObject is currently grounded on a surface.
        /// </summary>
        public bool Grounded { get; set; }
        /// <summary>
        /// The speed to move to at acceleration.
        /// </summary>
        public Vector2 Speed { get; set; }
        /// <summary>
        /// Whether or not this GameObject is jumping. This is different from 
        /// not being grounded as jumping only occurs when Jump() is called
        /// and it propels the GameObject upwards.
        /// </summary>
        public bool Jumping { get; protected set; }
        /// <summary>
        /// Whether or not this GameObject is resting on a sloped surface.
        /// </summary>
        public bool OnSlope { get; protected set; }
        /// <summary>
        /// This is true when this GameObject is being slowed because it isn't 
        /// grounded.
        /// </summary>
        public bool SlowedWhileNotGrounded { get; protected set; }
        /// <summary>
        /// Whether or not this GameObject is resting on a moving platform.
        /// </summary>
        public bool IsOnMovingPlatform { get; protected set; }
        /// <summary>
        /// The amount of frames this GameObject has been jumping. Will be zero 
        /// if the GameObject is not jumping.
        /// </summary>
        public int FramesSpentJumping { get; protected set; }
        /// <summary>
        /// When detecting surfaces underneath the GameObject, raycasts are 
        /// projected in a "X" shape. This the distance from the bottom of
        /// the GameObject those raycasts will travel.
        /// </summary>
        public const float SurfaceRaycastDistance = 0.25f;
        /// <summary>
        /// When detecting ceilings above GameObjects, this is the distance 
        /// the raycasts will travel.
        /// </summary>
        public const float CeilingRaycastDistance = 0.05f;
        /// <summary>
        /// The raycasts that check to see if this GameObject is resting on a 
        /// surface need to be slightly elevated a bit from the bottom of the
        /// collider, as to account for edge colliders which have a one pixel
        /// height.
        /// </summary>
        public const float RaycastStartOffset = 0.05f;
        /// <summary>
        /// When detecting if this GameObject is resting a slope, a raycast is 
        /// used going directly down the center of the GameObject to determine
        /// where the GameObject should rest. This is the length of that raycast.
        /// </summary>
        public const float MonorailRaycastLength = 1f;
        /// <summary>
        /// When detecting sloped surfaces, two raycasts are projected slightly
        /// next to and above the GameObject's minimum Y points. This is the
        /// amount of vertical offset.
        /// </summary>
        public const float SlopeRaycastOffset = 0.25f;
        /// <summary>
        /// The minimum exclusive angle of a surface before it is considered to 
        /// be a slope.
        /// </summary>
        public const float MinSlope = 0f;
        /// <summary>
        /// The maximum inclusive angle of a surface before it is considered to 
        /// be a slope.
        /// </summary>
        public const float MaxSlope = 45f;
        protected Collider2D Collider { get; set; }
        protected Rigidbody2D Rigidbody { get; set; }
        [CanBeNull]
        protected TimeOperator TimeOperator { get; set; }
        protected Collider2D RestingOn { get; set; }
        /// <summary>
        /// The duration for which raycasts are displayed each frame when debugging.
        /// </summary>
        protected const float RaycastDebugDuration = 0.02f;
        private bool _setSlopePosition;
        private readonly RaycastHit2D[] _slopeRaycastHits = new RaycastHit2D[3];
        private readonly RaycastHit2D[][] _slopeRaycastAllHits = new RaycastHit2D[3][];
        private readonly List<Collider2D> _wallColliders = new List<Collider2D>();
        private readonly List<Collider2D> _disabledColliders = new List<Collider2D>();
        private static List<MovementOperator> _allMovementOperators = new List<MovementOperator>();

        /// <summary>
        /// A list that consistently contains all currently active MovementOperators.
        /// </summary>
        public static List<MovementOperator> AllMovementOperators
        {
            get
            {
                _allMovementOperators.RemoveAll(n => n == null);

                return _allMovementOperators;
            }
            set => _allMovementOperators = value;
        }

        protected virtual void Awake()
        {
            AllMovementOperators.Add(this);
            Rigidbody = GetComponent<Rigidbody2D>();
            Collider = GetComponent<BoxCollider2D>();
            TimeOperator = GetComponent<TimeOperator>();
            foreach (Collider2D col in GetComponents<Collider2D>())
            {
                if (!col.isTrigger)
                {
                    Collider = col;
                }
            }
        }

        protected virtual void OnEnable()
        {
            UpdateCollisions();
        }

        protected virtual void FixedUpdate()
        {
            if (CanBeGrounded)
            {
                SlopeCheck();
            }

            if (Hold || Rigidbody.bodyType == RigidbodyType2D.Static) return;

            // Horizontal Movement
            if (Speed.x != Rigidbody.velocity.x)
            {
                Rigidbody.velocity = new Vector2
                (
                    Mathf.MoveTowards(Rigidbody.velocity.x, Speed.x, Acceleration), 
                    Rigidbody.velocity.y
                ); 
            }

            // Vertical Movement
            if ((Speed.y != 0 || !CanBeGrounded) && Speed.y != Rigidbody.velocity.y)
            {
                Rigidbody.velocity = new Vector2
                (
                    Rigidbody.velocity.x,
                    Mathf.MoveTowards(Rigidbody.velocity.y, Speed.y, Acceleration)
                );
            }

            // Jumping
            if (Jumping)
            {
                FramesSpentJumping++;
                const float slowJumpScale = 1.25f;
                if (TimeOperator != null && TimeOperator.TimeScale < 1f && TimeOperator.TimeScale > 0f)
                {
                    Rigidbody.velocity = new Vector2(Rigidbody.velocity.x, JumpPower / TimeOperator.TimeScale / slowJumpScale);
                }
                else
                {
                    Rigidbody.velocity = new Vector2(Rigidbody.velocity.x, JumpPower);
                }
            }

            if (FramesSpentJumping >= JumpHeight)
            {
                EndJump();
            }

            if (CanBeGrounded)
            {
                Physics2D.queriesHitTriggers = false; // Looking for solid ground, not triggers
                
                #region Ceiling Collision Check
                var ceilingLowerToUpperRaycast = new WorldlineRaycast
                (
                    Collider.bounds.min + new Vector3(0f, Collider.bounds.size.y, 0f),
                    Collider.bounds.max + new Vector3(0f, CeilingRaycastDistance, 0f),
                    0f,
                    GroundDetectionLayerMask
                );

                var ceilingLowerToUpperHit = ceilingLowerToUpperRaycast.Linecast();
                var ceilingUpperToLowerRaycast = new WorldlineRaycast
                (
                    Collider.bounds.max,
                    Collider.bounds.min + new Vector3(0f, Collider.bounds.size.y + CeilingRaycastDistance, 0f),
                    0f,
                    GroundDetectionLayerMask
                );

                var ceilingUpperToLowerHit = ceilingUpperToLowerRaycast.Linecast();
                Collider2D other = ceilingLowerToUpperHit ? ceilingLowerToUpperHit.collider : ceilingUpperToLowerHit.collider;
                if (Rigidbody.velocity.y > 0f && (ceilingLowerToUpperHit || ceilingUpperToLowerHit) && other.GetComponent<SolidTop>() == null)
                {
                    EndJump();
                }
                #endregion

                NormalSurfaceCheck(true);

                #region Debugging
                if (DebugCeilingDetection)
                {
                    ceilingLowerToUpperRaycast.DebugDrawLine(ceilingLowerToUpperHit ? Color.green : Color.red, RaycastDebugDuration);
                    ceilingUpperToLowerRaycast.DebugDrawLine(ceilingUpperToLowerHit ? Color.green : Color.red, RaycastDebugDuration);
                }
                #endregion

                Physics2D.queriesHitTriggers = true;

                #region Jump Mobility
                // Jump Mobility Loss
                if (!Grounded && !SlowedWhileNotGrounded)
                {
                    Acceleration -= JumpMobilityLoss;
                    SlowedWhileNotGrounded = true;
                }

                // Jump Loss Regain
                if (Grounded && SlowedWhileNotGrounded)
                {
                    Acceleration += JumpMobilityLoss;
                    SlowedWhileNotGrounded = false;
                }
                #endregion
            }
        }

        /// <summary>
        /// Updates collision between this GameObject and any slanted surfaces
        /// that aren't walls. Works by finding raycasts that are touching
        /// non-wall surfaces and applying the desired collision. Also checks
        /// to see if any additional hits are detected if collision should be
        /// disabled and a hit is detected on a slope. This fixes collisions
        /// with solids underneath slopes.
        /// </summary>
        /// <param name="hits">The raycast data to check.</param>
        /// <param name="allHits">An array of raycast data to check.</param>
        /// <param name="disableCollision">The desired collision.</param>
        private void UpdateRaycastCollision(RaycastHit2D[] hits, RaycastHit2D[][] allHits, bool disableCollision)
        {
            _wallColliders.Clear();
            for (var i = 0; i < hits.Length; i++)
            {
                RaycastHit2D hit = hits[i];
                if (!hit || hit.collider.GetComponent<SolidTop>() != null) continue;

                if (!Physics2D.GetIgnoreCollision(Collider, hit.collider) && GetAngleOfSurface(hit) <= MaxSlope || !disableCollision)
                {
                    Physics2D.IgnoreCollision(Collider, hit.collider, disableCollision);
                    if (disableCollision && !_disabledColliders.Contains(hit.collider))
                    {
                        _disabledColliders.Add(hit.collider);
                    }
                }
                else if (GetAngleOfSurface(hit) > MaxSlope || hit.distance <= 0f)
                {
                    Physics2D.IgnoreCollision(Collider, hit.collider, false);
                    _wallColliders.Add(hit.collider);    // Collider is wall
                    if (_disabledColliders.Contains(hit.collider))
                    {
                        _disabledColliders.Remove(hit.collider);
                    }
                }

                // This checks to see if there are any colliders underneath the slope that
                // could accidentally be collided with, creating a clipping effect.
                if (disableCollision && GetAngleOfSurface(hit) <= MaxSlope)
                {
                    foreach (RaycastHit2D allHit in allHits[i])
                    {
                        // If the collider was already determined to be a slope or not,
                        // in the above code, or the collider was already determined to
                        // be part of a wall that is actively being touched, then don't
                        // ignore it. We never want to ignore the collision between 
                        // an object and a wall, and at no point will an object be touching
                        // both a wall and a collider underneath the slope.
                        if (allHit && allHit.collider.GetComponent<SolidTop>() == null && 
                        allHit.collider != hit.collider && !_wallColliders.Contains(allHit.collider))
                        {
                            Physics2D.IgnoreCollision(Collider, allHit.collider, true);
                            if (!_disabledColliders.Contains(allHit.collider))
                            {
                                _disabledColliders.Add(allHit.collider);
                            }

                            break;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns whether or not a raycast is touching a valid slope.
        /// </summary>
        /// <param name="hit">The hit data.</param>
        /// <returns>True if the raycast is touching a valid slope.</returns>
        private bool RaycastIsTouchingSlope(RaycastHit2D hit)
        {
            return GetAngleOfSurface(hit) > MinSlope && GetAngleOfSurface(hit) <= MaxSlope && hit.distance > 0f;
        }

        /// <summary>
        /// Gets the angle of a surface a raycast is hitting.
        /// </summary>
        /// <param name="hit">The hit data.</param>
        /// <returns>The angle of the surface.</returns>
        private float GetAngleOfSurface(RaycastHit2D hit)
        {
            Vector2 normal = hit.normal;
            if (normal.y < 0f)
            {
                normal = -normal; 
            }

            if (DebugAngleCollection)
            {
                Debug.DrawRay(hit.point, normal, Color.red, RaycastDebugDuration);
            }

            return Vector2.Angle(normal, Vector2.up);
        }

        /// <summary>
        /// Checks to see if the GameObject is on a surface, and is resting on 
        /// that surface, updating the Grounded property.
        /// </summary>
        /// <param name="velocityIndicatesGrounding">If true, this GameObject will
        /// not be considered Grounded unless their vertical velocity is at zero.</param>
        protected void NormalSurfaceCheck(bool velocityIndicatesGrounding)
        {
            var surfaceUpperToLowerRaycast = new WorldlineRaycast
            (
                Collider.bounds.min - new Vector3(RaycastStartOffset, 0f, 0f),
                Collider.bounds.min + new Vector3(Collider.bounds.size.x, 0f, 0f) - new Vector3(0f, SurfaceRaycastDistance, 0f),
                0f,
                GroundDetectionLayerMask
            );

            var surfaceUpperToLowerHit = surfaceUpperToLowerRaycast.Linecast();
            var surfaceLowerToUpperRaycast = new WorldlineRaycast
            (
                Collider.bounds.min + new Vector3(Collider.bounds.size.x + RaycastStartOffset, 0f, 0f),
                Collider.bounds.min - new Vector3(0f, SurfaceRaycastDistance, 0f),
                0f,
                GroundDetectionLayerMask
            );

            var surfaceLowerToUpperHit = surfaceLowerToUpperRaycast.Linecast();
            var raycast = surfaceUpperToLowerHit ? surfaceUpperToLowerHit : surfaceLowerToUpperHit;
            var distanceToSurface = Collider.bounds.min.y - raycast.point.y;
            const float distanceToBeGrounded = 0.075f;
            if (raycast && (velocityIndicatesGrounding && Rigidbody.velocity.y == 0f || !velocityIndicatesGrounding && distanceToSurface <= distanceToBeGrounded))
            {
                Grounded = true;
                RestingOn = raycast.collider;
                IsOnMovingPlatform = raycast.collider.GetComponent<MovingSurface>() != null;
            }
            else
            {
                Grounded = false;
                RestingOn = null;
                IsOnMovingPlatform = false;
            }

            if (DebugSurfaceDetection)
            {
                surfaceLowerToUpperRaycast.DebugDrawLine(surfaceLowerToUpperHit ? Color.green : Color.red, RaycastDebugDuration);
                surfaceUpperToLowerRaycast.DebugDrawLine(surfaceUpperToLowerHit ? Color.green : Color.red, RaycastDebugDuration);
            }
        }

        /// <summary>
        /// Checks to see if the GameObject is on a slope and makes adjustments 
        /// accordingly.
        /// </summary>
        protected void SlopeCheck()
        {
            const float startOffset = 0.25f;
            const float endOffset = 0.2f;
            var leftSlopeRaycast = new WorldlineRaycast
            (
                Collider.bounds.center - new Vector3(Collider.bounds.size.x / 2f + startOffset, 0f, 0f),
                Collider.bounds.center - new Vector3(Collider.bounds.size.x / 2f - endOffset, Collider.bounds.size.y / 2f + MonorailRaycastLength, 0f),
                0f,
                GroundDetectionLayerMask
            );

            var leftSlopeHit = leftSlopeRaycast.Linecast();
            var leftSlopeHits = leftSlopeRaycast.LinecastAll(); // Used for detecting solids UNDER slopes
            _slopeRaycastHits[0] = leftSlopeHit;
            _slopeRaycastAllHits[0] = leftSlopeHits;
            var rightSlopRaycast = new WorldlineRaycast
            (
                Collider.bounds.center + new Vector3(Collider.bounds.size.x / 2f + startOffset, 0f, 0f),
                Collider.bounds.center - new Vector3(-Collider.bounds.size.x / 2f + endOffset, Collider.bounds.size.y / 2f + MonorailRaycastLength, 0f),
                0f,
                GroundDetectionLayerMask
            );

            var rightSlopeHit = rightSlopRaycast.Linecast();
            var rightSlopeHits = rightSlopRaycast.LinecastAll();
            _slopeRaycastHits[1] = rightSlopeHit;
            _slopeRaycastAllHits[1] = rightSlopeHits;
            var monorailRaycast = new WorldlineRaycast
            (
                Collider.bounds.center,
                Collider.bounds.center - new Vector3(0f, Collider.bounds.size.y / 2f + MonorailRaycastLength, 0f),
                0f,
                GroundDetectionLayerMask
            );

            var monorailHit = monorailRaycast.Linecast();
            var monorailHits = monorailRaycast.LinecastAll();
            _slopeRaycastHits[2] = rightSlopeHit;
            _slopeRaycastAllHits[2] = monorailHits;
            if ((leftSlopeHit || rightSlopeHit) && monorailHit)
            {
                bool raycastIsTouchingSlope = RaycastIsTouchingSlope(leftSlopeHit) || RaycastIsTouchingSlope(rightSlopeHit) || RaycastIsTouchingSlope(monorailHit);
                if (raycastIsTouchingSlope)
                {
                    OnSlope = true;
                    UpdateRaycastCollision
                    (
                        _slopeRaycastHits,
                        _slopeRaycastAllHits,
                        true
                    );

                    const float distanceToBeOnSlope = 0.05f;
                    if (!Jumping && (Collider.bounds.min.y - monorailHit.point.y < distanceToBeOnSlope || _setSlopePosition))
                    {
                        Grounded = true;
                        _setSlopePosition = true;
                        const float mysteryOffset = 0.025f;
                        Rigidbody.velocity = new Vector2(Rigidbody.velocity.x, 0f);
                        transform.position = new Vector3(transform.position.x, monorailHit.point.y - Collider.offset.y + mysteryOffset + Collider.bounds.size.y / 2f, transform.position.z);
                    }
                    else
                    {
                        _setSlopePosition = false;
                    }
                }
                else
                {
                    OnSlope = false;
                    _setSlopePosition = false;
                    UpdateRaycastCollision
                    (
                        _slopeRaycastHits,
                        _slopeRaycastAllHits,
                        false
                    );
                }
            }
            else
            {
                OnSlope = false;
                _setSlopePosition = false;
                UpdateRaycastCollision
                (
                    _slopeRaycastHits,
                    _slopeRaycastAllHits,
                    false
                );

                for (var i = 0; i < _disabledColliders.Count; i++)
                {
                    Collider2D col = _disabledColliders[i];
                    if (col != null)
                    {
                        Physics2D.IgnoreCollision(col, Collider, false);
                    }

                    _disabledColliders.Remove(col);
                }
            }

            if (DebugSlopeDetection)
            {
                leftSlopeRaycast.DebugDrawLine(leftSlopeHit ? Color.green : Color.red, RaycastDebugDuration);
                rightSlopRaycast.DebugDrawLine(rightSlopeHit ? Color.green : Color.red, RaycastDebugDuration);
                monorailRaycast.DebugDrawLine(monorailHit ? Color.green : Color.red, RaycastDebugDuration);
            }
        }

        /// <summary>
        /// Configures the colliders on this MovementOperator to ignore or not 
        /// ignore other colliders in the scene based on their tags. Kind of
        /// expensive, so don't call this every frame.
        /// </summary>
        public virtual void UpdateCollisions()
        {
            Collider2D[] colliders = FindObjectsOfType<Collider2D>();
            if (colliders == null) return;

            foreach (Collider2D other in colliders)
            {
                if (other.gameObject == gameObject) continue;

                if (other.isTrigger && IgnoreTriggers.Contains(other.tag) || !other.isTrigger && IgnoreCollisions.Contains(other.tag))
                {
                    Physics2D.IgnoreCollision(Collider, other, true);
                }
                else
                {
                    Physics2D.IgnoreCollision(Collider, other, false);
                }
            }
        }

        /// <summary>
        /// Causes this GameObject to jump, if it can.
        /// </summary>
        public virtual void Jump()
        {
            if (Hold || !Grounded) return;

            Jumping = true;
        }

        /// <summary>
        /// Forces this GameObject to jump.
        /// </summary>
        /// <param name="frameJump">Override the number of frames spent jumping, which
        /// normally starts at zero.</param>
        public virtual void ForceJump(int frameJump = 0)
        {
            FramesSpentJumping = frameJump;
            Jumping = true;
        }

        /// <summary>
        /// If this GameObject is jumping, this ends the jumping, and resets 
        /// the jumping frames.
        /// </summary>
        public virtual void EndJump()
        {
            FramesSpentJumping = 0;
            Jumping = false;
        }

        /// <summary>
        /// Forces the GameObject to ground itself by looking for the nearest 
        /// upward or downward surfaces and resting on it.
        /// </summary>
        /// <param name="speed">The amount per frame the GameObject is 
        /// adjusted until it is grounded. Increases improves performance but loses accuracy.</param>
        public virtual void Ground(float speed)
        {
            int debugCount = 0;
            float yUp = transform.position.y;
            float yDown = transform.position.y;
            Vector3 startPos = transform.position;
            Physics2D.queriesHitTriggers = false;
            while (!Grounded && debugCount < 100)
            {
                NormalSurfaceCheck(false);
                if (Grounded && RestingOn) break;

                transform.position = new Vector3(transform.position.x, debugCount % 2 == 1 ? yUp : yDown, transform.position.z);
                yUp += speed;
                yDown -= speed;
                debugCount++;
            }

            if (debugCount >= 100)
            {
                transform.position = startPos;
            }

            Physics2D.queriesHitTriggers = true;
        }
    }
}