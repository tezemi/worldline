﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;
using MEC;
using Random = UnityEngine.Random;

namespace Worldline.Level
{
    /// <summary>
    /// A spawn point for essence droplets. You can set how much the droplets
    /// are worth, the amount it will spawn each tick, and the rate of the tick,
    /// as well as the type of essence. You can call Spawn() on a disabled
    /// EssenceDropletSpawnPoint to spawn essence droplets when you desire,
    /// and ensure that they are not spawned during the tick.
    /// </summary>
    public class EssenceDropletSpawnPoint : MonoBehaviour
    {
        public int Amount = 5;
        public int Burst = 3;
        public float Rate = 0.5f;
        public TimeEffect Type;
        public GameObject EssenceDropletPrefab;
        public const string PoolName = "EssenceDropletPool";
        
        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        /// <summary>
        /// The main tick of this component. Spawns essence droplets every Rate. 
        /// </summary>
        /// <returns>IEnumerator to be used as an MEC coroutine.</returns>
        protected virtual IEnumerator<float> Tick()
        {
            start:
            yield return Timing.WaitForSeconds(Rate);
            Spawn();
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        /// <summary>
        /// Spawns an amount of essence droplets based on the Burst field.
        /// </summary>
        /// <param name="random">If true, the type of essence of the droplets will be random.</param>
        public virtual void Spawn(bool random = false, GameObject target = null)
        {
            if (!gameObject.activeSelf) return;

            for (int i = 0; i < Burst; i++)
            {
                GameObject essDrop;
                if (Pool.Has(PoolName))
                {
                    essDrop = Pool.Get(PoolName);
                }
                else
                {
                    essDrop = Instantiate(EssenceDropletPrefab);
                }
                 
                essDrop.SetActive(true);
                if (random)
                {
                    var timeEffects = (TimeEffect[])Enum.GetValues(typeof(TimeEffect));
                    essDrop.GetComponent<EssenceDroplet>().Type = timeEffects[Random.Range(0, 6)];
                }
                else
                {
                    essDrop.GetComponent<EssenceDroplet>().Type = Type;
                }
                
                essDrop.GetComponent<EssenceDroplet>().Amount = Amount;
                essDrop.GetComponent<EssenceDroplet>().Target = target;
                essDrop.transform.position = new Vector3
                (
                    transform.position.x,
                    transform.position.y,
                    transform.position.z + 0.1f
                );
            }
        }
    }
}
