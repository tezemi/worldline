// Copyright (c) 2019 Destin Hebner
namespace Worldline.Level
{
    /// <summary>
    /// A simple enumeration with four directions, up, down, left, and right.
    /// Can be used for a variety of different things.
    /// </summary>
	public enum Direction 
	{
        Up,
        Down,
        Left,
        Right
	}
}
