﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Combat;
using Worldline.Player;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Level
{
    /// <summary>
    /// When attached to a GameObject, it will spawn essence whenever injured,
    /// or based on a certain interval if Seep is enabled.
    /// </summary>
    public class EssenceWell : Combatant
    {
        [Tooltip("If true, this will spawn essence droplets in time-based intervals.")]
        public bool Seep;
        [Tooltip("If true, this well will always spawn essence of a random type.")]
        public bool RandomEffect;
        [Tooltip("The total amount of essence this well will give.")]
        public int Amount = 100;
        [Tooltip("How much each essence is spawned on taking damange.")]
        public int AmountOnHit = 15;
        [Tooltip("The type of essence to spawn, assuming RandomEffect is false.")]
        public TimeEffect Type;
        public int AmountGiven { get; set; }
        public TimeEffect RandomEffectColor { get; set; } = TimeEffect.Pause;
        public const float ZPosition = 0.25f;
        public const float RandomColorLerp = 0.04f;
        public const float PathDistanceBuffer = 3f;
        public const float VerticalHoverSpeed = 1f;
        public const float HorizontalHoverSpeed = 0.5f;
        public const float VerticalHoverDistance = 0.5f;        
        public const float HorizontalHoverDistance = 0.5f;        
        protected Vector3 HoverPosition { get; set; }
        
        protected override void Awake()
        {
            if (Fired)
            {
                Destroy(gameObject);
                return;
            }

            base.Awake();
            MovementOperator = GetComponent<MovementOperator>();
            transform.position = new Vector3(transform.position.x, transform.position.y, ZPosition);
        }

        protected virtual void OnEnable()
        {
            HoverPosition = transform.position;
            MovementOperator.Speed = new Vector2(Random.Range(-HorizontalHoverSpeed, HorizontalHoverSpeed), Random.Range(-VerticalHoverSpeed, VerticalHoverSpeed));
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            base.Update();

            if (CombatantTimeOperator.NoEffect)
            {
                if (!RandomEffect)
                {
                    GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[Type];
                }
                else
                {
                    GetComponent<SpriteRenderer>().color = Vector4.Lerp
                    (
                        GetComponent<SpriteRenderer>().color,
                        TimeOperator.EffectColors[RandomEffectColor],
                        RandomColorLerp
                    );

                    if (Random.value < 0.035f)
                    {
                        RandomEffectColor = (TimeEffect)Random.Range(0, 6);
                    }
                }
            }

            if (Seep && EssenceOperator.Main != null && EssenceOperator.Main.EssenceAmounts[Type] < Amount && 
            !EssenceOperator.Main.AnyEffectIsActive && CameraOperator.ActiveCamera.OnScreen(gameObject) && 
            EssenceDroplet.ActiveEssenceDroplets.Count <= 0 && !CombatantTimeOperator.ShouldHold)
            {
                Deploy();
            }

            if (!CombatantTimeOperator.ShouldHold)
            {
                if (transform.position.y < HoverPosition.y - VerticalHoverDistance)
                {
                    MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, VerticalHoverSpeed);
                }

                if (transform.position.y > HoverPosition.y + VerticalHoverDistance)
                {
                    MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, -VerticalHoverSpeed);
                }

                if (transform.position.x < HoverPosition.x - HorizontalHoverDistance)
                {
                    MovementOperator.Speed = new Vector2(HorizontalHoverSpeed, MovementOperator.Speed.y);
                }

                if (transform.position.x > HoverPosition.x + HorizontalHoverDistance)
                {
                    MovementOperator.Speed = new Vector2(-HorizontalHoverSpeed, MovementOperator.Speed.y);
                }
            }
        }

        public override void Hurt(int damage, object other)
        {
            if (CombatantTimeOperator.IsRewindEffect) return;

            FireID();   // When the player damages this, it will not spawn again, which prevents easy farming
            AmountGiven += AmountOnHit;
            Deploy();
            if (AmountGiven >= Amount)
            {
                Die();
            }
        }

        public virtual void Deploy()
        { 
            GetComponent<EssenceDropletSpawnPoint>().Type = RandomEffect ? (TimeEffect)Random.Range(0, 6) : Type;
            GetComponent<EssenceDropletSpawnPoint>().Amount = AmountOnHit / GetComponent<EssenceDropletSpawnPoint>().Burst;
            GetComponent<EssenceDropletSpawnPoint>().Spawn(RandomEffect);
        }

        public class EssenceWellSnapshot : CombatSnapshot
        {
            public int AmountGivenValue { get; set; }
            public TimeEffect RandomEffectColorValue { get; set; }
            
            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                AmountGivenValue = gameObject.GetComponent<EssenceWell>().AmountGiven;
                RandomEffectColorValue = gameObject.GetComponent<EssenceWell>().RandomEffectColor;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<EssenceWell>().AmountGiven = AmountGivenValue;
                gameObject.GetComponent<EssenceWell>().RandomEffectColor = RandomEffectColorValue;
            }
        }
    }
}
