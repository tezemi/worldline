﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Level.Puzzles
{
    /// <summary>
    /// A Switch is a component that can be attached to a GameObject and will
    /// check to see if the player enters its collision. When this is true,
    /// the switch will be set to on, and it's sprite will change. When the player
    /// leaves, the switch will turn off. When switches are set to a completed
    /// state, this means that the puzzle is solved and can no longer be effected
    /// by the player.
    /// </summary>
    public class Switch : MonoBehaviour
    {
        public Sprite OnSprite;
        public Sprite OffSprite;
        public Sprite CompleteSprite;
        public AudioClip SwitchOn;
        public AudioClip SwitchOff;
        public bool PermanentlyCompleted { get; set; }
        public List<GameObject> ObjectsInside { get; } = new List<GameObject>();
        private CoroutineHandle _flipCoroutineHandle;

        private bool _on;
        public virtual bool On
        {
            get => _on;
            set
            {
                if (PermanentlyCompleted || _on == value) return;

                Debug.Log($"Changing switch from {(_on ? "On" : "Off")} to {(value ? "On" : "Off")}.");
                Timing.KillCoroutines(_flipCoroutineHandle);
                _flipCoroutineHandle = Timing.RunCoroutine(Flip(), Segment.FixedUpdate);

                IEnumerator<float> Flip()
                {
                    if (GetComponent<TimeOperator>() != null && GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Pause)
                    {
                        yield return Timing.WaitForSeconds(0f);
                    }

                    if (GetComponent<TimeOperator>() != null && GetComponent<TimeOperator>().CurrentTimeEffect != TimeEffect.Rewind)
                    {
                        if (!_on && value && SwitchOn != null)
                        {
                            Debug.Log("Playing in the SwitchOn sound effect.");
                            SoundEffect.PlaySound(SwitchOn, GetComponent<TimeOperator>());
                        }

                        if (_on && !value && SwitchOff != null)
                        {
                            SoundEffect.PlaySound(SwitchOff, GetComponent<TimeOperator>());
                        }
                    }

                    GetComponent<SpriteRenderer>().sprite = value ? OnSprite : OffSprite;
                    _on = value;
                }
            }
        }

        private bool _completed;
        public bool Completed
        {
            get => _completed;
            set
            {
                GetComponent<SpriteRenderer>().sprite = value ? CompleteSprite : On ? OnSprite : OffSprite;
                _completed = value;
            }
        }

        protected virtual void Update()
        {
            if (GetComponent<TimeOperator>() != null && GetComponent<TimeOperator>().ShouldHold || PermanentlyCompleted)
            {
                return;
            }

            if (!On && ObjectsInside.Count > 0)
            {
                On = true;
            }
            else if (On && ObjectsInside.Count <= 0)
            {
                On = false;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<PlayerOperator>() != null && !ObjectsInside.Contains(other.gameObject))
            {
                var timeOperator = other.GetComponent<PlayerTimeOperator>();
                if (!timeOperator.IsResetClone || timeOperator.IsInsideAura && timeOperator.IsInsideAura)
                {
                    ObjectsInside.Add(other.gameObject);
                }
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            if (other.GetComponent<PlayerTimeOperator>() != null)
            {
                var timeOperator = other.GetComponent<PlayerTimeOperator>();
                if (timeOperator.IsResetClone && !timeOperator.IsInsideAura && ObjectsInside.Contains(other.gameObject))
                {
                    ObjectsInside.Remove(other.gameObject);
                }
                else if (timeOperator.IsResetClone && timeOperator.IsInsideAura && !ObjectsInside.Contains(other.gameObject))
                {
                    ObjectsInside.Add(other.gameObject);
                }
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<PlayerOperator>() != null && ObjectsInside.Contains(other.gameObject))
            {
                ObjectsInside.Remove(other.gameObject);
            }
        }
    }
}
