﻿using System.Collections.Generic;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class TilemapSliderDriver : SwitchDriver
    {
        public List<TilemapSlider> Sliders;

        public override void SetComplete()
        {
            foreach (TilemapSlider s in Sliders)
            {
                s.MoveNext();
            }
        }

        public override void SetIncomplete()
        {
            foreach (TilemapSlider s in Sliders)
            {
                s.MoveNext();
            }
        }

        public override void SetFired()
        {
            foreach (TilemapSlider s in Sliders)
            {
                s.MoveNext();
            }

            foreach (Switch sw in Switches)
            {
                sw.On = true;
            }

            SetSwitchesComplete(true);
            SetPipeZoneColors(CompletedColor);
            Completed = true;
        }
    }
}
