﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.ScriptedSequences.Generic;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class EnemyDoorDriver : PuzzleDriver
    {
        public bool MoveCameraOnComplete;
        public List<SlidingDoor> Doors;
        public List<Enemy> Enemies;
        public int OriginalEnemyCount { get; protected set; }

        protected override void Awake()
        {
            base.Awake();

            OriginalEnemyCount = Enemies.Count;
        }

        protected virtual void Update()
        {
            int deadEnemies = 0;
            foreach (Enemy enemy in Enemies)
            {
                if (enemy == null || enemy.Dead)
                {
                    deadEnemies++;
                }
            }

            if (!Completed && deadEnemies >= OriginalEnemyCount)
            {
                Completed = true;
                SetComplete();
            }
            else if (Completed && deadEnemies < OriginalEnemyCount)
            {
                Completed = false;
                SetIncomplete();
            }
        }

        public override void SetComplete()
        {
            Completed = true;
            if (MoveCameraOnComplete)
            {
                GetComponent<PuzzleCompleteCameraTransition>().PuzzleCompleteAction = OpenDoors;
                GetComponent<PuzzleCompleteCameraTransition>().Activate();

                void OpenDoors()
                {
                    foreach (SlidingDoor door in Doors)
                    {
                        door.Open = !door.Open;
                    }
                }
            }
            else
            {
                foreach (SlidingDoor door in Doors)
                {
                    door.Open = !door.Open;
                }
            }
        }

        public override void SetIncomplete()
        {
            Completed = false;
            foreach (SlidingDoor door in Doors)
            {
                door.Open = !door.Open;
            }
        }

        /// <summary>
        /// Opens all doors, if enemies were setup properly, they should not 
        /// spawn at all.
        /// </summary>
        public override void SetFired()
        {
            MoveCameraOnComplete = false;
            foreach (SlidingDoor door in Doors)
            {
                door.SetOpen(!door.Open);
            }
        }
    }
}
