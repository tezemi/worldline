﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using Worldline.Level.Obstacles;
using Worldline.Combat;
using UnityEngine;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    /// <summary>
    /// TODO: Should probably inherit from some sort of base class like "EnemyDriver."
    /// </summary>
    public class CorruptedZoneDriver : MonoBehaviour
    {
        public List<Enemy> Enemies;
        public List<CorruptedZone> Zones;
        public bool Completed { get; protected set; }
        public bool UncompleteOnResurrect { get; protected set; } = true;
        public int OriginalEnemyCount { get; protected set; }

        protected virtual void Update()
        {
            if (Completed && UncompleteOnResurrect && Enemies.Count(e => e.Dead) < OriginalEnemyCount)
            {
                Uncomplete();
            }

            if (!Completed && Enemies.Count(e => e.Dead) == OriginalEnemyCount)
            {
                Complete();
            }
        }

        public void Complete()
        {
            foreach (CorruptedZone z in Zones)
            {
                z.gameObject.SetActive(false);
            }

            Completed = true; 
        }

        public void Uncomplete()
        {
            foreach (CorruptedZone z in Zones)
            {
                z.gameObject.SetActive(true);
            }

            Completed = false;
        }
    }
}
