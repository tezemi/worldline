﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level.Platforms;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class PlatformReverserDriver : SwitchDriver
    {
        public List<Platform> Platforms;

        public override void SetComplete()
        {
            foreach (Platform platform in Platforms)
            {
                platform.Reverse();
            }
        }

        public override void SetIncomplete()
        {
            foreach (Platform platform in Platforms)
            {
                platform.Reverse();
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
