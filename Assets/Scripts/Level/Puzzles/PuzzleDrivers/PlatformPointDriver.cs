﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level.Platforms;
using UnityEngine;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class PlatformPointDriver : SwitchDriver
    {
        [Tooltip("The point the platform will move to if incomplete.")]
        public int PointA;
        [Tooltip("The point the platform will move to if complete.")]
        public int PointB = 1;
        public List<Platform> Platforms;

        public override void SetComplete()
        {
            foreach (Platform platform in Platforms)
            {
                platform.CurrentPoint = PointB;
            }
        }

        public override void SetIncomplete()
        {
            foreach (Platform platform in Platforms)
            {
                platform.CurrentPoint = PointA;
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
