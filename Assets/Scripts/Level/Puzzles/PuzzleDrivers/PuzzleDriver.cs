﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GameState;
using UnityEngine;
using Worldline.ScriptedSequences;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    /// <summary>
    /// This is the base abstract class for components that implement some 
    /// sort of puzzle behavior. The normal pattern to follow here is that
    /// some action should result in some other event. For example, flipping
    /// all of the switches results in a door being opened.
    /// </summary>
    public abstract class PuzzleDriver : TrackedMonoBehaviour
    {
        public bool PermanentlyComplete;
        public AudioClip CompleteSound;
        /// <summary>
        /// Any PuzzleDrivers in this list also get set complete (or incomplete) 
        /// when this puzzle is set to incomplete.
        /// </summary>
        public List<PuzzleDriver> AlsoCompletes;
        public List<ScriptedSequence> FiredOnComplete;
        private bool _completed;
        
        /// <summary>
        /// Whether or not the current puzzle is in a complete state.
        /// </summary>
        public bool Completed
        {
            get => _completed;
            protected set
            {
                if (value && PermanentlyComplete)
                {
                    FireID();
                    foreach (ScriptedSequence e in FiredOnComplete)
                    {
                        e.Activate();
                    }
                }
                else if (!value && PermanentlyComplete)
                {
                    UnsetID();
                }

                _completed = value;
            }
        }

        protected virtual void Awake()
        {
            if (Fired && PermanentlyComplete)
            {
                SetFired();
            }
        }

        /// <summary>
        /// This method is called when the puzzle is completed and should perform the
        /// completion behavior.
        /// </summary>
        public abstract void SetComplete();
        /// <summary>
        /// This method is called the puzzle was complete, but is no longer completed, 
        /// and should reset the completion behavior.
        /// </summary>
        public abstract void SetIncomplete();
        /// <summary>
        /// This method is called upon Awake() when this driver has already been 
        /// completed, and should implement behavior for immediately setting a
        /// PuzzleDriver to its completed state.
        /// </summary>
        public abstract void SetFired();
    }
}
