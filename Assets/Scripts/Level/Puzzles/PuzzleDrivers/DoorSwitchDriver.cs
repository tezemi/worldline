﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class DoorSwitchDriver : SwitchDriver
    {
        public List<SlidingDoor> Doors;

        public override void SetComplete()
        {
            foreach (SlidingDoor d in Doors)
            {
                d.Open = !d.Open;
            }
        }

        public override void SetIncomplete()
        {
            foreach (SlidingDoor d in Doors)
            {
                d.Open = !d.Open;
            }
        }

        public override void SetFired()
        {
            foreach (SlidingDoor d in Doors)
            {
                d.SetOpen(!d.Open);
            }

            foreach (Switch sw in Switches)
            {
                sw.On = true;
            }

            SetSwitchesComplete(true);
            SetPipeZoneColors(CompletedColor);
            Completed = true;
        }
    }
}
