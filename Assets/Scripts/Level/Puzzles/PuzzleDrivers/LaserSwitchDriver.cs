﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level.Obstacles;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class LaserSwitchDriver : SwitchDriver
    {
        public List<LaserWall> LaserWalls { get; protected set; }

        public override void SetComplete()
        {
            foreach (LaserWall l in LaserWalls)
            {
                l.On = !l.On;
            }
        }

        public override void SetIncomplete()
        {
            foreach (LaserWall l in LaserWalls)
            {
                l.On = !l.On;
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
