﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Level.Platforms;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class PlatformSpeedDriver : SwitchDriver
    {
        public float OffSpeed;
        public float OnSpeed;
        public List<Platform> Platforms;

        public override void SetComplete()
        {
            foreach (Platform l in Platforms)
            {
                if (Math.Abs(l.Speed - OnSpeed) > 0.001f)
                {
                    l.Speed = OnSpeed;
                }
            }
        }

        public override void SetIncomplete()
        {
            foreach (Platform l in Platforms)
            {
                if (Math.Abs(l.Speed - OffSpeed) > 0.001f)
                {
                    l.Speed = OffSpeed;
                }
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
