﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class GameObjectActivatorDriver : SwitchDriver
    {
        public List<GameObject> GameObjects;

        public override void SetComplete()
        {
            foreach (GameObject g in GameObjects)
            {
                if (g == null) continue;

                g.SetActive(!g.activeSelf);
            }
        }

        public override void SetIncomplete()
        {
            foreach (GameObject g in GameObjects)
            {
                if (g == null) continue;

                g.SetActive(!g.activeSelf);
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
