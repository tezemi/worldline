﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level.Obstacles;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    public class TurretSwitchDriver : SwitchDriver
    {
        public List<Turret> Turrets;

        public override void SetComplete()
        {
            foreach (Turret t in Turrets)
            {
                t.TurretTimer.Enabled = !t.TurretTimer.Enabled;
            }
        }

        public override void SetIncomplete()
        {
            foreach (Turret t in Turrets)
            {
                t.TurretTimer.Enabled = !t.TurretTimer.Enabled;
            }
        }

        public override void SetFired()
        {
            SetComplete();
        }
    }
}
