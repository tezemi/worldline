﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.Level.Puzzles.PuzzleDrivers
{
    /// <summary>
    /// This is the abstract base class for all switch drivers, and implements 
    /// the base code for being set to complete when all switches are enabled.
    /// </summary>
    public abstract class SwitchDriver : PuzzleDriver
    {
        public Color OffColor = Color.red;
        public Color OnColor = Color.green;
        public Color CompletedColor = Color.blue;
        public List<Switch> Switches;
        public List<Tilemap> PipeZones;

        protected virtual void Update()
        {
            int enabledSwitches = 0;
            foreach (Switch sw in Switches)
            {
                if (sw.On)
                {
                    enabledSwitches++;
                }
            }

            if (enabledSwitches == Switches.Count && !Completed)
            {
                SetSwitchesComplete(true);
                Completed = true;
                SetComplete();
            }

            if (enabledSwitches < Switches.Count && Completed)
            {
                SetSwitchesComplete(false);
                Completed = false;
                SetIncomplete();
            }

            if (Completed)
            {
                SetPipeZoneColors(CompletedColor);
            }
            else if (enabledSwitches > 0)
            {
                SetPipeZoneColors(OnColor);
            }
            else
            {
                SetPipeZoneColors(OffColor);
            }
        }

        protected void SetPipeZoneColors(Color color)
        {
            foreach (Tilemap pipeZone in PipeZones)
            {
                pipeZone.color = color;
            }
        }

        protected void SetSwitchesComplete(bool complete)
        {
            foreach (Switch sw in Switches)
            {
                sw.Completed = complete;
                sw.PermanentlyCompleted = PermanentlyComplete;
            }
        }
    }
}
