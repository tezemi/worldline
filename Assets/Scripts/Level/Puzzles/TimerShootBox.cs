﻿using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Level.Puzzles
{
    /// <summary>
    /// This is a type of <see cref="ShootBox"/> that will start a 
    /// <see cref="CoroutineTimer"/> when shot. When the timer is up, 
    /// the <see cref="ShootBox"/> will deactivate. Very similar to
    /// the <see cref="TimerSwitch"/> class, which also inherits from
    /// <see cref="Switch"/>.
    /// </summary>
    public class TimerShootBox : ShootBox
    {
        [Tooltip("The delay in seconds before the timer turns this off.")]
        public float Delay = 3;
        [Tooltip("A sprite that is shown when counting down.")]
        public Sprite TickingOffSprite;
        [Tooltip("The sound that is played when the timer is ticking down.")]
        public AudioClip TimerSound;
        /// <summary>
        /// The <see cref="CoroutineTimer"/> used to deactivate the switch after the 
        /// specified number of seconds.
        /// </summary>
        public CoroutineTimer Timer { get; protected set; }
        /// <summary>
        /// The actual <see cref="SoundEffect"/> instance that plays while ticking down. Can
        /// be null if no sound is playing.
        /// </summary>
        public SoundEffect Ticking { get; protected set; }
        private int _flashTicks;
        
        protected virtual void Awake()
        {
            Timer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                if (PermanentlyCompleted) return;

                On = false;
                Ticking?.Stop();
            });
        }

        protected override void Update()
        {
            base.Update(); // Base method does nothing, this is for future changes
            if (PermanentlyCompleted)
            {
                Ticking?.Stop();
            }

            if (!GetComponent<TimeOperator>().ShouldHold && !PermanentlyCompleted &&
            Timer != null && Timer.Enabled && On && _flashTicks++ > (Timer.TimeBetweenCalls - Timer.TimeElapsed) * 30)
            {
                _flashTicks = 0;
                if (GetComponent<SpriteRenderer>().sprite == TickingOffSprite)
                {
                    GetComponent<SpriteRenderer>().sprite = Completed ? CompleteSprite : OnSprite;
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = TickingOffSprite;
                }
            }
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (Timer != null && Timer.Enabled)
            {
                Timer.StopTimer();
                Ticking?.Stop();
            }

            base.OnTriggerEnter2D(other);            
            if (Timer != null && !Timer.Enabled && On)
            {
                Timer.StartTimer(Delay, 1);
                Ticking = SoundEffect.PlaySound(TimerSound, GetComponent<TimeOperator>());
                Ticking.AudioSource.time = Ticking.AudioSource.clip.length - Delay;
            }
        }
    }
}
