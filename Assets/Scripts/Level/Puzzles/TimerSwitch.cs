﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Level.Puzzles
{
    public class TimerSwitch : Switch
    {
        public float Delay = 3;
        public AudioClip TimerSound;
        public CoroutineTimer Timer { get; protected set; }
        public SoundEffect Ticking { get; protected set; }
        private int _flashTicks;

        protected virtual void Awake()
        {
            Timer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                if (PermanentlyCompleted) return;

                On = false;
                Ticking?.Stop();
            });
        }

        protected override void Update()
        {
            if (GetComponent<TimeOperator>().ShouldHold) return;

            if (PermanentlyCompleted)
            {
                Ticking?.Stop();
            }

            if (ObjectsInside.Count > 0)
            {
                On = true;
                Ticking?.Stop();
                Timer.StopTimer();
            }

            if (Timer != null && !Timer.Enabled && On && ObjectsInside.Count <= 0)
            {
                Timer.StartTimer(Delay, 1);
                Ticking = SoundEffect.PlaySound(TimerSound, GetComponent<TimeOperator>());
                Ticking.AudioSource.time = Ticking.AudioSource.clip.length - Delay;
            }

            if (!GetComponent<TimeOperator>().ShouldHold && !PermanentlyCompleted &&
            Timer != null && Timer.Enabled && On && _flashTicks++ > (Timer.TimeBetweenCalls - Timer.TimeElapsed) * 30)
            {
                _flashTicks = 0;
                if (GetComponent<SpriteRenderer>().sprite == OffSprite)
                {
                    GetComponent<SpriteRenderer>().sprite = Completed ? CompleteSprite : OnSprite;
                }
                else
                {
                    GetComponent<SpriteRenderer>().sprite = OffSprite;
                }
            }
        }
    }
}
