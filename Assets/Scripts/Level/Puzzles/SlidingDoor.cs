﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Player.Upgrades;
using MEC;
using UnityEngine;

namespace Worldline.Level.Puzzles
{
    /// <summary>
    /// TODO: Looks jittery because of Segment.FixedUpdate change.
    /// </summary>
    public class SlidingDoor : MonoBehaviour
    {
        public bool StartOpen;
        public bool DisableWallJump;
        public float Speed = .2f;
        public float Lip = -0.2f;
        public Direction Direction = Direction.Down;
        public float Size { get; protected set; }
        public Vector3 OpenPosition { get; protected set; }
        public Vector3 ClosedPosition { get; protected set; }
        protected CoroutineHandle MovingCoroutine { get; set; }

        private bool _open;
        public bool Open
        {
            get => _open;
            set
            {
                if (value == _open)
                {
                    return;
                }

                Timing.KillCoroutines(MovingCoroutine);
                MovingCoroutine = Timing.RunCoroutine(Move(), Segment.Update, GetInstanceID().ToString());
                IEnumerator<float> Move()
                {
                    startMove:
                    if (GetComponent<TimeOperator>().CurrentTimeEffect != TimeEffect.Rewind)
                    {
                        yield return this.WaitForFixedSecondsScaled(Time.deltaTime);
                    }
                    else
                    {
                        yield return Timing.WaitForSeconds(Time.deltaTime);
                    }
                    
                    if (value && transform.position != OpenPosition)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, OpenPosition, Speed);
                        goto startMove;
                    }

                    if (!value && transform.position != ClosedPosition)
                    {
                        transform.position = Vector3.MoveTowards(transform.position, ClosedPosition, Speed);
                        goto startMove;
                    }
                }

                _open = value;
            }
        }

        protected virtual void Awake()
        {
            ClosedPosition = transform.position;
            Size = transform.localScale.y;
            switch (Direction)
            {
                case Direction.Down:
                    OpenPosition = ClosedPosition + new Vector3(0f, -Size + Lip, 0f);
                    break;
                case Direction.Up:
                    OpenPosition = ClosedPosition + new Vector3(0f, Size - Lip, 0f);
                    break;
                case Direction.Left:
                    OpenPosition = ClosedPosition + new Vector3(-Size + Lip, 0f, 0f);
                    break;
                case Direction.Right:
                    OpenPosition = ClosedPosition + new Vector3(Size - Lip, 0f, 0f);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (DisableWallJump)
            {
                gameObject.AddComponent<DisableWallJump>();
            }

            if (StartOpen)
            {
                SetOpen(true);
            }
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        /// <summary>
        /// Sets a door to the open or closed position without any transition.
        /// </summary>
        /// <param name="open">If true, the door will open, if false, the door will close.</param>
        public void SetOpen(bool open)
        {
            Open = open;
            transform.position = open ? OpenPosition : ClosedPosition;
        }
    }
}
