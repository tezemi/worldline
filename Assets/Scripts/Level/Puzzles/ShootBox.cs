﻿using Worldline.Combat;
using Worldline.Combat.Projectiles;
using UnityEngine;

namespace Worldline.Level.Puzzles
{
    /// <summary>
    /// A <see cref="ShootBox"/> is a type of <see cref="Switch"/> that activates 
    /// or deactivates depending on whether or not it was hit by a 
    /// <see cref="Projectile"/> of a corresponding 
    /// <see cref="Combat.CombatAlignment"/>.
    /// </summary>
    public class ShootBox : Switch
    {
        [Tooltip("If true, the same projectile will not flip the switch multiple times.")]
        public bool ProjectilesOnlyHitOnce;
        [Tooltip("The CombatAlignment of this ShootBox. Set to EffectsAll if all projectiles should turn this on.")]
        public CombatAlignment CombatAlignment;

        protected override void Update()
        {
            for (int i = 0; i < ObjectsInside.Count; i++)
            {
                GameObject obj = ObjectsInside[i];
                if (obj == null || !obj.activeSelf)
                {
                    ObjectsInside.Remove(obj);
                }
            }
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if (other.GetComponent<Projectile>() != null &&
            Combatant.ShouldEffect(CombatAlignment, other.GetComponent<Projectile>().Alignment) &&
            !ObjectsInside.Contains(other.gameObject))
            {
                On = !On;
                ObjectsInside.Add(other.gameObject);
            }
        }

        protected override void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<Projectile>() != null && ObjectsInside.Contains(other.gameObject) && !ProjectilesOnlyHitOnce)
            {
                ObjectsInside.Remove(other.gameObject);
            }
        }
    }
}
