// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Physics;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Level.Platforms
{
    public class Platform : MonoBehaviour, ICanRessurect
    {
        public bool Smooth;
        public float Speed = 0.1f;
        public float CircleRadius = 4f;
        public float Acceleration = 0.02f;
        public float CircleAngle;
        public PlatformMode Mode;
        public Direction Direction;
        public List<Vector2> Points = new List<Vector2>();
        public bool Dead { get; set; }
        public bool Resurrected { get; set; }
        public bool ChangingDirection { get; set; }
        public bool ChangingPoint { get; set; }
        public int CurrentPoint { get; set; }
        public float SmoothedSpeed { get; set; }
        public bool MovingInReverse { get; protected set; }
        private Direction _direction;
        private Vector3 _startPos;

        protected virtual void Awake()
        {
            _startPos = transform.position;
            if (Mode == PlatformMode.Circle)    // Sets platform to init position
            {
                FixedUpdate();
            }
        }

        protected virtual void OnEnable()
        {
            _direction = Direction;
        }

        protected virtual void Update()
        {
            if (_direction != Direction)
            {
                if (Smooth)
                {
                    ChangingDirection = true;
                }
                else
                {
                    _direction = Direction;
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (GetComponent<TimeOperator>().ShouldHold) return;
            
            float timeBasedAcceleration = GetComponent<TimeOperator>().GetScaledValue(Acceleration);
            if (ChangingDirection)
            {
                SmoothedSpeed = Smooth ? Mathf.MoveTowards(SmoothedSpeed, 0f, timeBasedAcceleration) : Speed;
                if (SmoothedSpeed <= 0f)
                {
                    ChangingDirection = false;
                    _direction = Direction;
                }
            }
            else if (ChangingPoint)
            {
                SmoothedSpeed = Smooth ? Mathf.MoveTowards(SmoothedSpeed, 0f, timeBasedAcceleration) : Speed;
                if (SmoothedSpeed <= 0f)
                {
                    MoveNextPoint();
                    ChangingPoint = false;
                }
            }
            else
            {
                SmoothedSpeed = Smooth ? Mathf.MoveTowards(SmoothedSpeed, Speed, timeBasedAcceleration) : Speed;
            }

            float scaledSpeed = GetComponent<TimeOperator>().GetScaledValue(SmoothedSpeed);
            if (Mode == PlatformMode.Normal)
            {
                switch (_direction)
                {
                    case Direction.Up:
                        transform.position += new Vector3(0f, scaledSpeed, 0f);
                        break;
                    case Direction.Down:
                        transform.position -= new Vector3(0f, scaledSpeed, 0f);
                        break;
                    case Direction.Left:
                        transform.position -= new Vector3(scaledSpeed, 0f, 0f);
                        break;
                    case Direction.Right:
                        transform.position += new Vector3(scaledSpeed, 0f, 0f);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else if (Mode == PlatformMode.Path)
            {
                transform.position = Vector3.MoveTowards
                (
                    transform.position,
                    new Vector3(Points[CurrentPoint].x, Points[CurrentPoint].y, transform.position.z),
                    scaledSpeed
                );

                if (Smooth)
                {
                    if (Vector2.Distance(transform.position, Points[CurrentPoint]) < 1f)
                    {
                        ChangePoint();
                    }
                }
                else
                {
                    if ((Vector2)transform.position == Points[CurrentPoint])
                    {
                        ChangePoint();
                    }
                }
            }
            else if (Mode == PlatformMode.Circle)
            {
                CircleAngle += scaledSpeed * Time.deltaTime;
                Vector3 offset = new Vector3(Mathf.Sin(CircleAngle), Mathf.Cos(CircleAngle), 0f) * CircleRadius;
                transform.position = _startPos + offset;
            }
            else if (Mode == PlatformMode.Snap)
            {
                transform.position = Vector3.MoveTowards
                (
                    transform.position,
                    new Vector3(Points[CurrentPoint].x, Points[CurrentPoint].y, transform.position.z),
                    scaledSpeed
                );
            }

            GetComponent<MovingSurface>().UpdateGameObjectPositions();
        }
        
        /// <summary>
        /// If Mode is set to Path, then this change the point that the platform 
        /// is currently moving towards the next point. If the platform reaches
        /// the final point, and ReverseOnEnd is true, then this will set 
        /// GoingInReverse to true, and begin decrementing the through the list 
        /// of points. If ReverseOnEnd is not true, then this will cycle back to
        /// the first point.
        /// </summary>
        public void ChangePoint()
        {
            if (Smooth)
            {
                ChangingPoint = true;
            }
            else
            {
                MoveNextPoint();
            }
        }

        public void Reverse()
        {
            MovingInReverse = !MovingInReverse;
            MoveNextPoint();
        }

        public void Die()
        {
            if (Resurrected)
            {
                return;
            }

            Dead = true;
            gameObject.SetActive(false);
            DeathTimeOperator.Handle(gameObject, CombatAlignment.Decoration, true);
        }

        public void Resurrect()
        {
            Resurrected = true;
            gameObject.SetActive(true);
            Dead = false;
            Timing.RunCoroutine(FinishRes(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> FinishRes()
            {
                yield return Timing.WaitForSeconds(TimeOperator.StandardResurrectionInvincibilityTime);
                Resurrected = false;
            }
        }

        private void MoveNextPoint()
        {
            if (MovingInReverse)
            {
                CurrentPoint--;
                if (CurrentPoint < 0)
                {
                    CurrentPoint = Points.Count - 1;
                }
            }
            else
            {
                CurrentPoint++;
                if (CurrentPoint >= Points.Count)
                {
                    CurrentPoint = 0;
                }
            }
        }
    }
}
