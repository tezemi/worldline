﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Level.Platforms
{
    /// <summary>
    /// This component will spawn platforms based on a template platform at 
    /// it's position for the specified amount of time. Although this sets
    /// the platform's direction, that is mainly used by this component to
    /// determine when the platform is off screen. Any PlatformMode can be
    /// used for the platforms.
    /// </summary>
    public class PlatformSpawner : MonoBehaviour
    {
        public float SpawnDelay = 6f;
        public float DistanceOffScreen = 4f;
        /// <summary>
        /// Sets the direction of the platform, which is used to check when the 
        /// platform is off screen. Does nothing to the platform unless the
        /// platform's mode is set to Normal.
        /// </summary>
        public Direction Direction;
        /// <summary>
        /// This can either be a prefab, or an inactive platform within the scene.
        /// </summary>
        public GameObject PlatformTemplate;
        
        public List<GameObject> ActivePlatforms { get; } = new List<GameObject>();

        protected virtual void OnEnable()
        {
            CoroutineTimer spawnTimer = new CoroutineTimer(GetComponent<TimeOperator>(), SpawnPlatform);

            spawnTimer.StartTimer(SpawnDelay, () => isActiveAndEnabled);

            SpawnPlatform();
        }
        
        protected virtual void Update()
        {
            ActivePlatforms.RemoveAll(n => n == null);

            foreach (GameObject g in ActivePlatforms)
            {
                if (!g.activeSelf) continue;

                switch (Direction)
                {
                    case Direction.Up:
                        if (g.transform.position.y > LevelController.Main.MapHeightMax + DistanceOffScreen)
                        {
                            g.GetComponent<Platform>().Die();
                        }

                        break;
                    case Direction.Right:
                        if (g.transform.position.x > LevelController.Main.MapWidthMax + DistanceOffScreen)
                        {
                            g.GetComponent<Platform>().Die();
                        }

                        break;
                    case Direction.Left:
                        if (g.transform.position.x < LevelController.Main.MapWidthMin - DistanceOffScreen)
                        {
                            g.GetComponent<Platform>().Die();
                        }

                        break;
                    case Direction.Down:
                        if (g.transform.position.y < LevelController.Main.MapHeightMin - DistanceOffScreen)
                        {
                            g.GetComponent<Platform>().Die();
                        }

                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public void SpawnPlatform()
        {
            GameObject g = Instantiate(PlatformTemplate);
            g.GetComponent<Platform>().Direction = Direction;
            g.transform.position = transform.position;
            g.SetActive(true);
            ActivePlatforms.Add(g);
        }
    }
}
