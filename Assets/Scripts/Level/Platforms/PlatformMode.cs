﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.Level.Platforms
{
    public enum PlatformMode
    {
        /// <summary>
        /// Platforms moves in a specified direction.
        /// </summary>
        Normal,
        /// <summary>
        /// Platforms moves along a specified path defined by a set of points.
        /// </summary>
        Path,
        /// <summary>
        /// Platform moves in a circle using a specified radius.
        /// </summary>
        Circle,
        /// <summary>
        /// Platforms moves to the specified point in a set of points and then stops.
        /// </summary>
        Snap
    }
}
