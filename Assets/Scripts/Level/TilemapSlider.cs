﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Physics;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Level
{
    [RequireComponent(typeof(MovingSurface))]
    [RequireComponent(typeof(TimeOperator))]
    public class TilemapSlider : MonoBehaviour
    {
        [Tooltip("If true, this slider will transition to each point smoothly.")]
        public bool Smooth = true;
        [Tooltip("If true, this slider will not move on its own, but rather wait until something externally tells it to move.")]
        public bool UsedByDriver;
        public float Speed = 0.02f;
        public float DelayOnStart;
        public float DelayBetweenPoints = 1f;
        public Vector2[] Points;
        public bool Waiting { get; set; } = true;
        public int CurrentIndex { get; set; }
        public bool HasTimeOperator { get; protected set; }
        public CoroutineTimer WaitingTimer { get; protected set; }
        protected MovingSurface MovingSurface { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        
        protected virtual void Awake()
        {
            MovingSurface = GetComponent<MovingSurface>();
            TimeOperator = GetComponent<TimeOperator>();
            HasTimeOperator = TimeOperator != null;
        }

        protected virtual void OnEnable()
        {
            WaitingTimer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                Waiting = false;
            });

            WaitingTimer.StartTimer(DelayOnStart, 1);
        }
    
        protected virtual void FixedUpdate()
        {
            if (Waiting || TimeOperator.ShouldHold) return;

            float realSpeed = TimeOperator.GetScaledValue(Speed);
            Vector3 moveTo = new Vector3(Points[CurrentIndex].x, Points[CurrentIndex].y, transform.position.z);
            transform.position = Vector3.MoveTowards
            (
                transform.position,
                moveTo,
                realSpeed
            );

            if ((Vector2)transform.position == Points[CurrentIndex] && !UsedByDriver)
            {
                Waiting = true;
                if (++CurrentIndex >= Points.Length)
                {
                    CurrentIndex = 0;
                }

                WaitingTimer.StartTimer(DelayBetweenPoints, 1);
            }

            MovingSurface?.UpdateGameObjectPositions();
        }

        public virtual void MoveNext()
        {
            Waiting = false;
            if (++CurrentIndex >= Points.Length)
            {
                CurrentIndex = 0;
            }
        }
    }
}
