﻿using System;
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Physics;
using MEC;
using UnityEngine;
using UnityEngine.Serialization;

namespace Worldline.Level
{
    public class ChangeLevel2 : MonoBehaviour
    {
        [FormerlySerializedAs("ExitLevel")]
        [Tooltip("The level to change to when the player leaves this one.")]
        public string Level;
        [Tooltip("The direction at which the player must exit the level in order for a change to occur.")]
        public Direction Direction;
        [Tooltip("The position the player will spawn at in the next level.")]
        public Vector2 ExitPosition;
        /// <summary>
        /// If true, the player is inside the attached <see cref="BoxCollider2D"/> and
        /// if the player leaves the level in the specified direction, the level will
        ///  change.
        /// </summary>
        public bool AwaitingExit { get; protected set; }
        /// <summary>
        /// The amount the player will walk to the left or right when entering a new level.
        /// </summary>
        public const float HorizontalWalkAmount = 3f;
        protected BoxCollider2D BoxCollider { get; set; }

        protected virtual void Awake()
        {
            BoxCollider = GetComponent<BoxCollider2D>();
            if (!BoxCollider.isTrigger)
            {
                Debug.LogWarning($"BoxCollider2D attached to {gameObject.name} was not a trigger.", gameObject);
                BoxCollider.isTrigger = true;
            }
        }

        protected virtual void OnEnable()
        {
            enabled = false;
            Timing.RunCoroutine(Startup(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> Startup()
            {
                yield return Timing.WaitForSeconds(2f);
                yield return Timing.WaitUntilDone(() => PlayerMovementOperator.Main.Grounded);
                enabled = true;
            }
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void FixedUpdate()
        {
            if (!AwaitingExit) return;

            if (Direction == Direction.Up && MainPlayerOperator.MainPlayerObject.transform.position.y > LevelController.Main.MapHeightMax ||
            Direction == Direction.Down && MainPlayerOperator.MainPlayerObject.transform.position.y < LevelController.Main.MapHeightMin ||
            Direction == Direction.Left && MainPlayerOperator.MainPlayerObject.transform.position.x < LevelController.Main.MapWidthMin ||
            Direction == Direction.Right && MainPlayerOperator.MainPlayerObject.transform.position.x > LevelController.Main.MapWidthMax)
            {
                Timing.RunCoroutine(ChangeLevel(), Segment.FixedUpdate);
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                AwaitingExit = true;
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                AwaitingExit = false;
            }
        }

        protected virtual IEnumerator<float> ChangeLevel()
        {
            LevelLoader.Main.PlayerStartPosition = ExitPosition;
            yield return Timing.WaitUntilDone(LevelLoader.Main.LoadLevel(Level, true, true));
            MainPlayerOperator.MainPlayerComponent.OverrideSpeed = true;
            MainPlayerOperator.MainPlayerComponent.DisableMovementInput = true;
            switch (Direction)
            {
                case Direction.Up:
                    break;
                case Direction.Down:
                    break;
                case Direction.Left:
                    while (Vector2.Distance(ExitPosition, MainPlayerOperator.MainPlayerObject.transform.position) < HorizontalWalkAmount)
                    {
                        MainPlayerOperator.MainPlayerComponent.WalkLeft = true;
                        yield return Timing.WaitForOneFrame;
                    }

                    break;
                case Direction.Right:
                    while (Vector2.Distance(ExitPosition, MainPlayerOperator.MainPlayerObject.transform.position) < HorizontalWalkAmount)
                    {
                        MainPlayerOperator.MainPlayerComponent.WalkRight = true;
                        yield return Timing.WaitForOneFrame;
                    }
                    
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            MainPlayerOperator.MainPlayerComponent.OverrideSpeed = false;
            MainPlayerOperator.MainPlayerComponent.DisableMovementInput = false;
        }
    }
}
