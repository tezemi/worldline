﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Level
{
    /// <summary>
    /// Singleton class that tells the level loader where to place
    /// the player, assuming that no other position gets set. This
    /// is most commonly used for debugging, and serves as the default
    /// start position within the level. While you can have multiple
    /// start positions, only the first one will work.
    /// </summary>
    public class PlayerStartPosition : MonoBehaviour
    {
        public static PlayerStartPosition Main;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }
    }
}
