﻿// Copyright (c) 2019 Destin Hebner

using UnityEngine;
using Worldline.Physics;

namespace Worldline.Level.Obstacles
{
    public class CorruptionNode : MonoBehaviour
    {
        public int CorruptionAmount= 10;
        public int CorruptionSpread = 3;
        public int CorruptionTicks = 100;
        public float VerticalHoverDistance = 1f;
        public float HorizontalHoverDistance = 1f;
        public Vector3 Speed = new Vector3(0.5f, 0.5f);
        public EssenceAura EssenceAura;
        public const int TicksBetweenCorruptionTicks = 200;
        protected Vector3 StartPosition { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        private int _corruptionTicks;

        protected virtual void Awake()
        {
            MovementOperator = GetComponent<MovementOperator>();
            StartPosition = transform.position;
            MovementOperator.Speed = new Vector2(Random.Range(-Speed.x, Speed.x), Random.Range(-Speed.y, Speed.y));
        }

        protected virtual void Update()
        {
            Corrupt();
            DoMovement();
            UpdateAura();
        }

        protected virtual void Corrupt()
        {
            if (_corruptionTicks++ >= TicksBetweenCorruptionTicks)
            {
                _corruptionTicks = 0;
            }
        }

        protected virtual void DoMovement()
        {
            if (transform.position.y < StartPosition.y - VerticalHoverDistance)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, Speed.y);
            }

            if (transform.position.y > StartPosition.y + VerticalHoverDistance)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, -Speed.y);
            }

            if (transform.position.x < StartPosition.x - HorizontalHoverDistance)
            {
                MovementOperator.Speed = new Vector2(Speed.x, MovementOperator.Speed.y);
            }

            if (transform.position.x > StartPosition.x + HorizontalHoverDistance)
            {
                MovementOperator.Speed = new Vector2(-Speed.x, MovementOperator.Speed.y);
            }
        }

        protected virtual void UpdateAura()
        {
            if (EssenceAura != null)
            {
                CorruptionSpread = (int)EssenceAura.transform.localScale.x;
            }
        }
    }
}
