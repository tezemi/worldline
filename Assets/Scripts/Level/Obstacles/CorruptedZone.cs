﻿// Copyright (c) 2019 Destin Hebner

using System.Collections.Generic;
using MEC;
using UnityEngine;
using Worldline.Combat;
using Worldline.GameState;
using Worldline.Player;
using Worldline.TimeEngine;
using Worldline.Visuals;

namespace Worldline.Level.Obstacles
{
    public class CorruptedZone : MonoBehaviour
    {
        public bool SpawnTears = true;
        public float MinSpawnTime = 8f;
        public float MaxSpawnTime = 15f;
        public TimeEffect Imbuement = TimeEffect.None;
        public List<GameObject> Tears;
        public List<GameObject> CleanUpOnDeath { get; set; } = new List<GameObject>();
        public bool PlayerIsInside { get; protected set; }
        public const float TearUseChance = 0.5f;
        public const float SpawnSpeed = 0.005f;

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Spawn(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            if (CleanUpOnDeath != null)
            {
                foreach (GameObject g in CleanUpOnDeath)
                {
                    if (g == null) continue;

                    Destroy(g);
                }
            }

            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void OnDrawGizmos()
        {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;
            Gizmos.color = Color.green;
            Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(0f, bounds.size.y));
            Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(0f, bounds.size.y));
            Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(bounds.size.x, 0f));
            Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(bounds.size.x, 0f));
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.isTrigger && other.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                PlayerIsInside = true;
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other.isTrigger && other.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                PlayerIsInside = false;
            }
        }

        protected virtual IEnumerator<float> Spawn()
        {
            start:
            yield return Timing.WaitForSeconds(Random.Range(MinSpawnTime, MaxSpawnTime));
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => PlayerIsInside && !MainPlayerOperator.MainPlayerComponent.InEvent)
            );

            if (SpawnTears)
            {
                GameObject tearToSpawn = null;
                for (int i = Tears.Count - 1; i > 0; i--)
                {
                    if (Random.value < TearUseChance)
                    {
                        tearToSpawn = Tears[i];
                    }
                }

                if (tearToSpawn == null)
                {
                    tearToSpawn = Tears[Tears.Count - 1];
                }

                GameObject spawnedTear = Instantiate(tearToSpawn);
                Vector3 spawnPoint = new Vector3
                (
                    Random.Range(GetComponent<BoxCollider2D>().bounds.min.x, GetComponent<BoxCollider2D>().bounds.max.x),
                    Random.Range(GetComponent<BoxCollider2D>().bounds.min.y, GetComponent<BoxCollider2D>().bounds.max.y),
                    spawnedTear.transform.position.z
                );

                spawnedTear.transform.position = spawnPoint;
                spawnedTear.GetComponent<TrackedMonoBehaviour>().IgnoreFired = true;
                spawnedTear.GetComponent<Enemy>().Imbuement = Imbuement;
                spawnedTear.AddComponent<CorruptionSpawn>();
            }

            if (isActiveAndEnabled)
            {
                goto start;
            }
        }
    }
}
