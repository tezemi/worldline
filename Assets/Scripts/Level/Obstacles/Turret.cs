﻿// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Combat.Projectiles;
using UnityEngine;

namespace Worldline.Level.Obstacles
{
    public class Turret : MonoBehaviour
    {
        public bool FireOffScreen;
        public float Delay = 2f;
        public Direction Direction = Direction.Right;
        public GameObject Projectile;
        public AudioClip FireSound;
        public CoroutineTimer TurretTimer { get; protected set; }
        
        protected virtual void Awake()
        {
            TurretTimer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                if (!FireOffScreen && !CameraOperator.ActiveCamera.OnScreen(transform.position)) return;

                if (FireSound != null)
                {
                    SoundEffect.PlaySound(FireSound, GetComponent<TimeOperator>(), 0.25f);
                }

                GameObject bullet;
                if (Pool.Has(Projectile.GetComponent<Projectile>().DeathPoolName))
                {
                    bullet = Pool.Get(Projectile.GetComponent<Projectile>().DeathPoolName);
                }
                else
                {
                    bullet = Instantiate(Projectile);
                }

                bullet.transform.position = GetComponent<BoxCollider2D>().bounds.center;
                switch (Direction)
                {
                    case Direction.Up:
                        bullet.transform.rotation = Quaternion.Euler(0f, 0f, 90f);
                        bullet.transform.position += new Vector3(0f, 0.2f, 0f);
                        break;
                    case Direction.Down:
                        bullet.transform.rotation = Quaternion.Euler(0f, 0f, -90f);
                        bullet.transform.position -= new Vector3(0f, 0.2f, 0f);
                        break;
                    case Direction.Left:
                        bullet.transform.rotation = Quaternion.Euler(0f, 0f, 180f);
                        bullet.transform.position -= new Vector3(0.2f, 0f, 0f);
                        break;
                    case Direction.Right:
                        bullet.transform.rotation = Quaternion.Euler(0f, 0f, 0f);
                        bullet.transform.position += new Vector3(0.2f, 0, 0f);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                Projectile projectile = bullet.GetComponent<Projectile>();
                if (projectile != null)
                {
                    projectile.DestroyOffScreen = !FireOffScreen;
                }

                bullet.SetActive(true);
            });

            TurretTimer.StartTimer(Delay, () => isActiveAndEnabled);
        }
    }
}

