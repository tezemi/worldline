﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Player;
using Worldline.Utilities;
using Worldline.Corruption;
using MEC;
using UnityEngine;

namespace Worldline.Level.Obstacles
{
    public class Lesion : Enemy
    {
        public int NumberOfParticles = 15;
        public float WeirdSeed = 0.01f;
        public float PortalSpeed = 0.05f;
        public float ScaleAtHalfHealth = 1.5f;
        public float ScaleAtLowHealth = 2f;
        public float MaxParticleSize = 0.1f;
        public float ParticleSpeed = 0.8f;
        public float MaxParticleDistance = 3f;
        public float ParticleResetSize = 0.05f;
        public float MaxCorruption = 0.25f;
        public Vector3 Speed = new Vector3(1f, 1f, 0f);
        public Vector3 MaxDistance = new Vector3(5f, 5f, 0f);
        public SpriteRenderer[] Circles;
        public float DistanceToCorrupt { get; set; } = 16f;
        public Vector3 HoverPosition { get; set; }
        public Vector3 PreferredScale { get; protected set; }
        public Vector3 StartScale { get; protected set; }
        public List<GameObject> Particles { get; protected set; } = new List<GameObject>();
        public const float ScaleChangeSpeed = 0.015f;
        public const float ParticleZPosition = -5f;
        private bool _hurtOnce;
        private bool _hurtTwice;
        private float _lastCorruptionModification;
        private float _amountOfCorruptionSet;

        protected override void Awake()
        {
            base.Awake();

            HoverPosition = transform.position;
            StartScale = transform.localScale;
            PreferredScale = StartScale;
        }

        protected virtual void OnEnable()
        {
            MovementOperator.Speed = new Vector2(Random.value < 0.5f ? -Speed.x : Speed.x, Random.value < 0.5f ? -Speed.y : Speed.y);

            Timing.RunCoroutine(PortalEffect(), Segment.FixedUpdate, GetInstanceID().ToString());
            foreach (GameObject particle in Particles)
            {
                if (particle == null) continue;

                particle.SetActive(true);
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            foreach (GameObject particle in Particles)
            {
                if (particle == null) continue;

                particle.SetActive(false);
            }

            Corrupter.Intensity -= _amountOfCorruptionSet;
            _amountOfCorruptionSet = 0f;
        }

        protected virtual void OnDestroy()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            if (Particles.Count == 0 && GraphicsCorrupter.CorruptedSpriteResources != null)
            {
                for (int i = 0; i < NumberOfParticles; i++)
                {
                    GameObject particle = new GameObject($"Particle {i} for Lesion {GetInstanceID()}", typeof(SpriteRenderer));
                    particle.transform.localScale = new Vector3(MaxParticleSize, MaxParticleSize, 1f);
                    particle.transform.position = transform.position + new Vector3
                    (
                        Random.Range(-MaxParticleDistance, MaxParticleDistance),
                        Random.Range(-MaxParticleDistance, MaxParticleDistance),
                        ParticleZPosition
                    );

                    Sprite randomSprite = null;
                    foreach (Sprite sprite in GraphicsCorrupter.CorruptedSpriteResources.Keys)
                    {
                        randomSprite = sprite;
                        if (Random.value < WeirdSeed) break;
                    }

                    if (randomSprite != null)
                    {
                        particle.GetComponent<SpriteRenderer>().sprite = GraphicsCorrupter.CorruptedSpriteResources[randomSprite].SpriteAtIndex;
                    }

                    Particles.Add(particle);
                }

                Timing.RunCoroutine(ParticleEffect(), Segment.FixedUpdate, GetInstanceID().ToString());
            }

            base.Update();

            float distance = Vector2.Distance(transform.position, MainPlayerOperator.MainPlayerObject.transform.position);
            float localIntensity = MathUtilities.GetRange(distance, 0f, DistanceToCorrupt, 0f, MaxCorruption);
            float changeInIntensity = localIntensity - _lastCorruptionModification;
            _amountOfCorruptionSet += changeInIntensity;
            Corrupter.Intensity += changeInIntensity;
            _lastCorruptionModification = localIntensity;

            if (CombatantTimeOperator.ShouldHold || Dead) return;

            if (transform.position.y < HoverPosition.y - MaxDistance.y)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, Speed.y);
            }

            if (transform.position.y > HoverPosition.y + MaxDistance.y)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, -Speed.y);
            }

            if (transform.position.x < HoverPosition.x - MaxDistance.x)
            {
                MovementOperator.Speed = new Vector2(Speed.x, MovementOperator.Speed.y);
            }

            if (transform.position.x > HoverPosition.x + MaxDistance.x)
            {
                MovementOperator.Speed = new Vector2(-Speed.x, MovementOperator.Speed.y);
            }
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (Dead) return;

            if ((float)Health / MaxHealth <= 0.25f)
            {
                PreferredScale = StartScale * ScaleAtLowHealth;
            }
            else if ((float)Health / MaxHealth <= 0.5f)
            {
                PreferredScale = StartScale * ScaleAtHalfHealth;
            }
            else
            {
                PreferredScale = StartScale;
            }

            transform.localScale = Vector3.MoveTowards(transform.localScale, PreferredScale, ScaleChangeSpeed);
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);

            if (!Dead)
            {
                Vector2 heading = MainPlayerOperator.MainPlayerObject.transform.position - transform.position;
                MovementOperator.Speed = heading;
            }

            // talk about hard coding
            // i wrote that to justify my shit code
            // if i say, "this code is shit," is makes it okay
            if ((float)Health / MaxHealth <= 0.5f && !_hurtOnce)
            {
                Shake(0.15f, 0.35f);
                _hurtOnce = true;
            }

            if ((float)Health / MaxHealth <= 0.25f && !_hurtTwice)
            {
                Shake(0.25f, 0.5f);
                _hurtTwice = true;
            }
        }

        public override void Die()
        {
            if (Dead) return;

            Dead = true;
            TouchDamage = 0;
            MovementOperator.Speed = new Vector2(0f, 0f);

            Timing.RunCoroutine(DeathAnimation(), Segment.FixedUpdate);
        }

        protected IEnumerator<float> PortalEffect()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(PortalSpeed);
                for (int i = 0; i < Circles.Length; i++)
                {
                    var next = i + 1;
                    if (next >= Circles.Length)
                    {
                        next = 0;
                    }

                    Circles[i].color = Circles[next].color;
                }
            }
        }

        protected IEnumerator<float> ParticleEffect()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForOneFrame;
                foreach (GameObject particle in Particles)
                {
                    if (particle == null) continue;

                    var dist = Vector2.Distance(particle.transform.position, transform.position);
                    var zPos = particle.transform.position.z;

                    particle.transform.position = Vector3.MoveTowards
                    (
                        particle.transform.position,
                        transform.position,
                        ParticleSpeed
                    );

                    particle.transform.position = new Vector3(particle.transform.position.x, particle.transform.position.y, zPos);

                    particle.transform.localScale = new Vector3
                    (
                        MathUtilities.GetRange(dist, 0f, MaxParticleDistance, MaxParticleSize, 0f),
                        MathUtilities.GetRange(dist, 0f, MaxParticleDistance, MaxParticleSize, 0f),
                        1f
                    );

                    if (particle.transform.localScale.x <= ParticleResetSize)
                    {
                        particle.transform.position = transform.position + new Vector3
                        (
                            Random.Range(-MaxParticleDistance, MaxParticleDistance),
                            Random.Range(-MaxParticleDistance, MaxParticleDistance),
                            ParticleZPosition
                        );
                    }
                }
            }
        }

        protected IEnumerator<float> DeathAnimation()
        {
            const float speed = 0.12f;
            Vector3 scale = StartScale * ScaleAtLowHealth * 1.5f;
            while (transform.localScale.x <= scale.x - speed)
            {
                yield return Timing.WaitForOneFrame;
                transform.localScale = Vector2.Lerp(transform.localScale, scale, speed);
            }

            const float speed2 = 0.8f;
            while (transform.localScale.x > 0f)
            {
                yield return Timing.WaitForOneFrame;
                transform.localScale = Vector2.MoveTowards(transform.localScale, Vector2.zero, speed2);
            }

            while (Particles.Count > 0)
            {
                yield return Timing.WaitForOneFrame;
                foreach (GameObject particle in Particles)
                {
                    if (particle == null) continue;

                    if (particle.transform.localScale.x <= ParticleResetSize)
                    {
                        Destroy(particle);
                    }
                }

                Particles.RemoveAll(n => n == null);
            }

            Timing.KillCoroutines(GetInstanceID().ToString());
            Destroy(gameObject);
        }
    }
}
