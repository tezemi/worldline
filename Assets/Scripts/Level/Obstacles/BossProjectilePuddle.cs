﻿using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.Level.Obstacles
{
    public class BossProjectilePuddle : MonoBehaviour
    {
        public float TimeAlive = 8f;
        public const float FadeSpeed = 0.02f;
        public const string PoolName = "BossProjectilePuddlePool";
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected AnimationOperator AnimationOperator { get; set; }

        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            AnimationOperator = GetComponent<AnimationOperator>();
        }

        protected virtual void OnEnable()
        {
            SpriteRenderer.color = Color.white;
            AnimationOperator.CurrentFrame = 0;
            AnimationOperator.ForceUpdate();
            AnimationOperator.enabled = true;
            Timing.RunCoroutine(Despawn(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            if (AnimationOperator.CurrentFrame == AnimationOperator.Sprites.Length)
            {
                AnimationOperator.enabled = false;
            }
        }

        protected IEnumerator<float> Despawn()
        {
            yield return Timing.WaitForSeconds(TimeAlive);
            while (SpriteRenderer.color.a > 0f)
            {
                SpriteRenderer.color -= new Color(0f, 0f, 0f, FadeSpeed);
                yield return Timing.WaitForOneFrame;
            }

            gameObject.SetActive(false);
            DeathTimeOperator.Handle(gameObject, CombatAlignment.Enemy, false);
            Pool.Add(gameObject, PoolName, true);
        }
    }
}
