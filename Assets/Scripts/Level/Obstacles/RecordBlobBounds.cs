﻿// Copyright (c) 2019 Destin Hebner

using UnityEngine;
using Worldline.Player;

namespace Worldline.Level.Obstacles
{
    public class RecordBlobBounds : MonoBehaviour
    {
        public EssenceBlob EssenceBlob;
        public GameObject[] Points;

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject != MainPlayerOperator.MainPlayerObject) return;

            Vector3[] points = new Vector3[Points.Length];
            for (var i = 0; i < Points.Length; i++)
            {
                points[i] = Points[i].transform.position;
            }

            EssenceBlob.RecordBlobBoundsTriggerEnter(other, GetComponent<Collider2D>(), points);
        }
    }
}
