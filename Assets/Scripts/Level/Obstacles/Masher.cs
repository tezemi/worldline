﻿// Copyright (c) 2019 Destin Hebner

using System;
using UnityEngine;
using Worldline.TimeEngine;

namespace Worldline.Level.Obstacles
{
    /// <summary>
    /// When attached to a GameObject, the GameObject will move up and down 
    /// based on provided parameters. This is mainly used for the wall with
    /// spikes on the bottom.
    /// </summary>
    [Obsolete("Use a Tilemap with an attached TilemapSlider instead.")]
    public class Masher : MonoBehaviour
    {
        public bool Horizontal;
        public float Speed = 0.05f;
        public float RetractedDelay;
        public float ExtendedDelay;
        public float StartDelay;
        public bool MovingToStart { get; set; }
        public bool Waiting { get; set; } = true;
        public Vector3 StartPosition { get; protected set; }
        public Vector3 OppositePosition { get; protected set; }
        public CoroutineTimer WaitingTimer { get; protected set; }
        public const float Size = 3f;
        
        protected virtual void OnEnable()
        {
            StartPosition = transform.position;
            OppositePosition = StartPosition + new Vector3(Horizontal ? Size : 0f, Horizontal ? 0f : Size, 0f);
            WaitingTimer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                Waiting = false;
            });

            WaitingTimer.StartTimer(StartDelay, 1);
        }

        protected virtual void FixedUpdate()
        {
            if (Waiting) return;

            float realSpeed = GetComponent<TimeOperator>().GetScaledValue(Speed);
            if (MovingToStart)
            {
                Move(StartPosition, realSpeed);
                float endPos = !Horizontal ? Mathf.Abs(transform.position.y - StartPosition.y) : Mathf.Abs(transform.position.x - StartPosition.x);
                if (endPos <= realSpeed)
                {
                    Waiting = true;
                    MovingToStart = false;
                    WaitingTimer.StartTimer(ExtendedDelay, 1);
                }
            }
            else
            {
                Move(OppositePosition, realSpeed);
                float endPos = !Horizontal ? Mathf.Abs(transform.position.y - OppositePosition.y) : Mathf.Abs(transform.position.x - OppositePosition.x);
                if (endPos <= realSpeed)
                {
                    Waiting = true;
                    MovingToStart = true;
                    WaitingTimer.StartTimer(RetractedDelay, 1);
                }
            }
        }

        protected virtual void Move(Vector3 to, float speed)
        {
            if (!Horizontal)
            {
                if (to.y > transform.position.y)
                {
                    transform.position += new Vector3(0f, speed, 0f);
                }
                else if (to.y < transform.position.y)
                {
                    transform.position -= new Vector3(0f, speed, 0f);
                }
            }
            else
            {
                if (to.x > transform.position.x)
                {
                    transform.position += new Vector3(speed, 0f, 0f);
                }
                else if (to.x < transform.position.x)
                {
                    transform.position -= new Vector3(speed, 0f, 0f);
                }
            }
        }
    }
}