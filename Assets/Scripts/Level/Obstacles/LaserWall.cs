﻿// Copyright (c) 2019 Destin Hebner

using UnityEngine;
using Worldline.Player;

namespace Worldline.Level.Obstacles
{
    /// <summary>
    /// TODO: Bring back laser walls!!!
    /// </summary>
    public class LaserWall : MonoBehaviour
    {
        public Sprite LaserLensStart;
        public Sprite LaserLensEnd;
        public Sprite LaserMiddle;
        public int Size = 3;
        public bool InstantKill = true;
        public int Damage = 3;
        public BoxCollider2D Trigger { get; protected set; }
        private SpriteRenderer[] _laserSprites;
        private SpriteRenderer _endSprite;
        private bool _init;

        private bool _sideways;
        public bool Sideways
        {
            get
            {
                return _sideways;
            }
            set
            {
                SetupWall(value);
                _sideways = value;
            }
        }

        private bool _on = true;
        public bool On
        {
            get
            {
                return _on;
            }
            set
            {
                if (_laserSprites != null)
                {
                    foreach (SpriteRenderer s in _laserSprites)
                    {
                        s.enabled = value;
                        s.color = Color.red;
                    }
                }

                _on = value;
            }
        }

        private void Start()
        {
            SetupWall(Sideways);
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (!On) return;

            if (InstantKill && collision.gameObject.GetComponent<MainPlayerOperator>() != null)
            {
                collision.gameObject.GetComponent<MainPlayerOperator>().Die();
            }
        }

        public void SetupWall(bool sideways)
        {
            if (_init)
            {
                Destroy(Trigger);

                if (_endSprite != null) Destroy(_endSprite.gameObject);
                foreach (SpriteRenderer s in _laserSprites)
                {
                    Destroy(s.gameObject);
                }
            }

            if (sideways)
            {
                transform.rotation = Quaternion.Euler(0, 0, 90);
                _laserSprites = new SpriteRenderer[Size - 2];
                for (int i = 1; i < Size; i++)
                {
                    GameObject spriteGameObject = new GameObject("Laser Part", typeof(SpriteRenderer));
                    spriteGameObject.GetComponent<SpriteRenderer>().sprite = i == Size - 1 ? LaserLensEnd : LaserMiddle;
                    spriteGameObject.transform.position = transform.position;
                    spriteGameObject.transform.position += new Vector3(1 * i, 0, 0);
                    spriteGameObject.transform.rotation = Quaternion.Euler(0, 0, 90);
                    if (i != Size - 1) _laserSprites[i - 1] = spriteGameObject.GetComponent<SpriteRenderer>();
                    else _endSprite = spriteGameObject.GetComponent<SpriteRenderer>();
                }

                Trigger = gameObject.AddComponent<BoxCollider2D>();
                Trigger.isTrigger = true;
                Trigger.size = new Vector2(0.3f, Size);
                ApplyOffset();
            }
            else
            {
                _laserSprites = new SpriteRenderer[Size - 2];
                for (int i = 1; i < Size; i++)
                {
                    GameObject spriteGameObject = new GameObject("Laser Part", typeof(SpriteRenderer));
                    spriteGameObject.GetComponent<SpriteRenderer>().sprite = i == Size - 1 ? LaserLensEnd : LaserMiddle;
                    spriteGameObject.transform.position = transform.position;
                    spriteGameObject.transform.position -= new Vector3(0, 1 * i, 0);
                    if (i != Size - 1) _laserSprites[i - 1] = spriteGameObject.GetComponent<SpriteRenderer>();
                    else _endSprite = spriteGameObject.GetComponent<SpriteRenderer>();
                }

                Trigger = gameObject.AddComponent<BoxCollider2D>();
                Trigger.isTrigger = true;
                Trigger.size = new Vector2(0.3f, Size);
                ApplyOffset();
            }

            _init = true;
            On = _on;

            void ApplyOffset()
            {
                float offset = 0;
                for (int i = 0; i < Size - 1; i++)
                {
                    offset -= 0.5f;
                }

                Trigger.offset = new Vector2(0, offset);
            }
        }
    }
}
