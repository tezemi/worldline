﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Player;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Tilemaps;
using Random = UnityEngine.Random;

namespace Worldline.Level.Obstacles
{
    [HasDefaultState]
    public class EssenceBlob : MonoBehaviour
    {
        public static List<EssenceBlob> AllEssenceBlobs { get; private set; } = new List<EssenceBlob>();
        public bool RandomizeEffects;
        public int StarAmount = 1;
        public float TimeChangeMin = 8f;
        public float TimeChangeMax = 12f;
        public TimeEffect Type;
        public CombatAlignment Alignment = CombatAlignment.EffectsAll;
        public GameObject StarPrefab;
        [Tooltip("If assigned, this Essence Blob will move with the parent.")]
        public GameObject Parent;
        public List<TimeOperator> EntitiesEffected { get; set; } = new List<TimeOperator>();
        public List<GameObject> EntitiesCoolingDown { get; set; } = new List<GameObject>();
        public SpriteMask[] AuraMasks { get; protected set; }
        public const int ResetFrames = 120;
        public const float Cooldown = 0.25f;
        public const float RecordBGMFreq = 1500f;
        public const float NormalBGMFreq = 22000f;
        public const string PoolName = "EssenceBlobStarPool";
        protected Tilemap Tilemap { get; set; }
        protected TilemapCollider2D TilemapCollider { get; set; }
        private bool _killing;
        private bool _applicationQuitting;
        private Vector3? _parentStartPosition;
        private GameObject _recordingClone;

        protected virtual void Awake()
        {
            AllEssenceBlobs.Add(this);
            Tilemap = GetComponent<Tilemap>();
            TilemapCollider = GetComponent<TilemapCollider2D>();
            AuraMasks = GetComponentsInChildren<SpriteMask>();
            Timing.RunCoroutine(GenerateStars(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(ChangeEffect(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            if (Type == TimeEffect.Record)
            {
                GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.GetFloat("freq", out float currentFreq);
                if (_recordingClone != null && currentFreq > RecordBGMFreq)
                {
                    float lerpToFreq = Mathf.Lerp(currentFreq, RecordBGMFreq, 0.2f);
                    GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.SetFloat("freq", lerpToFreq);
                }
                else if (currentFreq < NormalBGMFreq)
                {
                    GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.SetFloat("freq", NormalBGMFreq);
                }
            }

            if (_recordingClone != null && _recordingClone.GetComponentInChildren<WeaponOperator>() != null)
            {
                foreach (GameObject obj in _recordingClone.GetComponentInChildren<WeaponOperator>().ActiveProjectiles)
                {
                    if (obj.GetComponent<SpriteRenderer>() != null)
                    {
                        obj.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (Parent != null)
            {
                if (_parentStartPosition == null)
                {
                    _parentStartPosition = Parent.transform.position;
                }

                Vector3 offset = Parent.transform.position - _parentStartPosition.Value;
                transform.position = new Vector3(offset.x, offset.y, transform.position.z);
            }

            if ((Parent == null || !Parent.gameObject.activeSelf) && _parentStartPosition != null)
            {
                Kill(0.05f);
            }
        }

        protected virtual void OnDisable()
        {
            if (_applicationQuitting) return;

            Timing.KillCoroutines(GetInstanceID().ToString());
            if (Type == TimeEffect.Record)
            {
                GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.GetFloat("freq", out float currentFreq);
                if (Math.Abs(currentFreq - NormalBGMFreq) > 1f)
                {
                    GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.SetFloat("freq", NormalBGMFreq);
                }
            }

            for (var i = 0; i < EntitiesEffected.Count; i++)
            {
                TimeOperator timeOperator = EntitiesEffected[i];
                OnTriggerExit2D(timeOperator.GetComponent<Collider2D>());
            }
        }

        protected virtual void OnDestroy()
        {
            AllEssenceBlobs.Remove(this);
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == _recordingClone && Type == TimeEffect.Reset)
            {
                _recordingClone.GetComponent<PlayerTimeOperator>().IsInsideAura = true;
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            if (Type == TimeEffect.Record && other.gameObject != MainPlayerOperator.MainPlayerObject)
            {
                return;
            }

            if (other == null || other.gameObject == null || other.GetComponent<TimeOperator>() == null)
            {
                return;
            }

            if (other.gameObject == Parent ||
            other.GetComponent<TimeOperator>().IsClone ||
            other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind && 
            EssenceOperator.Main.CurrentEffectActive == TimeEffect.Rewind)
            {
                return;
            }

            if (EntitiesEffected.Contains(other.GetComponent<TimeOperator>()) && other.GetComponent<TimeOperator>().CurrentTimeEffect != Type)
            {
                other.GetComponent<TimeOperator>().CurrentTimeEffect = Type;
                return;
            }

            if (Type != TimeEffect.Record && Type != TimeEffect.Reset)
            {
                if (other.GetComponent<TimeOperator>() != null &&
                !EntitiesCoolingDown.Contains(other.gameObject) &&
                !EntitiesEffected.Contains(other.GetComponent<TimeOperator>()))
                {
                    other.GetComponent<TimeOperator>().CurrentTimeEffect = Type;
                    EntitiesEffected.Add(other.GetComponent<TimeOperator>());
                }
            }
            else if (Type == TimeEffect.Reset && _recordingClone == null && other.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                if (other.GetComponent<TimeOperator>() != null &&
                other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.None &&
                !EntitiesCoolingDown.Contains(other.gameObject) &&
                !EntitiesEffected.Contains(other.GetComponent<TimeOperator>()))
                {
                    _recordingClone = other.GetComponent<TimeOperator>().PlaybackRecording(ResetFrames);
                    _recordingClone.GetComponent<PlayerTimeOperator>().IsResetClone = true;
                    _recordingClone.GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[TimeEffect.Reset];
                    _recordingClone.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                    _recordingClone.GetComponent<Combatant>().Invulnerable = true;
                    SetMaskForAllRenderers(_recordingClone.transform);
                    var bx = _recordingClone.GetComponents<BoxCollider2D>().SingleOrDefault(b => b.isTrigger);
                    if (bx != null)
                    {
                        bx.enabled = false;
                    }

                    void SetMaskForAllRenderers(Transform org)
                    {
                        foreach (Transform t in org)
                        {
                            t.GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[TimeEffect.Reset];
                            t.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
                            SetMaskForAllRenderers(t);
                        }
                    }
                }
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other == null || other.gameObject == null || other.GetComponent<TimeOperator>() == null)
            {
                return;
            }

            if (other.gameObject == _recordingClone && Type == TimeEffect.Reset)
            {
                _recordingClone.GetComponent<PlayerTimeOperator>().IsInsideAura = false;
                return;
            }

            if (EntitiesEffected.Contains(other.GetComponent<TimeOperator>()) &&
            other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
            {
                EntitiesEffected.Remove(other.GetComponent<TimeOperator>());
                if (!_applicationQuitting && isActiveAndEnabled)
                {
                    EntitiesCoolingDown.Add(other.gameObject);
                    StartCoroutine(StartCooldown(other.gameObject));
                    Timing.RunCoroutine(DisableRewind(), Segment.FixedUpdate, GetInstanceID().ToString());
                }

                IEnumerator<float> DisableRewind()
                {
                    yield return Timing.WaitForSeconds(4f);
                    if (other == null) yield break;

                    if (other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
                    {
                        other.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
                    }
                }

                return;
            }

            if (other.GetComponent<TimeOperator>() != null &&
            EntitiesEffected.Contains(other.GetComponent<TimeOperator>()))
            {
                other.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
                EntitiesEffected.Remove(other.GetComponent<TimeOperator>());
                if (!_applicationQuitting && isActiveAndEnabled)
                {
                    EntitiesCoolingDown.Add(other.gameObject);
                    Timing.RunCoroutine(StartCooldown(other.gameObject), Segment.FixedUpdate, GetInstanceID().ToString());
                }
            }
            

            IEnumerator<float> StartCooldown(GameObject otherObj)
            {
                yield return Timing.WaitForSeconds(Cooldown);
                if (otherObj == null)
                {
                    yield break;
                }

                EntitiesCoolingDown.Remove(otherObj);
            }
        }

        protected virtual void OnApplicationQuit()
        {
            _applicationQuitting = true;
        }

        protected virtual IEnumerator<float> ChangeEffect()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Random.Range(TimeChangeMin, TimeChangeMax));
                if (RandomizeEffects)
                {
                    Type = (TimeEffect) Random.Range(0, 4);
                }
            }
        }

        protected virtual IEnumerator<float> GenerateStars()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Random.Range(0.05f, .025f));
                for (int i = 0; i < StarAmount; i++)
                {
                    GameObject star;
                    if (Pool.Has(PoolName))
                    {
                        star = Pool.Get(PoolName);
                    }
                    else
                    {
                        star = Instantiate(StarPrefab);
                    }

                    star.SetActive(true);
                    star.hideFlags = HideFlags.HideInHierarchy;
                    star.GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[Type];
                    if (AuraMasks == null || AuraMasks.Length < 1)
                    {
                        star.transform.position = new Vector3
                        (
                            Random.Range(TilemapCollider.bounds.min.x, TilemapCollider.bounds.max.x),
                            Random.Range(TilemapCollider.bounds.min.y, TilemapCollider.bounds.max.y),
                            TilemapCollider.transform.position.z - 0.15f
                        );
                    }
                    else
                    {
                        Bounds randomBound = AuraMasks[Random.Range(0, AuraMasks.Length)].bounds; // Gets a random bound uses that for stars so they appear more condensed
                        star.transform.position = new Vector3
                        (
                            Random.Range(randomBound.min.x, randomBound.max.x),
                            Random.Range(randomBound.min.y, randomBound.max.y),
                            TilemapCollider.transform.position.z - 0.15f
                        );
                    }
                }
            }
        }

        public virtual void Kill(float speed)
        {
            if (_killing) return;

            _killing = true;
            Timing.RunCoroutine(Animate(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> Animate()
            {
                bool increaseAlpha = false;
                while (Tilemap.color.a > 0f)
                {
                    yield return Timing.WaitForOneFrame;
                    Tilemap.color += new Color(0f, 0f, 0f, increaseAlpha ? speed / 1.5f : -speed);
                    increaseAlpha = !increaseAlpha;
                }

                Destroy(gameObject);
            }
        }

        public virtual void RecordBlobBoundsTriggerEnter(Collider2D other, Collider2D self, Vector3[] points)
        {
            if (Type != TimeEffect.Record || _recordingClone != null) return;

            float longestDistance = 0;
            Vector3 furthestPoint = other.transform.position;
            foreach (Vector3 point in points)
            {
                float currentDistance = Vector2.Distance(other.transform.position, point);
                if (currentDistance > longestDistance)
                {
                    longestDistance = currentDistance;
                    furthestPoint = point;
                }
            }

            _recordingClone.transform.position = furthestPoint;
            _recordingClone.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            foreach (SpriteRenderer spriteRenderer in _recordingClone.GetComponentsInChildren<SpriteRenderer>())
            {
                spriteRenderer.GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
            }
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            AllEssenceBlobs = new List<EssenceBlob>();
        }
    }
}
