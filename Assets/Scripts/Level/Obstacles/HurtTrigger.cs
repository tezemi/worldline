﻿// Copyright (c) 2019 Destin Hebner

using UnityEngine;
using Worldline.Player;

namespace Worldline.Level.Obstacles
{
    /// <summary>
    /// When attached to a GameObject with a BoxCollider2D used as a trigger,
    /// this will hurt the player anytime they enter or stay within the trigger
    /// doing the specified amount of Damage.
    /// </summary>
    public class HurtTrigger : MonoBehaviour
    {
        public int Damage = 3;
        public bool Shrink = true;
        public const float ShrinkAmount = 0.15f;

        public virtual void Awake()
        {
            if (Shrink)
            {
                ShrinkHitbox();
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            if (isActiveAndEnabled && other.GetComponent<PlayerOperator>() != null)
            {
                other.GetComponent<PlayerOperator>().Hurt(Damage, this);
            }
        }

        protected virtual void OnDrawGizmos()
        {
            Bounds bounds = GetComponent<BoxCollider2D>().bounds;
            Gizmos.color = Color.red;
            Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(0f, bounds.size.y));
            Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(0f, bounds.size.y));
            Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(bounds.size.x, 0f));
            Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(bounds.size.x, 0f));
        }

        public virtual void ShrinkHitbox()
        {
            if (GetComponent<BoxCollider2D>().size.x > GetComponent<BoxCollider2D>().size.y)
            {
                GetComponent<BoxCollider2D>().size = new Vector2(GetComponent<BoxCollider2D>().size.x - ShrinkAmount, GetComponent<BoxCollider2D>().size.y);
            }
            else
            {
                GetComponent<BoxCollider2D>().size = new Vector2(GetComponent<BoxCollider2D>().size.x, GetComponent<BoxCollider2D>().size.y - ShrinkAmount);
            }
        }
    }
}

