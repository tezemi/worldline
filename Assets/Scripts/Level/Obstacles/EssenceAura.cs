﻿// Copyright (c) 2019 Destin Hebner

using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using Worldline.Combat;
using Worldline.Combat.Projectiles;
using Worldline.TimeEngine;
using Worldline.Utilities;
using Worldline.Visuals;
using Random = UnityEngine.Random;

namespace Worldline.Level.Obstacles
{ 
    /// <summary>
    /// TODO: Documentation, move to MEC coroutines.
    /// </summary>
    public class EssenceAura : MonoBehaviour
    {
        public bool Flux;
        public bool ChangeType;
        public float FluxSpeed = 0.01f;
        public float TypeChangeMaxTime = 10;
        public TimeEffect Type;
        public Vector2 StartScale;
        public Vector2 EndScale;        
        public List<TimeOperator> EntitiesEffected { get; set; } = new List<TimeOperator>();
        public List<GameObject> EntitiesCoolingDown { get; set; } = new List<GameObject>();
        public bool FluxingOut { get; protected set; }
        public const float Alpha = 0.75f;
        public const float Cooldown = 1f;
        public const float RewindTime = 3f;
        public const float Offset = 0.15f;
        public const float TypeChangeTimeMulti = 2f;
        public const float ColorChangeSpeed = 0.02f;
        public const float FluxLerpBuffer = 0.075f;
        public const float RotateSpeed = 50f;
        public const float LayerDrainSpeed = 0.01f;
        public const float LayerColorMulti = 1.5f;
        protected bool LayerAActive { get; set; } = true;
        protected Vector3 DrainSize { get; set; } = new Vector3(0f, 0f, 1f);
        protected GameObject LayerA { get; set; }
        protected GameObject LayerB { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        private bool _applicationQuitting;
        
        protected virtual void Awake()
        {
            SpriteRenderer = GetComponent<SpriteRenderer>();
            if (Flux)
            {
                transform.SetLossyScale(StartScale);
                StartCoroutine(DoFlux());
            }

            if (ChangeType)
            {
                StartCoroutine(TypeChange());
            }

            SpriteRenderer.color = TimeOperator.EffectColors[Type] - new Color(0f, 0f, 0f, Alpha);
            LayerA = new GameObject("Layer A", typeof(SpriteRenderer));
            LayerB = new GameObject("Layer B", typeof(SpriteRenderer));
            LayerA.GetComponent<SpriteRenderer>().sprite = SpriteRenderer.sprite;
            LayerB.GetComponent<SpriteRenderer>().sprite = SpriteRenderer.sprite;
            LayerA.GetComponent<SpriteRenderer>().color = SpriteRenderer.color - new Color(0f, 0f, 0f, 1f);
            LayerB.GetComponent<SpriteRenderer>().color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, 0f);
            LayerA.transform.position = transform.position + new Vector3(0, 0, 0.2f);
            LayerB.transform.position = transform.position + new Vector3(0, 0, 0.25f);
            LayerA.transform.SetLossyScale(EndScale);
            LayerB.transform.SetLossyScale(StartScale);
            LayerA.transform.parent = transform;
            LayerB.transform.parent = transform;
            StartCoroutine(AnimateLayers());
        }

        protected virtual void OnDisable()
        {
            if (_applicationQuitting) return;

            Timing.KillCoroutines(GetInstanceID().ToString());
            for (var i = 0; i < EntitiesEffected.Count; i++)
            {
                TimeOperator timeOperator = EntitiesEffected[i];
                OnTriggerExit2D(timeOperator.GetComponent<Collider2D>());
            }
        }

        protected virtual void Update()
        {
            for (int i = 0; i < EntitiesEffected.Count; i++)
            {
                TimeOperator timeOperator = EntitiesEffected[i];
                if (!timeOperator.isActiveAndEnabled)
                {
                    // TODO: This could cause issues, but hasn't so far :^)
                    if (!timeOperator.gameObject.activeSelf)
                    {
                        timeOperator.gameObject.SetActive(true);
                        OnTriggerExit2D(timeOperator.GetComponent<Collider2D>());
                        timeOperator.gameObject.SetActive(false);
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            SpriteRenderer.color = Vector4.MoveTowards(SpriteRenderer.color, TimeOperator.EffectColors[Type] - new Color(0f, 0f, 0f, Alpha), ColorChangeSpeed);
            transform.Rotate(0, 0, RotateSpeed * Time.deltaTime);
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            if (other == null || other.gameObject == null || other.GetComponent<TimeOperator>() == null)
            {
                return;
            }

            if (other.GetComponent<Projectile>() == null && other.isTrigger)
            {
                return;
            }

            if (EntitiesEffected.Contains(other.GetComponent<TimeOperator>()) && other.GetComponent<TimeOperator>().CurrentTimeEffect != Type)
            {
                other.GetComponent<TimeOperator>().CurrentTimeEffect = Type;
                return;
            }

            if (other.GetComponent<IHasAlignment>() == null || other.GetComponent<IHasAlignment>() != null &&
            other.GetComponent<TimeOperator>() != null &&            
            Combatant.ShouldEffect(GetComponentInParent<IHasAlignment>() == null ? 
            CombatAlignment.Enemy : GetComponentInParent<IHasAlignment>().Alignment, 
            other.GetComponent<IHasAlignment>().Alignment) &&
            other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.None &&
            !EntitiesCoolingDown.Contains(other.gameObject) && !EntitiesEffected.Contains(other.GetComponent<TimeOperator>()))
            {
                other.GetComponent<TimeOperator>().CurrentTimeEffect = Type;
                EntitiesEffected.Add(other.GetComponent<TimeOperator>());                
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other == null || other.gameObject == null || other.GetComponent<TimeOperator>() == null)
            {
                return;
            }

            if (EntitiesEffected.Contains(other.GetComponent<TimeOperator>()) && other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
            {
                EntitiesEffected.Remove(other.GetComponent<TimeOperator>());
                if (!_applicationQuitting && isActiveAndEnabled)
                {
                    EntitiesCoolingDown.Add(other.gameObject);
                    StartCoroutine(StartCooldown(other.gameObject));
                    Timing.RunCoroutine(DisableRewind(), Segment.FixedUpdate, GetInstanceID().ToString());
                }

                IEnumerator<float> DisableRewind()
                {
                    yield return Timing.WaitForSeconds(4f);
                    if (other.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Rewind)
                    {
                        other.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
                    }
                }

                return;
            }

            if (other.GetComponent<TimeOperator>() != null && EntitiesEffected.Contains(other.GetComponent<TimeOperator>()))
            {
                other.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
                EntitiesEffected.Remove(other.GetComponent<TimeOperator>());
                if (!_applicationQuitting && isActiveAndEnabled)
                {
                    EntitiesCoolingDown.Add(other.gameObject);
                    StartCoroutine(StartCooldown(other.gameObject));
                }
            }

            IEnumerator StartCooldown(GameObject otherObj)
            {
                yield return new WaitForSeconds(Cooldown);
                if (otherObj == null)
                {
                    yield break;
                }

                EntitiesCoolingDown.Remove(otherObj);
            }
        }
        
        protected virtual void OnApplicationQuit()
        {
            _applicationQuitting = true;
        }

        public virtual IEnumerator Collapse()
        {
            TrailAnimation.Create(gameObject);
            shrink:
            yield return new WaitForFixedUpdate();
            transform.localScale = Vector3.MoveTowards(transform.localScale, new Vector3(0f, 0f, 1f), 0.02f);
            if (transform.localScale.x > 0.05f)
            {
                goto shrink;
            }

            Destroy(gameObject);
        }

        public virtual IEnumerator DoFlux()
        {
            start:
            yield return new WaitForFixedUpdate();
            transform.localScale = Vector3.MoveTowards(transform.localScale, FluxingOut ? EndScale : StartScale, FluxSpeed);
            if (FluxingOut && Math.Abs(transform.localScale.x - EndScale.x) < FluxLerpBuffer)
            {
                FluxingOut = false;
            }
            else if (!FluxingOut && Math.Abs(transform.localScale.x - StartScale.x) < FluxLerpBuffer)
            {
                FluxingOut = true;
            }

            if (isActiveAndEnabled && Flux)
            {
                goto start;
            }
        }

        public virtual IEnumerator TypeChange()
        {
            start:
            yield return new WaitForSeconds(Random.Range(TypeChangeMaxTime / TypeChangeTimeMulti, TypeChangeMaxTime));
            switch (Random.Range(0, 4))
            {
                case 0:
                    Type = TimeEffect.Pause;
                    break;
                case 1:
                    Type = TimeEffect.Slow;
                    break;
                case 2:
                    Type = TimeEffect.Rewind;
                    break;
                case 3:
                    Type = TimeEffect.FastForward;
                    break;
                default:
                    throw new Exception();                    
            }

            if (isActiveAndEnabled && ChangeType)
            {
                goto start;
            }
        }

        public virtual IEnumerator AnimateLayers()
        {
            start:
            yield return new WaitForFixedUpdate();
            if (LayerAActive)
            {
                DrainSize += new Vector3(LayerDrainSpeed, LayerDrainSpeed, 0);
                LayerA.transform.localScale = new Vector3(1f, 1f, 1f) - DrainSize;
                LayerA.GetComponent<SpriteRenderer>().color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, LayerA.GetComponent<SpriteRenderer>().color.a);
                LayerA.GetComponent<SpriteRenderer>().color += new Color(0f, 0f, 0f, LayerDrainSpeed * LayerColorMulti);
                if (LayerA.transform.localScale.x <= 0.01f)
                {
                    DrainSize = new Vector3(0f, 0f, 1f);
                    LayerA.transform.localScale = new Vector3(1f, 1f, 1f);
                    LayerA.transform.position = transform.position + new Vector3(0, 0, 0.25f);
                    LayerB.transform.position = transform.position + new Vector3(0, 0, 0.2f);
                    LayerA.GetComponent<SpriteRenderer>().color = SpriteRenderer.color - new Color(0f, 0f, 0f, 1f);
                    LayerAActive = false;
                }
            }
            else
            {
                DrainSize += new Vector3(LayerDrainSpeed, LayerDrainSpeed, 0);
                LayerB.transform.localScale = new Vector3(1f, 1f, 1f) - DrainSize;
                LayerB.GetComponent<SpriteRenderer>().color = new Color(SpriteRenderer.color.r, SpriteRenderer.color.g, SpriteRenderer.color.b, LayerB.GetComponent<SpriteRenderer>().color.a);
                LayerB.GetComponent<SpriteRenderer>().color += new Color(0f, 0f, 0f, LayerDrainSpeed * LayerColorMulti);
                if (LayerB.transform.localScale.x <= 0.01f)
                {
                    DrainSize = new Vector3(0f, 0f, 1f);
                    LayerB.transform.localScale = new Vector3(1f, 1f, 1f);
                    LayerB.transform.position = transform.position + new Vector3(0, 0, 0.25f);
                    LayerA.transform.position = transform.position + new Vector3(0, 0, 0.2f);
                    LayerB.GetComponent<SpriteRenderer>().color = SpriteRenderer.color - new Color(0f, 0f, 0f, 1f);
                    LayerAActive = true;
                }
            }

            if (isActiveAndEnabled)
            {
                goto start;
            }
        }
    }
}
