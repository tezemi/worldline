﻿// Copyright (c) 2019 Destin Hebner

using UnityEngine;
using Worldline.Physics;
using Worldline.Player;

namespace Worldline.Level.Obstacles
{
    public class SpikeBlock : MonoBehaviour
    {
        public float Speed { get; set; } = 4f;
        protected MovementOperator MovementOperator { get; set; }

        protected virtual void Awake()
        {
            MovementOperator = GetComponent<MovementOperator>();
        }

        protected virtual void Update()
        {
            if (MainPlayerOperator.MainPlayerObject.transform.position.x > transform.position.x)
            {
                MovementOperator.Speed = new Vector2(Speed, 0f);
            }
            else if (MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x)
            {
                MovementOperator.Speed = new Vector2(-Speed, 0f);
            }
        }
    }
}
