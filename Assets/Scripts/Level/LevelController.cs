// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Utilities;
using Worldline.Corruption;
using Worldline.Player.Weapons;
using Worldline.ScriptedSequences;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.Level
{
    /// <summary>
    /// Singleton class that is responsible for data and functionality relevant
    /// to specific levels. Information about a certain level is stored here,
    /// which is both caluclated when the game loads a level, and is serialized
    /// before the level is loaded.
    /// </summary>
    public class LevelController : MonoBehaviour
    {
        public static LevelController Main;
        public static string PersistentDataPath => Application.persistentDataPath;
        public static string DataPath => Application.dataPath;
        [Tooltip("The amount of corruption this level will have by default.")]
        public float CorruptionIntensityInLevel;
        [Tooltip("The sprite bundle to use for this level.")]
        public PlayerSpriteBundle PlayerSpriteBundle;
        [Tooltip("The Tilemap that defines the physics layout of the level.")]
        public Tilemap PhysicsTilemap;
        [Tooltip("The canvas prefab. A new canvas must be created if the resolution is changed during gameplay.")]
        public GameObject CanvasPrefab;
        [Tooltip("The music to play while on this level.")]
        public AudioClip[] BackgroundMusic;
        [Tooltip("Any ScriptedSequences specified here get fired when the level loads.")]
        public ScriptedSequence[] FireOnLoad;
        [Tooltip("Any weapon sprite bundles that could be used on this level.")]
        public WeaponSpriteBundle[] WeaponSpriteBundles;
        public readonly Color SurfaceColor = Color.cyan;
        public float MapWidth => PhysicsTilemap.cellBounds.size.x;
        public float MapWidthMin => PhysicsTilemap.cellBounds.min.x;
        public float MapWidthMax => PhysicsTilemap.cellBounds.max.x;
        public float MapHeight => PhysicsTilemap.cellBounds.size.y;
        public float MapHeightMin => PhysicsTilemap.cellBounds.min.y;
        public float MapHeightMax => PhysicsTilemap.cellBounds.max.y;
        private bool _paused;
        private object _pauseSender;

        /// <summary>
        /// Whether or not the game is paused. Setting this to true will cause
        /// the game's time scale to be set to 0. If you are looking to pause
        /// the game via some sort of menu, you should use SetPaused(bool, object).
        /// </summary>
        public bool Paused
        {
            get => _paused;
            protected set
            {
                Cursor.visible = value;
                Time.timeScale = value ? 0f : 1f;
                _paused = value;
            }
        }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void Start()
        {
            if (BackgroundMusic == null || BackgroundMusic.Length == 0)
            {
                BackgroundMusicOperator.Main.AudioClip = null;
            }
            else if (BackgroundMusic.Length == 1)
            {
                BackgroundMusicOperator.Main.AudioClip = BackgroundMusic[0];
            }
            else
            {
                BackgroundMusicOperator.Main.AudioClips = BackgroundMusic;
            }
        }

        protected virtual void OnEnable()
        {
            if (!Paused)
            {
                Cursor.visible = false;
            }
        }

        protected virtual void OnLevelLoaded()
        {
            PhysicsTilemap.GetComponent<TilemapRenderer>().enabled = false;
            if (FireOnLoad == null) return;

            foreach (ScriptedSequence e in FireOnLoad)
            {
                e.Activate();
            }

            Corrupter.Intensity = CorruptionIntensityInLevel;
        }

        protected virtual void OnDrawGizmos()
        {
            foreach (EdgeCollider2D edgeCollider2D in GetComponentsInChildren<EdgeCollider2D>())
            {
                Vector2[] points = edgeCollider2D.points;
                Gizmos.color = SurfaceColor;

                for (int i = 0; i < points.Length - 1; i++)
                {
                    Gizmos.DrawLine((Vector2)edgeCollider2D.transform.position + points[i], (Vector2)edgeCollider2D.transform.position + points[i + 1]);
                }
            }

            foreach (CompositeCollider2D compositeCollider in GetComponentsInChildren<CompositeCollider2D>())
            {
                for (int i = 0; i < compositeCollider.pathCount; i++)
                {
                    compositeCollider.GetPathPointCount(i);
                    Vector2[] points = new Vector2[compositeCollider.GetPathPointCount(i)];
                    compositeCollider.GetPath(i, points);
                    Gizmos.color = SurfaceColor;

                    for (int j = 0; j < points.Length - 1; j++)
                    {
                        Gizmos.DrawLine(points[j], points[j + 1]);
                    }

                    Gizmos.DrawLine(points[points.Length - 1], points[0]);
                }
            }
        }

        /// <summary>
        /// Pauses or un-pauses the game. When the game is paused or un-paused
        /// a sender must be specified. A sender is some sort of unique object
        /// that is taking responsibility for pausing the game. The game can
        /// only be un-paused by the sender that paused the game. This is done
        /// to ensure that the pause does not "get paused twice" by two different
        /// objects, menus, or entities. You may pass null as the sender in order
        /// to override this feature and pause/un-pause the game regardless. The
        /// game can not be paused while the debug menu is open.
        /// </summary>
        /// <param name="value">If true, pause the game.</param>
        /// <param name="sender">Once paused, the game can only be un-paused by this object.</param>
        /// <returns>Whether or not pausing/un-pausing the game was successful.</returns>
        public bool SetPaused(bool value, object sender)
        {
            if ((_pauseSender == null || sender == _pauseSender) && !DebugUtils.Main.Active)
            {
                Paused = value;
                _pauseSender = _pauseSender == null ? sender : null;

                return true;
            }

            return false;
        }
    }
}
