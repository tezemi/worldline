﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Combat;
using Worldline.TimeEngine;
using Worldline.Player;
using UnityEngine;
using Worldline.Visuals;

namespace Worldline.Level
{
    /// <summary>
    /// When attached to a GameObject, this will deactivate the GameObject
    /// on Awake(), and will watch to see if the GameObject is close to being
    /// on the player's screen, which can be adjusted with the Cushion field.
    /// When the GameObject comes onto the player's screen, it will activate
    /// the GameObject. Good for enemies that shouldn't spawn until the player
    /// gets close.
    /// </summary>
    public class GameObjectActivator : MonoBehaviour
    {
        public GameObject Activatee { get; set; }
        public CombatAlignment? Alignment { get; protected set; }
        public const float Cushion = 0.1f;

        public static GameObjectActivator Create(GameObject activatee, bool takeOnEffect = false)
        {
            GameObject gameObject = new GameObject("Activator", typeof(GameObjectActivator))
            {
                hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector
            };

            gameObject.GetComponent<GameObjectActivator>().Activatee = activatee;
            if (activatee.GetComponent<IHasAlignment>() != null) gameObject.GetComponent<GameObjectActivator>().Alignment = activatee.GetComponent<IHasAlignment>().Alignment;
            activatee.SetActive(false);
            gameObject.transform.position = activatee.transform.position;
                        
            return gameObject.GetComponent<GameObjectActivator>();
        }

        private void Update()
        {
            if (OnScreen())
            {
                Activate();
            }
        }

        protected void Activate()
        {
            Activatee.SetActive(true);
            Destroy(gameObject);
        }

        protected bool OnScreen()
        {
            Vector2 screenPoint = CameraOperator.ActiveCamera.CameraComponent.WorldToViewportPoint(transform.position);
            return screenPoint.x + Cushion > 0 && screenPoint.x - Cushion < 1 && screenPoint.y + Cushion > 0 && screenPoint.y - Cushion < 1;   
        }
    }
}
