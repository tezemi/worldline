// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;
using UnityEngine.SceneManagement;
using MEC;
using Worldline.Player.Upgrades;
using Worldline.ScriptedSequences;
using Worldline.Visuals;
using Worldline.Visuals.Transitions;

namespace Worldline.Level
{
    /// <summary>
    /// Persistent singleton class that is responsible for transitions between
    /// scenes with an effect, and sets the player up in the next level. This
    /// class should always be used when transitioning between levels as opposed
    /// to use Scene.LoadScene().
    /// </summary>
	public class LevelLoader : MonoBehaviour
	{
        public static LevelLoader Main;
        [Tooltip("The effect to use next time the level changes.")]
        public Transition Transition;
        [Tooltip("The main player prefab. Is no player has already been loaded, this is the one that will be used.")]
        public GameObject PlayerPrefab;
        /// <summary>
        /// The start position for the player within the level. Gets reset to 
        /// null after every level load. If this is null when a new level is
        /// loaded, the LevelLoader will look for a PlayerStartPosition to use.
        /// Use PlayerSpawnedPosition to find where the player actually spawned
        /// in on the current level.
        /// </summary>
        public Vector2? PlayerStartPosition { get; set; }
        /// <summary>
        /// Whether or not the LevelLoader is changing to another level. If 
        /// this is true, then another level can't be loaded until the current
        /// is finished.
        /// </summary>
        public bool SwitchingLevels { get; protected set; }
        /// <summary>
        /// Indiciates whether or not the Transition is at it's "midpoint." The 
        /// midpoint indicates, usually, that the effect has reached some point
        /// where the screen is completely obscured to the player, and is safe to
        /// actually change scenes.
        /// </summary>
        public bool LevelTransAtMidpoint { get; protected set; }
        /// <summary>
        /// The Scene that is currently loaded. Gets set as soon as the 
        /// Transition reaches its midpoint and the scene is actually loaded
        /// in.
        /// </summary>
        public Scene CurrentScene { get; protected set; }
        /// <summary>
        /// The position the player spawned into on the current level. Use 
        /// PlayerStartPosition to specifiy where the player should spawn
        /// on the next level.
        /// </summary>
        public Vector3 PlayerSpawnedPosition { get; private set; }
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
                DontDestroyOnLoad(gameObject);
                this.DestroyOnSceneLoad();
            }
            else
            {
                Destroy(gameObject);
            }

            // Mainly for debugging, but also runs when the player starts a new game.
            if (MainPlayerOperator.MainPlayerObject == null)
            {
                SpawnPlayer();
                Timing.RunCoroutine(Cleanup(false), Segment.FixedUpdate);
            }
        }

        /// <summary>
        /// Starts the process of changing to a new level. Should always
        /// be used to transition between levels.
        /// </summary>
        /// <param name="scene">The scene to go to.</param>
        /// <param name="useTransition">Should a transition effect be used?</param>
        public void LoadLevel(Scene scene, bool useTransition)
        {
            Timing.RunCoroutine(Gen(scene.path, useTransition), Segment.FixedUpdate);  
        }

        /// <summary>
        /// Starts the process of changing to a new level. Should always
        /// be used to transition between levels.
        /// </summary>
        /// <param name="sceneName">The name of the scene to go to.</param>
        /// <param name="useTransition">Should a transition effect be used?</param>
        public void LoadLevel(string sceneName, bool useTransition)
        {
            Timing.RunCoroutine(Gen(sceneName, useTransition), Segment.FixedUpdate);
        }

	    /// <summary>
	    /// Starts the process of changing to a new level. Should always
	    /// be used to transition between levels.
	    /// </summary>
	    /// <param name="sceneName">The name of the scene to go to.</param>
	    /// <param name="useTransition">Should a transition effect be used?</param>
	    /// <param name="waitOn">Whether or not to wait for the level to change.</param>
	    public IEnumerator<float> LoadLevel(string sceneName, bool useTransition, bool waitOn)
	    {
	        yield return Timing.WaitUntilDone(Timing.RunCoroutine(Gen(sceneName, useTransition), Segment.FixedUpdate));
	    }

        /// <summary>
        /// Starts the process of reloading the current level.
        /// </summary>
        /// <param name="useTransition">Should a transition effect be used?</param>
        public void ReloadLevel(bool useTransition)
        {
            Timing.RunCoroutine(ReloadGen(useTransition), Segment.FixedUpdate);
        }
        
        /// <summary>
        /// Loads a new scene and can use an effect to transition between them. Should
        /// always be used when transitioning between levels.
        /// </summary>
        /// <param name="scene">The scene to go to.</param>
        /// <param name="useTransition">Should a transition effect be used?</param>
        /// <returns>Waits for the entire level load to take place.</returns>
        private IEnumerator<float> Gen(string scene, bool useTransition)
        {
            if (SwitchingLevels)
            {
                Debug.LogWarning($"Cant change level to {scene} while another is being loaded.");
                yield break;
            }

            StartLoad(useTransition);
            yield return Timing.WaitUntilDone(new WaitUntil(() => !useTransition || Transition.SafeToLoad));
            yield return Timing.WaitUntilDone(SceneManager.LoadSceneAsync(scene));
            if (useTransition)
            {
                Transition.ForceUpdate();
            }

            CurrentScene = SceneManager.GetActiveScene();
            SpawnPlayer();
            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(Cleanup(useTransition), Segment.FixedUpdate));
        }

        /// <summary>
        /// Reloads the same scene the player is in.
        /// </summary>
        /// <param name="useTransition">Should a transition effect be used?</param>
        /// <returns>Waits for entire level load to complete.</returns>
        private IEnumerator<float> ReloadGen(bool useTransition)
        {
            StartLoad(useTransition);

            yield return Timing.WaitUntilDone(new WaitUntil(() => !useTransition || Transition.SafeToLoad));

            SpawnPlayer();
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(Cleanup(useTransition), Segment.FixedUpdate));
        }

        /// <summary>
        /// Executes all behavior that should be performed the instant a level
        /// load begins, before the transition effect reaches its "safe point."
        /// </summary>
        /// <param name="useTransition">If true use a transition effect.</param>
        private void StartLoad(bool useTransition)
        {
            SwitchingLevels = true;
            LevelTransAtMidpoint = false;
            foreach (GameObject g in FindObjectsOfType<GameObject>())
            {
                g.SendMessageUpwards("LevelLoadStarted", SendMessageOptions.DontRequireReceiver);
                if (g.GetComponent<ScriptedSequence>() != null && g.GetComponent<ScriptedSequence>().Active && g.GetComponent<ScriptedSequence>().Overrideable)
                {
                    g.GetComponent<ScriptedSequence>().Deactivate();
                }
            }

            // Wait for transition.
            if (MainPlayerOperator.MainPlayerObject != null)
            {
                MainPlayerOperator.MainPlayerObject.SetActive(false);
            }

            if (useTransition)
            {
                Transition.StartTransition();
            }
        }

        /// <summary>
        /// Spawns or activates the player (if the player is already instantiated) and
        /// sets their position before activating them. If no position is specified by
        /// the PlayerStartPosition field, the player will be spawned at the
        /// PlayerStartPosition point in the level. 
        /// </summary>
        private void SpawnPlayer()
        {
            if (MainPlayerOperator.MainPlayerObject == null)
            {
                MainPlayerOperator.MainPlayerObject = Instantiate(PlayerPrefab);
            }

            if (PlayerStartPosition == null)
            {
                MainPlayerOperator.MainPlayerObject.transform.position = (Vector2)FindObjectOfType<PlayerStartPosition>().transform.position;
            }
            else
            {
                MainPlayerOperator.MainPlayerObject.transform.position = (Vector2)PlayerStartPosition;
            }

            PlayerSpawnedPosition = MainPlayerOperator.MainPlayerObject.transform.position;
            CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
            if (LevelController.Main.PlayerSpriteBundle != null)
            {
                MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle = LevelController.Main.PlayerSpriteBundle;
            }
        }

        /// <summary>
        /// Performs final tasks before ending the transition effect.
        /// </summary>
        /// <param name="useTransition">If no transition is used this ends instantly.</param>
        /// <returns>Can wait for expensive tasks, and the transition.</returns>
        private IEnumerator<float> Cleanup(bool useTransition)
        {
            LevelController.Main.PhysicsTilemap.CompressBounds();
            MainPlayerOperator.MainPlayerObject.SetActive(true);
            PlayerStartPosition = null;
            LevelTransAtMidpoint = true;
            CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
            GameObject[] transformsInLevel = Resources.FindObjectsOfTypeAll<GameObject>();
            transformsInLevel = transformsInLevel.Distinct().ToArray();
            foreach (GameObject g in transformsInLevel)
            {
                if (g == null) continue;

                g.SendMessage("OnLevelLoaded", SendMessageOptions.DontRequireReceiver);
            }

            foreach (Upgrade u in MainPlayerOperator.MainPlayerComponent.Upgrades)
            {
                if (u == null) continue;

                u.OnLevelLoaded();
            }

            foreach (TimeOperator timeOperator in FindObjectsOfType<TimeOperator>())
            {
                timeOperator.SpawnedOnLevelLoad = true;
            }

            yield return Timing.WaitUntilDone(Resources.UnloadUnusedAssets());
            GC.Collect();
            if (useTransition)
            {
                Transition.EndTransition();
            }

            yield return Timing.WaitUntilDone(new WaitUntil(() => !useTransition || !Transition.Active));
            //MainPlayerOperator.MainPlayerComponent.DisableMovementInput = false;
            SwitchingLevels = false;
        }
    }
}
