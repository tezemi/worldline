﻿// Copyright (c) 2019 Destin Hebner

using System;
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.Level
{
    public class BackgroundMusicOperator : MonoBehaviour
    {
        public static BackgroundMusicOperator Main;
        public AudioSource MainAudioSource { get; set; }
        public List<AudioSource> ExtraAudioSources { get; protected set; } = new List<AudioSource>();

        public float Volume
        {
            get
            {
                return _audioClip == null ? ExtraAudioSources[0].volume : MainAudioSource.volume;
            }
            set
            {
                if (this == null || MainAudioSource == null || !enabled)
                {
                    return;
                }

                foreach (AudioSource audioSource in ExtraAudioSources)
                {
                    audioSource.volume = value;
                }

                MainAudioSource.volume = value;
            }
        }

        public float Time
        {
            get
            {
                return _audioClip == null ? ExtraAudioSources[0].time : MainAudioSource.time;
            }
            set
            {
                if (this == null || MainAudioSource == null || !enabled)
                {
                    return;
                }

                foreach (AudioSource audioSource in ExtraAudioSources)
                {
                    audioSource.time = value;
                }

                MainAudioSource.time = value;
            }
        }

        public float Speed
        {
            get
            {
                return MainAudioSource.pitch;
            }
            set
            {
                if (MainAudioSource == null || !enabled)
                {
                    return;
                }

                foreach (AudioSource audioSource in ExtraAudioSources)
                {
                    audioSource.pitch = value;
                }

                MainAudioSource.pitch = value;
            }
        }

        public float Pitch
        {
            get
            {
                MainAudioSource.outputAudioMixerGroup.audioMixer.GetFloat("pitch", out float pitch);

                return pitch;
            }
            set
            {
                if (MainAudioSource == null || !enabled) return;

                MainAudioSource.outputAudioMixerGroup.audioMixer.SetFloat("pitch", value);
            }
        }

        public bool Mute
        {
            get
            {
                return _audioClip == null ? ExtraAudioSources[0].mute : MainAudioSource.mute;
            }
            set
            {
                if (MainAudioSource == null || !enabled) return;

                foreach (AudioSource audioSource in ExtraAudioSources)
                {
                    audioSource.mute = value;
                }

                MainAudioSource.mute = value;
            }
        }

        private AudioClip _audioClip;
        public AudioClip AudioClip
        {
            get
            {
                return _audioClip;
            }
            set
            {
                if (this == null || MainAudioSource == null || _audioClip == value && _audioClips == null)
                {
                    return;
                }

                foreach (AudioSource audioSource in ExtraAudioSources)
                {
                    audioSource.Fade(0f);
                }

                MainAudioSource.Fade(1f, 0.02f, value);
                _audioClips = null;
                _audioClip = value;
            }
        }

        private AudioClip[] _audioClips;
        public AudioClip[] AudioClips
        {
            get
            {
                return _audioClips;
            }
            set
            {
                if (this == null || MainAudioSource == null || _audioClips != null && _audioClips[0] == value[0] && _audioClip == null)
                {
                    return;
                }
                
                MainAudioSource.Fade(0f);
                for (int i = 0; i < value.Length; i++)
                {
                    if (ExtraAudioSources.Count < i + 1)
                    {
                        ExtraAudioSources.Add(gameObject.AddComponent<AudioSource>());
                    }

                    ExtraAudioSources[i].outputAudioMixerGroup = MainAudioSource.outputAudioMixerGroup;
                    ExtraAudioSources[i].loop = true;
                    ExtraAudioSources[i].volume = 0f;
                    ExtraAudioSources[i].Fade(1f, 0.02f, value[i]);
                }

                _audioClip = null;
                _audioClips = value;
            }
        }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
                this.DestroyOnSceneLoad();
            }
            else
            {
                Destroy(gameObject);
            }

            MainAudioSource = GetComponent<AudioSource>();
            MainAudioSource.loop = true;
        }

        protected virtual void Update()
        {
            if (CameraOperator.ActiveCamera != null)
            {
                transform.position = CameraOperator.ActiveCamera.transform.position;
            }
        }

        public void FadeTo(float volume)
        {
            foreach (AudioSource audioSource in ExtraAudioSources)
            {
                audioSource.Fade(volume, 0.02f, audioSource.clip);
            }

            MainAudioSource.Fade(volume, 0.02f, MainAudioSource.clip);
        }
    }
}
