﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Level
{
    public class EssenceDroplet : MonoBehaviour, ICanRessurect, IPoolable
    {
        public int Amount = 5;
        public TimeEffect Type;
        public AudioClip EssencePickupSound;
        public bool Started { get; set; }
        public bool ImmuneToPickup { get; set; }
        public GameObject Target { get; set; }
        public const float MinDistanceToAbsorb = 0.7f;
        public const float TimeStartingUp = 0.5f;
        public const float MaxDropletScale = 2f;
        public const float MinSpeed = 8f;
        public const float MaxSpeed = 15f;
        public const float MinAcceleration = 0.1f;
        public const float MaxAcceleration = 1f;
        public const float MinDistance = 1f;
        public const float MaxDistance = 12f;
        protected MovementOperator MovementOperator { get; set; }
        protected Rigidbody2D Rigidbody { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected TimeOperator TimeOperator { get; set; }

        private static readonly List<EssenceDroplet> _activeEssenceDroplets = new List<EssenceDroplet>();
        public static List<EssenceDroplet> ActiveEssenceDroplets
        {
            get
            {
                _activeEssenceDroplets.RemoveAll(n => n == null);
                return _activeEssenceDroplets;
            }
        }

        protected virtual void Awake()
        {
            MovementOperator = GetComponent<MovementOperator>();
            Rigidbody = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            TimeOperator = GetComponent<TimeOperator>();
        }

        protected virtual void OnEnable()
        {
            Started = false;
            ActiveEssenceDroplets.Add(this);
            MovementOperator.Acceleration = MinAcceleration;
            MovementOperator.Speed = new Vector2
            (
                Random.value < 0.5f ? Random.Range(MinSpeed, MaxSpeed) : -Random.Range(MinSpeed, MaxSpeed),
                Random.value < 0.5f ? Random.Range(MinSpeed, MaxSpeed) : -Random.Range(MinSpeed, MaxSpeed)
            );

            if (Random.value < 0.33f)
            {
                if (Random.value < 0.5f)
                {
                    MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, 0f);
                }
                else
                {
                    MovementOperator.Speed = new Vector2(0f, MovementOperator.Speed.y);
                }
            }

            CoroutineTimer timer = new CoroutineTimer(GetComponent<TimeOperator>(), StartNormalMovement);
            timer.StartTimer(TimeStartingUp, 1);
            void StartNormalMovement()
            {
                MovementOperator.Acceleration = MinAcceleration;
                Started = true;
            }
        }

        protected virtual void OnDisable()
        {
            ActiveEssenceDroplets?.Remove(this);
            ImmuneToPickup = false;
        }

        protected virtual void FixedUpdate()
        {
            if (MainPlayerOperator.MainPlayerObject == null) return;

            GameObject target = Target ?? MainPlayerOperator.MainPlayerObject;

            SpriteRenderer.color = TimeOperator.EffectColors[Type];
            if (Started && !TimeOperator.ShouldHold)
            {
                float distanceToPlayer = Vector2.Distance(transform.position, target.transform.position);
                if (Target == null)
                {
                    MovementOperator.Acceleration = MathUtilities.GetRange
                    (
                        distanceToPlayer,
                        MinDistance,
                        MaxDistance,
                        MinAcceleration,
                        MaxAcceleration
                    );
                }
                else
                {
                    MovementOperator.Acceleration = MinAcceleration;
                }

                if (MovementOperator.Acceleration < MinAcceleration)
                {
                    MovementOperator.Acceleration = MinAcceleration;
                }

                MovementOperator.Speed = new Vector2
                (
                    MathUtilities.GetRange(distanceToPlayer, MinDistance, MaxDistance, MaxSpeed, MinSpeed),
                    MathUtilities.GetRange(distanceToPlayer, MinDistance, MaxDistance, MaxSpeed, MinSpeed)
                );

                if (transform.position.x > target.transform.position.x)
                {
                    MovementOperator.Speed = new Vector2(-MovementOperator.Speed.x, MovementOperator.Speed.y);
                }

                if (transform.position.y > target.transform.position.y)
                {
                    MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, -MovementOperator.Speed.y);
                }

                transform.localScale = Vector3.Lerp(transform.localScale, new Vector3
                (
                    Math.Abs(Rigidbody.velocity.x) < 1f ? MaxDropletScale : MaxDropletScale / Mathf.Abs(Rigidbody.velocity.x),
                    Math.Abs(Rigidbody.velocity.y) < 1f ? MaxDropletScale : MaxDropletScale / Mathf.Abs(Rigidbody.velocity.y), 
                    1f
                ), 0.02f);
            }
            else if (!TimeOperator.ShouldHold)
            {
                transform.localScale = new Vector3(MaxDropletScale, MaxDropletScale, MaxDropletScale);
            }

            if (!ImmuneToPickup && Vector2.Distance(transform.position, MainPlayerOperator.MainPlayerObject.transform.position) < MinDistanceToAbsorb)
            {
                MainPlayerOperator.MainPlayerObject.GetComponent<EssenceOperator>().AddEssence(Type, Amount);
                SoundEffect.PlaySound(EssencePickupSound, Random.Range(0.75f, 1.25f), GetComponent<TimeOperator>());
                DeathTimeOperator.Handle(gameObject, Combat.CombatAlignment.Enemy, false);
                Pool.Add(gameObject, EssenceDropletSpawnPoint.PoolName, true);
                TimeOperator.CurrentTimeEffect = TimeEffect.None;
                gameObject.SetActive(false);
            }

            transform.up = -GetComponent<Rigidbody2D>().velocity;
        }

        public void Resurrect()
        {
            ImmuneToPickup = true;
            gameObject.SetActive(true);
            Timing.RunCoroutine(DisableImmunity(), Segment.FixedUpdate);

            IEnumerator<float> DisableImmunity()
            {
                yield return Timing.WaitForSeconds(0.25f);
                if (this != null)
                {
                    ImmuneToPickup = false;
                }
            }
        }

        public void Reuse()
        {
            GetComponent<Recorder>().ResetRecorder();
        }

        public class EssenceDropletSnapshot : VisualSnapshot
        {
            public bool StartedValue { get; set; }

            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                StartedValue = gameObject.GetComponent<EssenceDroplet>().Started;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<EssenceDroplet>().Started = StartedValue;
            }
        }
    }
}
