﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.Player
{
    [CreateAssetMenu(fileName = "New Player Sprite Bundle", menuName = "Data/Player/Player Sprite Bundle")]
    public class PlayerSpriteBundle : SerializableScriptableObject
    {
        public Sprite[] Idle;
        public Sprite[] Walking;
        public Sprite[] Jumping;
        public Sprite[] Falling;
        public Sprite[] Crouching;
        public Sprite[] Slide;
        public Sprite[] WallCling;
        public Sprite[] NoArmsIdle;
        public Sprite[] NoArmsWalking;
        public Sprite[] NoArmsJumping;
        public Sprite[] NoArmsFalling;
        public Sprite[] NoArmsCrouching;
        public Sprite[] NoArmsSlide;
        public Sprite[] NoArmsWallCling;
        public Sprite[] Dead;
        public Sprite[] LookingUp;
        public Sprite[] LookingAtArmBand;
    }
}
