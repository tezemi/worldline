﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using System.Linq;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using Worldline.Player.Upgrades;
using Worldline.TimeEngine.Recording;
using Worldline.ScriptedSequences.Generic;
using MEC;
using UnityEngine;

namespace Worldline.Player
{
    /// <summary>
    /// This is the base class for the player and player-like game objects.
    /// The player game object that the player controls is MainPlayerOperator,
    /// and it inherits from this class. RecordingPlayerOperator, which is
    /// used for the recording mechanic, also inherits from this class. This
    /// implements much of the base functionality needed for the player, or
    /// any object that acts similarly to the player, but allows inheriting
    /// classes to implement how the behavior should be used.
    /// </summary>
    [HasDefaultState]
    public abstract class PlayerOperator : Combatant
    {
        public float WalkSpeed = 4.5f;
        public Vector2 NormalHitboxOffset = new Vector2(0f, -0.073f);
        public Vector2 CrouchHitboxOffset = new Vector2(0f, -0.5469875f);
        public Vector2 NormalHitboxSize = new Vector2(0.5604935f, 1.887758f);
        public Vector2 CrouchHitboxSize = new Vector2(0.5604935f, 0.939783f);
        public Sprite Dog;
        public AudioClip HurtSound;
        public PlayerSpriteBundle PlayerSpriteBundle;
        /// <summary>
        /// If true, the player will crouch if they can crouch, meaning that 
        /// CanCrouch must also return true.
        /// </summary>
        public bool Crouch { get; set; }
        public bool WalkLeft { get; set; }
        /// <summary>
        /// Whether or not the player is crouching. Set to true in order to force 
        /// the player to crouch
        /// </summary>
        public bool Crouching { get; set; }
        public bool WalkRight { get; set; }
        public bool ShouldJump { get; set; }
        public bool ImmuneToPit { get; set; }
        public bool DisableCrouch { get; set; }
        public bool OverrideSpeed { get; set; }
        public bool OverrideAnimations { get; set; }
        public bool InImportantEvent { get; set; }
        public bool AllowOutsideLevel { get; set; }
        public bool DisableMovementInput { get; set; }
        public bool TakeDamageWhileHeld { get; set; }
        public List<Upgrade> Upgrades { get; protected set; } = new List<Upgrade>();
        public const float PitDepth = 5;
        public const float InvulnerableFlashDelay = 0.1f;
        public const float VelocityNeededToAnimateWalkAnimation = 0.05f;
        protected WeaponOperator WeaponOperator { get; set; }
        protected CircleCollider2D CrushCollider { get; set; }
        protected PlayerMovementOperator PlayerMovementOperator { get; set; }
        private static List<PlayerOperator> _allPlayers = new List<PlayerOperator>();
        private bool _hold;
        private bool _fixingCrush;
        private bool? _flipXWhileCrouched;
        private CoroutineHandle _invulnerableFlashHandle;
        
        public bool Hold
        {
            get => _hold;
            set
            {
                if (value)
                {
                    if (GetComponent<EssenceOperator>() != null)
                    {
                        GetComponent<EssenceOperator>().EndEffect();
                    }

                    GetComponent<MovementOperator>().Speed = new Vector2(0, 0);
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                    if (WeaponOperator.gameObject.activeSelf)
                    {
                        GetComponentInChildren<PlayerWeaponOperator>().enabled = false;
                    }
                }
                else
                {
                    if (WeaponOperator.gameObject.activeSelf)
                    {
                        GetComponentInChildren<PlayerWeaponOperator>().enabled = true;
                    }
                }

                _hold = value;
            }
        }

        public bool OutsideLevel => !CameraOperator.ActiveCamera.DisableTopBound &&
            transform.position.y > LevelController.Main.MapHeightMax ||
            !CameraOperator.ActiveCamera.DisableBottomBound && transform.position.y < LevelController.Main.MapHeightMin ||
            !CameraOperator.ActiveCamera.DisableRightBound && transform.position.x > LevelController.Main.MapWidthMax ||
            !CameraOperator.ActiveCamera.DisableLeftBound && transform.position.x < LevelController.Main.MapWidthMin;

        public bool CanCrouch => !DisableCrouch && !InImportantEvent && (GetUpgrade<WallJumpUpgrade>() == null ||
                                 !GetUpgrade<WallJumpUpgrade>().OnWall);

        public static List<PlayerOperator> AllPlayers
        {
            get
            {
                _allPlayers.RemoveAll(n => n == null);

                return _allPlayers;
            }
            set => _allPlayers = value;
        }

        protected override void Awake()
        {
            base.Awake();
            AllPlayers.Add(this);
            CrushCollider = GetComponent<CircleCollider2D>();
            WeaponOperator = GetComponentInChildren<WeaponOperator>();
            PlayerMovementOperator = GetComponent<PlayerMovementOperator>();
        }

        protected virtual void OnEnable()
        {
            SpriteRenderer.enabled = true;
        }

        protected virtual void OnDisable()
        {
            Crouch = false;
            Animate();
            Timing.KillCoroutines(GetInstanceID().ToString());
            foreach (Upgrade u in Upgrades)
            {
                u.OnDisabled();
            }
        }

        protected override void Update()
        {
            base.Update();

            // Do animation.
            Animate();

            // Keep player inside level.
            if (!AllowOutsideLevel)
            {
                float xExtends = GetComponent<BoxCollider2D>().bounds.extents.x;
                float boundLeft = LevelController.Main.PhysicsTilemap.cellBounds.min.x + xExtends;
                float boundRight = LevelController.Main.PhysicsTilemap.cellBounds.max.x - xExtends;
                if (transform.position.x > boundRight || transform.position.x < boundLeft)
                {
                    transform.position = transform.position.x < boundLeft ? 
                    new Vector2(boundLeft, transform.position.y) : new Vector2(boundRight, transform.position.y);
                }
            }

            // Kill player when they fall outside of the level (in a pit)
            if (transform.position.y < LevelController.Main.MapHeightMin - PitDepth && !ImmuneToPit)
            {
                if (CheatController.IsActive(Cheat.FallDeathEvent))
                {
                    if (GetComponent<PitEasterEgg>() == null)
                    {
                        gameObject.AddComponent<PitEasterEgg>();
                    }
                    
                    if (!GetComponent<PitEasterEgg>().Active)
                    {
                        GetComponent<PitEasterEgg>().Activate();
                    }
                }
                else
                {
                    Die();
                }
            }

            // We're being frozen or we're outside the level, don't move
            if (_hold || OutsideLevel && transform.position.y < 0 && !AllowOutsideLevel)
            {
                MovementOperator.Speed = new Vector2(0, 0);
                return;
            }

            // Player is stuck under a ceiling, move them out of it when they jump
            bool crouchedOnNormalSurface = Crouching && !MovementOperator.IsOnMovingPlatform;
            bool notDashing = GetUpgrade<DashUpgrade>() == null || GetUpgrade<DashUpgrade>() != null &&
                              !GetUpgrade<DashUpgrade>().Dashing;

            // If they are on a normal surface, and they aren't dashing...
            if (crouchedOnNormalSurface && notDashing)
            {
                RaycastHit2D ceilingHitA = Physics2D.Raycast
                (
                    GetComponent<BoxCollider2D>().bounds.max - new Vector3(0.1f, 0),
                    Vector2.up,
                    .085f,
                    MovementOperator.GroundDetectionLayerMask
                );

                RaycastHit2D ceilingHitB = Physics2D.Raycast
                (
                    GetComponent<BoxCollider2D>().bounds.max -
                    new Vector3(GetComponent<BoxCollider2D>().bounds.size.x, 0) +
                    new Vector3(0.1f, 0),
                    Vector2.up,
                    .085f,
                    MovementOperator.GroundDetectionLayerMask
                );

                if (ceilingHitA || ceilingHitB) // And there is a surface above their head...
                {
                    Crouching = true; // Force them to crouch
                    if (PlayerMovementOperator.ShouldJump) // And allow them to "nudge" their way out
                    {
                        if (_flipXWhileCrouched == null)
                        {
                            _flipXWhileCrouched = SpriteRenderer.flipX;
                        }

                        if (HasUpgrade<DashUpgrade>())
                        {
                            Rigidbody.velocity = new Vector2
                            (
                                (GetUpgrade<DashUpgrade>().LastDashDirection & DashDirection.Left) != 0 ? WalkSpeed : -WalkSpeed,
                                MovementOperator.Speed.y
                            );
                        }
                        else if (_flipXWhileCrouched.HasValue)
                        {
                            Rigidbody.velocity = new Vector2
                            (
                                _flipXWhileCrouched.Value ? WalkSpeed : -WalkSpeed,
                                MovementOperator.Speed.y
                            );
                        }
                    }
                }
                else
                {
                    Crouching = Crouch && CanCrouch;
                }
            }
            else
            {
                Crouching = Crouch && CanCrouch;
            }

            // Crouching behavior
            if (Crouching)
            {
                MovementOperator.Speed = new Vector2(0f, MovementOperator.Speed.y);
                foreach (BoxCollider2D box in GetComponents<BoxCollider2D>())
                {
                    if (box.offset != CrouchHitboxOffset || box.size != CrouchHitboxSize)
                    {
                        box.offset = CrouchHitboxOffset;
                        box.size = CrouchHitboxSize;
                    }
                }

                CrushCollider.offset = CrouchHitboxOffset;
            }
            else
            {
                _flipXWhileCrouched = null;
                Crouching = false;
                foreach (BoxCollider2D box in GetComponents<BoxCollider2D>())
                {
                    if (box.offset != NormalHitboxOffset || box.size != NormalHitboxSize)
                    {
                        box.offset = NormalHitboxOffset;
                        box.size = NormalHitboxSize;
                    }
                }

                CrushCollider.offset = Vector3.zero;
            }

            // Walking behavior
            PlayerMovementOperator.ShouldJump = ShouldJump;
            if (!OverrideSpeed && !Crouching)
            {
                if (WalkLeft)
                {
                    MovementOperator.Speed = new Vector2(-WalkSpeed, MovementOperator.Speed.y);
                }
                else if (WalkRight)
                {
                    MovementOperator.Speed = new Vector2(WalkSpeed, MovementOperator.Speed.y);
                }
                else
                {
                    MovementOperator.Speed = new Vector2(0, MovementOperator.Speed.y);
                }
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            // Crushing Behavior
            if (PlayerIsCrushed() && !_fixingCrush && !Hold && !GetComponent<TimeOperator>().ShouldHold)
            {
                Timing.RunCoroutine(FindSafeSpace(), Segment.FixedUpdate, GetInstanceID().ToString());

                IEnumerator<float> FindSafeSpace()
                {
                    _fixingCrush = true;
                    SolidCollider.enabled = false;
                    GetComponent<PlayerMovementOperator>().enabled = false;
                    Recorder recorder = GetComponent<Recorder>();
                    for (int i = recorder.CurrentIndex - 2; i != -1; i -= 5)
                    {
                        if (i < 0)
                        {
                            i = recorder.Snapshots.Length - 2;
                        }

                        if (recorder.Snapshots[i] != null)
                        {
                            transform.position = recorder.Snapshots[i].Position;
                        }

                        yield return Timing.WaitForOneFrame;
                        if (!PlayerIsCrushed())
                        {
                            break;
                        }
                    }

                    _fixingCrush = false;
                    SolidCollider.enabled = true;
                    GetComponent<PlayerMovementOperator>().enabled = true;
                }
            }

            // Checks to see if small circle collider is 
            // center of player object is being touched 
            // by a surface.
            bool PlayerIsCrushed()
            {
                return !other.isTrigger && !other.IsSolidTop() && other.gameObject.layer == MovementOperator.GroundDetectionLayerMask && CrushCollider.IsTouchingLayers(MovementOperator.GroundDetectionLayerMask);
            }
        }

        public virtual void Animate()
        {
            if (OverrideAnimations || CombatantTimeOperator.ShouldHold || LevelController.Main.Paused)
            {
                return;
            }

            bool hasWeapon = WeaponOperator != null && WeaponOperator.gameObject.activeSelf;
            if (Crouching)
            {
                SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsCrouching : PlayerSpriteBundle.Crouching);
                if (WeaponOperator.isActiveAndEnabled)
                {
                    SpriteRenderer.flipX = WeaponOperator.GetComponent<SpriteRenderer>().flipY;
                }
            }
            else
            {
                if (GetUpgrade<WallJumpUpgrade>() != null && GetUpgrade<WallJumpUpgrade>().OnWall)
                {
                    SpriteRenderer.flipX = GetUpgrade<WallJumpUpgrade>().OnRightWall;
                }
                else if (!hasWeapon && !Hold && Mathf.Abs(Rigidbody.velocity.x) > VelocityNeededToAnimateWalkAnimation)
                {
                    SpriteRenderer.flipX = Rigidbody.velocity.x < 0f;
                }
                else if (hasWeapon)
                {
                    SpriteRenderer.flipX = WeaponOperator.GetComponent<SpriteRenderer>().flipY;
                }

                if (!PlayerMovementOperator.OnSlope)
                {
                    if (GetUpgrade<WallJumpUpgrade>() != null && GetUpgrade<WallJumpUpgrade>().OnWall)
                    {
                        SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsWallCling : PlayerSpriteBundle.WallCling);
                    }
                    else if (Rigidbody.velocity.y > 0f)
                    {
                        SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsJumping : PlayerSpriteBundle.Jumping);
                    }
                    else if (Rigidbody.velocity.y < 0f)
                    {
                        SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsFalling : PlayerSpriteBundle.Falling);
                    }
                    else if (Math.Abs(Rigidbody.velocity.x) > 0f)
                    {
                        SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsWalking : PlayerSpriteBundle.Walking);
                    }
                    else if (Math.Abs(Rigidbody.velocity.x) < .001f)
                    {
                        SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsIdle : PlayerSpriteBundle.Idle);
                    }
                }
                else
                {
                    if (PlayerMovementOperator.Grounded)
                    {
                        if (Math.Abs(Rigidbody.velocity.x) > 0f)
                        {
                            SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsWalking : PlayerSpriteBundle.Walking);
                        }
                        else if (Math.Abs(Rigidbody.velocity.x) < .001f)
                        {
                            SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsIdle : PlayerSpriteBundle.Idle);
                        }
                    }
                    else
                    {
                        if (Rigidbody.velocity.y > 0f)
                        {
                            SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsJumping : PlayerSpriteBundle.Jumping);
                        }
                        else if (Rigidbody.velocity.y < 0f)
                        {
                            SetAnimation(hasWeapon ? PlayerSpriteBundle.NoArmsFalling : PlayerSpriteBundle.Falling);
                        }
                    }
                }
            }
        }

        public virtual void Heal(int health)
        {
            if (Health + health > MaxHealth)
            {
                Health = MaxHealth;
            }
            else
            {
                Health += health;
            }
        }

        public override void Hurt(int damage, object other)
        {
            if (Invulnerable || _hold && !TakeDamageWhileHeld) return;

            if (damage > MaxHealth)
            {
                damage = MaxHealth;
            }

            Health -= damage;
            MakeInvulnerable();
            SoundEffect.PlaySound(HurtSound, CombatantTimeOperator);
            if (Health <= 0)
            {
                SpriteRenderer.enabled = true;
                Die();
            }
            else
            {
                CombatantTimeOperator.Hurt(damage, other);
            }
        }

        public override void MakeInvulnerable()
        {
            if (!Invulnerable)
            {
                base.MakeInvulnerable();
                Timing.KillCoroutines(_invulnerableFlashHandle);
                _invulnerableFlashHandle = Timing.RunCoroutine
                (
                    FlashWhileInvulnerable(),
                    Segment.FixedUpdate
                );
            }

            IEnumerator<float> FlashWhileInvulnerable()
            {
                while (Invulnerable)
                {
                    yield return Timing.WaitForSeconds(InvulnerableFlashDelay);
                    SpriteRenderer.enabled = !SpriteRenderer.enabled;
                }

                SpriteRenderer.enabled = true;
            }
        }

        /// <summary>
        /// Gives an Upgrade to the player, and calls the Upgrade's OnAdded method.
        /// This will remove any Upgrades that the player has that are defined in
        /// this Upgrade's InheritsFrom field.
        /// </summary>
        /// <param name="upgrade">The upgrade to give the player.</param>
        public void GiveUpgrade(Upgrade upgrade)
        {
            if (Upgrades.Any(u => u.GetType() == upgrade.GetType()))
            {
                Debug.LogWarning($"Tried to give upgrade of type {upgrade.name} more than once.");
                return;
            }

            Upgrade copy = Instantiate(upgrade);
            List<Upgrade> remove = new List<Upgrade>();
            foreach (Upgrade u in Upgrades)
            {
                if (upgrade.InheritsFrom == null) continue;

                if (upgrade.InheritsFrom.GetType() == u.GetType())
                {
                    remove.Add(u);
                }
            }

            foreach (Upgrade u in remove)
            {
                RemoveUpgrade(u.GetType());
            }

            copy.PlayerOperator = this;
            copy.OnAdded();
            Upgrades.Add(copy);
        }

        /// <summary>
        /// Removes an upgrade from the player and calls the OnRemoved method.
        /// </summary>
        /// <param name="upgrade">The upgrade to remove.</param>
        public void RemoveUpgrade(Upgrade upgrade)
        {
            upgrade.OnRemoved();
            Upgrades.Remove(upgrade);
        }

        /// <summary>
        /// Removes of upgrade of the specified type from the player, of more 
        /// than one upgrade of a certain type exists, the first occurence will
        /// only be removed.
        /// </summary>
        /// <typeparam name="T">The type of Upgrade to remove.</typeparam>
        public void RemoveUpgrade<T>()
        {
            Upgrade upgrade = Upgrades.FirstOrDefault(u => u.GetType() == typeof(T));
            if (upgrade != null)
            {
                upgrade.OnRemoved();
                Upgrades.Remove(upgrade);
                Destroy(upgrade);
            }
        }

        /// <summary>
        /// Removes of upgrade of the specified type from the player, of more 
        /// than one upgrade of a certain type exists, the first occurence will
        /// only be removed.
        /// </summary>
        /// <param name="t">The type of Upgrade to remove.</param>
        public void RemoveUpgrade(Type t)
        {
            Upgrade upgrade = Upgrades.SingleOrDefault(u => u.GetType() == t);
            if (upgrade != null)
            {
                upgrade.OnRemoved();
                Upgrades.Remove(upgrade);
                Destroy(upgrade);
            }
        }

        /// <summary>
        /// Gets the Upgrade of the specific type if the player has it, if not, 
        /// returns null.
        /// </summary>
        /// <typeparam name="T">The type of Upgrade to get.</typeparam>
        /// <returns>If found, return the Upgrade of the specified type.</returns>
        public T GetUpgrade<T>() where T : Upgrade
        {
            return (T)Upgrades.FirstOrDefault(u => u.GetType() == typeof(T));
        }

        /// <summary>
        /// Checks to see if the player has the specified Upgrade.
        /// </summary>
        /// <typeparam name="T">The type of Upgrade to check for.</typeparam>
        /// <returns>True if the player has this upgrade.</returns>
        public bool HasUpgrade<T>() where T : Upgrade
        {
            return Upgrades.Any(u => u.GetType() == typeof(T));
        }

        [SetsUpDefaults]
        public static void ResetDefaults()
        {
            AllPlayers = new List<PlayerOperator>();
        }
    }
}
