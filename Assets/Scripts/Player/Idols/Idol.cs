﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.Player.Idols
{
    public abstract class Idol
    {
        public abstract string Name { get; }
        public abstract string Description { get; }
        public abstract Type Artifact { get; }

        public abstract void Add();
        public abstract void Remove();
    }
}
