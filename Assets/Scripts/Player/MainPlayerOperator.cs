// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GUI.HUD;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.Player.Upgrades;
using Worldline.ScriptedSequences;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.Player
{
    /// <summary>
    /// This componenet is used for the player game object that the player 
    /// controls. This implements the functionality of the PlayerOperator
    /// class by using its members to allow the user to control the player
    /// via input. This also introduces some more specific behavior, such
    /// as main player specific death behavior, and interacts with events. 
    /// </summary>
    public class MainPlayerOperator : PlayerOperator
    {
        public static GameObject MainPlayerObject;
        public static MainPlayerOperator MainPlayerComponent;
        public GameObject DeadPlayerPrefab;
        public bool InEvent { get; set; }
        public List<ScriptedSequence> EventsTouching { get; set; } = new List<ScriptedSequence>();
        private bool _initializedJump;
        
        protected override void Awake()
        {
            base.Awake();
            if (MainPlayerObject == null || MainPlayerComponent == null)
            {
                MainPlayerObject = gameObject;
                MainPlayerComponent = this;
            }

            DontDestroyOnLoad(gameObject);
            this.DestroyOnSceneLoad();

            if (CheatController.IsActive(Cheat.Dog))
            {
                GameObject dog = new GameObject("Dog", typeof(SpriteRenderer));
                dog.GetComponent<SpriteRenderer>().sprite = Dog;
                dog.transform.SetParent(transform);
                dog.transform.localPosition = new Vector3(-0.15f, 1.15f, 0.15f);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (CheatController.IsActive(Cheat.WallJump) && !HasUpgrade<WallJumpUpgrade>())
            {
                GiveUpgrade(DebugUtils.Main.WallJumpUpgrade);
            }

            if (CheatController.IsActive(Cheat.Dash) && !HasUpgrade<DashUpgrade>())
            {
                GiveUpgrade(DebugUtils.Main.DashUpgrade);
            }

            if (CheatController.IsActive(Cheat.InfiniteJump) && !HasUpgrade<DashUpgrade>())
            {
                GiveUpgrade(DebugUtils.Main.InfiniteJumpUpgrade);
            }

            if (CheatController.IsActive(Cheat.ThiccWynne))
            {
                transform.localScale = new Vector3(2f, transform.localScale.y, transform.localScale.z);
                Rigidbody.gravityScale = 5f;
            }
        }

        protected override void Update()
        {
            if (_initializedJump && !PlayerMovementOperator.Jumping)
            {
                _initializedJump = false;
            }

            if (!_initializedJump)
            {
                _initializedJump = InputManager.GetButtonDown("up");
            }

            if (!DisableMovementInput)
            {
                Crouch = InputManager.GetButton("down") && EventsTouching.Count == 0;
                ShouldJump = InputManager.GetButton("up") && _initializedJump;
                WalkLeft = !Crouch && InputManager.GetButton("left");
                WalkRight = !WalkLeft && !Crouch && InputManager.GetButton("right");
            }

            if ((CheatController.IsActive(Cheat.BindKKill) || Debug.isDebugBuild) && Input.GetKeyDown(KeyCode.K))
            {
                Die();
            }

            base.Update();
        }

        protected virtual void FixedUpdate()
        {
            if (SaveFile.LoadedSaveFile.Difficulty == Difficulty.AssBlasting && (Health > 1 || MaxHealth > 1))
            {
                MaxHealth = 1;
                Health = 1;
            }
        }

        protected override void OnTriggerEnter2D(Collider2D other)
        {
            base.OnTriggerEnter2D(other);

            // Tells the Event class what events the player is standing in.
            if (other.GetComponent<ScriptedSequence>() != null && 
            other.GetComponent<ScriptedSequence>().ScriptedSequenceStartMode == ScriptedSequenceStartMode.OnInteract && 
            !EventsTouching.Contains(other.GetComponent<ScriptedSequence>()))
            {
                EventsTouching.Add(other.GetComponent<ScriptedSequence>());
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other.GetComponent<ScriptedSequence>() != null && EventsTouching.Contains(other.GetComponent<ScriptedSequence>()))
            {
                EventsTouching.Remove(other.GetComponent<ScriptedSequence>());
            }
        }

        public override void Heal(int health)
        {
            if (health > 0)
            {
                TextPopupController.Main.ShowDamagePopup(-health, transform.position);
            }

            base.Heal(health);
        }

        public override void Hurt(int damage, object other)
        {
            if (Invulnerable || Hold && !TakeDamageWhileHeld || CheatController.IsActive(Cheat.God)) return;

            GameObject popup = TextPopupController.Main.ShowDamagePopup(damage, transform.position);
            popup.GetComponent<Text>().color = new Color(1f, 0.4f, 0f, 1f);
            float hudShake = (float)damage / MaxHealth * 5f;
            HUDController.Main.Shake(hudShake);
            base.Hurt(damage, other);
        }

        public override void Die()
        {
            if (!Dead)
            {
                Dead = true;
                OverrideSpeed = false;
                AnimationOperator.enabled = false;
                Vector2 vel = Rigidbody.velocity;
                GetComponent<SpriteRenderer>().color = Color.white;
                GetComponent<EssenceOperator>().EndEffect();

                gameObject.SetActive(false);
                GameObject dp = Instantiate(DeadPlayerPrefab);
                dp.transform.position = new Vector3
                (
                    transform.position.x, 
                    transform.position.y, 
                    dp.transform.position.z
                );

                dp.GetComponent<Rigidbody2D>().velocity = vel;
            }
        }
    }
}
