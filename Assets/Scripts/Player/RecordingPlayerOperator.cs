﻿// Copyright (c) 2019 Destin Hebner
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player
{
    /// <summary>
    /// This type of player operator is used for the recording mechanic,
    /// and implements the base PlayerOperator functionality by "mirroring"
    /// a specified PlayerOperator. This PlayerOperator will copy many
    /// actions that the specified one will make, and walk the opposite
    /// direction of the specified one.
    /// </summary>
    public class RecordingPlayerOperator : PlayerOperator
    {
        public PlayerOperator Mimicking;
        protected PlayerWeaponOperator PlayerWeaponOperator { get; set; }
        private bool _setPauseHere;

        protected override void Awake()
        {
            base.Awake();
            PlayerWeaponOperator = GetComponentInChildren<PlayerWeaponOperator>();
        }

        protected override void Update()
        {
            if (Mimicking == null)
            {
                return;
            }

            if (InputManager.GetButton("pause_recording") && CombatantTimeOperator.CurrentTimeEffect == TimeEffect.None)
            {
                CombatantTimeOperator.CurrentTimeEffect = TimeEffect.Pause;
                _setPauseHere = true;
            }
            else if (!InputManager.GetButton("pause_recording") && _setPauseHere)
            {
                CombatantTimeOperator.CurrentTimeEffect = TimeEffect.None;
                _setPauseHere = false;
            }

            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Pause)
            {
                SpriteRenderer.color = TimeOperator.EffectColors[TimeEffect.Pause] -
                                       new Color(0f, 0f, 0f, _setPauseHere ? Random.Range(0.25f, 0.75f) : 0f);

                foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
                {
                    spriteRenderer.color = TimeOperator.EffectColors[TimeEffect.Pause] -
                                           new Color(0f, 0f, 0f, _setPauseHere ? Random.Range(0.25f, 0.75f) : 0f);
                }
            }
            else if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.None)
            {
                SpriteRenderer.color = TimeOperator.EffectColors[TimeEffect.Record];
                foreach (SpriteRenderer spriteRenderer in GetComponentsInChildren<SpriteRenderer>())
                {
                    spriteRenderer.color = TimeOperator.EffectColors[TimeEffect.Record];
                }
            }

            Health = Mimicking.Health;
            MaxHealth = Mimicking.MaxHealth;
            if (!CombatantTimeOperator.ShouldHold)
            {
                Crouch = Mimicking.Crouch;
                ShouldJump = Mimicking.ShouldJump;
                WalkLeft = Mimicking.WalkRight;
                WalkRight = Mimicking.WalkLeft;
            }

            base.Update();
        }

        public override void Die()
        {
            if (!Dead)
            {
                Dead = true;
                CombatantTimeOperator.Evaporate();
            }
        }
    }
}
