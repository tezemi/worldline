﻿using System.Collections.Generic;
using Worldline.Level;
using Worldline.Visuals;
using Worldline.GUI.HUD;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.GUI.Menus;
using Worldline.TimeEngine;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.Player
{
    public class DeadPlayer2 : MonoBehaviour
    {
        public static DeadPlayer2 Main { get; protected set; }
        public Sprite DeathSpotlight;
        public Sprite BlackBackground;
        public AudioClip GameOverMusic;
        public AudioClip ResetActivatedSound;
        public bool GameOverMenuOpen { get; protected set; }
        public bool PlayerCanReset => EssenceOperator.Main.EssenceAmounts[TimeEffect.Reset] > 0 && !CheatController.IsActive(Cheat.Nightmare);
        public const float LongWaitTime = 4.5f;
        public const float ShortWaitTime = 0.15f;
        public const float DistanceOutsideLevel = 10f;
        public const float AnimationSpeedDelay = 0.25f;
        public const float BackgroundMusicPitchChangeSpeed = 0.01f;
        protected Rigidbody2D Rigidbody { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected EssenceOperator EssenceOperator { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            Rigidbody = GetComponent<Rigidbody2D>();
            TimeOperator = GetComponent<TimeOperator>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            MovementOperator = GetComponent<MovementOperator>();
        }

        protected virtual void OnEnable()
        {
            CameraOperator.ActiveCamera.Track = gameObject;
            SpriteRenderer.flipX = MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX;
            HUDController.Main.Shake(3);

            Timing.KillCoroutines(GetInstanceID().ToString());
            Timing.RunCoroutine(ClearOverlay(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(SlowBackgroundMusic(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(Animate(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
        }

        protected virtual void Update()
        {
            if (!GameOverMenuOpen && PlayerCanReset && TimeOperator.CurrentTimeEffect != TimeEffect.Reset && !MainPlayerOperator.MainPlayerComponent.InEvent)
            {
                if (InputManager.GetButtonDown("time_power_6") || InputManager.GetButtonDown("activate_essence"))
                {
                    ActivateReset();
                }

                CrystalElementsController.Main.ShowResetInfo = true;
            }
            else
            {
                CrystalElementsController.Main.ShowResetInfo = false;
            }

            if (GameOverMenuOpen)
            {
                foreach (AudioSource a in FindObjectsOfType<AudioSource>())
                {
                    if (a != BackgroundMusicOperator.Main.MainAudioSource && !BackgroundMusicOperator.Main.ExtraAudioSources.Contains(a))
                    {
                        a.mute = true;
                    }
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (transform.position.y < LevelController.Main.MapHeightMin - DistanceOutsideLevel)
            {
                Rigidbody.bodyType = RigidbodyType2D.Static;
            }
        }

        protected IEnumerator<float> SlowBackgroundMusic()
        {
            while (BackgroundMusicOperator.Main.Speed > 0.1f)
            {
                BackgroundMusicOperator.Main.Speed -= BackgroundMusicPitchChangeSpeed;
                yield return Timing.WaitForOneFrame;
            }

            BackgroundMusicOperator.Main.Speed = 0.1f;
        }

        protected IEnumerator<float> SpeedBackgroundMusic()
        {
            while (BackgroundMusicOperator.Main.Speed < 1f)
            {
                BackgroundMusicOperator.Main.Speed += BackgroundMusicPitchChangeSpeed;
                yield return Timing.WaitForOneFrame;
            }

            BackgroundMusicOperator.Main.Speed = 1f;
        }

        protected IEnumerator<float> Animate()
        {
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[0];
            yield return Timing.WaitForSeconds(AnimationSpeedDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[1];
            yield return Timing.WaitForSeconds(AnimationSpeedDelay);
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => GetComponent<MovementOperator>().Grounded || transform.position.y < LevelController.Main.MapHeightMin - DistanceOutsideLevel)
            );

            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[2];
            yield return Timing.WaitForSeconds(AnimationSpeedDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[3];
            yield return Timing.WaitForSeconds(AnimationSpeedDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[4];
            yield return Timing.WaitForSeconds(PlayerCanReset ? LongWaitTime : ShortWaitTime);
            Timing.RunCoroutine(LoadGameOverMenu(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> LoadGameOverMenu()
        {
            GameOverMenuOpen = true;

            Timing.RunCoroutine(FadeOutHUD(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(FadeInBackground(), Segment.FixedUpdate, GetInstanceID().ToString());

            yield return Timing.WaitUntilDone(new WaitUntil(() => BackgroundMusicOperator.Main.Speed <= 0.1f));
            yield return Timing.WaitForOneFrame;

            BackgroundMusicOperator.Main.Volume = 0f;
            yield return Timing.WaitForOneFrame;
            BackgroundMusicOperator.Main.Speed = 0.85f;
            yield return Timing.WaitForOneFrame;
            BackgroundMusicOperator.Main.AudioClip = GameOverMusic;
            BackgroundMusicOperator.Main.MainAudioSource.loop = false;

            GameOverMenu.Main.gameObject.SetActive(true);
            Timing.RunCoroutine(ShineSpotlight(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected IEnumerator<float> FadeOutHUD()
        {
            HUDController.Main.enabled = false;

            bool opaqueElementExists = true;

            while (opaqueElementExists && HUDController.Main != null)
            {
                opaqueElementExists = false;
                foreach (Transform element in HUDController.Main.GetComponentsInChildren<Transform>())
                {
                    if (element.GetComponent<Image>() != null)
                    {
                        element.GetComponent<Image>().color = Vector4.MoveTowards
                        (
                            element.GetComponent<Image>().color,
                            Color.clear,
                            0.02f
                        );

                        if (element.GetComponent<Image>().color.a > 0f)
                        {
                            opaqueElementExists = true;
                        }
                    }
                    else if (element.GetComponent<Text>() != null)
                    {
                        element.GetComponent<Text>().color = Vector4.MoveTowards
                        (
                            element.GetComponent<Text>().color,
                            Color.clear,
                            0.02f
                        );

                        if (element.GetComponent<Text>().color.a > 0f)
                        {
                            opaqueElementExists = true;
                        }
                    }
                }

                yield return Timing.WaitForOneFrame;
            }
        }

        protected IEnumerator<float> FadeInBackground()
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, CameraOperator.ActiveCamera.transform.position.z + 0.5f);

            GameObject blackBackgroundGameObject = new GameObject("Game Over Background", typeof(SpriteRenderer));
            blackBackgroundGameObject.GetComponent<SpriteRenderer>().sprite = BlackBackground;
            blackBackgroundGameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
            blackBackgroundGameObject.transform.localScale = new Vector3(10f, 10f, 1f);
            blackBackgroundGameObject.transform.SetParent(transform);
            blackBackgroundGameObject.transform.localPosition = new Vector3(0f, 0f, 0.5f);

            while (blackBackgroundGameObject.GetComponent<SpriteRenderer>().color != Color.white)
            {
                yield return Timing.WaitForOneFrame;
                blackBackgroundGameObject.GetComponent<SpriteRenderer>().color = Vector4.MoveTowards
                (
                    blackBackgroundGameObject.GetComponent<SpriteRenderer>().color,
                    Color.white,
                    0.04f
                );
            }
            
            Rigidbody.bodyType = RigidbodyType2D.Static;
            CameraOperator.ActiveCamera.Mode = CameraMode.Lerp;
            CameraOperator.ActiveCamera.HorizontalOffset = 0f;
            CameraOperator.ActiveCamera.DisableBounds = true;
            CameraOperator.ActiveCamera.Track = null;
            CameraOperator.ActiveCamera.LerpSpeed = 0.05f;
            CameraOperator.ActiveCamera.Focus = transform.position + new Vector3(0f, 2.5f, 0f);
        }

        protected IEnumerator<float> ShineSpotlight()
        {
            GameObject deathSpotlightGameObject = new GameObject("Death Spotlight", typeof(SpriteRenderer));
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().sprite = DeathSpotlight;
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
            deathSpotlightGameObject.transform.SetParent(transform);
            deathSpotlightGameObject.transform.localPosition = new Vector3(0f, -1f, 0.25f);

            while (deathSpotlightGameObject.GetComponent<SpriteRenderer>().color != Color.white)
            {
                yield return Timing.WaitForOneFrame;
                deathSpotlightGameObject.GetComponent<SpriteRenderer>().color = Vector4.MoveTowards
                (
                    deathSpotlightGameObject.GetComponent<SpriteRenderer>().color,
                    Color.white,
                    0.02f
                );
            }
        }

        protected IEnumerator<float> ClearOverlay()
        {
            while (CameraOperator.ActiveCamera.Overlay.color.a > 0f)
            {
                yield return Timing.WaitForOneFrame;
                CameraOperator.ActiveCamera.Overlay.color = Vector4.MoveTowards
                (
                    CameraOperator.ActiveCamera.Overlay.color,
                    new Color(1f, 1f, 1f, 0f),
                    0.02f
                );
            }
        }

        public void ActivateReset()
        {
            SoundEffect.PlaySound(ResetActivatedSound);
            TimeOperator.CurrentTimeEffect = TimeEffect.Reset;
            Timing.KillCoroutines(GetInstanceID().ToString());
            Timing.RunCoroutine(SpeedBackgroundMusic(), Segment.FixedUpdate, GetInstanceID().ToString());
        }
    }
}
