﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.Player.Upgrades
{
    public enum UpgradeType 
    {
        Player,
        Weapon,
        Essence
    }
}