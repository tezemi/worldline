﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Wall Jump Upgrade", menuName = "Data/Upgrades/Player/WallJumpUpgrade")]
    public class WallJumpUpgrade : Upgrade
    {
        public virtual bool On { get; protected set; } = true;
        public virtual bool WallJumping { get; protected set; }
        public virtual bool OnRightWall { get; protected set; }
        public virtual float WallClingForce { get; protected set; } = -1f;
        public virtual float JumpHorizontalForce { get; protected set; } = 4f;
        public virtual float JumpCooldown { get; protected set; } = 0.04f;
        public virtual float LungeCooldown { get; protected set; } = 0.02f;
        public const float RaycastDistance = 0.3f;
        protected Rigidbody2D Rigidbody { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        private bool _onWall;

        public virtual bool OnWall
        {
            get => _onWall;
            protected set
            {
                PlayerOperator.DisableCrouch = value;
                _onWall = value;
            }
        }

        public override void OnAdded()
        {
            TimeOperator = PlayerOperator.GetComponent<TimeOperator>();
            Rigidbody = PlayerOperator.GetComponent<Rigidbody2D>();
            SpriteRenderer = PlayerOperator.GetComponent<SpriteRenderer>();
            MovementOperator = PlayerOperator.GetComponent<MovementOperator>();
            Timing.RunCoroutine(Update(), Segment.Update, GetInstanceID().ToString());
            Timing.RunCoroutine(FixedUpdate(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        public override void OnRemoved()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        public virtual bool RaycastIsValid(RaycastHit2D raycast)
        {
            return !raycast.collider.isTrigger && !raycast.collider.IsSolidTop() &&
            raycast.collider.GetComponent<DisableWallJump>() == null;
        }

        public virtual bool ShouldWallCling(RaycastHit2D[] raycasts)
        {
            foreach (RaycastHit2D raycast in raycasts)
            {
                if (raycast && RaycastIsValid(raycast))
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsHoldingRightButton()
        {
            if (PlayerOperator is RecordingPlayerOperator)
            {
                return InputManager.GetButton("left");
            }

            return InputManager.GetButton("right");
        }

        public bool IsHoldingLeftButton()
        {
            if (PlayerOperator is RecordingPlayerOperator)
            {
                return InputManager.GetButton("right");
            }

            return InputManager.GetButton("left");
        }

        protected virtual IEnumerator<float> Update()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (PlayerOperator == null || !PlayerOperator.isActiveAndEnabled) goto start;

            PlayerMovementOperator op = PlayerOperator.GetComponent<PlayerMovementOperator>();
            RaycastHit2D[] raycastsRight = Physics2D.RaycastAll
            (
                PlayerOperator.transform.position,
                Vector2.right,
                RaycastDistance,
                op.GroundDetectionLayerMask
            );

            RaycastHit2D[] raycastsLeft = Physics2D.RaycastAll
            (
                PlayerOperator.transform.position,
                Vector2.left,
                RaycastDistance,
                op.GroundDetectionLayerMask
            );

            bool shouldClingLeft = ShouldWallCling(raycastsLeft);
            bool shouldClingRight = ShouldWallCling(raycastsRight);
            if ((shouldClingLeft || shouldClingRight) && !op.Grounded)
            {
                bool hasWeapon = PlayerOperator.GetComponentInChildren<PlayerWeaponOperator>() != null &&
                                 PlayerOperator.GetComponentInChildren<PlayerWeaponOperator>().isActiveAndEnabled;

                if (shouldClingRight && IsHoldingRightButton() && Rigidbody.velocity.y <= 0)
                {
                    OnWall = true;
                    OnRightWall = true;
                    PlayerOperator.Animate();
                    SpriteRenderer.sprite = hasWeapon ?
                        PlayerOperator.PlayerSpriteBundle.NoArmsWallCling[0] :
                        PlayerOperator.PlayerSpriteBundle.WallCling[0];

                    if (On && InputManager.GetButtonDown("up"))
                    {
                        On = false;
                        WallJumping = true;
                        op.ForceJump();
                        PlayerOperator.OverrideSpeed = true;
                        MovementOperator.Speed = new Vector2(-JumpHorizontalForce, 0);
                        Rigidbody.AddForce(new Vector2(-JumpHorizontalForce, 0f), ForceMode2D.Impulse);
                        Timing.RunCoroutine(EnableMovementOpCooldown(), Segment.FixedUpdate);
                        Timing.RunCoroutine(EnableModuleCooldown(), Segment.FixedUpdate);
                    }
                }
                else if (shouldClingLeft && IsHoldingLeftButton() && Rigidbody.velocity.y <= 0)
                {
                    OnWall = true;
                    OnRightWall = false;
                    PlayerOperator.Animate();
                    SpriteRenderer.sprite = hasWeapon ?
                        PlayerOperator.PlayerSpriteBundle.NoArmsWallCling[0] : 
                        PlayerOperator.PlayerSpriteBundle.WallCling[0];

                    if (On && InputManager.GetButtonDown("up"))
                    {
                        On = false;
                        WallJumping = true;
                        op.ForceJump();
                        PlayerOperator.OverrideSpeed = true;
                        MovementOperator.Speed = new Vector2(JumpHorizontalForce, 0);
                        Rigidbody.AddForce(new Vector2(JumpHorizontalForce, 0f), ForceMode2D.Impulse);
                        Timing.RunCoroutine(EnableMovementOpCooldown(), Segment.FixedUpdate);
                        Timing.RunCoroutine(EnableModuleCooldown(), Segment.FixedUpdate);
                    }
                }
                else
                {
                    OnWall = false;
                }

                IEnumerator<float> EnableMovementOpCooldown()
                {
                    yield return Timing.WaitForSeconds(LungeCooldown);
                    if (PlayerOperator.OverrideSpeed)
                    {
                        PlayerOperator.OverrideSpeed = false;
                        WallJumping = false;
                    }
                }

                IEnumerator<float> EnableModuleCooldown()
                {
                    yield return Timing.WaitForSeconds(JumpCooldown);
                    WallJumping = false;
                    On = true;
                }
            }
            else
            {
                OnWall = false;
            }

            if (this != null)
            {
                goto start;
            }
        }

        protected virtual IEnumerator<float> FixedUpdate()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (OnWall)
            {
                Rigidbody.velocity = new Vector2(Rigidbody.velocity.x, WallClingForce);
            }

            if (this != null)
            {
                goto start;
            }
        }
    }
}
