﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GUI;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using Worldline.Level;

namespace Worldline.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Map Upgrade", menuName = "Data/Upgrades/Player/MapUpgrade")]
    public class MapUpgrade : Upgrade
    {
        public override void OnAdded()
        {
            Timing.RunCoroutine(Update(), Segment.Update, GetInstanceID().ToString());
        }

        public override void OnRemoved()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> Update()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (MainPlayerOperator.MainPlayerObject == null || !MainPlayerOperator.MainPlayerObject.activeSelf) goto start;

            if (InputManager.GetButtonDown("map") && LevelController.Main.SetPaused(!LevelController.Main.Paused, this))
            {
                Map.Main.gameObject.SetActive(!Map.Main.gameObject.activeSelf);
            }

            if (this != null)
            {
                goto start;
            }
        }
    }
}
