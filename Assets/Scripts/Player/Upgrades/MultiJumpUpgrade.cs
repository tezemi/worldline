// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using Worldline.Physics;
using MEC;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Multi Jump Upgrade", menuName = "Data/Upgrades/Player/MultiJumpUpgrade")]
    public class MultiJumpUpgrade : Upgrade
    {
        public int ExtraJumps = 1;
        public int CurrentJumps { get; set; }
        public bool ExpendedJumps { get; set; }
        public const int AdditionalJumpReduction = 2;
        protected PlayerMovementOperator PlayerMovementOperator { get; set; }
        protected WallJumpUpgrade WallJumpUpgrade { get; set; }

        public override void OnAdded()
        {
            WallJumpUpgrade = (WallJumpUpgrade)PlayerOperator.Upgrades.SingleOrDefault(u => u is WallJumpUpgrade);
            PlayerMovementOperator = PlayerMovementOperator.Main;
            Timing.RunCoroutine(Update(), Segment.Update, GetInstanceID().ToString());
        }

        public override void OnRemoved()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> Update()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (MainPlayerOperator.MainPlayerObject == null || !MainPlayerOperator.MainPlayerObject.activeSelf) goto start;

            if (PlayerMovementOperator == null)
            {
                PlayerMovementOperator = PlayerMovementOperator.Main;
                goto start;
            }

            if (ExpendedJumps && PlayerMovementOperator.Grounded)
            {
                CurrentJumps = 0;
                ExpendedJumps = false;
            }

            if (!PlayerMovementOperator.Grounded && 
            InputManager.GetButtonDown("up") && !ExpendedJumps &&
            CurrentJumps < ExtraJumps &&
            (WallJumpUpgrade == null || WallJumpUpgrade != null && !WallJumpUpgrade.OnWall))
            {
                CurrentJumps++;
                PlayerMovementOperator.ForceJump(PlayerMovementOperator.FramesSpentJumping / AdditionalJumpReduction);
                if (CurrentJumps == ExtraJumps)
                {
                    ExpendedJumps = true;
                }
            }

            if (this != null)
            {
                goto start;
            }
        }
    }
}
