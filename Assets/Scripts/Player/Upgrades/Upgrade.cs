// Copyright (c) 2019 Destin Hebner
using Worldline.GameState;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    /// <summary>
    /// An Upgrade is a stored as a ScriptableObject and can be given  to the 
    /// player using the GiveUpgrade method. Only one type of upgrade may be
    /// given at a time, and so Upgrades are stored in a static HashSet that
    /// is reset per each save file. Inheritors of the Upgrade class must
    /// define their own CreateAssetMenu attribute as well as any code that
    /// is specific to that Upgrade.
    /// </summary>
    [HasDefaultState]
    public abstract class Upgrade : SerializableScriptableObject
    {
        public bool RequiresParent;
        public string NameLocation;
        public string DescriptionLocation;
        public UpgradeType UpgradeType;
        public Sprite UpgradeIcon;
        public Upgrade InheritsFrom;
        public PlayerOperator PlayerOperator { get; set; }

        public virtual void OnDestroy()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        public virtual void OnLevelLoaded() { }

        public virtual void OnDisabled() { }

        /// <summary>
        /// Is called when the player is given a new Upgrade.
        /// </summary>
        public abstract void OnAdded();
        
        /// <summary>
        /// Is called when an upgrade is removed from the player, and also when
        /// the HashSet of upgrades is reset (when a save file is changed.)
        /// </summary>
        public abstract void OnRemoved();
    }
}
