﻿// Copyright (c) 2019 Destin Hebner
using System.IO;
using System.Collections.Generic;
using Worldline.GameState;
using ProtoBuf;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    [ProtoContract]
    public class UpgradeData : FileData
    {
        [ProtoMember(1)]
        public List<string> UpgradeNames { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Postload;
        public const string UpgradeAssetBundle = "player_upgrades.bun";

        public override FileData PopulateAndGetFileData()
        {
            UpgradeNames = new List<string>();
            foreach (Upgrade upgrade in MainPlayerOperator.MainPlayerComponent.Upgrades)
            {
                UpgradeNames.Add(upgrade.AssetPath);
            }

            return this;
        }

        public override void SetFileData()
        {
            AssetBundle upgrades = AssetBundle.LoadFromFile
            (
                Path.Combine(Application.streamingAssetsPath, UpgradeAssetBundle)
            );

            if (upgrades == null)
            {
                Debug.LogError("Failed to load Asset Bundle upgrades!");
                return;
            }

            if (UpgradeNames != null)
            {
                foreach (string name in UpgradeNames)
                {
                    Upgrade upgrade = upgrades.LoadAsset<Upgrade>(name);
                    if (upgrade == null)
                    {
                        Debug.LogError($"Failed to load Upgrade {name}!");
                        return;
                    }

                    MainPlayerOperator.MainPlayerComponent.GiveUpgrade(upgrade);
                }
            }

            upgrades.Unload(false);
        }
    }
}
