﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.Player.Upgrades
{
    [Flags]
    public enum DashDirection
    {
        None = 0,
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }
}
