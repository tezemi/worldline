﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Physics;
using MEC;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    /// <summary>
    /// This is an upgrade designed to simulate getting more jump height 
    /// from consecutive jumps like in the Super Mario games. If the player
    /// lands from a jump and then very quickly jumps again, they will gain
    /// more jump height, up to a certain number of maximum boosts.
    /// </summary>
    [CreateAssetMenu(fileName = "New Spring Boots Upgrade", menuName = "Data/Upgrades/Player/SpringBootsUpgrade")]
    public class SpringBootsUpgrade : Upgrade
    {
        [Tooltip("If this is true, then the player must be moving to get a boost.")]
        public bool RequireVelocity = true;
        [Tooltip("The maximum consecutive boosts the player can get before it resets.")]
        public int MaxBoostTimes = 2;
        [Tooltip("The amount of time after landing the player can get a  boost.")]
        public float BoostWindow = 0.25f;
        [Tooltip("Flat multiplier applies to all consecutive boosts. If this is equal to one, then the boost will be double the regular jump height.")]
        public float BoostMultiplier = 1f;
        /// <summary>
        /// The current number of boosts the player has received.
        /// </summary>
        public int CurrentBoosts { get; set; }
        protected PlayerMovementOperator PlayerMovementOperator { get; set; }
        protected Rigidbody2D PlayerRigidbody { get; set; }
        private bool _groundedLastFrame;
        private bool _readyToGiveBoost;
        private CoroutineHandle _wearOffGiveBoost;

        public override void OnAdded()
        {
            PlayerMovementOperator = PlayerMovementOperator.Main;
            PlayerRigidbody = PlayerMovementOperator.GetComponent<Rigidbody2D>();
            Timing.RunCoroutine(FixedUpdate(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        public override void OnRemoved()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> FixedUpdate()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (MainPlayerOperator.MainPlayerObject == null || 
            !MainPlayerOperator.MainPlayerObject.activeSelf) goto start;

            // If the player is grounded and they are ready for a boost and they press up...
            if (PlayerMovementOperator.Grounded && _readyToGiveBoost && InputManager.GetButtonDown("up"))
            {
                // If the player is moving or we don't care if they're moving...
                bool isMoving = Mathf.Abs(PlayerRigidbody.velocity.x) >= 2f;
                if (isMoving || !RequireVelocity)
                {
                    // Increase number of boosts and give them a boost
                    CurrentBoosts++;
                    PlayerMovementOperator.ForceJump
                    (
                        (int) (-PlayerMovementOperator.FramesSpentJumping * BoostMultiplier) * CurrentBoosts
                    );
                }
                else
                {
                    CurrentBoosts = 0; // Otherwise, reset boosts
                }
            }

            // If the player has reached their max amount of boosts...
            if (CurrentBoosts >= MaxBoostTimes)
            {
                CurrentBoosts = 0; // Reset boosts
            }

            if (PlayerMovementOperator.Grounded && !_groundedLastFrame) // Player just landed
            {
                _readyToGiveBoost = true; // They're ready for a boost
                _wearOffGiveBoost = 
                Timing.RunCoroutine(WearOff(), Segment.FixedUpdate, GetInstanceID().ToString());

                IEnumerator<float> WearOff()
                {
                    yield return Timing.WaitForSeconds(BoostWindow); // But if they don't jump soon...
                    _readyToGiveBoost = false; // Reset boosts and they don't get one
                    CurrentBoosts = 0; 
                }
            }

            if (!PlayerMovementOperator.Grounded) // If the player jumps at any point...
            {
                _readyToGiveBoost = false; // They are no longer ready for a boost (but they have gotten one)
                Timing.KillCoroutines(_wearOffGiveBoost); // No need to wear this off anymore
            }

            // Will allow use to check if the player changed between being on the ground and in the air
            _groundedLastFrame = PlayerMovementOperator.Grounded; 
            if (this != null)
            {
                goto start;
            }
        }
    }
}
