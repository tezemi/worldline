﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using Worldline.Level;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    [CreateAssetMenu(fileName = "New Dash Upgrade", menuName = "Data/Upgrades/Player/DashUpgrade")]
    public class DashUpgrade : Upgrade
    {
        [Tooltip("If true, the player will be able to dash vertically.")]
        public bool AllowVertical;
        [Tooltip("If true, the player will be able to dash diagonally.")]
        public bool AllowDiagonal;
        [Tooltip("If true, the player's velocity will not reset after dashing.")]
        public bool PreserveVelocity;
        [Tooltip("The horizontal speed of the dash.")]
        public float HorizontalDashSpeed = 15f;
        [Tooltip("The horizontal speed of the dash.")]
        public float VerticalDashSpeed = 10f;
        [Tooltip("How long a dash will last.")]
        public float DashTime = 0.35f;
        [Tooltip("The time it takes for a dash to actually start.")]
        public float DashDelay = 0.1f;
        [Tooltip("The time before the player can dash again.")]
        public float CooldownTime = 1.35f;
        [Tooltip("The sound that will play when the player can dash again.")]
        public AudioClip DashRecovered;
        [Tooltip("When the player is able to dash again, they emit this particle.")]
        public GameObject RecoveredParticle;
        /// <summary>
        /// Whether or not the player is currently dashing.
        /// </summary>
        public bool Dashing { get; protected set; }
        /// <summary>
        /// Whether or not the player is cooling down from a dash.
        /// </summary>
        public bool CoolingDown { get; protected set; }
        public DashDirection LastDashDirection { get; protected set; }
        protected Rigidbody2D Rigidbody { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected PlayerMovementOperator MovementOperator { get; set; }
        private float? _gravityBeforeDash;
        private TrailAnimation _trailAnimation;

        /// <summary>
        /// Whether or not the player has the ability to dash.
        /// </summary>
        public bool PlayerCanDash => !CoolingDown && !PlayerOperator.InImportantEvent && 
        !PlayerOperator.Hold && !PlayerOperator.DisableMovementInput && !TimeOperator.ShouldHold && 
        Time.timeScale != 0;

        public override void OnAdded()
        {
            TimeOperator = PlayerOperator.GetComponent<TimeOperator>();
            Rigidbody = PlayerOperator.GetComponent<Rigidbody2D>();
            SpriteRenderer = PlayerOperator.GetComponent<SpriteRenderer>();
            MovementOperator = PlayerOperator.GetComponent<PlayerMovementOperator>();
            Timing.RunCoroutine(Update(), Segment.Update, GetInstanceID().ToString());
        }

        public override void OnRemoved()
        {
            ResetDash();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }
        
        public void ResetDash()
        {
            Debug.Log("Resetting dash!");
            Timing.KillCoroutines($"DashUpgrade{GetInstanceID()}");
            Dashing = false;
            if (!ChangeLevel.IsActive)
            {
                PlayerOperator.OverrideSpeed = false;
                PlayerOperator.DisableMovementInput = false;
            }

            PlayerOperator.OverrideAnimations = false;
            MovementOperator.Hold = false;
            if (_gravityBeforeDash.HasValue)
            {
                Rigidbody.gravityScale = _gravityBeforeDash.Value;
            }

            CoolingDown = false;

            if (_trailAnimation != null)
            {
                Destroy(_trailAnimation);
            }
        }

        public override void OnLevelLoaded()
        {
            //ResetDash();
        }

        public override void OnDisabled()
        {
            ResetDash();
        }

        protected virtual IEnumerator<float> Update()
        {
            while (this != null)
            {
                yield return Timing.WaitUntilDone(() => PlayerOperator != null && PlayerOperator.isActiveAndEnabled);
                yield return Timing.WaitForOneFrame;
                bool pressAnyDirection = InputManager.GetButton("right") || InputManager.GetButton("left") || InputManager.GetButton("up") || InputManager.GetButton("down");
                if (PlayerCanDash && pressAnyDirection && InputManager.GetButton("dash"))
                {
                    DashDirection dashDirection = DashDirection.None;
                    if (InputManager.GetButton("right"))
                    {
                        dashDirection = dashDirection | (PlayerOperator is RecordingPlayerOperator ? DashDirection.Left : DashDirection.Right);
                    }

                    if (InputManager.GetButton("left"))
                    {
                        dashDirection = dashDirection | (PlayerOperator is RecordingPlayerOperator ? DashDirection.Right : DashDirection.Left);
                        if ((dashDirection & DashDirection.Right) != 0 && !(PlayerOperator is RecordingPlayerOperator))
                        {
                            dashDirection = dashDirection & ~DashDirection.Right;
                        }

                        if ((dashDirection & DashDirection.Left) != 0 && (PlayerOperator is RecordingPlayerOperator))
                        {
                            dashDirection = dashDirection & ~DashDirection.Left;
                        }
                    }

                    if (InputManager.GetButton("up"))
                    {
                        dashDirection = dashDirection | DashDirection.Up;
                    }

                    if (InputManager.GetButton("down"))
                    {
                        dashDirection = dashDirection | DashDirection.Down;
                        if ((dashDirection & DashDirection.Up) != 0)
                        {
                            dashDirection = dashDirection & ~DashDirection.Up;
                        }
                    }
                    
                    if (dashDirection != DashDirection.None)
                    {
                        Timing.RunCoroutine(Dash(dashDirection), Segment.FixedUpdate, $"DashUpgrade{GetInstanceID()}");
                    }
                }
            }
        }

        protected IEnumerator<float> Dash(DashDirection direction)
        {
            float x = 0f;
            LastDashDirection = direction;
            if ((direction & DashDirection.Right) != 0)
            {
                x = HorizontalDashSpeed;
            }
            else if ((direction & DashDirection.Left) != 0)
            {
                x = -HorizontalDashSpeed;
            }

            float y = 0f;
            if ((direction & DashDirection.Up) != 0 && AllowVertical)
            {
                y = VerticalDashSpeed;
            }
            else if ((direction & DashDirection.Down) != 0 && AllowVertical)
            {
                y = -VerticalDashSpeed;
            }

            if (Mathf.Abs(x) > 0f && Mathf.Abs(y) > 0f && !AllowDiagonal)
            {
                y = 0f;
            }

            if (Mathf.Abs(x) <= 0f && Mathf.Abs(y) <= 0f)
            {
                yield break;
            }

            Dashing = true;
            CoolingDown = true;
            PlayerOperator.OverrideSpeed = true;
            PlayerOperator.DisableMovementInput = true;
            PlayerOperator.OverrideAnimations = true;
            MovementOperator.Hold = true;
            MovementOperator.Speed = new Vector2(0f, 0f);
            if (TimeOperator.TimeScale > 1f) // If fast-forwarding, give physics boost.
            {
                x = TimeOperator.GetScaledValue(x);
                y = TimeOperator.GetScaledValue(y);
            }

            _gravityBeforeDash = Rigidbody.gravityScale;
            Rigidbody.gravityScale = 0f;
            Rigidbody.velocity = new Vector2(x, y);
            _trailAnimation = TrailAnimation.Create(PlayerOperator.gameObject);
            SetSpriteForDash();
            yield return PlayerOperator.WaitForFixedSecondsScaled(DashTime);
            Dashing = false;
            Rigidbody.velocity = PreserveVelocity ? Rigidbody.velocity : Rigidbody.velocity / 2f;
            if (!ChangeLevel.IsActive)
            {
                PlayerOperator.OverrideSpeed = false;
                PlayerOperator.DisableMovementInput = false;
            }

            PlayerOperator.OverrideAnimations = false;
            MovementOperator.Hold = false;
            if (_gravityBeforeDash.HasValue)
            {
                Rigidbody.gravityScale = _gravityBeforeDash.Value;
            }

            _gravityBeforeDash = null;
            CoroutineTimer cooldown = new CoroutineTimer(TimeOperator, () =>
            {
                if (DashRecovered != null)
                {
                    SoundEffect.PlaySound(DashRecovered, TimeOperator);
                }

                CoolingDown = false;
            });

            cooldown.StartTimer(CooldownTime, 1);
            if (_trailAnimation != null)
            {
                Destroy(_trailAnimation);
            }
        }

        private void SetSpriteForDash()
        {
            if (PlayerOperator.Crouch)
            {
                PlayerOperator.GetComponent<AnimationOperator>().enabled = true;
                bool hasWeapon = PlayerOperator.GetComponentInChildren<PlayerWeaponOperator>() != null && 
                    PlayerOperator.GetComponentInChildren<PlayerWeaponOperator>().isActiveAndEnabled;
                SpriteRenderer.sprite = hasWeapon ? PlayerOperator.PlayerSpriteBundle.NoArmsSlide[0] :
                    PlayerOperator.PlayerSpriteBundle.Slide[0];
                PlayerOperator.GetComponent<AnimationOperator>().Sprites = hasWeapon
                    ? PlayerOperator.PlayerSpriteBundle.NoArmsSlide
                    : PlayerOperator.PlayerSpriteBundle.Slide;
            }
        }
    }
}
