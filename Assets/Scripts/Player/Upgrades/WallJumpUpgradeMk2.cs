﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Physics;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Upgrades
{
    [CreateAssetMenu(fileName = "New Wall Jump Upgrade Mk 2", menuName = "Data/Upgrades/Player/WallJumpUpgradeMk2")]
    public class WallJumpUpgradeMk2 : WallJumpUpgrade
    {
        public override float WallClingForce { get; protected set; } = OverrideClingForce;
        public const float OverrideClingForce = 0.5f;

        protected override IEnumerator<float> Update()
        {
            start:
            yield return Timing.WaitForOneFrame;
            if (MainPlayerOperator.MainPlayerObject == null || !MainPlayerOperator.MainPlayerObject.activeSelf) goto start;

            PlayerMovementOperator op = MainPlayerOperator.MainPlayerObject.GetComponent<PlayerMovementOperator>();
            RaycastHit2D raycastRight = Physics2D.Raycast
            (
                PlayerOperator.transform.position,
                Vector2.right,
                RaycastDistance,
                op.GroundDetectionLayerMask
            );

            RaycastHit2D raycastLeft = Physics2D.Raycast
            (
                PlayerOperator.transform.position,
                Vector2.left,
                RaycastDistance,
                op.GroundDetectionLayerMask
            );

            if ((raycastLeft || raycastRight) && !op.Grounded)
            {
                if (raycastLeft && raycastLeft.transform.GetComponent<SolidTop>() != null ||
                raycastRight && raycastRight.transform.GetComponent<SolidTop>() != null) goto start;

                if (raycastLeft && raycastLeft.transform.GetComponent<DisableWallJump>() != null ||
                raycastRight && raycastRight.transform.GetComponent<DisableWallJump>() != null)
                {
                    OnWall = false;
                    goto start;
                }

                bool hasWeapon = PlayerWeaponOperator.Main.gameObject.activeSelf;
                if (raycastRight && InputManager.GetButton("right") && Rigidbody.velocity.y <= 0)
                {
                    OnWall = true;
                    OnRightWall = true;
                    PlayerOperator.Animate();
                    SpriteRenderer.sprite = hasWeapon ?
                        PlayerOperator.PlayerSpriteBundle.NoArmsWallCling[0] :
                        PlayerOperator.PlayerSpriteBundle.WallCling[0];

                    if (On && InputManager.GetButtonDown("up"))
                    {
                        On = false;
                        WallJumping = true;
                        op.ForceJump();
                        PlayerOperator.OverrideSpeed = true;
                        MovementOperator.Speed = new Vector2(-JumpHorizontalForce, 0);
                        Rigidbody.AddForce(new Vector2(-JumpHorizontalForce, 0), ForceMode2D.Impulse);
                        Timing.RunCoroutine(EnableMovementOpCooldown(), Segment.FixedUpdate);
                        Timing.RunCoroutine(EnableModuleCooldown(), Segment.FixedUpdate);
                    }
                }
                else if (raycastLeft && InputManager.GetButton("left") && Rigidbody.velocity.y <= 0)
                {
                    OnWall = true;
                    OnRightWall = false;
                    PlayerOperator.Animate();
                    SpriteRenderer.sprite = hasWeapon ?
                        PlayerOperator.PlayerSpriteBundle.NoArmsWallCling[0] : 
                        PlayerOperator.PlayerSpriteBundle.WallCling[0];

                    if (On && InputManager.GetButtonDown("up"))
                    {
                        On = false;
                        WallJumping = true;
                        op.ForceJump();
                        PlayerOperator.OverrideSpeed = true;
                        MovementOperator.Speed = new Vector2(JumpHorizontalForce, 0);
                        Rigidbody.AddForce(new Vector2(JumpHorizontalForce, 0), ForceMode2D.Impulse);
                        Timing.RunCoroutine(EnableMovementOpCooldown(), Segment.FixedUpdate);
                        Timing.RunCoroutine(EnableModuleCooldown(), Segment.FixedUpdate);
                    }
                }
                else
                {
                    OnWall = false;
                }

                IEnumerator<float> EnableMovementOpCooldown()
                {
                    yield return Timing.WaitForSeconds(LungeCooldown);
                    if (PlayerOperator.OverrideSpeed)
                    {
                        PlayerOperator.OverrideSpeed = false;
                        WallJumping = false;
                    }
                }

                IEnumerator<float> EnableModuleCooldown()
                {
                    yield return Timing.WaitForSeconds(JumpCooldown);
                    WallJumping = false;
                    On = true;
                }
            }
            else
            {
                OnWall = false;
            }

            if (OnWall)
            {
                if (InputManager.GetButton("down") && !InputManager.GetButton("up"))
                {
                    WallClingForce = -1f;
                }
                else
                {
                    WallClingForce = OverrideClingForce;
                }
            }

            if (this != null)
            {
                goto start;
            }
        }
    }
}
