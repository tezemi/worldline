﻿using UnityEngine;

namespace Worldline.Player
{
    [RequireComponent(typeof(SpriteMask))]
    public class EssenceOverlayMask : MonoBehaviour
    {
        protected SpriteMask SpriteMask { get; set; }
        protected SpriteRenderer ParentSpriteRenderer { get; set; }

        protected virtual void Awake()
        {
            SpriteMask = GetComponent<SpriteMask>();            
        }

        protected virtual void OnDisable()
        {
            ParentSpriteRenderer = null;
        }

        protected virtual void Update()
        {
            if (ParentSpriteRenderer == null)
            {
                ParentSpriteRenderer = GetComponentInParent<SpriteRenderer>();
                return;
            }

            SpriteMask.sprite = ParentSpriteRenderer.sprite;
            SpriteMask.transform.localScale = new Vector3(ParentSpriteRenderer.flipX ? -1f : 1f, ParentSpriteRenderer.flipY ? -1f : 1f, 1f);
            SpriteMask.transform.rotation = ParentSpriteRenderer.transform.rotation;
        }
    }
}
