﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Dual Pistols", menuName = "Data/Weapons/Dual Pistols")]
    public class DualPistols : BulletWeaponBase
    {
        public float Spread = 2f;

        public override GameObject FirePrimary()
        {
            GameObject bullet = base.FirePrimary();
            if (bullet == null)
            {
                return null;
            }

            bullet.transform.rotation = Quaternion.Euler
            (
                Operator.transform.rotation.eulerAngles.x,
                Operator.transform.rotation.eulerAngles.y,
                Operator.transform.rotation.eulerAngles.z + Random.Range(-Spread, Spread)
            );

            return bullet;
        }

        public override GameObject FireSecondary()
        {
            GameObject bullet = base.FirePrimary();
            if (bullet == null)
            {
                return null;
            }

            bullet.transform.rotation = Quaternion.Euler
            (
                Operator.transform.rotation.eulerAngles.x,
                Operator.transform.rotation.eulerAngles.y,
                Operator.transform.rotation.eulerAngles.z + Random.Range(-Spread, Spread)
            );

            return bullet;
        }
    }
}
