﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Null Sphere Launcher", menuName = "Data/Weapons/Null Sphere Launcher")]
    public class NullSphereLauncher : BulletWeaponBase
    {
        public AudioClip NullSphereFiredSound;
        public int Charge { get; set; } = 100;
        public int ChargeGainedPerShot { get; set; } = 4;
        public const int MaxCharge = 100;

        public void AddCharge()
        {
            if (Charge < MaxCharge)
            {
                Charge += ChargeGainedPerShot;
                if (Charge > MaxCharge)
                {
                    Charge = MaxCharge;
                }
            }
        }

        public override GameObject FireSecondary()
        {
            if (CoolingDownSecondary || Charge < MaxCharge) return null;

            Operator.Recoil();
            GameObject bullet = Instantiate(ProjectileSecondary);
            bullet.transform.position = new Vector3(Operator.transform.position.x, Operator.transform.position.y, bullet.transform.position.z)
            + Operator.transform.right * XOffset + (Operator.GetComponent<SpriteRenderer>().flipY ? -Operator.transform.up : Operator.transform.up) * YOffset;
            bullet.transform.rotation = Operator.transform.rotation;

            SoundEffect.PlaySound(NullSphereFiredSound, Operator.GetComponent<TimeOperator>());

            Charge = 0;
            Cooldown(false);

            return bullet;
        }
    }
}
