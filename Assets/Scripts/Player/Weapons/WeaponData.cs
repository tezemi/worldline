﻿// Copyright (c) 2019 Destin Hebner
using System.IO;
using System.Collections.Generic;
using Worldline.GameState;
using ProtoBuf;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [ProtoContract]
    public class WeaponData : FileData
    {
        [ProtoMember(1)]
        public int WeaponIndex { get; set; }
        [ProtoMember(2)]
        public List<string> WeaponNames { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Postload;
        public const string WeaponAssetBundles = "weapons.bun";

        public override FileData PopulateAndGetFileData()
        {
            WeaponIndex = PlayerWeaponOperator.Main.WeaponIndex;
            WeaponNames = new List<string>();
            foreach (Weapon weapon in PlayerWeaponOperator.Main.Weapons)
            {
                WeaponNames.Add(weapon.AssetPath);
            }

            return this;
        }

        public override void SetFileData()
        {
            AssetBundle weapons = AssetBundle.LoadFromFile
            (
                Path.Combine(Application.streamingAssetsPath, WeaponAssetBundles)
            );

            if (weapons == null)
            {
                Debug.LogError($"Failed to load Asset Bundle {WeaponAssetBundles}!");
                return;
            }

            if (WeaponNames != null)
            {
                foreach (string name in WeaponNames)
                {
                    Weapon weapon = weapons.LoadAsset<Weapon>(name);
                    if (weapon == null)
                    {
                        Debug.LogError($"Failed to load Weapon {name}!");
                        return;
                    }

                    PlayerWeaponOperator.Main.GiveWeapon(weapon);
                }

                PlayerWeaponOperator.Main.gameObject.SetActive(true);
            }

            weapons.Unload(false);
        }
    }
}
