﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Combat.Projectiles;
using MEC;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Shotgun", menuName = "Data/Weapons/Shotgun")]
    public class Shotgun : BulletWeaponBase
    {
        public int BulletsInClip = 6;
        public int BulletsPerFullShot = 6;
        public float Spread = 10f;
        public float TimeToLoadShell = 0.3f;
        public int BulletsLoaded { get; set; } = 6;
        public List<Projectile> Bullets { get; } = new List<Projectile>();
        private CoroutineHandle _reloadHandle;

        protected IEnumerator<float> Reload()
        {
            yield return Operator.WaitForFixedSecondsScaled(TimeToLoadShell);
            while (BulletsLoaded < BulletsInClip)
            {
                yield return Operator.WaitForFixedSecondsScaled(TimeToLoadShell);
                BulletsLoaded++;
            }
        }

        protected virtual GameObject FireRound(int amount)
        {
            GameObject lastBullet = null;
            for (int i = 0; i < amount; i++)
            {
                if (BulletsLoaded > 0)
                {
                    lastBullet = FireBullet(ProjectilePrimary);
                    lastBullet.transform.rotation = Quaternion.Euler
                    (
                        Operator.transform.rotation.eulerAngles.x,
                        Operator.transform.rotation.eulerAngles.y,
                        Operator.transform.rotation.eulerAngles.z + Spread * (Random.value - 0.5f)
                    );

                    Timing.KillCoroutines(_reloadHandle);
                    BulletsLoaded--;
                    _reloadHandle = Timing.RunCoroutine(Reload(), Segment.FixedUpdate);
                }
                else
                {
                    break;
                }
            }

            return lastBullet;
        }

        public override GameObject FirePrimary()
        {
            if (BulletsLoaded <= 0 || CoolingDownPrimary) return null;

            SoundEffect.PlaySound(PrimaryFireSound, Operator.GetComponent<TimeOperator>(), FireSoundVolume, FireSoundPitch);
            GameObject lastBullet = FireRound(BulletsPerFullShot / 2);

            return lastBullet;
        }

        public override GameObject FireSecondary()
        {
            if (BulletsLoaded <= 0 || CoolingDownSecondary) return null;

            SoundEffect.PlaySound(PrimaryFireSound, Operator.GetComponent<TimeOperator>(), FireSoundVolume, FireSoundPitch);
            GameObject lastBullet = FireRound(BulletsPerFullShot);

            return lastBullet;
        }
    }
}
