// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Pistol", menuName = "Data/Weapons/Pistol")]
    public class Pistol : BulletWeaponBase
    {
        public override GameObject FireSecondary()
        {
            return null;
        }
    }
}
