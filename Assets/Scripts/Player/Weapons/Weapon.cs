// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    public abstract class Weapon : SerializableScriptableObject
    {
        public bool PrimaryAutomatic;
        public bool SecondaryAutomatic;
        public float Recoil = 0.05f;
        public float FireDelayPrimary;
        public float FireDelaySecondary;
        public string NameLocation;
        public string DescriptionLocation;
        public Vector3 RightOffset;
        public Vector3 LeftOffset;
        public Vector3 FrontArmRightOffset;
        public Vector3 FrontArmLeftOffset;
        public Vector3 BackRightOffset;
        public Vector3 BackLeftOffset;
        public WeaponSpriteBundle WeaponSpriteBundle;
        public AudioClip PrimaryFireSound;
        public GameObject ProjectilePrimary;
        public GameObject ProjectileSecondary;
        public bool CoolingDownPrimary { get; set; }
        public bool CoolingDownSecondary { get; set; }
        public PlayerWeaponOperator Operator { get; set; }

        protected virtual void Cooldown(bool primary = true)
        {
            if (primary)
            {
                CoolingDownPrimary = true;
            }
            else
            {
                CoolingDownSecondary = true;
            }

            Timing.RunCoroutine(DoCooldown(primary), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> DoCooldown(bool doPrimaryCooldown)
        {
            if (doPrimaryCooldown)
            {
                yield return Operator.WaitForFixedSecondsScaled(FireDelayPrimary);
                CoolingDownPrimary = false;
            }
            else
            {
                yield return Operator.WaitForFixedSecondsScaled(FireDelaySecondary);
                CoolingDownSecondary = false;
            }
        }

        public virtual void Deploy()
        {
            // ...
        }
        
        public abstract GameObject FirePrimary();
        public abstract GameObject FireSecondary();
    }
}
 