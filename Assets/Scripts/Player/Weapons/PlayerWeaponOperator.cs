// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.TimeEngine;
using Worldline.Level.Obstacles;
using Worldline.Player.Upgrades;
using Worldline.NPCs.Enemies.Tears;
using Worldline.Combat.Projectiles;
using Worldline.TimeEngine.Recording;
using TeamUtility.IO;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    public class PlayerWeaponOperator : WeaponOperator
    {
        public static PlayerWeaponOperator Main;
        public Sprite CrosshairSprite;
        /// <summary>
        /// If true, the weapon will be restricted to a forward-facing position 
        /// when wall jumping.
        /// </summary>
        public bool RestrictWallJump { get; set; } = true;
        public Weapon CurrentWeapon { get; protected set; }
        public GameObject CrosshairGameObject { get; protected set; }
        public List<Weapon> Weapons { get; protected set; } = new List<Weapon>();
        public const float CrosshairZPosition = -8f;
        public const float DuckingVerticalDifference = -0.40f;
        protected PlayerOperator PlayerOperator { get; set; }
        private bool _firePrimary;
        private bool _fireSecondary;
        private int _weaponIndex;

        /// <summary>
        /// TODO: Color palette changes.
        /// </summary>
        public readonly Dictionary<Type, Color> CrosshairColors = new Dictionary<Type, Color>
        {
            { typeof(Pistol), new Color(0.957f, 0.977f, .102f, 1f) },
            { typeof(MachineGun), new Color(0.369f, 0.957f, 1f, 1f) },
            { typeof(Shotgun), new Color(0.367f, 1f, 0.38f, 1f) },
            { typeof(NullSphereLauncher), new Color(0.624f, 0.039f, 0.839f, 1f) },
            { typeof(BurningSun), new Color(0.95f, 0.356f, 0.139f, 1f) }
        };

        public override bool Disabled
        {
            get => base.Disabled;
            set
            {

                if (isActiveAndEnabled && Weapons.Count > 0)
                {
                    CrosshairGameObject.SetActive(!value);
                }

                base.Disabled = value;
            }
        }

        /// <summary>
        /// The current index of the deployed weapon within the list of weapons. 
        /// Use this to change the active weapon.
        /// </summary>
        public int WeaponIndex
        {
            get => _weaponIndex;
            set
            {
                if (Weapons.Count <= 0)
                {
                    return;
                }

                int totalLoops = 0;
                int inca = value > _weaponIndex ? 1 : -1;
                for (int i = value; ; i += inca)
                {
                    if (i >= Weapons.Count)
                    {
                        i = 0;
                    }

                    if (i < 0)
                    {
                        i = Weapons.Count - 1;
                    }

                    if (Weapons[i] != null)
                    {
                        value = i;
                        break;
                    }

                    totalLoops++; 
                    if (totalLoops > 100)
                    {
                        break; // I did something wrong, this prevents the game from crashing.
                    }
                }

                CurrentWeapon = Weapons[value];
                SetWeaponSprites();
                CurrentWeapon.Operator = this;
                CurrentWeapon.Deploy();
                _weaponIndex = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            if (Main == null)
            {
                Main = this;
            }

            // Creates the crosshair, assuming this is the main WeaponOperator
            if (this == Main) 
            {
                CrosshairGameObject = new GameObject("Weapon Operator Crosshair", typeof(SpriteRenderer));
                CrosshairGameObject.GetComponent<SpriteRenderer>().sprite = CrosshairSprite;
                DontDestroyOnLoad(CrosshairGameObject);
                CrosshairGameObject.DestroyOnSceneLoad();
                if (!isActiveAndEnabled)
                {
                    CrosshairGameObject.SetActive(false);
                }
            }

            PlayerOperator = GetComponentInParent<PlayerOperator>();
        }

        protected virtual void OnEnable()
        {
            if (CrosshairGameObject != null && !Disabled && isActiveAndEnabled)
            {
                CrosshairGameObject.SetActive(true);
            }

            if (Main == this)
            {
                if (CheatController.IsActive(Cheat.DualPistols) && !HasWeapon<DualPistols>())
                {
                    GiveWeapon(DebugUtils.Main.DualPistols);
                }

                if (CheatController.IsActive(Cheat.AllWeaponsAndUpgrades) && !HasWeapon<Pistol>())
                {
                    DebugUtils.Main.GiveAllWeaponsAndUpgrades();
                }
            }
        }

        protected virtual void OnDisable()
        {
            if (CrosshairGameObject != null)
            {
                CrosshairGameObject.SetActive(false);
            }
        }

        protected override void Update()
        {
            base.Update();
            if (Disabled)
            {
                return;
            }

            // Set crosshair position and color
            if (CrosshairGameObject != null)
            {
                CrosshairGameObject.transform.rotation = transform.rotation;
                CrosshairGameObject.transform.position = new Vector3
                (
                    CameraOperator.ActiveCamera.CameraComponent.ScreenToWorldPoint(Input.mousePosition).x,
                    CameraOperator.ActiveCamera.CameraComponent.ScreenToWorldPoint(Input.mousePosition).y,
                    CrosshairZPosition
                );

                if (CurrentWeapon != null && CrosshairColors.ContainsKey(CurrentWeapon.GetType()))
                {
                    CrosshairGameObject.GetComponent<SpriteRenderer>().color = CrosshairColors[CurrentWeapon.GetType()];
                }
                else
                {
                    CrosshairGameObject.GetComponent<SpriteRenderer>().color = Color.white;
                }
            }

            // Stop if game is paused, or time effects, or holding player
            if (Disabled || LevelController.Main.Paused ||
            WeaponOperatorTimeOperator.ShouldHold || Recorder.Mode == RecorderMode.Playback)
            {
                return;
            }

            // Disable this if there is no weapon
            if (CurrentWeapon == null)
            {
                gameObject.SetActive(false);
                return;
            }

            // Aim and positions sprites
            Vector3 mousePosition =
            CameraOperator.ActiveCamera.GetComponent<Camera>().ScreenToWorldPoint(InputManager.mousePosition);
            Vector3 vectorToTarget = mousePosition - transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            if (RestrictWallJump)
            {
                q = RestrictWallJumpAngles(q);
            }

            // Enable/disable sprites if player isn't/is on wall.
            bool onWall = false;
            if (PlayerOperator.HasUpgrade<WallJumpUpgrade>())
            {
                onWall = PlayerOperator.GetUpgrade<WallJumpUpgrade>().OnWall;
                if (onWall)
                {
                    SpriteRenderer.enabled = false;
                    BackArmSpriteGameObject.GetComponent<SpriteRenderer>().enabled = false;
                }
                else
                {
                    SpriteRenderer.enabled = true;
                    BackArmSpriteGameObject.GetComponent<SpriteRenderer>().enabled = true;
                }
            }

            const float onWallZ = 0.15f;
            Position
            (
                q,
                CurrentWeapon.FrontArmLeftOffset,
                CurrentWeapon.FrontArmRightOffset,
                !onWall ? CurrentWeapon.LeftOffset : new Vector3(CurrentWeapon.LeftOffset.x, CurrentWeapon.LeftOffset.y, onWallZ),
                !onWall ? CurrentWeapon.RightOffset : new Vector3(CurrentWeapon.RightOffset.x, CurrentWeapon.RightOffset.y, onWallZ),
                CurrentWeapon.BackLeftOffset,
                CurrentWeapon.BackRightOffset,
                CurrentWeapon.Recoil
            );

            if (PlayerOperator.Crouching)
            {
                transform.localPosition += new Vector3(0f, DuckingVerticalDifference, 0f);
            }

            if (CurrentWeapon.PrimaryAutomatic)
            {
                if (InputManager.GetButton("primary"))
                {
                    _firePrimary = true;
                }
            }
            else
            {
                if (InputManager.GetButtonDown("primary"))
                {
                    _firePrimary = true;
                }
            }

            if (CurrentWeapon.SecondaryAutomatic)
            {
                if (InputManager.GetButton("secondary"))
                {
                    _fireSecondary = true;
                }
            }
            else
            {
                if (InputManager.GetButtonDown("secondary"))
                {
                    _fireSecondary = true;
                }
            }

            if (InputManager.GetButtonDown("switch_left"))
            {
                WeaponIndex--;
            }

            if (InputManager.GetButtonDown("switch_right"))
            {
                WeaponIndex++;
            }
        }

        protected virtual void FixedUpdate()
        {
            GameObject projectile = null;
            if (_firePrimary || Recorder.CheckTrigger("_firePrimary") && GetComponentInParent<PlayerTimeOperator>().IsClone)
            {
                projectile = CurrentWeapon.FirePrimary();
                FireProjectile(projectile, CurrentWeapon);
                CheckIsPlayback();
                Recorder.RecordTrigger("_firePrimary");
                _firePrimary = false;
            }
            else if (_fireSecondary || Recorder.CheckTrigger("_fireSecondary") && GetComponentInParent<PlayerTimeOperator>().IsClone)
            {
                projectile = CurrentWeapon.FireSecondary();
                FireProjectile(projectile, CurrentWeapon);
                CheckIsPlayback();
                Recorder.RecordTrigger("_fireSecondary");
                _fireSecondary = false;
            }

            if (projectile != null && !GetComponentInParent<PlayerTimeOperator>().IsClone)
            {
                foreach (MimiturgeWeaponOperator m in MimiturgeWeaponOperator.MimiturgeWeaponOperators)
                {
                    if (m == null || !m.isActiveAndEnabled) continue;

                    m.FireMimiturgeProjectile();
                }
            }

            void CheckIsPlayback()
            {
                if (projectile != null && GetComponentInParent<PlayerTimeOperator>().IsResetClone && GetComponent<Recorder>().Mode == RecorderMode.Playback)
                {
                    bool isInsideAnyMask = false;
                    foreach (EssenceBlob blob in EssenceBlob.AllEssenceBlobs)
                    {
                        foreach (SpriteMask mask in blob.AuraMasks)
                        {
                            if (mask.bounds.ContainsVector2(projectile.transform.position))
                            {
                                isInsideAnyMask = true;
                            }
                        }
                    }

                    if (!isInsideAnyMask)
                    {
                        DeathTimeOperator.Handle(projectile, projectile.GetComponent<Projectile>().Alignment, false);
                        projectile.SetActive(false);
                        Pool.Add(projectile, projectile.GetComponent<Projectile>().DeathPoolName, false);
                    }
                }
            }
        }
        
        private Quaternion RestrictWallJumpAngles(Quaternion q)
        {
            if (PlayerOperator.GetUpgrade<WallJumpUpgrade>() == null || !PlayerOperator.GetUpgrade<WallJumpUpgrade>().OnWall)
            {
                return q;
            }

            if (PlayerOperator.GetUpgrade<WallJumpUpgrade>().OnRightWall)
            {
                if (q.eulerAngles.z < 90f)
                {
                    return Quaternion.Euler(q.x, q.y, 90f);
                }
                
                if (q.eulerAngles.z > 270f)
                {
                    return Quaternion.Euler(q.x, q.y, 270f);
                }
            }
            else
            {
                if (q.eulerAngles.z < 90f || q.eulerAngles.z > 270f)
                {
                    return q;
                }
                
                if (q.eulerAngles.z < 180f)
                {
                    return Quaternion.Euler(q.x, q.y, 90f);
                }

                return Quaternion.Euler(q.x, q.y, 270f);
            }

            return q;
        }

        public override void FireProjectile(GameObject projectile, Weapon originWeapon)
        {
            if (DisableFiring) return;

            base.FireProjectile(projectile, originWeapon);
            if (GetComponentInParent<PlayerTimeOperator>().IsClone && projectile != null)
            {
                projectile.GetComponent<SpriteRenderer>().color = SpriteRenderer.color;
            }
        }

        public void SetWeaponSprites()
        {
            // Player's sprites have changed...
            if (CurrentWeapon.WeaponSpriteBundle == null || CurrentWeapon.WeaponSpriteBundle.PlayerSpriteBundle != PlayerOperator.PlayerSpriteBundle)
            {
                foreach (WeaponSpriteBundle spriteBundle in LevelController.Main.WeaponSpriteBundles)
                {
                    if (spriteBundle.PlayerSpriteBundle.GetType() == PlayerOperator.PlayerSpriteBundle.GetType() &&
                    spriteBundle.Weapon.GetType() == CurrentWeapon.GetType())
                    {
                        CurrentWeapon.WeaponSpriteBundle = spriteBundle;
                        break;
                    }
                }
            }

            if (CurrentWeapon.WeaponSpriteBundle == null)
            {
                CurrentWeapon.WeaponSpriteBundle = LevelController.Main.WeaponSpriteBundles[0];
            }

            SpriteRenderer.sprite = CurrentWeapon.WeaponSpriteBundle != null ? CurrentWeapon.WeaponSpriteBundle.FrontArmSprite : null;
            WeaponSpriteGameObject.GetComponent<SpriteRenderer>().sprite = CurrentWeapon.WeaponSpriteBundle != null ? CurrentWeapon.WeaponSpriteBundle.MainSprite : null;
            if (CurrentWeapon.WeaponSpriteBundle != null && CurrentWeapon.WeaponSpriteBundle.BackArmSprite != null)
            {
                BackArmSpriteGameObject.SetActive(true);
                BackArmSpriteGameObject.GetComponent<SpriteRenderer>().sprite = CurrentWeapon.WeaponSpriteBundle.BackArmSprite;
            }
            else
            {
                BackArmSpriteGameObject.GetComponent<SpriteRenderer>().sprite = null;
            }
        }

        public void GiveWeapon(Weapon weapon)
        {
            Weapon copy = Instantiate(weapon);

            if (Weapons.Any(c => c.GetType() == copy.GetType())) return;

            Weapons.Add(copy);
            WeaponIndex = Weapons.Count - 1;
            CurrentWeapon.Operator = this;
        }

        public void RemoveWeapon(Weapon weapon)
        {
            Weapons.Remove(weapon);
            Destroy(weapon);
        }

        public void RemoveWeapon<T>() where T : Weapon
        {
            Weapon weapon = Weapons.SingleOrDefault(u => u.GetType() == typeof(T));
            if (weapon != null)
            {
                Weapons.Remove(weapon);
                Destroy(weapon);
            }
        }

        public void SyncWeapons(PlayerWeaponOperator otherWeaponOperator)
        {
            foreach (Weapon weapon in Weapons)
            {
                Destroy(weapon);
            }

            Weapons.Clear();
            foreach (Weapon weapon in otherWeaponOperator.Weapons)
            {
                GiveWeapon(weapon);
            }
        }

        public T GetWeapon<T>() where T : Weapon
        {
            return (T)Weapons.FirstOrDefault(u => u.GetType() == typeof(T));
        }

        public bool HasWeapon<T>() where T : Weapon
        {
            return Weapons.OfType<T>().Any();
        }
    }
}
