﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Combat.Projectiles;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    public abstract class BulletWeaponBase : Weapon
    {
        public float XOffset = 0.85f;
        public float YOffset = 0.2f;
        public string PoolName;
        public float FireSoundVolume { get; protected set; } = 0.4f;
        public float FireSoundPitch { get; protected set; } = 1f;

        protected virtual GameObject FireBullet(GameObject bulletPrefab)
        {
            Operator.Recoil();
            GameObject bulletInstance;
            if (Pool.Has(PoolName))
            {
                bulletInstance = Pool.Get(PoolName);
            }
            else
            {
                bulletInstance = Instantiate(bulletPrefab);
            }

            bulletInstance.SetActive(true);
            bulletInstance.transform.position =
            Operator.transform.position + Operator.transform.right * XOffset +
            (Operator.GetComponent<SpriteRenderer>().flipY ? -Operator.transform.up : Operator.transform.up) * YOffset;
            bulletInstance.transform.rotation = Operator.transform.rotation;
            Projectile projectile = bulletInstance.GetComponent<Projectile>();
            if (projectile != null)
            {
                // If the player's gun is past a wall
                Vector3 endPoint = bulletInstance.transform.position + new Vector3
                (
                    bulletInstance.GetComponent<BoxCollider2D>().bounds.size.x,
                    0f,
                    0f
                );

                RaycastHit2D[] rays = Physics2D.LinecastAll
                (
                    Operator.transform.parent.position, 
                    endPoint, 
                    Operator.GetComponentInParent<MovementOperator>().GroundDetectionLayerMask
                );

                foreach (RaycastHit2D ray in rays)
                {
                    if (ray)
                    {
                        Collider2D collision = ray.collider;
                        if (!collision.isTrigger)
                        {
                            bulletInstance.transform.position = ray.point;
                            projectile.Die();
                            break;
                        }
                    }
                }
            }            

            return bulletInstance;
        }

        public override GameObject FirePrimary()
        {
            if (CoolingDownPrimary) return null;

            if (!Operator.GetComponentInParent<PlayerTimeOperator>().IsResetClone)
            {
                SoundEffect.PlaySound(PrimaryFireSound, Operator.GetComponent<TimeOperator>(), FireSoundVolume,FireSoundPitch);
            }

            GameObject bullet = FireBullet(ProjectilePrimary);
            Cooldown();

            return bullet;
        }

        public override GameObject FireSecondary()
        {
            if (CoolingDownSecondary) return null;

            if (!Operator.GetComponentInParent<PlayerTimeOperator>().ResetClone)
            {
                SoundEffect.PlaySound(PrimaryFireSound, Operator.GetComponent<TimeOperator>(), FireSoundVolume, FireSoundPitch);
            }

            GameObject bullet = FireBullet(ProjectileSecondary);
            Cooldown();

            return bullet;
        }
    }
}
