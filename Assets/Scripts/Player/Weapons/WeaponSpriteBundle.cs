﻿using Worldline.Utilities;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Weapon Sprite Bundle", menuName = "Data/Weapons/Weapon Sprite Bundle")]
    public class WeaponSpriteBundle : SerializableScriptableObject
    {
        public Sprite MainSprite;
        public Sprite FrontArmSprite;
        public Sprite BackArmSprite;
        public Sprite HudSprite;
        public Sprite DisabledSprite;
        public Weapon Weapon;
        public PlayerSpriteBundle PlayerSpriteBundle;
    }
}
