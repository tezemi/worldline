// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Machine Gun", menuName = "Data/Weapons/Machine Gun")]
    public class MachineGun : BulletWeaponBase
    {
        public float Spread = 4f;
        public float CooldownRate = 0.1f;
        public float CooldownAmount = 1f;
        public float OverheatRate = 1.5f;
        public float OverheatedTime = 3f;
        public bool Overheated { get; set; }
        public float Overheat { get; set; }
        public Timer CoolDownTimer { get; protected set; }
        public Timer OverheatedTimer { get; protected set; }
        public const int OverheatCap = 25;
        
        public override GameObject FirePrimary()
        {
            if (CoolingDownPrimary || Overheated) return null;

            Overheat += Operator.GetComponent<TimeOperator>().GetScaledValue(OverheatRate);
            if (CoolDownTimer == null)
            {
                CoolDownTimer = Timer.CreateNewTimer(Operator.gameObject, CooldownRate, 0);
                CoolDownTimer.EffectedByTimescale = true;
                CoolDownTimer.Elapsed = () =>
                {
                    Overheat -= Operator.GetComponent<TimeOperator>().GetScaledValue(CooldownAmount);
                    if (Overheat <= 0f)
                    {
                        Overheat = 0f;
                        Destroy(CoolDownTimer);
                    }
                };
            }
            else
            {
                CoolDownTimer.ActualTime = 0f;
            }

            if (Overheat >= OverheatCap)
            {
                Timer overheatTimer = Timer.CreateNewTimer(Operator.gameObject, OverheatedTime, 1);
                overheatTimer.EffectedByTimescale = true;
                overheatTimer.Elapsed = () =>
                {
                    Overheat = OverheatCap - 9f;
                    Overheated = false;
                    CoolDownTimer.ActualTime = 0f;
                    CoolDownTimer.enabled = true;
                };

                CoolDownTimer.enabled = false;
                Overheated = true;

                return null;
            }

            FireSoundPitch = MathUtilities.GetRange(Overheat, 0f, OverheatCap, 1.5f, 1f);
            GameObject bullet = base.FirePrimary();
            bullet.transform.rotation = Quaternion.Euler
            (
                Operator.transform.rotation.eulerAngles.x,
                Operator.transform.rotation.eulerAngles.y,
                Operator.transform.rotation.eulerAngles.z + Random.Range(-Spread, Spread) + Random.Range(-Overheat, Overheat)
            );

            return bullet;
        }

        public override GameObject FireSecondary()
        {
            return null;
        }
    }
}
