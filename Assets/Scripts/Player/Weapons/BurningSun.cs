﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.Player.Weapons
{
    [CreateAssetMenu(fileName = "New Burning Sun", menuName = "Data/Weapons/Burning Sun")]
    public class BurningSun : BulletWeaponBase
    {
        public float CooldownTimeNext { get; set; } = CooldownTimeBase;
        public Timer CooldownCooldownTimer { get; set; }
        public const float CooldownTimeBase = 0.15f;
        public const float CooldownIncaPerShot = 0.25f;
        public const float CooldownResetTime = 3.5f;
        
        public override void Deploy()
        {
            if (CoolingDownPrimary)
            {
                Operator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().sprite = WeaponSpriteBundle.DisabledSprite;
            }
            else
            {
                Operator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().sprite = WeaponSpriteBundle.MainSprite;
            }
        }

        public override GameObject FirePrimary()
        {
            FireSoundPitch = 0.25f;
            GameObject bullet = base.FirePrimary();
            if (bullet != null)
            {
                Operator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().sprite = WeaponSpriteBundle.DisabledSprite;
                CooldownTimeNext += CooldownIncaPerShot;
            }

            return bullet;
        }

        public override GameObject FireSecondary()
        {
            return null;
        }

        protected override IEnumerator<float> DoCooldown(bool primary)
        {
            yield return Operator.WaitForFixedSecondsScaled(primary ? CooldownTimeNext : FireDelaySecondary);
            if (primary)
            {
                CoolingDownPrimary = false;
                if (Operator.CurrentWeapon is BurningSun)
                {
                    Operator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().sprite = WeaponSpriteBundle.MainSprite;
                    Timing.RunCoroutine(Shake(0.15f), Segment.FixedUpdate);
                }

                if (CooldownCooldownTimer == null)
                {
                    CooldownCooldownTimer = Timer.CreateNewTimer(Operator.gameObject, CooldownResetTime, 1, true, false);
                    CooldownCooldownTimer.Important = true;
                    CooldownCooldownTimer.EffectedByTimescale = true;
                    CooldownCooldownTimer.Elapsed = () =>
                    {
                        CooldownTimeNext = CooldownTimeBase;
                    };
                }
                else
                {
                    CooldownCooldownTimer.StartTimer();
                }
            }
            else
            {
                CoolingDownSecondary = false;
            }
        }

        protected IEnumerator<float> Shake(float shakeIntensity)
        {
            Operator.SetWeaponPosition = false;
            Vector3 orgPos = Operator.WeaponSpriteGameObject.transform.localPosition;
            const float shakeIntensityDeca = 0.02f;
            bool up = true;

            start:
            yield return Operator.WaitForFixedSecondsScaled(Time.fixedDeltaTime);
            Operator.WeaponSpriteGameObject.transform.localPosition += 
                new Vector3(0f, up ? shakeIntensity : -shakeIntensity, 0f);

            up = !up;
            shakeIntensity -= shakeIntensityDeca;

            if (shakeIntensity > 0f)
            {
                goto start;
            }

            Operator.transform.localPosition = orgPos;
            Operator.SetWeaponPosition = true;
        }
    }
}
