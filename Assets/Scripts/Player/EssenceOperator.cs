// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Visuals;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using Worldline.Level;

namespace Worldline.Player
{
    /// <summary>
    /// This is a singleton component that is attached to the main player
    /// Game Object, and will only be enabled while on the main player. It
    /// is responsible for handling all of the player's essence and essence
    /// related abilities. This includes keeping track of the player's essence,
    /// essence caps, durations, and use amounts, as well as activating and
    /// deactivating effects, draining essence, the times and entities effected
    /// of certain effects, ect.
    /// </summary>
    [DisableOnPlayback]
    public class EssenceOperator : MonoBehaviour
    {
        public static EssenceOperator Main;
        public bool DebugMe;
        public AudioClip EssenceEffectStart;
        public AudioClip EssenceEffectEnd;
        public AudioClip[] Sounds;
        public SoundEffect CurrentSoundEffect { get; set; }
        public int EssenceSelection { get; protected set; }
        public TimeEffect CurrentEffectActive { get; protected set; } = TimeEffect.None;
        public List<GameObject> EntitiesEffected { get; protected set; } = new List<GameObject>();
        public const float OverlayAlphaMax = 0.35f;
        public const float OverlayFadeSpeed = 0.01f;
        public const string SpriteMaskPool = "EssenceOperatorSpriteMaskPool";
        private bool _applicationQuitting;
        private float _overlayAlpha;

        /// <summary>
        /// The total amount of essence. Won't go over the cap, which is stored 
        /// in MaxEssences.
        /// </summary>
        public Dictionary<TimeEffect, int> EssenceAmounts { get; set; } = new Dictionary<TimeEffect, int>
        {
            {TimeEffect.Pause, 0},
            {TimeEffect.Slow, 0},
            {TimeEffect.Rewind, 0},
            {TimeEffect.FastForward, 0},
            {TimeEffect.Record, 0},
            {TimeEffect.Reset, 0}
        };

        /// <summary>
        /// The amount of essence drained each tick while and effect is active.
        /// </summary>
        public Dictionary<TimeEffect, int> UseAmounts { get; set; } = new Dictionary<TimeEffect, int>
        {
            {TimeEffect.Pause, 10},
            {TimeEffect.Slow, 10},
            {TimeEffect.Rewind, 10},
            {TimeEffect.FastForward, 10},
            {TimeEffect.Record, 10},
            {TimeEffect.Reset, 10}
        };

        /// <summary>
        /// The cap for each amount of essence.
        /// </summary>
        public Dictionary<TimeEffect, int> MaxEssences { get; set; } = new Dictionary<TimeEffect, int>
        {
            {TimeEffect.Pause, 100},
            {TimeEffect.Slow, 100},
            {TimeEffect.Rewind, 100},
            {TimeEffect.FastForward, 100},
            {TimeEffect.Record, 100},
            {TimeEffect.Reset, 100}
        };

        /// <summary>
        /// The amount of time each tick lasts while draining for each type of 
        /// essence.
        /// </summary>
        public Dictionary<TimeEffect, float> Durations { get; set; } = new Dictionary<TimeEffect, float>
        {
            {TimeEffect.Pause, 1f},
            {TimeEffect.Slow, 1f},
            {TimeEffect.Rewind, 1f},
            {TimeEffect.FastForward, 1f},
            {TimeEffect.Record, 1f},
            {TimeEffect.Reset, 1f}
        };
        
        public bool AnyEffectIsActive => WorldEffectIsActive || PlayerEffectIsActive;

        public bool WorldEffectIsActive => CurrentEffectActive == TimeEffect.Pause ||
                                           CurrentEffectActive == TimeEffect.Slow ||
                                           CurrentEffectActive == TimeEffect.Rewind;

        public bool PlayerEffectIsActive => CurrentEffectActive == TimeEffect.FastForward ||
                                            CurrentEffectActive == TimeEffect.Record ||
                                            CurrentEffectActive == TimeEffect.Reset;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void OnEnable()
        {
            if (CheatController.IsActive(Cheat.InfininteEssence) || Debug.isDebugBuild && Input.GetKeyDown(KeyCode.Alpha0))
            {
                DebugMe = true;
            }
        }

        protected virtual void OnDisable()
        {
            if (_applicationQuitting) return;

            // It doesn't really make sense to change levels and still have record
            // be active, as the clone would not exist anymore.
            if (CurrentEffectActive == TimeEffect.Record)
            {
                EndEffect();
            }
        }

        protected virtual void Update()
        {
            if (DebugMe)
            {
                EssenceAmounts[TimeEffect.Pause] = 100;
                EssenceAmounts[TimeEffect.Slow] = 100;
                EssenceAmounts[TimeEffect.Rewind] = 100;
                EssenceAmounts[TimeEffect.FastForward] = 100;
                EssenceAmounts[TimeEffect.Record] = 100;
                EssenceAmounts[TimeEffect.Reset] = 100;
            }

            // Prevent essence amounts from being lower than 0.
            foreach (TimeEffect essenceAmount in EssenceAmounts.Keys)
            {
                if (EssenceAmounts[essenceAmount] < 0)
                {
                    EssenceAmounts[essenceAmount] = 0;
                }
            }

            if (LevelController.Main.Paused) return;

            // If any essence effect hot-key has been pressed, or the activate button has been
            // pressed this will not be -1, and will contain that index.
            int indexTimePowerSelected = -1;
            if (InputManager.GetButtonDown("activate_essence"))
            {
                indexTimePowerSelected = EssenceSelection;
            }

            if (InputManager.GetButtonDown("switch_essence_left") && EssenceSelection > 0)
            {
                EssenceSelection--;
            }

            if (InputManager.GetButtonDown("switch_essence_right") && EssenceSelection < 5)
            {
                EssenceSelection++;
            }

            // Keyboard hot-keys.
            if (InputManager.GetButtonDown("time_power_1") && CurrentEffectActive != TimeEffect.Record)
                indexTimePowerSelected = 0;
            if (InputManager.GetButtonDown("time_power_2"))
                indexTimePowerSelected = 1;
            if (InputManager.GetButtonDown("time_power_3"))
                indexTimePowerSelected = 2;
            if (InputManager.GetButtonDown("time_power_4"))
                indexTimePowerSelected = 3;
            if (InputManager.GetButtonDown("time_power_5"))
                indexTimePowerSelected = 4;
            if (InputManager.GetButtonDown("time_power_6"))
                indexTimePowerSelected = 5;

            if (!AnyEffectIsActive)
            {
                if (!MainPlayerOperator.MainPlayerComponent.InImportantEvent)
                {
                    if (indexTimePowerSelected == 0 && EssenceAmounts[TimeEffect.Pause] > 0)
                    {
                        ApplyWorldEffect(TimeEffect.Pause);
                    }
                    else if (indexTimePowerSelected == 1 && EssenceAmounts[TimeEffect.Slow] > 0)
                    {
                        ApplyWorldEffect(TimeEffect.Slow);
                    }
                    else if (indexTimePowerSelected == 2 && EssenceAmounts[TimeEffect.Rewind] > 0)
                    {
                        ApplyWorldEffect(TimeEffect.Rewind);
                    }
                    else if (!GetComponent<PlayerTimeOperator>().Shattered && 
                    GetComponent<PlayerTimeOperator>().CurrentTimeEffect == TimeEffect.None)
                    {
                        if (indexTimePowerSelected == 3 && EssenceAmounts[TimeEffect.FastForward] > 0)
                        {
                            ApplyPlayerEffect(TimeEffect.FastForward);
                        }
                        else if (indexTimePowerSelected == 4 && EssenceAmounts[TimeEffect.Record] > 0)
                        {
                            ApplyPlayerEffect(TimeEffect.Record);
                        }
                        else if (indexTimePowerSelected == 5 && EssenceAmounts[TimeEffect.Reset] > 0)
                        {
                            ApplyPlayerEffect(TimeEffect.Reset);
                        }
                    }
                }
            }
            else
            {
                if (WorldEffectIsActive)
                {
                    foreach (TimeOperator timeOp in TimeOperator.AllTimeOperators)
                    {
                        if (!timeOp.isActiveAndEnabled || EntitiesEffected.Contains(timeOp.gameObject)) continue;

                        if (timeOp.CurrentTimeEffect != TimeEffect.None || timeOp.GetComponent<PlayerWeaponOperator>() != null || (timeOp is CombatantTimeOperator cto && cto.Shattered))
                        {
                            AssignMask(timeOp.gameObject);
                            continue;
                        }

                        if (timeOp.GetComponent<IHasAlignment>() != null)
                        {
                            if (Combatant.ShouldEffect(GetComponent<IHasAlignment>().Alignment,
                            timeOp.GetComponent<IHasAlignment>().Alignment))
                            {
                                timeOp.CurrentTimeEffect = CurrentEffectActive;
                                if (!EntitiesEffected.Contains(timeOp.gameObject))
                                {
                                    EntitiesEffected.Add(timeOp.gameObject);
                                }
                            }
                            else
                            {
                                AssignMask(timeOp.gameObject);
                            }
                        }
                        else if (timeOp.GetComponentInParent<IHasAlignment>() != null)
                        {
                            if (Combatant.ShouldEffect(GetComponent<IHasAlignment>().Alignment,
                            timeOp.GetComponentInParent<IHasAlignment>().Alignment))
                            {
                                timeOp.CurrentTimeEffect = CurrentEffectActive;
                                if (!EntitiesEffected.Contains(timeOp.gameObject))
                                {
                                    EntitiesEffected.Add(timeOp.gameObject);
                                }
                            }
                            else
                            {
                                AssignMask(timeOp.gameObject);
                            }
                        }
                        else
                        {
                            timeOp.CurrentTimeEffect = CurrentEffectActive;
                            if (!EntitiesEffected.Contains(timeOp.gameObject))
                            {
                                EntitiesEffected.Add(timeOp.gameObject);
                            }
                        }
                    }

                    void AssignMask(GameObject obj)
                    {
                        // Making a big assumption here on the mask interaction thing, might be a bad idea
                        if (obj.GetComponent<SpriteRenderer>() != null && 
                        obj.GetComponent<SpriteRenderer>().maskInteraction == SpriteMaskInteraction.None && 
                        obj.GetComponentInChildren<EssenceOverlayMask>() == null)
                        {
                            if (CameraOperator.ActiveCamera.Overlay.maskInteraction != SpriteMaskInteraction.VisibleOutsideMask)
                            {
                                CameraOperator.ActiveCamera.Overlay.maskInteraction = SpriteMaskInteraction.VisibleOutsideMask;
                            }

                            GameObject spriteMask;
                            if (Pool.Has(SpriteMaskPool))
                            {
                                spriteMask = Pool.Get(SpriteMaskPool);
                            }
                            else
                            {
                                spriteMask = new GameObject("Sprite Mask", typeof(SpriteMask), typeof(EssenceOverlayMask));
                            }

                            spriteMask.SetActive(true);
                            spriteMask.transform.SetParent(obj.transform);
                            spriteMask.transform.localPosition = Vector3.zero;
                        }
                    }
                }

                bool hitTimePowerKey = indexTimePowerSelected != -1;
                if (hitTimePowerKey)
                {
                    EssenceAmounts[CurrentEffectActive] = 
                    EssenceAmounts[CurrentEffectActive] - UseAmounts[CurrentEffectActive] < 0 ? 
                    0 : EssenceAmounts[CurrentEffectActive] - UseAmounts[CurrentEffectActive];
                    EndEffect();
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            if (PlayerEffectIsActive && CurrentEffectActive != TimeEffect.None && 
            GetComponent<PlayerTimeOperator>().CurrentTimeEffect != CurrentEffectActive)
            {
                EndEffect();   
            }

            // All that's here is really just the code that colors the overlay when certain
            // effects are active.
            if (CurrentEffectActive != TimeEffect.None && CameraOperator.ActiveCamera.Overlay.color.a < OverlayAlphaMax)
            {
                CameraOperator.ActiveCamera.Overlay.color = new Color
                (
                    TimeOperator.EffectColors[CurrentEffectActive].r,
                    TimeOperator.EffectColors[CurrentEffectActive].g,
                    TimeOperator.EffectColors[CurrentEffectActive].b,
                    _overlayAlpha += OverlayFadeSpeed
                );
            }
            else if (_overlayAlpha >= 0)
            {
                CameraOperator.ActiveCamera.Overlay.color = new Color
                (
                    CameraOperator.ActiveCamera.Overlay.color.r,
                    CameraOperator.ActiveCamera.Overlay.color.g,
                    CameraOperator.ActiveCamera.Overlay.color.b,
                    _overlayAlpha -= OverlayFadeSpeed
                );

                if (_overlayAlpha <= 0f)
                {
                    foreach (EssenceOverlayMask m in FindObjectsOfType<EssenceOverlayMask>())
                    {
                        m.transform.SetParent(null);
                        Pool.Add(m.gameObject, SpriteMaskPool, false);
                        m.gameObject.SetActive(false);
                    }

                    CameraOperator.ActiveCamera.Overlay.maskInteraction = SpriteMaskInteraction.None;
                }
            }
        }

        protected virtual void OnApplicationQuit()
        {
            _applicationQuitting = true;
        }

        /// <summary>
        /// Starts up and applies an effect like pause, slow, or rewind, which effects
        /// the world.
        /// </summary>
        /// <param name="effect">The type of effect to apply.</param>
        public void ApplyWorldEffect(TimeEffect effect)
        {
            _overlayAlpha = 0;
            EntitiesEffected.Clear();
            CurrentEffectActive = effect;
            Timing.KillCoroutines(GetInstanceID().ToString());
            BackgroundMusicOperator.Main.Volume = 0f;
            SoundEffect.PlaySound(EssenceEffectStart, false);
            if (CurrentSoundEffect != null)
            {
                CurrentSoundEffect.Stop();
            }

            CurrentSoundEffect = SoundEffect.PlaySound(Sounds[(int) effect], false, false, 0f, true, true);
            Timing.RunCoroutine(DrainEssence(effect), Segment.FixedUpdate, GetInstanceID().ToString());
            // Normally we'd have to set the entities that should get effected, but this 
            // happens in the update to ensure any newly spawned entities also get effected.
        }

        /// <summary>
        /// Starts up and applies an effect like fast-forward, record, or reset, 
        /// which effects the player.
        /// </summary>
        /// <param name="effect">The type of effect to apply.</param>
        public void ApplyPlayerEffect(TimeEffect effect)
        {
            CurrentEffectActive = effect;
            Timing.KillCoroutines(GetInstanceID().ToString());
            BackgroundMusicOperator.Main.Volume = 0f;
            SoundEffect.PlaySound(EssenceEffectStart, false);
            if (CurrentSoundEffect != null)
            {
                CurrentSoundEffect.Stop();
            }

            CurrentSoundEffect = SoundEffect.PlaySound(Sounds[(int) effect], false, false, 0f, true, true);
            Timing.RunCoroutine(DrainEssence(effect), Segment.FixedUpdate, GetInstanceID().ToString());
            GetComponent<PlayerTimeOperator>().CurrentTimeEffect = effect;
        }

        /// <summary>
        /// Turns off the current effect. Any entities in the world will lose 
        /// the effect given to them.
        /// </summary>
        public void EndEffect()
        {
            if (CurrentEffectActive == TimeEffect.None)
            {
                return;
            }

            if (WorldEffectIsActive)
            {
                EntitiesEffected.RemoveAll(n => n == null);
                foreach (GameObject g in EntitiesEffected)
                {
                    if (g.GetComponent<TimeOperator>().CurrentTimeEffect == CurrentEffectActive)
                    {
                        g.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
                    }
                }
            }
            else if (PlayerEffectIsActive)
            {
                GetComponent<PlayerTimeOperator>().ForceSetTimeEffect(TimeEffect.None);
            }

            Timing.KillCoroutines(GetInstanceID().ToString());
            SoundEffect.PlaySound(EssenceEffectEnd, false);
            BackgroundMusicOperator.Main.Volume = 1f;
            if (CurrentSoundEffect != null)
            {
                CurrentSoundEffect.Stop();
            }

            EntitiesEffected.Clear();
            CurrentEffectActive = TimeEffect.None;
        }

        /// <summary>
        /// Adds to the player's specified type of essence.
        /// </summary>
        /// <param name="effect">The type of essence to add.</param>
        /// <param name="amount">The amount of it to add.</param>
        public void AddEssence(TimeEffect effect, int amount)
        {
            EssenceAmounts[effect] += amount;
            if (EssenceAmounts[effect] > MaxEssences[effect])
            {
                EssenceAmounts[effect] = MaxEssences[effect];
            }
        }

        /// <summary>
        /// Subtracts from the player's specified type of essence.
        /// </summary>
        /// <param name="effect">The type to remove from.</param>
        /// <param name="amount">The amount to remove.</param>
        public void RemoveEssence(TimeEffect effect, int amount)
        {
            EssenceAmounts[effect] -= amount;
            if (EssenceAmounts[effect] < 0)
            {
                EssenceAmounts[effect] = 0;
            }
        }

        /// <summary>
        /// Checks to see if the player has the ability to activate the specified
        /// type of essence. This includes whether or not the player has that type
        /// of essence, if the player is shattered, or if the player already effected
        /// by another type of essence. This will also return true if the player is
        /// using an essence effect.
        /// </summary>
        /// <param name="effect">The type of essence to check.</param>
        /// <returns>Whether or not he specified type of essence can be activated.</returns>
        public bool CanActivateEssence(TimeEffect effect)
        {
            if (GetComponent<PlayerTimeOperator>().CurrentTimeEffect == CurrentEffectActive && PlayerEffectIsActive)
            {
                return true;
            }

            switch (effect)
            {
                case TimeEffect.Pause:
                case TimeEffect.Slow:
                case TimeEffect.Rewind:
                    return EssenceAmounts[effect] > 0f;
                case TimeEffect.FastForward:
                case TimeEffect.Record:
                case TimeEffect.Reset:
                    return EssenceAmounts[effect] > 0f && !GetComponent<PlayerTimeOperator>().Shattered &&
                           GetComponent<PlayerTimeOperator>().CurrentTimeEffect == TimeEffect.None;
                case TimeEffect.None:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        /// <summary>
        /// Drains essence until the amount hits zero, or the current effect changes.
        /// </summary>
        /// <param name="effect">The essence type to drain.</param>
        public IEnumerator<float> DrainEssence(TimeEffect effect)
        {
            start:
            EssenceAmounts[effect] = EssenceAmounts[effect] - UseAmounts[effect] < 0 ? 
            0 : EssenceAmounts[effect] - UseAmounts[effect];

            yield return Timing.WaitForSeconds(Durations[effect]);
            yield return Timing.WaitUntilDone(new WaitUntil(() => MainPlayerOperator.MainPlayerObject.activeSelf));
            if (EssenceAmounts[effect] > 0 && CurrentEffectActive == effect)
            {
                goto start;
            }

            if (CurrentEffectActive == effect)
            {
                EndEffect();
            }
        }
    }
}
