﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Player.PUP
{
    public class Scannable : MonoBehaviour
    {
        public PupType Type;
        public string Unlocks;
    }
}
