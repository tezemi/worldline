﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using ProtoBuf;
using Worldline.GameState;

namespace Worldline.Player.PUP
{
    public class PupSystem
    {
        public static List<string> UnlockedEntries { get; protected set; } = new List<string>();

        //public static string GetPupName(string name, PupType type)
        //{
        //    return CultureString.Get("PUP/" + type)[name + "Name"];
        //}

        //public static string GetPupDetails(string name, PupType type)
        //{
        //    return CultureString.Get("PUP/" + type)[name + "Details"];
        //}

        //[ProtoContract]
        //public class PupData : FileData
        //{
        //    public List<string> UnlockedEntriesData;

        //    public override FileData GetFileData()
        //    {
        //        UnlockedEntriesData = UnlockedEntries;

        //        return this;
        //    }

        //    public override void LoadFileData()
        //    {
        //        UnlockedEntries = UnlockedEntriesData ?? new List<string>();
        //    }
        //}
    }
}
