﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.Player.PUP
{
    public enum PupType
    {
        Species,
        Locations,
        Tears
    }
}
