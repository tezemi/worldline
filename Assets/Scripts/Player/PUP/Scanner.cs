﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using TeamUtility.IO;
using UnityEngine;
using Worldline.Level;
using Worldline.Visuals;

namespace Worldline.Player.PUP
{
    public class Scanner : MonoBehaviour
    {
        public Sprite CrosshairSprite;
        public GameObject Crosshair { get; protected set; }
        public Scannable[] Scannables { get; protected set; }

        private bool _open;
        public bool Open
        {
            get
            {
                return _open;
            }
            protected set
            {
                if (_open == value) return;

                if (LevelController.Main.SetPaused(value, this))
                {
                    Crosshair.SetActive(value);
                    _open = value;
                }
            }
        }

        protected virtual void Awake()
        {
            Crosshair = new GameObject("Crosshair", typeof(SpriteRenderer));
            Crosshair.GetComponent<SpriteRenderer>().sprite = CrosshairSprite;
            Crosshair.SetActive(false);
        }

        protected virtual void OnLevelLoaded()
        {
            Scannables = FindObjectsOfType<Scannable>();
        }

        protected virtual void Update()
        {
            if (InputManager.GetButtonDown("scanner"))
            {
                Open = !Open;
            }

            if (Open)
            {
                Crosshair.transform.position = new Vector3
                (
                    CameraOperator.ActiveCamera.CameraComponent.ScreenToWorldPoint(Input.mousePosition).x,
                    CameraOperator.ActiveCamera.CameraComponent.ScreenToWorldPoint(Input.mousePosition).y,
                    -8f
                );

                foreach (Scannable s in Scannables)
                {
                    float minx = s.GetComponent<BoxCollider2D>().bounds.min.x;
                    float maxx = s.GetComponent<BoxCollider2D>().bounds.max.x;
                    float miny = s.GetComponent<BoxCollider2D>().bounds.min.y;
                    float maxy = s.GetComponent<BoxCollider2D>().bounds.max.y;

                    if (Crosshair.transform.position.x > minx &&
                    Crosshair.transform.position.x < maxx &&
                    Crosshair.transform.position.y > miny &&
                    Crosshair.transform.position.y < maxy &&
                    !PupSystem.UnlockedEntries.Contains(s.Unlocks))
                    {
                        Crosshair.GetComponent<SpriteRenderer>().color = Color.green;
                        if (InputManager.GetButtonDown("primary"))
                        {
                            PupSystem.UnlockedEntries.Add(s.Unlocks);
                        }
                    }
                    else
                    {
                        Crosshair.GetComponent<SpriteRenderer>().color = Color.white;
                    }
                }
            }
        }
    }
}
