﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.TimeEngine;
using ProtoBuf;

namespace Worldline.Player
{
    [ProtoContract]
    public class EssenceData : FileData
    {
        [ProtoMember(1)]
        public int EssenceForHealingCollected { get; set; }
        [ProtoMember(2)]
        public int EssenceForHealingNeeded { get; set; }
        [ProtoMember(3)]
        public int AmountOfHealingGiven { get; set; }
        [ProtoMember(4)]
        public Dictionary<TimeEffect, int> EssenceAmounts { get; set; }
        [ProtoMember(5)]
        public Dictionary<TimeEffect, int> UseAmounts { get; set; }
        [ProtoMember(6)]
        public Dictionary<TimeEffect, float> Durations { get; set; }
        [ProtoMember(7)]
        public Dictionary<TimeEffect, int> MaxEssences { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Postload;

        /// <summary>
        /// Sets the fields inside this PlayerData and returns the PlayerData
        /// as a FileData.
        /// </summary>
        /// <returns>This PlayerData as a FileData.</returns>
        public override FileData PopulateAndGetFileData()
        {
            EssenceAmounts = EssenceOperator.Main.EssenceAmounts;
            UseAmounts = EssenceOperator.Main.UseAmounts;
            Durations = EssenceOperator.Main.Durations;
            MaxEssences = EssenceOperator.Main.MaxEssences;

            return this;
        }

        /// <summary>
        /// Sets data about the player using data from inside this PlayerData.
        /// </summary>
        public override void SetFileData()
        {
            EssenceOperator.Main.EssenceAmounts = EssenceAmounts ?? new Dictionary<TimeEffect, int>
            {
                {TimeEffect.Pause, 0},
                {TimeEffect.Slow, 0},
                {TimeEffect.Rewind, 0},
                {TimeEffect.FastForward, 0},
                {TimeEffect.Record, 0},
                {TimeEffect.Reset, 0}
            };

            EssenceOperator.Main.UseAmounts = UseAmounts ?? new Dictionary<TimeEffect, int>
            {
                {TimeEffect.Pause, 10},
                {TimeEffect.Slow, 10},
                {TimeEffect.Rewind, 10},
                {TimeEffect.FastForward, 10},
                {TimeEffect.Record, 10},
                {TimeEffect.Reset, 10}
            };
            
            EssenceOperator.Main.Durations = Durations ?? new Dictionary<TimeEffect, float>
            {
                {TimeEffect.Pause, 10f},
                {TimeEffect.Slow, 10f},
                {TimeEffect.Rewind, 10f},
                {TimeEffect.FastForward, 10f},
                {TimeEffect.Record, 10f},
                {TimeEffect.Reset, 10f}
            };

            EssenceOperator.Main.MaxEssences = MaxEssences ?? new Dictionary<TimeEffect, int>
            {
                {TimeEffect.Pause, 100},
                {TimeEffect.Slow, 100},
                {TimeEffect.Rewind, 100},
                {TimeEffect.FastForward, 100},
                {TimeEffect.Record, 100},
                {TimeEffect.Reset, 100}
            };
        }
    }
}
