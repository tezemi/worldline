﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Player
{
    public class DeadPlayerTutorial : DeadPlayer2
    {
        public bool AllowedToReset { get; set; }
        public bool ResetAmountRecorded { get; protected set; }
        public const int PositionToResetTo = 200;
        
        protected override void Update()
        {
            if (!AllowedToReset) return;

            base.Update();

            if (TimeOperator.CurrentTimeEffect == TimeEffect.Reset && GetComponent<Recorder>().AmountRecorded > PositionToResetTo && !ResetAmountRecorded)
            {
                GetComponent<Recorder>().RewindToPoint(PositionToResetTo);
                ResetAmountRecorded = true;
            }
        }

        protected virtual void OnDestroy()
        {
            if (CameraOperator.ActiveCamera != null)
            {
                CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
                CameraOperator.ActiveCamera.enabled = true;
            }

            Timing.RunCoroutine(WaitForReset(), Segment.FixedUpdate);

            IEnumerator<float> WaitForReset()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() =>
                MainPlayerOperator.MainPlayerObject.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.None));
                MainPlayerOperator.MainPlayerObject.GetComponent<AnimationOperator>().enabled = true;
            }
        }

        protected override IEnumerator<float> LoadGameOverMenu()
        {
            yield break;
        }
    }
}
