﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Visuals;
using Worldline.Physics;
using Worldline.GUI.HUD;
using Worldline.GUI.Menus;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using Worldline.GameState;
using Worldline.ScriptedSequences;

namespace Worldline.Player
{
    public class DeadPlayer : MonoBehaviour
    {
        public static DeadPlayer Main;
        public AudioClip GameOverMusic;
        public AudioClip ResetActivated;
        public Sprite BlackBackgroundSprite;
        public Sprite DeathSpotlight;
        public bool GameOver { get; protected set; }
        public bool MenuOpen { get; protected set; }
        public bool ReadyToSkip { get; protected set; }
        public virtual bool ShouldEndEvents { get; } = true;
        public const int DisableReadyTicks = 25;
        public const float LongWaitTime = 4.5f;
        public const float ShortWaitTime = 0.35f;
        public const float AnimDelay = 0.25f;
        public const float BgmPitch = 0.1f;
        public const float BgmSlowSpeed = 0.01f;
        public const float DistanceOutsideLevel = 10f;
        protected EssenceOperator EssenceOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        private int _readyTicks;
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            if (ShouldEndEvents)
            {
                ScriptedSequence.EndAllEvents();
            }
            
            SpriteRenderer = GetComponent<SpriteRenderer>();
            EssenceOperator = MainPlayerOperator.MainPlayerObject.GetComponent<EssenceOperator>();
            CameraOperator.ActiveCamera.Track = gameObject;
            GetComponent<SpriteRenderer>().flipX = MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX;
            HUDController.Main.Shake(3);
            Timing.RunCoroutine(SlowBgm(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(Animate(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            if (SceneTransitionOperator.Main == null || SceneTransitionOperator.Main.Transitioning) return;

            // Use reset essence.
            if (!GameOver && 
            !CheatController.IsActive(Cheat.Nightmare) &&
            GetComponent<TimeOperator>().CurrentTimeEffect != TimeEffect.Reset &&
            EssenceOperator.Main.EssenceAmounts[TimeEffect.Reset] > 0 && 
            !MainPlayerOperator.MainPlayerComponent.InEvent &&
            (InputManager.GetButtonDown("time_power_6") || 
            InputManager.GetButtonDown("activate_essence")))
            {

                GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.Reset;
                Timing.RunCoroutine(SpeedBgm(), Segment.FixedUpdate, GetInstanceID().ToString());
                SoundEffect.PlaySound(ResetActivated);
                Timing.RunCoroutine(AnimateResetIcon(), Segment.FixedUpdate);
            }

            // Don't fall too far out of level.
            if (transform.position.y < LevelController.Main.MapHeightMin - DistanceOutsideLevel)
            {
                GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            }

            // Skip if double click
            if (InputManager.GetButtonDown("primary") && !MenuOpen)
            {
                if (ReadyToSkip)
                {
                    if (!SceneTransitionOperator.Main.Transition)
                    {
                        // TODO: SaveFile.LoadGame();
                        //SceneTransitionOperator.Main.LoadScene();
                    }
                }
                else
                {
                    ReadyToSkip = true;
                }
            }

            if (ReadyToSkip)
            {
                if (_readyTicks++ > DisableReadyTicks)
                {
                    ReadyToSkip = false;
                }
            }
            else
            {
                _readyTicks = 0;
            }

            // Shut things up.
            if (GameOver)
            {
                foreach (AudioSource a in FindObjectsOfType<AudioSource>())
                {
                    if (a != BackgroundMusicOperator.Main.MainAudioSource)
                    {
                        a.mute = true;
                    }
                }
            }

            // Clear the overlay
            CameraOperator.ActiveCamera.Overlay.color = Vector4.MoveTowards
            (
                CameraOperator.ActiveCamera.Overlay.color,
                new Color(1f, 1f, 1f, 0f), 
                0.02f
            );
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> SlowBgm()
        {
            start:
            yield return Timing.WaitForSeconds(0.02f);
            if (BackgroundMusicOperator.Main.Speed > 0f)
            {
                BackgroundMusicOperator.Main.Speed -= BgmSlowSpeed;
                goto start;
            }
        }

        public virtual IEnumerator<float> SpeedBgm()
        {
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => GetComponent<Recorder>().AmountRecorded <= 120)
            );

            start:
            yield return Timing.WaitForSeconds(0.02f);
            if (BackgroundMusicOperator.Main.Speed < 1f)
            {
                BackgroundMusicOperator.Main.Speed += BgmSlowSpeed;
                goto start;
            }
            else
            {
                BackgroundMusicOperator.Main.Speed = 1f;
            }
        }

        protected virtual IEnumerator<float> Animate()
        {
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[0];
            yield return Timing.WaitForSeconds(AnimDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[1];
            yield return Timing.WaitForSeconds(AnimDelay);
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => GetComponent<MovementOperator>().Grounded 
                || transform.position.y < LevelController.Main.MapHeightMin - DistanceOutsideLevel)
            );

            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[2];
            yield return Timing.WaitForSeconds(AnimDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[3];
            yield return Timing.WaitForSeconds(AnimDelay);
            SpriteRenderer.sprite = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.Dead[4];
            Timing.RunCoroutine(TransitionToGameOver(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> TransitionToGameOver()
        {
            yield return this.WaitForFixedSecondsScaled
            (
                EssenceOperator.Main.EssenceAmounts[TimeEffect.Reset] > 0 && !CheatController.IsActive(Cheat.Nightmare) ? LongWaitTime : ShortWaitTime
            );

            if (GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Reset)
            {
                yield break;
            }

            GameOver = true;
            GameObject blackBackgroundGameObject = new GameObject("Game Over Background", typeof(SpriteRenderer));
            blackBackgroundGameObject.GetComponent<SpriteRenderer>().sprite = BlackBackgroundSprite;
            blackBackgroundGameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
            transform.position = new Vector3(transform.position.x, transform.position.y, CameraOperator.ActiveCamera.transform.position.z + 1f);
            blackBackgroundGameObject.transform.position = transform.position + new Vector3(0f, 0f, 0.25f);
            blackBackgroundGameObject.transform.localScale = new Vector3(10f, 10f, 1f);
            Timing.RunCoroutine(FadeHud(), Segment.FixedUpdate, GetInstanceID().ToString());

            fadeInBackground:
            yield return this.WaitForFixedSecondsScaled(0.01f);
            blackBackgroundGameObject.GetComponent<SpriteRenderer>().color = Vector4.MoveTowards
            (
                blackBackgroundGameObject.GetComponent<SpriteRenderer>().color,
                Color.white,
                0.04f
            );

            if (blackBackgroundGameObject.GetComponent<SpriteRenderer>().color != Color.white)
            {
                goto fadeInBackground;
            }

            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            CameraOperator.ActiveCamera.Mode = CameraMode.Lerp;
            CameraOperator.ActiveCamera.HorizontalOffset = 0f;
            CameraOperator.ActiveCamera.DisableBounds = true;
            CameraOperator.ActiveCamera.Track = null;
            CameraOperator.ActiveCamera.LerpSpeed = 0.05f;
            CameraOperator.ActiveCamera.Focus = transform.position + new Vector3(0f, 2.5f, 0f);
            yield return this.WaitForFixedSecondsScaled(1f);
            BackgroundMusicOperator.Main.AudioClip = null;
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => BackgroundMusicOperator.Main.MainAudioSource.volume < 0.05f || BackgroundMusicOperator.Main.AudioClip == null)
            );

            BackgroundMusicOperator.Main.MainAudioSource.CancelAudioFade();
            BackgroundMusicOperator.Main.Volume = 1f;
            BackgroundMusicOperator.Main.Speed = 0.85f;
            BackgroundMusicOperator.Main.MainAudioSource.loop = false;
            BackgroundMusicOperator.Main.MainAudioSource.clip = GameOverMusic;
            BackgroundMusicOperator.Main.MainAudioSource.Play();
            MenuOpen = true;
            GameOverMenu.Main.gameObject.SetActive(true);
            GameObject deathSpotlightGameObject = new GameObject("Death Spotlight", typeof(SpriteRenderer));
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().sprite = DeathSpotlight;
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().color = new Color(1f, 1f, 1f, 0f);
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().transform.position = transform.position + new Vector3(0f, -0.95f, 0.15f);

            fadeInSpotlight:
            yield return this.WaitForFixedSecondsScaled(0.02f);
            deathSpotlightGameObject.GetComponent<SpriteRenderer>().color = Vector4.MoveTowards
            (
                deathSpotlightGameObject.GetComponent<SpriteRenderer>().color,
                Color.white,
                0.02f
            );

            if (deathSpotlightGameObject.GetComponent<SpriteRenderer>().color != Color.white)
            {
                goto fadeInSpotlight;
            }
        }

        protected virtual IEnumerator<float> FadeHud()
        {
            HUDController.Main.enabled = false;
            bool shouldFadeHud = false;
            fadeOutHud:
            yield return Timing.WaitForSeconds(0.02f);
            if (HUDController.Main == null) yield break;
            foreach (Transform element in HUDController.Main.GetComponentsInChildren<Transform>())
            {
                if (element.GetComponent<Image>() != null)
                {
                    element.GetComponent<Image>().color = Vector4.MoveTowards
                    (
                        element.GetComponent<Image>().color,
                        Color.clear,
                        0.02f
                    );

                    if (element.GetComponent<Image>().color != Color.clear)
                    {
                        shouldFadeHud = true;
                    }
                }
                else if (element.GetComponent<Text>() != null)
                {
                    element.GetComponent<Text>().color = Vector4.MoveTowards
                    (
                        element.GetComponent<Text>().color,
                        Color.clear,
                        0.02f
                    );

                    if (element.GetComponent<Text>().color != Color.clear)
                    {
                        shouldFadeHud = true;
                    }
                }
            }

            if (shouldFadeHud)
            {
                goto fadeOutHud;
            }
        }
        
        public virtual IEnumerator<float> AnimateResetIcon()
        {
            GameObject resetIcon = new GameObject("Reset Icon", typeof(Image));
            resetIcon.transform.SetParent(HUDController.Main.transform);
            resetIcon.transform.SetAsFirstSibling();
            resetIcon.transform.position = CrystalElementsController.Main.OpaqueCrystals[5].transform.position;
            resetIcon.GetComponent<Image>().sprite = CrystalElementsController.Main.OpaqueCrystals[5].sprite;
            resetIcon.GetComponent<Image>().color = new Color(1f, 1f, 1f, 0.75f);
            resetIcon.GetComponent<RectTransform>().sizeDelta =
            CrystalElementsController.Main.OpaqueCrystals[5].GetComponent<RectTransform>().sizeDelta;
            resetIcon.GetComponent<RectTransform>().localScale =
            CrystalElementsController.Main.OpaqueCrystals[5].GetComponent<RectTransform>().localScale / 2f;
            for (int i = 0; i < 120; i++)
            {
                yield return Timing.WaitForOneFrame;
                resetIcon.GetComponent<RectTransform>().localScale += new Vector3(0.02f, 0.02f, 0f);
                resetIcon.GetComponent<Image>().color -= new Color(0f, 0f, 0f, 0.02f);
            }

            Destroy(resetIcon);
        }
    }
}
