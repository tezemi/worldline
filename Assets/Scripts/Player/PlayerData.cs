﻿// Copyright (c) 2019 Destin Hebner
using System.IO;
using Worldline.GameState;
using ProtoBuf;
using UnityEngine;

namespace Worldline.Player
{
    /// <summary>
    /// PlayerData is a FileData that holds and sets information relevant
    /// to the PlayerOperator and the main player GameObject.
    /// </summary>
    [ProtoContract]
    public class PlayerData : FileData
    {
        [ProtoMember(1)]
        public int Health { get; set; }
        [ProtoMember(2)]
        public int MaxHealth { get; set; }
        [ProtoMember(3)]
        public float PositionX { get; set; }
        [ProtoMember(4)]
        public float PositionY { get; set; }
        [ProtoMember(5)]
        public float PositionZ { get; set; }
        [ProtoMember(6)]
        public string PlayerSpriteBundle { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Postload;
        public const string PlayerSpriteBundleAssetBundle = "player_sprite_bundles.bun";

        /// <summary>
        /// Sets the fields inside this PlayerData and returns the PlayerData
        /// as a FileData.
        /// </summary>
        /// <returns>This PlayerData as a FileData.</returns>
        public override FileData PopulateAndGetFileData()
        {
            Health = MainPlayerOperator.MainPlayerComponent.Health;
            MaxHealth = MainPlayerOperator.MainPlayerComponent.MaxHealth;
            PositionX = MainPlayerOperator.MainPlayerObject.transform.position.x;
            PositionY = MainPlayerOperator.MainPlayerObject.transform.position.y;
            PositionZ = MainPlayerOperator.MainPlayerObject.transform.position.z;
            PlayerSpriteBundle = MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle.AssetPath;

            return this;
        }

        /// <summary>
        /// Sets data about the player using data from inside this PlayerData.
        /// </summary>
        public override void SetFileData()
        {
            MainPlayerOperator.MainPlayerComponent.Health = Health;
            MainPlayerOperator.MainPlayerComponent.MaxHealth = MaxHealth;
            MainPlayerOperator.MainPlayerObject.transform.position = new Vector3(PositionX, PositionY, PositionZ);
            AssetBundle playerSpriteBundles = AssetBundle.LoadFromFile
            (
                Path.Combine(Application.streamingAssetsPath, PlayerSpriteBundleAssetBundle)
            );

            if (playerSpriteBundles == null)
            {
                Debug.LogError($"Failed to load Asset Bundle {PlayerSpriteBundleAssetBundle}!");
                return;
            }

            MainPlayerOperator.MainPlayerComponent.PlayerSpriteBundle = playerSpriteBundles.LoadAsset<PlayerSpriteBundle>(PlayerSpriteBundle);
            playerSpriteBundles.Unload(false);
        }
    }
}
