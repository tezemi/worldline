﻿// Copyright (c) 2019 Destin Hebner
using Worldline.ScriptedSequences;
using UnityEngine;

namespace Worldline.NPCs.Characters
{
    public class GregSimulation : AnimatedCharacter
    {
        [Header("Greg Simulation")]
        [Tooltip("If true, Greg will spawn regardless of whether or not PlayerHasMadeGregSoMadHeQuit is true.")]
        public bool SpawnAlways;
        [Tooltip("If true, Greg will only spawn when guiding the player.")]
        public bool OnlySpawnIfGregIsShowingPlayerWhereBurningSunIs;

        protected override void Awake()
        {
            if (GlobalFlags.GregIsShowingPlayerWhereBurningSunIs && Fired)
            {
                gameObject.SetActive(false);
            }
            else if (!GlobalFlags.GregIsShowingPlayerWhereBurningSunIs && OnlySpawnIfGregIsShowingPlayerWhereBurningSunIs && !SpawnAlways)
            {
                gameObject.SetActive(false);
            }
            else if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit && !SpawnAlways)
            {
                gameObject.SetActive(false);
            }
            else
            {
                base.Awake();
            }
        }
    }
}
