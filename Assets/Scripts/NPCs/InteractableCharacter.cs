﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using UnityEngine;

namespace Worldline.NPCs
{
    /// <summary>
    /// Class for character GameObjects that don't engage in combat. Implements
    /// behavior for interacting with the player through events. Used for
    /// friendly NPCs that essentially act as decoration.
    /// </summary>
    public class InteractableCharacter : GameCharacter
    {
        [Header("Interactable Character")]
        [Tooltip("If true, this NPC will always turn to face the player.")]
        public bool AlwaysFacePlayer;
        [Tooltip("If true, this NPC will face the player when talked to.")]
        public bool FacePlayerWhenTalkedTo = true;
        public bool PlayerIsTalkingTo { get; protected set; }

        protected virtual void Update()
        {
            if (MainPlayerOperator.MainPlayerObject == null) return;

            if (AlwaysFacePlayer && !PlayerIsTalkingTo)
            {
                SpriteRenderer.flipX = MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x;
            }
        }

        protected virtual void OnEventActivate()
        {
            PlayerIsTalkingTo = true;
            if (FacePlayerWhenTalkedTo)
            {
                SpriteRenderer.flipX = MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x;
            }
        }

        protected virtual void OnEventDeactivate()
        {
            PlayerIsTalkingTo = false;
        }
    }
}
