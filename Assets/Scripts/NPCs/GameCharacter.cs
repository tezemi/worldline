﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Combat;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.GameState;
using UnityEngine;

namespace Worldline.NPCs
{
    /// <summary>
    /// Base class for GameObjects that serve as game characters or 
    /// in the game's world. Adds behavior for physics, collision, 
    /// and animations. Requires physics and movement components.
    /// Can also have an Alignment for time-related stuff, but any
    /// combat behavior is implented in the Combatant class.
    /// </summary>
    [RequireComponent(typeof(Collider2D))]
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(MovementOperator))]
    [RequireComponent(typeof(AnimationOperator))]
    public class GameCharacter : TrackedMonoBehaviour, IHasAlignment
    {
        protected Rigidbody2D Rigidbody { get; set; }
        protected Collider2D SolidCollider { get; set; }
        protected Collider2D TriggerCollider { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        protected AnimationOperator AnimationOperator { get; set; }

        [SerializeField]
        [Header("GameCharacter")]
        [Tooltip("The alignment of this character, used for time and combat mechanics.")]
        protected CombatAlignment _alignment = CombatAlignment.Neutral;
        public CombatAlignment Alignment
        {
            get => _alignment;
            set => _alignment = value;
        }

        protected virtual void Awake()
        {
            Rigidbody = GetComponent<Rigidbody2D>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            MovementOperator = GetComponent<MovementOperator>();
            AnimationOperator = GetComponent<AnimationOperator>();
            foreach (Collider2D c in GetComponents<Collider2D>())
            {
                if (c.isTrigger) continue;

                SolidCollider = c;
                break;
            }

            foreach (Collider2D c in GetComponents<Collider2D>())
            {
                if (!c.isTrigger) continue;

                TriggerCollider = c;
                break;
            }
        }

        /// <summary>
        /// Sets the animator for this NPC to the specified sprites if the animation
        /// has not already been set, preventing accidental resets.
        /// </summary>
        /// <param name="sprites">The sprites to set the animation to.</param>
        public void SetAnimation(Sprite[] sprites)
        {
            if (AnimationOperator.Sprites != sprites)
            {
                SpriteRenderer.sprite = sprites[0];
                AnimationOperator.Sprites = sprites;
            }
        }
    }
}
