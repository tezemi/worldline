﻿
namespace Worldline.NPCs.Enemies.Tears
{
    public enum BossMovementState
    {
        Idle,
        SwitchSides,
        FollowPlayer,
        MoveOffScreen
    }
}
