﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Physics;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Hurl : Tear
    {
        [Header("Hurl")]
        public int JumpOnFrame = 4;
        public float DistanceToLook;
        public float DistanceToJump;
        public float JumpCooldownTime = 1f;
        public Sprite[] IdleSprites;
        public Sprite[] LookSprites;
        public Sprite[] JumpSprites;
        public bool Jumping { get; protected set; }
        public bool CoolingDownJump { get; protected set; }
        
        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
            Jumping = false;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (CombatantTimeOperator.ShouldHold) return;

            if (Vector2.Distance(GetClosestPlayer().transform.position, transform.position) < DistanceToJump && !Jumping && !CoolingDownJump)
            {
                Timing.RunCoroutine(Jump(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
            else if (Vector2.Distance(GetClosestPlayer().transform.position, transform.position) < DistanceToLook && !Jumping)
            {
                SetAnimation(LookSprites);
            }
            else if (!Jumping)
            {
                SetAnimation(IdleSprites);
            }

            if (Math.Abs(Rigidbody.velocity.y) > 0.05f)
            {
                SpriteRenderer.flipX = Rigidbody.velocity.x < 0f;
            }
            else if (Vector2.Distance(GetClosestPlayer().transform.position, transform.position) < DistanceToLook)
            {
                SpriteRenderer.flipX = GetClosestPlayer().transform.position.x < transform.position.x;
            }
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);
            if (!Jumping && !CoolingDownJump && !CombatantTimeOperator.ShouldHold)
            {
                Timing.RunCoroutine(Jump(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
        }

        public IEnumerator<float> Jump()
        {
            Jumping = true;
            CoolingDownJump = true;
            SetAnimation(JumpSprites);
            yield return Timing.WaitUntilDone(() => AnimationOperator.CurrentFrame == JumpOnFrame);
            MovementOperator.Jump();
            float speedVariance = Vector2.Distance(GetClosestPlayer().transform.position, transform.position);
            MovementOperator.Speed = new Vector2
            (
                GetClosestPlayer().transform.position.x > transform.position.x ? speedVariance : -speedVariance,
                0f
            );

            float normalAnimationDelay = AnimationOperator.Delay;
            AnimationOperator.Delay = int.MaxValue;
            yield return Timing.WaitForSeconds(0.1f);
            yield return Timing.WaitUntilDone(() => GetComponent<MovementOperator>().Grounded);
            SetAnimation(IdleSprites);

            AnimationOperator.Delay = normalAnimationDelay;
            MovementOperator.Speed = new Vector2(0, 0);
            Jumping = false;
            yield return this.WaitForFixedSecondsScaled(JumpCooldownTime);
            CoolingDownJump = false;
        }
    }
}
