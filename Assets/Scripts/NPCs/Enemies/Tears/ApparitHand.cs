﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Physics;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.NPCs
{
    public class ApparitHand : Tear
    {
        [Header("Apparit Hand")]
        public float RotateSpeed = 5f;
        public float Radius = 0.5f;
        public Sprite MiddleFinger;
        public Apparit ApparitParent;
        public bool Attacking { get; set; } = true;
        public const float AttackInterval = 5;
        public const float Speed = 3;
        public const float SpeedRange = 0.25f;
        protected CoroutineTimer AttackTimer;
        private Vector3 _posOnAttack;
        private float _angle;

        protected override void Awake()
        {
            base.Awake();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            AttackTimer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                _posOnAttack = transform.position;
                Attacking = true;
            });
        }

        protected virtual void OnEnable()
        {
            _posOnAttack = transform.position;
            AttackTimer.StartTimer(AttackInterval, () => isActiveAndEnabled);
        }

        protected override void Update()
        {
            base.Update();

            if (GetComponent<TimeOperator>().ShouldHold || MainPlayerOperator.MainPlayerObject == null)
            {
                GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
                GetComponent<MovementOperator>().Speed = new Vector2(0f, 0f);
                return;
            }

            if (ApparitParent != null)
            {
                SpriteRenderer.flipX = ApparitParent.GetComponent<SpriteRenderer>().flipX;
            }
            
            float speedFinal = Speed - Random.Range(0, SpeedRange);
            if (Attacking)
            {
                // Fly at player
                PlayerOperator ply = GetClosestPlayer();
                float px = ply.transform.position.x;
                float py = ply.transform.position.y;
                GetComponent<MovementOperator>().Speed = new Vector2
                (
                    px > transform.position.x ? speedFinal : -speedFinal, 
                    py > transform.position.y ? speedFinal : -speedFinal
                );

                // If the hands go past the player, retract them
                if (_posOnAttack.x < px && transform.position.x > px || _posOnAttack.x > px && transform.position.x < px)
                {
                    Attacking = false;
                }
            }
            else
            {
                // Move in circular motion, trying to keep up with body.
                _angle += RotateSpeed * Time.deltaTime * GetComponent<TimeOperator>().TimeScale;

                if (ApparitParent != null)
                {
                    Vector3 center = ApparitParent.transform.position;
                    Vector3 offset = new Vector3(Mathf.Sin(_angle), Mathf.Cos(_angle), center.z) * Radius;
                    GetComponent<MovementOperator>().Speed = new Vector2
                    (
                        transform.position.x > (center + offset).x ? -speedFinal : speedFinal, 
                        transform.position.y > (center + offset).y ? -speedFinal : speedFinal
                    );
                }
            }
        }
    }
}
