﻿
namespace Worldline.NPCs.Enemies.Tears
{
    public enum BossHandMovementState
    {
        Idle,
        Shake,
        FollowBoss,
        FlyAtPlayer,
        FollowPlayer
    }
}
