﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.Combat;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Corruption;
using Worldline.Combat.Projectiles;
using Worldline.ScriptedSequences.Sim.Main;
using MEC;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.NPCs.Enemies.Tears
{
    public class Boss : Enemy
    {
        public static Boss Main { get; protected set; }
        public bool FacePlayer = true;
        public int NumberOfProjectilesToVomit = 8;
        public float Speed = 4f;
        public float OffScreenYPosition;
        public float DistanceFromPlayer = 8f;
        public float VerticalLevelPadding = 2f;
        public float HorizontalLevelPadding = 2f;
        public BossActionState ActionState = BossActionState.Idle;
        public BossMovementState MovementState = BossMovementState.FollowPlayer;
        public Tilemap Layout1;
        public Tilemap Layout2;
        public Tilemap Layout3;
        public AudioClip SpawningEnemySound;
        public GameObject EnemyToSpawnPrefab;
        public GameObject ProjectileToSpawnPrefab;
        public GameObject ProjectileToVomitPrefab;
        public Sprite[] IdleSprites;
        public Sprite[] SpawningEnemySprites;
        public Sprite[] SpawningProjectilesSprites;
        public Sprite[] VomittingProjectilesSprites;
        public Vector2 HoverPoint { get; set; }
        public Vector2? IdlePoint { get; set; }
        public bool SpawningProjectiles { get; protected set; }
        public bool VomittingProjectiles { get; protected set; }
        public bool ActivatedLayout2 { get; protected set; }
        public bool ActivatedLayout3 { get; protected set; }
        public const float EnemySpawnShakeAmount = 0.01f;
        private bool? _startedRotationRightOfPlayer;
        private CoroutineHandle _anyAction;
        private SoundEffect _spawningEnemySound;

        protected override void Awake()
        {
            base.Awake();
            Main = this;
            gameObject.SetActive(false);
            TearDeathAnimation.AddSpriteToCache(DeathSprite);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(AITick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Rewind)
            {
                if (SpawningProjectiles || VomittingProjectiles)
                {
                    Timing.KillCoroutines(_anyAction);
                    SpawningProjectiles = false;
                    VomittingProjectiles = false;
                }
            }

            if ((float)Health / MaxHealth <= 0.05f)
            {
                Corrupter.Intensity = 1f;
            }
            else if ((float)Health / MaxHealth <= 0.25f)
            {
                Corrupter.Intensity = 0.75f;
                if (!ActivatedLayout3)
                {
                    Timing.RunCoroutine(ActivateLayout(Layout2, Layout3));
                    ActivatedLayout3 = true;
                }
            }
            else if ((float)Health / MaxHealth <= 0.5f)
            {
                Corrupter.Intensity = 0.5f;
                if (!ActivatedLayout2)
                {
                    Timing.RunCoroutine(ActivateLayout(Layout1, Layout2));
                    ActivatedLayout2 = true;
                }
            }
            else if ((float)Health / MaxHealth <= 0.75f)
            {
                Corrupter.Intensity = 0.25f;
            }
            else
            {
                Corrupter.Intensity = 0f;
            }

            if (CombatantTimeOperator.ShouldHold)
            {
                return;
            }

            GameObject player = MainPlayerOperator.MainPlayerObject;

            if (FacePlayer)
            {
                SpriteRenderer.flipX = player.transform.position.x > transform.position.x;
            }

            #region Actions
            if (ActionState == BossActionState.SpawningEnemy)
            {
                if (AnimationOperator.Sprites != SpawningEnemySprites)
                {
                    AnimationOperator.Sprites = SpawningEnemySprites;
                    AnimationOperator.ForceUpdate();
                }

                if (_spawningEnemySound == null || !_spawningEnemySound.IsPlaying)
                {
                    _spawningEnemySound = SoundEffect.PlaySound(SpawningEnemySound, CombatantTimeOperator);
                }

                MovementState = BossMovementState.Idle;
                float shakeAmount = EnemySpawnShakeAmount * AnimationOperator.CurrentFrame;
                transform.position += new Vector3(Random.value < 0.5f ? shakeAmount : -shakeAmount, 0f, 0f);
                if (AnimationOperator.CurrentFrame == SpawningEnemySprites.Length)
                {
                    GameObject tear = Instantiate(EnemyToSpawnPrefab);
                    tear.transform.position = transform.position + new Vector3(0f, 1f, 0.025f);
                    if (tear.GetComponent<Flockling>() != null)
                    {
                        tear.GetComponent<Flockling>().AttackPlayer();
                    }

                    AnimationOperator.Sprites = IdleSprites;
                    AnimationOperator.ForceUpdate();
                    ActionState = BossActionState.Idle;
                    MovementState = BossMovementState.FollowPlayer;
                    if (_spawningEnemySound != null && _spawningEnemySound.IsPlaying)
                    {
                        _spawningEnemySound.Stop();
                    }
                }
            }
            else if (ActionState == BossActionState.RotatePosition)
            {
                if (!IdlePoint.HasValue)
                {
                    MovementState = BossMovementState.MoveOffScreen;
                }

                if (transform.position.y >= OffScreenYPosition)
                {
                    IdlePoint = null;
                    MovementState = BossMovementState.SwitchSides;
                    _startedRotationRightOfPlayer = transform.position.x > player.transform.position.x;
                }

                if (_startedRotationRightOfPlayer.HasValue)
                {
                    if (_startedRotationRightOfPlayer.Value && transform.position.x < player.transform.position.x || !_startedRotationRightOfPlayer.Value && transform.position.x > player.transform.position.x)
                    {
                        _startedRotationRightOfPlayer = null;
                        ActionState = BossActionState.Idle;
                        MovementState = BossMovementState.FollowPlayer;
                    }
                }
            }
            else if (ActionState == BossActionState.SpawningProjectiles)
            {
                if (!SpawningProjectiles)
                {
                    Timing.KillCoroutines(_anyAction);
                    _anyAction = Timing.RunCoroutine(SpawnProjectiles(), Segment.FixedUpdate, GetInstanceID().ToString());
                    SpawningProjectiles = true;
                }
            }
            else if (ActionState == BossActionState.VomittingProjectiles)
            {
                if (!VomittingProjectiles)
                {
                    Timing.KillCoroutines(_anyAction);
                    _anyAction = Timing.RunCoroutine(VomitProjectiles(), Segment.FixedUpdate, GetInstanceID().ToString());
                    VomittingProjectiles = true;
                }
            }
            #endregion

            #region Movement
            if (MovementState == BossMovementState.Idle)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = transform.position;
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value;
                }
            }
            else if (MovementState == BossMovementState.FollowPlayer)
            {
                if (IdlePoint.HasValue)
                {
                    IdlePoint = null;
                }

                HoverPoint = player.transform.position + new Vector3(player.transform.position.x < transform.position.x ? DistanceFromPlayer : -DistanceFromPlayer, 0f);
            }
            else if (MovementState == BossMovementState.MoveOffScreen)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = new Vector2(transform.position.x, OffScreenYPosition);
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value;
                }
            }
            else if (MovementState == BossMovementState.SwitchSides)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = new Vector3
                    (
                        player.transform.position.x < transform.position.x ? player.transform.position.x - DistanceFromPlayer : player.transform.position.x + DistanceFromPlayer, 
                        0f, 
                        0f
                    );
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value;
                }
            }

            if (!IdlePoint.HasValue)
            {
                if (HoverPoint.x >= LevelController.Main.MapWidthMax - HorizontalLevelPadding)
                {
                    HoverPoint = new Vector2(LevelController.Main.MapWidthMax - HorizontalLevelPadding, HoverPoint.y);
                }
                else if (HoverPoint.x <= LevelController.Main.MapWidthMin + HorizontalLevelPadding)
                {
                    HoverPoint = new Vector2(LevelController.Main.MapWidthMin + HorizontalLevelPadding, HoverPoint.y);
                }

                if (HoverPoint.y <= LevelController.Main.MapHeightMin + VerticalLevelPadding)
                {
                    HoverPoint = new Vector2(HoverPoint.x, LevelController.Main.MapHeightMin + VerticalLevelPadding);
                }
            }

            MovementOperator.Speed = new Vector2(transform.position.x < HoverPoint.x ? Speed : -Speed, transform.position.y < HoverPoint.y ? Speed : -Speed);
            #endregion
        }

        protected IEnumerator<float> SpawnProjectiles()
        {
            MovementState = BossMovementState.Idle;
            yield return Timing.WaitForSeconds(1f);
            AnimationOperator.enabled = false;
            SpriteRenderer.sprite = SpawningProjectilesSprites[0];
            yield return Timing.WaitForSeconds(0.15f);
            SpriteRenderer.sprite = SpawningProjectilesSprites[1];
            yield return Timing.WaitForSeconds(1.5f);

            GameObject projectile1 = Instantiate(ProjectileToSpawnPrefab);
            projectile1.transform.position = transform.position + new Vector3(-2.701f, 1f, 0f);

            GameObject projectile2 = Instantiate(ProjectileToSpawnPrefab);
            projectile2.transform.position = transform.position + new Vector3(2.791f, 0.915f, 0f);

            SpriteRenderer.sprite = SpawningProjectilesSprites[2];
            yield return Timing.WaitForSeconds(1f);
            SpriteRenderer.sprite = SpawningProjectilesSprites[3];
            yield return Timing.WaitForSeconds(0.15f);
            AnimationOperator.enabled = true;
            ActionState = BossActionState.Idle;
            MovementState = BossMovementState.FollowPlayer;
            SpawningProjectiles = false;
        }

        protected IEnumerator<float> VomitProjectiles()
        {
            MovementState = BossMovementState.Idle;
            AnimationOperator.enabled = false;
            SpriteRenderer.sprite = VomittingProjectilesSprites[0];
            yield return Timing.WaitForSeconds(1f);
            for (int i = 0; i < NumberOfProjectilesToVomit; i++)
            {
                ShootProjectile();
                yield return Timing.WaitForSeconds(1f);
            }
            
            yield return Timing.WaitForSeconds(1f);
            AnimationOperator.enabled = true;
            ActionState = BossActionState.Idle;
            MovementState = BossMovementState.FollowPlayer;
            VomittingProjectiles = false;

            void ShootProjectile()
            {
                PlayerOperator player = GetClosestPlayer();
                if (player == null)
                {
                    return;
                }

                string pool = ProjectileToVomitPrefab.GetComponent<Projectile>().DeathPoolName;
                GameObject projectileGameObject;
                if (!string.IsNullOrEmpty(pool) && Pool.Has(pool))
                {
                    projectileGameObject = Pool.Get(pool);
                }
                else
                {
                    projectileGameObject = Instantiate(ProjectileToVomitPrefab);
                }

                projectileGameObject.SetActive(true);
                projectileGameObject.transform.position = transform.position;
                Vector3 vectorToTarget = player.transform.position + new Vector3(0f, Random.Range(7f, 14f), 0f) - projectileGameObject.transform.position;
                float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
                Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
                projectileGameObject.transform.rotation = q;
  
            }
        }

        protected IEnumerator<float> ActivateLayout(Tilemap deactivate, Tilemap activate)
        {
            Corrupter.Intensity += 0.5f;
            yield return Timing.WaitForSeconds(0.5f);
            deactivate.gameObject.SetActive(false);
            activate.gameObject.SetActive(true);
            yield return Timing.WaitForSeconds(0.5f);
            Corrupter.Intensity -= 0.5f;
            GetComponent<EssenceDropletSpawnPoint>().Spawn(true);
        }

        protected IEnumerator<float> AITick()
        {
            yield return Timing.WaitUntilDone(() => !MainPlayerOperator.MainPlayerComponent.InEvent);

            while (isActiveAndEnabled && !Dead)
            {
                yield return Timing.WaitForSeconds(Random.Range(6f, 12f));
                if (MainPlayerOperator.MainPlayerComponent.Hold)
                {
                    ActionState = BossActionState.VomittingProjectiles;
                }
                else
                {
                    if (Random.value < 0.75f)
                    {
                        ActionState = Random.value < 0.25f ? BossActionState.RotatePosition : BossActionState.VomittingProjectiles;
                    }
                    else
                    {
                        ActionState = Random.value < 0.5f ? BossActionState.SpawningEnemy : BossActionState.VomittingProjectiles;
                    }
                }

                yield return Timing.WaitUntilDone(() => ActionState == BossActionState.Idle);
            }
        }

        public override void Die()
        {
            Dead = true;
            Timing.KillCoroutines(GetInstanceID().ToString());
            ActionState = BossActionState.Idle;
            GetComponent<BossDeath>().Activate();
            TearDeathAnimation.Play(gameObject, DeathSprite, Alignment);
            base.Die();
        }
    }
}
