﻿
namespace Worldline.NPCs.Enemies.Tears
{
    public enum BossActionState
    {
        Idle,
        SpawningEnemy,
        RotatePosition,
        SpawningProjectiles,
        VomittingProjectiles
    }
}
