﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.GUI.HUD;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.NPCs
{
    /// <summary>
    /// The Mimiturge Tear is unique in the sense that it doesn't inherit 
    /// from the Tear class, but rather the PlayerOperator class, in order
    /// to ensure that when it mimics the actual player's actions, it is
    /// as accurate as possible. Because of this, some code from the Tear
    /// class that doesn't exist inside the PlayerOperator class must also
    /// be implemented here.
    /// </summary>
    public class Mimiturge : PlayerOperator
    {
        [Tooltip("The distance to a player before the Mimiturge starts moving. Set to -1 for always move.")]
        public float ActivateDistance = 10f;
        protected EssenceDropletSpawnPoint EssenceDropletSpawnPoint { get; set; }

        protected override void Awake()
        {
            base.Awake();
            EssenceDropletSpawnPoint = GetComponent<EssenceDropletSpawnPoint>();
            TearDeathAnimation.AddSpriteToCache(PlayerSpriteBundle.Dead[0], true);
        }

        protected override void Update()
        {
            base.Update();
            if (!CombatantTimeOperator.ShouldHold && (ActivateDistance < 0f || Vector2.Distance(transform.position, MainPlayerOperator.MainPlayerObject.transform.position) < ActivateDistance))
            {
                Crouch = MainPlayerOperator.MainPlayerComponent.Crouch;
                ShouldJump = MainPlayerOperator.MainPlayerComponent.ShouldJump;
                WalkLeft = MainPlayerOperator.MainPlayerComponent.WalkRight;
                WalkRight = MainPlayerOperator.MainPlayerComponent.WalkLeft;
            }
            else
            {
                Crouch = false;
                ShouldJump = false;
                WalkLeft = false;
                WalkRight = false;
            }
        }

        public override void MakeInvulnerable()
        {
            if (Invulnerable)
            {
                return;
            }

            Invulnerable = true;
            Timing.RunCoroutine(MakeVulnerable(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> MakeVulnerable()
            {
                yield return Timing.WaitForSeconds(InvincibilityTime);
                Invulnerable = false;
            }
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);
            TextPopupController.Main.ShowDamagePopup(damage, transform.position);
        }

        public override void Die()
        {
            EssenceDropletSpawnPoint.Spawn(true);
            var anim = TearDeathAnimation.Play(gameObject, PlayerSpriteBundle.Dead[0], Alignment);
            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Rewind)
            {   
                anim.GetComponent<TimeOperator>().enabled = false;
            }

            base.Die();
        }
    }
}
