﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Concourse : Tear
    {
        [Header("Concourse")]
        public float Speed = 8f;
        public float SpeedVariance = 1f;
        public float ActivateDistance = 16f;
        public float DistanceFromPlayer = 7f;
        public Concourse[] AlsoActivates;
        public bool Activated { get; set; }
        public bool AttackingPlayer { get; protected set; }
        public float NormalAnimationDelay { get; protected set; }
        public Vector2? AttackingPosition { get; protected set; }

        protected virtual void OnEnable()
        {
            NormalAnimationDelay = AnimationOperator.Delay;
        }

        protected override void Update()
        {
            base.Update();

            if (CombatantTimeOperator.ShouldHold || MainPlayerOperator.MainPlayerObject == null) return;

            SpriteRenderer.flipY = Mathf.Abs(transform.rotation.z) > 0.5f;
            AnimationOperator.Delay = Rigidbody.velocity.y > 0f ? NormalAnimationDelay : int.MaxValue;
            transform.right = Rigidbody.velocity;

            if (Activated)
            {
                if (!AttackingPosition.HasValue)
                {
                    Vector2 playerPosition = GetClosestPlayer().transform.position;
                    if (transform.position.y > playerPosition.y)
                    {
                        AttackingPosition = playerPosition + new Vector2(DistanceFromPlayer, -DistanceFromPlayer);
                    }
                    else
                    {
                        AttackingPosition = playerPosition + new Vector2(DistanceFromPlayer, DistanceFromPlayer);
                    }
                }

                if (AttackingPosition.HasValue && Vector2.Distance(AttackingPosition.Value, transform.position) < 4f)
                {
                    if (AlsoActivates != null)
                    {
                        foreach (Concourse c in AlsoActivates)
                        {
                            c.Activated = true;
                        }

                        AlsoActivates = null;
                    }

                    if (!AttackingPlayer)
                    {
                        AttackingPosition = GetClosestPlayer().transform.position;
                        AttackingPlayer = true;
                    }
                    else
                    {
                        Vector2 playerPosition = GetClosestPlayer().transform.position;
                        if (transform.position.y > playerPosition.y)
                        {
                            AttackingPosition = playerPosition + new Vector2(DistanceFromPlayer, -DistanceFromPlayer);
                        }
                        else
                        {
                            AttackingPosition = playerPosition + new Vector2(DistanceFromPlayer, DistanceFromPlayer);
                        }

                        AttackingPlayer = false;
                    }
                }

                if (AttackingPosition.HasValue)
                {
                    MovementOperator.Speed = new Vector2(AttackingPosition.Value.x > transform.position.x ? Speed : -Speed, AttackingPosition.Value.y > transform.position.y ? Speed : -Speed);
                    MovementOperator.Speed += new Vector2(Random.Range(-SpeedVariance, 0f), Random.Range(-SpeedVariance, 0f));
                }
            }
            else if (Vector2.Distance(GetClosestPlayer().transform.position, transform.position) < ActivateDistance)
            {
                Activated = true;
            }
        }
    }
}
