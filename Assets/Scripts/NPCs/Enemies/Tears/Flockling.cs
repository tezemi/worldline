﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Flockling : Tear
    { 
        [Header("Flockling")]
        [Tooltip("If true, this Flockling will idle in a circle instead of just up and down.")]
        public bool CircleIdle;
        [Tooltip("If true, this Flockling will immediately attack the player when it is enabled.")]
        public bool TargetPlayerOnEnable;
        [Tooltip("If CircleIdle is true, this is the size of the idle's radius.")]
        public float CircleRadius = 3.5f;
        [Tooltip("If AttacksWithProjectile is true, the Flockling will shoot this projectile.")]
        public GameObject ProjectilePrefab;
        /// <summary>
        /// Whether or not this Flockling is moving up in its idle movement.
        /// </summary>
        public bool MovingUp { get; set; }
        /// <summary>
        /// If true, this Flockling will not attack the player or any enemies.
        /// </summary>
        public bool DisableAggression { get; set; }
        /// <summary>
        /// If true, the Flockling will ignore whether or not the enemy is outside 
        /// of its normal range, and it will just attack. This gets set to true
        /// internally when the Flockling is attacked by something while it is idle.
        /// </summary>
        public bool IgnoreLeaveRange { get; set; }
        /// <summary>
        /// If true, the Flockling will always face the nearest enemy instead 
        /// of the direction it is moving.
        /// </summary>
        public bool AlwaysFaceTarget { get; set; }
        /// <summary>
        /// The speed of the default idle movement.
        /// </summary>
        public float IdleSpeed { get; set; } = 1;
        /// <summary>
        /// The speed the Flockling has when it is attacking the player.
        /// </summary>
        public float AttackSpeed { get; set; } = 4;
        /// <summary>
        /// The y-position of the Flockling hovers around while it is idling.
        /// </summary>
        public float HoverPoint { get; set; }
        /// <summary>
        /// The speed the Flockling moves in a circle if CircleIdle is true.
        /// </summary>
        public float CircleSpeed { get; set; } = 0.43f;
        /// <summary>
        /// If not null, the Flockling will fly towards the specified point.
        /// </summary>
        public Vector3? TargetPoint { get; set; }
        public const bool AttacksWithProjectiles = false;
        /// <summary>
        /// The number of projectiles that will be fired if/when the Flockling 
        /// fires its projectiles.
        /// </summary>
        public const int NumberOfProjectiles = 3;
        /// <summary>
        /// The range around HoverPoint the Flockling will move while it is idling.
        /// </summary>
        public const float FloatRange = 0.75f;
        /// <summary>
        /// The distance close enough to the Flockling an enemy must get for the 
        /// Flocking to start to attack it.
        /// </summary>
        public const float EnterRange = 6;
        /// <summary>
        /// While the Flockling is attacking an enemy, this is the distance the 
        /// enemy must get away from the Flockling for the Flockling to stop 
        /// attacking.
        /// </summary>
        public const float LeaveRange = 10;
        /// <summary>
        /// The animation speed of the Flockling's idle animation.
        /// </summary>
        public const float AnimationIdleSpeed = 0.15f;
        /// <summary>
        /// The animation speed of the Flockling's attack animation.
        /// </summary>
        public const float AnimationAttackSpeed = 0.1f;
        /// <summary>
        /// The time an enemy must be outside of the Flockling's range for the 
        /// Flockling to stop attacking, assuming the attacker hurt the Flockling
        /// while it was idle.
        /// </summary>
        public const float IgnoreLeaveRangeTime = 3;
        /// <summary>
        /// The amount of rotation that is applied to a Flockling based on its velocity.
        /// </summary>
        public const float RotationScale = 8f;
        /// <summary>
        /// A variance in speed that is randomly applied each frame.
        /// </summary>
        public const float Variance = 0.75f;
        /// <summary>
        /// The X force applied to the Flockling's projectiles when they are created.
        /// </summary>
        public const float ProjectileXForce = 17f;
        /// <summary>
        /// The Y force applied to the Flockling's projectiles when they are created.
        /// </summary>
        public const float ProjectileYForce = 10f;
        /// <summary>
        /// If AttacksWithProjectile is true, this is the minimum amount of time
        /// between it attacks it could possibly take.
        /// </summary>
        public const float ProjectileAttackIntervalMin = 3.5f;
        /// <summary>
        /// If AttacksWithProjectile is true, this is the maximum amount of time
        /// between it attacks it could possibly take.
        /// </summary>
        public const float ProjectileAttackIntervalMax = 6.5f;
        public const string ProjectilePool = "FlocklingProjectiles";
        private bool _recentlyFlippedSprite;
        private int _framesSinceLastFlippedSprite;
        private float _angle;
        private GameObject _targetGameObject;
        private readonly Vector2[] _projectileVelocities = new Vector2[NumberOfProjectiles];

        /// <summary>
        /// The current GameObject the Flockling is attacking.
        /// </summary>
        public GameObject TargetGameObject
        {
            get => _targetGameObject;
            set
            {
                if (DisableAggression && value != null)
                {
                    _targetGameObject = null;
                    return;
                }

                GetComponent<AnimationOperator>().Delay = value != null ? AnimationAttackSpeed : AnimationIdleSpeed;
                _targetGameObject = value;
            }
        }
        
        protected virtual void OnEnable()
        {
            HoverPoint = transform.position.y;
            if (TargetPlayerOnEnable)
            {
                AttackPlayer();
            }

            if (AttacksWithProjectiles && ProjectilePrefab != null)
            {
                Timing.RunCoroutine(AttackInterval(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            base.Update();

            if (GetClosestPlayer() == null || GetComponent<TimeOperator>().ShouldHold) return;

            GameObject player = GetClosestPlayer().gameObject;
            float px = player.transform.position.x + Random.Range(-Variance, Variance);
            float py = player.transform.position.y + Random.Range(-Variance, Variance);
            if (TargetPoint == null && TargetGameObject == null)
            {
                RaycastHit2D flocklingCanNotSeeTarget = Physics2D.Linecast
                (
                    transform.position,
                    player.transform.position,
                    player.GetComponent<MovementOperator>().GroundDetectionLayerMask
                );

                if (TargetGameObject == null && 
                Mathf.Abs(px - transform.position.x) < EnterRange && 
                Mathf.Abs(py - transform.position.y) < EnterRange && 
                !flocklingCanNotSeeTarget || IgnoreLeaveRange)
                {
                    TargetGameObject = player;               
                }

                if (!CircleIdle)
                {
                    if (MovingUp && transform.position.y > HoverPoint + FloatRange)
                    {
                        MovingUp = false;
                    }

                    if (!MovingUp && transform.position.y < HoverPoint - FloatRange)
                    {
                        MovingUp = true;
                    }

                    MovementOperator.Speed = new Vector2(0, MovingUp ? IdleSpeed : -IdleSpeed);
                }
                else
                {
                    _angle += CircleSpeed * Time.deltaTime;
                    MovementOperator.Speed = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * CircleRadius;
                }
            }
            else
            {
                float x = px > transform.position.x ? AttackSpeed : -AttackSpeed;
                float y = py > transform.position.y ? AttackSpeed : -AttackSpeed;
                if (TargetPoint != null)
                {
                    x = TargetPoint.Value.x > transform.position.x ? AttackSpeed : -AttackSpeed;
                    y = TargetPoint.Value.y > transform.position.y ? AttackSpeed : -AttackSpeed;
                }

                MovementOperator.Speed = new Vector2(x, y);
                if (!IgnoreLeaveRange && (Mathf.Abs(px - transform.position.x) > LeaveRange || Mathf.Abs(py - transform.position.y) > LeaveRange))
                {
                    HoverPoint = transform.position.y;
                    TargetGameObject = null;
                }
            }

            // In order to prevent a rapid "flashing" effect when the Flockling changes direction
            // while attacking or flying at something, we only allow the sprite to be flipped
            // every 75 frames.
            bool tryFlipSprite = AlwaysFaceTarget ? px < transform.position.x : MovementOperator.Speed.x < 0f;
            if (!_recentlyFlippedSprite && SpriteRenderer.flipX != tryFlipSprite)
            {
                SpriteRenderer.flipX = tryFlipSprite;
                _recentlyFlippedSprite = true;
                _framesSinceLastFlippedSprite = 0;
            }
            else
            {
                const int framesTilFlipAgain = 75;
                if (_framesSinceLastFlippedSprite++ > framesTilFlipAgain)
                {
                    _recentlyFlippedSprite = false;
                }
            }

            transform.rotation = Quaternion.Euler(0f, 0f, Rigidbody.velocity.x * RotationScale * (Rigidbody.velocity.y / AttackSpeed));
        }

        protected virtual IEnumerator<float> AttackInterval()
        {
            while (AttacksWithProjectiles)
            {
                float waitTime = Random.Range(ProjectileAttackIntervalMin, ProjectileAttackIntervalMax);
                yield return Timing.WaitUntilDone(() => TargetGameObject != null);
                yield return this.WaitForFixedSecondsScaled(waitTime);
                yield return Timing.WaitUntilDone(() => TargetGameObject != null && transform.position.y > TargetGameObject.transform.position.y);
                AlwaysFaceTarget = true;
                TargetPoint = transform.position;
                yield return this.WaitForFixedSecondsScaled(0.5f);
                for (int i = 0; i < _projectileVelocities.Length; i++)
                {
                    _projectileVelocities[i] = new Vector2
                    (
                        TargetGameObject.transform.position.x > transform.position.x ?
                        Random.Range(ProjectileXForce / 2f, ProjectileXForce) :
                        Random.Range(-ProjectileXForce, -ProjectileXForce / 2f),
                        Random.Range(ProjectileYForce / 2f, ProjectileYForce)
                    );
                }

                GameObject[] bullets = FireProjectiles
                (
                    ProjectilePrefab, 
                    ProjectilePool, 
                    _projectileVelocities, 
                    NumberOfProjectiles
                );

                foreach (GameObject bullet in bullets)
                {
                    bullet.GetComponent<Rigidbody2D>().AddTorque(ProjectileXForce, ForceMode2D.Impulse);
                }

                yield return this.WaitForFixedSecondsScaled(0.25f);
                TargetPoint = null;
                TemporarilyIgnoreLeaveRange();
                AlwaysFaceTarget = false;
            }
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);

            // If the enemy gets hurt outside of their normal attack range, they attack the player regardless, and
            // will pursue them for a few seconds regardless of distance.
            if (TargetGameObject == null)
            {
                TemporarilyIgnoreLeaveRange();
            }
        }

        /// <summary>
        /// Causes the Flockling to temporarily ignore the maximum attack range, 
        /// and to start attacking the nearest enemy for IgnoreLeaveRangeTime in
        /// seconds.
        /// </summary>
        public void TemporarilyIgnoreLeaveRange()
        {
            IgnoreLeaveRange = true;
            if (isActiveAndEnabled)
            {
                Timing.RunCoroutine(UnsetIgnore(), Segment.FixedUpdate, GetInstanceID().ToString());
            }

            IEnumerator<float> UnsetIgnore()
            {
                yield return Timing.WaitForSeconds(IgnoreLeaveRangeTime);
                IgnoreLeaveRange = false;
            }
        }

        /// <summary>
        /// Causes the Flockling to immediately target the main player.
        /// </summary>
        public void AttackPlayer()
        {
            TargetGameObject = Player.MainPlayerOperator.MainPlayerObject;
            IgnoreLeaveRange = true;
        }
    }
}
