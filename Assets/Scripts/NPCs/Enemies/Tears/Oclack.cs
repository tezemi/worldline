﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using MEC;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Oclack : Tear
    {
        [Header("Oclack")]
        public bool CanAttackWhileMoving = true;
        public int AmountOfProjectiles = 1;
        public float TimeBetweenShots = 0.1f;
        public Sprite FingerGun;
        public Sprite ClosedSprite;
        public Sprite AttackingSprite;
        public GameObject ProjectilePrefab;
        public bool Shooting { get; protected set; }
        public const float Speed = 3;
        public const float LookSpeed = 0.2f;
        public const float TimeBeforeShoot = 5f;
        public const float TimeTilShoot = 1f;
        public const float ShakeIntensity = 0.05f;
        public const float RotationOffset = 86f;
        public const float StartRotation = -90f;
        public const string ProjectilePool = "OclackShootingProjectilePool";
        private bool _attacking;
        private bool _killedMainBehavior;
        private Vector3 _shootPosition;
        private Vector3 _hoverPosition;

        public bool Attacking
        {
            get => _attacking;
            protected set
            {
                if (Shooting)
                {
                    _attacking = false;
                    return;
                }

                if (_attacking != value && !value)
                {
                    _hoverPosition = transform.position;
                }

                _attacking = value;
            }
        }

        protected virtual void OnEnable()
        {
            _hoverPosition = transform.position;
            Timing.RunCoroutine(ShootInterval(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            base.Update();
            
            if (CombatantTimeOperator.TimeScale < 0f)
            {
                Timing.KillCoroutines(GetInstanceID().ToString());
                Shooting = false;
                _killedMainBehavior = true;
                return;
            }

            if (_killedMainBehavior)
            {
                Timing.RunCoroutine(ShootInterval(), Segment.FixedUpdate, GetInstanceID().ToString());
                _killedMainBehavior = false;
            }

            Animate();                  // Sets animation based on behavior
            DetermineIfShouldAttack();  // Sets Attacking depending on playing position
            Move();                     // Moves based on behavior
        }

        protected virtual void DetermineIfShouldAttack()
        {
            if (CombatantTimeOperator.ShouldHold || !CameraOperator.ActiveCamera.OnScreen(gameObject)) return;

            if (PlayerWeaponOperator.Main.isActiveAndEnabled)
            {
                if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x >
                transform.position.x && !PlayerWeaponOperator.Main.GetComponent<SpriteRenderer>().flipY)
                {
                    Attacking = true;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x >
                transform.position.x && PlayerWeaponOperator.Main.GetComponent<SpriteRenderer>().flipY)
                {
                    Attacking = false;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x &&
                PlayerWeaponOperator.Main.GetComponent<SpriteRenderer>().flipY)
                {
                    Attacking = true;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x &&
                !PlayerWeaponOperator.Main.GetComponent<SpriteRenderer>().flipY)
                {
                    Attacking = false;
                }
            }
            else
            {
                if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x > transform.position.x &&
                !Player.MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX)
                {
                    Attacking = true;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x > transform.position.x &&
                Player.MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX)
                {
                    Attacking = false;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x &&
                Player.MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX)
                {
                    Attacking = true;
                }
                else if (Player.MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x &&
                !Player.MainPlayerOperator.MainPlayerObject.GetComponent<SpriteRenderer>().flipX)
                {
                    Attacking = false;
                }
            }
        }

        protected virtual void Animate()
        {
            if (CombatantTimeOperator.ShouldHold) return;

            if (Shooting)
            {
                GetComponent<SpriteRenderer>().sprite = FingerGun;
            }
            else if (Attacking)
            {
                GetComponent<SpriteRenderer>().sprite = AttackingSprite;
            }
            else
            {
                GetComponent<SpriteRenderer>().sprite = ClosedSprite;
            }
        }

        protected virtual void Move()
        {
            GameObject player = GetClosestPlayer().gameObject;
            float px = player.transform.position.x;
            float py = player.transform.position.y;
            GetComponent<SpriteRenderer>().flipX = px < transform.position.x || Shooting;
            if (Attacking && !CombatantTimeOperator.ShouldHold && CameraOperator.ActiveCamera.OnScreen(gameObject))
            {
                // Move towards player
                float x = px > transform.position.x ? Speed : -Speed;
                float y = py > transform.position.y ? Speed : -Speed;
                GetComponent<MovementOperator>().Speed = new Vector2(x, y);
                RotateTowards(Quaternion.identity);
                Invulnerable = false;
            }
            else
            {
                // Otherwise, don't move...
                if (!Shooting) 
                {
                    Invulnerable = true; // If not shooting, be invulnerable, hover, default rotation
                    MovementOperator.Speed = new Vector2(0f, transform.position.y > _hoverPosition.y ? -2f : 2f);
                    RotateTowards(Quaternion.identity);
                }
                else
                {
                    Invulnerable = false;
                    MovementOperator.Speed = Vector2.zero;

                    // If shooting, set rotation to look at the player with lerp based on timescale
                    Quaternion targetRotation = Quaternion.LookRotation
                    (
                        (Vector2)player.transform.position - (Vector2)transform.position, 
                        Vector3.forward
                    );

                    targetRotation = Quaternion.Euler
                    (
                        targetRotation.eulerAngles.x, 
                        targetRotation.eulerAngles.y, 
                        targetRotation.eulerAngles.z + RotationOffset
                    );

                    RotateTowards(targetRotation);

                    if (!CombatantTimeOperator.ShouldHold)
                    {
                        transform.position = _shootPosition + new Vector3 // Shake it up!
                        (
                            Random.Range(-ShakeIntensity, ShakeIntensity),
                            Random.Range(-ShakeIntensity, ShakeIntensity),
                            0f
                        );
                    }
                }
            }
        }

        protected virtual IEnumerator<float> ShootInterval()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitUntilDone(() => CanAttackWhileMoving || !Attacking);
                yield return this.WaitForFixedSecondsScaled(TimeBeforeShoot);
                if ((CanAttackWhileMoving || !Attacking) && CombatantTimeOperator.TimeScale > 0f && CameraOperator.ActiveCamera.OnScreen(gameObject))
                {
                    Shooting = true;
                    transform.rotation = Quaternion.Euler
                    (
                        transform.rotation.eulerAngles.x,
                        transform.rotation.eulerAngles.y,
                        StartRotation
                    );

                    _shootPosition = transform.position;
                    yield return this.WaitForFixedSecondsScaled(TimeTilShoot);
                    for (int i = 0; i < AmountOfProjectiles; i++)
                    {
                        FireProjectileAtPlayer(ProjectilePrefab, ProjectilePool);
                        yield return this.WaitForFixedSecondsScaled(TimeBetweenShots);
                        if (CombatantTimeOperator.TimeScale < 0f)
                        {
                            Shooting = false;
                            break;
                        }
                    }

                    yield return this.WaitForFixedSecondsScaled(TimeBetweenShots);
                    yield return Timing.WaitForSeconds(0.25f);
                    Shooting = false;
                    transform.rotation = Quaternion.Euler
                    (
                        transform.rotation.eulerAngles.x,
                        transform.rotation.eulerAngles.y,
                        transform.rotation.eulerAngles.z + RotationOffset
                    );
                }
            }
        }

        public void RotateTowards(Quaternion targetRotation)
        {
            targetRotation.x = 0f;      // 2D rotation only...
            targetRotation.y = 0f;
            float realSpeed = CombatantTimeOperator.GetScaledValue(LookSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, realSpeed);
        }
    }
}
