﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using Worldline.TimeEngine.Recording;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.NPCs.Enemies.Tears
{
    [HasDefaultState]
    public class MimiturgeWeaponOperator : WeaponOperator
    {
        public static List<MimiturgeWeaponOperator> MimiturgeWeaponOperators { get; private set; } = new List<MimiturgeWeaponOperator>();
        public Vector3 LeftOffset;
        public Vector3 RightOffset;
        public GameObject ProjectilePrefab;
        public const float RecoilAmount = 0.1f;

        protected override void Awake()
        {
            base.Awake();
            MimiturgeWeaponOperators.Add(this);
        }

        protected virtual void OnDestroy()
        {
            MimiturgeWeaponOperators.Remove(this);
        }

        protected override void Update()
        {
            base.Update();

            // Stop if game is paused, or time effects, or holding player
            if (Disabled || LevelController.Main.Paused ||
            WeaponOperatorTimeOperator.ShouldHold || Recorder.Mode == RecorderMode.Playback)
            {
                return;
            }

            // Aim and positions sprites. Weapon always mirrors the main player's weapon...
            Quaternion q = Quaternion.Euler
            (
                PlayerWeaponOperator.Main.transform.rotation.eulerAngles.x,
                PlayerWeaponOperator.Main.transform.rotation.eulerAngles.y,
                180f - PlayerWeaponOperator.Main.transform.rotation.eulerAngles.z
            );

            Position
            (
                q,
                LeftOffset,
                RightOffset,
                Vector3.zero,
                Vector3.zero,
                Vector3.zero,
                Vector3.zero,
                RecoilAmount
            );
        }

        public virtual void FireMimiturgeProjectile()
        {
            if (Disabled || LevelController.Main.Paused ||
            WeaponOperatorTimeOperator.ShouldHold || Recorder.Mode == RecorderMode.Playback)
            {
                return;
            }

            GameObject projectileInstance;
            if (Pool.Has(ProjectilePrefab.name))
            {
                projectileInstance = Pool.Get(ProjectilePrefab.name);
            }
            else
            {
                projectileInstance = Instantiate(ProjectilePrefab);
            }

            projectileInstance.SetActive(true);
            projectileInstance.transform.position = transform.position;
            projectileInstance.transform.rotation = transform.rotation;
            FireProjectile(projectileInstance, null);
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private void SetupDefaults()
        {
            MimiturgeWeaponOperators = new List<MimiturgeWeaponOperator>();
        }
    }
}
