﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.Player;
using Worldline.Physics;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.NPCs.Enemies.Tears
{
    public class AugliteHat : Tear
    {
        public Sprite IdleSprite;
        public Sprite HurtSprite;
        public Sprite AttackSprite;
        public GameObject Body;
        public bool HurtAnimationOn { get; set; }
        public const float Speed = 4f;
        public const float Offset = 0.7f;
        public const float HurtAnimationTime = 0.5f;
        protected CoroutineHandle HurtHandle { get; set; }

        protected override void Awake()
        {
            base.Awake();
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected override void Update()
        {
            base.Update();

            if (CombatantTimeOperator.ShouldHold) return;

            if (Body != null && !Body.GetComponent<AugliteBody>().Dead && !Body.GetComponent<AugliteBody>().Invulnerable)
            {
                GetComponent<MovementOperator>().Speed = new Vector2
                (
                    Body.transform.position.x > transform.position.x ? Speed : -Speed, 
                    Body.transform.position.y + Offset > transform.position.y ? Speed : -Speed
                );

                SpriteRenderer.sprite = HurtAnimationOn ? HurtSprite : IdleSprite;
            }
            else if (MainPlayerOperator.MainPlayerObject != null)
            {
                GetComponent<MovementOperator>().Speed = new Vector2
                (
                    MainPlayerOperator.MainPlayerObject.transform.position.x > transform.position.x ? Speed : -Speed, 
                    MainPlayerOperator.MainPlayerObject.transform.position.y > transform.position.y ? Speed : -Speed
                );

                SpriteRenderer.sprite = AttackSprite;
            }

            if (GetComponent<TimeOperator>().CurrentTimeEffect != TimeEffect.Pause && 
            MainPlayerOperator.MainPlayerObject != null)
            {
                GetComponent<SpriteRenderer>().flipX = MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x;
            }
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);
            HurtAnimationOn = true;
            Timing.KillCoroutines(HurtHandle);
            HurtHandle = Timing.RunCoroutine(DisableHurtAnimation());

            IEnumerator<float> DisableHurtAnimation()
            {
                yield return this.WaitForFixedSecondsScaled(HurtAnimationTime);
                HurtAnimationOn = false;
            }
        }
    }
}
