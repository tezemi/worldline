﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Player;
using Worldline.GUI.HUD;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Combat.Projectiles;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.NPCs.Enemies.Tears
{
    public class AugliteBody : Tear
    {
        [Header("Auglite Body")]
        public Sprite[] IdleSprite;
        public Sprite[] JumpSprite;
        public Sprite SummonSprite1;
        public Sprite SummonSprite2;
        public Sprite SummonSprite3;
        public Sprite[] CowerSprite;
        public GameObject Hat;
        public GameObject Projectile;
        public List<AugliteBorder> AugliteBorders;
        public AugliteState State { get; protected set; }
        public const float TimeBetweenJumps = 0.3f;
        public const float JumpsBetweenAttacks = 1;
        public const float Speed = 2.5f;
        public const float SlowSpeed = 0.5f;
        public const float DistanceFromPlayer = 8f;
        public const float CowerShakeIntensity = 0.05f;
        public const float DelayBeforeShoot = 0.5f;
        public const float SummonAnimationSpeed = 0.075f;
        public const float DelayAfterShoot = 0.2f;
        public const float TimeInvulnerable = 4f;
        public const string ProjectilePool = "AugliteProjectilePool";
        private bool _killedCoroutines;
        private CoroutineHandle _mainBehaviorHandle;

        protected override void Awake()
        {
            base.Awake();
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        protected virtual void OnEnable()
        {
            _mainBehaviorHandle = Timing.RunCoroutine(MainBehavior(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            base.Update();
            
            if (CombatantTimeOperator.ShouldHold)
            {
                State = AugliteState.Idle;
                Invulnerable = false;
                Timing.KillCoroutines(GetInstanceID().ToString());
                _killedCoroutines = true;
            }
            else if (_killedCoroutines)
            {
                Timing.RunCoroutine(MainBehavior(), Segment.FixedUpdate, GetInstanceID().ToString());
                _killedCoroutines = false;
            }

            // Face player, set color
            if (MainPlayerOperator.MainPlayerObject != null && !GetComponent<TimeOperator>().ShouldHold)
            {
                if (State != AugliteState.Cowering && Hat != null && !Hat.GetComponent<AugliteHat>().Dead)
                {
                    SpriteRenderer.flipX = MainPlayerOperator.MainPlayerObject.transform.position.x < transform.position.x;
                }

                if (State == AugliteState.Cowering && Hat != null && !Hat.GetComponent<AugliteHat>().Dead)
                {
                    SpriteRenderer.color = new Color
                    (
                        SpriteRenderer.color.r, 
                        SpriteRenderer.color.g, 
                        SpriteRenderer.color.b, 
                        Random.Range(0.45f, 0.65f)
                    );
                }
                else
                {
                    SpriteRenderer.color = new Color
                    (
                        SpriteRenderer.color.r,
                        SpriteRenderer.color.g,
                        SpriteRenderer.color.b,
                        1f
                    );
                }
            }

            switch (State)
            {
                case AugliteState.Idle:
                    SetAnimation(IdleSprite);
                    break;
                case AugliteState.Jumping:
                    SetAnimation(JumpSprite);
                    break;
                case AugliteState.Cowering:
                    SetAnimation(CowerSprite);
                    break;
            }

            // Kill out of bounds
            if (transform.position.y < LevelController.Main.MapHeightMin)
            {
                Die();
            }
        }

        protected virtual IEnumerator<float> MainBehavior()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForOneFrame;
                for (int i = 0; i < JumpsBetweenAttacks; i++)
                {
                    yield return this.WaitForFixedSecondsScaled(TimeBetweenJumps);

                    if (State == AugliteState.Idle)
                    {
                        State = AugliteState.Jumping;
                        Jump();
                        yield return Timing.WaitForSeconds(0.5f);
                        yield return Timing.WaitUntilDone(new WaitUntil(() => MovementOperator.Grounded));
                        MovementOperator.Speed = new Vector2(0f, 0f);
                        State = AugliteState.Idle;
                    }
                }

                yield return this.WaitForFixedSecondsScaled(TimeBetweenJumps);

                var raycastToNearPlayer = Physics2D.Linecast(transform.position, GetClosestPlayer().transform.position, MovementOperator.GroundDetectionLayerMask);
                if (State == AugliteState.Idle && !raycastToNearPlayer && Vector2.Distance(transform.position, GetClosestPlayer().transform.position) < 15f)
                {
                    State = AugliteState.Attacking;
                    SetAnimation(new[] { SummonSprite1 } );
                    yield return this.WaitForFixedSecondsScaled(DelayBeforeShoot);
                    GameObject projectile = FireProjectile(Projectile, ProjectilePool, Vector2.zero, -0.15f);
                    loopShootAnim:
                    yield return this.WaitForFixedSecondsScaled(SummonAnimationSpeed);
                    SetAnimation(new[] { SummonSprite2 });
                    yield return this.WaitForFixedSecondsScaled(SummonAnimationSpeed);
                    SetAnimation(new[] { SummonSprite3 });
                    if (projectile != null && !projectile.GetComponent<Projectile>().Dead &&
                    projectile.GetComponent<Projectile>().isActiveAndEnabled)
                    {
                        goto loopShootAnim;
                    }

                    SetAnimation(new[] { SummonSprite1 });
                    yield return this.WaitForFixedSecondsScaled(DelayAfterShoot);
                    State = AugliteState.Idle;
                }
            }
        }

        protected virtual IEnumerator<float> Shake()
        {
            Vector3 startPosition = transform.position;
            start:
            yield return this.WaitForFixedSecondsScaled(Time.fixedDeltaTime);
            transform.position = new Vector3(startPosition.x, transform.position.y, transform.position.z);
            transform.position += new Vector3(Random.Range(-CowerShakeIntensity, CowerShakeIntensity), 0f, 0f);
            if (isActiveAndEnabled && Invulnerable)
            {
                goto start;
            }
        }

        protected virtual IEnumerator<float> Cower()
        {
            State = AugliteState.Cowering;
            Invulnerable = true;
            yield return this.WaitForFixedSecondsScaled(TimeInvulnerable);
            Invulnerable = false;
            State = AugliteState.Idle;
            _mainBehaviorHandle = Timing.RunCoroutine(MainBehavior(), GetInstanceID().ToString());
        }

        protected virtual void Jump()
        {
            MovementOperator.Jump();
            bool skipOtherChecks = false;
            if (AugliteBorders != null)
            {
                foreach (AugliteBorder a in AugliteBorders)
                {
                    if (a.transform.position.x > transform.position.x && a.GetComponent<BoxCollider2D>().bounds.min.x - transform.position.x < DistanceFromPlayer)
                    {
                        MovementOperator.Speed = new Vector2(-Speed, 0);
                        skipOtherChecks = true;
                        break;
                    }

                    if (a.transform.position.x < transform.position.x && transform.position.x - a.GetComponent<BoxCollider2D>().bounds.max.x < DistanceFromPlayer)
                    {
                        MovementOperator.Speed = new Vector2(Speed, 0);
                        skipOtherChecks = true;
                        break;
                    }
                }
            }

            if (!skipOtherChecks)
            {
                if (Vector2.Distance(GetClosestPlayer().transform.position, transform.position) < DistanceFromPlayer)
                {
                    if (transform.position.x < DistanceFromPlayer)
                    {
                        MovementOperator.Speed = new Vector2(Speed, 0);
                    }
                    else if (LevelController.Main.MapHeight - transform.position.x < DistanceFromPlayer)
                    {
                        MovementOperator.Speed = new Vector2(-Speed, 0);
                    }
                    else
                    {
                        MovementOperator.Speed = new Vector2(GetClosestPlayer().transform.position.x > transform.position.x ? -Speed : Speed, 0);
                    }
                }
                else
                {
                    MovementOperator.Speed = new Vector2(Random.value > 0.5f ? -0.5f : Random.Range(SlowSpeed, -SlowSpeed), 0);
                }
            }
        }

        public override void Hurt(int damage, object other)
        {
            if (Invulnerable) return;

            Health -= damage;
            TextPopupController.Main.ShowDamagePopup(damage, transform.position);
            if (Health <= 0)
            {
                Die();
            }
            else
            {
                CombatantTimeOperator.Hurt(damage, other);
            }

            if (!Invulnerable && Hat != null && !Hat.GetComponent<Combatant>().Dead)
            {
                Timing.RunCoroutine(Cower(), GetInstanceID().ToString());
                Timing.RunCoroutine(Shake(), GetInstanceID().ToString());
                Timing.KillCoroutines(_mainBehaviorHandle);
            }
            else
            {
                MakeInvulnerable();
            }
        }
    }
}
