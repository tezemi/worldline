﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Player;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using Worldline.Combat.Projectiles;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.NPCs.Enemies.Tears
{
    public class BossHand : Enemy
    {
        public int AmountOfProjectiles = 3;
        public int ShakesNeededToEscapeGrab = 10;
        public float Speed = 4f;
        public float GroundLevel;
        public float LookSpeed = 0.2f;
        public float RotationOffset;
        public float ShakeIntensity = 0.5f;
        public float DistanceFromPlayer = 8f;
        public float VerticalLevelPadding = 2f;
        public float HorizontalLevelPadding = 2f;
        public Vector2 TrackingOffset;
        public Vector2 PlayerGrabbedPosition;
        public Vector2 PlayerGrabbedFlipXPosition;
        public BossHandActionState ActionState;
        public BossHandMovementState MovementState;
        public Boss Boss;
        public GameObject PlayerGrabbedBody;
        public GameObject ProjectilePrefab;
        public GameObject EnemyToSpawnPrefab;
        public Sprite PlayerGrabbedSprite;
        public Sprite[] IdleSprites;
        public Sprite[] FlyingAtPlayerSprites;
        public Sprite[] GrabbedPlayerSprites;
        public Sprite[] FiringProjectileSprites;
        public Vector2 HoverPoint { get; set; }
        public Vector2? IdlePoint { get; set; }
        public int GrabbedPlayerShakes { get; protected set; }
        public bool SmashingGround { get; protected set; }
        public bool FiringProjectiles { get; protected set; }
        private int _originalTouchDamage;
        private BossHandActionState _actionStateLastTick;
        private CoroutineHandle _anyAction;
        private CoroutineHandle _resetTouchDamageHandle;

        protected override void Awake()
        {
            base.Awake();
            TearDeathAnimation.AddSpriteToCache(DeathSprite);
            gameObject.SetActive(false);
        }

        protected virtual void OnEnable()
        {
            Invulnerable = true;
            _originalTouchDamage = TouchDamage;
            Timing.RunCoroutine(AITick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (Boss.Dead)
            {
                if (!Dead)
                {
                    Die();
                }

                return;
            }

            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Rewind)
            {
                if (SmashingGround || FiringProjectiles)
                {
                    Timing.KillCoroutines(_anyAction);
                    SmashingGround = false;
                    FiringProjectiles = false;
                }
            }

            GameObject player = MainPlayerOperator.MainPlayerObject;

            #region Actions
            if (ActionState != BossHandActionState.GrabPlayer && 
            ActionState != BossHandActionState.GrabbedPlayer && 
            ActionState != BossHandActionState.SmashingGround)
            {
                SpriteRenderer.flipX = player.transform.position.x > transform.position.x && Math.Abs(transform.rotation.eulerAngles.z) < 0.1f;
            }

            if (ActionState == BossHandActionState.GrabbedPlayer && (CombatantTimeOperator.ShouldHold || !player.activeSelf || player.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.Reset))
            {
                ActionState = BossHandActionState.Idle;
            }

            if (ActionState == BossHandActionState.GrabPlayer)
            {
                SetAnimation(FlyingAtPlayerSprites);
                MovementState = BossHandMovementState.FlyAtPlayer;
                TouchDamage = 0;
                if (Vector2.Distance(player.transform.position, transform.position) < 1f && !player.GetComponent<PlayerOperator>().Hold)
                {
                    ActionState = BossHandActionState.GrabbedPlayer;
                }
                else if (Vector2.Distance(transform.position, HoverPoint) < 2f && IdlePoint.HasValue)
                {
                    ActionState = BossHandActionState.Idle;
                    MovementState = BossHandMovementState.FollowBoss;
                    if (TouchDamage == 0)
                    {
                        TouchDamage = _originalTouchDamage;
                    }
                }
            }
            else if (ActionState == BossHandActionState.GrabbedPlayer)
            {
                Timing.KillCoroutines(_resetTouchDamageHandle);
                if (!PlayerGrabbedBody.activeSelf)
                {
                    PlayerGrabbedBody.SetActive(true);
                }

                PlayerGrabbedBody.transform.localPosition = new Vector3
                (
                    SpriteRenderer.flipX ? PlayerGrabbedFlipXPosition.x : PlayerGrabbedPosition.x,
                    SpriteRenderer.flipX ? PlayerGrabbedFlipXPosition.y : PlayerGrabbedPosition.y,
                    PlayerGrabbedBody.transform.localPosition.z
                );

                if (player.GetComponent<TimeOperator>().CurrentTimeEffect == TimeEffect.FastForward)
                {
                    PlayerGrabbedBody.GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[TimeEffect.FastForward];
                }
                else
                {
                    PlayerGrabbedBody.GetComponent<SpriteRenderer>().color = Color.white;
                }

                SetAnimation(GrabbedPlayerSprites);
                MovementState = BossHandMovementState.FollowBoss;
                player.GetComponent<SpriteRenderer>().sprite = PlayerGrabbedSprite;
                player.GetComponent<SpriteRenderer>().enabled = false;
                player.GetComponent<PlayerOperator>().OverrideAnimations = true;
                player.GetComponent<PlayerOperator>().Hold = true;
                player.GetComponent<PlayerOperator>().TakeDamageWhileHeld = true;
                player.GetComponent<PlayerOperator>().DisableMovementInput = true;
                if (player.GetComponentInChildren<PlayerWeaponOperator>() != null)
                {
                    player.GetComponentInChildren<PlayerWeaponOperator>().Disabled = true;
                }

                if (InputManager.GetButtonDown("left") || InputManager.GetButtonDown("right") || InputManager.GetButtonDown("up"))
                {
                    Shake(0.15f, 0.25f);
                    bool isTouchingSurface = player.GetComponent<BoxCollider2D>().IsTouching(LevelController.Main.PhysicsTilemap.GetComponent<Collider2D>());
                    if (GrabbedPlayerShakes++ >= ShakesNeededToEscapeGrab && !isTouchingSurface)
                    {
                        ActionState = BossHandActionState.Idle;
                        player.GetComponent<PlayerMovementOperator>().ForceJump();
                    }
                }

                player.transform.position = new Vector3(transform.position.x, transform.position.y, player.transform.position.z);
            }
            else if (ActionState == BossHandActionState.SwitchSides)
            {
                SetAnimation(IdleSprites);
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = new Vector2(player.transform.position.x + (player.transform.position.x > transform.position.x ? DistanceFromPlayer : -DistanceFromPlayer), transform.position.y);
                }

                MovementState = BossHandMovementState.Idle;
                if (Vector2.Distance(transform.position, IdlePoint.Value) < 2f)
                {
                    ActionState = BossHandActionState.Idle;
                    if (transform.position.x > player.transform.position.x && Boss.transform.position.x < player.transform.position.x ||
                    transform.position.x < player.transform.position.x && Boss.transform.position.x > player.transform.position.x)
                    {
                        MovementState = BossHandMovementState.FollowPlayer;
                    }
                    else
                    {
                        MovementState = BossHandMovementState.FollowBoss;
                    }
                }
            }
            else if (ActionState == BossHandActionState.FiringProjectile)
            {
                SetAnimation(FiringProjectileSprites);
                Quaternion targetRotation = Quaternion.LookRotation
                (
                    (Vector2)player.transform.position - (Vector2)transform.position,
                    Vector3.forward
                );

                targetRotation = Quaternion.Euler
                (
                    targetRotation.eulerAngles.x,
                    targetRotation.eulerAngles.y,
                    targetRotation.eulerAngles.z + RotationOffset
                );

                RotateTowards(targetRotation);

                if (!FiringProjectiles)
                {
                    FiringProjectiles = true;
                    Timing.KillCoroutines(_anyAction);
                    _anyAction = Timing.RunCoroutine(FireProjectiles(), Segment.FixedUpdate, GetInstanceID().ToString());
                }

                IEnumerator<float> FireProjectiles()
                {
                    yield return Timing.WaitForSeconds(1.5f);
                    for (int i = 0; i < AmountOfProjectiles; i++)
                    {
                        FireProjectileAtPlayer(ProjectilePrefab, ProjectilePrefab.GetComponent<Projectile>().DeathPoolName);
                        yield return Timing.WaitForSeconds(0.5f);
                    }

                    FiringProjectiles = false;
                    ActionState = BossHandActionState.Idle;
                }
            }
            else if (ActionState == BossHandActionState.SmashingGround)
            {
                SetAnimation(GrabbedPlayerSprites);
                MovementState = BossHandMovementState.Idle;

                if (!SmashingGround)
                {
                    SmashingGround = true;
                    Timing.KillCoroutines(_anyAction);
                    _anyAction = Timing.RunCoroutine(SmashGround(), Segment.FixedUpdate, GetInstanceID().ToString());
                }

                IEnumerator<float> SmashGround()
                {
                    yield return Timing.WaitForSeconds(2f);
   
                    while (transform.position.y > GroundLevel + 0.25f)
                    {
                        Rigidbody.MovePosition(transform.position - new Vector3(0f, 0.65f, 0f));
                        yield return Timing.WaitForOneFrame;
                    }

                    CameraOperator.ActiveCamera.Shake(0.1f, 0.02f, 0.5f);
                    GameObject enemy = Instantiate(EnemyToSpawnPrefab);
                    enemy.transform.position = player.transform.position + new Vector3(0f, 15f, 0f);
                    yield return Timing.WaitForSeconds(2f);                    

                    ActionState = BossHandActionState.Idle;
                    MovementState = BossHandMovementState.FollowBoss;
                    SmashingGround = false;
                }
            }
            else
            {
                SetAnimation(IdleSprites);
                if (PlayerGrabbedBody.activeSelf)
                {
                    PlayerGrabbedBody.SetActive(false);
                }
            }

            if (ActionState != BossHandActionState.FiringProjectile)
            {
                RotateTowards(Quaternion.identity);
            }

            if (ActionState != BossHandActionState.GrabbedPlayer && _actionStateLastTick == BossHandActionState.GrabbedPlayer)
            {
                GrabbedPlayerShakes = 0;
                player.GetComponent<SpriteRenderer>().enabled = true;
                player.GetComponent<PlayerOperator>().OverrideAnimations = false;
                player.GetComponent<PlayerOperator>().Hold = false;
                player.GetComponent<PlayerOperator>().TakeDamageWhileHeld = false;
                player.GetComponent<PlayerOperator>().DisableMovementInput = false;
                if (player.GetComponentInChildren<PlayerWeaponOperator>() != null)
                {
                    player.GetComponentInChildren<PlayerWeaponOperator>().Disabled = false;
                }

                MovementState = BossHandMovementState.FollowBoss;
            }
            #endregion

            #region Movement
            if (MovementState == BossHandMovementState.FollowPlayer)
            {
                if (IdlePoint.HasValue)
                {
                    IdlePoint = null;
                }

                HoverPoint = player.transform.position + new Vector3(player.transform.position.x < transform.position.x ? DistanceFromPlayer : -DistanceFromPlayer, 0f);
            }
            else if (MovementState == BossHandMovementState.FollowBoss)
            {
                if (IdlePoint.HasValue)
                {
                    IdlePoint = null;
                }

                HoverPoint = (Vector2)Boss.transform.position + TrackingOffset;
            }
            else if (MovementState == BossHandMovementState.Idle)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = transform.position;
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value;
                }
            }
            else if (MovementState == BossHandMovementState.FlyAtPlayer)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = new Vector2
                    (
                        player.transform.position.x < transform.position.x ? player.transform.position.x - 6f : player.transform.position.x + 6f,
                        player.transform.position.y
                    );
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value;
                }
            }
            else if (MovementState == BossHandMovementState.Shake)
            {
                if (!IdlePoint.HasValue)
                {
                    IdlePoint = Boss.transform.position;
                }

                if (IdlePoint.HasValue)
                {
                    HoverPoint = IdlePoint.Value + new Vector2(0f, Mathf.Sin(Time.realtimeSinceStartup) * ShakeIntensity);
                }
            }

            if (!IdlePoint.HasValue)
            {
                if (HoverPoint.x >= LevelController.Main.MapWidthMax - HorizontalLevelPadding)
                {
                    HoverPoint = new Vector2(LevelController.Main.MapWidthMax - HorizontalLevelPadding, HoverPoint.y);
                }
                else if (HoverPoint.x <= LevelController.Main.MapWidthMin + HorizontalLevelPadding)
                {
                    HoverPoint = new Vector2(LevelController.Main.MapWidthMin + HorizontalLevelPadding, HoverPoint.y);
                }

                if (HoverPoint.y <= LevelController.Main.MapHeightMin + VerticalLevelPadding)
                {
                    HoverPoint = new Vector2(HoverPoint.x, LevelController.Main.MapHeightMin + VerticalLevelPadding);
                }
            }

            MovementOperator.Speed = new Vector2(transform.position.x < HoverPoint.x ? Speed : -Speed, transform.position.y < HoverPoint.y ? Speed : -Speed);
            #endregion

            _actionStateLastTick = ActionState;
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other.gameObject == MainPlayerOperator.MainPlayerObject && TouchDamage == 0)
            {
                _resetTouchDamageHandle = Timing.RunCoroutine(EnableTouchDamage(), Segment.FixedUpdate, GetInstanceID().ToString());
            }

            IEnumerator<float> EnableTouchDamage()
            {
                yield return Timing.WaitForSeconds(1f);
                TouchDamage = _originalTouchDamage;
            }
        }
        
        protected IEnumerator<float> AITick()
        {
            yield return Timing.WaitUntilDone(() => !MainPlayerOperator.MainPlayerComponent.InEvent);

            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(Random.Range(6f, 12f));
                if (MainPlayerOperator.MainPlayerComponent.Hold)
                {
                    ActionState = Random.value < 0.5f ? BossHandActionState.FiringProjectile : BossHandActionState.SmashingGround;
                }
                else
                {
                    if (Random.value < 0.75f)
                    {
                        ActionState = Random.value < 0.25f ? BossHandActionState.SwitchSides : BossHandActionState.FiringProjectile;
                    }
                    else
                    {
                        ActionState = Random.value < 0.5f ? BossHandActionState.GrabPlayer : BossHandActionState.SmashingGround;
                    }
                }
                
                yield return Timing.WaitUntilDone(() => ActionState == BossHandActionState.Idle);
            }
        }

        public void RotateTowards(Quaternion targetRotation)
        {
            targetRotation.x = 0f;      // 2D rotation only...
            targetRotation.y = 0f;
            float realSpeed = CombatantTimeOperator.GetScaledValue(LookSpeed);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, realSpeed);
        }

        public override void Die()
        {
            TearDeathAnimation.Play(gameObject, DeathSprite, Alignment);
            base.Die();
        }
    }
}
