﻿
namespace Worldline.NPCs.Enemies.Tears
{
    public enum AugliteState
    {
        Idle,
        Attacking,
        Jumping,
        Cowering
    }
}
