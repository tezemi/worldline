﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.TimeEngine.Recording;
using UnityEngine;
using Worldline.Physics;

namespace Worldline.NPCs
{
    public class Wormo : Tear
    {
        public float Speed;
        public float DistanceToReveal;
        public bool Hidden { get; protected set; }
        public bool InitJump { get; protected set; }
        public int FramesToSmallJump { get; protected set; }
        public const int JumpIntervalMin = 60;
        public const int JumpIntervalMax = 120;
        public const int SupriseJumpHeight = 40;
        private int _frameToJumpCount;
        private int _normalJumpHeight;
        private float _normalGravity;
        
        protected virtual void OnEnable()
        {
            _normalGravity = Rigidbody.gravityScale;
            _normalJumpHeight = MovementOperator.JumpHeight;

            Hidden = true;

            Rigidbody.gravityScale = 0f;
            MovementOperator.JumpHeight = SupriseJumpHeight;

            this.GetSolidCollider().enabled = false;

            FramesToSmallJump = Random.Range(JumpIntervalMin, JumpIntervalMax);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (CombatantTimeOperator.ShouldHold) return;

            var target = GetClosestPlayer();

            SpriteRenderer.flipX = target.transform.position.x < transform.position.x;

            if (Hidden && Vector2.Distance(transform.position, target.transform.position) < DistanceToReveal)
            {
                Hidden = false;
                InitJump = true;
                Rigidbody.gravityScale = _normalGravity;
                MovementOperator.ForceJump();
            }

            if (InitJump && Rigidbody.velocity.y < 0f)
            {
                InitJump = false;
                MovementOperator.JumpHeight = _normalJumpHeight;
                this.GetSolidCollider().enabled = true;
            }

            if (!Hidden && !InitJump)
            {
                MovementOperator.Speed = new Vector2
                (
                    target.transform.position.x > transform.position.x ? Speed : -Speed,
                    MovementOperator.Speed.y
                );

                var raycast = new WorldlineRaycast
                (
                    new Vector2(this.GetSolidCollider().bounds.min.x - 0.1f, this.GetSolidCollider().bounds.center.y),
                    new Vector2(this.GetSolidCollider().bounds.max.x + 0.1f, this.GetSolidCollider().bounds.center.y),
                    0f,
                    MovementOperator.GroundDetectionLayerMask
                );

                var hit = raycast.Linecast();
                raycast.DebugDrawLine(hit ? Color.green : Color.red, 0.02f);

                if (hit)
                {
                    MovementOperator.Jump();
                }
                else if (_frameToJumpCount++ >= FramesToSmallJump && MovementOperator.Grounded)
                {
                    _frameToJumpCount = 0;
                    FramesToSmallJump = Random.Range(JumpIntervalMin, JumpIntervalMax);
                    MovementOperator.ForceJump(MovementOperator.JumpHeight / 2);
                }
            }
        }

        public class WormoSnapshot : CombatSnapshot
        {
            public bool Hidden { get; set; }
            public bool InitJump { get; set; }

            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                Hidden = gameObject.GetComponent<Wormo>().Hidden;
                InitJump = gameObject.GetComponent<Wormo>().InitJump;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<Wormo>().Hidden = Hidden;
                gameObject.GetComponent<Wormo>().InitJump = InitJump;
            }
        }
    }
}
