﻿using Worldline.Combat;
using Worldline.Visuals;
using Worldline.Physics;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.NPCs.Enemies.Tears
{
    public class BossSmallHand : MonoBehaviour
    {
        public float Speed;
        public Sprite DeathSprite;
        public Boss Boss;
        public Vector2 TrackingOffset;
        public bool Dead { get; protected set; }
        protected TimeOperator TimeOperator { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected MovementOperator MovementOperator { get; set; }

        protected virtual void Awake()
        {
            TimeOperator = GetComponent<TimeOperator>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            MovementOperator = GetComponent<MovementOperator>();
            TearDeathAnimation.AddSpriteToCache(DeathSprite);
            gameObject.SetActive(false);
        }

        protected virtual void FixedUpdate()
        {
            if (Boss.Dead)
            {
                if (!Dead)
                {
                    Dead = true;
                    TearDeathAnimation.Play(gameObject, DeathSprite, CombatAlignment.Enemy);
                    gameObject.SetActive(false);
                }
            }

            if (TimeOperator.CurrentTimeEffect == TimeEffect.Rewind) return;

            if (Boss.ActionState == BossActionState.SpawningEnemy)
            {
                SpriteRenderer.enabled = false;
            }
            else
            {
                SpriteRenderer.enabled = true;
            }

            Vector2 point = (Vector2)Boss.transform.position + TrackingOffset;
            if (point.x > transform.position.x)
            {
                MovementOperator.Speed = new Vector2(Speed, MovementOperator.Speed.y);
            }
            else if (point.x < transform.position.x)
            {
                MovementOperator.Speed = new Vector2(-Speed, MovementOperator.Speed.y);
            }

            if (point.y > transform.position.y)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, Speed);
            }
            else if (point.x < transform.position.x)
            {
                MovementOperator.Speed = new Vector2(MovementOperator.Speed.x, -Speed);
            }
        }

        public class BossSmallHandSnapshot : PhysicsSnapshot
        {
            public bool SpriteRendererEnabled { get; set; }

            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                SpriteRendererEnabled = gameObject.GetComponent<SpriteRenderer>().enabled;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<SpriteRenderer>().enabled = SpriteRendererEnabled;
            }
        }
    }
}
