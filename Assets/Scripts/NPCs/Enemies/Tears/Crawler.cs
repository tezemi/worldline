﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Physics;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Crawler : Tear
    {
        public Sprite NormalSprite;
        public const float Speed = 6;
        public const float WallDetectDistance = 1.3f;
        public const float DipScale = 1.3f;
        public const float DistanceToActivate = 4f;

        private bool _movingLeft;
        public bool MovingLeft
        {
            get
            {
                return _movingLeft;
            }
            set
            {
                GetComponent<MovementOperator>().Speed = value ? new Vector2(-Speed, 0) : new Vector2(Speed, 0);
                GetComponent<SpriteRenderer>().flipX = value;
                _movingLeft = value;
            }
        }

        protected virtual void Start()
        {
            MovingLeft = true;
        }

        protected override void Update()
        {
            base.Update();

            RaycastHit2D rayLeft = Physics2D.Raycast(transform.position, Vector2.left, GetComponent<BoxCollider2D>().size.x / 2 + WallDetectDistance, GetComponent<MovementOperator>().GroundDetectionLayerMask);
            RaycastHit2D rayRight = Physics2D.Raycast(transform.position, Vector2.right, GetComponent<BoxCollider2D>().size.x / 2 + WallDetectDistance, GetComponent<MovementOperator>().GroundDetectionLayerMask);

            if (MovingLeft && rayLeft)
            {
                MovingLeft = false;
            }

            if (!MovingLeft && rayRight)
            {
                MovingLeft = true;
            }

            float newHeight = Mathf.Abs(Mathf.Sin(Time.time)) * DipScale;
            foreach (BoxCollider2D box in GetComponents<BoxCollider2D>())
            {
                if (box.isTrigger) continue;

                box.size = new Vector2(box.size.x, newHeight < 0.1f ? 0.1f : newHeight);
            }

            if (Invulnerable && GetComponent<SpriteRenderer>().sprite == NormalSprite)
            {
                GetComponent<SpriteRenderer>().sprite = DeathSprite;
            }

            if (!Invulnerable && GetComponent<SpriteRenderer>().sprite == DeathSprite)
            {
                GetComponent<SpriteRenderer>().sprite = NormalSprite;
            }
        }
    }
}
 