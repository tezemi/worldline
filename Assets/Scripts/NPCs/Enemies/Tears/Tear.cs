﻿// Copyright (c) 2019 Destin Hebner
using System.Diagnostics.CodeAnalysis;
using Worldline.Level;
using Worldline.Combat;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Corruption;
using UnityEngine;

namespace Worldline.NPCs
{
    /// <summary>
    /// Abstract base class for the tear type of enemies. Tears are unique 
    /// from other types of enemies because they drop essence when they die,
    /// cause corruption when close to the player, and have a unique death 
    /// animation. They also require the Tearifier component which gives them 
    /// their signature look.
    /// </summary>
    [RequireComponent(typeof(Tearifier))]
    [RequireComponent(typeof(EssenceDropletSpawnPoint))]
    [SuppressMessage("ReSharper", "CompareOfFloatsByEqualityOperator")]
    public abstract class Tear : Enemy
    {
        [Header("Tear")]
        [Tooltip("The amount of essence this tear will drop when it dies.")]
        public int EssenceDropAmount = 50;
        public float MaxCorruption = 0.25f;
        public float DistanceToCorrupt { get; set; } = 16f;
        protected EssenceDropletSpawnPoint EssenceDropletSpawnPoint { get; set; }
        private float _lastCorruptionModification;
        private float _amountOfCorruptionSet;
        
        protected override void Awake()
        {
            base.Awake();
            EssenceDropletSpawnPoint = GetComponent<EssenceDropletSpawnPoint>();
            if (!CombatantTimeOperator.IsClone && (LevelLoader.Main == null || LevelLoader.Main.SwitchingLevels))
            {
                TearDeathAnimation.AddSpriteToCache(DeathSprite, true);
            }
        }

        protected override void OnTriggerStay2D(Collider2D other)
        {
            if (other.GetComponent<Combatant>() == null || !other.IsTouching(TriggerCollider)) return;

            bool shouldEffect = ShouldEffect(Alignment, other.GetComponent<Combatant>().Alignment);
            if (shouldEffect && TouchDamage > 0 && other.gameObject == MainPlayerOperator.MainPlayerObject && !MainPlayerOperator.MainPlayerComponent.Invulnerable)
            {
                MaxCorruption += 0.15f;
                DistanceToCorrupt += 3f;
            }

            base.OnTriggerStay2D(other);
        }

        protected override void Update()
        {
            base.Update();
            float distance = Vector2.Distance(MainPlayerOperator.MainPlayerObject.transform.position, transform.position);
            if (distance < DistanceToCorrupt + 1f)
            {
                float localIntensity = MathUtilities.GetRange(distance, 0f, DistanceToCorrupt, 0f, MaxCorruption);
                if (localIntensity < 0f)
                {
                    localIntensity = 0f;
                }

                float changeInIntensity = localIntensity - _lastCorruptionModification;
                _amountOfCorruptionSet += changeInIntensity;
                Corrupter.Intensity += changeInIntensity;
                _lastCorruptionModification = localIntensity;
            }
            else if (_amountOfCorruptionSet > 0f)
            {
                Corrupter.Intensity -= _amountOfCorruptionSet;
                _amountOfCorruptionSet = 0f;
                _lastCorruptionModification = 0f;
            }
        }

        public override void Die()
        {
            var anim = TearDeathAnimation.Play(gameObject, DeathSprite, Alignment);
            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Rewind)
            {   // If a tear dies while being rewound, we want the animation to
                // player normally, because enemies can't be brought back to life
                // if they die while rewinding.
                anim.GetComponent<TimeOperator>().enabled = false;
            }

            if (_amountOfCorruptionSet > 0f)
            {
                Corrupter.Intensity -= _amountOfCorruptionSet;
                _amountOfCorruptionSet = 0f;
                _lastCorruptionModification = 0;
            }

            var amountOfDrops = EssenceDropAmount / EssenceDropletSpawnPoint.Amount;
            EssenceDropletSpawnPoint.Burst = amountOfDrops;

            if (Imbuement != TimeEffect.None)   // If tear is imbued, spawn corresponding essence
            {
                EssenceDropletSpawnPoint.Type = Imbuement;
                EssenceDropletSpawnPoint.Spawn();
            }
            else                                // If not, spawn random essence
            {
                EssenceDropletSpawnPoint.Spawn(true);
            }
            
            base.Die();
        }
    }
}
