﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.NPCs
{
    public class AugliteBorder : MonoBehaviour
    {
        protected virtual void OnDrawGizmos()
        {
            if (GetComponent<BoxCollider2D>() != null)
            {
                Bounds bounds = GetComponent<BoxCollider2D>().bounds;
                Gizmos.color = new Color(0.5f, 0.15f, 0.55f, 1f);
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(bounds.size.x, 0f));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(bounds.size.x, 0f));
            }
        }
    }
}

