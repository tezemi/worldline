﻿
namespace Worldline.NPCs.Enemies.Tears
{
    public enum BossHandActionState
    {
        Idle,
        GrabPlayer,
        SwitchSides,
        GrabbedPlayer,
        SmashingGround,
        FiringProjectile
    }
}
