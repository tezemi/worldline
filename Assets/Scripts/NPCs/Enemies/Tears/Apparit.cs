﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using Worldline.Player;
using Worldline.TimeEngine;
using UnityEngine;

namespace Worldline.NPCs
{
    public class Apparit : Tear
    {
        [Header("Apparit")]
        public float AccelerationDuringAttack = 0.025f;
        public Sprite Idle;
        public Sprite[] AttackSprites;
        public GameObject[] Hands;
        public bool Attacking { get; set; }
        public int AttackFrame { get; set; }
        public bool Idleing { get; protected set; }
        public const float AttackInterval = 5;
        public const float AttackingSpeed = 8;
        public const float IdleSpeed = 4;
        public const float MaxDistance = 8;
        public const float MinDistance = 6;
        protected CoroutineTimer AttackTimer;
        private bool? _startRight;
        private bool _handsDied;
        private int _attackAnimFrames;
        private float _normalAcceleration;

        protected override void Awake()
        {
            base.Awake();
            AttackTimer = new CoroutineTimer(GetComponent<TimeOperator>(), () =>
            {
                if (!Idleing)
                {
                    Attacking = true;
                }
            });

            AttackTimer.StartTimer(AttackInterval, 0);
            _normalAcceleration = MovementOperator.Acceleration;
        }

        protected override void Update()
        {
            base.Update();

            if (CombatantTimeOperator.CurrentTimeEffect == TimeEffect.Rewind)
            {
                Attacking = false;
                _startRight = null;
            }

            if (GetComponent<TimeOperator>().ShouldHold || MainPlayerOperator.MainPlayerObject == null) return;

            if (Hands.All(h => h == null || h.GetComponent<ApparitHand>().Dead) && !_handsDied)
            {
                _handsDied = true;
                Attacking = true;
            }
            else if (_handsDied && Hands.Any(h => h != null && !h.GetComponent<ApparitHand>().Dead))
            {
                _handsDied = false;
                Attacking = false;
            }

            SpriteRenderer.sprite = Attacking ? AttackSprites[AttackFrame] : Idle;
            const int attackAnimFrames = 10;
            if (Attacking && _attackAnimFrames++ >= attackAnimFrames && AttackFrame < AttackSprites.Length - 1)
            {
                _attackAnimFrames = 0;
                AttackFrame++;
            }
            else if (!Attacking)
            {
                _attackAnimFrames = 0;
                AttackFrame = 0;
            }
            
            GameObject player = GetClosestPlayer().gameObject;
            SpriteRenderer.flipX = player.transform.position.x < transform.position.x;
            float x = 0;
            float y;
            if (!Attacking)
            {
                if (player.transform.position.x < transform.position.x)
                {
                    if (transform.position.x > player.transform.position.x + MaxDistance)
                    {
                        x = -IdleSpeed;
                    }

                    if (transform.position.x < player.transform.position.x + MinDistance)
                    {
                        x = IdleSpeed;
                    }
                }
                else
                {
                    if (transform.position.x < player.transform.position.x - MaxDistance)
                    {
                        x = IdleSpeed;
                    }

                    if (transform.position.x > player.transform.position.x - MinDistance)
                    {
                        x = -IdleSpeed;
                    }
                }

                y = MainPlayerOperator.MainPlayerObject.transform.position.y > transform.position.y ? IdleSpeed : -IdleSpeed;
                MovementOperator.Acceleration = _normalAcceleration;
            }
            else
            {
                float px = MainPlayerOperator.MainPlayerObject.transform.position.x;
                float py = MainPlayerOperator.MainPlayerObject.transform.position.y;
                x = px > transform.position.x ? AttackingSpeed : -AttackingSpeed;
                y = py > transform.position.y ? AttackingSpeed : -AttackingSpeed;
                if (_startRight == null)
                {
                    _startRight = px < transform.position.x;
                }
                else if (_startRight == true && px >= transform.position.x)
                {
                    _startRight = null;
                    Attacking = false;
                }
                else if (_startRight == false && px <= transform.position.x)
                {
                    _startRight = null;
                    Attacking = false;
                }

                if (Vector2.Distance(transform.position, new Vector2(px, py)) < 6f)
                {
                    MovementOperator.Acceleration = AccelerationDuringAttack;
                }
            }

            MovementOperator.Speed = new Vector2(x, y);
        }

        public override void Die()
        {
            base.Die();
            bool anyHandIsAlive = false;
            foreach (GameObject hand in Hands)
            {
                if (hand != null && !hand.GetComponent<ApparitHand>().Dead)
                {
                    anyHandIsAlive = true;
                    break;
                }
            }

            foreach (GameObject hand in Hands)
            {
                if (anyHandIsAlive)
                {
                    hand.GetComponent<ApparitHand>().DeathSprite = hand.GetComponent<ApparitHand>().MiddleFinger;
                }

                hand.GetComponent<ApparitHand>().Die();
            }
        }
    }
}
