﻿
namespace Worldline.NPCs
{
    public enum GameCharacterAction
    {
        Idle,
        WalkLeft,
        WalkRight,
        Jump,
        ChangeSpeed,
        ChangeJumpPower,
        FireID
    }
}
