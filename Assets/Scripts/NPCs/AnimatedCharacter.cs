﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.NPCs
{
    /// <summary>
    /// Class for NPCs which should be animated when engaging in certain 
    /// behaviors like walking, falling, jumping, ect.
    /// </summary>
    public class AnimatedCharacter : InteractableCharacter
    {
        [Header("Animated Character")]
        [Tooltip("If true, this NPC will face the player even while they walk.")]
        public bool FacePlayerWhileMoving;
        [Tooltip("If true, this NPC's animations will no longer be set automatically, allowing for custom animations to be set.")]
        public bool OverrideDefaultAnimations;
        [Tooltip("The NPC's idle/standing sprites, for when they are not moving.")]
        public Sprite[] IdleSprites;
        [Tooltip("The NPC's walking sprites that get set when they are moving.")]
        public Sprite[] WalkingSprites;
        [Tooltip("The NPC's falling sprites, that get set while they fall.")]
        public Sprite[] FallingSprites;
        [Tooltip("The NPC's jumping sprites, that get set while they jump.")]
        public Sprite[] JumpingSprites;

        protected override void Update()
        {
            if (Rigidbody.velocity.y > 0f)
            {
                if (!OverrideDefaultAnimations)
                {
                    SetAnimationIfAvailable(JumpingSprites);
                }

                if (FacePlayerWhileMoving)
                {
                    base.Update();
                }
            }
            else if (Rigidbody.velocity.y < 0f && !MovementOperator.Grounded)
            {
                if (!OverrideDefaultAnimations)
                {
                    SetAnimationIfAvailable(FallingSprites);
                }

                if (FacePlayerWhileMoving)
                {
                    base.Update();
                }
            }
            else if (Rigidbody.velocity.x > 0f || Rigidbody.velocity.x < 0f)
            {
                if (!OverrideDefaultAnimations)
                {
                    SetAnimation(WalkingSprites);
                }

                if (FacePlayerWhileMoving)
                {
                    base.Update();
                }
                else
                {
                    SpriteRenderer.flipX = Rigidbody.velocity.x < 0f;
                }
            }
            else
            {
                if (!OverrideDefaultAnimations)
                {
                    SetAnimation(IdleSprites);
                }

                base.Update();
            }
        }

        protected void SetAnimationIfAvailable(Sprite[] sprites)
        {
            SetAnimation(sprites.IsNullOrEmpty() ? IdleSprites : sprites);
        }
    }
}
