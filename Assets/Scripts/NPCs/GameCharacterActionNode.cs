﻿using Worldline.Player;
using Worldline.Physics;
using UnityEngine;
using Worldline.Combat;

namespace Worldline.NPCs
{
    public class GameCharacterActionNode : MonoBehaviour
    {
        [Tooltip("If true, any GameCharacter that enters this node will idle until the player reaches the specified PlayerDistance.")]
        public bool WaitForPlayer;
        [Tooltip("The speed at which this GameCharacter will walk.")]
        public float MoveSpeed = 4f;
        [Tooltip("If WaitForPlayer is true, this is the distance that the player needs to reach in order for the GameCharacter to carry out the action.")]
        public float PlayerDistance = 8f;
        public float JumpPowerChange;
        [Tooltip("If the Action is set to ChangeSpeed, this is the new speed.")]
        public Vector2 SpeedChange;
        [Tooltip("The type of NPCs this node will effect.")]
        public CombatAlignment Alignment;
        [Tooltip("The action to perform when a GameCharacter enters the trigger.")]
        public GameCharacterAction Action;
        /// <summary>
        /// If <see cref="WaitForPlayer"/> is true, this is the <see cref="GameCharacter"/> that entered 
        /// the trigger and is waiting for the <see cref="MainPlayerOperator"/> to get within 
        /// <see cref="PlayerDistance"/>.
        /// </summary>
        public GameCharacter CharacterWaiting { get; protected set; }

        protected virtual void Update()
        {
            if (CharacterWaiting != null && Vector2.Distance(CharacterWaiting.transform.position, MainPlayerOperator.MainPlayerObject.transform.position) < PlayerDistance)
            {
                PerformAction(CharacterWaiting);
                CharacterWaiting = null;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D col)
        {
            if (col.GetComponent<GameCharacter>() != null && col.GetComponent<GameCharacter>().Alignment == Alignment && col.GetComponent<PlayerOperator>() == null && col.isTrigger)
            {
                if (WaitForPlayer)
                {
                    col.GetComponent<MovementOperator>().Speed = Vector2.zero;
                    CharacterWaiting = col.GetComponent<GameCharacter>();
                }
                else
                {
                    PerformAction(col.GetComponent<GameCharacter>());
                }
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (GetComponent<BoxCollider2D>() != null)
            {
                Bounds bounds = GetComponent<BoxCollider2D>().bounds;
                Gizmos.color = Color.magenta;
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(bounds.size.x, 0f));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(bounds.size.x, 0f));
            }
        }

        protected void PerformAction(GameCharacter character)
        {
            switch (Action)
            {
                case GameCharacterAction.Idle:
                    character.GetComponent<MovementOperator>().Speed = Vector2.zero;
                    break;
                case GameCharacterAction.WalkLeft:
                    character.GetComponent<MovementOperator>().Speed = new Vector2(-MoveSpeed, 0f);
                    break;
                case GameCharacterAction.WalkRight:
                    character.GetComponent<MovementOperator>().Speed = new Vector2(MoveSpeed, 0f);
                    break;
                case GameCharacterAction.Jump:
                    character.GetComponent<MovementOperator>().Jump();
                    break;
                case GameCharacterAction.ChangeSpeed:
                    character.GetComponent<MovementOperator>().Speed = SpeedChange;
                    break;
                case GameCharacterAction.ChangeJumpPower:
                    character.GetComponent<MovementOperator>().JumpPower = JumpPowerChange;
                    break;
                case GameCharacterAction.FireID:
                    character.FireID();
                    break;
            }
        }
    }
}
