// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Items;
using Worldline.Player;
using Worldline.ScriptedSequences;
using Worldline.Player.Weapons;
using Worldline.Player.Upgrades;
using ProtoBuf;
using UnityEngine;

namespace Worldline.GameState
{
    /// <summary>
    /// Abstract base class for classes that contain data about the game that
    /// should be serialized and methods that retrieve and set that data.
    /// Any class that inherits from <see cref="FileData"/> should be included
    /// as an attribute on this class. When the game saves a file to the disk,
    /// one aspect of that file is a List of <see cref="FileData"/>. The 
    /// <see cref="FileData"/> inside that List contain information about
    /// various aspects of the game, such as the 
    /// <see cref="MainPlayerOperator"/>'s health, the 
    /// <see cref="EssenceOperator"/>'s essence amounts, ect. Inheritors
    /// of <see cref="FileData"/> decide what should be saved to the disk,
    /// by defining their own fields, and then setting those fields when
    /// the game needs to be saved. When loading, each <see cref="FileData"/>
    /// is deserialized, and the opposite should occur, where the fields are
    /// used to populate actual values in the game world. For example, the
    /// <see cref="PlayerData"/> class will serialize data about the player's
    /// health during a game save, but will deserialize and set it when the
    /// game is loaded.
    /// </summary>
    [ProtoContract]
    [ProtoInclude(1, typeof(PlayerData))]
    [ProtoInclude(2, typeof(ScriptedSequenceData))]
    [ProtoInclude(3, typeof(StateData))]
    [ProtoInclude(4, typeof(WeaponData))]
    [ProtoInclude(5, typeof(EssenceData))]
    [ProtoInclude(6, typeof(UpgradeData))]
    [ProtoInclude(7, typeof(ItemData))]
    public abstract class FileData
    {
        /// <summary>
        /// See <see cref="GameState.FileDataLoadType"/> for more information. Setting this will 
        /// determine whether or not file data is loaded before or after the  level is loaded.
        /// </summary>
        public abstract FileDataLoadType FileDataLoadType { get; }

        /// <summary>
        /// Implementations of this method should cause a FileData to return an
        /// instance of itself with all of its fields populated. For example,
        /// a <see cref="FileData"/> like <see cref="PlayerData"/> would retreive
        /// info about the <see cref="MainPlayerOperator"/>'s health when saving.
        /// </summary>
        /// <returns></returns>
        public abstract FileData PopulateAndGetFileData();

        /// <summary>
        /// Implementations of this method should cause a FileData to set the
        /// needed fields using the data it holds. For example, a 
        /// <see cref="FileData"/> like <see cref="PlayerData"/> would set
        /// values like the <see cref="MainPlayerOperator"/>'s health when 
        /// loading the game.
        /// </summary>
        public abstract void SetFileData();

        /// <summary>
        /// Gets all of the file data based on the listed attributes on the
        /// <see cref="FileData"/> class.
        /// </summary>
        /// <returns>A list of all populated FileData.</returns>
        public static List<FileData> GetAllFileData()
        {
            List<FileData> fileDatum = new List<FileData>();
            foreach (object type in typeof(FileData).GetCustomAttributes(typeof(ProtoIncludeAttribute), false))
            {
                if (type is ProtoIncludeAttribute protoInclude)
                {
                    FileData fileData = ((FileData)Activator.CreateInstance(protoInclude.KnownType)).PopulateAndGetFileData();
                    fileDatum.Add(fileData);
                }
            }

            return fileDatum;
        }

        /// <summary>
        /// Sets all specified FileDatum. This is usually called after the game 
        /// has just deserialized a <see cref="SaveFile"/>, and it needs to assign
        /// <see cref="FileData"/> values based on what was serialized on that file.
        /// </summary>
        /// <param name="fileDatum">The FileDatum to set.</param>
        /// <param name="fileDataLoadType">Specifies the type of data to load.</param>
        public static void SetAllFileData(List<FileData> fileDatum, FileDataLoadType fileDataLoadType)
        {
            if (fileDatum == null) return;

            Debug.Log("Setting file data...");
            foreach (FileData fileData in fileDatum)
            {
                if (fileData.FileDataLoadType == fileDataLoadType)
                {
                    fileData.SetFileData();
                }
            }

            Debug.Log("Set all file data successfully.");
        }
    }
}
