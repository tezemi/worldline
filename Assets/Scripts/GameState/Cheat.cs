﻿
namespace Worldline.GameState
{
    public enum Cheat
    {
        Crash,
        InfininteEssence,
        WallJump,
        DualPistols,
        Dash,
        AllWeaponsAndUpgrades,
        God,
        Thirdperson,
        FallDeathEvent,
        Dog,
        InfiniteJump,
        Censorship,
        ThiccWynne,
        BindKKill,
        Nightmare
    }
}
