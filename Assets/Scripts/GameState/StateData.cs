﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using ProtoBuf;

namespace Worldline.GameState
{
    /// <summary>
    /// A type of <see cref="FileData"/> that holds information about 
    /// <see cref="GameState"/>, notably any IDs managed by the 
    /// <see cref="TrackedMonoBehaviour"/> class.
    /// </summary>
    [ProtoContract]
    public class StateData : FileData
    {
        [ProtoMember(1)]
        public List<long> TrackedMonoBehaviourIDs { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Preload;

        public override FileData PopulateAndGetFileData()
        {
            TrackedMonoBehaviourIDs = TrackedMonoBehaviour.FiredIDs;

            return this;
        }

        public override void SetFileData()
        {
            TrackedMonoBehaviour.FiredIDs = TrackedMonoBehaviourIDs ?? new List<long>();
        }
    }
}
