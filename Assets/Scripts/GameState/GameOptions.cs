﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Worldline.Utilities;
using ProtoBuf;
using UnityEngine;

namespace Worldline.GameState
{
    /// <summary>
    /// This represents a configuration of options that can be saved
    /// and loaded from the hard disk. Options include global settings
    /// that effect the game's overall functionality, such as volumes,
    /// frame rates, and resolution.
    /// </summary>
    [ProtoContract]
    public class GameOptions
    {
        /// <summary>
        /// Possible frame rates that are allowed to be selected.
        /// </summary>
        public static readonly int[] AvailableFrameRates = { 15, 30, 60, 120, 240 };
        private static GameOptions _loadedGameOptions;
        
        /// <summary>
        /// XML files of different languages. No code defaults to EN_US. Don't 
        /// call this every frame, it checks each XML file stored on the disk.
        /// </summary>
        public static Language[] AvailableLanguages
        {
            get
            {
                List<Language> languages = new List<Language>() { Language.EN_US };                
                Language[] allLanguages = (Language[])Enum.GetValues(typeof(Language));
                foreach (string filePath in Directory.GetFiles($"{Application.dataPath}/Data/Text/", "*.xml", SearchOption.AllDirectories))
                {
                    string fileName = Path.GetFileName(filePath);
                    Regex regex = new Regex("[A-Z]{2}_[A-Z]{2}|[A-Z]{2}");
                    if (fileName != null && regex.IsMatch(fileName))
                    {
                        string sub = regex.Match(fileName).Value;
                        Language languageOnDisk = allLanguages.SingleOrDefault(l => l.ToString() == sub);
                        if (languageOnDisk != Language.NONE && !languages.Contains(languageOnDisk))
                        {
                            languages.Add(languageOnDisk);
                        }
                    }
                }

                return languages.ToArray();
            }
        }

        // Audio
        /// <summary>
        /// The master volume for the game's <see cref="AudioListener"/>, (0 - 1).
        /// </summary>
        [ProtoMember(1)]
        public float MasterVolume { get; set; } = 0.5f;
        /// <summary>
        /// The volume for the game's BGM <see cref="UnityEngine.Audio.AudioMixerGroup"/>.
        /// </summary>
        [ProtoMember(2)]
        public float MusicVolume { get; set; } = -10f;
        /// <summary>
        /// The volume for the game's SFX <see cref="UnityEngine.Audio.AudioMixerGroup"/>.
        /// </summary>
        [ProtoMember(3)]
        public float SoundEffectsVolume { get; set; }

        // GFX
        /// <summary>
        /// When true, numbers should display on the HUD by the essence crystals.
        /// </summary>
        [ProtoMember(4)]
        public bool ShowEssenceValuesOnHUD { get; set; }
        /// <summary>
        /// Whether or not the game should/is running in fullscreen.
        /// </summary>
        [ProtoMember(5)]
        public bool Fullscreen { get; set; } = true;
        /// <summary>
        /// The currently selected resolution within the <see cref="Screen.resolutions"/> 
        /// array.
        /// </summary>
        [ProtoMember(6)]
        public int ResolutionIndex { get; set; }
        /// <summary>
        /// The current selected FPS within the <see cref="AvailableFrameRates"/> array.
        /// </summary>
        [ProtoMember(7)]
        public int FPSIndex { get; set; } = 2;

        // Other
        /// <summary>
        /// Restricts some visual effects in order to make them less seizure-inducing.
        /// </summary>
        [ProtoMember(8)]
        public bool PhotosensitiveMode { get; set; }
        /// <summary>
        /// The currently selected language. Tells the game what strings to pull 
        /// from XML dialog documents.
        /// </summary>
        [ProtoMember(9)]
        public Language Language { get; set; } = Language.EN_US;

        public const float AudioMixerMaxVolume = 0f;
        public const float AudioMixerMinVolume = -80f;
        
        /// <summary>
        /// The currently loaded <see cref="GameOptions"/> file. If one does not 
        /// exist, a new one will be created, although this should only happen in 
        /// debugging mode, or when the game starts for the first time.
        /// </summary>
        public static GameOptions LoadedGameOptions
        {
            get
            {
                if (_loadedGameOptions == null)
                {
                    if (OptionsFileExists())
                    {
                        _loadedGameOptions = LoadOptionsFile(true);
                    }
                    else
                    {
                        _loadedGameOptions = new GameOptions();
                    }
                }

                return _loadedGameOptions;
            } 
            set => _loadedGameOptions = value;
        }

        /// <summary>
        /// Saves these options to the hard disk in the options file.
        /// </summary>
        public void Save()
        {
            FileStream fileStream = File.Create(Application.persistentDataPath + "/options.bin");
            Serializer.Serialize(fileStream, this);
            fileStream.Close();
        }
        
        /// <summary>
        /// This will apply any options stored within this file. If this is a
        /// brand new options file and you call this, you will break everything.
        /// </summary>
        public void ApplyOptions()
        {
            // Audio
            AudioListener.volume = MasterVolume;
            GlobalStorage.Get().BackgroundMusicAudioMixerGroup.audioMixer.SetFloat("volume", MusicVolume);
            GlobalStorage.Get().SoundEffectsAudioMixerGroup.audioMixer.SetFloat("volume", SoundEffectsVolume);

            // GFX
            try
            {
                Screen.SetResolution
                (
                    Screen.resolutions[ResolutionIndex].width,
                    Screen.resolutions[ResolutionIndex].height,
                    Fullscreen,
                    Screen.resolutions[ResolutionIndex].refreshRate
                );
            }
            catch (IndexOutOfRangeException)
            {
                Screen.SetResolution
                (
                    Screen.resolutions[0].width,
                    Screen.resolutions[0].height,
                    Fullscreen,
                    Screen.resolutions[0].refreshRate
                );
            }

            Application.targetFrameRate = 60;
        }

        /// <summary>
        /// Checks to see if an options file exists on the hard drive.
        /// </summary>
        /// <returns>True if the save file exists.</returns>
        public static bool OptionsFileExists()
        {
            return File.Exists(Application.persistentDataPath + "/options.bin");
        }

        /// <summary>
        /// Loads a <see cref="GameOptions"/> from the hard disk.
        /// </summary>
        /// <param name="applyOptions">Whether or not to apploy the options after loading them.</param>
        /// <returns>The <see cref="GameOptions"/> from the disk.</returns>
        public static GameOptions LoadOptionsFile(bool applyOptions)
        {
            try
            {
                FileStream fileStream = File.Open(Application.persistentDataPath + "/options.bin", FileMode.Open);
                GameOptions gameOptions = Serializer.Deserialize<GameOptions>(fileStream);
                fileStream.Close();
                if (applyOptions)
                {
                    gameOptions.ApplyOptions();
                }

                return gameOptions;
            }
            catch (Exception e)
            {
                Debug.LogWarning($"Could not load options, it might be an old file.{Environment.NewLine}{e}");

                return new GameOptions();
            }
        }
    }
}
