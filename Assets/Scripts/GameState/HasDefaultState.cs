﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.GameState
{
    /// <summary>
    /// When this is placed on a class, it tells the game that this class 
    /// has default static values that need to be established when a new
    /// <see cref="SaveFile"/> is created or loaded. Use the 
    /// <see cref="SetsUpDefaults"/> attribute to mark a static method to 
    /// be called when a new <see cref="SaveFile"/>is created or loaded.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class HasDefaultState : Attribute
    {
        // ...
    }
}
