﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.GameState
{
    /// <summary>
    /// Static methods with this attribute will be ran when a new 
    /// <see cref="SaveFile"/> is created or loaded and should be 
    /// used to reset static fields and properties for a new game.
    /// </summary>
    [AttributeUsage(AttributeTargets.Method)]
    public class SetsUpDefaults : Attribute
    {
        // ...
    }
}
