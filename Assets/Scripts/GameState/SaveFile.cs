﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.ScriptedSequences;
using MEC;
using ProtoBuf;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Worldline.GameState
{
    /// <summary>
    /// This represents a <see cref="SaveFile"/> that the player is playing on, 
    /// and can be saved or loaded on the hard disk. All <see cref="SaveFile"/>s 
    /// have a name. When a new <see cref="SaveFile"/> is created, it's name ID 
    /// is just the previous <see cref="SaveFile"/>'s number incremented. When a 
    /// <see cref="SaveFile"/> is deleted, the index of the <see cref="SaveFile"/>s
    /// does not change or shift. Some <see cref="SaveFile"/>s have their own names,
    /// just as "testing," or "demo."
    /// </summary>
    [ProtoContract]
    public class SaveFile
    {
        /// <summary>
        /// The name of a <see cref="SaveFile"/>. Usually, this is just an 
        /// auto-incrementing number.
        /// </summary>
        [ProtoMember(1)]
        public string Name { get; set; }
        /// <summary>
        /// The <see cref="UnityEngine.SceneManagement.Scene"/> the player was on when they last saved.
        /// </summary>
        [ProtoMember(2)]
        public string Scene { get; set; }
        /// <summary>
        /// The version of the game this <see cref="SaveFile"/> is intended for.
        /// </summary>
        [ProtoMember(3)]
        public string GameVersion { get; set; }
        /// <summary>
        /// The <see cref="GameState.Difficulty"/> of this <see cref="SaveFile"/>.
        /// </summary>
        [ProtoMember(4)]
        public Difficulty Difficulty { get; set; }
        /// <summary>
        /// The <see cref="DateTime"/> this <see cref="SaveFile"/> was created on.
        /// </summary>
        [ProtoMember(5)]
        public DateTime StartDate { get; set; }
        /// <summary>
        /// The <see cref="DateTime"/> this <see cref="SaveFile"/> was last saved.
        /// </summary>
        [ProtoMember(6)]
        public DateTime LastPlayedDate { get; set; }
        /// <summary>
        /// The amount of time this <see cref="SaveFile"/> has been played on.
        /// </summary>
        [ProtoMember(7)]
        public TimeSpan TotalPlayedTime { get; set; }
        /// <summary>
        /// All the <see cref="FileData"/> on this <see cref="SaveFile"/>. See the
        /// <see cref="FileData"/> class for more details.
        /// </summary>
        [ProtoMember(8)]
        public List<FileData> FileDatum { get; set; }
        /// <summary>
        /// The first level that is loaded when a new save is created.
        /// </summary>
        public const string DefaultScene = "Sim01";
        private static SaveFile _loadedSaveFile;

        /// <summary>
        /// This is the <see cref="SaveFile"/> that is currently being played on. If 
        /// no <see cref="SaveFile"/> is loaded, which should only be possible on the 
        /// title screen and during debugging, then this will either load and use the 
        /// first <see cref="SaveFile"/>, or create a new one.
        /// </summary>
        public static SaveFile LoadedSaveFile
        {
            get
            {
                if (_loadedSaveFile == null)
                {
                    _loadedSaveFile = new SaveFile("testing", Difficulty.Normal, false);                    
                }

                return _loadedSaveFile;
            }
            set => _loadedSaveFile = value;
        }

        /// <summary>
        /// <see cref="ProtoBuf"/> needs a parameterless constructor for serialization. 
        /// Do not use this when actually creating new <see cref="SaveFile"/>s.
        /// </summary>
        [UsedImplicitly]
        [Obsolete("Used exclusively for serialization, use SaveFile(int, difficulty) instead.")]
        public SaveFile()
        {
            // ...
        }

        /// <summary>
        /// Creates a new and blank <see cref="SaveFile"/>.
        /// </summary>
        /// <param name="name">The name or number of the save file.</param>
        /// <param name="difficulty">The <see cref="Difficulty"/> on this save file.</param>
        public SaveFile(string name, Difficulty difficulty)
        {
            Name = name;
            Difficulty = difficulty;
            GameVersion = Application.version;
            StartDate = LastPlayedDate = DateTime.Now;
            TotalPlayedTime = TimeSpan.Zero;
            SetupDefaultStates();

            BindingFlags bindingFlags = BindingFlags.GetField | BindingFlags.GetProperty |
                                        BindingFlags.Public | BindingFlags.Static;

            foreach (MemberInfo memberInfo in typeof(GlobalFlags).GetMembers(bindingFlags))
            {
                switch (memberInfo)
                {
                    case FieldInfo fieldInfo:
                        if (fieldInfo.FieldType == typeof(bool))
                        {
                            fieldInfo.SetValue(null, false);
                        }
                        else if (fieldInfo.FieldType == typeof(int))
                        {
                            fieldInfo.SetValue(null, 0);
                        }

                        break;
                    case PropertyInfo propertyInfo:
                        if (propertyInfo.PropertyType == typeof(bool))
                        {
                            propertyInfo.SetValue(null, false);
                        }
                        else if (propertyInfo.PropertyType == typeof(int))
                        {
                            propertyInfo.SetValue(null, 0);
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// This version of the constructor allows you to specify that the 
        /// <see cref="SaveFile"/> should not setup defaults, which is 
        /// normally not allowed. This is used for debugging purposes, as 
        /// setting up defaults in the middle of gameplay can cause issues.
        /// </summary>
        /// <param name="name">The name of the <see cref="SaveFile"/>.</param>
        /// <param name="difficulty">The <see cref="Difficulty"/> on this save file.</param>
        /// <param name="setupDefaults">Whether or not to call 
        /// <see cref="SetupDefaultStates"/>.</param>
        protected SaveFile(string name, Difficulty difficulty, bool setupDefaults)
        {
            Name = name;
            Difficulty = difficulty;
            GameVersion = Application.version;
            StartDate = LastPlayedDate = DateTime.Now;
            TotalPlayedTime = TimeSpan.Zero;
            if (setupDefaults)
            {
                SetupDefaultStates();
            }
        }

        /// <summary>
        /// Saves this <see cref="SaveFile"/> to the hard disk.
        /// </summary>
        public void Save()
        {
            Debug.Log($"Saving file {Name} to the hard disk. Game version is {GameVersion}");
            Scene = SceneManager.GetActiveScene().name;
            LastPlayedDate = DateTime.Now;
            TotalPlayedTime = LastPlayedDate.Subtract(StartDate);
            FileDatum = FileData.GetAllFileData();
            FileStream fileStream = File.Create(Application.persistentDataPath + $"/file_{Name}.bin");
            Serializer.Serialize(fileStream, this);
            fileStream.Close();
            Debug.Log($"File {Name} was saved to the hard disk.");
        }

        /// <summary>
        /// Moves to the scene that is saved on the file and loads all of the 
        /// <see cref="FileDatum"/>, essentially loading a game from a save file.
        /// </summary>
        public void LoadGame()
        {
            Debug.Log($"Loading file {Name} from the hard disk. The game version is {GameVersion}");
            SetupDefaultStates();
            Timing.RunCoroutine(WaitForSceneLoad(), Segment.FixedUpdate);

            IEnumerator<float> WaitForSceneLoad()
            {
                FileData.SetAllFileData(FileDatum, FileDataLoadType.Preload);
                SceneTransitionOperator.Main.LoadScene(Scene);
                yield return Timing.WaitUntilDone
                (
                    new WaitUntil(() => !SceneTransitionOperator.Main.Transitioning)
                );

                FileData.SetAllFileData(FileDatum, FileDataLoadType.Postload);
                Debug.Log($"Loaded file {Name} from the hard disk.");
            }
        }

        /// <summary>
        /// Runs through every class that has the <see cref="HasDefaultState"/> attribute 
        /// and calls their static method that has the <see cref="SetsUpDefaults"/> 
        /// attributes. This gets called when a new game is created, or a game is loaded 
        /// from a <see cref="SaveFile"/>.
        /// </summary>
        public void SetupDefaultStates()
        {
            Debug.Log("Settings up default states...");
            BindingFlags methodBindingFlags = BindingFlags.NonPublic | BindingFlags.Static;
            foreach (TypeInfo typeInfo in GetType().Assembly.DefinedTypes)
            {
                if (Attribute.GetCustomAttribute(typeInfo, typeof(HasDefaultState), false) != null)
                {
                    foreach (MethodInfo methodInfo in typeInfo.GetMethods(methodBindingFlags))
                    {
                        if (Attribute.GetCustomAttribute(methodInfo, typeof(SetsUpDefaults), false) != null)
                        {
                            methodInfo.Invoke(null, null);
                        }
                    }
                }
            }

            Debug.Log("Set up default states.");
        }

        /// <summary>
        /// Checks to see if a <see cref="SaveFile"/> with a given name  exists on the
        /// hard drive.
        /// </summary>
        /// <param name="name">The name to check for.</param>
        /// <returns>True if the save file exists.</returns>
        public static bool SaveFileExists(string name)
        {
            return File.Exists(Application.persistentDataPath + $"/file_{name}.bin");
        }

        /// <summary>
        /// Loads a <see cref="SaveFile"/> from the hard disk.
        /// </summary>
        /// <param name="name">The <see cref="SaveFile"/>'s name.</param>
        /// <returns>The <see cref="SaveFile"/> with the corresponding name.</returns>
        public static SaveFile LoadSaveFile(string name)
        {
            try
            {
                using (FileStream fileStream = File.Open(Application.persistentDataPath + $"/file_{name}.bin", FileMode.Open))
                {
                    try
                    {
                        SaveFile saveFile = Serializer.Deserialize<SaveFile>(fileStream);
                        if (saveFile.GameVersion != Application.version)
                        {
                            Debug.Log($"Loaded save file with different game version.{Environment.NewLine}Loaded file: {saveFile.GameVersion}{Environment.NewLine}Game version: {Application.version}");

                            return new SaveFile(saveFile.Name, saveFile.Difficulty);
                        }

                        fileStream.Close();

                        return saveFile;
                    }
                    catch (ProtoException)
                    {
                        Debug.Log("Could not deserialize save file. The save file's version may be pre version a2.");
                        fileStream.Close();

                        return new SaveFile(name, Difficulty.Normal);
                    }
                }
            }
            catch (IOException)
            {
                Debug.Log("Could not deserialize save file.");

                return new SaveFile(name, Difficulty.Normal);
            }
        }

        /// <summary>
        /// Returns every <see cref="SaveFile"/> that is saved on the hard disk. 
        /// This is quite expensive.
        /// </summary>
        /// <returns>All <see cref="SaveFile"/>s on the hard disk.</returns>
        public static List<SaveFile> GetSaveFilesOnHardDisk()
        {
            List<SaveFile> saveFiles = new List<SaveFile>();
            foreach (string fileName in Directory.GetFiles(Application.persistentDataPath, "file_*"))
            {
                string dissectedName = Path.GetFileName(fileName).Replace("file_", "").Replace(".bin", "");
                saveFiles.Add(LoadSaveFile(dissectedName));
            }

            return saveFiles;
        }
    }
}