﻿using System.IO;
using System.Text;
using System.Linq;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Diagnostics;

namespace Worldline.GameState
{
    public static class CheatController
    {
        public static HashSet<Cheat> ActiveCheats { get; } = new HashSet<Cheat>();

        public static Dictionary<Cheat, string> CheatDictionary { get; } = new Dictionary<Cheat, string>
        {
            { Cheat.Crash, "度겧걬䩨෌㼚盈ᒮ㍼䗜ὧ⢄䱰준ਵ" },                // TREVOR
            { Cheat.InfininteEssence, "��䤮뮑鲨셒䶷៮㭶͒�皐豪訛" },      // WYNNE
            { Cheat.WallJump, "ࡩ墺쟥䂩�❩錅⶧쎣藉憔掕妩" },             // GREG
            { Cheat.DualPistols, "甲ꍕ⷇↾쿷ډⷆߪ䃒扚儙ꉬꂝ椞" },              // RUPERT
            { Cheat.Dash, "帮쨧迯侟䑬ͦ냒䀺遊녏⏉튯솏紣ᇝ" },              // TEZEMI
            { Cheat.AllWeaponsAndUpgrades, "�혃巡㧬䉡퉕ᨏ쓀皥䂣⥨Ბꉈ姷" },    // IMPULSE101
            { Cheat.God, "᰺绷줎嘱轸苧繌潊嶟ꄚ㢖輖ጜ故ᩱ" },                  // GOD
            { Cheat.Thirdperson, "Ԭ춦솋ꌇ툀梣쯅獉뺨朐餕큗ꭰ揿" },          // THIRDPERSON
            { Cheat.FallDeathEvent, "䭮鰤⓹�䐤膗壓檬ꅱ㒀ퟧ汃ઐჩ苎꽎" },       // YROTS EVAC
            { Cheat.Dog, "읳핃ࠛ萿䶅豝妾約띏盔ᤌ녲䋮ᩩ⫙" },                    // JENKA
            { Cheat.InfiniteJump, "頄︋켌璲媭꽅춏혢⭧颴䕊퇪닟仑ꪟ" },          // PWING
            { Cheat.Censorship, "�케糺뉧됑ꐎ꺉⛒鱺扗愎⩸徒�㡻" },           // FUCK
            { Cheat.ThiccWynne, "약텸깝ェ뙸苭蒘蛇Ⲭ媵䷼꟪믆붖鍌" },           // HE THICC
            { Cheat.BindKKill, "ꂎ뤇賌◶莜콨捜ᙄܖ홃췆窜鰃梮﫶" },            // BIND K KILL
            { Cheat.Nightmare, "洂傟䳇庰넻㢆깗ᚺ렓�ὅ仂ቜ鴽" }             // NIGHTMARE
        };

        public static Dictionary<string, Cheat> StringDictionary => CheatDictionary.ToDictionary(i => i.Value, i => i.Key);

        public static bool IsActive(Cheat cheat)
        {
            if (!CheatDictionary.ContainsKey(cheat))
            {
                Debug.LogError($"Cheat '{cheat}' has not been registered!");
                return false;
            }

            return ActiveCheats.Contains(cheat);
        }

        public static bool EnableCheat(Cheat cheat)
        {
            if (cheat == Cheat.Crash)
            {
                Utils.ForceCrash(ForcedCrashCategory.AccessViolation);
            }

            if (!ActiveCheats.Contains(cheat) && CheatDictionary.ContainsKey(cheat))
            {
                ActiveCheats.Add(cheat);

                return true;
            }

            return false;
        }

        public static string GenerateHash(string data)
        {
            HashAlgorithm algorithm = new SHA256Managed();

            byte[] plainText = Encoding.Unicode.GetBytes(data);

            byte[] hashedBytes = algorithm.ComputeHash(plainText);
            
            string decoded = Encoding.Unicode.GetString(hashedBytes);

            if (Debug.isDebugBuild)
            {
                File.WriteAllText($@"{Application.persistentDataPath}\cheats.txt", decoded);
            }

            return decoded;
        }
    }
}
