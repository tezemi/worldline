﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.GameState
{
    /// <summary>
    /// <see cref="FileData"/> with the <see cref="Preload"/> type will 
    /// get loaded before the level loads, which is required by some types 
    /// of data that would modify level behavior (for example, 
    /// <see cref="ScriptedSequences.GlobalFlags"/>, since they might 
    /// determine the spawn points of NPCs.) <see cref="Postload"/> will 
    /// get loaded after the level loads, and is needed for data that sets
    /// values on level specific objects that need to have already been 
    /// loaded in (for example, the <see cref="Player.PlayerData"/>, which
    /// requires a <see cref="Player.MainPlayerOperator"/> instance to 
    /// already be loaded in.
    /// </summary>
    public enum FileDataLoadType
    {
        /// <summary>
        /// Indicates the data should be loaded before the level and its contents 
        /// are loaded.
        /// </summary>
        Preload,
        /// <summary>
        /// Indicates the data should be loaded after the level and its contents 
        /// are loaded.
        /// </summary>
        Postload
    }
}
