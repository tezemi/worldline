// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GameState
{
    /// <summary>
    /// Present at the very start of the game, this component performs some
    /// initializing behavior and then dies, just like people.
    /// </summary>
    public class StartupController : MonoBehaviour
    {
        protected virtual void Awake()
        {
            QualitySettings.vSyncCount = 0;
            if (GameOptions.OptionsFileExists())
            {
                GameOptions.LoadedGameOptions = GameOptions.LoadOptionsFile(true);
            }
            else
            {
                GameOptions.LoadedGameOptions = new GameOptions();
                GameOptions.LoadedGameOptions.Save();
            }

            GameOptions.LoadedGameOptions.ApplyOptions();
        }
    }
}
