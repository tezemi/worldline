﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.GameState
{
    /// <summary>
    /// A <see cref="TrackedMonoBehaviour"/> is a <see cref="MonoBehaviour"/> 
    /// that keeps track of a serialized ID, and contains methods for firing 
    /// that ID, checking if the ID has been fired, and resetting the global 
    /// list of fired IDs. These IDs will get assigned via the 
    /// TrackedMonoBehaviourIDAssignor (editor only) class. This is useful for
    /// <see cref="GameObject"/>s that only get spawned once, or only spawn or 
    /// activate based on some sort of criteria. For example, and enemy that 
    /// won't spawn again after it's killed, or a 
    /// <see cref="ScriptedSequences.ScriptedSequence"/> won't fire again after 
    /// it's already been fired.
    /// </summary>
    [HasDefaultState]
    public abstract class TrackedMonoBehaviour : MonoBehaviour
    {
        /// <summary>
        /// All of the FiredIDs on the currently loaded <see cref="SaveFile"/>.
        /// </summary>
        public static List<long> FiredIDs { get; set; } = new List<long>();
        /// <summary>
        /// A unique <see cref="ID"/> generated for this 
        /// <see cref="TrackedMonoBehaviour"/>. The <see cref="ID"/> is 
        /// composed of two <see cref="ID"/>s, one from the 
        /// <see cref="UnityEngine.SceneManagement.Scene"/> the 
        /// <see cref="TrackedMonoBehaviour"/> is in, and the other is from 
        /// the instance ID of the <see cref="TrackedMonoBehaviour"/> itself. 
        /// If <see cref="UseGlobalID"/> is true, this <see cref="ID"/> is based 
        /// of the  <see cref="Component"/> as a class, and will not be unique 
        /// from other instances of the <see cref="Component"/>.
        /// </summary>
        [HideInInspector]
        public long ID;
        /// <summary>
        /// True if this <see cref="GameObject"/> should stop checking its 
        /// <see cref="ID"/>.
        /// </summary>
        public bool IgnoreFired { get; set; }
        /// <summary>
        /// Whether or not this <see cref="GameObject"/>'s <see cref="ID"/> has been 
        /// fired.
        /// </summary>
        public bool Fired => FiredIDs.Contains(ID) && !IgnoreFired;
        /// <summary>
        /// If true, this <see cref="GameObject"/> will use an <see cref="ID"/> 
        /// based on its class rather than its instance. This means that when 
        /// the <see cref="ID"/> is fired for a <see cref="Component"/> of a type, 
        /// it is actually fired for all <see cref="Component"/>s of that type.
        /// </summary>
        public virtual bool UseGlobalID { get; } = false;

        /// <summary>
        /// Adds this <see cref="TrackedMonoBehaviour"/>'s <see cref="ID"/> to the list 
        /// of <see cref="FiredIDs"/>.
        /// </summary>
        public virtual void FireID()
        {
            FiredIDs.Add(ID);
        }

        /// <summary>
        /// Removes an <see cref="ID"/> from <see cref="FiredIDs"/>.
        /// </summary>
        public virtual void UnsetID()
        {
            if (FiredIDs.Contains(ID))
            {
                FiredIDs.Remove(ID);
            }
            else
            {
                Debug.LogWarning("Trying to unset ID that has not been set.", this);
            }
        }

        /// <summary>
        /// If <see cref="UseGlobalID"/> is set to true, then a 
        /// <see cref="TrackedMonoBehaviour"/>'s <see cref="ID"/> will 
        /// not be unique, and is instead based on the name of the class. 
        /// This will return the <see cref="ID"/> that would be generated 
        /// for a class.
        /// </summary>
        /// <typeparam name="T">The <see cref="TrackedMonoBehaviour"/> type 
        /// to get the <see cref="ID"/> for.</typeparam>
        /// <returns>An <see cref="ID"/> based on the name of a class.</returns>
        public static int GetIDFromType<T>() where T : TrackedMonoBehaviour
        {
            return typeof(T).Name.GetHashCode();
        }

        [UsedImplicitly]
        [SetsUpDefaults]
        private static void SetupDefaults()
        {
            FiredIDs = new List<long>();
        }
    }
}
