﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.GameState
{
    /// <summary>
    /// Defines the game's different difficulties.
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// The difficulty <see cref="Easy"/> should make aspects about the game easier.
        /// </summary>
        Easy,
        /// <summary>
        /// The difficulty <see cref="Normal"/> is considered to be the default 
        /// difficulty. Usually, when implementing difficulty-specific behavior, 
        /// normal should the default. For example, if an enemy's health should 
        /// be effected by difficulty, then if the difficulty is <see cref="Normal"/>, 
        /// the enemy's health shouldn't change.
        /// </summary>
        Normal,
        /// <summary>
        /// The difficulty <see cref="Hard"/> should make aspects about the game harder.
        /// </summary>
        Hard,
        /// <summary>
        /// <see cref="AssBlasting"/> is similar to <see cref="Hard"/>, and usually, 
        /// if the difficulty is <see cref="AssBlasting"/>, then whatever is being
        /// changed should default to whatever the settings for <see cref="Hard"/>
        /// is. The only difference between <see cref="Hard"/> and 
        /// <see cref="AssBlasting"/> is that <see cref="AssBlasting"/> has the 
        /// unique property of killing the <see cref="Player.MainPlayerOperator"/>
        /// instantly.
        /// </summary>
        AssBlasting
    }
}