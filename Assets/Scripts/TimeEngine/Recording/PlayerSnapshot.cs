﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Player.Upgrades;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class PlayerSnapshot : CombatSnapshot
    {
        public int CurrentJumps;
        public bool ExpendedJumps;

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            if (gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>() != null)
            {
                CurrentJumps = gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>().CurrentJumps;
                ExpendedJumps = gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>().ExpendedJumps;
            }
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            if (gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>() != null)
            {
                gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>().CurrentJumps = CurrentJumps;
                gameObject.GetComponent<PlayerOperator>().GetUpgrade<MultiJumpUpgrade>().ExpendedJumps = ExpendedJumps;
            }
        }
    }
}
