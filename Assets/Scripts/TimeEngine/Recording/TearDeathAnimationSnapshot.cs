﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Visuals;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class TearDeathAnimationSnapshot : VisualSnapshot
    {
        public int AnimationIndex { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            AnimationIndex = gameObject.GetComponent<TearDeathAnimation>().AnimationIndex;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<TearDeathAnimation>().AnimationIndex = AnimationIndex;
        }
    }
}
