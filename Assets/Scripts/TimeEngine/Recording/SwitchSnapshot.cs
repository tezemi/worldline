// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using UnityEngine;
using Worldline.Level.Puzzles;

namespace Worldline.TimeEngine.Recording
{
    public class SwitchSnapshot : Snapshot
    {
        public bool On;
        public bool Completed;
        public bool PermanentlyCompleted;

        public override void Record(GameObject gameObject)
        {
            On = gameObject.GetComponent<Switch>().On;
            Completed = gameObject.GetComponent<Switch>().Completed;
            PermanentlyCompleted = gameObject.GetComponent<Switch>().PermanentlyCompleted;
        }

        public override void Replay(GameObject gameObject)
        {
            gameObject.GetComponent<Switch>().On = On;
            gameObject.GetComponent<Switch>().Completed = Completed;
            gameObject.GetComponent<Switch>().PermanentlyCompleted = PermanentlyCompleted;
        }
    }
}
