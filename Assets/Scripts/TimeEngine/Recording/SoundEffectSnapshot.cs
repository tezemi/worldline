﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class SoundEffectSnapshot : Snapshot
    {
        public float Pitch { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Pitch = gameObject.GetComponent<SoundEffect>().AudioSource.pitch;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<SoundEffect>().AudioSource.pitch = -Pitch;
        }
    }
}
