// Copyright (c) 2019 Destin Hebner
using Worldline.NPCs;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class ApparitHandSnapshot : CombatSnapshot
    {
        public bool Attacking { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Attacking = gameObject.GetComponent<ApparitHand>().Attacking;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<ApparitHand>().Attacking = Attacking;
        }
    }
}
