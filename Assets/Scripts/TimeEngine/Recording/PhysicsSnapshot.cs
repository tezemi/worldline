﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// A PhysicsSnapshot records a Rigidbody's velocity.
    /// </summary>
    public class PhysicsSnapshot : VisualSnapshot
    {
        public Vector2 Velocity { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Velocity = gameObject.GetComponent<Rigidbody2D>().velocity;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Rigidbody2D>().velocity = Velocity;
        }
    }
}
