// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class MasherSnapshot : VisualSnapshot
    {
        public bool MovingToStart { get; set; }
        public bool Waiting { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);

        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);

        }
    }
}
