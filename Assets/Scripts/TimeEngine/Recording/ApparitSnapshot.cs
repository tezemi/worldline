// Copyright (c) 2019 Destin Hebner
using Worldline.NPCs;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class ApparitSnapshot : CombatSnapshot
    {
        public bool Attacking { get; set; }
        public int AttackFrame { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Attacking = gameObject.GetComponent<Apparit>().Attacking;
            AttackFrame = gameObject.GetComponent<Apparit>().AttackFrame;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Apparit>().Attacking = Attacking;
            gameObject.GetComponent<Apparit>().AttackFrame = AttackFrame;
        }
    }
}
