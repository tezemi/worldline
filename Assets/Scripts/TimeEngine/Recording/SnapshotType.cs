﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.TimeEngine.Recording
{
    public enum SnapshotType
    {
        Snapshot,
        ApparitHandSnapshot,
        ApparitSnapshot,
        AudioSnapshot,
        CombatSnapshot,
        CrawlerSnapshot,
        FlocklingSnapshot,
        MasherSnapshot,
        MovingPlatformSnapshot,
        SwitchSnapshot,
        VisualSnapshot,
        DoorSnapshot,
        PlayerSnapshot,
        TilemapSliderSnapshot
    }
}
