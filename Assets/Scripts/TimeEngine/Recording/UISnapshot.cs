﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// UISnapshots record the sprite field on Image components.
    /// </summary>
    public class UISnapshot : Snapshot
    {
        public Sprite Sprite { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            //Sprite = gameObject.GetComponent<Image>().sprite;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            //gameObject.GetComponent<Image>().sprite = Sprite;
        }
    }
}
