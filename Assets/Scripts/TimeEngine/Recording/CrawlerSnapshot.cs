// Copyright (c) 2019 Destin Hebner
using Worldline.NPCs;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class CrawlerSnapshot : CombatSnapshot
    {
        public bool MovingLeft;

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            MovingLeft = gameObject.GetComponent<Crawler>().MovingLeft;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Crawler>().MovingLeft = MovingLeft;
        }
    }
}
