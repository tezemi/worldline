// Copyright (c) 2019 Destin Hebner
using Worldline.Combat;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// A combat snapshot records health and max health, and can be used for
    /// objects that have a Combatant.
    /// </summary>
    public class CombatSnapshot : PhysicsSnapshot
    {
        public int Health { get; set; }
        public int MaxHealth { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Health = gameObject.GetComponent<Combatant>().Health;
            MaxHealth = gameObject.GetComponent<Combatant>().MaxHealth;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Combatant>().Health = Health;
            gameObject.GetComponent<Combatant>().MaxHealth = MaxHealth;
        }
    }
}
