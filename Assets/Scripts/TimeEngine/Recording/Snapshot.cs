// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// This is the base snapshot, and can be used for recording data on
    /// objects that don't change much. This snapshot records position,
    /// rotation, scale, and finally any timers on the object, as long
    /// as the timers have "EffectedByTimescale" set to true.
    /// 
    /// Triggers: You'll notice a list of strings called "Triggers."
    /// triggers are simple tags or names that identify when something
    /// should happen during playback. For example, let's say an enemy
    /// fires a bullet. During playback, it might make sense to have
    /// the enemy fire a bullet at the same time it would have at that
    /// point in the past. To accomplish this, when the enemy fires a
    /// bullet normally, you would add a string to the list during that
    /// frame called "FireBullet," or something similar. During playback,
    /// a seperate collection can checked to see if the trigger exsisted
    /// at that point in time. You can use that trigger to call the
    /// method during playback, and simulate the playback firing a bullet.
    /// </summary>
    public class Snapshot
    {
        public Vector3 Position { get; set; }
        public Quaternion Rotation { get; set; }
        public Vector3 Scale { get; set; }
        public List<string> Triggers { get; set; }
        public Dictionary<Timer, bool> TimerStates { get; set; }
        
        public virtual void Record(GameObject gameObject)
        {
            Position = gameObject.transform.position;
            Rotation = gameObject.transform.rotation;
            Scale = gameObject.transform.localScale;
            foreach (Timer timer in gameObject.GetComponents<Timer>())
            {
                if (timer == null || !timer.EffectedByTimescale) continue;

                if (TimerStates == null)
                {
                    TimerStates = new Dictionary<Timer, bool>();
                }

                if (TimerStates.ContainsKey(timer))
                {
                    TimerStates[timer] = timer.enabled;
                }
                else
                {
                    TimerStates.Add(timer, timer.enabled);
                }
            }
        }

        public virtual void Replay(GameObject gameObject)
        {
            gameObject.transform.position = Position;
            gameObject.transform.rotation = Rotation;
            gameObject.transform.localScale = Scale;
            if (TimerStates != null)
            {
                foreach (Timer timer in TimerStates.Keys)
                {
                    if (timer == null || !timer.EffectedByTimescale) continue;

                    timer.enabled = TimerStates[timer];
                }
            }
        }
    }
}
