﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// Snapshot used for GameObject's with the TilemapSlider component.
    /// </summary>
    public class TilemapSliderSnapshot : Snapshot
    {
        public bool Smooth;
        public int CurrentIndex;
        public float Speed;
        public float DelayBetweenPoints;

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Smooth = gameObject.GetComponent<TilemapSlider>().Smooth;
            CurrentIndex = gameObject.GetComponent<TilemapSlider>().CurrentIndex;
            Speed = gameObject.GetComponent<TilemapSlider>().Speed;
            DelayBetweenPoints = gameObject.GetComponent<TilemapSlider>().DelayBetweenPoints;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<TilemapSlider>().Smooth = Smooth;
            gameObject.GetComponent<TilemapSlider>().CurrentIndex = CurrentIndex;
            gameObject.GetComponent<TilemapSlider>().Speed = Speed;
            gameObject.GetComponent<TilemapSlider>().DelayBetweenPoints = DelayBetweenPoints;
        }
    }
}
