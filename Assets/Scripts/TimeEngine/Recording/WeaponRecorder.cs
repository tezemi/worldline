﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using Worldline.Player.Weapons;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class WeaponRecorder : Recorder
    {
        protected PlayerWeaponOperator PlayerWeaponOperator { get; set; }

        protected override void Awake()
        {
            base.Awake();
            PlayerWeaponOperator = GetComponent<PlayerWeaponOperator>();
        }

        public override Snapshot RecordSnapshot()
        {
            WeaponOperatorSnapshot snapshot = new WeaponOperatorSnapshot();
            snapshot.Record(gameObject);
            if (PlayerWeaponOperator.Weapons.OfType<MachineGun>().Any())
            {
                MachineGun machineGun = PlayerWeaponOperator.Weapons.OfType<MachineGun>().First();
                snapshot.Overheat = machineGun.Overheat;
                snapshot.CooldownRate = machineGun.CooldownRate;
                snapshot.CooldownAmount = machineGun.CooldownAmount;
                snapshot.OverheatRate = machineGun.OverheatRate;
                snapshot.OverheatedTime = machineGun.OverheatedTime;
                snapshot.Overheated = machineGun.Overheated;
            }

            if (PlayerWeaponOperator.Weapons.OfType<NullSphereLauncher>().Any())
            {
                NullSphereLauncher nsl = PlayerWeaponOperator.Weapons.OfType<NullSphereLauncher>().First();
                snapshot.Charge = nsl.Charge;
            }

            snapshot.WeaponSpriteFlipY = PlayerWeaponOperator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().flipY;

            return snapshot;
        }

        public override void ReplaySnapshot(Snapshot snapshot)
        {
            WeaponOperatorSnapshot woSnapshot = (WeaponOperatorSnapshot)snapshot;
            PlayerWeaponOperator.WeaponIndex = woSnapshot.WeaponIndex;
            PlayerWeaponOperator.WeaponSpriteGameObject.GetComponent<SpriteRenderer>().flipY = woSnapshot.WeaponSpriteFlipY;
            if (PlayerWeaponOperator.Weapons.OfType<MachineGun>().Any())
            {
                MachineGun machineGun = PlayerWeaponOperator.Weapons.OfType<MachineGun>().Single();
                machineGun.Overheat = woSnapshot.Overheat;
                machineGun.CooldownRate = woSnapshot.CooldownRate;
                machineGun.CooldownAmount = woSnapshot.CooldownAmount;
                machineGun.OverheatRate = woSnapshot.OverheatRate;
                machineGun.OverheatedTime = woSnapshot.OverheatedTime;
                machineGun.Overheated = woSnapshot.Overheated;
            }

            if (PlayerWeaponOperator.Weapons.OfType<NullSphereLauncher>().Any())
            {
                NullSphereLauncher nsl = PlayerWeaponOperator.Weapons.OfType<NullSphereLauncher>().First();
                nsl.Charge = woSnapshot.Charge;
            }

            snapshot.Replay(gameObject);
        }
    }
}
