﻿// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.Player.Weapons;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class WeaponOperatorSnapshot : VisualSnapshot
    {
        // All
        public int WeaponIndex;
        public bool WeaponSpriteFlipY;

        // Machine Gun
        public float Overheat;
        public float CooldownRate;
        public float CooldownAmount;
        public float OverheatRate;
        public float OverheatedTime;
        public bool Overheated;

        // Null Sphere Launcher
        public int Charge;
        public float Delay;

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            WeaponIndex = gameObject.GetComponent<PlayerWeaponOperator>().WeaponIndex;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<PlayerWeaponOperator>().WeaponIndex = WeaponIndex;
        }
    }
}
