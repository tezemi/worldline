// Copyright (c) 2019 Destin Hebner
namespace Worldline.TimeEngine.Recording
{
    public enum RecorderMode
    {
        Recording,
        Rewinding,
        Playback
    }
}
