// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using UnityEngine;
using Worldline.Level.Platforms;

namespace Worldline.TimeEngine.Recording
{
    public class MovingPlatformSnapshot : VisualSnapshot
    {
        public bool ChangingDirection;
        public bool ChangingPoint;
        public int CurrentPoint;
        public float SmoothedSpeed;
        public float Speed;
        public float CircleAngle;
        
        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            ChangingDirection = gameObject.GetComponent<Platform>().ChangingDirection;
            ChangingPoint = gameObject.GetComponent<Platform>().ChangingPoint;
            Speed = gameObject.GetComponent<Platform>().Speed;
            SmoothedSpeed = gameObject.GetComponent<Platform>().SmoothedSpeed;
            CurrentPoint = gameObject.GetComponent<Platform>().CurrentPoint;
            CircleAngle = gameObject.GetComponent<Platform>().CircleAngle;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Platform>().ChangingDirection = ChangingDirection;
            gameObject.GetComponent<Platform>().ChangingPoint = ChangingPoint;
            gameObject.GetComponent<Platform>().Speed = Speed;
            gameObject.GetComponent<Platform>().SmoothedSpeed = SmoothedSpeed;
            gameObject.GetComponent<Platform>().CurrentPoint = CurrentPoint;
            gameObject.GetComponent<Platform>().CircleAngle = CircleAngle;
        }
    }
}
