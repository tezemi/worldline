// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// The Recorder is a component used to keep track of specified fields 
    /// and properties of other components on the GameObject. This is used
    /// for the rewind mechanic, which uses to the Recorder to rewind
    /// the GameObject. A Recorder has three modes, Record, Rewind, and
    /// Playback. A Recorder takes a snapshot which can be specified in the
    /// inspector via a dropdown menu, or specified in code via the 
    /// SnapshotType property. A snapshot defines what exactly about the
    /// GameObject needs to be recorded, and any class that inherits from
    /// the Snapshot class can be used as a snapshot. This is quite expensive,
    /// and should only be attached to objects that absolutely need to be
    /// recorded. Objects that don't move, or don't have any values that need
    /// to be tracked, but still need to be effected by the time engine,
    /// simply need a TimeOperator.
    /// </summary>
    public class Recorder : MonoBehaviour
    {
        /// <summary>
        /// The type of snapshot. This will throw an exception if the Type does 
        /// not inherit from Snapshot.
        /// </summary>
        public Type SnapshotType { get; set; }
        /// <summary>
        /// The index within the Snapshot[]. This really should only be set by 
        /// the TimeOperator.
        /// </summary>
        public int CurrentIndex { get; set; }
        /// <summary>
        /// An array of the recorded Snapshots. This acts like a buffer, when 
        /// the array is full, the Recorder will loop back around and start
        /// overwriting old Snapshots.
        /// </summary>
        public Snapshot[] Snapshots { get; protected set; } = new Snapshot[AmountOfSnapshots];
        /// <summary>
        /// The amount of Snapshots recorded. This is better than using CurrentIndex 
        /// as CurrentIndex will loop back around to zero when the Snapshot[] is full.
        /// </summary>
        public int AmountRecorded { get; protected set; }
        /// <summary>
        /// The total amount of Snapshots before the Recorder will begin to 
        /// overwrite old Snapshots.
        /// </summary>
        public const int AmountOfSnapshots = 3600;
        /// <summary>
        /// Contains the trigger that should be added to the snapshot this frame.
        /// </summary>
        protected List<string> TriggersToAdd { get; set; } = new List<string>();
        /// <summary>
        /// A list of MonoBehaviours that will be deactivated when RecorderMode 
        /// is set to RecorderMode.Rewinding.
        /// </summary>
        protected List<MonoBehaviour> DisableOnRewinding { get; set; } = new List<MonoBehaviour>();
        /// <summary>
        /// A list of MonoBehaviours that will be deactivated when RecorderMode 
        /// is set to RecorderMode.Playback.
        /// </summary>
        protected List<MonoBehaviour> DisableOnPlayback { get; set; } = new List<MonoBehaviour>();
        /// <summary>
        /// A list of MonoBehaviours that have been deactivated when RecorderMode 
        /// was set to Rewinding or Playback.
        /// </summary>
        protected List<MonoBehaviour> DisabledBehaviours { get; set; } = new List<MonoBehaviour>();
        [SerializeField]
        [HideInInspector]
        private string _snapshotType = "Worldline.TimeEngine.Recording.Snapshot";
        private RecorderMode _mode = RecorderMode.Recording;
        private bool _handledRewindOutOfMemory;
        private Recorder _useSnapshots;

        /// <summary>
        /// Recorders have three modes, Recording, Rewinding, and Playback. Recording 
        /// is the default mode, and the Recorder will record Snapshots every FixedUpdate
        /// in Snapshot[]. When set to Rewinding, the Recorder will replay Snapshots
        /// every frame and move backwards. When set to Playback, the Recorder will
        /// replay Snapshots, but move forwards.
        /// </summary>
        public RecorderMode Mode
        {
            get => _mode;
            set
            {
                if (value == RecorderMode.Rewinding)
                {
                    foreach (MonoBehaviour m in DisableOnRewinding)
                    {
                        if (m.enabled)
                        {
                            m.enabled = false;
                            DisabledBehaviours.Add(m);
                        }
                    }
                }

                if (value == RecorderMode.Playback)
                {
                    foreach (MonoBehaviour m in DisableOnPlayback)
                    {
                        if (m.enabled)
                        {
                            m.enabled = false;
                            DisabledBehaviours.Add(m);
                        }
                    }

                    if (_handledRewindOutOfMemory)
                    {
                        _handledRewindOutOfMemory = false;
                    }
                }

                if (value == RecorderMode.Recording)
                {
                    foreach (MonoBehaviour m in DisabledBehaviours)
                    {
                        if (m == null) continue;
                        
                        m.enabled = true;
                    }

                    if (_handledRewindOutOfMemory)
                    {
                        _handledRewindOutOfMemory = false;
                    }

                    DisabledBehaviours.Clear();
                }

                _mode = value;
            }
        }

        /// <summary>
        /// When this is set to not null, this Recorder will begin using the 
        /// Snapshot[] from the specified Recorder. This is good for making
        /// a GameObject mimic another GameObject.
        /// </summary>
        public Recorder UseSnapshots
        {
            get => _useSnapshots;
            set
            {
                Timing.KillCoroutines(GetInstanceID().ToString());
                if (value != null)
                {
                    CurrentIndex = value.CurrentIndex;
                    AmountRecorded = value.AmountRecorded;                    
                    Timing.RunCoroutine(ShareTick(), Segment.FixedUpdate, GetInstanceID().ToString());
                }
                else
                {
                    CurrentIndex = 0;
                    AmountRecorded = 0;                    
                    Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());
                }
                
                _useSnapshots = value;
            }
        }

        protected virtual void Awake()
        {
            SnapshotType = Type.GetType($"{_snapshotType}");

            if (SnapshotType == null)
            {
                Debug.LogError($"Could not create instance of snapshot of type {_snapshotType}", gameObject);
                enabled = false;
                return;
            }

            foreach (MonoBehaviour m in GetComponents<MonoBehaviour>())
            {
                if (m == null)
                {
                    Debug.LogWarning($"Missing componenent on {gameObject.name}", gameObject);
                    continue;
                }

                Type type = m.GetType();
                if (Attribute.GetCustomAttribute(type, typeof(DisableOnRewinding)) != null)
                {
                    DisableOnRewinding.Add(m);
                }

                if (Attribute.GetCustomAttribute(type, typeof(DisableOnPlayback)) != null)
                {
                    DisableOnPlayback.Add(m);
                }
            }
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        /// <summary>
        /// This is the main tick for the Recorder that loops every fixed frame.
        /// This will Record, Rewind, or Playback each frame based on the mode.
        /// </summary>
        /// <returns>IEnumerator to be used as a MEC coroutine.</returns>
        protected virtual IEnumerator<float> Tick()
        {
            start:
            yield return Timing.WaitForOneFrame;
            switch (Mode)
            {
                case RecorderMode.Recording:
                    Snapshots[CurrentIndex++] = RecordSnapshot();
                    AmountRecorded++;
                    break;
                case RecorderMode.Rewinding:
                    if (AmountRecorded <= 0 && !_handledRewindOutOfMemory)
                    {
                        GetComponent<TimeOperator>().HandleRewindOutOfMemory();
                        if (Snapshots[CurrentIndex] != null)
                        {
                            // Normally an index should never be null when working within the scope
                            // of recorded memory, but in cases where a GameObject gets rewound as
                            // soon as it is created, it will have no snapshots, and simply need to
                            // destroy itself.
                            ReplaySnapshot(Snapshots[CurrentIndex]);
                            SendMessageUpwards("OnReplaySnapshot", SendMessageOptions.DontRequireReceiver);
                        }

                        _handledRewindOutOfMemory = true;
                    }
                    else if (!_handledRewindOutOfMemory)
                    {
                        ReplaySnapshot(Snapshots[--CurrentIndex]);
                        SendMessageUpwards("OnReplaySnapshot", SendMessageOptions.DontRequireReceiver);
                        --AmountRecorded;
                    }

                    break;
                case RecorderMode.Playback:
                    ReplaySnapshot(Snapshots[++CurrentIndex]);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (AmountRecorded > 0)
            {
                if (CurrentIndex > AmountOfSnapshots - 1)
                {
                    CurrentIndex = 0;
                }
                else if (CurrentIndex <= 0) // TODO: Impossible...
                {
                    CurrentIndex = AmountOfSnapshots - 1;
                }
            }

            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        /// <summary>
        /// When UseSnapshots is set to not null, this will effectively replace 
        /// Tick() in terms of behavior. It's similar, but set to use the
        /// Snapshot[] from the specified Recorder.
        /// </summary>
        /// <returns>IEnumerator to be used as a MEC coroutine.</returns>
        protected virtual IEnumerator<float> ShareTick()
        {
            start:
            yield return Timing.WaitForOneFrame;
            switch (Mode)
            {
                case RecorderMode.Recording:
                    CurrentIndex = UseSnapshots.CurrentIndex;
                    break;
                case RecorderMode.Rewinding:
                    if (AmountRecorded <= 0 && !_handledRewindOutOfMemory)
                    {
                        GetComponent<TimeOperator>().HandleRewindOutOfMemory();
                        if (UseSnapshots.Snapshots[CurrentIndex] != null)
                        {
                            ReplaySnapshot(UseSnapshots.Snapshots[CurrentIndex]);
                            SendMessageUpwards("OnReplaySnapshot", SendMessageOptions.DontRequireReceiver);
                        }

                        _handledRewindOutOfMemory = true;
                    }
                    else if (!_handledRewindOutOfMemory)
                    {
                        ReplaySnapshot(UseSnapshots.Snapshots[CurrentIndex--]);
                        SendMessageUpwards("OnReplaySnapshot", SendMessageOptions.DontRequireReceiver);
                        --AmountRecorded;
                    }

                    break;
                case RecorderMode.Playback:
                    if (CurrentIndex < 0 || CurrentIndex >= Snapshots.Length) break;

                    ReplaySnapshot(UseSnapshots.Snapshots[CurrentIndex++]);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            if (CurrentIndex >= AmountOfSnapshots - 1)
            {
                CurrentIndex = 0;
            }
            else if (CurrentIndex <= 0)
            {
                CurrentIndex = AmountOfSnapshots - 1;
            }

            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        /// <summary>
        /// Records and returns a Snapshot.
        /// </summary>
        /// <returns>The Snapshot containing the recorded values.</returns>
        public virtual Snapshot RecordSnapshot()
        {
            try
            {
                Snapshot snapshot = (Snapshot)ObjectCreator.CreateInstance(SnapshotType);
                snapshot.Record(gameObject);
                foreach (string s in TriggersToAdd)
                {
                    if (snapshot.Triggers == null)
                    {
                        snapshot.Triggers = new List<string>();
                    }

                    snapshot.Triggers.Add(s);
                }
                
                TriggersToAdd.Clear();

                return snapshot;
            }
            catch (ArgumentNullException e)
            {
                Debug.LogError($"Could not create instance of snapshot.{Environment.NewLine}{e}", gameObject);
                enabled = false;
                return null;
            }            
        }

        /// <summary>
        /// Replays a Snapshot, setting the Snapshot's values.
        /// </summary>
        /// <param name="snapshot">The Snapshot to replay.</param>
        public virtual void ReplaySnapshot(Snapshot snapshot)
        {
            try
            {
                snapshot.Replay(gameObject);
            }
            catch (NullReferenceException e)
            {
                Debug.LogError($"Tried to replay null snapshot on {gameObject.name}{Environment.NewLine}{e}", gameObject);
            }
        }

        /// <summary>
        /// If an index falls outside the bounds of the Snapshot[], either in 
        /// the positive or the negative, this will return the index "adjusted"
        /// to loop around the array like a buffer and fall within it.
        /// </summary>
        /// <param name="value">The starting value.</param>
        /// <returns>The adjusted value.</returns>
        public int GetAdjustedAmount(int value)
        {
            if (value >= AmountOfSnapshots - 1)
            {
                value = AmountOfSnapshots - value;
            }
            else if (value < 0)
            {
                value = AmountOfSnapshots + value;
                if (value > AmountRecorded)
                {
                    value = AmountRecorded - 1;
                }
            }

            return value;
        }

        /// <summary>
        /// Records a trigger, which is simply a tag stored in a snapshot's 
        /// collection of tags. The collection can be checked during playback
        /// or rewind to perform some sort of action.
        /// </summary>
        /// <param name="trigger">The name of the trigger.</param>
        public void RecordTrigger(string trigger)
        {
            if (!TriggersToAdd.Contains(trigger))
            {
                TriggersToAdd.Add(trigger);
            }
        }

        /// <summary>
        /// Checks to see if a trigger exsisted on the current frame. If the 
        /// mode is set to recording, this always return false.
        /// </summary>
        /// <param name="trigger">The trigger to check for.</param>
        /// <returns>True if the trigger exsisted on that frame.</returns>
        public bool CheckTrigger(string trigger)
        {
            if (Mode == RecorderMode.Recording)
            {
                return false;
            }

            if (Mode == RecorderMode.Rewinding)
            {
                return Snapshots[CurrentIndex] != null && Snapshots[CurrentIndex].Triggers != null && Snapshots[CurrentIndex].Triggers.Contains(trigger);
            }

            return UseSnapshots != null && UseSnapshots.Snapshots[CurrentIndex] != null && 
                UseSnapshots.Snapshots[CurrentIndex].Triggers != null && 
                UseSnapshots.Snapshots[CurrentIndex].Triggers.Contains(trigger);
        }

        /// <summary>
        /// Rewinds the Recorder the specified amount. If the amount falls outside 
        /// of the Snapshot[], then this will loop back around.
        /// </summary>
        /// <param name="amount">The amount to rewind.</param>
        /// <param name="replaySnapshot">If true, after done rewinding, this 
        /// recording will immediately play the current snapshot</param>
        public void Rewind(int amount, bool replaySnapshot)
        {
            if (AmountRecorded - amount <= 0)
            {
                CurrentIndex = 0;
            }
            else
            {
                CurrentIndex -= amount;
                CurrentIndex = GetAdjustedAmount(CurrentIndex);
            }

            if (replaySnapshot)
            {
                try
                {
                    ReplaySnapshot(UseSnapshots == null ? Snapshots[CurrentIndex] : UseSnapshots.Snapshots[CurrentIndex]);
                }
                catch (IndexOutOfRangeException e)
                {
                    Debug.LogWarning($"Could not replay snapshot on {gameObject.name}.{Environment.NewLine}{e}", gameObject);
                }
            }
        }

        /// <summary>
        /// Rewinds the Recorder to the specified position. The position must 
        /// be within the bounds of the Snapshot[].
        /// </summary>
        /// <param name="position">The position to rewind to.</param>
        public void RewindToPoint(int position)
        {
            CurrentIndex = position;
            AmountRecorded = position;
        }

        /// <summary>
        /// Simply resets the Recorder to the starting position. Does not clear 
        /// the Snapshot[].
        /// </summary>
        public void ResetRecorder()
        {
            CurrentIndex = 0;
            Mode = RecorderMode.Recording;
            AmountRecorded = 0;
        }
    }
}
