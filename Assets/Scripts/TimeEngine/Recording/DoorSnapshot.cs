﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using UnityEngine;
using Worldline.Level.Puzzles;

namespace Worldline.TimeEngine.Recording
{
    public class DoorSnapshot : Snapshot
    {
        public float Speed;
        public bool Open;

        public override void Record(GameObject gameObject)
        {
            Speed = gameObject.GetComponent<SlidingDoor>().Speed;
            Open = gameObject.GetComponent<SlidingDoor>().Open;
        }

        public override void Replay(GameObject gameObject)
        {
            gameObject.GetComponent<SlidingDoor>().Speed = Speed;
            gameObject.GetComponent<SlidingDoor>().Open = Open;
        }
    }
}
