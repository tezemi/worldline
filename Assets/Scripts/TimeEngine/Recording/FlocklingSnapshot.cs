// Copyright (c) 2019 Destin Hebner
using Worldline.NPCs;
using UnityEngine;

namespace Worldline.TimeEngine.Recording
{
    public class FlocklingSnapshot : CombatSnapshot
    {
        public bool MovingUp;

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            MovingUp = gameObject.GetComponent<Flockling>().MovingUp;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<Flockling>().MovingUp = MovingUp;
        }
    }
}
