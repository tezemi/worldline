// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using Worldline.Visuals;

namespace Worldline.TimeEngine.Recording
{
    /// <summary>
    /// Visual snapshots record an entity's Sprite, FlipX, and FlipY fields,
    /// and should be used for objects with SpriteRenderers.
    /// </summary>
    public class VisualSnapshot : Snapshot
    {
        public Sprite Sprite { get; set; }
        public bool FlipX { get; set; }
        public bool FlipY { get; set; }

        public override void Record(GameObject gameObject)
        {
            base.Record(gameObject);
            Sprite = gameObject.GetComponent<SpriteRenderer>().sprite;
            FlipX = gameObject.GetComponent<SpriteRenderer>().flipX;
            FlipY = gameObject.GetComponent<SpriteRenderer>().flipY;
        }

        public override void Replay(GameObject gameObject)
        {
            base.Replay(gameObject);
            gameObject.GetComponent<SpriteRenderer>().sprite = Sprite;
            gameObject.GetComponent<SpriteRenderer>().flipX = FlipX;
            gameObject.GetComponent<SpriteRenderer>().flipY = FlipY;
        }
    }
}
