// Copyright (c) 2019 Destin Hebner
using Worldline.Combat;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is a special TimeOperator that is responsible for 
    /// resurrecting and cleaning dead GameObject's. GameObjects that implement
    /// the IResurrectable interface have the ability to resurrect from death
    /// during a rewind. When this TimeOperator if effected by rewind, and runs
    /// out of memory, it will resurrect its child (the dead GameObject) and then
    /// mark itself ready for pooling.
    /// </summary>
    public class DeathTimeOperator : TimeOperator, IHasAlignment, IPoolable
    {
        public GameObject Child;
        public CombatAlignment Alignment { get; set; }
        public bool DestroyOnElapsed { get; protected set; } = true;
        public const float DeathTimeDuration = 20f;
        public const string PoolName = "DeathTimeOperator";
        private string _childName;
        private CoroutineTimer _deathTimer;

        protected override void OnEnable()
        {
            base.OnEnable();
            _deathTimer = new CoroutineTimer(this, () => Free(true), HandleRewindOutOfMemory);
            _deathTimer.StartTimer(DeathTimeDuration, 1);
        }

        protected virtual void OnLevelLoaded()
        {
            Free(false);
        }

        private void Free(bool killChild)
        {
            if (killChild && DestroyOnElapsed)
            {
                Destroy(Child);
            }

            _deathTimer.StopTimer();
            CurrentTimeEffect = TimeEffect.None;
            gameObject.SetActive(false);
            Pool.Add(gameObject, PoolName, false);
        }
        
        public void Reuse()
        {
            Child = null;
            Alignment = CombatAlignment.Decoration;
            DestroyOnElapsed = true;
            CurrentTimeEffect = TimeEffect.None;
            _deathTimer = new CoroutineTimer(this, () => Free(true), HandleRewindOutOfMemory);
        }

        public override void HandleRewindOutOfMemory()
        {
            if (Child != null)
            {
                if (Child.GetComponent<ICanRessurect>() != null)
                {
                    Child.GetComponent<ICanRessurect>().Resurrect();
                }
                else
                {
                    Child.SetActive(true);
                }

                Child.GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.None;
            }
            else
            {
                Debug.LogWarning("Death handler tried to resurrect with null child on " + _childName + ". Are you cleaning the child up early?");
            }

            Free(false);
        }
        
        public static GameObject Handle(GameObject obj, CombatAlignment alignment, bool destroyOnElapsed)
        {
            GameObject deathHandler;
            if (Pool.Has(PoolName))
            {
                deathHandler = Pool.Get(PoolName);
            }
            else
            {
                deathHandler = new GameObject($"Death Handler for {obj.name}", typeof(DeathTimeOperator));
            }

            DeathTimeOperator d = deathHandler.GetComponent<DeathTimeOperator>();
            d.Child = obj;
            d.Alignment = alignment;
            d.DestroyOnElapsed = destroyOnElapsed;
            d._childName = obj.name;
            deathHandler.SetActive(true);
            DontDestroyOnLoad(deathHandler);
            deathHandler.DestroyOnSceneLoad();
           
            return deathHandler;
        }
    }
}