﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Worldline.TimeEngine
{
    public class CoroutineTimer
    {
        public bool Enabled { get; set; }
        public int Iterations { get; protected set; }
        public int CurrentIterations { get; protected set; }
        public float TimeElapsed { get; protected set; }
        public float TimeBetweenCalls { get; protected set; }
        public TimeOperator TimeOperator { get; protected set; }
        public Action CoroutineForward { get; protected set; }
        public Action CoroutineBackward { get; protected set; }
        public Func<bool> LoopCondition { get; protected set; }
        public Func<float> GetTimeBetweenCalls { get; protected set; }
        public Stack<float> PreviousTimes { get; protected set; } = new Stack<float>();
        protected CoroutineHandle MainLoopHandle { get; set; }
        
        public CoroutineTimer(TimeOperator timeOperator, Action coroutineForward)
        {
            TimeOperator = timeOperator;
            CoroutineForward = coroutineForward;
        }

        public CoroutineTimer(TimeOperator timeOperator, Action coroutineForward, Action coroutineBackward)
        {
            TimeOperator = timeOperator;
            CoroutineForward = coroutineForward;
            CoroutineBackward = coroutineBackward;
        }

        public void StartTimer(float timeBetweenCalls, int iterations)
        {
            Iterations = iterations;
            StartTimer(timeBetweenCalls, null);
            if (!Enabled)
            {
                MainLoopHandle = Timing.RunCoroutine(MainLoop(), Segment.FixedUpdate);
            }
        }

        public void StartTimer(float timeBetweenCalls, Func<bool> loopCondition)
        {
            CurrentIterations = 0;
            TimeElapsed = 0f;
            PreviousTimes = new Stack<float>();
            TimeBetweenCalls = timeBetweenCalls;
            LoopCondition = loopCondition;
            if (!Enabled)
            {
                MainLoopHandle = Timing.RunCoroutine(MainLoop(), Segment.FixedUpdate);
            }
        }
        
        public void StartTimer(Func<float> timeBetweenCalls, Func<bool> loopCondition)
        {
            GetTimeBetweenCalls = timeBetweenCalls;
            StartTimer(timeBetweenCalls(), loopCondition);
        }

        public void StopTimer()
        {
            Enabled = false;
            Timing.KillCoroutines(MainLoopHandle);
        }

        protected IEnumerator<float> MainLoop()
        {
            Enabled = true;
            start:
            yield return Timing.WaitForOneFrame;
            if (TimeOperator == null)
            {
                StopTimer();
                yield break;
            }

            yield return Timing.WaitUntilDone(new WaitUntil(() => Enabled && (TimeOperator == null || TimeOperator.isActiveAndEnabled)));
            if (TimeOperator == null)
            {
                StopTimer();
                yield break;
            }

            TimeElapsed += TimeOperator.GetScaledValue(0.02f);
            if (TimeElapsed >= TimeBetweenCalls)
            {
                if (GetLoopConditionOrIterations())
                {
                    CoroutineForward();
                }

                TimeElapsed = 0;
                CurrentIterations++;
                PreviousTimes.Push(TimeBetweenCalls);
                if (GetTimeBetweenCalls != null)
                {
                    TimeBetweenCalls = GetTimeBetweenCalls();
                }
            }

            if (TimeElapsed < 0)
            {
                if (PreviousTimes.Count > 0)
                {
                    TimeBetweenCalls = PreviousTimes.Pop();
                }

                if (CurrentIterations > 0 && Iterations > 0)
                {
                    CurrentIterations--;
                }

                TimeElapsed = TimeBetweenCalls - Time.fixedDeltaTime;
                CoroutineBackward?.Invoke();
            }

            if (TimeOperator != null)
            {
                goto start;
            }

            Enabled = false;
        }

        private bool GetLoopConditionOrIterations()
        {
            if (LoopCondition != null)
            {
                return LoopCondition();
            }

            return CurrentIterations < Iterations || LoopCondition == null && Iterations == 0;
        }
    }
}
