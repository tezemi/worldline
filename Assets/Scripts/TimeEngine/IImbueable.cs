// Copyright (c) 2019 Destin Hebner
namespace Worldline.TimeEngine
{
    public interface IImbueable
    {
        TimeEffect Imbuement { get; set; }
	}
}
