// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.Player.Upgrades;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is used to apply TimeEffects to GameObject's that 
    /// have physics, and so, a Rigidbody2D is required to be attached to the
    /// GameObject. This will give GameObject's speed adjustments depending
    /// on the TimeScale. If the GameObject also has a MovementOperator, it
    /// will be affected as well.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class PhysicsTimeOperator : VisualTimeOperator
    {
        protected Rigidbody2D Rigidbody { get; set; }
        private float _torqueBeforePause;
        private Vector3 _lastPosition;
        private Vector3 _velocityBeforePause;

        protected override void Awake()
        {
            base.Awake();
            Rigidbody = GetComponent<Rigidbody2D>();
        }

        protected virtual void FixedUpdate()
        {
            if (TimeScale > 0f && TimeScale < 1f && Rigidbody.bodyType != RigidbodyType2D.Static)
            {
                if (transform.position != _lastPosition)
                {
                    bool noX = GetComponent<PlayerOperator>() != null &&
                               GetComponent<PlayerOperator>().GetUpgrade<WallJumpUpgrade>() != null &&
                               GetComponent<PlayerOperator>().GetUpgrade<WallJumpUpgrade>().OnWall;
                    bool noY = GetComponent<MovementOperator>() != null && GetComponent<MovementOperator>().Grounded;
                    Vector2 lerped = Vector2.Lerp(transform.position, _lastPosition, TimeScale);
                    Vector3 final = new Vector3
                    (
                        noX ? transform.position.x : lerped.x, 
                        noY ? transform.position.y : lerped.y, 
                        transform.position.z
                    );
                        
                    transform.position = final;
                }
            }

            if (TimeScale > 1f && Rigidbody.bodyType != RigidbodyType2D.Static && !(this is PlayerTimeOperator))
            {
                Vector3 difference = (transform.position - _lastPosition) / TimeScale;
                transform.position += difference;
            }

            _lastPosition = transform.position;
        }

        protected override void ApplyPause()
        {
            base.ApplyPause();
            _torqueBeforePause = Rigidbody.angularVelocity;
            _velocityBeforePause = Rigidbody.velocity;
            Rigidbody.bodyType = RigidbodyType2D.Static;
        }

        protected override void ApplySlow()
        {
            base.ApplySlow();
            _lastPosition = transform.position;
        }

        protected override void ApplyFastForward()
        {
            base.ApplyFastForward();
            _lastPosition = transform.position;
        }

        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();
            Rigidbody.bodyType = RigidbodyType2D.Dynamic;
            if (PreviousEffect == TimeEffect.Pause)
            {
                Rigidbody.AddTorque(_torqueBeforePause, ForceMode2D.Impulse);
                Rigidbody.velocity = _velocityBeforePause;
            }
        }

        public override GameObject PlaybackRecording(int framesToRewind)
        {
            GameObject clone = base.PlaybackRecording(framesToRewind);

            clone.GetComponent<MonoBehaviour>().GetSolidCollider().enabled = false;

            return clone;
        }
    } 
}
