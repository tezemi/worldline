// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Combat;
using Worldline.Visuals;
using MEC;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is used to apply TimeEffects to GameObjects that 
    /// have a component which inherits from the Combatant class. Handles
    /// the "shattering" and "chronoshift" mechanics. Requires GameObject 
    /// have a Combatant component attached.
    /// </summary>
    [RequireComponent(typeof(Combatant))]
    public class CombatantTimeOperator : PhysicsTimeOperator
    {
        /// <summary>
        /// The percentage of a Combatant's health that has to be dealt in 
        ///  damage in order to shatter from TimeEffect.Pause.
        /// </summary>
        public float PauseBreak { get; set; } = 0.5f;
        /// <summary>
        /// The percentage of a Combatant's health that has to be dealt in 
        ///  damage in order to shatter from TimeEffect.Slow.
        /// </summary>
        public float SlowBreak { get; set; } = 0f;
        /// <summary>
        /// The percentage of a Combatant's health that has to be dealt in 
        ///  damage in order to shatter from TimeEffect.Rewind.
        /// </summary>
        public float RewindBreak { get; set; } = .15f;
        /// <summary>
        /// The percentage of a Combatant's health that has to be dealt in 
        ///  damage in order to shatter from TimeEffect.FastForward.
        /// </summary>
        public float FastForwardBreak { get; set; } = .15f;
        /// <summary>
        /// When a GameObject is "shattered," this will be the amount of time 
        /// it takes before the GameObject can be effected by TimeEffects again.
        /// </summary>
        public float BreakCooldownTime { get; set; } = 6f;
        /// <summary>
        /// If true, this GameObject cannot be effected by any TimeEffects.
        /// </summary>
        public bool Shattered { get; protected set; }
        /// <summary>
        /// The amount of damage that has been taken while under the most recently 
        /// applied TimeEffect, excluding TimeEffect.None.
        /// </summary>
        public int DamageTakenWhileUnderEffect { get; protected set; }
        /// <summary>
        /// The amount of time in seconds to apply a certain TimeEffect for chronoshift.
        /// </summary>
        public const float ChronoShiftCooldownTime = 4f;
        protected Combatant Combatant { get; set; }
        protected DistortionAnimation AddedDistortionAnimation { get; set; }
        protected TrailAnimation TimeEffectTrailAnimation { get; set; }

        /// <summary>
        /// Sets the current TimeEffect if this GameObject is not shattered.
        /// </summary>
        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (Shattered && value != TimeEffect.None)
                {
                    return;
                }

                base.CurrentTimeEffect = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            Combatant = GetComponent<Combatant>();
        }

        protected override void ApplyFastForward()
        {
            base.ApplyFastForward();
            TimeEffectTrailAnimation = TrailAnimation.Create(gameObject, 0.05f, 0.06f);
        }

        /// <summary>
        /// Resets DamageTakenWhileUnderEffect.
        /// </summary>
        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();
            DamageTakenWhileUnderEffect = 0;
            if (TimeEffectTrailAnimation != null)
            {
                Destroy(TimeEffectTrailAnimation);
            }
        }

        public override GameObject PlaybackRecording(int framesToRewind)
        {
            GameObject clone = base.PlaybackRecording(framesToRewind);

            clone.GetComponent<Combatant>().Invulnerable = true;

            return clone;
        }

        /// <summary>
        /// This gets called by the attached Combatant whenever it takes damage.
        /// If the CurrentTimeEffect is anything but TimeEffect.None, then this
        /// will increase the DamageTakenWhileUnderEffect. If 
        /// DamageTakenWhileUnderEffect exceeds the limit for the particular
        /// TimeEffect, then this GameObject will shatter.
        /// </summary>
        /// <param name="damage">The amount of damage taken.</param>
        /// <param name="other">The object that is doing the damage.</param>
        public void Hurt(int damage, object other)
        {
            if (CurrentTimeEffect != TimeEffect.None)
            {
                DamageTakenWhileUnderEffect += damage;
                switch (CurrentTimeEffect)
                {
                    case TimeEffect.Pause:
                        if (DamageTakenWhileUnderEffect > Combatant.MaxHealth - Combatant.MaxHealth * PauseBreak)
                        {
                            Shatter();
                        }

                        break;
                    case TimeEffect.Slow:
                        if (DamageTakenWhileUnderEffect > Combatant.MaxHealth - Combatant.MaxHealth * SlowBreak)
                        {
                            Shatter();
                        }

                        break;
                    case TimeEffect.Rewind:
                        if (DamageTakenWhileUnderEffect > Combatant.MaxHealth - Combatant.MaxHealth * RewindBreak)
                        {
                            Shatter();
                        }

                        break;
                    case TimeEffect.FastForward:
                        if (DamageTakenWhileUnderEffect > Combatant.MaxHealth * FastForwardBreak)
                        {
                            Shatter();
                        }

                        break;
                    case TimeEffect.Record:
                    case TimeEffect.Reset:
                    case TimeEffect.None:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
            else if (other is IImbueable imbueable)
            {
                IImbueable imb = imbueable;
                switch (imb.Imbuement)
                {
                    case TimeEffect.None:
                        break;
                    case TimeEffect.Pause:
                        ApplyChronoshift(imb.Imbuement);
                        break;
                    case TimeEffect.Slow:
                        ApplyChronoshift(imb.Imbuement);
                        break;
                    case TimeEffect.Rewind:
                        ApplyChronoshift(imb.Imbuement);
                        break;
                    case TimeEffect.FastForward:
                    case TimeEffect.Record:
                    case TimeEffect.Reset:
                        break;
                    default:
                        throw new ArgumentException();
                }
            }
        }

        /// <summary>
        /// Adds Chronoshift to this GameObject. This normally gets called by 
        /// the attached Combatant when it gets hurt by something with Imbuement.
        /// If the Item2 value of ChronoShiftAmount exceeds ChronoShiftCap, then
        /// this TimeOperator's CurrentTimeEffect will temporarily change to 
        /// the Item1 value of ChronoShiftAmount for ChronoShiftCooldownTime in
        /// seconds.
        /// </summary>
        /// <param name="effect">The TimeEffect to apply or add to.</param>
        public void ApplyChronoshift(TimeEffect effect)
        {
            if (CurrentTimeEffect != TimeEffect.None || Shattered)
            {
                return;
            }

            CurrentTimeEffect = effect;
            Timing.RunCoroutine(CooldownChronoshift(effect), Segment.FixedUpdate);

            IEnumerator<float> CooldownChronoshift(TimeEffect timeEffect)
            {
                yield return Timing.WaitForSeconds(ChronoShiftCooldownTime);
                if (CurrentTimeEffect == timeEffect)
                {
                    CurrentTimeEffect = TimeEffect.None;
                }
            }
        }

        /// <summary>
        /// Adds a DistortionAnimation to the GameObject and sets Shattered to 
        /// true for BreakCooldownTime in seconds, preventing any TimeEffects
        /// to effect this GameObject.
        /// </summary>
        public virtual void Shatter()
        {
            Shattered = true;
            CurrentTimeEffect = TimeEffect.None;
            if (gameObject.GetComponent<DistortionAnimation>() == null)
            {
                AddedDistortionAnimation = gameObject.AddComponent<DistortionAnimation>();
            }

            Timing.RunCoroutine(Cooldown(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> Cooldown()
            {
                yield return Timing.WaitForSeconds(BreakCooldownTime);
                Shattered = false;
                if (AddedDistortionAnimation != null)
                {
                    Destroy(AddedDistortionAnimation);
                }
            }
        }
    }
}
