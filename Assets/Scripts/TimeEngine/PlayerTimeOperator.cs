// Copyright (c) 2019 Destin Hebner
using System.Diagnostics.CodeAnalysis;
using Worldline.Level;
using Worldline.Player;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.Level.Obstacles;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This is the TimeOperator for the player GameObject. This main purpose 
    /// of this behaviour is to implement the code for player-specific time
    /// effects. Notably, fast-forward, record, and reset, which all function
    /// differently for the player when compared to most other GameObjects.
    /// </summary>
    [SuppressMessage("ReSharper", "ConditionIsAlwaysTrueOrFalse")]
    public class PlayerTimeOperator : CombatantTimeOperator
    {
        /// <summary>
        /// This is true when <see cref="IsResetClone"/> is true and this is inside a 
        /// <see cref="EssenceBlob"/>.
        /// </summary>
        public bool IsInsideAura { get; set; }
        /// <summary>
        /// This is true when this <see cref="PlayerOperator"/> was created inside a reset
        /// <see cref="EssenceBlob"/>.
        /// </summary>
        public bool IsResetClone { get; set; }
        /// <summary>
        /// If true, when the player is being reset, and the effect ends, this 
        /// component will make the player invincible for a few seconds. This
        /// is normally called by the DeadPlayerTimeOperator, as this occurs
        /// after the player uses reset while they're dead.
        /// </summary>
        public bool MakePlayerInvincibleAfterReset { get; set; }
        /// <summary>
        /// The default jump power fast-forward multiplier, being 1.5, is too 
        /// high, so this used instead.
        /// </summary>
        public const float FastForwardJumpPowerScale = 1.25f;
        protected PlayerOperator PlayerOperator { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        private float _walkSpeedAdjustment;
        private float _jumpPowerAdjustment;
        private float _accelerationAdjustment;
        
        protected override void Awake()
        {
            base.Awake();
            PlayerOperator = GetComponent<PlayerOperator>();
            MovementOperator = GetComponent<MovementOperator>();
            _walkSpeedAdjustment = PlayerOperator.WalkSpeed * FastForwardScale - PlayerOperator.WalkSpeed;
            _jumpPowerAdjustment = MovementOperator.JumpPower * FastForwardJumpPowerScale - MovementOperator.JumpPower;
            _accelerationAdjustment = MovementOperator.Acceleration * FastForwardScale - MovementOperator.Acceleration;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            foreach (GameObject g in RecordingClones)
            {
                g.GetComponent<TimeOperator>().Evaporate();
            }

            RecordingClones.Clear();
        }

        public override void Shatter()
        {
            base.Shatter();
            if (EssenceOperator.Main.PlayerEffectIsActive)
            {
                EssenceOperator.Main.EndEffect();
            }
        }

        protected override void ApplyPause()
        {
            base.ApplyPause();
            if (GetComponent<RecordingPlayerOperator>() != null)
            {
                Rigidbody.bodyType = RigidbodyType2D.Kinematic;
            }

            BackgroundMusicOperator.Main.Speed = 0f;
        }

        protected override void ApplySlow()
        {
            base.ApplySlow();
            BackgroundMusicOperator.Main.Speed = 0.5f;
        }

        protected override void ApplyRewind()
        {
            base.ApplyRewind();
            BackgroundMusicOperator.Main.Speed = -1f;
        }

        protected override void ApplyFastForward()
        {
            base.ApplyFastForward();
            BackgroundMusicOperator.Main.Speed = 2f;
            PlayerOperator.WalkSpeed += _walkSpeedAdjustment;
            MovementOperator.JumpPower += _jumpPowerAdjustment;
            MovementOperator.Acceleration += _accelerationAdjustment;            
        }

        protected override void ApplyReset()
        {
            if (Recorder != null)
            {
                Recorder.Mode = RecorderMode.Rewinding;
            }

            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            GetComponent<SpriteRenderer>().color = EffectColors[TimeEffect.Reset];
            GetComponent<PlayerOperator>().Invulnerable = true;
            TimeEffectTrailAnimation = TrailAnimation.Create(gameObject);
        }

        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();

            BackgroundMusicOperator.Main.Speed = 1f;
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

            if (PreviousEffect == TimeEffect.Reset)
            {
                GetComponent<PlayerOperator>().Invulnerable = false;
                if (MakePlayerInvincibleAfterReset)
                {
                    GetComponent<PlayerOperator>().MakeInvulnerable();
                    GetComponent<PlayerOperator>().ImmuneToPit = false;
                    MakePlayerInvincibleAfterReset = false;
                }
            }
            else if (PreviousEffect == TimeEffect.FastForward)
            {
                PlayerOperator.WalkSpeed -= _walkSpeedAdjustment;
                MovementOperator.JumpPower -= _jumpPowerAdjustment;
                MovementOperator.Acceleration -= _accelerationAdjustment;
            }
        }
    }
}
