// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is used to apply effect colors to this GameObject, 
    /// and implement other visual details. As such, it is required that the
    /// GameObject have a SpriteRenderer.
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class VisualTimeOperator : TimeOperator
    {
        [Tooltip("If true, this will blend its color with the SpriteRenderer's current color instead of overriding it.")]
        public bool BlendColor;
        protected SpriteRenderer SpriteRenderer { get; set; }
        private Color _originalColor;

        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (CurrentTimeEffect == TimeEffect.None)
                {
                    _originalColor = SpriteRenderer.color;
                }

                base.CurrentTimeEffect = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            SpriteRenderer = GetComponent<SpriteRenderer>();
        }

        /// <summary>
        /// Applies the TimeEffect.Pause color.
        /// </summary>
        protected override void ApplyPause()
        {
            base.ApplyPause();
            Color color;
            if (BlendColor)
            {
                color = SpriteRenderer.color.BlendColors(EffectColors[TimeEffect.Pause]);
            }
            else
            {
                color = EffectColors[TimeEffect.Pause];
            }

            SpriteRenderer.color = color;
            ColorChildren(color, gameObject);
        }

        /// <summary>
        /// Applies the TimeEffect.Slow color.
        /// </summary>
        protected override void ApplySlow()
        {
            base.ApplySlow();
            Color color;
            if (BlendColor)
            {
                color = SpriteRenderer.color.BlendColors(EffectColors[TimeEffect.Slow]);
            }
            else
            {
                color = EffectColors[TimeEffect.Slow];
            }

            SpriteRenderer.color = color;
            ColorChildren(color, gameObject);
        }

        /// <summary>
        /// Applies the TimeEffect.Rewind color.
        /// </summary>
        protected override void ApplyRewind()
        {
            base.ApplyRewind();
            Color color;
            if (BlendColor)
            {
                color = SpriteRenderer.color.BlendColors(EffectColors[TimeEffect.Rewind]);
            }
            else
            {
                color = EffectColors[TimeEffect.Rewind];
            }

            SpriteRenderer.color = color;
            ColorChildren(color, gameObject);
        }

        /// <summary>
        /// Applies the TimeEffect.FastFroward color, and adds a trail.
        /// </summary>
        protected override void ApplyFastForward()
        {
            base.ApplyFastForward();
            Color color;
            if (BlendColor)
            {
                color = SpriteRenderer.color.BlendColors(EffectColors[TimeEffect.FastForward]);
            }
            else
            {
                color = EffectColors[TimeEffect.FastForward];
            }

            SpriteRenderer.color = color;
            ColorChildren(color, gameObject);
        }

        /// <summary>
        /// Sets this GameObject's color to white.
        /// </summary>
        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();
            if (BlendColor)
            {
                SpriteRenderer.color = _originalColor;
                ColorChildren(_originalColor, gameObject);
            }
            else
            {
                SpriteRenderer.color = Color.white;
                ColorChildren(Color.white, gameObject);
            }
        }

        /// <summary>
        /// Sets the color for all of the children under this game object.
        /// </summary>
        /// <param name="color">The color to set the children to.</param>
        /// <param name="gameObj">The GameObject to apply the color to.</param>
        protected virtual void ColorChildren(Color color, GameObject gameObj)
        {
            if (gameObj == null)
            {
                return;
            }

            foreach (SpriteRenderer spriteRenderer in gameObj.GetComponentsInChildren<SpriteRenderer>())
            {
                spriteRenderer.color = color;
            }
        }

        public override GameObject PlaybackRecording(int framesToRewind)
        {
            GameObject clone = base.PlaybackRecording(framesToRewind);

            Color color = EffectColors[TimeEffect.Record] - new Color(0f, 0f, 0f, 0.25f);
            clone.GetComponent<SpriteRenderer>().color = color;
            ColorChildren(color, clone);

            return clone;
        }

        /// <summary>
        /// Kills the GameObject after showing a visual effect.
        /// TODO: Pool this effect.
        /// </summary>
        public override void Evaporate()
        {
            GameObject evaporation = new GameObject("Evap", typeof(SpriteRenderer), typeof(DistortionAnimation));
            SpriteRenderer evapRenderer = evaporation.GetComponent<SpriteRenderer>();
            evapRenderer.sprite = SpriteRenderer.sprite;
            evapRenderer.color = SpriteRenderer.color - new Color(0, 0, 0, .2f);
            evapRenderer.flipX = SpriteRenderer.flipX;
            evapRenderer.flipY = SpriteRenderer.flipY;
            evaporation.transform.position = transform.position + new Vector3(0f, 0f, 0.15f);
            evaporation.transform.localScale = transform.localScale;
            evaporation.transform.rotation = transform.rotation;
            Timing.RunCoroutine(PhaseOut(), Segment.FixedUpdate, GetInstanceID().ToString());
            Destroy(gameObject);

            IEnumerator<float> PhaseOut()
            {
                bool decreaseAlpha = true;
                while (evapRenderer.color.a > 0)
                {
                    yield return Timing.WaitForSeconds(0.05f);
                    if (evapRenderer == null) yield break;

                    if (decreaseAlpha)
                    {
                        evapRenderer.color -= new Color(0f, 0f, 0f, 0.05f);
                        decreaseAlpha = false;
                    }
                    else
                    {
                        evapRenderer.color += new Color(0f, 0f, 0f, 0.02f);
                        decreaseAlpha = true;
                    }
                }

                Destroy(evaporation);
            }
        }
    }
}
