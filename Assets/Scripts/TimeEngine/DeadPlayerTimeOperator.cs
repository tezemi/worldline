﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Visuals;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is used by the player's dead corpse. Because only
    /// reset should be able to effect the dead player, this will do nothing
    /// unless the time effect being set is reset or none.
    /// </summary>
    [DisableOnPlayback]
    public class DeadPlayerTimeOperator : PhysicsTimeOperator
    {
        private TrailAnimation _timeEffectTrailAnimation;

        /// <summary>
        /// Sets the current time effect to reset or nothing, if anything else
        /// is set, then nothing will happen.
        /// </summary>
        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (value == TimeEffect.Reset)
                {
                    base.CurrentTimeEffect = value;
                }
            }
        }

        /// <summary>
        /// Performs the normal functionality of reset in regards to the player, 
        /// but with some cosmetic changes.
        /// </summary>
        protected override void ApplyReset()
        {
            if (Recorder != null)
            {
                Recorder.Mode = RecorderMode.Rewinding;
            }

            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            GetComponent<SpriteRenderer>().color = EffectColors[TimeEffect.Reset];
            _timeEffectTrailAnimation = TrailAnimation.Create(gameObject);
        }

        /// <summary>
        /// When the rewind runs out of memory, we resurrect the player.
        /// </summary>
        public override void HandleRewindOutOfMemory()
        {
            Destroy(_timeEffectTrailAnimation);
            CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
            MainPlayerOperator.MainPlayerObject.SetActive(true);
            MainPlayerOperator.MainPlayerObject.GetComponent<AnimationOperator>().enabled = true;
            MainPlayerOperator.MainPlayerComponent.Dead = false;
            MainPlayerOperator.MainPlayerComponent.ImmuneToPit = true;
            MainPlayerOperator.MainPlayerObject.GetComponent<PlayerTimeOperator>().MakePlayerInvincibleAfterReset = true;
            EssenceOperator.Main.ApplyPlayerEffect(TimeEffect.Reset);
            Destroy(gameObject);
        }
    }
}
