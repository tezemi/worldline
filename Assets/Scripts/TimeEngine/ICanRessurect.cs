// Copyright (c) 2019 Destin Hebner
namespace Worldline.TimeEngine
{
    /// <summary>
    /// This interface can be used for GameObjects that can be resurrected
    /// through time effects. Although the default implementation of this
    /// behavior is just setting the GameObject to active, this will allow
    /// your to specify your own behavior via the Resurrect() method.
    /// Implementing this interfaces means that the GameObject will not
    /// be automatically set to active, as this interface is designed
    /// for the implementation of completely custom behavior.
    /// </summary>
	public interface ICanRessurect
	{
        void Resurrect();
    }
}
