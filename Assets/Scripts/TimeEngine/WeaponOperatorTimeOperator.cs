// Copyright (c) 2019 Destin Hebner
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.TimeEngine
{
    public class WeaponOperatorTimeOperator : VisualTimeOperator
    {
        protected override void ApplyReset()
        {
            GetComponent<Recorder>().Mode = RecorderMode.Rewinding;
            GetComponent<SpriteRenderer>().color = EffectColors[TimeEffect.Reset];
        }

        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();
            GetComponent<SpriteRenderer>().color = Color.white;
        }
    }
}
