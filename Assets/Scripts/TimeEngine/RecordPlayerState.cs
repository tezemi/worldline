﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.TimeEngine
{
    [Flags]
    public enum RecordPlayerState : short
    {
        Idle = 0,
        WalkingRight = 1,
        WalkingLeft = 2,
        Jumping = 4,
        Mirror
    }
}
