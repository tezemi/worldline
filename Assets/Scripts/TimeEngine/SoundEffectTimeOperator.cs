﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.TimeEngine.Recording;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This is the TimeOperator for the SoundEffect component. This should 
    /// automatically be attached to GameObject's that have a SoundEffect
    /// component that indicates that it should be effected by TimeEffects.
    /// This controls the pitch of the sound. Both a SoundEffect and
    /// AudioSource must be attached to this component's GameObject.
    /// </summary>
    [RequireComponent(typeof(SoundEffect))]
    [RequireComponent(typeof(AudioSource))]
    public class SoundEffectTimeOperator : TimeOperator
    {
        public float PitchBeforeTimeEffect { get; protected set; }
        protected SoundEffect SoundEffect { get; set; }
        protected AudioSource AudioSource { get; set; }

        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (SoundEffect == null) return;

                if (SoundEffect.EffectedByTimeEngine)
                {
                    base.CurrentTimeEffect = value;
                }
            }
        }

        protected virtual void Start()
        {
            SoundEffect = GetComponent<SoundEffect>();
            AudioSource = GetComponent<AudioSource>();
        }

        protected override void ApplyPause()
        {
            base.ApplyPause();
            PitchBeforeTimeEffect = GetComponent<AudioSource>().pitch;
            AudioSource.pitch = 0f;
        }

        protected override void ApplySlow()
        {
            base.ApplySlow();
            PitchBeforeTimeEffect = GetComponent<AudioSource>().pitch;
            AudioSource.pitch = PitchBeforeTimeEffect * SlowScale;
        }

        protected override void ApplyFastForward()
        {
            base.ApplyFastForward();
            PitchBeforeTimeEffect = GetComponent<AudioSource>().pitch;
            AudioSource.pitch = PitchBeforeTimeEffect * FastForwardScale;
        }

        protected override void ApplyRewind()
        {
            PitchBeforeTimeEffect = GetComponent<AudioSource>().pitch;
            if (Recorder != null)
            {
                Recorder.Mode = RecorderMode.Rewinding;
            }
        }

        protected override void ApplyNoEffect()
        {
            base.ApplyNoEffect();
            AudioSource.pitch = PitchBeforeTimeEffect;
        }
    }
}
