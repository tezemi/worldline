﻿// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// Any Component with this attribute will disable (if it's enabled) when 
    /// the status of the attached Recorder is set to Rewinding, and re-enable 
    /// when status is change off of Rewinding.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DisableOnRewinding : Attribute
    {
        // ...
    }
}
