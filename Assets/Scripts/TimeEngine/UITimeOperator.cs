﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This TimeOperator is used to apply effect colors to this GameObject, 
    /// and implement other visual details. As such, it is required that the
    /// GameObject have a SpriteRenderer.
    /// </summary>
    public class UITimeOperator : TimeOperator
    {
        [Tooltip("If true, this will blend its color with the Image's current color instead of overriding it.")]
        public bool BlendColor;
        [CanBeNull]
        protected Image Image { get; set; }
        private Color _originalColor;

        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (CurrentTimeEffect == TimeEffect.None)
                {
                    _originalColor = Image != null ? Image.color : GetComponentInChildren<Image>().color;
                }

                Color color;
                if (BlendColor)
                {
                    if (value != TimeEffect.None)
                    {
                        color = Image != null ? Image.color.BlendColors(EffectColors[value]) : GetComponentInChildren<Image>().color;
                    }
                    else
                    {
                        color = _originalColor;
                    }
                }
                else if (value != TimeEffect.None)
                {
                    color = EffectColors[value];
                }
                else
                {
                    color = _originalColor;
                }

                if (Image != null)
                {
                    Image.color = color;
                }
                
                ColorChildren(color, gameObject);

                base.CurrentTimeEffect = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            Image = GetComponent<Image>();
        }

        /// <summary>
        /// Sets the color for all of the children under this game object.
        /// </summary>
        /// <param name="color">The color to set the children to.</param>
        /// <param name="gameObj">The GameObject to apply the color to.</param>
        protected virtual void ColorChildren(Color color, GameObject gameObj)
        {
            if (gameObj == null)
            {
                return;
            }

            foreach (Image image in gameObj.GetComponentsInChildren<Image>())
            {
                image.color = color;
            }
        }
    }
}
