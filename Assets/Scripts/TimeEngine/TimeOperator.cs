// Copyright (c) 2019 Destin Hebner
using System;
using System.ComponentModel;
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Player.Weapons;
using Worldline.TimeEngine.Recording;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.TimeEngine
{
    /// <summary>
    /// This is the base class for all entities that can be effected by the
    /// time engine. Implements some very basic behavior, such as setting
    /// the time value of TimeScale, and creating recording clones.
    /// </summary>
    [HasDefaultState]
    public class TimeOperator : MonoBehaviour
    {
        public static List<TimeOperator> AllTimeOperators { get; protected set; } = new List<TimeOperator>();
        [Tooltip("The number of recording clones this object will create when record is actiavted.")]
        public int NumberOfClones = 3;
        [Tooltip("The amount of time in the past, in frames, each clone is created.")]
        public int CloneGapSize = 45;
        /// <summary>
        /// If true, this indicates that this object spawned when the level loaded, 
        /// and upon running out of rewind memory, this object should just stand still,
        /// or engage in some sort of idle behavior. If false, this object should get
        /// destroyed.
        /// </summary>
        public bool SpawnedOnLevelLoad { get; set; }
        /// <summary>
        /// This is the amount of time in seconds to go back when generating a 
        /// recording clone.
        /// </summary>
        public float RecordDistance { get; set; } = 10f;
        /// <summary>
        /// The current time scale of this TimeOperator, where 1 is considered 
        /// normal time. When slowed, this will be 0.5, when fast-forwarded, this
        /// will be 1.5, and when rewinding, this will be -1.
        /// </summary>
        public float TimeScale { get; set; } = 1f;
        /// <summary>
        /// If true this is a recording clone.
        /// </summary>
        public bool IsClone { get; protected set; }
        /// <summary>
        /// This contains the previously set time effect, before the current 
        /// one. This is good when comparing changes between time effects.
        /// </summary>
        public TimeEffect PreviousEffect { get; protected set; } = TimeEffect.None;
        /// <summary>
        /// If this GameObject is under the reset effect, then this should hold
        /// a clone GameObject that mimicked the GameObject's behavior from the
        /// past for one frame, and is now operating independently.
        /// </summary>
        public GameObject ResetClone { get; protected set; }
        /// <summary>
        /// If this GameObject is under the record effect, then this should hold 
        /// a clone GameObject that is mimicking this GameObject's behavior from 
        /// the past.
        /// </summary>
        public List<GameObject> RecordingClones { get; protected set; } = new List<GameObject>();
        /// <summary>
        /// Returns true if this entity is under a TimeEffect that should disable 
        /// behavior that effects movement, sprites, color, rotation, ect.
        /// </summary>
        public bool ShouldHold => CurrentTimeEffect == TimeEffect.Pause || IsRewindEffect;
        /// <summary>
        /// True if any TimeEffect is active.
        /// </summary>
        public bool AnyEffect => CurrentTimeEffect != TimeEffect.None;
        /// <summary>
        /// True if no effect is active.
        /// </summary>
        public bool NoEffect => CurrentTimeEffect == TimeEffect.None;
        /// <summary>
        /// The number of frames in the past the a recording should start at.
        /// </summary>
        public const int RecordFramesToRewind = 120;
        /// <summary>
        /// The normal timescale for LocalClocks.
        /// </summary>
        public const float NormalScale = 1;
        /// <summary>
        /// The paused timescale for LocalClocks.
        /// </summary>
        public const float PauseScale = 0f;
        /// <summary>
        /// The slowed timescale for LocalClocks.
        /// </summary>
        public const float SlowScale = 0.5f;
        /// <summary> 
        /// The rewinding timescale for LocalClocks, because LocalClocks are not 
        /// used for rewind behavior, this value is the same as NormalScale.
        /// </summary>
        public const float RewindScale = -1f;
        /// <summary>
        /// The fast-forwarding timescale for LocalClocks;
        /// </summary>
        public const float FastForwardScale = 2;
        /// <summary>
        /// When resurrecting something from death using rewind, there is a brief 
        /// period where whatever  triggers the object to die might still be true, 
        /// for example, an enemy at zero health, or a bullet against a wall. We
        /// give resurrected entities some delay where they cannot be killed to 
        /// prevent this from happening.
        /// </summary>
        public const float StandardResurrectionInvincibilityTime = .1f;
        [CanBeNull]
        protected Recorder Recorder { get; set; }
        private bool _ignoreActiveState;
        private TimeEffect _currentTimeEffect = TimeEffect.None;
        
        /// <summary>
        /// Colors associated with each time effect.
        /// </summary>
        public static readonly Dictionary<TimeEffect, Color> EffectColors = new Dictionary<TimeEffect, Color>
        {
            {TimeEffect.Pause, new Color(0.27451f, 0.27451f, 1f, 1f)},
            {TimeEffect.Slow, new Color(0.99216f, 0.79216f, 0.25098f, 1f)},
            {TimeEffect.Rewind, new Color(0.60784f, 0.89804f, 0.39216f, 1f)},
            {TimeEffect.FastForward, new Color(0.643f, 0.011f, 0.435f)},
            {TimeEffect.Record, new Color(0.89f, 0.09f, 0.039f, 1f)},
            {TimeEffect.Reset, new Color(1f, 1f, 1f, .25f)}
        };

        /// <summary>
        /// The current time effect of this TimeOperator.
        /// </summary>
        public virtual TimeEffect CurrentTimeEffect
        {
            get => _currentTimeEffect;
            set
            {
                if ((!isActiveAndEnabled && !_ignoreActiveState) || _currentTimeEffect == value || IsClone)
                {
                    return;
                }

                PreviousEffect = _currentTimeEffect;

                // If we are changing between two TimeEffects (neither of which are no
                // effect), then call ApplyNoEffect which does some important cleaning
                // up before applying the new effect.
                if (_currentTimeEffect != TimeEffect.None && value != TimeEffect.None)
                {
                    ApplyNoEffect();
                }

                // To make implementation of time-control-specific code easy, we simply
                // call a virtual method based on the specified effect.
                switch (value)
                {
                    case TimeEffect.Pause:
                        ApplyPause();
                        break;
                    case TimeEffect.Slow:
                        ApplySlow();
                        break;
                    case TimeEffect.Rewind:
                        ApplyRewind();
                        break;
                    case TimeEffect.FastForward:
                        ApplyFastForward();
                        break;
                    case TimeEffect.Record:
                        ApplyRecord();
                        break;
                    case TimeEffect.Reset:
                        ApplyReset();
                        break;
                    case TimeEffect.None:
                        ApplyNoEffect();
                        break;
                    default:
                        throw new InvalidEnumArgumentException();
                }

                // Apply to Children
                if (value != TimeEffect.Record)
                {
                    for (int i = 0; i < transform.childCount; i++)
                    {
                        Transform t = transform.GetChild(i);
                        if (t.GetComponent<TimeOperator>() != null)
                        {
                            t.GetComponent<TimeOperator>().CurrentTimeEffect = value;
                        }
                    }
                }

                _currentTimeEffect = value;
            }
        }

        /// <summary>
        /// Returns true if this entity is being rewound, either via rewind or 
        /// reset.
        /// </summary>
        public bool IsRewindEffect
        {
            get
            {
                switch (_currentTimeEffect)
                {
                    case TimeEffect.Pause:
                        return false;
                    case TimeEffect.Slow:
                        return false;
                    case TimeEffect.Rewind:
                        return true;
                    case TimeEffect.FastForward:
                        return false;
                    case TimeEffect.Record:
                        return false;
                    case TimeEffect.Reset:
                        return true;
                    case TimeEffect.None:
                        return false;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }
        
        protected virtual void Awake()
        {
            if (GetComponent<Recorder>() != null)
            {
                Recorder = GetComponent<Recorder>();
            }

            AllTimeOperators.Add(this);
        }

        protected virtual void OnDisable()
        {
            foreach (GameObject clone in RecordingClones)
            {
                if (clone == null || !clone.activeSelf) continue;

                clone.SetActive(false);
            }
        }

        protected virtual void OnEnable()
        {
            foreach (GameObject clone in RecordingClones)
            {
                if (clone == null || clone.activeSelf) continue;

                clone.SetActive(true);
            }
        }

        protected virtual void OnDestroy()
        {
            AllTimeOperators.Remove(this);
            foreach (GameObject clone in RecordingClones)
            {
                if (clone == null) continue;

                Destroy(clone);
            }
        }

        /// <summary>
        /// Applies the TimeEffect.Pause to this GameObject. The most basic 
        /// implementation of this is setting the LocalClock's timescale to
        /// PauseScale.
        /// </summary>
        protected virtual void ApplyPause()
        {
            TimeScale = PauseScale;
        }

        /// <summary>
        /// Applies the TimeEffect.Slow to this GameObject. The most basic 
        /// implementation of this is setting the LocalClock's timescale to
        /// SlowScale.
        /// </summary>
        protected virtual void ApplySlow()
        {
            TimeScale = SlowScale;
        }

        /// <summary>
        /// Applies the TimeEffect.Rewind to this GameObject. The most basic 
        /// implementation of this is setting the attached Recorder's status
        /// to Rewinding.
        /// </summary>
        protected virtual void ApplyRewind()
        {
            TimeScale = RewindScale;
            if (Recorder != null)
            {
                Recorder.Mode = RecorderMode.Rewinding;
            }
        }

        /// <summary>
        /// Applies the TimeEffect.FastForward to this GameObject. The most basic 
        /// implementation of this is setting the LocalClock's timescale to
        /// FastForwardScale.
        /// </summary>
        protected virtual void ApplyFastForward()
        {
            TimeScale = FastForwardScale;
        }

        /// <summary>
        /// Applies the TimeEffect.Record to this GameObject. The most basic 
        /// implementation of this is setting creating a clone of this
        /// GameObject and setting it's recorder to RecordDistance in the
        /// past and setting it to playback mode.
        /// </summary>
        protected virtual void ApplyRecord()
        {
            for (int i = 0; i < NumberOfClones; i++)
            {
                GameObject recording = PlaybackRecording((i + 1) * CloneGapSize);
                RecordingClones.Add(recording);
            }
        }

        /// <summary>
        /// Applies the TimeEffect.Reset to this GameObject. The most basic 
        /// implementation of this is setting creating a clone of this
        /// GameObject and setting it's recorder to RecordDistance in the
        /// past, but then allowing it to behave like an independent entity
        /// afterward.
        /// </summary>
        protected virtual void ApplyReset()
        {
            int framesToRewind = RecordFramesToRewind;
            ResetClone = Instantiate(gameObject, gameObject.transform.parent);
            ResetClone.name = ResetClone.name.Replace("(Clone)", "(From Reset)");
            ResetClone.GetComponent<Recorder>().UseSnapshots = GetComponent<Recorder>();
            ResetClone.GetComponent<Recorder>().Rewind(framesToRewind, true);
            ResetClone.GetComponent<Recorder>().UseSnapshots = null;
            ResetChildren(ResetClone, gameObject);

            void ResetChildren(GameObject parentClone, GameObject parentOriginal)
            {
                for (int i = 0; i < parentClone.transform.childCount; i++)
                {
                    parentClone.transform.GetChild(i).GetComponent<Recorder>().UseSnapshots = parentOriginal.transform.GetChild(i).GetComponent<Recorder>();
                    parentClone.transform.GetChild(i).GetComponent<Recorder>().Rewind(framesToRewind, true);
                    parentClone.transform.GetChild(i).GetComponent<Recorder>().UseSnapshots = null;
                    ResetChildren
                    (
                        parentClone.transform.GetChild(i).gameObject, 
                        parentOriginal.transform.GetChild(i).gameObject
                    );
                }
            }
        }

        /// <summary>
        /// Applies the TimeEffect.None to this GameObject. The most basic implementation
        /// of which sets the LocalClock's timescale to NormalScale, sets the recorder's
        /// mode to Recording, and kills a RecordingClone if it exists.
        /// </summary>
        protected virtual void ApplyNoEffect()
        {
            TimeScale = NormalScale;
            if (Recorder != null)
            {
                Recorder.Mode = RecorderMode.Recording;
            }

            foreach (GameObject g in RecordingClones)
            {
                g.GetComponent<TimeOperator>().Evaporate();
            }

            RecordingClones.Clear();
        }

        /// <summary>
        /// Creates a duplicate GameObject that is the specified amount of 
        /// frames in the past. That GameObject will then use the current's
        /// memory to simulate playback.
        /// </summary>
        /// <param name="framesToRewind">The amount of frames to go back.</param>
        /// <returns>The duplicate GameObject.</returns>
        public virtual GameObject PlaybackRecording(int framesToRewind)
        {
            GameObject clone = Instantiate(gameObject);
            clone.name = clone.name.Replace("(Clone)", "(Recording)");
            clone.GetComponent<TimeOperator>().IsClone = true;
            if (GetComponentInChildren<PlayerWeaponOperator>() != null && clone.GetComponentInChildren<PlayerWeaponOperator>() != null)
            {
                clone.GetComponentInChildren<PlayerWeaponOperator>().SyncWeapons(GetComponentInChildren<PlayerWeaponOperator>());
            }

            if (clone.GetComponent<TrailRenderer>() != null)
            {
                Destroy(clone.GetComponent<TrailRenderer>());
            }

            clone.GetComponent<Recorder>().UseSnapshots = GetComponent<Recorder>();
            clone.GetComponent<Recorder>().CurrentIndex = GetComponent<Recorder>().CurrentIndex;
            clone.GetComponent<Recorder>().Rewind(framesToRewind, false);
            clone.GetComponent<Recorder>().Mode = RecorderMode.Playback;
            StartPlaybackInChildren(clone, gameObject);

            void StartPlaybackInChildren(GameObject parentClone, GameObject parentOriginal)
            {
                if (parentClone.transform.childCount <= 0) return;

                for (int i = 0; i < parentClone.transform.childCount; i++)
                {
                    if (parentClone.transform.GetChild(i).gameObject.activeInHierarchy)
                    {
                        if (parentClone.transform.GetChild(i).GetComponent<Recorder>() == null) continue;

                        parentClone.transform.GetChild(i).GetComponent<Recorder>().UseSnapshots = parentOriginal.transform.GetChild(i).GetComponent<Recorder>();
                        parentClone.transform.GetChild(i).GetComponent<Recorder>().CurrentIndex = parentOriginal.transform.GetChild(i).GetComponent<Recorder>().CurrentIndex;
                        parentClone.transform.GetChild(i).GetComponent<Recorder>().Rewind(framesToRewind, false);
                        parentClone.transform.GetChild(i).GetComponent<Recorder>().Mode = RecorderMode.Playback;
                    }

                    StartPlaybackInChildren
                    (
                        parentClone.transform.GetChild(i).gameObject,
                        parentOriginal.transform.GetChild(i).gameObject
                    );
                }
            }

            return clone;
        }

        /// <summary>
        /// Gets called by the attached Recorder when the Recorder has run out 
        /// of snapshots to rewind. If the entity was spawned when the level
        /// loaded, then it will engage in some sort of idle behavior, but if
        /// the entity was spawned in later, then it will be destroyed or killed.
        /// </summary>
        public virtual void HandleRewindOutOfMemory()
        {
            if (!SpawnedOnLevelLoad)
            {
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// A purely visual effect that makes a GameObject look like it's 
        /// "phasing out of existence" before it is destroyed. On the base
        /// TimeOperator, this just destroys the GameObject.
        /// </summary>
        public virtual void Evaporate()
        {
            foreach (GameObject g in RecordingClones)
            {
                Destroy(g);
            }
        }

        public virtual void ForceSetTimeEffect(TimeEffect timeEffect)
        {
            _ignoreActiveState = true;
            CurrentTimeEffect = timeEffect;
            _ignoreActiveState = false;
        }

        /// <summary>
        /// Gets a number after the CurrentTimeEffect has been applied to it
        /// For example, if the CurrentTimeEffect was TimeEffect.Slow, then the
        /// 1f would become 0.5f, but if the number was 10f, the number would
        /// become 5f.
        /// </summary>
        /// <param name="input">The starting value.</param>
        /// <returns>The value after the CurrentTimeEffect has been applied.</returns>
        public virtual float GetScaledValue(float input)
        {
            return CurrentTimeEffect == TimeEffect.None ? input :
            CurrentTimeEffect == TimeEffect.Pause ? 0f :
            CurrentTimeEffect == TimeEffect.Slow ? input * SlowScale :
            CurrentTimeEffect == TimeEffect.Rewind ? input * RewindScale :
            CurrentTimeEffect == TimeEffect.FastForward ? input * FastForwardScale :
            input;
        }

        public virtual Vector3 GetScaledValue(Vector3 input)
        {
            return CurrentTimeEffect == TimeEffect.None ? input :
            CurrentTimeEffect == TimeEffect.Pause ? Vector3.zero :
            CurrentTimeEffect == TimeEffect.Slow ? input * SlowScale :
            CurrentTimeEffect == TimeEffect.Rewind ? input * RewindScale :
            CurrentTimeEffect == TimeEffect.FastForward ? input * FastForwardScale :
            input;
        }

        public virtual Vector2 GetScaledValue(Vector2 input)
        {
            return CurrentTimeEffect == TimeEffect.None ? input :
            CurrentTimeEffect == TimeEffect.Pause ? Vector2.zero :
            CurrentTimeEffect == TimeEffect.Slow ? input * SlowScale :
            CurrentTimeEffect == TimeEffect.Rewind ? input * RewindScale :
            CurrentTimeEffect == TimeEffect.FastForward ? input * FastForwardScale :
            input;
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void ResetDefaults()
        {
            AllTimeOperators = new List<TimeOperator>();
        }
    }
}