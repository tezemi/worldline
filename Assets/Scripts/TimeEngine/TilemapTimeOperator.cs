﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.TimeEngine
{
    public class TilemapTimeOperator : TimeOperator
    {
        protected Tilemap Tilemap { get; set; }

        public override TimeEffect CurrentTimeEffect
        {
            get => base.CurrentTimeEffect;
            set
            {
                if (value != TimeEffect.None && CurrentTimeEffect != TimeEffect.None) return;

                base.CurrentTimeEffect = value;
                if (value == TimeEffect.Record || value == TimeEffect.Reset) return;

                if (value != TimeEffect.None)
                {
                    Tilemap.color = EffectColors[value];
                    ColorAll(EffectColors[value]);
                }
                else
                {
                    Tilemap.color = Color.white;
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();
            Tilemap = GetComponent<Tilemap>();
        }

        protected virtual void ColorAll(Color color)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Transform t = transform.GetChild(i);
                if (t.GetComponent<SpriteRenderer>() != null)
                {
                    t.GetComponent<SpriteRenderer>().color = color;
                    if (t.GetComponent<VisualTimeOperator>() != null)
                    {
                        t.GetComponent<TilemapTimeOperator>().ColorAll(color);
                    }
                }
            }
        }
    }
}
