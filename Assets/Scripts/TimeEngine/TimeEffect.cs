// Copyright (c) 2019 Destin Hebner
namespace Worldline.TimeEngine
{
	public enum TimeEffect 
	{
		Pause,
        Slow,
        Rewind,
        FastForward,
        Record,
        Reset,
        None
	}
}
