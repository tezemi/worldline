﻿using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEditor.EditorTools;

namespace Worldline.EditorExtensions
{
    [InitializeOnLoad]
    [EditorTool("Snapped Transform Editor", typeof(GameObject))]
    internal class SnappedTransformEditor : EditorTool
    {
        public override GUIContent toolbarIcon => null;

        static SnappedTransformEditor()
        {
            Selection.selectionChanged += () =>
            {
                foreach (GameObject obj in Selection.gameObjects)
                {
                    if (obj.GetComponentInParent<Grid>() != null)
                    {
                        if (Tools.current != Tool.Custom)
                        {
                            Tools.current = Tool.Custom;    
                        }

                        break;
                    }
                }
            };
        }

        public override void OnToolGUI(EditorWindow window)
        {
            EditorGUI.BeginChangeCheck();

            Vector3 position = Tools.handlePosition;
            if (float.IsInfinity(position.x) || float.IsInfinity(position.y))
            {
                return;
            }

            Handles.DrawSolidRectangleWithOutline(new Rect(position, new Vector2(0.65f, 0.65f)), new Color(1f, 0f, 0f, 0.25f), Color.red);
            Vector3 newTargetPosition = Handles.PositionHandle(Tools.handlePosition, Tools.handleRotation);

            if (EditorGUI.EndChangeCheck())
            {
                Vector3 delta = newTargetPosition - Tools.handlePosition;
                
                Undo.RecordObjects(Selection.transforms, "Move Snapped Objects");

                foreach (var transform in Selection.transforms)
                {
                    transform.position += new Vector3(delta.x, delta.y, 0f);
                }
            }
        }
    }
}
