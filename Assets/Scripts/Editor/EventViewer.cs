﻿// Copyright (c) 2019 Destin Hebner
using System;
using UnityEditor;
using UnityEngine;
using Worldline.ScriptedSequences;

namespace Worldline.EditorExtensions
{
    /// <summary>
    /// A window that shows all the currently running events.
    /// TODO: Could be touched up. It's really ugly, and the window only updates when you click inside it.
    /// </summary>
    internal class EventViewer : EditorWindow
    {
        protected string RunningEvents { get; set; }

        [MenuItem("Window/Event Viewer")]
        public static void ShowWindow()
        {
            GetWindow(typeof(EventViewer));
        }

        protected virtual void OnGUI()
        {
            RunningEvents = string.Empty;
            foreach (ScriptedSequence e in ScriptedSequence.ActiveScriptedSequences)
            {
                RunningEvents += e.GetType().FullName + Environment.NewLine;
            }

            RunningEvents = EditorGUILayout.TextArea(RunningEvents, GUILayout.Height(60f));
        }
    }
}
