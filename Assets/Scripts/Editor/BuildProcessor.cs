﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.Build.Reporting;

namespace Worldline.EditorExtensions
{
    public class BuildProcessor
    {
        [MenuItem("Build/Build Win x64")]
        public static void Build64()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Win x64\\Worldline.exe";
            if (buildPlayerOptions.target != BuildTarget.StandaloneWindows64)
            {
                buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
            }

            DoBuild(buildPlayerOptions);
        }

        [MenuItem("Build/Build Win x64 (Debugging)")]
        public static void Build64Debugging()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Win x64 (Debugging)\\Worldline.exe";
            buildPlayerOptions.target = BuildTarget.StandaloneWindows64;
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.AllowDebugging | BuildOptions.ConnectWithProfiler;
            DoBuild(buildPlayerOptions);
        }

        [MenuItem("Build/Build Linux x64")]
        public static void BuildLinux64()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Linux x64\\Worldline.exe";
            buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
            DoBuild(buildPlayerOptions);
        }

        [MenuItem("Build/Build Linux x64 (Debugging)")]
        public static void BuildLinux64Debugging()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Linux x64 (Debugging)\\Worldline.exe";
            buildPlayerOptions.target = BuildTarget.StandaloneLinux64;
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.AllowDebugging;
            DoBuild(buildPlayerOptions);
        }

        [MenuItem("Build/Build Mac x64")]
        public static void BuildMac64()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Mac x64\\Worldline.exe";
            buildPlayerOptions.target = BuildTarget.StandaloneOSX;
            DoBuild(buildPlayerOptions);
        }

        [MenuItem("Build/Build Mac x64 (Debugging)")]
        public static void BuildMac64Debugging()
        {
            BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
            buildPlayerOptions.locationPathName = "Builds\\Mac x64 (Debugging)\\Worldline.exe";
            buildPlayerOptions.target = BuildTarget.StandaloneOSX;
            buildPlayerOptions.options = BuildOptions.Development | BuildOptions.AllowDebugging;
            DoBuild(buildPlayerOptions);
        }

        private static void DoBuild(BuildPlayerOptions buildPlayerOptions)
        {
            #region Add Scenes to Build
            List<string> scenes = new List<string>();
            foreach (string sceneId in AssetDatabase.FindAssets("t:scene", new[] { "Assets\\Scenes" }))
            {
                scenes.Add(AssetDatabase.GUIDToAssetPath(sceneId));
            }

            buildPlayerOptions.scenes = scenes.OrderBy(s => s != "IntroScreen.unity").ToArray();
            #endregion

            #region Assign Asset Bundle Tags
            Debug.Log("Assigning asset bundles tags...");
            var assetsWithNoTags = new List<AssetImporter>();
            var folderTags = new Dictionary<string, string>();
            foreach (string assetId in AssetDatabase.FindAssets("", new[] { "Assets\\Data" }))
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(assetId);
                if (Path.GetExtension(assetPath) == ".asset")
                {
                    var asset = AssetImporter.GetAtPath(assetPath);
                    if (asset != null)
                    {
                        if (string.IsNullOrEmpty(asset.assetBundleName) || string.IsNullOrEmpty(asset.assetBundleVariant))
                        {
                            assetsWithNoTags.Add(asset);
                        }
                        else
                        {
                            var path = Path.GetDirectoryName(assetPath);
                            if (path != null && !folderTags.ContainsKey(path))
                            {
                                folderTags.Add(path, asset.assetBundleName);
                            }
                        }
                    }
                }
            }

            foreach (AssetImporter asset in assetsWithNoTags)
            {
                Debug.Log($"Asset '{asset.assetPath}' has no asset tag.");
                var path = Path.GetDirectoryName(asset.assetPath);
                if (path != null && folderTags.ContainsKey(path))
                {
                    asset.assetBundleName = folderTags[path];
                    asset.assetBundleVariant = "bun";
                    asset.SaveAndReimport();
                    Debug.Log($"Asset '{asset.assetPath}' has been given the tag '{folderTags[path]}'");
                }
            }

            Debug.Log("Assets assigned tags.");
            #endregion

            #region Build Asset Bundles
            Debug.Log("Building asset bundles...");
            BuildAssetBundles.BuildAllAssetBundles();
            Debug.Log("Asset bundles built.");
            #endregion

            #region Build Game
            Debug.Log("Building game...");
            BuildReport report = BuildPipeline.BuildPlayer(buildPlayerOptions);
            BuildSummary summary = report.summary;
            if (summary.result == BuildResult.Succeeded)
            {
                Debug.Log($"Build succeeded: {summary.totalErrors} total errors");
            }
            else if (summary.result == BuildResult.Failed)
            {
                Debug.Log($"Build failed: {summary.totalErrors} total errors");
            }
            #endregion
        }

        [PostProcessBuild(1)]
        public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
        {
            string pathToData = Path.GetFullPath(Path.Combine(pathToBuiltProject, @"..\"));
            pathToData += @"Worldline_Data";

            string pathToText = $"{Application.dataPath}\\Data\\Text";

            var filePaths = Directory.GetFiles(pathToText, "*.*", SearchOption.AllDirectories).Where(s => s.EndsWith(".xml") || s.EndsWith(".txt"));
            Debug.Log("Copying XML to build directory...");
            foreach (string file in filePaths)
            {
                string end = Regex.Replace(file, ".*Assets", string.Empty);
                string dest = pathToData + end;

                Debug.Log($"Moving file {file} to{Environment.NewLine}{dest}");

                var dir = Path.GetDirectoryName(dest);
                if (dir != null)
                {
                    Directory.CreateDirectory(dir);
                }
                
                File.Copy(file, dest, true);
            }

            Debug.Log("Copied XML files to build directory.");
        }
    }
}
