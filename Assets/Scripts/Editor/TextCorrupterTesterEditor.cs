﻿using Worldline.Corruption.TextCorruption;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [CustomEditor(typeof(TextCorrupterTester))]
    public class TextCorrupterTesterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            TextCorrupterTester tester = (TextCorrupterTester)target;
            if (GUILayout.Button("Apply Corrupted Text") && EditorApplication.isPlaying)
            {
                tester.ApplyCorruptedText();
            }

            if (GUILayout.Button("Reset Text") && EditorApplication.isPlaying)
            {
                tester.ResetText();
            }
        }
    }
}
