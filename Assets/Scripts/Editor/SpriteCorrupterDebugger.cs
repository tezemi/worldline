﻿using Worldline.Corruption;
using Worldline.Corruption.SpriteCorruption;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [CustomEditor(typeof(SpriteCorrupter))]
    public class SpriteCorrupterDebugger : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (GraphicsCorrupter.CorruptedSpriteResources == null || !EditorApplication.isPlaying) return; // Game isn't running

            SpriteCorrupter corrupter = (SpriteCorrupter)target;
            int number = 1;
            foreach (Sprite sprite in GraphicsCorrupter.CorruptedSpriteResources.Keys)
            {
                if (sprite == null) continue;

                bool corruptionEnabled = corrupter.CorruptedReferences.Contains(sprite);
                GUILayout.BeginHorizontal();
                GUILayout.Box(number + " - " + (corruptionEnabled ? "✓" : "X") + " " + sprite.name);
                foreach (Sprite corruptedSprite in GraphicsCorrupter.CorruptedSpriteResources[sprite].CorruptedSprites)
                {
                    if (corruptedSprite == null)
                    {
                        GUILayout.Box("Missing sprite");
                    }
                    else
                    {
                        GUILayout.Box(corruptedSprite.texture);
                    }                    
                }

                GUILayout.EndHorizontal();
            }
        }
    }
}
