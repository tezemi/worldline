﻿// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.GameState;
using Worldline.GUI.Menus.Main;
using UnityEditor;
using UnityEngine;

namespace Worldline.EditorExtensions
{
    [CustomEditor(typeof(SecretMenu), true)]
    public class SecretMenuEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var cheats = (Cheat[])Enum.GetValues(typeof(Cheat));
            SecretMenu secretMenu = (SecretMenu)target;
            if (secretMenu.SerializedSecretSounds == null || secretMenu.SerializedSecretSounds.Length != cheats.Length)
            {
                AudioClip[] oldArray = null;

                if (secretMenu.SerializedSecretSounds != null)
                {
                    oldArray = secretMenu.SerializedSecretSounds;
                }

                secretMenu.SerializedSecretSounds = new AudioClip[cheats.Length];

                if (oldArray != null)
                {
                    for (int i = 0; i < oldArray.Length && i < secretMenu.SerializedSecretSounds.Length; i++)
                    {
                        secretMenu.SerializedSecretSounds[i] = oldArray[i];
                    }
                }
            }

            for (int i = 0; i < cheats.Length; i++)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.LabelField($"{cheats[i]}:", GUILayout.MinWidth(30f));

                secretMenu.SerializedSecretSounds[i] = (AudioClip)EditorGUILayout.ObjectField
                (
                    secretMenu.SerializedSecretSounds[i],
                    typeof(AudioClip),
                    true,
                    GUILayout.MinWidth(30f)
                );

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}