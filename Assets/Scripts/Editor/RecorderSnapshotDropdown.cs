﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Reflection;
using System.Collections.Generic;
using Worldline.TimeEngine.Recording;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(Recorder), true)]
    public class RecorderSnapshotDropdown : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            if (!(target is Recorder)) return;
            
            List<string> recorderNames = new List<string>();
            List<string> fullyQualifiedNames = new List<string>();
            foreach (Type type in Assembly.GetAssembly(typeof(Recorder)).GetTypes())
            {
                if (type == typeof(Snapshot) || type.IsSubclassOf(typeof(Snapshot)))
                {
                    recorderNames.Add(type.Name);
                    fullyQualifiedNames.Add(type.FullName);
                }
            }

            Recorder recorder = (Recorder)target;
            SerializedObject serializedObj = new SerializedObject(recorder);
            SerializedProperty serializedProperty = serializedObj.FindProperty("_snapshotType");
            string trimmed = TrimNamespace(serializedProperty.stringValue);
            int current = recorderNames.IndexOf(trimmed);
            int selected = EditorGUILayout.Popup
            (
                "Snapshot",
                current < 0 ? 0 : current, 
                recorderNames.ToArray()
            );

            serializedProperty.stringValue = fullyQualifiedNames[selected];
            serializedObj.ApplyModifiedProperties();

            string TrimNamespace(string fullName)
            {
                if (fullName.Contains("+")) // Check subclasses first
                {
                    return fullName.Substring(fullName.LastIndexOf('+') + 1);
                }

                return fullName.Substring(fullName.LastIndexOf('.') + 1);
            }
        }
    }
}
