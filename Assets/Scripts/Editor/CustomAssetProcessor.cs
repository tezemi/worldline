﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using UnityEditor;

namespace Worldline.EditorExtensions
{ 
    /// <summary>
    /// A custom asset processor that detects when the game's assets are getting
    /// saved before resuming normal functionality.
    /// </summary>
    public class CustomAssetProcessor : UnityEditor.AssetModificationProcessor
    {
        /// <summary>
        /// Gets called when the game's assets are getting saved and updates the
        /// scenes in the build pipeline.
        /// </summary>
        /// <param name="paths">The paths of the assets.</param>
        /// <returns>The paths of the assets... for some reason.</returns>
        public static string[] OnWillSaveAssets(string[] paths)
        {
            UpdateScenesInBuildPipeline();

            return paths;
        }

        /// <summary>
        /// Adds new or not previously added scenes to the build pipeline, and
        /// removes deleted or missing scenes from the pipeline. This is called
        /// automatically when scripts are reloaded, but can be called manually
        /// at any point.
        /// TODO: Remove deleted scenes.
        /// </summary>
        public static void UpdateScenesInBuildPipeline()
        {
            List<EditorBuildSettingsScene> scenes = EditorBuildSettings.scenes.ToList();
            foreach (string prefabGuid in AssetDatabase.FindAssets("", new[] { "Assets/Scenes" }))
            {
                string assetName = AssetDatabase.GUIDToAssetPath(prefabGuid);
                if (assetName.Contains(".unity") && scenes.All(s => s.path != assetName))
                {
                    scenes.Add(new EditorBuildSettingsScene(assetName, true));
                }
            }

            scenes.RemoveAll(s => s == null || !s.enabled);
            EditorBuildSettings.scenes = scenes.ToArray();
        }
    }
}
