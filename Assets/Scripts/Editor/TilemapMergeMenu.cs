﻿using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

namespace Worldline.EditorExtensions
{
    /// <summary>
    /// Contains static methods that add context menu functions for creating 
    /// temporary and merging <see cref="Tilemap"/>s. Unity suffers from
    /// performance loss when editing large <see cref="Tilemap"/>s when not
    /// in prefab mode. In order to mitigate this, it might be helpful to
    /// use <see cref="CreateTemp(MenuCommand)"/>, which creates a clean
    /// <see cref="Tilemap"/> to paint on. When done editing, you can use
    /// <see cref="MergeTemp(MenuCommand)"/> to merge the newly painted on
    /// <see cref="Tilemap"/> into the original.
    /// </summary>
    internal class TilemapMergeMenu
    {
        /// <summary>
        /// The name that temporary <see cref="Tilemap"/>s have appended.
        /// </summary>
        private const string TempName = "(Temporary)";

        /// <summary>
        /// When used on a <see cref="GameObject"/> with a <see cref="Tilemap"/>, this 
        /// will create a temporary duplicate which can be used to mitigate performance
        /// issues. You can use <see cref="MergeTemp(MenuCommand)"/> to merge the two
        /// <see cref="Tilemap"/>s.
        /// </summary>
        /// <param name="command">Context information about the command.</param>
        [MenuItem("GameObject/2D Object/Create Temp Tilemap")]
        private static void CreateTemp(MenuCommand command)
        {
            if (command.context is GameObject originalObject)
            {
                if (originalObject.GetComponent<Tilemap>() == null) return;

                GameObject tempObject = new GameObject($"{originalObject.name} {TempName}", typeof(Tilemap), typeof(TilemapRenderer));
                int count = 1;
                while (GameObject.Find(tempObject.name) != null && GameObject.Find(tempObject.name) != tempObject)
                {
                    tempObject.name = $"{originalObject.name} {TempName} ({count++})";
                    if (count > 100)
                    {
                        break;
                    }
                }

                tempObject.transform.SetParent(originalObject.transform.parent);
                tempObject.transform.localPosition = originalObject.transform.localPosition;
                Selection.activeGameObject = tempObject;
            }
        }

        /// <summary>
        /// When used on a <see cref="GameObject"/> created via the 
        /// <see cref="CreateTemp(MenuCommand)"/> method, this will merge the original
        /// <see cref="Tilemap"/> and the old <see cref="Tilemap"/> into one.
        /// </summary>
        /// <param name="command">Context information about the command.</param>
        [MenuItem("GameObject/2D Object/Merge Temp Tilemap")]
        private static void MergeTemp(MenuCommand command)
        {
            if (command.context is GameObject tempObj)
            {
                if (tempObj.GetComponent<Tilemap>() == null || !tempObj.name.Contains(TempName)) return;

                GameObject org = MergeTilemap(tempObj, true);
                if (org != null)
                {
                    Selection.activeGameObject = org;
                }
            }
        }

        /// <summary>
        /// When used on a <see cref="GameObject"/> created via the <see cref="CreateTemp"/> 
        /// method, this will refresh all the new <see cref="Tile"/>s by placing them onto
        /// the original <see cref="Tilemap"/>.
        /// </summary>
        /// <param name="command">Context information about the command.</param>
        [MenuItem("GameObject/2D Object/Refresh Temp Tilemap")]
        private static void RefreshTemp(MenuCommand command)
        {
            if (command.context is GameObject tempObj)
            {
                if (tempObj.GetComponent<Tilemap>() == null || !tempObj.name.Contains(TempName)) return;

                MergeTilemap(tempObj, false);
                if (tempObj != null)
                {
                    Selection.activeGameObject = tempObj;
                }
            }
        }

        /// <summary>
        /// Goes through all temporary <see cref="Tilemap"/>s in the 
        /// <see cref="UnityEngine.SceneManagement.Scene"/> and merges them with their
        /// original <see cref="Tilemap"/>s.
        /// </summary>
        /// <param name="command">Context information about the command.</param>
        [MenuItem("GameObject/2D Object/Merge All Temp Tilemaps")]
        private static void MergeAllTemp(MenuCommand command)
        {
            foreach (Tilemap tmap in Object.FindObjectsOfType<Tilemap>())
            {
                if (!tmap.name.Contains(TempName)) return;

                GameObject tempObj = tmap.gameObject;
                MergeTilemap(tempObj, true);
            }
        }

        /// <summary>
        /// When given the <see cref="GameObject"/> with a temporary <see cref="Tilemap"/>,
        /// this will find and merge it with its original.
        /// </summary>
        /// <param name="tempObj">A temporary <see cref="GameObject"/> created by 
        /// <see cref="CreateTemp(MenuCommand)"/></param>
        /// <returns>The original <see cref="GameObject"/>.</returns>
        private static GameObject MergeTilemap(GameObject tempObj, bool destroyTemp)
        {
            string originalName = Regex.Replace(tempObj.name.Replace($" {TempName}", string.Empty), "/[0-9]$", string.Empty);
            GameObject originalObj = GameObject.Find(originalName);
            if (originalObj != null && originalObj.GetComponent<Tilemap>() != null)
            {
                Tilemap tempTilemap = tempObj.GetComponent<Tilemap>();
                Tilemap originalTilemap = originalObj.GetComponent<Tilemap>();
                for (int x = tempTilemap.cellBounds.xMin; x <= tempTilemap.cellBounds.xMax; x++)
                {
                    for (int y = tempTilemap.cellBounds.yMin; y <= tempTilemap.cellBounds.yMax; y++)
                    {
                        Vector3Int position = new Vector3Int(x, y, 0);
                        TileBase tileBase = tempTilemap.GetTile(position);
                        if (tileBase is Tile tile)
                        {
                            if (tile.sprite.name == "TilesPhys_2")
                            {
                                originalTilemap.SetTile(position, null);
                            }
                            else
                            {
                                originalTilemap.SetTile(position, tile);
                            }
                            
                        }
                    }
                }
            }

            if (destroyTemp)
            {
                Object.DestroyImmediate(tempObj);
            }

            return originalObj;
        }
    }
}
