﻿// Copyright (c) 2019 Destin Hebner
using System.IO;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    public class BuildAssetBundles
    {
        [MenuItem("Assets/Build Asset Bundles")]
        public static void BuildAllAssetBundles()
        {
            string assetBundleDirectory = "Assets/StreamingAssets";
            if (!Directory.Exists(assetBundleDirectory))
            {
                Directory.CreateDirectory(assetBundleDirectory);
            }

            BuildPipeline.BuildAssetBundles(assetBundleDirectory, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
        }
    }
}
