﻿// Copyright (c) 2019 Destin Hebner
using Worldline.GameState;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [InitializeOnLoad]
    internal class TrackedMonoBehaviourIDAssignor
    {
        static TrackedMonoBehaviourIDAssignor()
        {
            Selection.selectionChanged += () =>
            {
                foreach (Object obj in Selection.objects)
                {
                    if (!(obj is GameObject) || ((GameObject)obj).GetComponent<TrackedMonoBehaviour>() == null || (obj as GameObject).scene.name == obj.name || string.IsNullOrEmpty((obj as GameObject).scene.name)) continue;

                    foreach (TrackedMonoBehaviour monoBehaviour in ((GameObject)obj).GetComponents<TrackedMonoBehaviour>())
                    {
                        if (monoBehaviour == null) continue;

                        SerializedObject serializedObj = new SerializedObject(monoBehaviour);
                        SerializedProperty serializedProperty = serializedObj.FindProperty("ID");
                        long originalID = serializedProperty.longValue;
                        string idAsString = $"{monoBehaviour.gameObject.scene.name.GetHashCode()}{monoBehaviour.GetInstanceID()}".Replace("-", "");
                        long generatedID = monoBehaviour.UseGlobalID ? monoBehaviour.GetType().Name.GetHashCode() : long.Parse(idAsString);
                        if (originalID != generatedID)
                        {
                            serializedProperty.longValue = generatedID;
                        }

                        serializedObj.ApplyModifiedProperties();
                        PrefabUtility.RecordPrefabInstancePropertyModifications(monoBehaviour);
                    }
                }
            };
        }
    }
}
