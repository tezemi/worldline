﻿using Worldline.Corruption.SpriteCorruption;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [CustomEditor(typeof(CorruptedSpriteTester))]
    public class CorruptedSpriteTesterEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            CorruptedSpriteTester tester = (CorruptedSpriteTester)target;
            if (GUILayout.Button("Apply Corrupted Sprite") && EditorApplication.isPlaying)
            {
                tester.ApplyCorruptedSprite();
            }

            if (GUILayout.Button("Reset Sprite") && EditorApplication.isPlaying)
            {
                tester.ResetSprite();
            }

            if (GUILayout.Button("Save Sprite as PNG") && EditorApplication.isPlaying)
            {
                tester.SaveSpriteAsPNG();
            }
        }
    }
}
