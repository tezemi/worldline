﻿// Copyright (c) 2019 Destin Hebner
using Worldline.GUI.Menus;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.EditorExtensions
{
    /// <summary>
    /// This adds a 2d array layout to TextMenus.
    /// </summary>
    [CanEditMultipleObjects]
    [CustomEditor(typeof(SelectorMenu), true)]
    public class GraphicMenuEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            SelectorMenu gameMenu = (SelectorMenu)target;
            if (gameMenu.SerializedOptions == null ||
            gameMenu.SerializedOptions.Length != gameMenu.Rows * gameMenu.Columns)
            {
                gameMenu.SerializedOptions = new Graphic[gameMenu.Rows * gameMenu.Columns];
            }

            int total = 0;
            for (int i = 0; i < gameMenu.Rows; i++)
            {
                EditorGUILayout.BeginHorizontal();
                for (int j = 0; j < gameMenu.Columns; j++)
                {
                    gameMenu.SerializedOptions[total] = (Graphic)EditorGUILayout.ObjectField
                    (
                        gameMenu.SerializedOptions[total],
                        typeof(Graphic),
                        true,
                        GUILayout.MinWidth(30f)
                    );

                    total++;
                }

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}