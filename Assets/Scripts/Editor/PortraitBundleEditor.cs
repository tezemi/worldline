﻿// Copyright (c) 2019 Destin Hebner
using Worldline.ScriptedSequences;
using UnityEditor;
using UnityEngine;

namespace Worldline.EditorExtensions
{
    [CanEditMultipleObjects]
    [CustomEditor(typeof(PortraitBundle), true)]
    public class PortraitBundleEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var portraitBundle = (PortraitBundle)target;
            if (portraitBundle.SerializedNames == null || portraitBundle.SerializedNames.Length != portraitBundle.Portraits)
            {
                string[] updatedArray = new string[portraitBundle.Portraits];
                if (portraitBundle.SerializedNames != null)
                {
                    for (int i = 0; i < updatedArray.Length; i++)
                    {
                        if (i >= portraitBundle.SerializedNames.Length) break;

                        updatedArray[i] = portraitBundle.SerializedNames[i];
                    }
                }

                portraitBundle.SerializedNames = updatedArray;
            }

            if (portraitBundle.SerialziedSprites == null || portraitBundle.SerialziedSprites.Length != portraitBundle.Portraits)
            {
                Sprite[] updatedArray = new Sprite[portraitBundle.Portraits];
                if (portraitBundle.SerialziedSprites != null)
                {
                    for (int i = 0; i < updatedArray.Length; i++)
                    {
                        if (i >= portraitBundle.SerialziedSprites.Length) break;

                        updatedArray[i] = portraitBundle.SerialziedSprites[i];
                    }
                }

                portraitBundle.SerialziedSprites = updatedArray;
            }

            for (int i = 0; i < portraitBundle.Portraits; i++)
            {
                EditorGUILayout.BeginHorizontal();
                portraitBundle.SerializedNames[i] = EditorGUILayout.TextField(portraitBundle.SerializedNames[i]);
                portraitBundle.SerialziedSprites[i] = (Sprite)EditorGUILayout.ObjectField(portraitBundle.SerialziedSprites[i], typeof(Sprite), true, GUILayout.MinWidth(30f));
                EditorGUILayout.EndHorizontal();
            }
        }
    }
}