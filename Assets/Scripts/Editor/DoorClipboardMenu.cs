﻿// Copyright (c) 2019 Destin Hebner
using Worldline.ScriptedSequences.Generic;
using UnityEditor;
using UnityEngine;

namespace Worldline.EditorExtensions
{
    /// <summary>
    /// Adds the ability to copy a door's position on a door MonoBehavior's
    /// context menu, or via a GameObject within the hierarchy if the GameObject
    /// has a door attached to it.
    /// </summary>
    internal class DoorClipboardMenu
    {
        [MenuItem("CONTEXT/Door/Copy Position")]
        private static void CopyPositionContext(MenuCommand command)
        {
            if (command.context is MonoBehaviour)
            {
                MonoBehaviour monoBehaviour = (MonoBehaviour)command.context;
                Vector3 position = monoBehaviour.transform.position;
                EditorGUIUtility.systemCopyBuffer = position.ToString();
                Debug.Log(EditorGUIUtility.systemCopyBuffer);
            }
        }

        [MenuItem("CONTEXT/Door/Paste Position")]
        private static void PastePositionContext(MenuCommand command)
        {
            if (command.context is MonoBehaviour)
            {
                MonoBehaviour monoBehaviour = (MonoBehaviour)command.context;
                string textPosition = EditorGUIUtility.systemCopyBuffer;
                textPosition = textPosition.Replace('(', ' ');
                textPosition = textPosition.Replace(')', ' ');
                string[] positions = textPosition.Split(',');
                SerializedObject serializedObject = new SerializedObject(monoBehaviour);
                SerializedProperty serializedProperty = serializedObject.FindProperty("ExitPosition");
                serializedProperty.vector3Value = new Vector3
                (
                    float.Parse(positions[0]),
                    float.Parse(positions[1]),
                    float.Parse(positions[2])
                );

                Debug.Log($"{ serializedProperty.displayName } is { serializedProperty.vector3Value }");
                serializedObject.ApplyModifiedProperties();
            }
        }

        [MenuItem("GameObject/2D Object/Copy Door Position")]
        private static void CopyPositionHierarchy(MenuCommand command)
        {
            if (command.context is GameObject)
            {
                GameObject gameObject = (GameObject)command.context;
                if (gameObject.GetComponent<Door>() != null)
                {
                    Vector3 position = gameObject.GetComponent<Door>().transform.position;
                    EditorGUIUtility.systemCopyBuffer = position.ToString();
                    Debug.Log(EditorGUIUtility.systemCopyBuffer);
                }
            }
            else
            {
                Debug.LogWarning("There is no door attached to this game object.");
            }
        }

        [MenuItem("GameObject/2D Object/Paste Door Position")]
        private static void PastePositionHierarchy(MenuCommand command)
        {
            if (command.context is GameObject)
            {
                GameObject gameObject = (GameObject)command.context;
                if (gameObject.GetComponent<Door>() != null)
                {
                    string textPosition = EditorGUIUtility.systemCopyBuffer;
                    textPosition = textPosition.Replace('(', ' ');
                    textPosition = textPosition.Replace(')', ' ');
                    string[] positions = textPosition.Split(',');
                    SerializedObject serializedObject = new SerializedObject(gameObject.GetComponent<Door>());
                    SerializedProperty serializedProperty = serializedObject.FindProperty("ExitPosition");
                    serializedProperty.vector3Value = new Vector3
                    (
                        float.Parse(positions[0]),
                        float.Parse(positions[1]),
                        float.Parse(positions[2])
                    );

                    Debug.Log($"{serializedProperty.displayName} is {serializedProperty.vector3Value}");
                    serializedObject.ApplyModifiedProperties();
                }
            }
            else
            {
                Debug.LogWarning("There is no door attached to this game object.");
            }
        }
    }
}
