﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using UnityEngine;
using UnityEditor;

namespace Worldline.EditorExtensions
{
    [InitializeOnLoad]
    internal class AssetPathAssignor
    {
        static AssetPathAssignor()
        {
            Selection.selectionChanged += () =>
            {
                foreach (Object obj in Selection.objects)
                {
                    if (!(obj is SerializableScriptableObject)) continue;

                    SerializableScriptableObject sso = (SerializableScriptableObject)obj;
                    SerializedObject serializedObj = new SerializedObject(sso);
                    SerializedProperty serializedProperty = serializedObj.FindProperty("AssetPath");
                    string originalPath = serializedProperty.stringValue;
                    string generatedPath = AssetDatabase.GetAssetPath(sso);
                    if (generatedPath != originalPath)
                    {
                        serializedProperty.stringValue = generatedPath;
                    }

                    serializedObj.ApplyModifiedProperties();
                }
            };
        }
    }
}
