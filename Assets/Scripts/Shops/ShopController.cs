﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.Shops
{
    public class ShopController : MonoBehaviour
    {
        public SpriteRenderer ShopKeeperSprite;
        public Shop Shop { get; set; }

        private void Awake()
        {
            Shop = new ExampleShop(this);
        }

        private void Start()
        {
            ShopKeeperSprite.sprite = Shop.ShopSprite;
            StartCoroutine(Shop.ShopRoutine());
        }
    }
}
