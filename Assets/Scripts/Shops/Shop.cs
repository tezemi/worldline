﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Linq;
using System.Collections;
using UnityEngine;

namespace Worldline.Shops
{
    public abstract class Shop
    {
        public abstract Sprite ShopSprite { get; }
        public ShopController ShopController { get; set; }

        protected Shop(ShopController shopController)
        {
            ShopController = shopController;
        }

        protected WaitUntil ShowDialog(string message)
        {
            //ShopController.Printer.Print(message);
            return new WaitUntil(() => Input.GetButtonDown("primary"));
        }

        protected WaitUntil ShowQuestion(string[] answers)
        {
            string output = answers.Aggregate(string.Empty, (current, s) => current + s + Environment.NewLine);
            output = output.Remove(output.Length - 1);
            //ShopController.Printer.Print(output);
            return new WaitUntil(() => Input.GetButtonDown("primary"));
        }

        public abstract IEnumerator ShopRoutine();
    }
}
