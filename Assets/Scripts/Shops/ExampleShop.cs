﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using UnityEngine;

namespace Worldline.Shops
{
    public class ExampleShop : Shop
    {
        public override Sprite ShopSprite => Resources.Load<Sprite>("Sprites/Shops/ShopkeeperTest1");

        public ExampleShop(ShopController shopController) : base(shopController)
        {
            //...   
        }

        public override IEnumerator ShopRoutine()
        {
            yield return ShowDialog("HEY. You can't use the bathroom unless you buy something. ALSO, the bathroom is employees only. So you can't use the bathroom unless you get a job here.");
            yield return ShowDialog("Anyway, what do you want?");
            yield return ShowQuestion(new[] { "Shop.", "Talk.", "Start quickly walking toward the bathroom.", "Goodbye!" });
        }
    }
}
