﻿using UnityEngine;
using UnityEngine.UI;
using Worldline.GUI.Printing;

namespace Worldline.GUI
{
    public class TextRenderer : Text, IRendersText
    {
        public string Text
        {
            get => text;
            set => text = value;
        }

        public TextPrinter TextPrinter
        {
            get => null;
            set { }
        }

        public T FindComponent<T>() where T : Component
        {
            return GetComponent<T>();
        }
    }
}
