﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GUI
{
    /// <summary>
    /// Ensures a UI element will stay in the same position even if its parent
    /// elements are moved.
    /// </summary>
    public class UiLocker : MonoBehaviour
    {
        public Vector3 LockedPosition { get; protected set; }
        private bool _getNewPos;
        private Resolution? _resolutionLastFrame;

        protected virtual void Start()
        {
            LockedPosition = GetComponent<RectTransform>().position;
        }

        protected virtual void LateUpdate()
        {
            if (_resolutionLastFrame.HasValue && 
            (Screen.currentResolution.width != _resolutionLastFrame.Value.width || 
            Screen.currentResolution.height != _resolutionLastFrame.Value.height))
            {
                _resolutionLastFrame = Screen.currentResolution;
                _getNewPos = true;
                return;
            }

            if (_getNewPos)
            {
                LockedPosition = GetComponent<RectTransform>().position;
                _getNewPos = false;
            }
            
            GetComponent<RectTransform>().position = LockedPosition;

            _resolutionLastFrame = Screen.currentResolution;
        }
    }
}
