﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Visuals;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class DemoMenu : TextIndentionMenu
    {
        public GameObject MainMenu;
        public GameObject DifficultyMenu;
        public Text DescriptionText;
        public readonly Color UnselectableFade = new Color(0.25f, 0.25f, 0.25f, 0f);
        private bool _demoFileExists;
        private List<SaveFile> _saveFiles;

        protected override void Awake()
        {
            base.Awake();
            Options[0, 0].text = SerializedStrings.GetString("MainMenu/Main", "StartDemo");
            Options[1, 0].text = SerializedStrings.GetString("MainMenu/Main", "LoadDemo");
            Options[2, 0].text = SerializedStrings.GetString("MainMenu/Main", "MenuGoBack");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _saveFiles = SaveFile.GetSaveFilesOnHardDisk();
            _demoFileExists = false;
            foreach (SaveFile saveFile in _saveFiles)
            {
                if (saveFile.Name.Contains("demo"))
                {
                    _demoFileExists = true;
                }
            }
        }
        
        protected override void Update()
        {
            base.Update();
            if (!_demoFileExists)
            {
                Options[1, 0].color = SelectedElement == Options[1, 0] ? SelectedColor - UnselectableFade : Color.gray;
            }
            else
            {
                Options[1, 0].color = SelectedElement == Options[1, 0] ? SelectedColor : UnselectedColor;
            }

            if (SelectedElement == Options[0, 0])
            {
                DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", _demoFileExists ? "StartDemoWarning" : "StartDemoDesc");
            }
            else if (SelectedElement == Options[1, 0])
            {
                DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "LoadDemoDesc");
            }
            else if (SelectedElement == Options[2, 0])
            {
                DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "GoBackDesc");
            }
        }

        protected override void OnBackOut()
        {
            MainMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                DifficultyMenu.GetComponent<DifficultyMenu>().UseDemoFile = true;
                DifficultyMenu.GetComponent<DifficultyMenu>().MenuToGoBackTo = gameObject;
                DifficultyMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 1)
            {
                if (_demoFileExists)
                {
                    SaveFile.LoadedSaveFile = SaveFile.LoadSaveFile("demo");
                    if (string.IsNullOrEmpty(SaveFile.LoadedSaveFile.Scene))
                    {
                        if (!SceneTransitionOperator.Main.Transitioning)
                        {
                            SaveFile.LoadedSaveFile = new SaveFile("demo", (Difficulty)itemClicked.Item2);
                            SceneTransitionOperator.Main.LoadScene("Sim01");
                        }

                        enabled = false;
                    }
                    else
                    {
                        SaveFile.LoadedSaveFile.LoadGame();
                    }

                    PersistentCanvasController.Main.ShowLoadingText = true;
                    enabled = false;
                }
                else
                {
                    OptionDisabled();
                }
            }
            else if (itemClicked.Item1 == 2)
            {
                OnBackOut();
            }
        }
    }
}
