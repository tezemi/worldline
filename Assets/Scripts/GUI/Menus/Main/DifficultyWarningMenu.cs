﻿using Worldline.Visuals;
using Worldline.GameState;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    /// <summary>
    /// This is the menu that is used to control the main menu and adds
    /// behavior for each selected option.
    /// </summary>
    public class DifficultyWarningMenu : ScaleMenu
    {
        public Text WarningText;
        public Text AcceptText;
        public Text DeclineText;
        public DifficultyMenu DifficultyMenu;

        protected override void Awake()
        {
            base.Awake();

            WarningText.text = SerializedStrings.GetString("MainMenu/Main", "DifficultyWarning");
            AcceptText.text = SerializedStrings.GetString("MainMenu/Main", "DifficultyAccept");
            DeclineText.text = SerializedStrings.GetString("MainMenu/Main", "DifficultyDecline");
        }

        protected override void OnBackOut()
        {
            OptionSelected();
            DifficultyMenu.enabled = true;
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item2 == 0)
            {
                OptionSelected();
                if (!SceneTransitionOperator.Main.Transitioning)
                {
                    SaveFile.LoadedSaveFile = new SaveFile
                    (
                        DifficultyMenu.UseDemoFile ? "demo" : (DifficultyMenu.LastSavedFileIndex + 1).ToString(),
                        Difficulty.AssBlasting
                    );

                    SceneTransitionOperator.Main.LoadScene(SaveFile.DefaultScene);
                    MainMenuCameraController.Main.GetComponent<AudioSource>().Fade(0f);
                    PersistentCanvasController.Main.ShowLoadingText = true;
                }

                enabled = false;
            }
            else if (itemClicked.Item2 == 1)
            {
                OptionSelected();
                DifficultyMenu.enabled = true;
                gameObject.SetActive(false);
            }
        }
    }
}
