﻿// Copyright (c) 2019 Destin Hebner
using System;
using Worldline.Utilities;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    /// <summary>
    /// This is the menu that is used to control the main menu and adds
    /// behavior for each selected option.
    /// </summary>
    public class MainMenu : TextIndentionMenu
    {
        public Text NewGameText;
        public Text LoadGameText;
        public Text DemoText;
        public Text OptionsText;
        public Text CreditsText;
        public Text ExitText;
        public Text DemoTitleText;
        public GameObject FileSelectMenu;
        public GameObject OptionsMenu;
        public GameObject DemoMenu;
        public GameObject CreditsMenu;
        public GameObject SecretMenu;
        public GameObject Version;
        public int TimesBackedOut { get; protected set; }
        public readonly Color UnselectalbleFade = new Color(0.25f, 0.25f, 0.25f, 0f);
        public const int BackOutPressTimesNeeded = 25;
        
        protected override void Awake()
        {
            base.Awake();
            InputManager.Load();
            Cursor.visible = true;
            NewGameText.text = SerializedStrings.GetString("MainMenu/Main", "NewGame");
            LoadGameText.text = SerializedStrings.GetString("MainMenu/Main", "LoadGame");
            DemoText.text = SerializedStrings.GetString("MainMenu/Main", "Demo");
            OptionsText.text = SerializedStrings.GetString("MainMenu/Main", "Options");
            CreditsText.text = SerializedStrings.GetString("MainMenu/Main", "Credits");
            ExitText.text = SerializedStrings.GetString("MainMenu/Main", "Exit");
            DemoTitleText.text = SerializedStrings.GetString("MainMenu/Main", "DemoTitleText");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (Debug.isDebugBuild)
            {
                Version.SetActive(true);
            }
            else
            {
                Version.SetActive(false);
            }

            Version.GetComponent<Text>().text = $"Debug Build{Environment.NewLine}Version: {Application.version}";
        }

        protected override void Update()
        {
            base.Update();
            Options[0, 0].color = SelectedElement == Options[0, 0] ? SelectedColor - UnselectalbleFade : Color.gray;
            Options[1, 0].color = SelectedElement == Options[1, 0] ? SelectedColor - UnselectalbleFade : Color.gray;
        }

        protected virtual void OnDisable()
        {
            if (Version != null)
            {
                Version.SetActive(false);
            }
        }

        protected override void OnBackOut()
        {
            if (TimesBackedOut++ < BackOutPressTimesNeeded && !Debug.isDebugBuild)
            {
                PlayMenuSound(UnselectableOptionSound);
            }
            else
            {
                SecretMenu.SetActive(true);
                gameObject.SetActive(false);
            }
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                OptionDisabled();
            }
            else if (itemClicked.Item1 == 1)
            {
                OptionDisabled();
            }
            else if (itemClicked.Item1 == 2)
            {
                OptionSelected();
                DemoMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 3)
            {
                OptionSelected();
                OptionsMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 4)
            {
                OptionSelected();
                CreditsMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 5)
            {
                Application.Quit();
            }
        }
    }
}
