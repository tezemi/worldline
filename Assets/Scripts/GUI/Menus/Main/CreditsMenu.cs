﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class CreditsMenu : ScaleMenu
    {
        public Text Credits0;
        public Text Credits1;
        public Text Credits2;
        public Text Credits3;
        public Text Credits4;
        public Text Credits5;
        public GameObject MainMenu;
        public const float LinkShrinkSpeed = 0.02f;

        protected override void Awake()
        {
            base.Awake();
            DisableColorsFor.Add(Credits1);
            Credits0.text = SerializedStrings.GetString("MainMenu/Main", "Credits0");
            Credits1.text = SerializedStrings.GetString("MainMenu/Main", "Credits1");
            Credits2.text = SerializedStrings.GetString("MainMenu/Main", "Credits2");
            Credits3.text = SerializedStrings.GetString("MainMenu/Main", "Credits3");
            Credits4.text = SerializedStrings.GetString("MainMenu/Main", "Credits4");
            Credits5.text = SerializedStrings.GetString("MainMenu/Main", "Credits5");
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Timing.RunCoroutine
            (
                KickstarterFlash(Credits1, 0.1f), 
                Segment.FixedUpdate,
                GetInstanceID().ToString()
            );
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual IEnumerator<float> KickstarterFlash(Text text, float speed)
        {
            start:
            Color moveToColor = TimeOperator.EffectColors[(TimeEffect)Random.Range(0, 5)];
            while (text.color != moveToColor)
            {
                yield return Timing.WaitForOneFrame;
                text.color = Vector4.MoveTowards(text.color, moveToColor, speed);
            }

            if (isActiveAndEnabled)
            {
                goto start;
            }
        }

        protected override void OnBackOut()
        {
            MainMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                Application.OpenURL(URLs.Kickstarter);
            }
            else if (itemClicked.Item1 == 1)
            {
                Application.OpenURL(URLs.ContactPage);
            }
            else if (itemClicked.Item1 == 2)
            {
                Application.OpenURL(URLs.ContactEmail);
            }
            else if (itemClicked.Item1 == 3)
            {
                OnBackOut();
            }
        }
    }
}
