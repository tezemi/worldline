﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.GameState;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class DifficultyMenu : SelectorMenu
    {
        public Text HeaderText;
        public Text DescriptionText;
        public Text GoBackText;
        public Color SelectedColor;
        public Color UnselectedColor;
        public GameObject DifficultyWarningMenu;
        public bool UseDemoFile { get; set; }
        public GameObject MenuToGoBackTo { get; set; }
        public int LastSavedFileIndex { get; protected set; }

        protected override void Awake()
        {
            base.Awake();
            List<SaveFile> saveFiles = SaveFile.GetSaveFilesOnHardDisk();
            int largestFileIndex = 0;
            foreach (SaveFile saveFile in saveFiles)
            {
                if (int.TryParse(saveFile.Name, out int fileIndex))
                {
                    if (fileIndex > largestFileIndex)
                    {
                        largestFileIndex = fileIndex;
                    }
                }
            }

            LastSavedFileIndex = saveFiles.Count > 0 ? largestFileIndex : -1;
        }

        protected override void Update()
        {
            base.Update();
            if (CurrentlySelectedOption.Item1 != 0)
            {
                if (Selector.activeSelf)
                {
                    Selector.SetActive(false);
                }

                Options[1, 0].color = SelectedColor;
            }
            else if (CurrentlySelectedOption.Item1 == 0)
            {
                if (!Selector.activeSelf)
                {
                    Selector.SetActive(true);
                }

                Options[1, 0].color = UnselectedColor;
            }

            if (CurrentlySelectedOption.Item1 == 0)
            {
                if (CurrentlySelectedOption.Item2 == 0)
                {
                    HeaderText.text = SerializedStrings.GetString("MainMenu/Main", "EasyHeader");
                    DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "Easy");
                }
                else if (CurrentlySelectedOption.Item2 == 1)
                {
                    HeaderText.text = SerializedStrings.GetString("MainMenu/Main", "NormalHeader");
                    DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "Normal");
                }
                else if (CurrentlySelectedOption.Item2 == 2)
                {
                    HeaderText.text = SerializedStrings.GetString("MainMenu/Main", "HardHeader");
                    DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "Hard");
                }
                else if (CurrentlySelectedOption.Item2 == 3)
                {
                    HeaderText.text = SerializedStrings.GetString("MainMenu/Main", "AssBlastingHeader");
                    DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "AssBlasting");
                }
            }

            GoBackText.text = SerializedStrings.GetString("MainMenu/Main", "MenuGoBack");
        }

        protected override void OnBackOut()
        {
            MenuToGoBackTo.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (CurrentlySelectedOption.Item1 == 0 && CurrentlySelectedOption.Item2 != 3)
            {
                if (!SceneTransitionOperator.Main.Transitioning)
                {
                    SaveFile.LoadedSaveFile = new SaveFile
                    (
                        UseDemoFile ? "demo" : (LastSavedFileIndex + 1).ToString(),
                        CheatController.IsActive(Cheat.Nightmare) ? Difficulty.AssBlasting : (Difficulty)itemClicked.Item2
                    );

                    SceneTransitionOperator.Main.LoadScene(SaveFile.DefaultScene);
                    MainMenuCameraController.Main.GetComponent<AudioSource>().Fade(0f);
                    PersistentCanvasController.Main.ShowLoadingText = true;
                }

                enabled = false;
            }
            else if (CurrentlySelectedOption.Item2 == 3)
            {
                OptionSelected();
                DifficultyWarningMenu.SetActive(true);
                enabled = false;
            }
            else
            {
                OnBackOut();
            }
        }
    }
}
