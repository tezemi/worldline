﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class EditControlMenu : TextIndentionMenu
    {
        public Text KeysText;
        public Text DescriptionText;
        public Image UpArrow;
        public Image DownArrow;
        public ControlsMenu ControlsMenu;
        public bool RebindingPositive { get; protected set; }
        public bool RebindingAltPositive { get; protected set; }
        public const float ArrowTransitionSpeed = 0.12f;
        private InputConfiguration _configuration;
        private int _axesIndexOffset;

        public int AxesIndexOffset
        {
            get
            {
                return _axesIndexOffset;
            }
            set
            {
                if (SerializedOptions.Length + value > _configuration.axes.Count)
                {
                    value -= 1;
                }

                if (value < 0)
                {
                    value = 0;
                }

                ShowInputsBasedOnOffset(value);
                _axesIndexOffset = value;
            }
        }

        protected override void Awake()
        {
            base.Awake();
            _configuration = InputManager.PlayerOneConfiguration;
            ShowInputsBasedOnOffset(0);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            ShowInputsBasedOnOffset(0);
        }

        protected override void Update()
        {
            if (UpArrow.MouseInside())
            {
                UpArrow.rectTransform.localScale = Vector3.Lerp
                (
                    UpArrow.rectTransform.localScale,
                    new Vector3(0.8f, 0.8f, 1f),
                    ArrowTransitionSpeed
                );
            }
            else
            {
                UpArrow.rectTransform.localScale = Vector3.Lerp
                (
                    UpArrow.rectTransform.localScale,
                    new Vector3(1f, 1f, 1f),
                    ArrowTransitionSpeed
                );
            }

            if (DownArrow.MouseInside())
            {
                DownArrow.rectTransform.localScale = Vector3.Lerp
                (
                    DownArrow.rectTransform.localScale,
                    new Vector3(0.8f, 0.8f, 1f),
                    ArrowTransitionSpeed
                );
            }
            else
            {
                DownArrow.rectTransform.localScale = Vector3.Lerp
                (
                    DownArrow.rectTransform.localScale,
                    new Vector3(1f, 1f, 1f),
                    ArrowTransitionSpeed
                );
            }

            if (InputManager.GetButtonDown("down") && CurrentlySelectedOption.Item1 >= SerializedOptions.Length - 1 ||
            DownArrow.MouseInside() && InputManager.GetButtonDown("primary"))
            {
                AxesIndexOffset++;
            }
            else if (InputManager.GetButtonDown("up") && CurrentlySelectedOption.Item1 == 0 && AxesIndexOffset > 0 ||
            UpArrow.MouseInside() && InputManager.GetButtonDown("primary"))
            {
                AxesIndexOffset--;
            }
            else
            {
                base.Update();
            }

            KeysText.text = $"{GetAxisFromSelection().positive}{Environment.NewLine}{GetAxisFromSelection().altPositive}";
            if (!RebindingPositive && !RebindingAltPositive)
            {
                DescriptionText.color = Color.white;
                DescriptionText.text = 
                $"{SerializedStrings.GetString("MainMenu/Main", $"{GetAxisFromSelection().name}_description")}{Environment.NewLine}<color=#FFFF00>{SerializedStrings.GetString("MainMenu/Main", "PressGoBack")}</color>";
            }
            else if (RebindingPositive)
            {
                DescriptionText.color = Color.yellow;
                DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "RebindPositive");
            }
            else if (RebindingAltPositive)
            {
                DescriptionText.color = Color.yellow;
                DescriptionText.text = SerializedStrings.GetString("MainMenu/Main", "RebindAlt");
            }
        }

        protected virtual void ShowInputsBasedOnOffset(int offset)
        {
            for (var i = 0; i < SerializedOptions.Length; i++)
            {
                Text t = SerializedOptions[i];
                t.text = SerializedStrings.GetString("MainMenu/Main", _configuration.axes[i + offset].name);
            }
        }

        protected override void OnBackOut()
        {
            ControlsMenu.enabled = true;
            enabled = false;
            foreach (Text t in SerializedOptions)
            {
                t.color = UnselectedColor;
                t.rectTransform.anchoredPosition = OriginalPositions[t];
            }
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            Timing.RunCoroutine(RebindSelectedControl(true), Segment.Update, GetInstanceID().ToString());
        }

        public AxisConfiguration GetAxisFromSelection()
        {
            return _configuration.axes[CurrentlySelectedOption.Item1 + AxesIndexOffset];
        }

        public IEnumerator<float> RebindSelectedControl(bool alsoGetSecondary)
        {
            DisableColorsFor.Add(SelectedElement);
            CoroutineHandle flashHandle = Timing.RunCoroutine
            (
                FlashSelection(SelectedElement, 0.25f), 
                Segment.FixedUpdate,
                GetInstanceID().ToString()
            );

            DisableInput = true;
            AxisConfiguration configuration = GetAxisFromSelection();
            RebindingPositive = true;
            yield return Timing.WaitUntilDone(GetAndSetInput(configuration, true));

            if (alsoGetSecondary)
            {
                RebindingPositive = false;
                RebindingAltPositive = true;
                yield return Timing.WaitUntilDone(GetAndSetInput(configuration, false));
            }

            DisableColorsFor.Remove(SelectedElement);
            Timing.KillCoroutines(flashHandle);
            ShowInputsBasedOnOffset(AxesIndexOffset);
            yield return Timing.WaitForSeconds(0.25f);
            InputManager.Save();
            RebindingPositive = false;
            RebindingAltPositive = false;
            DisableInput = false;
        }

        public IEnumerator<float> FlashSelection(Text text, float delay)
        {
            while (isActiveAndEnabled)
            {
                text.color = text.color == Color.green ? Color.magenta : Color.green;
                yield return Timing.WaitForSeconds(delay);
            }
        }

        protected IEnumerator<float> GetAndSetInput(AxisConfiguration configuration, bool primary)
        {
            yield return Timing.WaitForSeconds(0.25f);
            yield return Timing.WaitUntilDone(new WaitUntil(() => InputManager.anyKeyOrMouseDown));
            if (Input.GetAxis("mouse_axis_2") > AxisConfiguration.ScrollWheelSensitivity)
            {
                if (primary)
                {
                    configuration.positive = KeyCode.Mouse3;
                }
                else
                {
                    configuration.altPositive = KeyCode.Mouse3;
                }
            }
            else if (Input.GetAxis("mouse_axis_2") < -AxisConfiguration.ScrollWheelSensitivity)
            {
                if (primary)
                {
                    configuration.positive = KeyCode.Mouse4;
                }
                else
                {
                    configuration.altPositive = KeyCode.Mouse4;
                }
            }
            else
            {
                foreach (KeyCode keyCode in Enum.GetValues(typeof(KeyCode)))
                {
                    if (keyCode == KeyCode.Mouse3 || keyCode == KeyCode.Mouse4) continue;

                    if (Input.GetKeyDown(keyCode) && keyCode == KeyCode.Delete)
                    {
                        if (primary)
                        {
                            configuration.positive = KeyCode.None;
                        }
                        else
                        {
                            configuration.altPositive = KeyCode.None;
                        }
                    }
                    else if (Input.GetKeyDown(keyCode))
                    {
                        if (primary)
                        {
                            configuration.positive = keyCode;
                        }
                        else
                        {
                            configuration.altPositive = keyCode;
                        }
                    }
                }
            }

            foreach (AxisConfiguration otherConfig in InputManager.PlayerOneConfiguration.axes)
            {
                if (otherConfig == configuration) continue;

                if (primary)
                {
                    if (otherConfig.positive == configuration.positive)
                    {
                        otherConfig.positive = KeyCode.None;
                    }
                }
                else
                {
                    if (otherConfig.altPositive == configuration.altPositive)
                    {
                        otherConfig.altPositive = KeyCode.None;
                    }
                }
            }
        }
    }
}