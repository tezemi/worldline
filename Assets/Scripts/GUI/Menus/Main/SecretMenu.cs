﻿using System;
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.GameState;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.GUI.Printing;
using UnityEngine;
using UnityEngine.UI;
using TeamUtility.IO;
using Random = UnityEngine.Random;

namespace Worldline.GUI.Menus.Main
{
    public class SecretMenu : ScaleMenu
    {
        public float ColorChangeMinSpeed = 0.02f;
        public float ColorChangeMaxSpeed = 0.34f;
        public float ColorChangeCooldownSpeed = 0.002f;
        public float PositionChangeMinSpeed = 1f;
        public float PositionChangeMaxSpeed = 5f;
        public float PositionChangeCooldownSpeed = 0.05f;
        public float PositionChangeMaxDistance = 8f;
        public GameObject MainMenu;
        public AdvancedText CheatEntryBox;
        public AudioClip GenericCheatWorkedSound;
        public AudioClip CheatNotWorkedSound;
        public AudioClip CheatAlreadyOnSound;
        public AudioClip LevelLoadSound;
        public AudioClip BackspaceSound;
        public AudioClip[] TypingSounds;
        [HideInInspector]
        public AudioClip[] SerializedSecretSounds;
        public float CurrentColorChangeSpeed { get; protected set; }
        public float CurrentPositionChangeSpeed { get; protected set; }
        private readonly Dictionary<Text, Color> _moveColorsTo = new Dictionary<Text, Color>();
        private readonly Dictionary<Text, Vector2> _originalTextPositions = new Dictionary<Text, Vector2>();
        private readonly Dictionary<Text, Vector2> _moveTextPositionsTo = new Dictionary<Text, Vector2>();

        protected override void OnEnable()
        {
            base.OnEnable();
            CurrentColorChangeSpeed = ColorChangeMinSpeed;
            CurrentPositionChangeSpeed = PositionChangeMinSpeed;
        }

        protected override void Update()
        {
            base.Update();
            if (CurrentlySelectedOption.Item1 != 0) return;

            foreach (KeyCode key in Enum.GetValues(typeof(KeyCode)))
            {
                if (!InputManager.GetKeyDown(key)) continue;

                if (key == KeyCode.Return)
                {
                    string text = CheatEntryBox.Message;
                    string hashedText = CheatController.GenerateHash(text);
                    if (CheatController.StringDictionary.ContainsKey(hashedText))
                    {
                        Cheat cheat = CheatController.StringDictionary[hashedText];
                        if (CheatController.IsActive(cheat))
                        {
                            SoundEffect.PlaySound(CheatAlreadyOnSound, false);
                        }
                        else
                        {
                            CheatController.EnableCheat(cheat);
                            var cheats = (Cheat[])Enum.GetValues(typeof(Cheat));
                            for (int i = 0; i < cheats.Length; i++)
                            {
                                if (cheats[i] == cheat)
                                {
                                    SoundEffect.PlaySound(SerializedSecretSounds[i], false);
                                    break;
                                }
                            }
                        }
                    }
                    else if (Application.CanStreamedLevelBeLoaded(text))
                    {
                        SoundEffect.PlaySound(LevelLoadSound, false);
                        SceneTransitionOperator.Main.LoadScene(text);
                    }
                    else
                    {
                        SoundEffect.PlaySound(CheatNotWorkedSound, false);
                    }

                    CheatEntryBox.SetMessage(string.Empty);
                }
                else if (key == KeyCode.Backspace)
                {
                    if (CheatEntryBox.Message.Length > 0)
                    {
                        CheatEntryBox.SetMessage(CheatEntryBox.Message.Remove(CheatEntryBox.Message.Length - 1));
                    }

                    PlayMenuSound(BackspaceSound);
                }
                else if (key == KeyCode.Space)
                {
                    if (CheatEntryBox.Message.Length > 0)
                    {
                        CheatEntryBox.SetMessage(CheatEntryBox.Message += " ");
                    }

                    PlayMenuSound(TypingSounds[Random.Range(0, TypingSounds.Length)]);
                }
                else if ((int)key >= 97 && (int)key <= 122 || (int)key >= 48 && (int)key <= 57)
                {
                    string keyAsString;
                    if ((int)key >= 48 && (int)key <= 57)
                    {
                        keyAsString = ((int)key - 48).ToString();
                    }
                    else
                    {
                        keyAsString = key.ToString();
                    }

                    if (CheatEntryBox.Message.Length < CheatEntryBox.Lines[0].Count - 1)
                    {
                        CheatEntryBox.SetMessage(CheatEntryBox.Message += keyAsString);
                    }

                    CurrentColorChangeSpeed = ColorChangeMaxSpeed;
                    CurrentPositionChangeSpeed = ColorChangeMaxSpeed;
                    PlayMenuSound(TypingSounds[Random.Range(0, TypingSounds.Length)]);
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            foreach (Text t in CheatEntryBox.Characters)
            {
                if (!_moveColorsTo.ContainsKey(t))
                {
                    _moveColorsTo.Add(t, TimeOperator.EffectColors[(TimeEffect)Random.Range(0, 6)]);
                }

                t.color = Vector4.MoveTowards(t.color, _moveColorsTo[t], CurrentColorChangeSpeed);
                if (t.color == _moveColorsTo[t])
                {
                    _moveColorsTo[t] = TimeOperator.EffectColors[(TimeEffect)Random.Range(0, 6)];
                }

                if (CurrentColorChangeSpeed > ColorChangeMinSpeed)
                {
                    CurrentColorChangeSpeed -= ColorChangeCooldownSpeed;
                    if (CurrentColorChangeSpeed < ColorChangeMinSpeed)
                    {
                        CurrentColorChangeSpeed = ColorChangeMinSpeed;
                    }
                }

                if (!_originalTextPositions.ContainsKey(t))
                {
                    _originalTextPositions.Add(t, t.rectTransform.anchoredPosition);
                }

                if (!_moveTextPositionsTo.ContainsKey(t))
                {
                    _moveTextPositionsTo.Add(t, _originalTextPositions[t] + new Vector2
                    (
                        Random.Range(-PositionChangeMaxDistance, PositionChangeMaxDistance),
                        Random.Range(-PositionChangeMaxDistance, PositionChangeMaxDistance)
                    ));
                }

                t.rectTransform.anchoredPosition = Vector2.MoveTowards
                (
                    t.rectTransform.anchoredPosition,
                    _moveTextPositionsTo[t],
                    CurrentPositionChangeSpeed
                );

                if (t.rectTransform.anchoredPosition == _moveTextPositionsTo[t])
                {
                    _moveTextPositionsTo[t] = _originalTextPositions[t] + new Vector2
                    (
                        Random.Range(-PositionChangeMaxDistance, PositionChangeMaxDistance),
                        Random.Range(-PositionChangeMaxDistance, PositionChangeMaxDistance)
                    );
                }

                if (CurrentPositionChangeSpeed > PositionChangeMinSpeed)
                {
                    CurrentPositionChangeSpeed -= PositionChangeCooldownSpeed;
                    if (CurrentPositionChangeSpeed < PositionChangeMinSpeed)
                    {
                        CurrentPositionChangeSpeed = PositionChangeMinSpeed;
                    }
                }
            }
        }

        protected override void OnBackOut()
        {
            MainMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 1 && itemClicked.Item2 == 0)
            {
                OptionSelected();
                OnBackOut();
            }
        }
    }
}
