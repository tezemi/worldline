// Copyright (c) 2019 Destin Hebner

//namespace Worldline.GUI.Menus.Main
//{
//    public class LoadGameFileSelect : MenuOperator
//    {
//        public GameObject MainMenu;
//        public string GameScene;
//        public Sprite Easy;
//        public Sprite Normal;
//        public Sprite Hard;
//        public Sprite Ass;
//        public Sprite SaveIcon;
//        public Image DiffIcon;
//        public Text DiffText;
//        public Text HealthText;
//        public Text CurrencyText;
//        public Text TimeText;
//        public GameObject FileDescGroup;
//        public GameObject NewDescGroup;
//        //public GameState.FileMeta[] FileMetas { get; protected set; }
//        private bool _disableUpdate;

//        protected override void Awake()
//        {
//            base.Awake();

//            Options[0].text = CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["FileA"];
//            Options[1].text = CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["FileB"];
//            Options[2].text = CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["FileC"];
//            NewDescGroup.GetComponent<Text>().text = CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["LoadFile"];
//        }

//        private void OnEnable()
//        {
//            //FileMetas = GameState.LoadFileMeta();
//        }

//        protected override void Update()
//        {
//            if (_disableUpdate) return;
//            base.Update();

//            //for (int i = 0; i < GameState.NumberOfSaveFiles; i++)
//            //{
//            //    if (i == CurrentColumn)
//            //    {
//            //        if (FileMetas?[i] != null)
//            //        {
//            //            FileDescGroup.SetActive(true);
//            //            NewDescGroup.SetActive(false);
//            //            DiffIcon.sprite = FileMetas[i].FileDifficulty == Difficulty.Easy ? Easy : FileMetas[i].FileDifficulty == Difficulty.Normal ? Normal : FileMetas[i].FileDifficulty == Difficulty.Hard ? Hard : Ass;
//            //            DiffText.text = FileMetas[i].FileDifficulty == Difficulty.Easy ? CultureString.Get("General/Difficulty", Application.persistentDataPath, Application.dataPath)["Easy"] : FileMetas[i].FileDifficulty == Difficulty.Normal ? CultureString.Get("General/Difficulty", Application.persistentDataPath, Application.dataPath)["Normal"] : FileMetas[i].FileDifficulty == Difficulty.Hard ? CultureString.Get("General/Difficulty", Application.persistentDataPath, Application.dataPath)["Hard"] : CultureString.Get("General/Difficulty", Application.persistentDataPath, Application.dataPath)["AssBlasting"];
//            //            HealthText.text = FileMetas[i].Health + " / " + FileMetas[i].MaxHealth;
//            //            CurrencyText.text = FileMetas[i].Currency.ToString();
//            //            //TimeText.text = FileMetas[i].Playtime.Hours + " " + CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["Hours"] + " " + FileMetas[i].Playtime.Minutes + " " + CultureString.Get("MainMenu/Main", Application.persistentDataPath, Application.dataPath)["Minutes"];
//            //            string addHours = FileMetas[i].Playtime.Hours < 10 ? "0" : "";
//            //            string addMinutes = FileMetas[i].Playtime.Minutes < 10 ? "0" : "";
//            //            TimeText.text = addHours + FileMetas[i].Playtime.Hours + ":" + addMinutes + FileMetas[i].Playtime.Minutes;
//            //        }
//            //        else
//            //        {
//            //            FileDescGroup.SetActive(false);
//            //            NewDescGroup.SetActive(true);
//            //        }
//            //    }
//            //}

//            if (InputManagerExtended.GetBackButton())
//            {
//                MainMenu.SetActive(true);
//                gameObject.SetActive(false);
//            }
//        }

//        protected override void OnClick(int elementClicked)
//        {
//            //if (FileMetas[elementClicked] != null)
//            //{
//            //    ElementClick(elementClicked);
//            //}
//        }

//        private void ElementClick(int elementClicked)
//        {
//            if (SceneTransitionOperator.Main.Fading) return;

//            //switch (elementClicked)
//            //{
//            //    case 0:
//            //        GameState.SaveFile = SaveFile.A;
//            //        break;
//            //    case 1:
//            //        GameState.SaveFile = SaveFile.B;
//            //        break;
//            //    case 2:
//            //        GameState.SaveFile = SaveFile.C;
//            //        break;
//            //    default:
//            //        throw new ArgumentOutOfRangeException();
//            //}

//            enabled = false;
//            SceneTransitionOperator.Main.LoadGame();
//        }

//        private IEnumerator DisableUpdate()
//        {
//            _disableUpdate = true;
//            yield return new WaitForSeconds(0.02f);
//            _disableUpdate = false;
//        }
//    }
//}
