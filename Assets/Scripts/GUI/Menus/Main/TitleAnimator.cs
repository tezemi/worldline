﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GUI.Menus.Main
{
    /// <summary>
    /// When attached to the title on the main menu, it will float off
    /// screen when the MainMenu is closed, and float back when it is
    /// opened again.
    /// </summary>
    public class TitleAnimator : MonoBehaviour
    {
        public MainMenu MainMenu;
        public Vector2 StartingPosition { get; protected set; }
        public const float AnimationSpeed = 0.04f;
        public const float OffScreenOffset = 300f;

        protected virtual void Awake()
        {
            StartingPosition = GetComponent<RectTransform>().anchoredPosition;
        }

        protected virtual void FixedUpdate()
        {
            if (MainMenu.isActiveAndEnabled)
            {
                GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp
                (
                    GetComponent<RectTransform>().anchoredPosition,
                    StartingPosition,
                    AnimationSpeed
                );
            }
            else
            {
                GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp
                (
                    GetComponent<RectTransform>().anchoredPosition,
                    StartingPosition + new Vector2(0f, OffScreenOffset),
                    AnimationSpeed
                );
            }
        }
    }
}
