﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class ControlsMenu : TextIndentionMenu
    {
        public GameObject PreviousMenu;
        public Text KeysText;
        public Text DescriptionText;
        public EditControlMenu EditControlMenu;
        private bool _showResetMessage;

        protected override void Update()
        {
            base.Update();
            KeysText.text = "";
            if (_showResetMessage)
            {
                DescriptionText.color = Color.yellow;
                DescriptionText.text = "Reset all inputs to their defaults!";
            }
            else
            {
                DescriptionText.color = Color.white;
                if (CurrentlySelectedOption.Item1 == 0)
                {
                    DescriptionText.text = "Goodbye!";
                }
                else if (CurrentlySelectedOption.Item1 == 1)
                {
                    DescriptionText.text = "Pick out singular inputs to edit.";
                }
                else if (CurrentlySelectedOption.Item1 == 2)
                {
                    DescriptionText.text = "Unbind all controls and then go down the list of inputs one-by-one and assign a new key.";
                }
                else if (CurrentlySelectedOption.Item1 == 3)
                {
                    DescriptionText.text = "Resets inputs to the game's defaults.";
                }
            }
        }

        protected override void OnBackOut()
        {
            EditControlMenu.enabled = true;
            enabled = false;
            foreach (Text t in SerializedOptions)
            {
                t.color = UnselectedColor;
                t.rectTransform.anchoredPosition = OriginalPositions[t];
            }
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                PreviousMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 1)
            {
                OnBackOut();
            }
            else if (itemClicked.Item1 == 2)
            {
                foreach (AxisConfiguration axisConfiguration in InputManager.GetInputConfiguration("Keyboard Mode").axes)
                {
                    axisConfiguration.positive = KeyCode.None;
                    axisConfiguration.altPositive = KeyCode.None;
                }

                Timing.RunCoroutine(BindAll(), Segment.Update, GetInstanceID().ToString());
            }
            else if (itemClicked.Item1 == 3)
            {
                List<AxisConfiguration> defaultAxes = InputManager.GetInputConfiguration("Default").axes;
                List<AxisConfiguration> currentAxes = InputManager.PlayerOneConfiguration.axes;
                for (var i = 0; i < defaultAxes.Count; i++)
                {
                    currentAxes[i].Copy(defaultAxes[i]);
                }

                InputManager.Save();
                Timing.RunCoroutine(ShowResetMessage(), Segment.FixedUpdate, GetInstanceID().ToString());

                IEnumerator<float> ShowResetMessage()
                {
                    _showResetMessage = true;
                    yield return Timing.WaitForSeconds(1f);
                    _showResetMessage = false;
                }
            }
        }

        protected virtual IEnumerator<float> BindAll()
        {
            enabled = false;
            EditControlMenu.DisableInput = true;
            EditControlMenu.enabled = true;
            EditControlMenu.CurrentlySelectedOption = (0, 0);
            EditControlMenu.AxesIndexOffset = 0;
            int lasti = 0 ;
            for (int i = 0; i < InputManager.PlayerOneConfiguration.axes.Count; i++)
            {
                if (i < EditControlMenu.SerializedOptions.Length)
                {
                    EditControlMenu.CurrentlySelectedOption = (i, 0);
                    lasti = i;
                }
                else
                {
                    EditControlMenu.CurrentlySelectedOption = (lasti, 0);
                    EditControlMenu.AxesIndexOffset = i - lasti;
                }

                yield return Timing.WaitForSeconds(0.25f);
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(EditControlMenu.RebindSelectedControl(false)));
            }

            InputManager.Save();
            EditControlMenu.CurrentlySelectedOption = (0, 0);
            EditControlMenu.AxesIndexOffset = 0;
            EditControlMenu.enabled = false;
            enabled = true;
        }
    }
}
