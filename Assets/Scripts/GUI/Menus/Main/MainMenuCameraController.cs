﻿using UnityEngine;

namespace Worldline.GUI.Menus.Main
{
    public class MainMenuCameraController : MonoBehaviour
    {
        public static MainMenuCameraController Main { get; protected set; }
        public float Speed = 0.02f;
        public GameObject[] Menus;
        public Vector3[] Positions;
        private int _menuIndex;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void FixedUpdate()
        {
            for (int i = 0; i < Menus.Length; i++)
            {
                if (Menus[i].activeSelf)
                {
                    _menuIndex = i;
                    break;
                }
            }

            transform.position = Vector3.Lerp(transform.position, Positions[_menuIndex], Speed);
        }
    }
}
