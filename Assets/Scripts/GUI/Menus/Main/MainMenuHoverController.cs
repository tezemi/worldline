﻿using UnityEngine;

namespace Worldline.GUI.Menus.Main
{
    public class MainMenuHoverController : MonoBehaviour
    {
        public float Intensity = 1f;
        private float _startY;

        protected virtual void OnEnable()
        {
            _startY = transform.position.y;
        }

        protected virtual void Update()
        {
            transform.position = new Vector3(transform.position.x, _startY + Mathf.Sin(Time.realtimeSinceStartup * Intensity), transform.position.z);
        }
    }
}
