﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.GameState;
//using Worldline.Player;
using Worldline.TimeEngine;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Worldline.GUI.Menus.Main
{
    public class FileSelectMenu : ScrollMenu
    {
        public GameObject PreviousMenu;
        public GameObject NewSaveFileTemplate;
        public GameObject UsedSaveFileTemplate;
        public GameObject DifficultySelectMenu;
        public bool Loading { get; set; }
        public List<SaveFile> SaveFiles { get; protected set; }

        protected override void Awake()
        {
            base.Awake();
            SaveFiles = SaveFile.GetSaveFilesOnHardDisk();
            foreach (SaveFile saveFile in SaveFiles)
            {
                GameObject usedTemplate = Instantiate(UsedSaveFileTemplate);
                AddOption(usedTemplate.GetComponent<Graphic>(), true);
                usedTemplate.SetActive(true);
                usedTemplate.GetComponent<Image>().color = TimeOperator.EffectColors[(TimeEffect)Random.Range(0, 5)];
                SetElementText(saveFile, usedTemplate.GetComponentInChildren<Text>());
            }

            if (!Loading)
            {
                GameObject newTemplate = Instantiate(NewSaveFileTemplate);
                AddOption(newTemplate.GetComponent<Graphic>(), true);
                newTemplate.SetActive(true);
                newTemplate.GetComponent<Image>().color = TimeOperator.EffectColors[(TimeEffect) Random.Range(0, 5)];
            }
        }

        protected virtual void SetElementText(SaveFile saveFile, Text text)
        {
            if (saveFile.FileDatum == null)
            {
                text.text = "Error Loading File";
                //return;
            }

            //int health = ((PlayerData)saveFile.FileDatum.Find(f => f is PlayerData)).Health;
            //int maxHealth = ((PlayerData)saveFile.FileDatum.Find(f => f is PlayerData)).MaxHealth;
            //string difficulty = $"{CultureString.Get("MainMenu/Main")["Difficulty"]} {saveFile.Difficulty}{Environment.NewLine}";
            //string playtime = $"{CultureString.Get("MainMenu/Main")["Playtime"]} {saveFile.TotalPlayedTime}{Environment.NewLine}";
            //string healthRatio = $"{CultureString.Get("MainMenu/Main")["Health"]} {health} / {maxHealth}{Environment.NewLine}";
            //text.text = $"{difficulty}{playtime}{healthRatio}";
        }

        protected override void OnBackOut()
        {
            PreviousMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (Options[itemClicked.Item1, itemClicked.Item2] != null)
            {
                if (Loading)
                {
                    //SaveFile.LoadedSaveFile = SaveFile.LoadSaveFile(itemClicked.Item1);
                    SaveFile.LoadedSaveFile.LoadGame();
                }
                else
                {
                    //bool playerWantsToOverwrite = true;
                    //if (SaveFile.SaveFileExists(itemClicked.Item1)) // TODO: More than four save files.
                    //{
                    //    Debug.Log("Ask for warning.");
                    //    playerWantsToOverwrite = false;
                    //}

                    //if (playerWantsToOverwrite)
                    //{
                    //    DifficultySelectMenu.SetActive(true);
                    //    gameObject.SetActive(false);
                    //}
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }
    }
}
