﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Utilities;
using MEC;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Main
{
    public class OptionsMenu : TextMenu
    {
        public Text ResolutionValue;
        public Text FullScreenValue;
        public Text PhotosensitiveModeValue;
        public Text MasterVolumeValue;
        public Text MusicVolumeValue;
        public Text SFXVolumeValue;
        public Text LanguageValue;
        public Text RequireRestart;
        public SliderBar MasterVolumeSlider;
        public SliderBar MusicVolumeSlider;
        public SliderBar SFXVolumeSlider;
        public GameObject MainMenu;
        public GameObject ControlsMenu;
        public int TempResolutionIndex { get; protected set; }
        public bool TempFullScreen { get; protected set; }
        public bool TempPhotosensitiveMode { get; protected set; }
        public float TempMasterVolume { get; protected set; }
        public float TempMusicVolume { get; protected set; }
        public float TempSfxVolume { get; protected set; }
        public int TempLanguageIndex { get; protected set; }
        public Language TempLanguage { get; protected set; }  
        public string Yes { get; private set; }
        public string No { get; private set; }
        public readonly Color ChangedOptionColor = Color.red;
        public readonly Color UnchangedOptionColor = Color.white;
        public const float VolumeSliderDelay = 0.1f;
        public const float VolumeSliderIncrement = 0.01f;
        public const float VolumeSliderRampUp = 0.01f;
        private GameOptions _options;
        private Language[] _languages;

        protected override void OnEnable()
        {
            base.OnEnable();
            _options = GameOptions.LoadedGameOptions;
            _languages = GameOptions.AvailableLanguages;
            Yes = SerializedStrings.GetString("General/VeryGeneric", "YesMenu");
            No = SerializedStrings.GetString("General/VeryGeneric", "NoMenu");
            RequireRestart.text = SerializedStrings.GetString("MainMenu/Main", "Restart");
            SyncMenuWithFile();
            Timing.RunCoroutine(VolumeSliderTick(), Segment.Update, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected override void Update()
        {
            if (SliderBar.SelectedSliderBar == MasterVolumeSlider)
            {
                CurrentlySelectedOption = (3, 0);
            }
            else if (SliderBar.SelectedSliderBar == MusicVolumeSlider)
            {
                CurrentlySelectedOption = (4, 0);
            }
            else if (SliderBar.SelectedSliderBar == SFXVolumeSlider)
            {
                CurrentlySelectedOption = (5, 0);
            }

            base.Update();
            UpdateLeftAndRight();
            TempMasterVolume = MasterVolumeSlider.Value;
            TempMusicVolume = SliderValueToMixerVolume(MusicVolumeSlider.Value);
            TempSfxVolume = SliderValueToMixerVolume(SFXVolumeSlider.Value);

            if (TempResolutionIndex >= Screen.resolutions.Length)
            {
                TempResolutionIndex = 0;
            }

            ResolutionValue.text = Screen.resolutions[TempResolutionIndex].ToString();
            FullScreenValue.text = TempFullScreen ? Yes : No;
            PhotosensitiveModeValue.text = TempPhotosensitiveMode ? Yes : No;
            MasterVolumeValue.text = $"{(int)(TempMasterVolume * 100f)}%";
            int i = (int)MixerVolumeToSliderValue(TempMusicVolume);
            Debug.Log($"{MixerVolumeToSliderValue(TempMusicVolume)} - {i}");
            MusicVolumeValue.text = $"{(int)(MixerVolumeToSliderValue(TempMusicVolume) * 100f)}%";
            SFXVolumeValue.text = $"{(int)(MixerVolumeToSliderValue(TempSfxVolume) * 100f)}%";
            if (SerializedStrings.GetStringOrNull("General/Languages", TempLanguage.ToString()) != null)
            {
                LanguageValue.text = SerializedStrings.GetString("General/Languages", TempLanguage.ToString());
            }
            else
            {
                LanguageValue.text = TempLanguage.ToString();
            }           

            ResolutionValue.color = TempResolutionIndex != _options.ResolutionIndex ? 
                ChangedOptionColor : UnchangedOptionColor;
            FullScreenValue.color = TempFullScreen != _options.Fullscreen ?
                ChangedOptionColor : UnchangedOptionColor;
            PhotosensitiveModeValue.color = TempPhotosensitiveMode != _options.PhotosensitiveMode ?
                ChangedOptionColor : UnchangedOptionColor;
            LanguageValue.color = TempLanguage != _options.Language ?
                ChangedOptionColor : _languages.Length == 1 ? Color.grey : UnchangedOptionColor;
            MasterVolumeValue.color = Math.Abs(TempMasterVolume - _options.MasterVolume) > 0.09f ?
                ChangedOptionColor : UnchangedOptionColor;
            MusicVolumeValue.color = Math.Abs(TempMusicVolume - _options.MusicVolume) > 0.9f ?
                ChangedOptionColor : UnchangedOptionColor;
            SFXVolumeValue.color = Math.Abs(TempSfxVolume - _options.SoundEffectsVolume) > 0.9f ?
                ChangedOptionColor : UnchangedOptionColor;

            RequireRestart.enabled = TempLanguage != _options.Language;
        }

        /// <summary>
        /// Called inside the Update(), this handles the player pressing their
        /// left and right inputs with an option selected.
        /// </summary>
        protected virtual void UpdateLeftAndRight()
        {
            if (InputManager.GetButtonDown("right"))
            {
                if (CurrentlySelectedOption.Item1 == 0)
                {
                    if (++TempResolutionIndex >= Screen.resolutions.Length)
                    {
                        TempResolutionIndex = 0;
                    }
                }
                else if (CurrentlySelectedOption.Item1 == 1)
                {
                    TempFullScreen = !TempFullScreen;
                }
                else if (CurrentlySelectedOption.Item1 == 2)
                {
                    TempPhotosensitiveMode = !TempPhotosensitiveMode;
                }
                else if (CurrentlySelectedOption.Item1 == 6)
                {
                    if (_languages.Length == 1)
                    {
                        SoundEffect.PlaySound(UnselectableOptionSound, false);
                    }
                    else
                    {
                        if (++TempLanguageIndex >= _languages.Length)
                        {
                            TempLanguageIndex = 0;
                        }

                        TempLanguage = _languages[TempLanguageIndex];
                    }
                }
            }
            else if (InputManager.GetButtonDown("left"))
            {
                if (CurrentlySelectedOption.Item1 == 0)
                {
                    if (--TempResolutionIndex < 0)
                    {
                        TempResolutionIndex = Screen.resolutions.Length - 1;
                    }
                }
                else if (CurrentlySelectedOption.Item1 == 1)
                {
                    TempFullScreen = !TempFullScreen;
                }
                else if (CurrentlySelectedOption.Item1 == 2)
                {
                    TempPhotosensitiveMode = !TempPhotosensitiveMode;
                }
                else if (CurrentlySelectedOption.Item1 == 6)
                {
                    if (_languages.Length == 1)
                    {
                        SoundEffect.PlaySound(UnselectableOptionSound, false);
                    }
                    else
                    {
                        if (--TempLanguageIndex < 0)
                        {
                            TempLanguageIndex = _languages.Length - 1;
                        }

                        TempLanguage = _languages[TempLanguageIndex];
                    }
                }
            }
        }

        /// <summary>
        /// This runs every VolumeSliderDelay while the player is pressing the 
        /// left or right inputs, and handles increasing or decreasing the volumes
        /// of the three different volume sliders.
        /// </summary>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> VolumeSliderTick()
        {
            float speedDecrease = 0f;
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitUntilDone(new WaitUntil
                (
                    () => InputManager.GetButton("right") || InputManager.GetButton("left")
                ));

                if (CurrentlySelectedOption.Item1 == 3)
                {
                    if (InputManager.GetButton("right"))
                    {
                        MasterVolumeSlider.Value += VolumeSliderIncrement;
                    }
                    else if (InputManager.GetButton("left"))
                    {
                        MasterVolumeSlider.Value -= VolumeSliderIncrement;
                    }
                }
                else if (CurrentlySelectedOption.Item1 == 4)
                {
                    if (InputManager.GetButton("right"))
                    {
                        MusicVolumeSlider.Value += VolumeSliderIncrement;
                    }
                    else if (InputManager.GetButton("left"))
                    {
                        MusicVolumeSlider.Value -= VolumeSliderIncrement;
                    }
                }
                else if (CurrentlySelectedOption.Item1 == 5)
                {
                    if (InputManager.GetButton("right"))
                    {
                        SFXVolumeSlider.Value += VolumeSliderIncrement;
                    }
                    else if (InputManager.GetButton("left"))
                    {
                        SFXVolumeSlider.Value -= VolumeSliderIncrement;
                    }
                }

                yield return Timing.WaitForSeconds(VolumeSliderDelay - speedDecrease);
                if (InputManager.GetButton("right") || InputManager.GetButton("left"))
                {
                    speedDecrease += VolumeSliderRampUp;
                }
                else
                {
                    speedDecrease = 0f;
                }
            }
        }

        protected override void OnBackOut()
        {
            MainMenu.SetActive(true);
            gameObject.SetActive(false);
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                if (++TempResolutionIndex >= Screen.resolutions.Length)
                {
                    TempResolutionIndex = 0;
                }
            }
            else if (itemClicked.Item1 == 1)
            {
                TempFullScreen = !TempFullScreen;
            }
            else if (itemClicked.Item1 == 2)
            {
                TempPhotosensitiveMode = !TempPhotosensitiveMode;
            }
            else if (itemClicked.Item1 == 6)
            {
                if (_languages.Length == 1)
                {
                    SoundEffect.PlaySound(UnselectableOptionSound, false);
                }
                else
                {
                    if (++TempLanguageIndex >= _languages.Length)
                    {
                        TempLanguageIndex = 0;
                    }

                    TempLanguage = _languages[TempLanguageIndex];
                }
            }
            else if (itemClicked.Item1 == 7)
            {
                ControlsMenu.SetActive(true);
                gameObject.SetActive(false);
            }
            else if (itemClicked.Item1 == 8)        // Apply button clicked
            {
                bool needFixSliders = _options.ResolutionIndex != TempResolutionIndex || _options.Fullscreen != TempFullScreen; // Resolution changes fuck with sliders
                _options.ResolutionIndex = TempResolutionIndex;                
                _options.Fullscreen = TempFullScreen;
                _options.PhotosensitiveMode = TempPhotosensitiveMode;
                _options.MasterVolume = TempMasterVolume;
                _options.MusicVolume = TempMusicVolume;
                _options.SoundEffectsVolume = TempSfxVolume;
                _options.Language = TempLanguage;
                if (needFixSliders)
                {
                    gameObject.SetActive(false);
                }

                _options.ApplyOptions();
                _options.Save();

                if (needFixSliders) // This if fixed by assigning the slider's positions based on the change
                {
                    Timing.RunCoroutine(RestartMenu(), Segment.Update);

                    IEnumerator<float> RestartMenu()
                    {
                        yield return Timing.WaitForSeconds(1f);
                        gameObject.SetActive(true);
                    }
                }
            }
            else if (itemClicked.Item1 == 9) // Default values button clicked
            {
                GameOptions.LoadedGameOptions = new GameOptions();
                _options = GameOptions.LoadedGameOptions;
                _options.ApplyOptions();
                _options.Save();
                SyncMenuWithFile();

                gameObject.SetActive(false);
                Timing.RunCoroutine(RestartMenu(), Segment.Update);

                IEnumerator<float> RestartMenu()
                {
                    yield return Timing.WaitForSeconds(1f);
                    gameObject.SetActive(true);
                }
            }
            else if (itemClicked.Item1 == 10)
            {
                OnBackOut();
            }
        }

        /// <summary>
        /// Updates the options menu to reflect the values within the currently 
        /// loaded options file. This is almost the opposite of pressing the apply
        /// button, which would update the options file to reflect the values on
        /// the menu.
        /// </summary>
        public virtual void SyncMenuWithFile()
        {
            TempResolutionIndex = _options.ResolutionIndex;
            TempFullScreen = _options.Fullscreen;
            TempPhotosensitiveMode = _options.PhotosensitiveMode;
            TempMasterVolume = _options.MasterVolume;
            TempMusicVolume = _options.MusicVolume;
            TempSfxVolume = _options.SoundEffectsVolume;
            TempLanguage = _options.Language;

            Canvas.ForceUpdateCanvases();
            MasterVolumeSlider.Value = TempMasterVolume;
            MusicVolumeSlider.Value = MixerVolumeToSliderValue(TempMusicVolume);
            SFXVolumeSlider.Value = MixerVolumeToSliderValue(TempSfxVolume);
        }

        /// <summary>
        /// Slider UI elements only output a range from 0f to 1f depending on the 
        /// position of the slider. Meanwhile, AudioMixer's volumes are measured
        /// from -80f to 0f. This will convert an AudioMixer's volume to a 
        /// corresponding value for a slider. For example, if the input here was
        /// -40f, the returned value would be 0.5f.
        /// </summary>
        /// <param name="audioMixerVolume">The AudioMixer's volume.</param>
        /// <returns>A corresponding value for a slider.</returns>
        public static float MixerVolumeToSliderValue(float audioMixerVolume)
        {
            return MathUtilities.GetRange
            (
                audioMixerVolume,
                GameOptions.AudioMixerMinVolume,
                GameOptions.AudioMixerMaxVolume,
                1f,
                0f
            );
        }

        /// <summary>
        /// Slider UI elements only output a range from 0f to 1f depending on the 
        /// position of the slider. Meanwhile, AudioMixer's volumes are measured
        /// from -80f to 0f. This will convert a Slider's value to a 
        /// corresponding volume for an AudioMixer. For example, if the input here 
        /// was 0.5f, the returned value would be -40f.
        /// </summary>
        /// <param name="sliderValue">The value of a SliderBar.</param>
        /// <returns>A corresponding AudioMixer volume.</returns>
        public static float SliderValueToMixerVolume(float sliderValue)
        {
            return MathUtilities.GetRange
            (
                sliderValue,
                0f,
                1f,
                GameOptions.AudioMixerMaxVolume,
                GameOptions.AudioMixerMinVolume
            );
        }
    }
}
