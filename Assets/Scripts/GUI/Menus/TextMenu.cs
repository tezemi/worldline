﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    /// <summary>
    /// This is a menu that can be used to configure text options, highlighting
    /// the selected option with a specific color.
    /// </summary>
    public abstract class TextMenu : GameMenu<Text>
    {
        public Color SelectedColor = Color.red;
        public Color UnselectedColor = Color.blue;
        public List<Text> DisableColorsFor { get; protected set; } = new List<Text>();

        protected override void Update()
        {
            foreach (Text text in Options)
            {
                if (text == null || DisableColorsFor.Contains(text)) continue;

                text.color = UnselectedColor;
            }

            if (!DisableColorsFor.Contains(Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2]))
            {
                Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2].color = SelectedColor;
            }

            base.Update();
        }
    }
}
