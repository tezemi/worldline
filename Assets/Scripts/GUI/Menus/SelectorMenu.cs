﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    /// <summary>
    /// A menu used to configure options that represents what is selected
    /// using some graphic, preferably an arrow or box.
    /// </summary>
    public abstract class SelectorMenu : GameMenu<Graphic>
    {
        public bool ScaleSelectorToFitImage;
        public int SelectorIndexFromEnd;
        public float SelectorAnimationSpeed = 0.04f;
        public Image.Type SelectorImageType;
        public Vector2 SelectorOffset;
        public Vector2 SelectorSizeDelta;
        public Sprite SelectorSprite;
        public GameObject Selector { get; protected set; }
        
        protected override void Awake()
        {
            base.Awake();
            if (Selector == null)
            {
                Selector = new GameObject($"{name} Selector", typeof(Image));
                Selector.GetComponent<Image>().sprite = SelectorSprite;
                Selector.GetComponent<Image>().SetNativeSize();
                Selector.GetComponent<Image>().type = SelectorImageType;
                Selector.transform.SetParent(transform);
                Selector.transform.SetSiblingIndex(Selector.transform.parent.childCount - 1 - SelectorIndexFromEnd);
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Selector.gameObject.SetActive(true);
            Selector.transform.position = (Vector2)SelectedElement.transform.position + SelectorOffset;
        }

        protected virtual void OnDisable()
        {
            if (Selector != null)
            {
                Selector.gameObject.SetActive(false);
            }
        }

        protected override void Update()
        {
            base.Update();
            if (SelectedElement != null && Selector.activeSelf)
            {
                Selector.transform.position = Vector2.Lerp
                (
                    Selector.transform.position,
                    (Vector2) SelectedElement.transform.position + SelectorOffset,
                    SelectorAnimationSpeed
                );

                if (ScaleSelectorToFitImage && Selector.activeSelf)
                {
                    Selector.GetComponent<RectTransform>().localScale = new Vector3(1f, 1f, 1f);
                    Selector.GetComponent<RectTransform>().sizeDelta = SelectedElement.rectTransform.sizeDelta + SelectorSizeDelta;
                }
            }
        }
    }
}
