﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    public class SliderBar : MonoBehaviour
    {
        public static SliderBar SelectedSliderBar { get; protected set; }
        public Image Knob;
        public Image Background;
        private bool _dragging;

        public float Value
        {
            get => MathUtilities.GetRange
            (
                Knob.rectTransform.position.x,
                Background.GetWorldBounds().min.x,
                Background.GetWorldBounds().max.x,
                1f,
                0f
            );
            set
            {
                if (value > 1f)
                {
                    value = 1f;
                }

                if (value < 0f)
                {
                    value = 0f;
                }

                float xPos = MathUtilities.GetRange
                (
                    value,
                    0f,
                    1f,
                    Background.GetWorldBounds().max.x,
                    Background.GetWorldBounds().min.x
                );

                Knob.rectTransform.position = new Vector3
                (
                    xPos,
                    Knob.rectTransform.position.y,
                    Knob.rectTransform.position.z
                );
            }
        }

        protected virtual void Update()
        {
            if ((Knob.MouseInside() || _dragging) &&
            (SelectedSliderBar == this || SelectedSliderBar == null) &&
            InputManager.GetButton("primary"))
            {
                _dragging = true;
                SelectedSliderBar = this;
                Knob.rectTransform.position = new Vector3
                (
                    InputManager.mousePosition.x,
                    Knob.rectTransform.position.y,
                    Knob.rectTransform.position.z
                );

                if (Knob.rectTransform.position.x < Background.GetWorldBounds().min.x)
                {
                    Knob.rectTransform.position = new Vector3
                    (
                        Background.GetWorldBounds().min.x,
                        Knob.rectTransform.position.y,
                        Knob.rectTransform.position.z
                    );
                }
                else if (Knob.rectTransform.position.x > Background.GetWorldBounds().max.x)
                {
                    Knob.rectTransform.position = new Vector3
                    (
                        Background.GetWorldBounds().max.x,
                        Knob.rectTransform.position.y,
                        Knob.rectTransform.position.z
                    );
                }
            }
            else
            {
                _dragging = false;
                if (SelectedSliderBar == this)
                {
                    SelectedSliderBar = null;
                }
            }

            Knob.rectTransform.position = new Vector3
            (
                Mathf.RoundToInt(Knob.rectTransform.position.x), 
                Knob.rectTransform.position.y, 
                Knob.rectTransform.position.z
            );
        }
    }
}
