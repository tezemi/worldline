﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    /// <summary>
    /// This a menu that can be used to configure text options with a selected
    /// color and also indents the selected option.
    /// </summary>
    public abstract class TextIndentionMenu : TextMenu
    {
        public float IndentAnimationSpeed = 0.08f;
        public float IndentAmount = 25f;
        public Dictionary<Text, Vector2> OriginalPositions { get; set; } = new Dictionary<Text, Vector2>();

        protected override void Awake()
        {
            base.Awake();
            foreach (Text text in Options)
            {
                if (text == null) continue;

                if (!OriginalPositions.ContainsKey(text))
                {
                    OriginalPositions.Add(text, text.rectTransform.anchoredPosition);
                }
            }
        }

        protected override void Update()
        {
            for (var x = 0; x < Options.GetLength(0); x++)
            {
                for (var y = 0; y < Options.GetLength(1); y++)
                {
                    Text text = Options[x, y];
                    if (text == null) continue;

                    bool isSelected = x == CurrentlySelectedOption.Item1 && y == CurrentlySelectedOption.Item2;
                    text.rectTransform.anchoredPosition = Vector2.Lerp
                    (
                        text.rectTransform.anchoredPosition,
                        isSelected
                            ? OriginalPositions[text] + new Vector2(IndentAmount, 0f)
                            : OriginalPositions[text],
                        IndentAnimationSpeed
                    );
                }
            }

            base.Update();
        }
    }
}
