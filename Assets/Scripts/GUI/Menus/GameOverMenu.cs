﻿// Copyright (c) 2019 Destin Hebner
using System;
using UnityEngine;
using Worldline.Level;
using Worldline.Visuals;
using Worldline.GameState;
using Worldline.Utilities;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Worldline.GUI.Menus
{
    public class GameOverMenu : TextIndentionMenu
    {
        public static GameOverMenu Main { get; private set; }
        public Text GameOverText;
        public Text ContinueText;
        public Text BackToTitleText;
        public string GameScene;
        public string TitleScene;

        protected override void Awake()
        {
            if (Main == null)
            {
                Main = this;
                gameObject.SetActive(false);
            }

            if (Environment.UserName.Contains("jerma") ||
            Environment.UserName.Contains("Jerma") ||
            Environment.UserName.Contains("jerm") ||
            Environment.UserName.Contains("Jerm") ||
            Environment.UserName.Contains("jeremy") ||
            Environment.UserName.Contains("Jeremy") ||
            Random.value < 0.0001f)
            {
                GameOverText.text = SerializedStrings.GetString("GameOverMenu/Menu", "GameOverJ");
            }
            else
            {
                GameOverText.text = SerializedStrings.GetString("GameOverMenu/Menu", "GameOver");
            }

            ContinueText.text = SerializedStrings.GetString("GameOverMenu/Menu", "Continue");
            BackToTitleText.text = SerializedStrings.GetString("GameOverMenu/Menu", "BackToMenu");
            base.Awake();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            Cursor.visible = true;
        }

        protected virtual void OnDisable()
        {
            Cursor.visible = false;
        }

        protected override void OnBackOut()
        {
            // ...
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            BackgroundMusicOperator.Main.MainAudioSource.volume = 1f;
            switch (itemClicked.Item1)
            {
                case 0:
                    if (SaveFile.SaveFileExists(SaveFile.LoadedSaveFile.Name))
                    {
                        SaveFile.LoadedSaveFile = SaveFile.LoadSaveFile(SaveFile.LoadedSaveFile.Name);
                        SaveFile.LoadedSaveFile.LoadGame();
                    }
                    else
                    {
                        SaveFile.LoadedSaveFile = new SaveFile
                        (
                            SaveFile.LoadedSaveFile.Name,
                            SaveFile.LoadedSaveFile.Difficulty
                        );

                        SceneTransitionOperator.Main.LoadScene(SaveFile.DefaultScene);
                    }

                    break;
                case 1:
                    SceneTransitionOperator.Main.LoadScene(TitleScene);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
 