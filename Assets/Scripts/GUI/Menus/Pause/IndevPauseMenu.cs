﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using Worldline.Visuals;
using Worldline.Utilities;
using UnityEngine.UI;

namespace Worldline.GUI.Menus.Pause
{
    public class IndevPauseMenu : ScaleMenu
    {
        public static IndevPauseMenu Main { get; set; }
        public Text Header;
        public Text InfoText;
        public Text Continue;
        public Text Exit;

        protected override void Awake()
        {
            base.Awake();
            if (Main == null)
            {
                Main = this;
                gameObject.SetActive(false);
            }

            Header.text = SerializedStrings.GetString("General/PauseMenu", "Header");
            InfoText.text = SerializedStrings.GetString("General/PauseMenu", "Info");
            Continue.text = SerializedStrings.GetString("General/PauseMenu", "Continue");
            Exit.text = SerializedStrings.GetString("General/PauseMenu", "Exit");
        }

        protected override void OnBackOut()
        {
            // set unpaused in main canvas controller
        }

        protected override void OnSelected((int, int) itemClicked)
        {
            if (itemClicked.Item1 == 0)
            {
                gameObject.SetActive(false);
                LevelController.Main.SetPaused(false, this);
            }
            else
            {
                LevelController.Main.SetPaused(false, this);
                enabled = false;
                SceneTransitionOperator.Main.LoadScene("TitleScreen");
            }
        }
    }
}
