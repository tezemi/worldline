﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.Player;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using Worldline.Level;
using Worldline.Visuals;

namespace Worldline.GUI.Menus.Pause
{
    [RequireComponent(typeof(AudioSource))]
    public class PauseMenu : MonoBehaviour
    {
        public static PauseMenu Main;
        public GameObject InGameOptionsMenu;
        public Text[] Options;
        public Color UnselectedColor = Color.white;
        public Color SelectedColor = Color.yellow;
        public AudioClip Open;
        public AudioClip Close;
        public AudioClip Up;
        public AudioClip Down;
        public GameObject[] Children;
        public bool Out;
        public int Index { get; protected set; }
        private bool _applicationQuitting;

        private bool _inPauseMenu;
        public bool InPauseMenu
        {
            get
            {
                return _inPauseMenu;
            }
            set
            {
                if (!MainPlayerOperator.MainPlayerComponent.Dead && InGameOptionsMenu.gameObject.activeSelf || SceneTransitionOperator.Main.Transitioning) return;

                if (LevelController.Main.SetPaused(value, this))
                {
                    _inPauseMenu = value;
                }
            }
        }

        private void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        private void OnEnable()
        {
            for (int i = 0; i < Options.Length; i++)
            {
                //Options[i].text = CultureString.Get("UI/PauseMenu", Application.persistentDataPath, Application.dataPath)[i];
            }
        }

        private void OnDisable()
        {
            if (!_applicationQuitting)
            {
                SoundEffect.PlaySound(Close);
            }
        }

        private void OnApplicationQuit()
        {
            _applicationQuitting = true;
        }

        private void Update()
        {
            if (InputManager.GetButtonDown("pause"))
            {
                InPauseMenu = true;
            }

            if (!_inPauseMenu)
            {
                return;
            }

            if (InputManager.GetButtonDown("pause"))
            {
                InPauseMenu = false;
            }

            if (!Out)
            {
                if (InputManager.GetButtonDown("up") && Index > 0)
                {
                    Index--;
                }

                if (InputManager.GetButtonDown("down") && Index < Options.Length - 1)
                {
                    Index++;
                }
            }

            if (!Out && InputManager.GetButtonDown("right"))
            {                
                switch (Index)
                {
                    case 0:
                        Out = true;
                        //StatusMenu.Main.In = true;
                        //StatusMenu.Main.CurrentNode = StatusMenu.Main.Nodes[0];

                        break;
                    case 1:
                        Out = true;
                        //SystemMenu.Main.In = true;

                        break;
                    case 2:
                        //if (WeaponOperator.Main.Weapons.Count > 0)
                        //{
                        //    Out = true;
                        //    WeaponsMenu.Main.In = true;
                        //}
                        //if (PlayerOperator.MainPlayerOperator.Inventory.Count > 0)
                        //{
                        //    Out = true;
                        //    InventoryMenu.Main.In = true;
                        //}

                        break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                        //Out = true;
                        //SystemMenu.Main.In = true;                        
                        break;
                }
            } 

            int i = 0;
            foreach (Text t in Options)
            {
                if (Out)
                {
                    t.color = UnselectedColor;
                    continue;
                }

                t.color = i == Index ? SelectedColor : UnselectedColor;
                i++;
            }

            for (int j = 0; j < Children.Length; j++)
            {
                Children[j].SetActive(j == Index);
            }
        }
    }
}
