﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using Worldline.Level;
using Worldline.Utilities;

namespace Worldline.GUI.Menus.Pause
{
    public class StatusMenuNode : MonoBehaviour
    {
        public bool DirectionExitsMenu;
        public int DescriptionIndex;
        public StatusMenuNode Up;
        public StatusMenuNode Down;
        public StatusMenuNode Left;
        public StatusMenuNode Right;
        public Direction ExitDirection;
    }
}
