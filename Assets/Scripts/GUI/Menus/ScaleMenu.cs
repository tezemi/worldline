﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    public abstract class ScaleMenu : TextMenu
    {
        public float ScaleSpeed = 0.02f;
        public Vector3 SelectedScale;
        public Vector3 UnselectedScale;

        protected override void Update()
        {
            base.Update();
            for (var x = 0; x < Options.GetLength(0); x++)
            {
                for (var y = 0; y < Options.GetLength(1); y++)
                {
                    Text text = Options[x, y];
                    if (text == null) continue;

                    bool isSelected = x == CurrentlySelectedOption.Item1 && y == CurrentlySelectedOption.Item2;
                    text.rectTransform.localScale = Vector3.MoveTowards
                    (
                        text.rectTransform.localScale,
                        isSelected ? SelectedScale : UnselectedScale,
                        ScaleSpeed
                    );
                }
            }
        }
    }
}
