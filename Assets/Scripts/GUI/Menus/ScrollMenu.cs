﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Menus
{
    /// <summary>
    /// This menu can be used to configure a list of options that scroll as
    /// you move through them, holding a certain amount of options inside a
    /// layout, adding new options, and removing old options from the layout
    /// as the list is scrolled through.
    /// </summary>
    public abstract class ScrollMenu : SelectorMenu
    {
        public int ScrollSize;
        public HorizontalOrVerticalLayoutGroup LayoutGroup;
        public Graphic[] GraphicsInsideLayout { get; protected set; }

        protected override void Awake()
        {
            base.Awake();
            GraphicsInsideLayout = new Graphic[ScrollSize];
        }

        protected override void Update()
        {
            base.Update();
            if ((InputManager.GetButtonDown("down") ||
            InputManager.GetButtonDown("right")) &&
            !GraphicsInsideLayout.Contains(SelectedElement))
            {
                ScrollDown();
            }
            else if ((InputManager.GetButtonDown("up") ||
            InputManager.GetButtonDown("left")) &&
            !GraphicsInsideLayout.Contains(SelectedElement))
            {
                ScrollUp();
            }
        }

        /// <summary>
        /// Scrolls the list of elements down, and should get called when the
        /// player scrolls down in the menu, and they are at the bottom of the
        /// visible elements, but there are most options.
        /// </summary>
        private void ScrollDown()
        {
            GraphicsInsideLayout[0].transform.SetParent(GetComponentInParent<Canvas>().transform);
            GraphicsInsideLayout[0].rectTransform.anchoredPosition = new Vector2(-2000f, -2000f);
            for (int i = 0; i < GraphicsInsideLayout.Length - 1; i++)
            {
                GraphicsInsideLayout[i] = GraphicsInsideLayout[i + 1];
            }

            SelectedElement.transform.SetParent(LayoutGroup.transform);
            GraphicsInsideLayout[GraphicsInsideLayout.Length - 1] = SelectedElement;
            AdjustSiblings();
        }

        /// <summary>
        /// Scrolls the list of elements up, and should get called when the
        /// player scrolls up in the menu, and they are at the top of the
        /// visible elements, but there are most options.
        /// </summary>
        private void ScrollUp()
        {
            GraphicsInsideLayout[GraphicsInsideLayout.Length - 1].transform.SetParent(GetComponentInParent<Canvas>().transform);
            GraphicsInsideLayout[GraphicsInsideLayout.Length - 1].rectTransform.anchoredPosition = new Vector2(-2000f, -2000f);
            for (int i = GraphicsInsideLayout.Length - 1; i > 0; i--)
            {
                GraphicsInsideLayout[i] = GraphicsInsideLayout[i - 1];
            }

            SelectedElement.transform.SetParent(LayoutGroup.transform);
            GraphicsInsideLayout[0] = SelectedElement;
            AdjustSiblings();
        }

        /// <summary>
        /// Goes through all of the elements inside the currently visible ones
        /// and ensures they are in order.
        /// </summary>
        private void AdjustSiblings()
        {
            for (var i = 0; i < GraphicsInsideLayout.Length; i++)
            {
                Graphic graphic = GraphicsInsideLayout[i];
                graphic.transform.SetSiblingIndex(i);
            }

            Selector.transform.SetSiblingIndex(int.MaxValue); // lol
        }

        /// <inheritdoc />
        /// <summary>
        /// Performs base behavior of adding a new options to the list of
        /// possible elements, but also updates the visible elements inside
        /// of the LayoutGroup and the GraphicsInsideLayout array.
        /// </summary>
        public override void AddOption(Graphic option, bool addRow)
        {
            base.AddOption(option, addRow);
            foreach (Graphic graphic in SerializedOptions)
            {
                graphic.transform.SetParent(null);
            }

            for (var i = 0; i < SerializedOptions.Length; i++)
            {
                Graphic graphic = SerializedOptions[i];
                if (i < ScrollSize)
                {
                    graphic.transform.SetParent(LayoutGroup.transform);
                    GraphicsInsideLayout[i] = graphic;
                }
                else
                {
                    graphic.transform.SetParent(GetComponentInParent<Canvas>().transform);
                    graphic.rectTransform.anchoredPosition = new Vector2(-2000f, -2000f);
                }

                graphic.rectTransform.localScale = new Vector3(1f, 1f, 1f);
            }
        }
    }
}
