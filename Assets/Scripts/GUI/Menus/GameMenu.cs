﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Worldline.Utilities;

namespace Worldline.GUI.Menus
{
    /// <summary>
    /// This is the abstract base class for all in-game menus. This class
    /// is very simple, and so a majority of menus that are used in the
    /// game should be able to inherit from this class, or another child
    /// of this class that fleshes out more behavior. You can use the
    /// CurrentlySelectedOption tuple to find the element within Options
    /// that the player is currently selecting. This class will handle
    /// input and out of bounds checking.
    /// </summary>
    /// <typeparam name="T">The type of UI element that composes the menu.</typeparam>
    public abstract class GameMenu<T> : MonoBehaviour where T : UIBehaviour
    {
        public bool DisableMouseInput;
        public bool DisableKeyboardInput;
        public int Rows;
        public int Columns;
        public int DefaultRow;
        public int DefaultColumn;
        public AudioClip SelectedOptionSound;
        public AudioClip ChangeOptionSound;
        public AudioClip OnBackOutSound;
        public AudioClip UnselectableOptionSound;
        [HideInInspector]
        public T[] SerializedOptions;
        public T[,] Options { get; set; }
        /// <summary>
        /// When true, that means that the user recently moved the mouse, and
        /// menu selection should be based on mouse position rather than other
        /// input.
        /// </summary>
        public bool MouseInControl { get; set; }
        /// <summary>
        /// Leaves most of the menu functionality enabled but disables direct 
        /// user input. Good for simulating navigation with code.
        /// </summary>
        public bool DisableInput { get; set; }
        public (int, int) LastSelectedOption { get; protected set; }
        public const float MouseMovementAmount = 1f;
        private Vector2 _previousMousePosition;
        private SoundEffect _currentlyPlayingSoundEffect;
        private (int, int) _currentlySelectedOption;

        public (int, int) CurrentlySelectedOption
        {
            get
            {
                return _currentlySelectedOption;
            }
            set
            {
                if (ChangeOptionSound != null && (_currentlySelectedOption.Item1 != value.Item1 || _currentlySelectedOption.Item2 != value.Item2))
                {
                    PlayMenuSound(ChangeOptionSound);
                }

                _currentlySelectedOption = value;
            }
        }

        public bool ShouldUseMouse
        {
            get
            {
                if (Vector2.Distance(InputManager.mousePosition, _previousMousePosition) > MouseMovementAmount)
                {
                    MouseInControl = true;
                    _previousMousePosition = InputManager.mousePosition;

                    return true;
                }

                return false;
            }
        }

        public bool ShouldUseKeyboard
        {
            get
            {
                if (InputManager.anyKeyDown)
                {
                    MouseInControl = false;
                    _previousMousePosition = InputManager.mousePosition;

                    return true;
                }

                return false;
            }
        }

        public T SelectedElement
        {
            get
            {
                if (Options.Length > 0)
                {
                    return Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2];
                }

                return null;
            }
        } 

        protected virtual void Awake()
        {
            UpdateOptions();
        }

        protected virtual void OnEnable()
        {
            CurrentlySelectedOption = (DefaultRow, DefaultColumn);
        }

        protected virtual void Update()
        {
            if (DisableInput) return;

            if (InputManager.GetButtonDown("pause"))
            {
                if (OnBackOutSound != null)
                {
                    PlayMenuSound(OnBackOutSound);
                }

                OnBackOut();
                return;
            }
            
            if ((MouseInControl || ShouldUseMouse) && !DisableMouseInput)
            {
                MouseUpdate();
            }

            if ((!MouseInControl || ShouldUseKeyboard) && !DisableKeyboardInput)
            {
                KeyboardUpdate();
            }

            if (InputManager.GetButtonDown("primary"))
            {
                OnSelected(CurrentlySelectedOption);
            }
        }

        protected void MouseUpdate()
        {
            for (var i = 0; i < Options.GetLength(0); i++)
            {
                for (var j = 0; j < Options.GetLength(1); j++)
                {
                    UIBehaviour behaviour = Options[i, j];
                    if (behaviour is Graphic g)
                    {
                        if (g.MouseInside())
                        {
                            CurrentlySelectedOption = (i, j);
                            break;
                        }
                    }
                }
            }
        }

        protected void KeyboardUpdate()
        {
            if (InputManager.GetButtonDown("up") &&
            CurrentlySelectedOption.Item1 - 1 >= 0 &&
            Options[CurrentlySelectedOption.Item1 - 1, CurrentlySelectedOption.Item2] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1 - 1, CurrentlySelectedOption.Item2);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (InputManager.GetButtonDown("down") &&
            Options.GetLength(0) > CurrentlySelectedOption.Item1 + 1 &&
            Options[CurrentlySelectedOption.Item1 + 1, CurrentlySelectedOption.Item2] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1 + 1, CurrentlySelectedOption.Item2);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (InputManager.GetButtonDown("left") &&
            CurrentlySelectedOption.Item2 - 1 >= 0 &&
            Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 - 1] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 - 1);
                LastSelectedOption = CurrentlySelectedOption;
            }

            if (InputManager.GetButtonDown("right") &&
            Options.GetLength(1) > CurrentlySelectedOption.Item2 + 1 &&
            Options[CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 + 1] != null)
            {
                CurrentlySelectedOption = (CurrentlySelectedOption.Item1, CurrentlySelectedOption.Item2 + 1);
                LastSelectedOption = CurrentlySelectedOption;
            }
        }

        /// <summary>
        /// Internally all options are stored in a 1d T array, although on the
        /// editor's GUI this is laid our 2-dimensionally. The fields in the
        /// 2d layout are turned into a 1d array that gets serialized. This
        /// will convert that 1d serialized array into a 2d array of those options.
        /// </summary>
        protected void UpdateOptions()
        {
            Options = new T[Rows, Columns];
            for (var x = 0; x < Options.GetLength(0); x++)
            {
                for (var y = 0; y < Options.GetLength(1); y++)
                {
                    Options[x, y] = SerializedOptions[x * Columns + y];
                }
            }
        }

        protected void OptionSelected()
        {
            if (SelectedOptionSound != null)
            {
                PlayMenuSound(SelectedOptionSound);
            }
        }

        protected void OptionDisabled()
        {
            if (UnselectableOptionSound)
            {
                PlayMenuSound(UnselectableOptionSound);
            }
        }

        public void PlayMenuSound(AudioClip clip)
        {
            if (_currentlyPlayingSoundEffect != null && _currentlyPlayingSoundEffect.IsPlaying)
            {
                _currentlyPlayingSoundEffect.Stop();
            }

            _currentlyPlayingSoundEffect = SoundEffect.PlaySound(clip, false);
        }

        /// <summary>
        /// Adds a new option to both SerializedOptions and Options.
        /// </summary>
        /// <param name="option">The instance of the new options to add.</param>
        /// <param name="addRow">If true, this new options will be added to the rows of
        /// options, otherwise it will be added to the columns.</param>
        public virtual void AddOption(T option, bool addRow)
        {
            List<T> options = SerializedOptions.ToList();
            options.Add(option);
            SerializedOptions = options.ToArray();
            if (addRow)
            {
                Rows++;
            }
            else
            {
                Columns++;
            }

            UpdateOptions();
        }
        
        /// <summary>
        /// Called when the player presses the back button. This should be
        /// overriden by the final inheriting menu in order to implement
        /// behavior.
        /// </summary>
        protected abstract void OnBackOut();

        /// <summary>
        /// Called when the player presses the forward button. This should be
        /// overriden by the final inheriting menu in order to implement
        /// behavior.
        /// </summary>
        protected abstract void OnSelected((int, int) itemClicked);
    }
}
