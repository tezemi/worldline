﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.TimeEngine;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using ColorMap = System.Collections.Generic.Dictionary<UnityEngine.Color, UnityEngine.Color>;

namespace Worldline.GUI
{
    public class UiTearifier : MonoBehaviour
    {
        public static int ColorIndex { get; set; }
        public static List<Sprite> GeneratedSprites { get; } = new List<Sprite>();
        public static Dictionary<Sprite, Sprite[]> TearifiedSprites { get; } = new Dictionary<Sprite, Sprite[]>();
        public static Dictionary<int, ColorMap> ColorMaps { get; } = new Dictionary<int, ColorMap>();
        public int FrameCount { get; protected set; }
        public int FramesBeforeChange { get; protected set; } = 175;
        public int FramesChanging { get; protected set; }
        public bool Changing { get; protected set; }
        public Sprite NormalSprite { get; protected set; }
        public const int AmountOfVariants = 6;
        public const int FrameCountMin = 120;
        public const int FrameCoutnMax = 220;
        public const int ChangeFrameMin = 15;
        public const int ChangeFrameMax = 30;
        public const float TimeEffectShadeAmount = 0.45f;
        protected Image Image { get; set; }
        private short _realSpriteId;

        private TimeEffect _timeEffect = TimeEffect.None;
        public TimeEffect TimeEffect
        {
            get
            {
                return _timeEffect;
            }
            set
            {
                if (value == _timeEffect) return;
                _realSpriteId = (short)(((short)value + 1) << 8 | (_spriteId + 1));

                _timeEffect = value;
            }
        }

        private short _spriteId;
        public short SpriteId
        {
            get
            {
                return _spriteId;
            }
            protected set
            {
                if (value == _spriteId) return;
                _realSpriteId = (short)(((short)_timeEffect + 1) << 8 | (value + 1));

                _spriteId = value;
            }
        }

        protected virtual void Awake()
        {
            _realSpriteId = (short)(((short)_timeEffect + 1) << 8 | (_spriteId + 1));
            Image = GetComponent<Image>();
        }

        protected virtual void LateUpdate()
        {
            if (!GeneratedSprites.Contains(Image.sprite))
            {
                NormalSprite = Image.sprite;
                UpdateSprite();
            }

            if (!Changing && FrameCount >= FramesBeforeChange)
            {
                Changing = true;
                FrameCount = 0;
                FramesBeforeChange = Random.Range(FrameCountMin, FrameCoutnMax);
                FramesChanging = Random.Range(ChangeFrameMin, ChangeFrameMax);
            }

            if (Changing && FrameCount < FramesChanging)
            {
                SpriteId = (short)Random.Range(0, AmountOfVariants);
                UpdateSprite();
            }

            if (Changing && FrameCount >= FramesChanging)
            {
                Changing = false;
                FrameCount = 0;
            }

            FrameCount++;
        }

        public void UpdateSprite()
        {
            if (NormalSprite == null) return;

            if (!TearifiedSprites.ContainsKey(NormalSprite) || TearifiedSprites[NormalSprite][_realSpriteId] == null)
            {
                CreateSprite();
            }

            Image.sprite = TearifiedSprites[NormalSprite][_realSpriteId];
        }

        public void ForceUpdateSprite()
        {
            NormalSprite = Image.sprite;
            UpdateSprite();
        }

        public void CreateSprite()
        {
            if (!TearifiedSprites.ContainsKey(NormalSprite))
            {
                TearifiedSprites.Add(NormalSprite, new Sprite[short.MaxValue]);
            }

            if (!ColorMaps.ContainsKey(_realSpriteId))
            {
                ColorMaps.Add(_realSpriteId, new ColorMap());
            }

            Sprite drawnSprite = DrawSprite(NormalSprite, ColorMaps[_realSpriteId], TimeEffect);
            GeneratedSprites.Add(drawnSprite);
            TearifiedSprites[NormalSprite][_realSpriteId] = drawnSprite;
        }

        public void Flicker()
        {
            Changing = false;
            FrameCount = FramesBeforeChange;
        }

        public static Sprite DrawSprite(Sprite original, ColorMap colorMap, TimeEffect timeEffect)
        {
            try
            {
                int width = (int)original.rect.width;
                int height = (int)original.rect.height;
                Texture2D texture = new Texture2D(width, height);
                Color[] pixels = original.texture.GetPixels
                (
                    (int)original.textureRect.x - 1 > 0 ? (int)original.textureRect.x - 1 : 0,
                    (int)original.textureRect.y - 1 > 0 ? (int)original.textureRect.y - 1 : 0,
                    width,
                    height
                );

                for (int i = 0; i < pixels.Length; i++)
                {
                    if (pixels[i].a > 0.1f)
                    {
                        if (!colorMap.ContainsKey(pixels[i]))
                        {
                            if (timeEffect != TimeEffect.None && timeEffect != TimeEffect.FastForward)
                            {
                                Color normColor = TimeOperator.EffectColors[timeEffect];
                                Color newColor;
                                switch (timeEffect)
                                {
                                    case TimeEffect.Pause:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b
                                        );

                                        break;
                                    case TimeEffect.Slow:
                                        newColor = new Color
                                        (
                                            normColor.r,
                                            normColor.g,
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Rewind:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g,
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Record:
                                        newColor = new Color
                                        (
                                            normColor.r,
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    case TimeEffect.Reset:
                                        newColor = new Color
                                        (
                                            normColor.r + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.g + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount),
                                            normColor.b + Random.Range(-TimeEffectShadeAmount, TimeEffectShadeAmount)
                                        );

                                        break;
                                    default:
                                        throw new ArgumentOutOfRangeException(nameof(timeEffect), timeEffect, null);
                                }

                                colorMap.Add(pixels[i], newColor);
                            }
                            else
                            {
                                colorMap.Add(pixels[i], TimeOperator.EffectColors[(TimeEffect)ColorIndex]);
                                if (++ColorIndex > 4)
                                {
                                    ColorIndex = 0;
                                }
                            }
                        }

                        pixels[i] = colorMap[pixels[i]];
                    }
                }

                texture.filterMode = FilterMode.Point;
                texture.SetPixels(pixels);
                texture.Apply();

                Bounds bounds = original.bounds;
                var pivotX = -bounds.center.x / bounds.extents.x / 2 + 0.5f;
                var pivotY = -bounds.center.y / bounds.extents.y / 2 + 0.5f;

                return Sprite.Create
                (
                    texture,
                    new Rect(0, 0, width, height),
                    new Vector2(pivotX, pivotY),
                    32
                );
            }
            catch (UnityException e)
            {
                Debug.LogWarning(e);
                return original;
            }
        }
    }
}
