﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI
{
    public class UIAnimationOperator : MonoBehaviour
    {
        public float Delay;
        public int CurrentFrame { get; protected set; }
        [CanBeNull]
        protected TimeOperator TimeOperator { get; set; }
        protected Image Image { get; set; }

        [SerializeField]
        private Sprite[] _sprites;
        public Sprite[] Sprites
        {
            get => _sprites;
            set
            {
                CurrentFrame = 0;
                _sprites = value;
            }
        }

        protected virtual void Awake()
        {
            Image = GetComponent<Image>();
            if (GetComponent<TimeOperator>() != null)
            {
                TimeOperator = GetComponent<TimeOperator>();
            }
            else if (GetComponentInParent<TimeOperator>() != null)
            {
                TimeOperator = GetComponentInParent<TimeOperator>();
            }
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(DoAnimation(), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        /// <summary>
        /// Forces the animation to go to the next frame.
        /// </summary>
        public virtual void ForceUpdate()
        {
            Image.sprite = _sprites[0];
        }

        /// <summary>
        /// Gets called after every animation frame. Does nothing by default, 
        /// but can be overridden to add behavior.
        /// </summary>
        protected virtual void Step() { }

        /// <summary>
        /// This is the main animation loop. Moves to the next sprite every tick.
        /// </summary>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> DoAnimation()
        {
            start:
            if (TimeOperator != null)
            {
                yield return this.WaitForFixedSecondsScaled(Delay);
            }
            else
            {
                yield return Timing.WaitForSeconds(Delay);
            }

            if (CurrentFrame > _sprites.Length - 1)
            {
                CurrentFrame = 0;
            }

            if (_sprites.Length <= 0)
            {
                yield break;
            }

            Image.sprite = _sprites.Length > 1 ? _sprites[CurrentFrame++] : _sprites[0];
            Step();
            if (isActiveAndEnabled)
            {
                goto start;
            }
        }
    }
}
