// Copyright (c) 2019 Destin Hebner
using Worldline.Level;
using Worldline.GUI.HUD;
using Worldline.GUI.Menus;
using Worldline.Utilities;
using Worldline.GUI.Menus.Pause;
using TeamUtility.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI
{
    /// <summary>
    /// Provides an interface to some elements of the canvas, and initializes 
    /// some elements of the canvas in case they haven't been initialized yet.
    /// Also handles pausing/un-pausing the game, and opening up the pause menu.
    /// </summary>
    public class MainCanvasController : MonoBehaviour
    {
        public static MainCanvasController Main;
        public HUDController HUDController;
        public IndevPauseMenu PauseMenu;
        public DebugUtils DebugUtils;
        public DialogController DialogController;
        public GameOverMenu GameOverMenu;
        public Map Map;
        public Text VersionWarning;
        public Image CanvasFader;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
                this.DestroyOnSceneLoad();

                HUDController.gameObject.SetActive(true);
                PauseMenu.gameObject.SetActive(true);
                DebugUtils.gameObject.SetActive(true);
                DialogController.gameObject.SetActive(true);
                GameOverMenu.gameObject.SetActive(true);
                Map.gameObject.SetActive(true);

                GameOverMenu.gameObject.SetActive(false);
                PauseMenu.gameObject.SetActive(false);
            }
            else
            {
                Destroy(gameObject);
            }

            VersionWarning.text = SerializedStrings.GetString("General/VeryGeneric", "VersionWarning");
        }

        protected virtual void Update()
        {
            if (InputManager.GetButtonDown("pause") && !LevelController.Main.Paused && ScriptedSequences.ScriptedSequence.ActiveScriptedSequences.Count <= 0)
            {
                IndevPauseMenu.Main.gameObject.SetActive(true);
                LevelController.Main.SetPaused(true, IndevPauseMenu.Main);
            }
            else if (InputManager.GetButtonDown("pause") && IndevPauseMenu.Main.gameObject.activeSelf)
            {
                IndevPauseMenu.Main.gameObject.SetActive(false);
                LevelController.Main.SetPaused(false, IndevPauseMenu.Main);
            }
        }
    }
}
