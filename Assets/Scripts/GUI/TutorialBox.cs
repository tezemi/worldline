﻿// Copyright (c) 2019 Destin Hebner
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI
{
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(TimeOperator))]    
    public class TutorialBox : MonoBehaviour
    {
        public Text PressText;
        [CanBeNull]
        public Text InputText;
        public Text ResultText;
        public Image Icon;
        public float TimeSinceStartup { get; set; }
        public Vector3 HoverPoint { get; set; }
        public bool StartFloating { get; protected set; }
        public const float HoverSpeed = 0.04f;
        public const float HoverIntensity = 0.2f;
        protected Recorder Recorder { get; set; }
        protected TimeOperator TimeOperator { get; set; }

        protected virtual void Awake()
        {
            Recorder = GetComponent<Recorder>();
            TimeOperator = GetComponent<TimeOperator>();
        }

        protected virtual void FixedUpdate()
        {
            if (TimeOperator.ShouldHold) return;

            Vector3 sinOffset = new Vector3(0f, Mathf.Sin(TimeSinceStartup) * HoverIntensity, 0f);
            sinOffset = TimeOperator.GetScaledValue(sinOffset);
            if (!StartFloating && Vector2.Distance(transform.position, HoverPoint + sinOffset) < HoverIntensity)
            {
                StartFloating = true;
            }

            transform.position = Vector3.Lerp
            (
                transform.position,
                HoverPoint + sinOffset,
                HoverSpeed
            );

            TimeSinceStartup += Time.fixedDeltaTime;
        }

        public class TutorialBoxSnapshot : VisualSnapshot
        {
            public float TimeSinceStartupValue { get; set; }

            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                TimeSinceStartupValue = gameObject.GetComponent<TutorialBox>().TimeSinceStartup;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<TutorialBox>().TimeSinceStartup = TimeSinceStartupValue;
            }
        }
    }
}
