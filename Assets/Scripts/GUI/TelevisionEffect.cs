﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using UnityEngine;

namespace Worldline.GUI
{
    public class TelevisionEffect : MonoBehaviour
    {
        public bool StartOff = true;
        public bool RunOnEnabled = true;
        public bool DisableXScaling;
        public TelevisionEffect WaitFor;
        public bool Started { get; protected set; }
        public bool Finished { get; protected set; }
        public const float Speed = 0.1f;
        public const float VerticalScale = 20f;
        public const float HorizontalRequirement = 0.8f;
        public const float CompletionMargin = 0.01f;
        protected Vector3 NormalScale { get; set; }

        protected virtual void Awake()
        {
            NormalScale = transform.localScale;
        }

        protected virtual void OnEnable()
        {
            if (StartOff)
            {
                Started = false;
                Finished = false;
                transform.localScale = new Vector3(0f, transform.localScale.y / VerticalScale, 1f);
            }

            if (RunOnEnabled && WaitFor == null)
            {
                Finished = false;
                Started = true;
                StartCoroutine(RunOpen());
            }
        }

        protected virtual void Update()
        {
            if (WaitFor != null && !Started && !Finished && WaitFor.Finished)
            {
                Finished = false;
                Started = true;
                StartCoroutine(RunOpen());
            }
        }

        protected virtual IEnumerator RunOpen()
        {
            start:
            yield return new WaitForFixedUpdate();
            if (DisableXScaling)
            {
                transform.localScale = new Vector3(NormalScale.x, transform.localScale.y, 1f);
            }

            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, NormalScale.x, Speed), transform.localScale.y, 1f);
            if (transform.localScale.x >= HorizontalRequirement)
            {
                transform.localScale = new Vector3(transform.localScale.x, Mathf.Lerp(transform.localScale.y, NormalScale.y, Speed), 1f);
            }

            if (transform.localScale.x >= NormalScale.x - CompletionMargin)
            {
                transform.localScale = new Vector3(NormalScale.x, transform.localScale.y, 1f);
            }

            if (transform.localScale.y >= NormalScale.y - CompletionMargin)
            {
                transform.localScale = new Vector3(transform.localScale.x, NormalScale.y, 1f);
            }

            if (transform.localScale == NormalScale)
            {
                Finished = true;
            }

            if (isActiveAndEnabled && !Finished) goto start;
        }
        
        protected virtual IEnumerator RunClose()
        {
            yield return new WaitForFixedUpdate();
            transform.localScale = new Vector3(Mathf.Lerp(transform.localScale.x, 0f, Speed), transform.localScale.y, 1f);
            if (transform.localScale.x <= HorizontalRequirement)
            {
                transform.localScale = new Vector3(transform.localScale.x, Mathf.Lerp(transform.localScale.y, 0f, Speed), 1f);
            }

            if (transform.localScale.x <= CompletionMargin)
            {
                transform.localScale = new Vector3(0f, transform.localScale.y, 1f);
            }

            if (transform.localScale.y <= CompletionMargin)
            {
                transform.localScale = new Vector3(transform.localScale.x, 0f, 1f);
            }

            if (transform.localScale.x <= 0f)
            {
                Finished = true;
            }

            if (isActiveAndEnabled && !Finished) StartCoroutine(RunClose());
        }

        public virtual void Open()
        {
            if (!Finished) return;

            Finished = false;
            Started = true;
            StartCoroutine(RunOpen());
        }

        public virtual void Close()
        {
            if (!Finished) return;

            Finished = false;
            Started = true;
            StartCoroutine(RunClose());
        }
    }
}
