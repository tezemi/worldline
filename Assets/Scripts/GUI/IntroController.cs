﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using System.Collections.Generic;
using MEC;
using Worldline.Utilities;
using Worldline.GUI.Printing;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace Worldline.GUI
{
    /// <summary>
    /// This is responsible for the game's intro animation and text.
    /// TODO: Move coroutines over to MEC.
    /// </summary>
    public class IntroController : MonoBehaviour
    {
        public Text SkipInfoText;
        public Text DialogText;
        public SimpleTextPrinter DialogPrinter;
        public AudioSource BgmAudioSource;
        public Image Panel1A;   // Erystitilian with its arms outstretched
        public Image Panel2A;   // Pargin 
        public Image Panel2B;   // Vextoran
        public Image Panel2C;   // Symbari
        public Image Panel2D;   // Tektin
        public Image Panel2E;   // Glothian
        public Image Panel2F;   // Caustari 
        public Image Panel3A;   // Pause 
        public Image Panel3B;   // Slow
        public Image Panel3C;   // Rewind
        public Image Panel3D;   // Fast-Forward
        public Image Panel3E;   // Record
        public Image Panel3F;   // Reset
        public Image Panel4A;   // Tear 1
        public Image Panel4B;   // Tear 2
        public Image Panel4C;   // Tear 3
        public Image Panel5A;   // Wynne
        public Image Panel5AFull;
        public Image Panel5B;   // Koth
        public Image Panel5B2;  // Pause
        public Image Panel5C;   // Poggle
        public Image Panel5C2;  // Slow
        public Image Panel5D;   // Walani
        public Image Panel5D2;  // Rewind
        public Image Panel5E;   // Lycus
        public Image Panel5E2;  // Fast-Forward
        public Image Panel5F;   // Leo
        public Image Panel5F2;  // Record
        public Image Panel5G;   // Arielle
        public Image Panel5G2;  // Reset
        public Image BlackOverlay;
        public string[] Dialog { get; set; }
        public bool FadingToTitle { get; protected set; }
        public const int CharactersPerLine = 36;
        public const float TextDelay = 0.05f;
        public const float DelayBetweenEachPanel = 3f;
        public const float FadeInDelay = 0.4f;
        public const float FadeInAmount = 0.1f;
        public readonly Color FadeStartColor = new Color(1f, 1f, 1f, 0.1f);
        public readonly Color FadeEndColor = new Color(1f, 1f, 1f, 0.9f);
        
        protected virtual void Awake()
        {
            DialogText.text = string.Empty;
        }

        protected virtual void OnEnable()
        {
            SkipInfoText.text = SerializedStrings.GetString("MainMenu/Intro", "IntroTip");
            Dialog = new[]
            {
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro0"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro1"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro2"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro3"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro4"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro5"), CharactersPerLine),
                AdvancedText.InsertLineBreaks(SerializedStrings.GetString("MainMenu/Intro", "Intro6"), CharactersPerLine),
            };

            StartCoroutine(IntroEvent());
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            if (Input.anyKeyDown && !Input.GetKeyDown(KeyCode.Mouse0) && !Input.GetKeyDown(KeyCode.Mouse1) && !Input.GetKeyDown(KeyCode.Mouse2) && !Input.GetKeyDown(KeyCode.Mouse3) && !Input.GetKeyDown(KeyCode.Mouse4) && !Input.GetKeyDown(KeyCode.Mouse5) && !Input.GetKeyDown(KeyCode.Mouse6))
            {
                if (!FadingToTitle)
                {
                    StartCoroutine(FadeToTitle());
                }
            }
        }

        protected virtual IEnumerator IntroEvent()
        {
            // Panel 1
            #region Panel1
            StartCoroutine(FadeMusicIn(0.02f, 0.01f));
            Panel1A.color = FadeStartColor;
            StartCoroutine(Fade(Panel1A, FadeInDelay, 0.1f));
            PrintLine(Dialog[0]);
            yield return new WaitForSeconds(1.5f);
            StartCoroutine(BlinkOut(SkipInfoText, 0.05f, 0.1f));
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 2
            #region Panel2
            PrintLine(Dialog[1]);
            const float timeBetweenFades = 0.6f;
            Panel2A.color = FadeStartColor;
            StartCoroutine(Fade(Panel2A, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2B.color = FadeStartColor;
            StartCoroutine(Fade(Panel2B, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2C.color = FadeStartColor;
            StartCoroutine(Fade(Panel2C, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2D.color = FadeStartColor;
            StartCoroutine(Fade(Panel2D, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2E.color = FadeStartColor;
            StartCoroutine(Fade(Panel2E, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2F.color = FadeStartColor;
            StartCoroutine(Fade(Panel2F, FadeInDelay, FadeInAmount));
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 3
            #region Panel3
            PrintLine(Dialog[2]);
            Panel1A.color = FadeEndColor;
            StartCoroutine(Fade(Panel1A, FadeInDelay, -FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2A.color = FadeEndColor;
            StartCoroutine(Fade(Panel2A, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3A.gameObject, 0.25f));
            Panel3A.color = FadeStartColor;
            StartCoroutine(Fade(Panel3A, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2B.color = FadeEndColor;
            StartCoroutine(Fade(Panel2B, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3B.gameObject, 0.15f));
            Panel3B.color = FadeStartColor;
            StartCoroutine(Fade(Panel3B, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2C.color = FadeEndColor;
            StartCoroutine(Fade(Panel2C, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3C.gameObject, 0.22f));
            Panel3C.color = FadeStartColor;
            StartCoroutine(Fade(Panel3C, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2D.color = FadeEndColor;
            StartCoroutine(Fade(Panel2D, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3D.gameObject, 0.31f));
            Panel3D.color = FadeStartColor;
            StartCoroutine(Fade(Panel3D, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2E.color = FadeEndColor;
            StartCoroutine(Fade(Panel2E, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3E.gameObject, 0.25f));
            Panel3E.color = FadeStartColor;
            StartCoroutine(Fade(Panel3E, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            Panel2F.color = FadeEndColor;
            StartCoroutine(Fade(Panel2F, FadeInDelay, -FadeInAmount));
            StartCoroutine(Bop(Panel3F.gameObject, 0.275f));
            Panel3F.color = FadeStartColor;
            StartCoroutine(Fade(Panel3F, FadeInDelay, FadeInAmount));
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 4
            #region Panel4
            PrintLine(Dialog[3]);
            const float lerpSpeed = 0.02f;
            StartCoroutine(SetEssSpritesToCorruptedVersion(0.5f));
            Timing.RunCoroutine(TearMovementIn(Panel4A, lerpSpeed), Segment.Update, GetInstanceID().ToString());
            Timing.RunCoroutine(TearMovementIn(Panel4B, lerpSpeed), Segment.Update, GetInstanceID().ToString());
            Timing.RunCoroutine(TearMovementIn(Panel4C, lerpSpeed), Segment.Update, GetInstanceID().ToString());
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 5 
            #region Panel5
            PrintLine(Dialog[4]);

            // Tears MOVE Out
            const float speed = 3f;
            Timing.RunCoroutine(TearMovementOut(Panel4A, speed), Segment.Update, GetInstanceID().ToString());
            Timing.RunCoroutine(TearMovementOut(Panel4B, speed), Segment.Update, GetInstanceID().ToString());
            Timing.RunCoroutine(TearMovementOut(Panel4C, speed), Segment.Update, GetInstanceID().ToString());

            yield return new WaitForSeconds(DelayBetweenEachPanel);

            // Agents Fade In
            Panel5A.color = FadeStartColor;
            StartCoroutine(Fade(Panel5A, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5B.color = FadeStartColor;
            StartCoroutine(Fade(Panel5B, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5C.color = FadeStartColor;
            StartCoroutine(Fade(Panel5C, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5D.color = FadeStartColor;
            StartCoroutine(Fade(Panel5D, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5E.color = FadeStartColor;
            StartCoroutine(Fade(Panel5E, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5F.color = FadeStartColor;
            StartCoroutine(Fade(Panel5F, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5G.color = FadeStartColor;
            StartCoroutine(Fade(Panel5G, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 6
            #region Panel6
            PrintLine(Dialog[5]);
            Panel5B2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5B2, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5C2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5C2, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5D2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5D2, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5E2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5E2, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5F2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5F2, FadeInDelay, FadeInAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            Panel5G2.color = FadeStartColor;
            StartCoroutine(Fade(Panel5G2, FadeInDelay, FadeInAmount));
            yield return new WaitUntil(() => !DialogPrinter.Printing);
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            // Panel 7
            #region Panel7
            const float blinkDelay = 0.05f;
            const float blinkAmount = 0.1f;
            PrintLine(Dialog[6]);
            yield return new WaitForSeconds(DelayBetweenEachPanel);

            StartCoroutine(BlinkOut(Panel5B, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.6f);
            StartCoroutine(BlinkOut(Panel5B2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            StartCoroutine(BlinkOut(Panel5C, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.5f);
            StartCoroutine(BlinkOut(Panel5C2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            StartCoroutine(BlinkOut(Panel5D, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.4f);
            StartCoroutine(BlinkOut(Panel5D2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            //StartCoroutine(BlinkOut(Panel5A, blinkDelay, blinkAmount));

            StartCoroutine(BlinkOut(Panel5E, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.3f);
            StartCoroutine(BlinkOut(Panel5E2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            StartCoroutine(BlinkOut(Panel5F, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.2f);
            StartCoroutine(BlinkOut(Panel5F2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);

            StartCoroutine(BlinkOut(Panel5G, blinkDelay, blinkAmount));
            yield return new WaitForSeconds(0.1f);
            StartCoroutine(BlinkOut(Panel5G2, blinkDelay, blinkAmount));
            yield return new WaitForSecondsRealtime(timeBetweenFades);
            StartCoroutine(Fade(Panel5AFull, FadeInDelay, FadeInAmount));
            yield return new WaitForSeconds(DelayBetweenEachPanel);
            #endregion

            
            StartCoroutine(FadeToTitle());
        }

        private IEnumerator FadeToTitle()
        {
            FadingToTitle = true;
            BlackOverlay.color = new Color(0f, 0f, 0f, 0.1f);
            StartCoroutine(Fade(BlackOverlay, FadeInDelay, FadeInAmount));
            StartCoroutine(FadeMusic(0.05f, -0.01f));
            yield return new WaitUntil(() => BlackOverlay.color.a >= 1 && BgmAudioSource.volume <= 0);
            yield return new WaitForSeconds(1f);
            SceneManager.LoadScene("TitleScreen");
        }

        private IEnumerator Fade(Image image, float delay, float amount)
        {
            start:
            yield return new WaitForSecondsRealtime(delay);
            image.color += new Color(0f, 0f, 0f, amount);
            if (image.color.a > 0 && image.color.a < 1)
            {
                goto start;
            }
        }

        private IEnumerator BlinkOut(Image image, float delay, float amount)
        {
            bool blinkdown = true;

            start:
            yield return new WaitForSecondsRealtime(delay);
            if (blinkdown)
            {
                image.color -= new Color(amount, amount, amount, amount);
            }
            else
            {
                image.color += new Color(amount / 2f, amount / 2f, amount / 2f, amount / 2f);
            }
            
            if (image.color.a > 0f)
            {
                blinkdown = !blinkdown;
                goto start;
            }
        }

        private IEnumerator BlinkOut(Text text, float delay, float amount)
        {
            bool blinkdown = true;

            start:
            yield return new WaitForSecondsRealtime(delay);
            if (blinkdown)
            {
                text.color -= new Color(amount, amount, amount, amount);
            }
            else
            {
                text.color += new Color(amount / 2f, amount / 2f, amount / 2f, amount / 2f);
            }

            if (text.color.a > 0f)
            {
                blinkdown = !blinkdown;
                goto start;
            }
        }

        private IEnumerator SetEssSpritesToCorruptedVersion(float delay)
        {
            const float blinkDelay = 0.05f;
            const float blinkAmount = 0.2f;
            //Panel3A.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3A.sprite, SpriteCorruptionType.InvertRandomSectors);
            //Panel3A.SetNativeSize();
            yield return new WaitForSeconds(0.05f);
            //Panel3A.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3A.sprite, SpriteCorruptionType.Blur);
            //Panel3A.SetNativeSize();
            yield return new WaitForSeconds(0.05f);
            //Panel3A.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3A.sprite, SpriteCorruptionType.InvertRandomSectors);
            //Panel3A.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            StartCoroutine(BlinkOut(Panel3A, blinkDelay, blinkAmount));
            //Panel3B.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3B.sprite, SpriteCorruptionType.SwapSplit);
            //Panel3B.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            StartCoroutine(BlinkOut(Panel3B, blinkDelay, blinkAmount));
            //Panel3A.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3A.sprite, SpriteCorruptionType.PalletSwap);
            //Panel3C.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3C.sprite, SpriteCorruptionType.Blur);
            //Panel3A.SetNativeSize();
            //Panel3C.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            StartCoroutine(BlinkOut(Panel3C, blinkDelay, blinkAmount));
            //Panel3D.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3D.sprite, SpriteCorruptionType.RemoveRows);
            //Panel3D.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            StartCoroutine(BlinkOut(Panel3D, blinkDelay, blinkAmount));
            //Panel3E.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3E.sprite, SpriteCorruptionType.Blur);
            //Panel3E.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            StartCoroutine(BlinkOut(Panel3E, blinkDelay, blinkAmount));
            //Panel3A.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3A.sprite, SpriteCorruptionType.InvertRandomSectors);
            //Panel3D.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3C.sprite, SpriteCorruptionType.InvertRandomSectors);
            //Panel3F.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3F.sprite, SpriteCorruptionType.FewColorSwap);
            //Panel3A.SetNativeSize();
            //Panel3D.SetNativeSize();
            //Panel3F.SetNativeSize();
            yield return new WaitForSecondsRealtime(delay);
            //Panel3F.sprite = WorldSpriteCorrupter.CorruptSprite(Panel3F.sprite, SpriteCorruptionType.Blur);
            //Panel3F.SetNativeSize();
            StartCoroutine(BlinkOut(Panel3F, blinkDelay, blinkAmount));
        }

        private IEnumerator FadeMusic(float delay, float amount)
        {
            start:
            yield return new WaitForSecondsRealtime(delay);
            BgmAudioSource.volume += amount;
            if (BgmAudioSource.volume > 0)
            {
                goto start;
            }
        }

        private IEnumerator FadeMusicIn(float delay, float amount)
        {
            start:
            yield return new WaitForSecondsRealtime(delay);
            BgmAudioSource.volume += amount;
            if (BgmAudioSource.volume < 1)
            {
                goto start;
            }
        }

        private IEnumerator Bop(GameObject bobber, float intensity)
        {
            start:
            yield return new WaitForFixedUpdate();
            bobber.transform.position += new Vector3(0f, Mathf.Sin(Time.realtimeSinceStartup) * intensity * Random.Range(0.25f, 2.75f), 0f);
            goto start;
        }

        private IEnumerator<float> TearMovementIn(Image panel, float speed)
        {
            var pos = panel.rectTransform.anchoredPosition.y - 500f;
            while (panel.rectTransform.anchoredPosition.y > pos + 10f)
            {
                yield return Timing.WaitForOneFrame;
                panel.rectTransform.anchoredPosition = Vector2.Lerp
                (
                    panel.rectTransform.anchoredPosition,
                    new Vector2(panel.rectTransform.anchoredPosition.x, pos),
                    speed
                );
            }

            StartCoroutine(Bop(panel.gameObject, 0.25f));
        }

        private IEnumerator<float> TearMovementOut(Image panel, float speed)
        {
            var pos = panel.rectTransform.anchoredPosition.y + 2000f;
            while (panel.rectTransform.anchoredPosition.y < pos)
            {
                yield return Timing.WaitForOneFrame;
                panel.rectTransform.anchoredPosition = Vector2.MoveTowards
                (
                    panel.rectTransform.anchoredPosition,
                    new Vector2(panel.rectTransform.anchoredPosition.x, pos),
                    speed
                );

                speed += 0.02f;
            }
        }

        private void PrintLine(string dialog)
        {
            DialogText.text = string.Empty;
            DialogPrinter.Print(dialog, TextDelay, true);
        }
    }
}
