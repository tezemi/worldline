// Copyright (c) 2019 Destin Hebner
using System;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI
{
    /// <summary>
    /// Attach to any GameObject with a UI Text and it will create a shadow.
    /// TODO: Sucks ass!
    /// </summary>
    public class TextShadow : MonoBehaviour
    {
        public Color ShadowColor = new Color(0f, 0f, 0f, 1f);
        public Vector3 Offset = new Vector3(1.6f, -0.6f, 0f);
        public Text OriginalText { get; protected set; }
        public Text ShadowText { get; protected set; }
        protected GameObject ShadowObject { get; set; }

        protected virtual void Awake()
        {
            ShadowObject = new GameObject($"Shadow for {gameObject.name}", typeof(Text))
            {
                hideFlags = HideFlags.HideInHierarchy | HideFlags.HideInInspector
            };

            DontDestroyOnLoad(ShadowObject);
            OriginalText = GetComponent<Text>();
            ShadowText = ShadowObject.GetComponent<Text>();
            ShadowObject.transform.SetParent(OriginalText.transform.parent);
            ShadowObject.transform.SetSiblingIndex(OriginalText.transform.GetSiblingIndex());
        }

        protected virtual void Update()
        {
            if (OriginalText.transform.parent != ShadowObject.transform.parent)
            {
                ShadowObject.transform.SetParent(OriginalText.transform.parent);
                ShadowObject.transform.SetSiblingIndex(OriginalText.transform.GetSiblingIndex());
            }

            Draw();
        }

        protected virtual void OnEnable()
        {
            if (ShadowText != null)
            {
                ShadowText.enabled = true;
            }

            Draw();
        }

        protected virtual void OnDisable()
        {
            if (ShadowText != null)
            {
                ShadowText.enabled = false;
            }
        }

        protected virtual void OnDestroy()
        {
            if (ShadowText != null)
            {
                Destroy(ShadowText.gameObject);
            }
        }

        public void Draw()
        {
            if (ShadowObject.transform.position != OriginalText.transform.position + Offset)
            {
                ShadowObject.transform.position = OriginalText.transform.position + Offset;
            }

            if (ShadowObject.transform.localScale != OriginalText.transform.localScale)
            {
                ShadowObject.transform.localScale = OriginalText.transform.localScale;
            }

            if (ShadowText.transform.rotation != OriginalText.transform.rotation)
            {
                ShadowText.transform.rotation = OriginalText.transform.rotation;
            }

            if (ShadowText.enabled != OriginalText.enabled)
            {
                ShadowText.enabled = OriginalText.enabled;
            }

            if (ShadowObject.GetComponent<RectTransform>().sizeDelta != OriginalText.GetComponent<RectTransform>().sizeDelta)
            {
                ShadowObject.GetComponent<RectTransform>().sizeDelta = OriginalText.GetComponent<RectTransform>().sizeDelta;
            }

            if (ShadowText.fontStyle != OriginalText.fontStyle)
            {
                ShadowText.fontStyle = OriginalText.fontStyle;
            }

            if (ShadowText.resizeTextForBestFit != OriginalText.resizeTextForBestFit)
            {
                ShadowText.resizeTextForBestFit = OriginalText.resizeTextForBestFit;
            }

            if (ShadowText.resizeTextMaxSize != OriginalText.resizeTextMaxSize)
            {
                ShadowText.resizeTextMaxSize = OriginalText.resizeTextMaxSize;
            }

            if (ShadowText.resizeTextMinSize != OriginalText.resizeTextMinSize)
            {
                ShadowText.resizeTextMinSize = OriginalText.resizeTextMinSize;
            }

            if (ShadowText.alignment != OriginalText.alignment)
            {
                ShadowText.alignment = OriginalText.alignment;
            }

            if (ShadowText.color != new Color(ShadowColor.r, ShadowColor.g, ShadowColor.b, OriginalText.color.a))
            {
                ShadowText.color = new Color(ShadowColor.r, ShadowColor.g, ShadowColor.b, OriginalText.color.a);
            }

            if (ShadowText.font != OriginalText.font)
            {
                ShadowText.font = OriginalText.font;
            }

            if (ShadowText.fontSize != OriginalText.fontSize)
            {
                ShadowText.fontSize = OriginalText.fontSize;
            }

            if (ShadowText.horizontalOverflow != OriginalText.horizontalOverflow)
            {
                ShadowText.horizontalOverflow = OriginalText.horizontalOverflow;
            }

            if (ShadowText.verticalOverflow != OriginalText.verticalOverflow)
            {
                ShadowText.verticalOverflow = OriginalText.verticalOverflow;
            }

            if (Math.Abs(ShadowText.lineSpacing - OriginalText.lineSpacing) > 0.01f)
            {
                ShadowText.lineSpacing = OriginalText.lineSpacing;
            }

            if (ShadowText.text != OriginalText.text)
            {
                // Remove <color> tags.
                ShadowText.text = OriginalText.text;
                bool badMarkupOpen = false;
                StringBuilder removedMarkup = new StringBuilder();
                for (var i = 0; i < ShadowText.text.Length; i++)
                {
                    try
                    {
                        char c = ShadowText.text[i];
                        if (c == '<' && (ShadowText.text[i + 1] == 'c' || ShadowText.text[i + 2] == 'c'))
                        {
                            badMarkupOpen = true;
                        }

                        if (!badMarkupOpen)
                        {
                            removedMarkup.Append(c);
                        }

                        if (c == '>' && badMarkupOpen)
                        {
                            badMarkupOpen = false;
                        }
                    }
                    catch (IndexOutOfRangeException)
                    {
                        badMarkupOpen = false;
                        removedMarkup = new StringBuilder(ShadowText.text);
                    }
                }

                ShadowText.text = removedMarkup.ToString();
            }
        }
    }
}
