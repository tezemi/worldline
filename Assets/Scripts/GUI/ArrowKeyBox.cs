﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GUI
{
    public class ArrowKeyBox : TutorialBox
    {
        public Sprite KeyUp;
        public Sprite KeyDown;
        public Sprite KeyLeft;
        public Sprite KeyRight;
    }
}
