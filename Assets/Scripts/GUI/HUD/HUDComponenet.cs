﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Player;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public abstract class HUDComponenet : MonoBehaviour
    {
        public List<MaskableGraphic> Elements;
        protected MainPlayerOperator MainPlayerOperator => _mainPlayerOperator ?? (_mainPlayerOperator = MainPlayerOperator.MainPlayerComponent);
        private MainPlayerOperator _mainPlayerOperator;

        protected virtual void OnEnable()
        {
            foreach (MaskableGraphic graphic in Elements)
            {
                graphic.enabled = true;
            }
        }

        protected virtual void OnDisable()
        {
            foreach (MaskableGraphic graphic in Elements)
            {
                graphic.enabled = false;
            }
        }

        protected virtual void Update()
        {
            if (ShouldBeOff())
            {
                foreach (MaskableGraphic graphic in Elements)
                {
                    graphic.enabled = false;
                }

                return;
            }

            foreach (MaskableGraphic graphic in Elements)
            {
                graphic.enabled = true;
            }
        }

        protected abstract bool ShouldBeOff();
    }
}
