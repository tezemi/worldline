﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Utilities;
using Worldline.TimeEngine;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class CrystalElementsController : HUDComponenet
    {
        public static CrystalElementsController Main { get; set; }
        public Text ResetInfo;
        public Image EssenceSelector;
        public RectMask2D[] CrystalMasks;
        public Image[] OpaqueCrystals;
        public bool ShowResetInfo { get; set; }
        public Vector2[] CrystalHighPositions { get; protected set; } = new Vector2[6];
        public Vector2[] CrystalLowPositions { get; protected set; } = new Vector2[6];
        public const float SelectorSpeed = 0.12f;
        public const float CrystalAlphaMin = 0.3f;
        public const float CrystalShakeIntensity = 16f;
        public const float MeterFillSpeed = 0.12f;
        private bool _fadeResetInfoTextIn;
        private Vector2? _resetCrystalPosition;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            for (var i = 0; i < CrystalMasks.Length; i++)
            {
                RectMask2D rectMask = CrystalMasks[i];
                CrystalHighPositions[i] = rectMask.rectTransform.anchoredPosition;
                CrystalLowPositions[i] = CrystalHighPositions[i] - new Vector2(0f, rectMask.rectTransform.sizeDelta.y);
            }

            ResetInfo.text = SerializedStrings.GetString("General/VeryGeneric", "ResetInfo");
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            for (var i = 0; i < CrystalMasks.Length; i++)
            {
                RectMask2D rectMask = CrystalMasks[i];
                int essenceAmount = EssenceOperator.Main.EssenceAmounts[(TimeEffect)i];
                var newPos = new Vector2
                (
                    rectMask.rectTransform.anchoredPosition.x,
                    MathUtilities.GetRange
                    (
                        EssenceOperator.Main.CanActivateEssence((TimeEffect)i) ? essenceAmount : 0f,
                        0f,
                        EssenceOperator.Main.MaxEssences[(TimeEffect)i],
                        CrystalHighPositions[i].y,
                        CrystalLowPositions[i].y
                    )
                );

                rectMask.rectTransform.anchoredPosition = Vector2.Lerp(rectMask.rectTransform.anchoredPosition, newPos, MeterFillSpeed);
            }

            Vector3 selectorPosition = DeadPlayer2.Main == null || DeadPlayer2.Main != null && !DeadPlayer2.Main.PlayerCanReset
            ? OpaqueCrystals[EssenceOperator.Main.EssenceSelection].rectTransform.position
            : OpaqueCrystals[5].rectTransform.position;
            EssenceSelector.rectTransform.position = Vector2.Lerp
            (
                EssenceSelector.rectTransform.position,
                selectorPosition,
                SelectorSpeed
            );

            Color selectorColor;
            if (DeadPlayer2.Main != null && DeadPlayer2.Main.PlayerCanReset)
            {
                selectorColor = TimeOperator.EffectColors[TimeEffect.Reset];
            }
            else 
            {
                selectorColor = TimeOperator.EffectColors[(TimeEffect)EssenceOperator.Main.EssenceSelection];
            }

            float selectedAlpha = MathUtilities.GetRange
            (
                EssenceOperator.Main.EssenceAmounts[(TimeEffect)EssenceOperator.Main.EssenceSelection],
                0f,
                EssenceOperator.Main.MaxEssences[(TimeEffect)EssenceOperator.Main.EssenceSelection],
                1f,
                CrystalAlphaMin
            );

            EssenceSelector.color = Vector4.Lerp
            (
                EssenceSelector.color,
                new Color
                (
                    selectorColor.r,
                    selectorColor.g,
                    selectorColor.b,
                    selectedAlpha
                ),
                SelectorSpeed
            );

            if (ShowResetInfo && _fadeResetInfoTextIn)
            {
                ResetInfo.color = Vector4.MoveTowards(ResetInfo.color, Color.white, 0.12f);
                if (ResetInfo.color.a >= 1f)
                {
                    _fadeResetInfoTextIn = false;
                }
            }
            else
            {
                ResetInfo.color = Vector4.MoveTowards(ResetInfo.color, Color.clear, 0.12f);
                if (ResetInfo.color.a < 0.1f)
                {
                    _fadeResetInfoTextIn = true;
                }
            }
        }

        protected override bool ShouldBeOff()
        {
            return (MainPlayerOperator == null || !MainPlayerOperator.isActiveAndEnabled) &&
            (EssenceOperator.Main == null || DeadPlayer2.Main ==  null || DeadPlayer2.Main.GameOverMenuOpen);
        }
    }
}
