﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;
using Worldline.Utilities;
using Worldline.Corruption;

namespace Worldline.GUI.HUD
{
    public class CorruptionElementsController : HUDComponenet
    {
        public static CorruptionElementsController Main { get; set; }
        public RectMask2D MeterMask;
        public Vector2 MaskHighPosition { get; protected set; }
        public Vector2 MaskLowPosition { get; protected set; }
        public const float MeterSpeed = 0.12f;
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            MaskHighPosition = MeterMask.rectTransform.anchoredPosition;
            MaskLowPosition = MeterMask.rectTransform.anchoredPosition - new Vector2(0f, MeterMask.rectTransform.sizeDelta.y);
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            MeterMask.rectTransform.anchoredPosition = Vector2.Lerp
            (
                MeterMask.rectTransform.anchoredPosition,
                new Vector2(MeterMask.rectTransform.anchoredPosition.x, MathUtilities.GetRange(Corrupter.Intensity, 0f, 1f, MaskHighPosition.y, MaskLowPosition.y)),
                MeterSpeed
            );
        }

        protected override bool ShouldBeOff()
        {
            return true;
        }
    }
}
