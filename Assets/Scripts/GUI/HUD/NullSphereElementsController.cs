﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.Player.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class NullSphereElementsController : HUDComponenet
    {
        public static NullSphereElementsController Main { get; set; }
        public RectMask2D MeterMask;
        public Vector2 MeterMaskLowPosition { get; protected set; }
        public Vector2 MeterMaskHighPosition { get; protected set; }
        public const float MeterSpeed = 0.12f;
        public const float NoChargeAlpha = 0.5f;
        public const float FullChargeAlpha = 1f;
        protected PlayerWeaponOperator MainPlayerWeaponOperator => _mainPlayerWeaponOperator ?? (_mainPlayerWeaponOperator = PlayerWeaponOperator.Main);
        private PlayerWeaponOperator _mainPlayerWeaponOperator;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            MeterMaskHighPosition = MeterMask.rectTransform.anchoredPosition;
            MeterMaskLowPosition = MeterMaskHighPosition - new Vector2(0f, MeterMask.rectTransform.sizeDelta.y);
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            NullSphereLauncher nullSphereLauncher = (NullSphereLauncher)MainPlayerWeaponOperator.CurrentWeapon;
            float charge = nullSphereLauncher.Charge;
            Vector2 maskPos = new Vector2
            (
                MeterMask.rectTransform.anchoredPosition.x,
                MathUtilities.GetRange
                (
                    charge,
                    0f,
                    NullSphereLauncher.MaxCharge,
                    MeterMaskHighPosition.y,
                    MeterMaskLowPosition.y
                )
            );

            MeterMask.rectTransform.anchoredPosition = Vector2.Lerp(MeterMask.rectTransform.anchoredPosition, maskPos, MeterSpeed);
            foreach (MaskableGraphic graphic in Elements)
            {
                graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, charge >= NullSphereLauncher.MaxCharge ? FullChargeAlpha : NoChargeAlpha);
            }
        }

        protected override bool ShouldBeOff()
        {
            return MainPlayerWeaponOperator == null || !MainPlayerWeaponOperator.isActiveAndEnabled ||
            !(MainPlayerWeaponOperator.CurrentWeapon is NullSphereLauncher);
        }
    }
}
