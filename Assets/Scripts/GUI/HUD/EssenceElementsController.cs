﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class EssenceElementsController : HUDComponenet
    {
        public static EssenceElementsController Main { get; set; }
        public Text Text;
        public bool TextFadingIn { get; protected set; } = true;
        public string Pause { get; private set; }
        public string Slow { get; private set; }
        public string Rewind { get; private set; }
        public string FastForward { get; private set; }
        public string Record { get; private set; }
        public string Reset { get; private set; }
        public const float TextFadeSpeed = 0.06f;
        public const float TextFadedAlpha = 0.38f;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            Pause = SerializedStrings.GetString("General/VeryGeneric", "Pause");
            Slow = SerializedStrings.GetString("General/VeryGeneric", "Slow");
            Rewind = SerializedStrings.GetString("General/VeryGeneric", "Rewind");
            FastForward = SerializedStrings.GetString("General/VeryGeneric", "FastForward");
            Record = SerializedStrings.GetString("General/VeryGeneric", "Record");
            Reset = SerializedStrings.GetString("General/VeryGeneric", "Reset");
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff())
            {
                Text.color = Color.clear;
                return;
            }

            if (!EssenceOperator.Main.AnyEffectIsActive)
            {
                Text.color = Vector4.MoveTowards(Text.color, Color.clear, TextFadeSpeed);
            }
            else
            {
                switch (EssenceOperator.Main.CurrentEffectActive)
                {
                    case TimeEngine.TimeEffect.Pause:
                        Text.text = Pause;
                        break;
                    case TimeEngine.TimeEffect.Slow:
                        Text.text = Slow;
                        break;
                    case TimeEngine.TimeEffect.Rewind:
                        Text.text = Rewind;
                        break;
                    case TimeEngine.TimeEffect.FastForward:
                        Text.text = FastForward;
                        break;
                    case TimeEngine.TimeEffect.Record:
                        Text.text = Record;
                        break;
                    case TimeEngine.TimeEffect.Reset:
                        Text.text = Reset;
                        break;
                    case TimeEngine.TimeEffect.None:
                        break;
                }

                if (TextFadingIn)
                {
                    Text.color = Vector4.MoveTowards(Text.color, Color.white, TextFadeSpeed);
                    if (Text.color.a >= 1f)
                    {
                        TextFadingIn = false;
                    }
                }
                else
                {
                    Text.color = Vector4.MoveTowards(Text.color, Color.clear, TextFadeSpeed);
                    if (Text.color.a <= TextFadedAlpha)
                    {
                        TextFadingIn = true;
                    }
                }
            }
        }

        protected override bool ShouldBeOff()
        {
            return (MainPlayerOperator == null || !MainPlayerOperator.isActiveAndEnabled) &&
            (EssenceOperator.Main == null || DeadPlayer.Main == null || DeadPlayer.Main.GameOver);
        }
    }
}
