// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class HUDController : MonoBehaviour
    {
        public static HUDController Main { get; set; }
        public GameObject CorruptionElements;
        public HUDComponenet[] HUDComponenets;
        private bool _disableHUD;
        
        public bool DisableHUD
        {
            get => _disableHUD;
            set
            {
                foreach (HUDComponenet hudComponenet in HUDComponenets)
                {
                    if (hudComponenet.enabled != !value)
                    {
                        hudComponenet.enabled = !value;
                    }
                }

                _disableHUD = value;
            }
        }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        public void Shake(float intensity)
        {
            foreach (HUDComponenet hudComponenet in HUDComponenets)
            {
                foreach (MaskableGraphic graphic in hudComponenet.Elements)
                {
                    Timing.RunCoroutine(DoShake(graphic), Segment.FixedUpdate, GetInstanceID().ToString());
                }
            }

            IEnumerator<float> DoShake(MaskableGraphic graphic)
            {
                bool shakeRight = true;
                int loopCount = 0;
                const int loopAmount = 15;
                Vector2 startingPosition = graphic.rectTransform.anchoredPosition;
                startLoop:
                yield return Timing.WaitForOneFrame;
                float shakePosition = shakeRight ? intensity : -intensity;
                graphic.rectTransform.anchoredPosition = startingPosition + new Vector2(shakePosition, 0f);
                shakeRight = !shakeRight;
                if (loopCount++ < loopAmount)
                {
                    goto startLoop;
                }

                graphic.rectTransform.anchoredPosition = startingPosition;
            }
        }
    }
}
