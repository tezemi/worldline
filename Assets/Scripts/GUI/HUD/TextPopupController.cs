﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using Worldline.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class TextPopupController : MonoBehaviour
    {
        public static TextPopupController Main;
        public GameObject DamagePopupPrefab;
        public const string DamagePopupPool = "Damage Popup Pool";

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        public GameObject ShowDamagePopup(int damage, Vector3 position)
        {
            GameObject popup;
            if (Pool.Has(DamagePopupPool))
            {
                popup = Pool.Get(DamagePopupPool);
            }
            else
            {
                popup = Instantiate(DamagePopupPrefab);
            }
            
            popup.SetActive(true);
            popup.transform.position = position + new Vector3(Random.Range(-0.1f, 0.1f), Random.Range(-0.1f, 0.1f), -5f);
            popup.GetComponent<RectTransform>().SetParent(transform);
            popup.GetComponent<Text>().color = damage > 0 ? Color.red : Color.green;
            popup.GetComponent<Text>().text = (0 - damage).ToString();
            popup.GetComponent<Text>().StartCoroutine(Animate(popup));

            IEnumerator Animate(GameObject popupObj)
            {
                const int popupTickLength = 60;
                const float speed = 0.05f;
                for (int i = 0; i < popupTickLength; i++)
                {
                    yield return new WaitForSeconds(0.02f);
                    popupObj.transform.position += new Vector3(0f, speed, 0f);
                }

                popupObj.SetActive(false);
                Pool.Add(popupObj, DamagePopupPool, false);
            }

            return popup;
        }
    }
}
