﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Player.Weapons;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class WeaponElementsController : HUDComponenet
    {
        public static WeaponElementsController Main { get; set; }
        public Image LeftWeapon;
        public Image RightWeapon;
        public Image MainWeapon;
        protected PlayerWeaponOperator MainPlayerWeaponOperator => _mainPlayerWeaponOperator ?? (_mainPlayerWeaponOperator = PlayerWeaponOperator.Main);
        private PlayerWeaponOperator _mainPlayerWeaponOperator;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected override void Update()
        {
            if (ShouldBeOff())
            {
                foreach (MaskableGraphic graphic in Elements)
                {
                    graphic.enabled = false;
                }

                return;
            }

            foreach (MaskableGraphic graphic in Elements)
            {
                if (graphic == RightWeapon || graphic == LeftWeapon) continue;

                graphic.enabled = true;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            // If there is one weapon, only enable one element.
            if (MainPlayerWeaponOperator.Weapons.Count == 1)
            {
                MainWeapon.enabled = true;
                RightWeapon.enabled = false;
                LeftWeapon.enabled = false;
            }
            else if (MainPlayerWeaponOperator.Weapons.Count > 1) // Otherwise, enable all of them.
            {
                MainWeapon.enabled = true;
                RightWeapon.enabled = true;
                LeftWeapon.enabled = true;
            }

            // Sets weapon sprites.
            MainWeapon.sprite = MainPlayerWeaponOperator.CurrentWeapon.WeaponSpriteBundle.HudSprite;
            if (MainPlayerWeaponOperator.Weapons.Count > 1)
            {
                int leftWeaponIndex = MainPlayerWeaponOperator.WeaponIndex - 1;
                int rightWeaponIndex = MainPlayerWeaponOperator.WeaponIndex + 1;
                if (leftWeaponIndex < 0)
                {
                    leftWeaponIndex = MainPlayerWeaponOperator.Weapons.Count - 1;
                }

                if (rightWeaponIndex >= MainPlayerWeaponOperator.Weapons.Count)
                {
                    rightWeaponIndex = 0;
                }

                LeftWeapon.sprite = MainPlayerWeaponOperator.Weapons[leftWeaponIndex].WeaponSpriteBundle.HudSprite;
                RightWeapon.sprite = MainPlayerWeaponOperator.Weapons[rightWeaponIndex].WeaponSpriteBundle.HudSprite;
            }
        }

        protected override bool ShouldBeOff()
        {
            return MainPlayerWeaponOperator == null || !MainPlayerWeaponOperator.isActiveAndEnabled ||
            MainPlayerWeaponOperator.CurrentWeapon == null;
        }
    }
}
