﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class HealthElementsController : HUDComponenet
    {
        public static HealthElementsController Main { get; set; }
        public Text HealthValue;
        public const float LowHealthModifier = 2.5f;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            HealthValue.text = MainPlayerOperator.Health < 0 ? "0" : MainPlayerOperator.Health.ToString();
            if (MainPlayerOperator.Health > MainPlayerOperator.MaxHealth / LowHealthModifier)
            {
                HealthValue.color = Color.white;
            }
            else
            {
                HealthValue.color = Color.red;
            }
        }

        protected override bool ShouldBeOff()
        {
            return MainPlayerOperator == null || !MainPlayerOperator.isActiveAndEnabled;
        }
    }
}
