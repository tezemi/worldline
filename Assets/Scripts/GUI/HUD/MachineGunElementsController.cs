﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.Player.Weapons;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class MachineGunElementsController : HUDComponenet
    {
        public static MachineGunElementsController Main { get; set; }
        public RectMask2D OverheatMask;
        public Vector2 OverheatLowMaskPosition { get; protected set; }
        public Vector2 OverheatHighMaskPosition { get; protected set; }
        protected PlayerWeaponOperator MainPlayerWeaponOperator => _mainPlayerWeaponOperator ?? (_mainPlayerWeaponOperator = PlayerWeaponOperator.Main);
        private PlayerWeaponOperator _mainPlayerWeaponOperator;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            OverheatHighMaskPosition = OverheatMask.rectTransform.anchoredPosition;
            OverheatLowMaskPosition = OverheatHighMaskPosition - new Vector2(0f, OverheatMask.rectTransform.sizeDelta.y);
        }

        protected virtual void FixedUpdate()
        {
            if (ShouldBeOff()) return;

            // Sets position of the overheat meter's mask.
            MachineGun machineGun = (MachineGun)MainPlayerWeaponOperator.CurrentWeapon;
            float overheatAmount = machineGun.Overheat;
            OverheatMask.rectTransform.anchoredPosition = new Vector2
            (
                OverheatMask.rectTransform.anchoredPosition.x,
                MathUtilities.GetRange
                (
                    overheatAmount,
                    0f,
                    MachineGun.OverheatCap,
                    OverheatHighMaskPosition.y,
                    OverheatLowMaskPosition.y
                )
            );
        }

        protected override bool ShouldBeOff()
        {
            return MainPlayerWeaponOperator == null || !MainPlayerWeaponOperator.isActiveAndEnabled ||
            !(MainPlayerWeaponOperator.CurrentWeapon is MachineGun);
        }
    }
}
