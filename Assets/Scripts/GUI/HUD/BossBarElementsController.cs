﻿using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.NPCs.Enemies.Tears;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class BossBarElementsController : HUDComponenet
    {
        public int NumberOfNodes = 12;
        public float NodeSpace = 30f;
        public float BarDropForce = 150f;
        public float BarDropTorqueMin = 5f;
        public float BarDropTorqueMax = 25f;
        public GameObject BarNodeTemplate;
        public bool Initialized { get; protected set; }
        public int HealthPerNode { get; protected set; } = -1;
        public List<GameObject> Nodes { get; protected set; } = new List<GameObject>();
        public Dictionary<GameObject, Vector3> NodePositions { get; protected set; } = new Dictionary<GameObject, Vector3>();

        protected virtual void Awake()
        {
            Nodes.Add(BarNodeTemplate);
            NodePositions.Add(BarNodeTemplate, BarNodeTemplate.transform.position);
            var lastNode = BarNodeTemplate;
            for (int i = 0; i < NumberOfNodes; i++)
            {
                var node = Instantiate(BarNodeTemplate);
                node.transform.SetParent(lastNode.transform.parent);
                node.transform.position = lastNode.transform.position + new Vector3(NodeSpace, 0f, 0f);
                NodePositions.Add(node, node.transform.position);
                Nodes.Add(node);
                Elements.Add(node.GetComponent<Image>());
                lastNode = node;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (Boss.Main != null && !Initialized)
            {
                HealthPerNode = Boss.Main.MaxHealth / NumberOfNodes;
                Initialized = true;
            }

            if (Initialized && Boss.Main != null)
            {
                int currentNodeIndex = (int)MathUtilities.GetRange(Boss.Main.Health, 0f, Boss.Main.MaxHealth, NumberOfNodes, 0);
                for (int i = 0; i < Nodes.Count; i++)
                {
                    GameObject node = Nodes[i];
                    if (currentNodeIndex < i && node.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Static && !node.GetComponent<TimeOperator>().ShouldHold)
                    {
                        node.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        node.GetComponent<Rigidbody2D>().AddForce(new Vector2(Random.Range(-BarDropForce, BarDropForce), BarDropForce), ForceMode2D.Impulse);
                        node.GetComponent<Rigidbody2D>().AddTorque(Random.Range(BarDropTorqueMin, BarDropTorqueMax), ForceMode2D.Impulse);
                    }
                    else if (currentNodeIndex >= i && node.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Dynamic)
                    {
                        node.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
                        node.transform.position = NodePositions[node];
                    }
                }
            }
        }

        protected override bool ShouldBeOff()
        {
            return MainPlayerOperator == null || !MainPlayerOperator.isActiveAndEnabled || Boss.Main == null || !Boss.Main.isActiveAndEnabled;
        }
    }
}
