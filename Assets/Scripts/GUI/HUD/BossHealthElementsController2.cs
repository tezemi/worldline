﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Utilities;
using Worldline.NPCs.Enemies.Tears;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.HUD
{
    public class BossHealthElementsController2 : HUDComponenet
    {
        public static BossHealthElementsController2 Main { get; set; }
        public Image BossHealthMeter;
        public RectMask2D MeterMask;
        public Vector2 MeterHighPosition { get; protected set; }
        public Vector2 MeterLowPosition { get; protected set; }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }

            MeterHighPosition = MeterMask.rectTransform.anchoredPosition;
            MeterLowPosition = MeterHighPosition - new Vector2(MeterMask.rectTransform.sizeDelta.x, 0f);
        }

        protected override void Update()
        {
            base.Update();

            if (ShouldBeOff()) return;

            Boss boss = Boss.Main;

            MeterMask.rectTransform.anchoredPosition = new Vector2
            (
                MathUtilities.GetRange(boss.Health, 0, boss.MaxHealth, MeterHighPosition.x, MeterLowPosition.x),
                MeterMask.rectTransform.anchoredPosition.y
            );
        }

        protected override bool ShouldBeOff()
        {
            if (Boss.Main == null || !Boss.Main.isActiveAndEnabled || Boss.Main.Dead || MainPlayerOperator == null || MainPlayerOperator.Dead)
            {
                return true;
            }

            return false;
        }
    }
}
