﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using System.Linq;
using Worldline.Utilities;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI
{
    /// <summary>
    /// This is the controller for a canvas that persists between all scene
    /// loads, and is notably used by the SceneTransitionOperator for handling
    /// effects. If you want, you can put any UI elements in here to ensure
    /// they will persist between scene loads.
    /// </summary>
    public class PersistentCanvasController : MonoBehaviour
    {
        public static PersistentCanvasController Main { get; protected set; }
        public Image Overlay;
        public Text LoadingText;
        public bool ShowLoadingText { get; set; }
        public int LoadingTextIndex { get; protected set; }
        public string[] LoadingStrings { get; protected set; } = new string[4];
        public const float TextFadeSpeed = 0.02f;
        public const float TimeBetweenText = 0.25f;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
                transform.SetParent(null);
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }

            LoadingStrings[0] = SerializedStrings.GetString("General/VeryGeneric", "Loading0");
            LoadingStrings[1] = SerializedStrings.GetString("General/VeryGeneric", "Loading1");
            LoadingStrings[2] = SerializedStrings.GetString("General/VeryGeneric", "Loading2");
            LoadingStrings[3] = SerializedStrings.GetString("General/VeryGeneric", "Loading3");
        }

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(LoadingAnimation(), Segment.FixedUpdate);
        }

        protected virtual void FixedUpdate()
        {
            LoadingText.color = Vector4.MoveTowards(LoadingText.color, new Color(LoadingText.color.r, LoadingText.color.g, LoadingText.color.b, ShowLoadingText ? 1f : 0f), TextFadeSpeed);
        }

        protected IEnumerator<float> LoadingAnimation()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitForSeconds(TimeBetweenText);
                LoadingText.text = LoadingStrings[LoadingTextIndex];
                if (LoadingTextIndex++ >= 3)
                {
                    LoadingTextIndex = 0;
                }
            }
        }
    }
}
