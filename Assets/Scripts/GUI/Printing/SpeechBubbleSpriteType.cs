﻿// Copyright (c) 2019 Destin Hebner
namespace Worldline.GUI.Printing
{
    public enum SpeechBubbleSpriteType
    {
        Normal,
        Thought,
        Watch,
        Info
    }
}
