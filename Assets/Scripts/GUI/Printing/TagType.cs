// Copyright (c) 2019 Destin Hebner
namespace Worldline.GUI.Printing
{
    public enum TagType
    {
        Bold,
        Italics,
        Size,
        Color,
        Material,
        Quad,
        Input,
        Shake,
        ColorSync,
        ColorAsync,
        Waver,
        Delay,
        GameOver
    }
}
