﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// TODO: Possibly deprecated?
    /// </summary>
    public class SimpleTextPrinter : MonoBehaviour
    {
        public bool InsertLineBreaks;
        public int CharactersPerLine;
        public bool Printing { get; protected set; }
        public const float PunctuationDelay = 8f;

        public void Print(string text, float delay, bool pauseOnPunctuation = false)
        {
            if (InsertLineBreaks)
            {
                text = AdvancedText.InsertLineBreaks(text, CharactersPerLine);
            }

            GetComponent<Text>().text = "";
            Printing = true;
            StartCoroutine(DoPrint(0));

            IEnumerator DoPrint(int pos)
            {
                start:
                yield return new WaitForSecondsRealtime(delay * (CheckForPunc(text, pos) ? PunctuationDelay : 1f));
                GetComponent<Text>().text += text[pos];
                if (isActiveAndEnabled && pos < text.Length - 1)
                {
                    pos++;
                    goto start;
                }

                Printing = false;
            }
        }

        private bool CheckForPunc(string text, int pos)
        {
            if (pos > 0 && (text[pos - 1] == '.' || text[pos - 1] == ',' || text[pos - 1] == '!' || text[pos - 1] == '?'))
            {
                return true;
            }

            return false;
        }
    }
}
