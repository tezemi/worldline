﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    public class ColorAsyncEffect : MarkupEffect
    {
        /// <summary>
        /// The delay in seconds before each character will change color.
        /// </summary>
        public float Delay { get; set; }
        /// <summary>
        /// The list of colors to change between.
        /// </summary>
        public List<Color> Colors { get; set; }
        protected Dictionary<Text, Color> OriginalColors { get; set; } = new Dictionary<Text, Color>();
        private CoroutineHandle _changeColorHandle;

        public ColorAsyncEffect(List<Text> characters, float delay, List<Color> colors) : base(characters)
        {
            Delay = delay;
            Colors = colors;
        }

        protected override void OnEnable()
        {
            _changeColorHandle = Timing.RunCoroutine(ChangeColor(), Segment.FixedUpdate);

            IEnumerator<float> ChangeColor()
            {
                int colorIndex = 0;
                while (Enabled)
                {
                    Debug.Log("Changecolor is enabled.");
                    yield return Timing.WaitForSeconds(Delay);
                    foreach (Text t in Characters)
                    {
                        if (!OriginalColors.ContainsKey(t))
                        {
                            OriginalColors.Add(t, t.color);
                        }

                        t.color = Colors[colorIndex++];
                        if (colorIndex >= Colors.Count)
                        {
                            colorIndex = 0;
                        }
                    }
                }
            }
        }

        protected override void OnDisable()
        {
            Timing.KillCoroutines(_changeColorHandle);
            foreach (Text text in Characters)
            {
                text.color = OriginalColors[text];
            }
        }
    }
}
