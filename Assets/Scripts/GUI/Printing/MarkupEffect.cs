﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// Base abstract class for MarkupEffects. MarkupEffects manipulate a 
    /// range of specified characters when the effect is enabled, and (should)
    /// set the characters back to normal when the effect is disabled.
    /// </summary>
    public abstract class MarkupEffect
    {
        /// <summary>
        /// The list of characters that will be affected by this effect.
        /// </summary>
        public List<Text> Characters { get; set; }
        private bool _enabled;

        /// <summary>
        /// Whether or not this effect is enabled. Disable effects when they are 
        /// no longer needed. Simply waiting for the garbage collector to come
        /// clean up the effect is not a good way to stop an effect.
        /// </summary>
        public bool Enabled
        {
            get => _enabled;
            set
            {
                _enabled = value;
                if (value)
                {
                    OnEnable();
                }
                else
                {
                    OnDisable();
                }
            }
        }

        /// <summary>
        /// Creates a new MarkupEffect.
        /// </summary>
        /// <param name="characters">The range of characters to be effected.</param>
        protected MarkupEffect(List<Text> characters)
        {
            Characters = characters;
        }

        /// <summary>
        /// This disables the effect when this object gets cleaned up. This shouldn't
        /// be necessary, but is here in case an effect object gets cleaned up before
        /// the effect gets disabled. 
        /// </summary>
        ~MarkupEffect()
        {
            if (Enabled)
            {
                OnDisable();
            }
        }

        /// <summary>
        /// This gets called when an effect gets enabled.
        /// </summary>
        protected abstract void OnEnable();

        /// <summary>
        /// This gets called when an effect gets disabled.
        /// </summary>
        protected abstract void OnDisable();
    }
}
