// Copyright (c) 2019 Destin Hebner
using System;
using System.Text;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// Used by the TextPrinter component, this represents a markup tag and
    /// its position within a string. You can retrieve the full text, only
    /// the body, the position within a string, the type of tag, whether or
    /// not it is a closing tag, and the tag it is siblings with.
    /// </summary>
    public class MarkupTag
    {
        /// <summary>
        /// The position of this markup tag within the string it was removed 
        /// from.
        /// </summary>
        public int Index { get; set; }
        /// <summary>
        /// The corresponding closing tag or opening tag for this tag. Some tags 
        /// do not have closing tags, such as the input tag, and in that case, this
        /// would be null.
        /// </summary>
        [CanBeNull]
        public MarkupTag OppositeTag { get; set; }
        public bool IsClosingTag { get; protected set; }
        /// <summary>
        /// Just the body of the markup string.
        /// </summary>
        public string Body { get; protected set; }
        public TagType Type { get; protected set; }
        /// <summary>
        /// The array of values, if any exist, found within this tag.
        /// </summary>
        public List<string> Values { get; protected set; }
        private string _text;

        /// <summary>
        /// This is true if the MarkupTag creates some sort of custom effect.
        /// </summary>
        public bool AppliesMarkupEffect
        {
            get
            {
                switch (Type)
                {
                    case TagType.Bold:
                    case TagType.Italics:
                    case TagType.Size:
                    case TagType.Color:
                    case TagType.Material:
                    case TagType.Quad:
                    case TagType.Input:
                    case TagType.Delay:
                        return false;
                    case TagType.Shake:
                    case TagType.ColorSync:
                    case TagType.ColorAsync:
                    case TagType.Waver:
                    case TagType.GameOver:
                        return true;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// This is true if this MarkupTag is a built in Unity tag, such as bold 
        /// or italics.
        /// </summary>
        public bool IsUnityMarkup
        {
            get
            {
                switch (Type)
                {
                    case TagType.Bold:
                    case TagType.Italics:
                    case TagType.Size:
                    case TagType.Color:
                    case TagType.Material:
                    case TagType.Quad:
                        return true;
                    case TagType.Input:
                    case TagType.Shake:
                    case TagType.ColorSync:
                    case TagType.ColorAsync:
                    case TagType.Waver:
                    case TagType.Delay:
                    case TagType.GameOver:
                        return false;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        /// <summary>
        /// This will return true if this tag needs a closing tag in order to 
        /// function properly. For example, bold, italics, and color tags need
        /// closing tags. The input tag does not need a closing tag.
        /// </summary>
        public bool NeedsClosingTag
        {
            get
            {
                switch (Type)
                {
                    case TagType.Bold:
                    case TagType.Italics:
                    case TagType.Size:
                    case TagType.Color:
                    case TagType.Material:
                    case TagType.Quad:
                    case TagType.Shake:
                    case TagType.ColorSync:
                    case TagType.ColorAsync:
                    case TagType.Waver:
                    case TagType.Delay:
                    case TagType.GameOver:
                        return true;
                    case TagType.Input:
                        return false;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

            }
        }

        /// <summary>
        /// The full text of the markup string.
        /// </summary>
        public string Text
        {
            get => _text;
            set
            {
                Body = new StringBuilder(value).Replace("<", "").Replace(">", "").ToString();
                IsClosingTag = value.Contains("/");
                if (Body.Contains("colorasync")) Type = TagType.ColorAsync;
                else if (Body.Contains("colorsync")) Type = TagType.ColorSync;
                else if (Body.Contains("material")) Type = TagType.Material;
                else if (Body.Contains("gameover")) Type = TagType.GameOver;
                else if (Body.Contains("color")) Type = TagType.Color;
                else if (Body.Contains("waver")) Type = TagType.Waver;
                else if (Body.Contains("input")) Type = TagType.Input;
                else if (Body.Contains("shake")) Type = TagType.Shake;
                else if (Body.Contains("delay")) Type = TagType.Delay;
                else if (Body.Contains("size")) Type = TagType.Size;
                else if (Body.Contains("quad")) Type = TagType.Quad;
                else if (Body.Contains("b")) Type = TagType.Bold;
                else if (Body.Contains("i")) Type = TagType.Italics;

                bool gatheringValue = false;
                string valueString = string.Empty;
                Values = new List<string>();
                foreach (char c in value)
                {
                    
                    if (c == '=') // We're about to grab a value;
                    {
                        gatheringValue = true;
                        valueString = string.Empty;
                    }
                    else if ((c == ' ' || c == '>') && gatheringValue)
                    {
                        gatheringValue = false;
                        Values.Add(valueString);
                    }
                    else if (gatheringValue)
                    {
                        valueString += c;
                    }
                }

                _text = value;
            }
        }

        /// <summary>
        /// Creates an effect for the specified range of characters.
        /// </summary>
        /// <param name="characters">The characters to be effected.</param>
        /// <returns>The created effect object.</returns>
        public MarkupEffect CreateEffect(List<Text> characters)
        {
            if (Type == TagType.Shake)
            {
                return CreateShakeEffect(characters);
            }

            if (Type == TagType.ColorAsync)
            {
                return CreateColorAsyncEffect(characters);
            }

            if (Type == TagType.Waver)
            {
                return CreateWaverEffect(characters);
            }

            if (Type == TagType.GameOver)
            {
                return CreateGameOverEffect(characters);
            }

            return null;
        }

        protected ShakeEffect CreateShakeEffect(List<Text> characters)
        {
            const float defaultIntensity = 1f;
            if (Values.Count == 0)
            {
                return new ShakeEffect(characters, defaultIntensity);
            }

            if (Values.Count == 1)
            {
                float intensity = 1f;
                if (float.TryParse(Values[0], out float intensityResult))
                {
                    intensity = intensityResult;
                }

                return new ShakeEffect(characters, intensity);
            }

            float delay = 0.02f;
            float spread = 1f;
            if (float.TryParse(Values[0], out float delayResult))
            {
                delay = delayResult;
            }

            if (float.TryParse(Values[1], out float spreadResult))
            {
                spread = spreadResult;
            }

            return new ShakeEffect(characters, delay, spread);
        }

        protected ColorAsyncEffect CreateColorAsyncEffect(List<Text> characters)
        {
            const float defaultDelay = 0.02f;
            if (Values.Count < 2)
            {
                Debug.LogWarning("A tag <colorasync> is formatted incorrectly", characters[0]);
                return new ColorAsyncEffect(characters, defaultDelay, new List<Color> { Color.white });
            }

            float delay = 0.02f;
            if (float.TryParse(Values[0], out float delayResult))
            {
                delay = delayResult;
            }

            List<Color> colors = new List<Color>();
            for (int i = 1; i < Values.Count; i++)
            {
                if (ColorUtility.TryParseHtmlString(Values[i], out Color color))
                {
                    colors.Add(color);
                }
            }

            return new ColorAsyncEffect(characters, delay, colors);
        }

        protected WaverEffect CreateWaverEffect(List<Text> characters)
        {
            if (Values.Count < 3)
            {
                return new WaverEffect(characters);
            }
            
            float delay = 0.1f;
            if (float.TryParse(Values[0], out float delayResult))
            {
                delay = delayResult;
            }

            float size = 1f;
            if (float.TryParse(Values[1], out float sizeResult))
            {
                size = sizeResult;
            }

            float speed = 0.02f;
            if (float.TryParse(Values[2], out float speedResult))
            {
                speed = speedResult;
            }

            return new WaverEffect(characters, delay, size, speed);
        }

        protected GameOverEffect CreateGameOverEffect(List<Text> characters)
        {
            if (Values.Count < 2)
            {
                return new GameOverEffect(characters);
            }

            float distance = 25f;
            if (float.TryParse(Values[0], out float distanceResult))
            {
                distance = distanceResult;
            }

            float speed = 0.25f;
            if (float.TryParse(Values[1], out float speedResult))
            {
                speed = speedResult;
            }

            return new GameOverEffect(characters, distance, speed);
        }
    }
}
