﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using Worldline.Player;

namespace Worldline.GUI.Printing
{
    public class SpeechBubble : MonoBehaviour
    {
        public Sprite NormalBubble;
        public Sprite ThoughtBubble;
        public Sprite WatchBubble;
        public Sprite InfoBubble;
        public bool CleanUpSelf { get; set; }
        public bool FancyDeath { get; set; } // floats off screen then dies
        public float XOffset { get; set; } = 1.5f;
        public float YOffset { get; set; } = 2f;
        public float TimeDisplayed { get; set; } = 1.5f;
        public float LerpSpeed { get; set; } = 0.08f;
        public SpeechBubbleSpriteType SpriteType { get; set; }
        public GameObject Speaker { get; set; }
        public Color TextColor { get; set; } = Color.white;
        public const float InitTime = 0.5f;
        public SimpleTextPrinter TextPrinter { get; protected set; }
        protected Image Image { get; set; }

        protected virtual void Awake()
        {
            Image = GetComponent<Image>();
            TextPrinter = GetComponentInChildren<SimpleTextPrinter>();
            if (CleanUpSelf)
            {
                Timing.RunCoroutine(AutoDie(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
        }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        protected virtual void Update()
        {
            switch (SpriteType)
            {
                case SpeechBubbleSpriteType.Normal:
                    if (Image.sprite != NormalBubble) Image.sprite = NormalBubble;
                    break;
                case SpeechBubbleSpriteType.Thought:
                    if (Image.sprite != ThoughtBubble) Image.sprite = ThoughtBubble;
                    break;
                case SpeechBubbleSpriteType.Watch:
                    if (Image.sprite != WatchBubble) Image.sprite = WatchBubble;
                    break;
                case SpeechBubbleSpriteType.Info:
                    if (Image.sprite != InfoBubble) Image.sprite = InfoBubble;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            TextPrinter.GetComponent<Text>().color = TextColor;
        }

        protected virtual void FixedUpdate()
        {
            transform.position = Vector3.Lerp
            (
                transform.position, 
                Speaker.transform.position + new Vector3(XOffset, YOffset, 0.15f), 
                LerpSpeed
            );

            transform.position = new Vector3
            (
                transform.position.x, 
                transform.position.y, 
                MainPlayerOperator.MainPlayerObject.transform.position.z + 0.15f
            );
        }

        protected virtual IEnumerator<float> AutoDie()
        {
            yield return Timing.WaitForSeconds(InitTime);
            yield return Timing.WaitUntilDone(new WaitUntil(() => !TextPrinter.Printing));
            yield return Timing.WaitForSeconds(TimeDisplayed);
            if (CleanUpSelf)
            {
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(Die(), Segment.FixedUpdate, GetInstanceID().ToString()));
            }
        }

        public virtual IEnumerator<float> Die()
        {
            if (FancyDeath)
            {
                YOffset = 10f;
                yield return Timing.WaitUntilDone
                (
                    new WaitUntil
                    (
                        () => this == null || 
                        Speaker == null || 
                        Vector2.Distance(Speaker.transform.position, transform.position) > 10f
                    )
                );
            }

            if (this == null) yield break;

            Destroy(gameObject);
        }
    }
}

