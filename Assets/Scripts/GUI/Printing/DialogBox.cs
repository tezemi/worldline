﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    public class DialogBox : MonoBehaviour
    {
        public Image Portrait;
        public Image Background;
        public Text QuestionText;
        public AdvancedText NormalText;
        public AdvancedText PortraitText;
        protected RectTransform RectTransform { get; set; }
        private DialogMode _currentDialogMode = DialogMode.Unset;

        /// <summary>
        /// The TextPrinter of the currently active AdvancedText.
        /// </summary>
        public TextPrinter TextPrinter
        {
            get
            {
                foreach (AdvancedText advancedText in GetComponentsInChildren<AdvancedText>())
                {
                    if (advancedText.isActiveAndEnabled)
                    {
                        return advancedText.TextPrinter;
                    }
                }

                return NormalText.TextPrinter;
            }
        }

        protected virtual void Awake()
        {
            RectTransform = GetComponent<RectTransform>();
            gameObject.SetActive(false);
        }

        protected virtual void OnDisable()
        {
            foreach (Text t in NormalText.Characters)
            {
                t.enabled = false;
            }

            foreach (Text t in PortraitText.Characters)
            {
                t.enabled = false;
            }
        }

        /// <summary>
        /// This sets up this dialog box to print text without a portrait.
        /// </summary>
        public void SetupNormalDialog()
        {
            if (_currentDialogMode == DialogMode.Normal) return;

            Portrait.enabled = false;
            Background.enabled = true;
            QuestionText.enabled = false;
            NormalText.enabled = true;
            PortraitText.enabled = false;
            NormalText.SetMessage(string.Empty);
            PortraitText.SetMessage(string.Empty);
            _currentDialogMode = DialogMode.Normal;
        }

        /// <summary>
        /// This sets up this dialog box to have a portrait.
        /// </summary>
        public void SetupPortraitDialog()
        {
            if (_currentDialogMode == DialogMode.Portrait) return;

            Portrait.enabled = true;
            Background.enabled = true;
            QuestionText.enabled = false;
            NormalText.enabled = false;
            PortraitText.enabled = true;
            NormalText.SetMessage(string.Empty);
            PortraitText.SetMessage(string.Empty);
            _currentDialogMode = DialogMode.Portrait;
        }

        /// <summary>
        /// This sets up dialog to have a text box contaings responses for the 
        /// player to pick.
        /// </summary>
        public void SetupQuestionDialog()
        {
            if (_currentDialogMode == DialogMode.Question) return;

            Portrait.enabled = true;
            Background.enabled = true;
            QuestionText.enabled = true;
            NormalText.enabled = false;
            PortraitText.enabled = false;
            NormalText.SetMessage(string.Empty);
            PortraitText.SetMessage(string.Empty);
            _currentDialogMode = DialogMode.Question;
        }

        /// <summary>
        /// Represents the kinds of dialog that can be displayed.
        /// </summary>
        private enum DialogMode
        {
            /// <summary>
            /// No dialog displayed.
            /// </summary>
            Unset,
            /// <summary>
            /// A normal text box with no portrait.
            /// </summary>
            Normal,
            /// <summary>
            /// A text box with a portrait.
            /// </summary>
            Portrait,
            /// <summary>
            /// A text box with responses for the player.
            /// </summary>
            Question
        }
    }
}
