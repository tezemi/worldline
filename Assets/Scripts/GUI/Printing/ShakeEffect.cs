﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// Causes the specified set of characters to start shaking.
    /// </summary>
    public class ShakeEffect : MarkupEffect
    {
        /// <summary>
        /// The delay in seconds before each character will shake.
        /// </summary>
        public float Delay { get; set; } = 0.02f;
        /// <summary>
        /// The distance from their original position they can move.
        /// </summary>
        public float Spread { get; set; }
        /// <summary>
        /// The positions of each character before they start shaking.
        /// </summary>
        protected Dictionary<Text, Vector2> OriginalPositions { get; set; } = new Dictionary<Text, Vector2>();
        private CoroutineHandle _shakeHandle;

        /// <summary>
        /// Set the spread of each character as they shake.
        /// </summary>
        /// <param name="characters">The characters to shake.</param>
        /// <param name="intensity">The distance each character could shake.</param>
        public ShakeEffect(List<Text> characters, float intensity) : base(characters)
        {
            Spread = intensity;
        }

        /// <summary>
        /// Set the delay and spread of each character as they shake.
        /// </summary>
        /// <param name="characters">The characters to shake.</param>
        /// <param name="delay">The delay in seconds before each shake.</param>
        /// <param name="spread">The distance each character could shake.</param>
        public ShakeEffect(List<Text> characters, float delay, float spread) : base(characters)
        {
            Delay = delay;
            Spread = spread;
        }

        protected override void OnEnable()
        {
            _shakeHandle = Timing.RunCoroutine(Shake(), Segment.FixedUpdate);

            IEnumerator<float> Shake()
            {
                while (Enabled)
                {
                    yield return Timing.WaitForSeconds(Delay);
                    foreach (Text t in Characters)
                    {
                        if (!OriginalPositions.ContainsKey(t))
                        {
                            OriginalPositions.Add(t, t.rectTransform.anchoredPosition);
                        }
                        else
                        {
                            t.rectTransform.anchoredPosition = OriginalPositions[t] +
                            new Vector2
                            (
                                Random.Range(-Spread, Spread),
                                Random.Range(-Spread, Spread)
                            );
                        }
                    }
                }
            }
        }

        protected override void OnDisable()
        {
            Timing.KillCoroutines(_shakeHandle);
            foreach (Text text in Characters)
            {
                if (text == null) continue;

                text.rectTransform.anchoredPosition = OriginalPositions[text];
            }
        }
    }
}
