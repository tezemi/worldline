﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// TODO
    /// </summary>
    public class GameOverEffect : MarkupEffect
    {
        public float Scale { get; set; }
        public float Speed { get; set; }
        protected Dictionary<Text, Vector2> OriginalScales { get; set; } = new Dictionary<Text, Vector2>();
        private CoroutineHandle _moveHandle;

        public GameOverEffect(List<Text> characters, float scale = 1.25f, float speed = 0.25f) : base(characters)
        {
            Scale = scale;
            Speed = speed;
        }

        protected override void OnEnable()
        {
            _moveHandle = Timing.RunCoroutine(Move(), Segment.FixedUpdate);

            IEnumerator<float> Move()
            {
                while (Enabled)
                {
                    yield return Timing.WaitForOneFrame;
                    foreach (Text t in Characters)
                    {
                        if (!OriginalScales.ContainsKey(t))
                        {
                            OriginalScales.Add(t, t.rectTransform.localScale);
                        }

                        
                    }
                }
            }
        }

        protected override void OnDisable()
        {
            Timing.KillCoroutines(_moveHandle);
            foreach (Text text in Characters)
            {

            }
        }
    }
}
