// Copyright (c) 2019 Destin Hebner
using System;

namespace Worldline.GUI
{
    [Obsolete("Use markup instead.")]
    public enum TextEffect
    {
        None,
        Shake,
        Wave,
        Bounce,
        Flux,
        Corrupt,
        GameOver
    }
}
