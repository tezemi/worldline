﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// This is a type of dialog box which can be moved off or on screen 
    /// smoothly using the ExtendBox and RetractBox methods.
    /// </summary>
    public class SlidingDialogBox : DialogBox
    {
        public float TransitionSpeed = 0.08f;
        public Vector2 RetractedOffset;
        public bool Extended { get; protected set; }
        public bool Transitioning { get; protected set; }
        public Vector2 ExtendedPosition { get; protected set; }
        public Vector2 RetractedPosition { get; protected set; }
        public const float LerpBuffer = 0.2f;

        protected override void Awake()
        {
            base.Awake();
            ExtendedPosition = RectTransform.anchoredPosition;
            RetractedPosition = RectTransform.anchoredPosition -= new Vector2(0f, Background.rectTransform.sizeDelta.y) + RetractedOffset;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        /// <summary>
        /// Moves the dialog box either on or off the screen.
        /// </summary>
        /// <param name="onScreen">Where to move the box.</param>
        /// <returns>IEnumerator to be used as MEC coroutine.</returns>
        protected virtual IEnumerator<float> DoTransition(bool onScreen)
        {
            Vector2 moveTo = onScreen ? ExtendedPosition : RetractedPosition;
            start:
            Transitioning = true;
            yield return Timing.WaitForOneFrame;
            RectTransform.anchoredPosition = Vector2.Lerp
            (
                RectTransform.anchoredPosition,
                moveTo,
                TransitionSpeed
            );

            if (Math.Abs(RectTransform.anchoredPosition.y - moveTo.y) > LerpBuffer)
            {
                goto start;
            }
            
            Transitioning = false;
        }

        /// <summary>
        /// Extends the box, putting it on the screen and making it visible.
        /// </summary>
        public void ExtendBox()
        {
            Extended = true;
            Timing.KillCoroutines(GetInstanceID().ToString());
            Timing.RunCoroutine(DoTransition(true), Segment.FixedUpdate, GetInstanceID().ToString());
        }

        /// <summary>
        /// Retracts the box, moving it off the screen.
        /// </summary>
        public void RetractBox()
        {
            Extended = false;
            Timing.KillCoroutines(GetInstanceID().ToString());
            Timing.RunCoroutine(DoTransition(false), Segment.FixedUpdate, GetInstanceID().ToString());
        }
    }
}
