﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GUI.Printing
{
    public class SpeechBubbleController : MonoBehaviour
    {
        public static SpeechBubbleController Main;
        public GameObject SpeechBubblePrefab;

        private void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        public SpeechBubble Say(GameObject speaker, string message, float delay = 0.04f, bool cleanUpSelf = false)
        {
            return Say(speaker, message, speaker.transform.position, delay, cleanUpSelf);
        }

        public void Say(SpeechBubble speechBubble, string message, float delay = 0.04f, bool cleanUpSelf = false)
        {
            speechBubble.GetComponent<SpeechBubble>().CleanUpSelf = cleanUpSelf;
            speechBubble.GetComponent<SpeechBubble>().TextPrinter.Print(message, delay, true);
        }

        public void Say(SpeechBubble speechBubble, string message, Vector3 position, float delay = 0.04f, bool cleanUpSelf = false) // probably don't use this
        {
            speechBubble.transform.position = position + new Vector3(speechBubble.XOffset, speechBubble.YOffset, 0.15f);
            speechBubble.GetComponent<SpeechBubble>().CleanUpSelf = cleanUpSelf;
            speechBubble.GetComponent<SpeechBubble>().TextPrinter.Print(message, delay, true);
        }

        public SpeechBubble Say(GameObject speaker, string message, Vector3 position, float delay = 0.04f, bool cleanUpSelf = false)
        {
            GameObject g = Instantiate(SpeechBubblePrefab);
            g.transform.SetParent(transform);
            g.GetComponent<SpeechBubble>().CleanUpSelf = cleanUpSelf;
            g.GetComponent<SpeechBubble>().Speaker = speaker;
            g.GetComponent<SpeechBubble>().TextPrinter.Print(message, delay, true);
            g.transform.position = position + new Vector3(g.GetComponent<SpeechBubble>().XOffset, g.GetComponent<SpeechBubble>().YOffset, 0.15f);

            return g.GetComponent<SpeechBubble>();
        }
    }
}
