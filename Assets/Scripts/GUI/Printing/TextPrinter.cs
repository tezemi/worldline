// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Worldline.Utilities;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
using Random = UnityEngine.Random;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// When attached to a GameObject alongside an AdvancedText component, 
    /// this will enable the AdvancedText to print strings using a typewriter
    /// effect while still maintaining positioning and markup.
    /// </summary>
    public class TextPrinter : MonoBehaviour
    {
        public bool PunctuationDelay = true;
        public bool AwaitInputForTextPush = true;
        public bool HighlightCharacterNames = true;
        public float Delay = .04f;
        public AudioMixerGroup AudioMixerGroup;
        public AudioClip[] DialogBlips;
        public bool SpeedUpPrinting { get; set; }
        public bool Printing { get; protected set; }
        public bool AwaitingInput { get; protected set; }
        public string MessageBeingPrinted { get; protected set; }
        public const int DialogBlipDelay = 3;
        public const float PunctuationDelayAmount = 6f;
        protected AdvancedText AdvancedText { get; set; }
        private bool _forcePrinting;
        private bool _continuePrinting;
        private int _index;

        protected virtual void Awake()
        {
            AdvancedText = GetComponent<AdvancedText>();
        }

        public virtual void Print(string message)
        {
            Printing = true;
            _forcePrinting = false;
            _continuePrinting = false;
            MessageBeingPrinted = message;
            if (HighlightCharacterNames && Regex.IsMatch(MessageBeingPrinted, "^[A-z]*:"))
            {
                MessageBeingPrinted = $"<color=#FFFFFF>{MessageBeingPrinted}";
                int firstColonIndex = MessageBeingPrinted.IndexOf(":", StringComparison.Ordinal);
                MessageBeingPrinted = MessageBeingPrinted.Insert(firstColonIndex + 1, "</color>");
            }

            AdvancedText.SetMessage(MessageBeingPrinted);
            Timing.KillCoroutines(GetInstanceID().ToString());
            Timing.RunCoroutine(Tick(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> Tick()
            {
                foreach (Text text in AdvancedText.FilledCharacters)
                {
                    text.enabled = false;
                }

                _index = 0;
                while (Printing)
                {
                    Text text = AdvancedText.FilledCharacters[_index];
                    text.enabled = true;
                    if (DialogBlips != null && _index % DialogBlipDelay == 1 && !_forcePrinting)
                    {
                        SoundEffect.PlaySound(DialogBlips[Random.Range(0, DialogBlips.Length)]);
                    }

                    if (!_forcePrinting)
                    {
                        float waitFor = Delay;
                        if (AdvancedText.CharactersWithCustomDelay.ContainsKey(text))
                        {
                            waitFor = AdvancedText.CharactersWithCustomDelay[text];
                        }
                        else if (text.text == "." || text.text == "," || 
                        text.text == "?" || text.text == "!")
                        {
                            waitFor *= PunctuationDelayAmount;
                        }
                        else if (SpeedUpPrinting)
                        {
                            waitFor = 0f;
                        }

                        yield return Timing.WaitForSeconds(waitFor);
                    }

                    if (++_index >= AdvancedText.FilledCharacters.Count)
                    {
                        if (AdvancedText.MessageExceedsBox && AwaitInputForTextPush)
                        {
                            if (!_forcePrinting)
                            {
                                AwaitingInput = true;
                                yield return Timing.WaitForOneFrame;
                                yield return Timing.WaitUntilDone
                                (
                                    new WaitUntil(() => _continuePrinting || _forcePrinting)
                                );
                            }

                            AwaitingInput = false;
                            _continuePrinting = false;
                            yield return Timing.WaitUntilDone
                            (
                                AdvancedText.PushMessage(true),
                                Segment.Update, 
                                GetInstanceID().ToString()
                            );
                        }
                        else
                        {
                            Printing = false;
                            _forcePrinting = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// If the text has just been pushed by an <see cref="AdvancedText"/>, this 
        /// can be called to disable the text on the newly created line, and update
        /// the printing index.
        /// </summary>
        public void DisableUnprintedLine()
        {
            for (int j = AdvancedText.FilledCharacters.Count - 1; j >= 0; j--)
            {
                if (AdvancedText.FilledCharacters[j].text == "\n")
                {
                    _index = j;
                    break;
                }

                AdvancedText.FilledCharacters[j].enabled = false;
            }
        }

        /// <summary>
        /// If AdvancedText.MessageExceedsBox is true, and AwaitInputForTextPush 
        /// is true, and the we are printing is halted, then this will trim the
        /// first line of the message and continue printing.
        /// </summary>
        public virtual void ContinuePrinting()
        {
            if (Printing && AdvancedText.MessageExceedsBox && AwaitInputForTextPush)
            {
                _continuePrinting = true;
            }
        }

        /// <summary>
        /// Instantly finish what is currently being printed.
        /// </summary>
        public virtual void FinishPrinting()
        {
            if (Printing)
            {
                _forcePrinting = true;
            }
        }
    }
}
