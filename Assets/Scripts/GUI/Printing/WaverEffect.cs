﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace Worldline.GUI.Printing
{
    /// <summary>
    /// Causes the specified set of characters to start shaking.
    /// </summary>
    public class WaverEffect : MarkupEffect
    {
        /// <summary>
        /// The delay in seconds before each character starts to waver.
        /// </summary>
        public float Delay { get; set; } = 0.025f;
        /// <summary>
        /// The distance from their original position they can move.
        /// </summary>
        public float Size { get; set; } = 5f;
        /// <summary>
        /// The speed of the wavering effect.
        /// </summary>
        public float Speed { get; set; } = 5f;
        /// <summary>
        /// The positions of each character before they start shaking.
        /// </summary>
        protected Dictionary<Text, Vector2> OriginalPositions { get; set; } = new Dictionary<Text, Vector2>();
        private const string WaverTag = "WaverCharactersTag";

        public WaverEffect(List<Text> characters) : base(characters)
        {
            // ...
        }

        public WaverEffect(List<Text> characters, float delay, float size, float speed) : base(characters)
        {
            Delay = delay;
            Size = size;
            Speed = speed;
        }

        protected override void OnEnable()
        {
            float currentDelay = 0f;
            foreach (Text text in Characters)
            {
                Timing.RunCoroutine(Waver(text, currentDelay), Segment.FixedUpdate, WaverTag);
                currentDelay += Delay;
            }

            IEnumerator<float> Waver(Text character, float delay)
            {
                yield return Timing.WaitForSeconds(delay);
                float angle = 0f;
                while (Enabled)
                {
                    yield return Timing.WaitForOneFrame;
                    if (!OriginalPositions.ContainsKey(character))
                    {
                        OriginalPositions.Add(character, character.rectTransform.anchoredPosition);
                    }
                    else
                    {
                        angle += Speed * Time.deltaTime;
                        Vector2 center = OriginalPositions[character] + new Vector2(Size, -Size);
                        Vector2 offset = new Vector2(Mathf.Sin(angle), Mathf.Cos(angle)) * Size;
                        character.rectTransform.anchoredPosition = center + offset;
                    }
                }
            }
        }

        protected override void OnDisable()
        {
            Timing.KillCoroutines(WaverTag);
            foreach (Text text in Characters)
            {
                if (text == null || !OriginalPositions.ContainsKey(text)) continue;

                text.rectTransform.anchoredPosition = OriginalPositions[text];
            }
        }
    }
}
