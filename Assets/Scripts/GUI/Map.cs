﻿// Copyright (c) 2019 Destin Hebner
using System.Collections;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.ScriptedSequences.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;

namespace Worldline.GUI
{
    public class Map : MonoBehaviour
    {
        public static Map Main;
        public Sprite MapTilesOnly { get; protected set; }
        public Sprite MapWithEvents { get; protected set; }
        public readonly Color DoorColor = new Color(1f, 0.69f, 0.2f, 1f);
        public readonly Color SavePointColor = new Color(0.73f, 0.25f, 0.99f, 1f);
        public readonly Color PlayerColor = Color.green;
        public const float FlashTime = 0.5f;
        protected Image BackgroundImage { get; set; }
        protected Dictionary<Sprite, float> PixelsFound { get; private set; } = new Dictionary<Sprite, float>();
        private Color _pixelPlayerReplaced;
        private Color _backgroundColor;

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
            
            BackgroundImage = GetComponent<Image>();
            gameObject.SetActive(false);
            GenerateMap();
        }

        protected virtual void OnEnable()
        {
            if (MapWithEvents == null)
            {
                return;
            }
            
            AddPlayer();
            StartCoroutine(Flash());
        }

        protected virtual void OnDisable()
        {
            if (MapWithEvents == null || MainPlayerOperator.MainPlayerObject == null)
            {
                return;
            }

            RemovePlayer();
        }

        protected virtual IEnumerator Flash()
        {
            yield return new WaitForSecondsRealtime(FlashTime);
            BackgroundImage.sprite = BackgroundImage.sprite == MapWithEvents ? MapTilesOnly : MapWithEvents;
            if (isActiveAndEnabled) StartCoroutine(Flash());
        }

        public void GenerateMap()
        {
            PixelsFound = new Dictionary<Sprite, float>();
            MapTilesOnly = DrawSprite(true);
            MapWithEvents = DrawSprite(false);
            BackgroundImage.preserveAspect = true;
            BackgroundImage.sprite = MapWithEvents;
        }

        private Sprite DrawSprite(bool tilesOnly)
        {
            Texture2D texture = new Texture2D((int)LevelController.Main.MapWidth, (int)LevelController.Main.MapHeight);
            Color[] background = new Color[texture.width * texture.height];
            for (int i = 0; i < background.Length; i++)
            {
                background[i] = Color.clear;
            }

            texture.SetPixels(background);
            DrawTiles(ref texture, LevelController.Main.PhysicsTilemap, false);
            if (!tilesOnly)
            {
                foreach (ChangeLevel l in FindObjectsOfType<ChangeLevel>())
                {
                    Bounds bounds = l.GetComponent<BoxCollider2D>().bounds;
                    for (int x = (int)LevelController.Main.MapWidthMin; x < (int)LevelController.Main.MapWidthMax; x++)
                    {
                        for (int y = (int)LevelController.Main.MapHeightMin; y < (int)LevelController.Main.MapHeightMax; y++)
                        {
                            if (x >= bounds.min.x && x < bounds.max.x)
                            {
                                if (y >= bounds.min.y && y < bounds.max.y)
                                {
                                    Vector2Int vec = GetPixelsFromWorldPosition(new Vector2(x, y), texture);
                                    texture.SetPixel
                                    (
                                        vec.x,
                                        vec.y,
                                        DoorColor
                                    );
                                }
                            }
                        }
                    }
                }

                foreach (Door d in FindObjectsOfType<Door>())
                {
                    texture.SetPixel
                    (
                        GetPixelsFromWorldPosition(d.transform.position, texture).x,
                        GetPixelsFromWorldPosition(d.transform.position, texture).y,
                        DoorColor
                    );
                }

                foreach (SavePoint s in FindObjectsOfType<SavePoint>())
                {
                    texture.SetPixel
                    (
                        GetPixelsFromWorldPosition(s.transform.position, texture).x,
                        GetPixelsFromWorldPosition(s.transform.position, texture).y,
                        SavePointColor
                    );
                }
            }

            _backgroundColor = AverageColorFromTexture(texture);
            Color[] textureWithNoBackgroundColors = texture.GetPixels();
            for (int i = 0; i < textureWithNoBackgroundColors.Length; i++)
            {
                if (textureWithNoBackgroundColors[i] == Color.clear)
                {
                    textureWithNoBackgroundColors[i] = GetInverse(_backgroundColor);
                }
            }

            _backgroundColor = GetInverse(_backgroundColor);
            texture.SetPixels(textureWithNoBackgroundColors);
            foreach (Tilemap tilemap in LevelController.Main.GetComponentsInChildren<Tilemap>())
            {
                if (tilemap == LevelController.Main.PhysicsTilemap) continue;

                DrawTiles(ref texture, tilemap, true);
            }
            
            texture.filterMode = FilterMode.Point;
            texture.Apply();

            return Sprite.Create
            (
                texture,
                new Rect(0, 0, texture.width, texture.height),
                new Vector2(0.5f, 0.5f),
                32
            );
        }

        private void AddPlayer()
        {
            _pixelPlayerReplaced = MapWithEvents.texture.GetPixel
            (
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).x, 
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).y
            );
            
            MapWithEvents.texture.SetPixel
            (
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).x,
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).y,
                PlayerColor
            );

            MapWithEvents.texture.Apply();
        }

        private void RemovePlayer()
        {
            MapWithEvents.texture.SetPixel
            (
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).x, 
                GetPixelsFromWorldPosition(MainPlayerOperator.MainPlayerObject.transform.position, MapWithEvents.texture).y, 
                _pixelPlayerReplaced
            );

            MapWithEvents.texture.Apply();
        }

        private void DrawTiles(ref Texture2D texture, Tilemap tilemap, bool blend)
        {
            Dictionary<Sprite, Color> averageColors = new Dictionary<Sprite, Color>();
            for (int x = LevelController.Main.PhysicsTilemap.cellBounds.xMin; x < LevelController.Main.PhysicsTilemap.cellBounds.xMax; x++)
            {
                for (int y = LevelController.Main.PhysicsTilemap.cellBounds.yMin; y < LevelController.Main.PhysicsTilemap.cellBounds.yMax; y++)
                {
                    TileBase tileBase = tilemap.GetTile(new Vector3Int(x, y, 0));
                    if (tileBase != null && tileBase is Tile tile)
                    {
                        Vector2Int vec = GetPixelsFromWorldPosition(new Vector2(x, y), texture);
                        if (averageColors.ContainsKey(tile.sprite))
                        {
                            texture.SetPixel(vec.x, vec.y, averageColors[tile.sprite]);
                        }
                        else
                        {
                            Color avgColor = blend ? BlendColors(AverageColorFromTexture(tile.sprite), _backgroundColor, 1.5f) : AverageColorFromTexture(tile.sprite);
                            averageColors.Add(tile.sprite, avgColor);
                            texture.SetPixel(vec.x, vec.y, avgColor);
                        }
                    }
                }
            }
        }

        private Vector2Int GetPixelsFromWorldPosition(Vector2 position, Texture texture)
        {
            int x = Mathf.RoundToInt((position.x - LevelController.Main.MapWidthMin) / (LevelController.Main.MapWidthMax - LevelController.Main.MapWidthMin) * (texture.width - 0) + 0);
            int y = texture.height - Mathf.RoundToInt((position.y - LevelController.Main.MapHeightMin) / (LevelController.Main.MapHeightMax - LevelController.Main.MapHeightMin) * (texture.height - 0) + 0) - 1;

            return new Vector2Int(x, y);
        }

        public static Color GetInverse(Color color)
        {
            return new Color
            (
                1f - color.r,
                1f - color.g,
                1f - color.b,
                color.a
            );
        }

        public static Color BlendColors(Color colorA, Color colorB, float weight)
        {
            float r = (colorA.r + colorB.r * weight) / 2f;
            float g = (colorA.g + colorB.g * weight) / 2f;
            float b = (colorA.b + colorB.b * weight) / 2f;

            return new Color(r, g, b, colorA.a);
        }

        public static Color AverageColorFromTexture(Sprite sprite)
        {
            Color[] texColors = sprite.texture.GetPixels
            (
                (int)sprite.textureRect.x,
                (int)sprite.textureRect.y,
                (int)sprite.textureRect.width,
                (int)sprite.textureRect.height
            );

            int total = texColors.Length;
            float r = 0f;
            float g = 0f;
            float b = 0f;
            for (int i = 0; i < total; i++)
            {
                if (texColors[i].a < 0.25f) continue;

                r += texColors[i].r;
                g += texColors[i].g;
                b += texColors[i].b;
            }

            return new Color(r / total, g / total, b / total, 1f);
        }

        public static Color AverageColorFromTexture(Texture2D texture)
        {
            Color[] texColors = texture.GetPixels();
            int total = texColors.Length;
            float r = 0f;
            float g = 0f;
            float b = 0f;
            for (int i = 0; i < total; i++)
            {
                if (texColors[i].a < 0.25f) continue;

                r += texColors[i].r;
                g += texColors[i].g;
                b += texColors[i].b;
            }

            return new Color(r / total, g / total, b / total, 1f);
        }
    }
}
