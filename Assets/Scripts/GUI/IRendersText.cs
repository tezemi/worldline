﻿using Worldline.GUI.Printing;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.GUI
{
    public interface IRendersText
    {
        string Text { get; set; }
        [CanBeNull]
        TextPrinter TextPrinter { get; set; }

        T FindComponent<T>() where T : Component;
    }
}
