﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.GUI
{
    public class MouseButtonBox : TutorialBox
    {
        public Sprite MouseZero;
        public Sprite MouseOne;
        public Sprite MouseTwo;
    }
}
