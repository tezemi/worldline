// Copyright (c) 2019 Destin Hebner
using Worldline.GUI.Printing;
using UnityEngine;

namespace Worldline.GUI
{
    public class DialogController : MonoBehaviour
    {
        public static DialogController Main;
        public SlidingDialogBox MainDialogBox;
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }
    }
}