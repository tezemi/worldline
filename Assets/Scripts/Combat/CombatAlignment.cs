// Copyright (c) 2019 Destin Hebner
namespace Worldline.Combat
{
    /// <summary>
    /// Represents an alignment or "team" which is used to determine how 
    /// two entities should interact, most commonly, if two entities should
    /// deal damage to each other. Neutral entities are exempt from combat
    /// and will not effect entities, or be effected by them. Friendly
    /// entities will effect enemy and decoration entities. Enemy entities
    /// will effect friendly and decoration entities. Decoration entities
    /// will be effected by other entities, but will not effect any other
    /// entities. EffectsAll entities will effect all entities, excluding
    /// neutral entities.
    /// </summary>
    public enum CombatAlignment
    {
        Neutral,
        Friendly,
        Enemy,
        EffectsAll,
        Decoration
    }
}
