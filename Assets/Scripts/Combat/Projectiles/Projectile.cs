// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Physics;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;
using UnityEngine.Serialization;

namespace Worldline.Combat.Projectiles
{
    /// <summary>
    /// This is a <see cref="Component"/> for <see cref="GameObject"/>s that move in a direction
    /// or toward a target, and do damage on impact with enemies. Things like bullets and other
    /// projectiles should have this <see cref="Component"/>. More functionality and be 
    /// implemented by inheriting from this class, but this class can also be used as a 
    /// <see cref="Component"/> by itself. By default, when an instance of a 
    /// <see cref="GameObject"/> with an attached <see cref="Projectile"/> is created, it will
    /// move forward at <see cref="Speed"/> using the <see cref="Transform"/>'s rotation.
    /// </summary>
    public class Projectile : MonoBehaviour, IImbueable, ICanRessurect, IHasAlignment, IPoolable
	{
        [Tooltip("The projectile will die when it moves off the of the screen.")]
        public bool DestroyOffScreen;
	    [Tooltip("The projectile will die when it collides with surfaces.")]
        public bool DestroyOnSurface;
	    [Tooltip("The projectile will die when it collides with an enemy.")]
        public bool DestroyOnCollision;
	    [Tooltip("The projectile will rotate to face the direction it is moving.")]
        public bool FaceMovingDirection;
        [Tooltip("Determines if this projectile will automatically start moving when created.")]
	    public bool ShouldSetSpeed = true;
	    [Tooltip("The amount of damage this projectile does to enemies it hits.")]
        public int Damage;
	    [Tooltip("The time this projectile will be alive for. Set to zero for infinite time.")]
        public float TimeAlive;
	    [Tooltip("The speed at which this projectile moves.")]
        public float Speed = 4f;
	    [Tooltip("The optional name of a pool to use for this projectile.")]
        public string DeathPoolName;
	    [Tooltip("Defines who the projectile will do damage to.")]
        public CombatAlignment CombatAlignment = CombatAlignment.Neutral;
        [FormerlySerializedAs("ParticleSprites")]
        [Tooltip("Sprites for an optional particle effect to create on death.")]
        public Sprite[] DeathParticleSprites;
        /// <summary>
        /// Whether or not the <see cref="Projectile"/> is dead, which usually occurs when one of
        /// the first three fields are checked, or it's time alive runs out.
        /// </summary>
        public bool Dead { get; set; }
        /// <summary>
        /// True of the <see cref="Projectile"/> has recently been resurrected by its
        /// <see cref="DeathTimeOperator"/>. If true, this projectile cannot die until this is
        /// false again, which occurs after a few seconds.
        /// </summary>
	    public bool Resurrected { get; set; }
	    protected Rigidbody2D Rigidbody { get; set; }
        protected TimeOperator TimeOperator { get; set; }
	    protected SpriteRenderer SpriteRenderer { get; set; }
        protected MovementOperator MovementOperator { get; set; }
        private bool _shouldSetSpeedOnStart;
        private TimeEffect _imbuement = TimeEffect.None;
        private Vector2? _positionLastFrame;
        
	    public CombatAlignment Alignment
	    {
	        get => CombatAlignment;
	        set => CombatAlignment = value;
	    }
        
	    public TimeEffect Imbuement
	    {
	        get => _imbuement;
	        set
            {
                if (value == TimeEffect.FastForward)
                {
                    GetComponent<TimeOperator>().CurrentTimeEffect = TimeEffect.FastForward;
                }

                _imbuement = value;
            }
	    }

        protected virtual void Awake()
        {
            _shouldSetSpeedOnStart = ShouldSetSpeed;
            Rigidbody = GetComponent<Rigidbody2D>();
            TimeOperator = GetComponent<TimeOperator>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            MovementOperator = GetComponent<MovementOperator>();
            if (TimeAlive > 0)
            {
                CoroutineTimer destroyTimer = new CoroutineTimer(TimeOperator, Die);
                destroyTimer.StartTimer(TimeAlive, 1);
            }
        }

        protected virtual void Update()
        {
            if (TimeOperator.ShouldHold)
            {
                return;
            }
            
            if (FaceMovingDirection)
            {
                transform.right = Rigidbody.velocity;
            }

            // Collision Raycast
            if (_positionLastFrame != null)
            {
                Debug.DrawLine(transform.position, (Vector2)_positionLastFrame, Color.cyan, 2f);
                RaycastHit2D[] rays = Physics2D.LinecastAll(transform.position, (Vector2)_positionLastFrame);
                foreach (RaycastHit2D ray in rays)
                {
                    if (ray)
                    {
                        Collider2D collision = ray.collider;
                        if (DestroyOnSurface && CheckSurfaceCollision(collision))
                        {
                            transform.position = ray.point;
                            Die();
                        }
                    }
                }
            }

            _positionLastFrame = transform.position;

            // Destroy Off Screen
            if (Resurrected)
            {
                return;
            }

            Camera mainCamera = CameraOperator.ActiveCamera.GetComponent<Camera>();
            if (DestroyOffScreen && Vector2.Distance(transform.position, mainCamera.transform.position) > mainCamera.orthographicSize * 2 && GetComponent<TimeOperator>().TimeScale > 0)
            {
                Die();
            }
        }

	    protected virtual void FixedUpdate()
	    {
            if (ShouldSetSpeed)
            {
                MovementOperator.Speed = transform.right * Speed;
                ShouldSetSpeed = false;
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D collision)
        {
            if (Resurrected || !collision.enabled) return;

            if (collision.gameObject.GetComponent<Combatant>() != null)
            {
                Combatant target = collision.gameObject.GetComponent<Combatant>();
                if (!Combatant.ShouldEffect(Alignment, target.Alignment)) //Ignore Friendlies
                {
                    foreach (Collider2D myCollider2D in GetComponents<Collider2D>())
                    {
                        foreach (Collider2D theirCollider2D in collision.GetComponents<Collider2D>())
                        {
                            try
                            {
                                Physics2D.IgnoreCollision(myCollider2D, theirCollider2D);
                            }
                            catch (UnityException e)
                            {
                                Debug.LogWarning(e);
                            }
                        }
                    }
                }
                else
                {
                    //THIS IS WHERE WE HURT THINGS
                    collision.GetComponent<Combatant>().Hurt(Damage, this);
                    OnHurtCombatant(collision.GetComponent<Combatant>());
                    if (DestroyOnCollision)
                    {
                        Die();
                    }
                }
            }

            //Handle surfaces...
            if (DestroyOnSurface && TimeOperator.TimeScale > 0)
            {
                if (CheckSurfaceCollision(collision))
                {
                    Die();
                }
            }
        }

        /// <summary>
        /// Gets called when this <see cref="Projectile"/> does damage to an enemy.
        /// </summary>
        /// <param name="combatant">The enemy that the <see cref="Projectile"/> hurt.</param>
        protected virtual void OnHurtCombatant(Combatant combatant) { }

	    public void SetAnimation(Sprite[] sprites)
	    {
	        if (GetComponent<AnimationOperator>() == null) return;

	        GetComponent<AnimationOperator>().Sprites = sprites;
            GetComponent<AnimationOperator>().ForceUpdate();
	    }

        public virtual void Die()
        {
            if (Resurrected) return;

            Dead = true;
            if (DeathParticleSprites.Length > 0)
            {
                ParticleEffect effect = ParticleEffect.Create(DeathParticleSprites, transform.position, Alignment);
                effect.GetComponent<SpriteRenderer>().color = GetComponent<SpriteRenderer>().color;
                effect.GetComponent<SpriteRenderer>().maskInteraction = GetComponent<SpriteRenderer>().maskInteraction;
                if (GetComponent<Tearifier>() != null)
                {
                    effect.gameObject.AddComponent<Tearifier>();
                }
            }

            TimeOperator.CurrentTimeEffect = TimeEffect.None;
            GetComponent<SpriteRenderer>().color = Color.white;
            DeathTimeOperator.Handle(gameObject, Alignment, string.IsNullOrEmpty(DeathPoolName));
            if (DeathPoolName != null)
            {
                Pool.Add(gameObject, DeathPoolName, true);
            }

            gameObject.SetActive(false);
        }

        public void Resurrect()
        {
            Dead = false;
            Resurrected = true;
            gameObject.SetActive(true);
            Timing.RunCoroutine(ResurrectMe(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> ResurrectMe()
            {
                yield return Timing.WaitForSeconds(TimeOperator.StandardResurrectionInvincibilityTime);
                Resurrected = false;
            }
        }

        public virtual void Reuse()
        {
            Dead = false;
            Resurrected = false;
            _positionLastFrame = null;
            ShouldSetSpeed = _shouldSetSpeedOnStart;
            GetComponent<Recorder>().ResetRecorder();
            GetComponent<MovementOperator>().Speed = new Vector2(0f, 0f);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0f, 0f);
            GetComponent<SpriteRenderer>().color = Color.white;
            GetComponent<SpriteRenderer>().maskInteraction = SpriteMaskInteraction.None;
            Awake();
        }

	    public virtual bool CheckSurfaceCollision(Collider2D collision)
        {
            return !collision.isTrigger && 
            (collision.gameObject.CompareTag("Surface") && 
            collision.GetComponent<SolidTop>() == null || 
            collision.gameObject.CompareTag("Surface") && 
            collision.GetComponent<SolidTop>() != null && 
            GetComponent<Rigidbody2D>().velocity.y < 0);
        }
    }
}
