﻿// Copyright (c) 2019 Destin Hebner
using System.Diagnostics.CodeAnalysis;
using Worldline.Level;
using Worldline.Physics;
using UnityEngine;

namespace Worldline.Combat.Projectiles
{
    /// <summary>
    /// Use this <see cref="Component"/> to create <see cref="Projectile"/>s that will follow the
    /// nearest enemy instead of moving in a fixed direction.
    /// </summary>
    [SuppressMessage("ReSharper", "Unity.RedundantEventFunction")]
    public class TrackingProjectile : Projectile
    {
        public bool IgnoreMinDistance;
        public float MinDistance = 10f;

        protected virtual void OnEnable()
        {
            ShouldSetSpeed = false;
        }

        protected override void Update()
        {
            if (LevelController.Main.Paused) return;

            base.Update();
            Transform target = GetClosestEnemy();
            Vector2 targVec;
            if (target != null)
            {
                targVec = target.transform.position;
            }
            else
            {
                return;
            }

            GetComponent<MovementOperator>().Speed = new Vector2(targVec.x > transform.position.x ? Speed : -Speed, targVec.y > transform.position.y ? Speed : -Speed);
            transform.right = GetComponent<Rigidbody2D>().velocity;
        }

        public Transform GetClosestEnemy()
        {
            Transform tMin = null;
            float minDist = Mathf.Infinity;
            Vector3 currentPos = transform.position;
            foreach (Combatant t in Combatant.AllCombatants)
            {
                float dist = Vector3.Distance(t.transform.position, currentPos);
                if (!t.isActiveAndEnabled || (t.isActiveAndEnabled && 
                !Combatant.ShouldEffect(t.Alignment, Alignment)) || (dist > MinDistance && 
                !IgnoreMinDistance) || (t is Enemy n && n.Dead)) continue;

                if (dist < minDist)
                {
                    tMin = t.transform;
                    minDist = dist;
                }
            }

            return tMin;
        }
    }
}
