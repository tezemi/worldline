﻿// Copyright (c) 2019 Destin Hebner
using Worldline.Visuals;
using Worldline.Physics;
using UnityEngine;

namespace Worldline.Combat.Projectiles
{
    /// <summary>
    /// This is a type of <see cref="Projectile"/> intended for the 
    /// <see cref="Player.Weapons.BurningSun"/>. These <see cref="Projectile"/>s
    /// bounce off of surfaces when they collide with them.
    /// </summary>
    public class BurningSunDisk : Projectile
    {
        public Sprite[] BouncedSprites;
        private Vector2? _lastNormal;
        private Sprite[] _startSprites;

        protected override void Awake()
        {
            base.Awake();
            _startSprites = GetComponent<AnimationOperator>().Sprites;
        }

        protected virtual void OnCollisionStay2D(Collision2D collision)
        {
            if (TimeOperator.ShouldHold) return;

            if (CheckSurfaceCollision(collision.collider))
            {
                Vector2 normal = collision.GetContact(0).normal;
                if (!_lastNormal.HasValue || _lastNormal.Value != normal)
                {
                    _lastNormal = normal;
                    Vector2 speedBeforeChange = GetComponent<MovementOperator>().Speed;
                    Vector2 speedAfterChange = ReflectDirection(speedBeforeChange.normalized, normal);
                    GetComponent<MovementOperator>().Speed = speedAfterChange * Speed;
                    GetComponent<AnimationOperator>().Sprites = BouncedSprites;
                    GetComponent<AnimationOperator>().ForceUpdate();
                }
            }

            Vector2 ReflectDirection(Vector2 inDirection, Vector2 normal)
            {
                inDirection.Normalize();
                normal.Normalize();

                return (2 * (Vector2.Dot(inDirection, normal) * normal) - inDirection).normalized * -1;
            }
        }

        protected virtual void OnCollisionExit2D(Collision2D collision)
        {
            _lastNormal = null;
        }

        public override void Reuse()
        {
            GetComponent<AnimationOperator>().Sprites = _startSprites;
            base.Reuse();
        }
    }
}
