﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.NPCs.Enemies.Tears;
using Worldline.TimeEngine.Recording;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Worldline.Combat.Projectiles
{
    public class NullSphere : Projectile
    {
        public int ExplosionDamage = 25;
        public float Radius = 6f;
        public float ExplosionRadius = 1.5f;
        public float TimeAsParticle = 0.25f;
        public float TimeUntilExplode = 3f;
        public float SpeedLossPerFrame = 0.15f;
        public float ChargeSoundStartTime;
        public float MaxShakeIntensity = 0.2f;
        public AudioClip ChargeSound;
        public AudioClip DetonateSound;
        public Sprite[] ParticleSprites;
        public Sprite[] VortexSprites;
        public Sprite[] ExplosionSprites;
        public bool IsVortex { get; set; }
        public Vector3? RestingPosition { get; protected set; }
        public SoundEffect ChargeSoundEffect { get; protected set; }
        public CoroutineTimer ExplosionTimer { get; protected set; }
        public const float Power = 0.0001f;
        public const float DistanceModifier = 0.65f;
        public const float AnimationSpeedIncrease = 0.00075f;
        protected AnimationOperator AnimationOperator { get; set; }
        
        public readonly Dictionary<Type, float> EnemyModifiers = new Dictionary<Type, float>
        {
            { typeof(Flockling), 4f },          // fixed
            { typeof(Apparit), 10f },           // fixed
            { typeof(ApparitHand), 7f },        // fixed
            { typeof(Hurl), 100000f },          // fixed
            { typeof(Crawler), 100000f },       // fixed
            { typeof(AugliteBody), 100000f },   // fixed
            { typeof(AugliteHat), 7f },         // fixed
            { typeof(Oclack), 25f },            // fixed
            { typeof(Concourse), 10f },         // fixed
            { typeof(Mimiturge), 100000f }      // fixed
        };

        protected override void Awake()
        {
            base.Awake();
            AnimationOperator = GetComponent<AnimationOperator>();
            AnimationOperator.DisableAnimationReset = true;
        }

        protected virtual void OnEnable()
        {
            IsVortex = false;
            SetAnimation(ParticleSprites);

            ChargeSoundEffect = SoundEffect.PlaySound(ChargeSound, TimeOperator);
            ChargeSoundEffect.AudioSource.time = ChargeSoundStartTime;

            CoroutineTimer vortexActivateTimer = new CoroutineTimer(TimeOperator, () =>
            {
                SetAnimation(VortexSprites);
                IsVortex = true;
            }, () =>
            {
                SetAnimation(ParticleSprites);
                IsVortex = false;
            });

            vortexActivateTimer.StartTimer(TimeAsParticle, 1);

            ExplosionTimer = new CoroutineTimer(TimeOperator, Die);
            ExplosionTimer.StartTimer(TimeUntilExplode, 1);
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (TimeOperator.ShouldHold) return;

            if (MovementOperator.Speed == Vector2.zero)
            {
                if (!RestingPosition.HasValue)
                {
                    RestingPosition = transform.position;
                }

                float intensity = MathUtilities.GetRange
                (
                    ExplosionTimer.TimeElapsed,
                    0f,
                    ExplosionTimer.TimeBetweenCalls,
                    MaxShakeIntensity,
                    0f
                );

                transform.position = RestingPosition.Value + new Vector3
                (
                    Random.Range(-intensity, intensity),
                    Random.Range(-intensity, intensity),
                    0f
                );
            }

            MovementOperator.Speed = Vector2.MoveTowards(MovementOperator.Speed, Vector2.zero, SpeedLossPerFrame);
            if (IsVortex)
            {
                foreach (Combatant c in Combatant.AllCombatants)
                {
                    if (c is MainPlayerOperator) continue;

                    float distance = Vector2.Distance(c.transform.position, transform.position);
                    if (Combatant.ShouldEffect(c.Alignment, Alignment) && distance < Radius)
                    {
                        float enemyMod = 1;
                        if (EnemyModifiers.ContainsKey(c.GetType()))
                        {
                            enemyMod = EnemyModifiers[c.GetType()];
                        }

                        c.GetComponent<Rigidbody2D>().AddForce
                        (
                            (transform.position - c.transform.position) * (Power * ((Radius - distance) * DistanceModifier) * enemyMod)
                        );
                    }
                }

                AnimationOperator.Delay -= AnimationSpeedIncrease;
            }
        }

        public override void Die()
        {
            if (Resurrected) return;

            base.Die();

            if (ChargeSoundEffect != null && ChargeSoundEffect.IsPlaying)
            {
                ChargeSoundEffect.Stop();
            }

            SoundEffect.PlaySound(DetonateSound, TimeOperator);

            if (ExplosionSprites.Length > 0)
            {
                ParticleEffect effect = ParticleEffect.Create(ExplosionSprites, transform.position, Alignment);
                effect.GetComponent<SpriteRenderer>().color = Color.magenta;
                effect.GetComponent<SpriteRenderer>().maskInteraction = GetComponent<SpriteRenderer>().maskInteraction;
                effect.transform.localScale = new Vector3(1.5f, 1.5f, 1f);
            }

            foreach (Combatant c in Combatant.AllCombatants)
            {
                if (c is MainPlayerOperator) continue;

                float distance = Vector2.Distance(c.transform.position, transform.position);
                if (Combatant.ShouldEffect(c.Alignment, Alignment) && distance < ExplosionRadius)
                {
                    c.ForceHurt(ExplosionDamage, this);
                }
            }
        }

        public class NullSphereSnapshot : VisualSnapshot
        {
            public float Delay { get; set; }
            public Vector2 Speed { get; set; }
            public Vector3? RestingPosition { get; set; }

            public override void Record(GameObject gameObject)
            {
                base.Record(gameObject);
                Delay = gameObject.GetComponent<AnimationOperator>().Delay;
                Speed = gameObject.GetComponent<MovementOperator>().Speed;
                RestingPosition = gameObject.GetComponent<NullSphere>().RestingPosition;
            }

            public override void Replay(GameObject gameObject)
            {
                base.Replay(gameObject);
                gameObject.GetComponent<AnimationOperator>().Delay = Delay;
                gameObject.GetComponent<MovementOperator>().Speed = Speed;
                gameObject.GetComponent<NullSphere>().RestingPosition = RestingPosition;
            }
        }
    }
}
