﻿using Worldline.Player.Weapons;

namespace Worldline.Combat.Projectiles
{
    public class NullSphereBullet : Projectile
    {
        protected override void OnHurtCombatant(Combatant combatant)
        {
            if (PlayerWeaponOperator.Main != null && PlayerWeaponOperator.Main.HasWeapon<NullSphereLauncher>())
            {
                PlayerWeaponOperator.Main.GetWeapon<NullSphereLauncher>().AddCharge();
            }
        }
    }
}
