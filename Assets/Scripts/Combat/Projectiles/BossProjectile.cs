﻿using Worldline.Player;
using Worldline.Physics;
using UnityEngine;

namespace Worldline.Combat.Projectiles
{
    public class BossProjectile : Projectile
    {
        public float VerticalDistance = 6f;
        
        protected virtual void OnEnable()
        {
            ShouldSetSpeed = true;
        }

        protected override void FixedUpdate()
        {
            if (ShouldSetSpeed)
            {
                if (MainPlayerOperator.MainPlayerObject.transform.position.x > transform.position.x)
                {
                    GetComponent<MovementOperator>().Speed = new Vector2(Speed, GetComponent<MovementOperator>().Speed.y);
                }
                else
                {
                    GetComponent<MovementOperator>().Speed = new Vector2(-Speed, GetComponent<MovementOperator>().Speed.y);
                }

                ShouldSetSpeed = false;
            }

            GetComponent<MovementOperator>().Speed = new Vector2(GetComponent<MovementOperator>().Speed.x, Mathf.Sin(Time.realtimeSinceStartup) * VerticalDistance);
        }
    }
}
