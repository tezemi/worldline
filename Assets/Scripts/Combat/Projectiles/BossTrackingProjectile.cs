﻿using Worldline.Utilities;
using Worldline.Level.Obstacles;
using UnityEngine;

namespace Worldline.Combat.Projectiles
{
    public class BossTrackingProjectile : TrackingProjectile
    {
        public GameObject BossPuddlePrefab;

        public override void Die()
        {
            GameObject puddle;
            if (Pool.Has(BossProjectilePuddle.PoolName))
            {
                puddle = Pool.Get(BossProjectilePuddle.PoolName);
            }
            else
            {
                puddle = Instantiate(BossPuddlePrefab);
            }

            puddle.SetActive(true);
            puddle.transform.position = transform.position;

            base.Die();
        }
    }
}
