// Copyright (c) 2019 Destin Hebner
namespace Worldline.Combat
{
    public interface IHasAlignment
    {
        CombatAlignment Alignment { get; set; }
    }
}
