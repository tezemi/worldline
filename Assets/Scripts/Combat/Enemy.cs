﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.TimeEngine;
using Worldline.Combat.Projectiles;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;


namespace Worldline.Combat
{ 
    public class Enemy : Combatant, IImbueable
    {
        [Header("Enemy")]
        [Tooltip("Runs all of the start-up methods, but disables the GameObject on load.")]
        public bool StartInactive;
        [Tooltip("If true, this enemy will not spawn anymore after it has been killed.")]
        public bool OnlySpawnOnce = true;
        [Tooltip("If true, health and damage will be scaled to match the selected difficulty.")]
        public bool EffectedByDifficulty = true;
        [Tooltip("If not zero, damage will be done to enemies when they collide.")]
        public int TouchDamage;
        [Tooltip("This sprite will be used when this enemy dies.")]
        public Sprite DeathSprite;
        [Tooltip("A list of sounds that will be randomly selected to play when this enemy is hurt.")]
        public AudioClip[] HurtSounds;
        [Tooltip("A list of sounds that will be randomly selected to play when this enemy dies.")]
        public AudioClip[] DeathSounds;
        public GameObject ResetShadow { get; protected set; }
        public const float EasyHealthScale = 0.7f;
        public const float EasyDamageScale = 0.85f;
        public const float HardHealthScale = 1.5f;
        public const float HardDamageScale = 1.5f;
        public const string ResetShadowPoolName = "ResetShadowPool";
        private bool _shaking;
        private bool _applicationEnding;
        private int _framesAlive;

        [SerializeField]
        private TimeEffect _imbuement = TimeEffect.None;
        public virtual TimeEffect Imbuement
        {
            get => _imbuement;
            set => _imbuement = value;
        }

        protected bool ShouldActivateResetShadow => _imbuement == TimeEffect.Reset &&
            Recorder.AmountRecorded > TimeOperator.RecordFramesToRewind;

        protected override void Awake()
        {
            if (Fired && LevelLoader.Main.SwitchingLevels)
            {   // If this enemy has been killed before, don't spawn, but only do this during level loads.
                // Enemies that are spawned during normal play are assumed to be exempt from this.
                Destroy(gameObject);
            }

            base.Awake();
            if (EffectedByDifficulty)
            {
                ApplyDifficultyScale();
            }

            if (StartInactive)
            {
                gameObject.SetActive(false);
            }
        }

        protected virtual void OnDisable()
        {
            if (_applicationEnding) return;

            // Here, if the imbuement is record or reset, we cache their shadows for use later.
            if (ResetShadow != null)
            {
                ResetShadow.SetActive(false);
                Pool.Add(ResetShadow, ResetShadowPoolName, false);
                ResetShadow = null;
            }
        }

        protected virtual void OnApplicationQuit()
        {
            _applicationEnding = true;
        }

        protected virtual void FixedUpdate()
        {
            // If imbuement is set to fast-forward, we apply the fast-forward effect and make it "unbreakable."
            if (_imbuement == TimeEffect.FastForward && CombatantTimeOperator.CurrentTimeEffect != _imbuement)
            {
                CombatantTimeOperator.FastForwardBreak = 1f;
                CombatantTimeOperator.CurrentTimeEffect = _imbuement;
            }

            // Same for record
            if (_imbuement == TimeEffect.Record && CombatantTimeOperator.CurrentTimeEffect != _imbuement && _framesAlive > CombatantTimeOperator.NumberOfClones * CombatantTimeOperator.CloneGapSize)
            {
                CombatantTimeOperator.CurrentTimeEffect = _imbuement;
            }
            
            if (CombatantTimeOperator.CurrentTimeEffect != TimeEffect.Record)
            {
                _framesAlive++;
            }
        }

        protected virtual void LateUpdate()
        {
            const float shadowOffset = 0.15f;
            if (ShouldActivateResetShadow)
            {
                if (ResetShadow == null)
                {
                    if (Pool.Has(ResetShadowPoolName))
                    {
                        ResetShadow = Pool.Get(ResetShadowPoolName);
                        ResetShadow.name = gameObject.name + " Reset Shadow";
                    }
                    else
                    {
                        ResetShadow = new GameObject(gameObject.name + " Reset Shadow", typeof(SpriteRenderer));
                    }
                    
                    ResetShadow.GetComponent<SpriteRenderer>().color = TimeOperator.EffectColors[TimeEffect.Reset];
                    ResetShadow.SetActive(true);
                }

                int framesToRewind = TimeOperator.RecordFramesToRewind;
                int shadowIndex = Recorder.CurrentIndex - framesToRewind;
                shadowIndex = Recorder.GetAdjustedAmount(shadowIndex);
                ResetShadow.transform.position = Recorder.Snapshots[shadowIndex].Position + new Vector3(0f, 0f, shadowOffset);
                ResetShadow.transform.rotation = Recorder.Snapshots[shadowIndex].Rotation;
                ResetShadow.GetComponent<SpriteRenderer>().sprite = ((VisualSnapshot)Recorder.Snapshots[shadowIndex]).Sprite;
                ResetShadow.GetComponent<SpriteRenderer>().flipX = ((VisualSnapshot)Recorder.Snapshots[shadowIndex]).FlipX;
                ResetShadow.GetComponent<SpriteRenderer>().flipY = ((VisualSnapshot)Recorder.Snapshots[shadowIndex]).FlipY;
            }
        }

        protected virtual void OnTriggerStay2D(Collider2D other)
        {
            if (other.GetComponent<Combatant>() == null || !other.IsTouching(TriggerCollider)) return;

            bool shouldEffect = ShouldEffect(Alignment, other.GetComponent<Combatant>().Alignment);
            if (shouldEffect && TouchDamage > 0)
            {
                other.GetComponent<Combatant>().Hurt(TouchDamage, this);
            }
        }

        protected virtual void ApplyDifficultyScale()
        {
            switch (SaveFile.LoadedSaveFile.Difficulty)
            {
                case Difficulty.Easy:
                    MaxHealth = (int)(MaxHealth * EasyHealthScale);
                    Health = (int)(Health * EasyHealthScale);
                    TouchDamage = (int)(TouchDamage * EasyDamageScale);
                    break;
                case Difficulty.Normal:
                    break;
                case Difficulty.Hard:
                case Difficulty.AssBlasting:
                    MaxHealth = (int)(MaxHealth * HardHealthScale);
                    Health = (int)(Health * HardHealthScale);
                    TouchDamage = (int)(TouchDamage * HardDamageScale);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private GameObject CreateProjectileAtPosition(GameObject projectilePrefab, string projectilePool, float zPos = 0.15f)
        {
            PlayerOperator player = GetClosestPlayer();
            if (player == null)
            {
                return null;
            }

            GameObject projectileGameObject;
            if (!string.IsNullOrEmpty(projectilePool) && Pool.Has(projectilePool))
            {
                projectileGameObject = Pool.Get(projectilePool);
            }
            else
            {
                projectileGameObject = Instantiate(projectilePrefab);
            }

            projectileGameObject.SetActive(true);
            projectileGameObject.transform.position = transform.position + new Vector3(0f, 0f, zPos);
            Vector3 vectorToTarget = player.transform.position - projectileGameObject.transform.position;
            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg;
            Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
            projectileGameObject.transform.rotation = q;
            Projectile projectile = projectileGameObject.GetComponent<Projectile>();
            projectile.Alignment = Alignment;
            projectile.Imbuement = Imbuement;

            return projectileGameObject;
        }

        public virtual void Shake(float intensity, float duration)
        {
            _shaking = true;
            Timing.RunCoroutine(DoShake(), Segment.FixedUpdate, GetInstanceID().ToString());
            Timing.RunCoroutine(TimeShake(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> DoShake()    // Actually causes shaking
            {
                while (_shaking && isActiveAndEnabled)
                {
                    yield return this.WaitForFixedSecondsScaled(Time.fixedDeltaTime);
                    transform.position += new Vector3(Random.value > 0.5f ? intensity : -intensity, 0f, 0f);
                }
            }

            IEnumerator<float> TimeShake()  // Determines when to stop.
            {
                yield return this.WaitForFixedSecondsScaled(duration);
                _shaking = false;
            }
        }

        public virtual PlayerOperator GetClosestPlayer()
        {
            float minDistance = int.MaxValue;
            PlayerOperator closestPlayer = null;
            foreach (PlayerOperator playerOperator in PlayerOperator.AllPlayers)
            {
                float currentDistance = Vector2.Distance(playerOperator.transform.position, transform.position);
                if (currentDistance < minDistance)
                {
                    minDistance = currentDistance;
                    closestPlayer = playerOperator;
                }
            }

            return closestPlayer;
        }

        public virtual GameObject FireProjectile(GameObject projectilePrefab, string projectilePool, Vector2 force, float zPos = 0.15f)
        {
            GameObject projectileGameObject = CreateProjectileAtPosition(projectilePrefab, projectilePool, zPos);
            projectileGameObject.GetComponent<Rigidbody2D>().AddForce(force, ForceMode2D.Impulse);

            return projectileGameObject;
        }

        public virtual GameObject[] FireProjectiles(GameObject projectilePrefab, string projectilePool, Vector2[] forces, int amount)
        {
            GameObject[] bullets = new GameObject[amount];
            for (int i = 0; i < amount; i++)
            {
                bullets[i] = FireProjectile(projectilePrefab, projectilePool, i >= forces.Length ? forces[0] : forces[i]);
            }

            return bullets;
        }

        public virtual GameObject FireProjectileAtPlayer(GameObject projectilePrefab, string projectilePool)
        {
            GameObject projectileGameObject = CreateProjectileAtPosition(projectilePrefab, projectilePool);

            return projectileGameObject;
        }

        public virtual GameObject[] FireProjectilesAtPlayer(GameObject projectilePrefab, string projectilePool, int amount)
        {
            GameObject[] bullets = new GameObject[amount];
            for (int i = 0; i < amount; i++)
            {
                bullets[i] = FireProjectileAtPlayer(projectilePrefab, projectilePool);
            }

            return bullets;
        }

        public override void Hurt(int damage, object other)
        {
            base.Hurt(damage, other);

            if (HurtSounds != null && HurtSounds.Length > 0 && !CombatantTimeOperator.IsClone)
            {
                SoundEffect.PlaySound(HurtSounds[Random.Range(0, HurtSounds.Length)], CombatantTimeOperator);
            }
        }

        public override void Die()
        {
            if (OnlySpawnOnce)
            {
                FireID();
            }

            if (Imbuement == TimeEffect.Reset)
            {
                CombatantTimeOperator.CurrentTimeEffect = TimeEffect.Reset;
                CombatantTimeOperator.ResetClone.GetComponent<SpriteRenderer>().color = Color.white;
                CombatantTimeOperator.ResetClone.GetComponent<Enemy>().Imbuement = TimeEffect.None;
            }

            if (DeathSounds != null && DeathSounds.Length > 0 && !Invulnerable)
            {
                SoundEffect.PlaySound(DeathSounds[Random.Range(0, DeathSounds.Length)], CombatantTimeOperator);
            }

            base.Die();
        }
    }
}
