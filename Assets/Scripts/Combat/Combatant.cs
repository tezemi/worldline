// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.GUI.HUD;
using Worldline.GameState;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Combat
{
    /// <summary>
    /// This is the base class for GameCharacters that can engage in 
    /// combat, implementing health, death, and associated time effects.
    /// Both NPCs and the PlayerOperator inherit from this class.
    /// </summary>
    [HasDefaultState]
    [DisableOnPlayback]
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(CombatantTimeOperator))]
    public abstract class Combatant : GameCharacter, ICanRessurect
    {
        [Header("Combatant")]
        public int Health = 10;
        public int MaxHealth = 10;
        [Tooltip("Time spent invulnerable after getting hurt.")]
        public float InvincibilityTime = 0.4f;
        public bool Dead { get; set; }
        public bool Invulnerable { get; set; }
        protected Recorder Recorder { get; set; }
        protected CombatantTimeOperator CombatantTimeOperator { get; set; }
        private static List<Combatant> _allCombatants = new List<Combatant>();

        public static List<Combatant> AllCombatants
        {
            get
            {
                _allCombatants.RemoveAll(n => n == null);

                return _allCombatants;
            }
            set => _allCombatants = value;
        }

        protected override void Awake()
        {
            base.Awake();
            Recorder = GetComponent<Recorder>();
            CombatantTimeOperator = GetComponent<CombatantTimeOperator>();
            AllCombatants.Add(this);
        }

        protected virtual void Update()
        {
            if (Alignment != _alignment)
            {
                Alignment = _alignment;
            }
        }

        public virtual void Hurt(int damage, object other)
        {
            if (Invulnerable) return;

            ForceHurt(damage, other);
        }

        public virtual void ForceHurt(int damage, object other)
        {
            Health -= damage;
            MakeInvulnerable();
            TextPopupController.Main.ShowDamagePopup(damage, transform.position);
            if (Health <= 0)
            {
                Die();
            }
            else
            {
                CombatantTimeOperator.Hurt(damage, other);
            }
        }

        public virtual void Die()
        {
            Dead = true;
            gameObject.SetActive(false);
            if (CombatantTimeOperator.CurrentTimeEffect != TimeEffect.Rewind)
            {   // If something dies while rewinding, it can't come back to life.
                // This rule makes sure they player can't farm insane amounts of essence
                // by killing something while rewinding it, immediately bringing it
                // back to life, and instantly killing it again, over and over
                DeathTimeOperator.Handle(gameObject, Alignment, true);
            }

            CombatantTimeOperator.CurrentTimeEffect = TimeEffect.None;
        }

        public virtual void Resurrect()
        {
            Dead = false;
            Invulnerable = false;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Makes Invulnerable for the specified amount of InvincibilityTime. 
        /// Does nothing if already Invulnerable, preventing infinite 
        /// invulnerability when taking damage or if Invulnerable through
        /// some other means.
        /// </summary>
        public virtual void MakeInvulnerable()
        {
            if (Invulnerable)
            {
                return;
            }

            Invulnerable = true;
            Timing.RunCoroutine(MakeVulnerable(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> MakeVulnerable()
            {
                yield return Timing.WaitForSeconds(InvincibilityTime);
                Invulnerable = false;
            }
        }

        /// <summary>
        /// Checks to see if alignmentA should effect or hurt alignmentB.
        /// </summary>
        /// <param name="alignmentA">The alignment effecting or "hurting" the other alignment.</param>
        /// <param name="alignmentB">The alignment which is being effected or hurt.</param>
        /// <returns>True if alignmentA should effect alignmentB.</returns>
        public static bool ShouldEffect(CombatAlignment alignmentA, CombatAlignment alignmentB)
        {
            switch (alignmentA)
            {
                case CombatAlignment.Neutral:
                    return false;
                case CombatAlignment.Friendly:
                    return alignmentB == CombatAlignment.Enemy || alignmentB == CombatAlignment.Decoration;
                case CombatAlignment.Enemy:
                    return alignmentB == CombatAlignment.Friendly || alignmentB == CombatAlignment.Decoration;
                case CombatAlignment.EffectsAll:
                    return alignmentB != CombatAlignment.Neutral;
                case CombatAlignment.Decoration:
                    return false;
                default:
                    throw new ArgumentOutOfRangeException(nameof(alignmentA), alignmentA, null);
            }
        }

        [SetsUpDefaults]
        public static void SetupDefaults()
        {
            AllCombatants = new List<Combatant>();
        }
    }
}