﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.Player.Weapons;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;

namespace Worldline.Combat
{
    /// <summary>
    /// Base abstract class for a weapon operator. When attached to a GameObject, 
    /// inheritors of this class may implement code that will allow the GameObject
    /// to rotate and render sprites, as well as fire projectiles based on the
    /// rotation. Mainly used for the player's weapons system, but can be used
    /// for NPCs or enemies.
    /// </summary>
    [RequireComponent(typeof(Recorder))]
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(WeaponOperatorTimeOperator))]
    public abstract class WeaponOperator : MonoBehaviour
    {
        [Tooltip("A GameObject with a SpriteRenderer that will be used to represent the weapon.")]
        public GameObject WeaponSpriteGameObject;
        [Tooltip("A GameObject with a SpriteRenderer that will be used to represent the back arm. Can be null.")]
        public GameObject BackArmSpriteGameObject;
        /// <summary>
        /// If false, the weapons localPosition will not be set, allowing custom 
        /// positions to be set.
        /// </summary>
        public bool SetWeaponPosition { get; set; } = true;
        /// <summary>
        /// When true, weapons cannot be fired, but all other behavior is the same.
        /// </summary>
        public bool DisableFiring { get; set; }
        /// <summary>
        /// The speed at which aiming occurs.
        /// </summary>
        public float AimLerp { get; set; } = 0.5f;
        /// <summary>
        /// The speed at which aiming occurs when this WeaponOperator is effected 
        /// by slow.
        /// </summary>
        public float AimLerpWhenSlowed { get; set; } = 0.08f;
        public const float FlipRotation = 0.7f;
        protected Recorder Recorder { get; set; }
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected WeaponOperatorTimeOperator WeaponOperatorTimeOperator { get; set; }
        private bool _disabled;
        private bool _recoiling;
        private List<GameObject> _activeProjectiles = new List<GameObject>();

        /// <summary>
        /// Contains every active projectile as soon as this WeaponOperator 
        /// fires them. This also contains any death handlers for the bullets.
        /// </summary>
        public List<GameObject> ActiveProjectiles
        {
            get
            {
                _activeProjectiles.RemoveAll(p => p == null || !p.activeSelf);

                return _activeProjectiles;
            }
            protected set => _activeProjectiles = value;
        }

        /// <summary>
        /// When true, this will disable the weapon's sprite and the operator's 
        /// behavior, without actually disabling the component.
        /// </summary>
        public virtual bool Disabled
        {
            get => _disabled;
            set
            {
                SpriteRenderer.enabled = !value;
                foreach (SpriteRenderer s in GetComponentsInChildren<SpriteRenderer>())
                {
                    s.enabled = !value;
                }

                _disabled = value;
            }
        }

        protected virtual void Awake()
        {
            Recorder = GetComponent<Recorder>();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            WeaponOperatorTimeOperator = GetComponent<WeaponOperatorTimeOperator>();
        }

        protected virtual void Update()
        {
            // Weapons and arms always take on color
            WeaponSpriteGameObject.GetComponent<SpriteRenderer>().color = SpriteRenderer.color;
            if (BackArmSpriteGameObject != null)
            {
                BackArmSpriteGameObject.GetComponent<SpriteRenderer>().color = SpriteRenderer.color;
            }
        }

        /// <summary>
        /// Default behavior called in the update for positing the current weapons
        /// based on the player's position and where they are aiming. Uses the lerp
        /// positioning to give the aiming a smooth effect, which is made slower
        /// if the player is slowed. Use PositionInstant() for instant aiming with
        /// no lerp.
        /// </summary>
        public virtual void Position(Quaternion q, Vector3 frontArmLeftOffset, Vector3 frontArmRightOffset, Vector3 weaponLeftOffset, Vector3 weaponRightOffset, Vector3 backArmLeftOffset, Vector3 backArmRightOffset, float recoil)
        {
            // Flip, Rotate
            SpriteRenderer.flipY = Math.Abs(transform.rotation.z) > FlipRotation;
            WeaponSpriteGameObject.GetComponent<SpriteRenderer>().flipY = SpriteRenderer.flipY;
            if (BackArmSpriteGameObject != null)
            {
                BackArmSpriteGameObject.GetComponent<SpriteRenderer>().flipY = SpriteRenderer.flipY;
            }

            transform.rotation = Quaternion.Lerp
            (
                transform.rotation,
                q,
                WeaponOperatorTimeOperator.CurrentTimeEffect == TimeEffect.Slow ? AimLerpWhenSlowed : AimLerp
            );

            // Front Arm
            transform.localPosition = SpriteRenderer.flipY ? frontArmLeftOffset : frontArmRightOffset;

            // Weapon
            if (SetWeaponPosition)
            {
                WeaponSpriteGameObject.transform.localPosition = SpriteRenderer.flipY ? weaponLeftOffset : weaponRightOffset;
            }

            // Back Arm
            if (BackArmSpriteGameObject != null)
            {
                BackArmSpriteGameObject.transform.localPosition = SpriteRenderer.flipY ? backArmLeftOffset : backArmRightOffset;
            }
        
            if (_recoiling)
            {
                transform.localPosition += -transform.right * recoil;
            }
        }

        /// <summary>
        /// Does the default "recoiling" animation where the weapon in the player's
        /// hand gets pushed back a little.
        /// </summary>
        public void Recoil()
        {
            if (_recoiling) return;

            _recoiling = true;
            Timing.RunCoroutine(EndRecoil(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> EndRecoil()
            {
                yield return this.WaitForFixedSecondsScaled(0.2f);
                _recoiling = false;
            }
        }

        /// <summary>
        /// Fires the specified Projectile. The GameObject should have a 
        /// Projectile component attached.
        /// </summary>
        /// <param name="projectile">The GameObject to be used as projectile.</param>
        /// <param name="originWeapon">If this projectile was fired from the player's weapon, this should be said weapon. If not, this should be null.</param>
        public virtual void FireProjectile(GameObject projectile, Weapon originWeapon)
        {
            if (DisableFiring) return;

            GameObject bul = projectile;
            if (bul == null) return; // This can actually happen if the bullet collides with a surface (gets destroyed) the same frame it is fired

            if (WeaponOperatorTimeOperator.CurrentTimeEffect != TimeEffect.None &&
            WeaponOperatorTimeOperator.CurrentTimeEffect != TimeEffect.Record &&
            WeaponOperatorTimeOperator.CurrentTimeEffect != TimeEffect.Reset)
            {
                bul.GetComponent<TimeOperator>().CurrentTimeEffect = WeaponOperatorTimeOperator.CurrentTimeEffect;
            }

            ActiveProjectiles.Add(bul);
            SendMessageUpwards("OnFireWeaponPrimary", bul, SendMessageOptions.DontRequireReceiver);
        }
    }
}
