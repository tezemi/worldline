﻿// Copyright (c) 2019 Destin Hebner
using System.IO;
using System.Collections.Generic;
using Worldline.GameState;
using ProtoBuf;
using UnityEngine;

namespace Worldline.Items
{
    [ProtoContract]
    public class ItemData : FileData
    {
        [ProtoMember(1)]
        public List<string> ItemNames { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Postload;
        public const string ItemAssetBundle = "items.bun";

        public override FileData PopulateAndGetFileData()
        {
            ItemNames = new List<string>();
            foreach (Item item in Item.Inventory)
            {
                ItemNames.Add(item.AssetPath);
            }

            return this;
        }

        public override void SetFileData()
        {
            AssetBundle items = AssetBundle.LoadFromFile
            (
                Path.Combine(Application.streamingAssetsPath, ItemAssetBundle)
            );

            if (items == null)
            {
                Debug.LogError("Failed to load Asset Bundle items!");
                return;
            }

            if (ItemNames != null)
            {
                foreach (string name in ItemNames)
                {
                    Item item = items.LoadAsset<Item>(name);
                    if (item == null)
                    {
                        Debug.LogError($"Failed to load Upgrade {name}!");
                        return;
                    }

                    Item.GiveItem(item);
                }
            }

            items.Unload(false);
        }
    }
}
