﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using Worldline.GameState;
using Worldline.Utilities;
using MEC;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.Items
{
    [HasDefaultState]
    [CreateAssetMenu(fileName = "New Item", menuName = "Data/Items/Simple Item")]
    public class Item : SerializableScriptableObject
    {
        public static List<Item> Inventory { get; set; } = new List<Item>();
        public string NameLocation;
        public string DescriptionLocation;
        public Sprite ItemIcon;

        public virtual void OnDestroy()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());
        }

        public static void GiveItem(Item item)
        {
            Item copy = Instantiate(item);
            Inventory.Add(copy);
        }

        public static void RemoveItem(Item item)
        {
            Inventory.Remove(item);
            Destroy(item);
        }

        public static void RemoveItem<T>()
        {
            Item item = Inventory.FirstOrDefault(u => u.GetType() == typeof(T));
            if (item != null)
            {
                RemoveItem(item);
            }
        }

        public static T GetUpgrade<T>() where T : Item
        {
            return (T)Inventory.SingleOrDefault(u => u.GetType() == typeof(T));
        }

        public static bool HasUpgrade<T>() where T : Item
        {
            return (T)Inventory.SingleOrDefault(u => u.GetType() == typeof(T)) != null;
        }

        [SetsUpDefaults]
        [UsedImplicitly]
        private static void ResetDefaults()
        {
            foreach (Item item in Inventory)
            {
                Destroy(item);
            }

            Inventory = new List<Item>();
        }
    }
}