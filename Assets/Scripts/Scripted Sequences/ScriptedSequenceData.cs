﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Reflection;
using System.Collections.Generic;
using Worldline.GameState;
using ProtoBuf;
using UnityEngine;

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// Represents serializable FileData related to ScriptedSequences.
    /// </summary>
    [ProtoContract]
    public class ScriptedSequenceData : FileData
    {
        /// <summary>
        /// Stores the names of fields and properties, their types, and their 
        /// values that reside in the <see cref="GlobalFlags"/> class.
        /// </summary>
        [ProtoMember(1)]
        public List<string> GlobalFlags { get; set; }
        public override FileDataLoadType FileDataLoadType => FileDataLoadType.Preload;

        public override FileData PopulateAndGetFileData()
        {
            GlobalFlags = new List<string>();
            BindingFlags bindingFlags = BindingFlags.GetField | BindingFlags.GetProperty | 
                                        BindingFlags.Public | BindingFlags.Static;

            foreach (MemberInfo memberInfo in typeof(GlobalFlags).GetMembers(bindingFlags))
            {
                switch (memberInfo)
                {
                    case FieldInfo fieldInfo:
                        GlobalFlags.Add($"{memberInfo.Name},{fieldInfo.FieldType},{fieldInfo.GetValue(null)}");
                        break;
                    case PropertyInfo propertyInfo:
                        GlobalFlags.Add($"{memberInfo.Name},{propertyInfo.PropertyType},{propertyInfo.GetValue(null)}");
                        break;
                }
            }
            
            return this;
        }

        public override void SetFileData()
        {
            if (GlobalFlags != null)
            {
                foreach (string s in GlobalFlags)
                {
                    string[] splitStrings = s.Split(',');
                    BindingFlags bindingFlags = BindingFlags.GetField | BindingFlags.GetProperty | 
                                                BindingFlags.Public | BindingFlags.Static;

                    MemberInfo memberInfo = typeof(GlobalFlags).GetMember(splitStrings[0], bindingFlags)[0];
                    Type type = Type.GetType(splitStrings[1]);
                    if (type != null)
                    {
                        switch (memberInfo)
                        {
                            case FieldInfo fieldInfo:
                                fieldInfo.SetValue(null, Convert.ChangeType(splitStrings[2], type));
                                break;
                            case PropertyInfo propertyInfo:
                                propertyInfo.SetValue(null, Convert.ChangeType(splitStrings[2], type));
                                break;
                        }
                    }
                    else
                    {
                        Debug.LogWarning($"Serialized a type that can not be found: {splitStrings[1]}");
                    }
                }
            }
        }
    }
}
