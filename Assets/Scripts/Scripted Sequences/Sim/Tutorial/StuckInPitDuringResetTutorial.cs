﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class StuckInPitDuringResetTutorial : ScriptedSequence
    {
        public GameObject GregGameObject;
        public GameObject DoorGameObject;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent; // Is activated by DelayedSequenceFire

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorialStuck0"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorialStuck1"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorialStuck2"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorialStuck3"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorialStuck4"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                yield return Timing.WaitForSeconds(1f);
                DoorGameObject.SetActive(true);
                yield return Timing.WaitForSeconds(1f);
                Active = false;
            }
            else
            {
                Player.Hold = true;
                CameraOperator.ActiveCamera.Focus = (GregGameObject.transform.position - Player.transform.position) / 2f;
                yield return Timing.WaitForSeconds(1f);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                CloseDialog();
                CameraOperator.ActiveCamera.Track = Player.gameObject;
                yield return Timing.WaitForSeconds(1f);
                CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
                DoorGameObject.SetActive(true);
                Active = false;
            }
        }
    }
}
