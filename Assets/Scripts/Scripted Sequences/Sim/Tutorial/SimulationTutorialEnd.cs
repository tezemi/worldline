﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SimulationTutorialEnd : ScriptedSequence
    {
        public AudioClip WeaponGetFanfare;
        public AudioClip UpgradeGetFanfare;
        public AudioClip PartyBlower;
        public Weapon MachineGun;
        public ConfettiSpawnPoint ConfettiSpawnPoint;
        public AnimatedCharacter Greg;
        public Sprite[] GregLookingUp;
        public StringNode[] Dialog { get; private set; }
        public StringNode[] DialogAlt { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd12"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEnd13")
            };

            DialogAlt = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndAlt12")
            };
        }

        public override IEnumerator<float> Fire()
        {
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(DialogAlt[0]);
                yield return ShowDialog(DialogAlt[1]);
                yield return ShowDialog(DialogAlt[2]);
                yield return ShowDialog(DialogAlt[3]);
                yield return ShowDialog(DialogAlt[4]);
                yield return ShowDialog(DialogAlt[5]);
                yield return ShowDialog(DialogAlt[6]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(DialogAlt[7], WeaponGetFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(DialogAlt[8]);
                yield return ShowDialog(DialogAlt[9]);
                yield return ShowDialog(DialogAlt[10]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(DialogAlt[11], UpgradeGetFanfare);
                yield return ShowDialog(Dialog[12]);
                Player.MaxHealth += 5;
                Player.Health = Player.MaxHealth;
                PlayerWeaponOperator.Main.GiveWeapon(MachineGun);
                CloseDialog();
                Active = false;
            }
            else
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[0]);
                CloseDialog();
                ConfettiSpawnPoint.ItsPartyTime();
                SoundEffect.PlaySound(PartyBlower);
                yield return Timing.WaitForSeconds(2f);
                Greg.OverrideDefaultAnimations = true;
                Greg.SetAnimation(GregLookingUp);
                yield return Timing.WaitForSeconds(2f);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                Greg.OverrideDefaultAnimations = false;
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[5], WeaponGetFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[12], UpgradeGetFanfare);
                yield return ShowDialog(Dialog[13]);
                Player.MaxHealth += 5;
                Player.Health = Player.MaxHealth;
                PlayerWeaponOperator.Main.GiveWeapon(MachineGun);
                CloseDialog();
                Active = false;
            }

            GlobalFlags.PlayerHasMadeGregSoMadHeQuit = false;
        }
    }
}
