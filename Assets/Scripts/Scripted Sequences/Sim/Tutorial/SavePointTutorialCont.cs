﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SavePointTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorialCont9")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.SavePointTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.SavePointTutorialContInteractions++;
            }
            else if (GlobalFlags.SavePointTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.SavePointTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
