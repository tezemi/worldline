﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class FastForwardTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial14"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial15"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorial16")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.FastForwardEssenceTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                GlobalFlags.FastForwardEssenceTutorialInteractions++;
            }
            else if (GlobalFlags.FastForwardEssenceTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                GlobalFlags.FastForwardEssenceTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                yield return ShowDialog(Dialog[16]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
