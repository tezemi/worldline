﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class RecordTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {           
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorial10")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.RecordTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.RecordTutorialInteractions++;
            }
            else if (GlobalFlags.RecordTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                GlobalFlags.RecordTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[10]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
