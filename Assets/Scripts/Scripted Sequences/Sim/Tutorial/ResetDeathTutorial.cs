﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.GUI.HUD;
using Worldline.Utilities;
using Worldline.TimeEngine;
using Worldline.TimeEngine.Recording;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Tilemaps;
using Worldline.Level.Puzzles;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class ResetDeathTutorial : ScriptedSequence
    {
        public float CrusherMinimum;
        public AnimatedCharacter GregGameCharacter;
        public Tilemap DeathTilemap;
        public SlidingDoor DoorA;
        public SlidingDoor DoorB;
        public GameObject FakeDeadPlayer;
        public Sprite[] GregLookingUp;
        public float CrushSpeed { get; protected set; } = 0.0005f;
        public float CrusherOrigin { get; protected set; }
        public StringNode[] Dialog { get; private set; }
        public override Func<bool>[] CancelSequenceActions => new Func<bool>[] { () => GlobalFlags.PlayerHasMadeGregSoMadHeQuit };
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial14"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial15"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial16"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorial17")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            CloseDialog();
            yield return Timing.WaitUntilDone(Timing.RunCoroutine
            (
                MoveCharacter(GregGameCharacter, new Vector3(-8f, 0f, 0f), new Vector2(-4f, 0f))
            ));

            Player.GetComponent<SpriteRenderer>().flipX = true;
            yield return ShowDialogWithDelay(Dialog[2], 8f, true);
            Timing.RunCoroutine(Crush(), Segment.FixedUpdate);
            DoorA.Open = false;
            DoorB.Open = false;

            yield return ShowDialogWithDelay(Dialog[3], 8f, true);
            yield return ShowDialogWithDelay(Dialog[4], 8f, true);
            yield return ShowDialogWithDelay(Dialog[5], 8f, true);
            CloseDialog();
            yield return Timing.WaitForSeconds(3f);
            GregGameCharacter.OverrideDefaultAnimations = true;
            GregGameCharacter.SetAnimation(GregLookingUp);
            yield return Timing.WaitForSeconds(0.5f);
            yield return ShowDialogWithDelay(Dialog[6], 8f, true);
            yield return ShowDialogWithDelay(Dialog[7], 8f, true);
            GregGameCharacter.OverrideDefaultAnimations = false;
            CrushSpeed = 0.5f;
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => DeathTilemap.transform.localPosition.y <= CrusherMinimum)
            );

            CameraOperator.ActiveCamera.Shake(1f, 0.05f, 0.3f);
            FakeDeath();
            yield return Timing.WaitForSeconds(0.05f);
            yield return ShowDialogWithDelay(Dialog[8], 8f, true);
            CloseDialog();
            CrushSpeed = 0.1f;
            Timing.RunCoroutine(Retract(), Segment.FixedUpdate);
            DoorA.Open = true;
            DoorB.Open = true;
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => DeathTilemap.transform.localPosition.y >= CrusherOrigin)
            );

            yield return Timing.WaitForSeconds(0.25f);
            yield return ShowDialogWithDelay(Dialog[9], 8f, true);
            yield return ShowDialogWithDelay(Dialog[10], 8f, true);
            yield return ShowDialogWithDelay(Dialog[11], 8f, true);
            CloseDialog();
            DeadPlayer2.Main.GetComponent<DeadPlayerTutorial>().AllowedToReset = true;
            if (EssenceOperator.Main.EssenceAmounts[TimeEffect.Reset] < 50)
            {
                EssenceOperator.Main.EssenceAmounts[TimeEffect.Reset] = EssenceOperator.Main.MaxEssences[TimeEffect.Reset];
            }

            Active = false;
            yield return Timing.WaitForSeconds(10f);
            if (!MainPlayerOperator.MainPlayerObject.activeSelf && DeadPlayer2.Main.GetComponent<TimeOperator>().CurrentTimeEffect != TimeEffect.Reset)
            {
                DeadPlayer2.Main.GetComponent<DeadPlayerTutorial>().AllowedToReset = false;
                Active = true;
                Player.Hold = true;
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialogWithDelay(Dialog[12], 8f, true);
                yield return ShowDialogWithDelay(Dialog[13], 8f, true);
                yield return ShowDialogWithDelay(Dialog[14], 8f, true);
                yield return ShowDialogWithDelay(Dialog[15], 8f, true);
                yield return ShowDialogWithDelay(Dialog[16], 8f, true);
                yield return ShowDialogWithDelay(Dialog[17], 8f, true);
                CloseDialog();
                yield return Timing.WaitForSeconds(1f);
                Active = false;
                DeadPlayer2.Main.GetComponent<DeadPlayerTutorial>().AllowedToReset = true;
                DeadPlayer2.Main.ActivateReset();
            }
        }

        public virtual IEnumerator<float> Crush()
        {
            start:
            yield return Timing.WaitForOneFrame;
            DeathTilemap.transform.position -= new Vector3(0f, CrushSpeed, 0f);
            if (isActiveAndEnabled && DeathTilemap.transform.localPosition.y >= CrusherMinimum)
            {
                goto start;
            }
        }

        public virtual IEnumerator<float> Retract()
        {
            start:
            yield return Timing.WaitForOneFrame;
            DeathTilemap.transform.position += new Vector3(0f, CrushSpeed, 0f);
            if (isActiveAndEnabled && DeathTilemap.transform.localPosition.y <= CrusherOrigin)
            {
                goto start;
            }
        }

        public virtual void FakeDeath()
        {
            GameObject popup = TextPopupController.Main.ShowDamagePopup(10, transform.position);
            popup.GetComponent<Text>().color = new Color(1f, 0.4f, 0f, 1f);
            float shakeAmount = (float)10 / 10 * 5f;
            HUDController.Main.Shake(shakeAmount);
            SoundEffect.PlaySound(Player.HurtSound, Player.GetComponent<TimeOperator>());
            Player.gameObject.SetActive(false);
            GameObject dp = Instantiate(FakeDeadPlayer);
            dp.transform.position = new Vector3(Player.transform.position.x, Player.transform.position.y, dp.transform.position.z);
        }
    }
}
