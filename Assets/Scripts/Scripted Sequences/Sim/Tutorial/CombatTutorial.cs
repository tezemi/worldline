﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Utilities;
using Worldline.Corruption;
using Worldline.Player.Weapons;
using MEC;
using UnityEngine;
using Worldline.Corruption.TextCorruption;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class CombatTutorial : ScriptedSequence
    {
        public AudioClip WeaponGetFanfare;
        public Weapon Pistol;
        public Flockling Flockling;
        public Vector3 FlocklingOnScreenPosition;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial14"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorial15")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextCorrupter.MainTextCorrupter.enabled = false;
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                yield return Timing.WaitForSeconds(1f);
                yield return ShowDialogWithAudioClip(Dialog[7], WeaponGetFanfare);
                PlayerWeaponOperator.Main.GiveWeapon(Pistol);
                PlayerWeaponOperator.Main.gameObject.SetActive(true);
                CloseDialog();
                Active = false;
            }
            else
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                CloseDialog();
                Vector3 flockingOriginalPosition = Flockling.transform.position;
                Flockling.DisableAggression = true;
                Flockling.TargetPoint = FlocklingOnScreenPosition;
                yield return Timing.WaitUntilDone
                (
                    () => Vector2.Distance(Flockling.transform.position, FlocklingOnScreenPosition) < 1.5f
                );

                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[7], WeaponGetFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);

                Corrupter.Intensity = 0.25f;
                Corrupter.ForceUpdate();
                yield return ShowDialogWithDelay(Dialog[10], 5f, false);
                                
                Corrupter.Intensity = 1f;
                Corrupter.ForceUpdate();
                yield return ShowDialogWithDelay(Dialog[11], 6f, false);

                Corrupter.Intensity = 0f;
                Corrupter.ForceUpdate();

                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                CloseDialog();
                Flockling.TargetPoint = flockingOriginalPosition;
                yield return Timing.WaitForSeconds(3f);
                Flockling.DisableAggression = false;
                Flockling.TargetPoint = null;
                Flockling.MaxCorruption = 0.15f;
                PlayerWeaponOperator.Main.GiveWeapon(Pistol);
                PlayerWeaponOperator.Main.gameObject.SetActive(true);
                CloseDialog();
                Active = false;
            }

            TextCorrupter.MainTextCorrupter.enabled = true;
        }
    }
}
