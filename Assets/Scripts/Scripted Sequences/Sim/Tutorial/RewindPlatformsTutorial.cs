﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class RewindPlatformsTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindPlatformsTutorial9")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.RewindPlatformsTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.RewindPlatformsTutorialInteractions++;
            }
            else if (GlobalFlags.RewindPlatformsTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.RewindPlatformsTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
