﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class WallJumpTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont12"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont13"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont14"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont15"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont16"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont17"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont18"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont19"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont20"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont21"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont22"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont23"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont24"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont25"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont26"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont27"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont28"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont29"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorialCont30")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.WallJumpTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                GlobalFlags.WallJumpTutorialContInteractions++;
            }
            else if (GlobalFlags.WallJumpTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                GlobalFlags.WallJumpTutorialContInteractions++;
            }
            else if (GlobalFlags.WallJumpTutorialContInteractions == 2)
            {
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                GlobalFlags.WallJumpTutorialContInteractions++;
            }
            else if (GlobalFlags.WallJumpTutorialContInteractions == 3)
            {
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                yield return ShowDialog(Dialog[16]);
                yield return ShowDialog(Dialog[17]);
                yield return ShowDialog(Dialog[18]);
                yield return ShowDialog(Dialog[19]);
                yield return ShowDialog(Dialog[20]);
                yield return ShowDialog(Dialog[21]);
                yield return ShowDialog(Dialog[22]);
                yield return ShowDialog(Dialog[23]);
                yield return ShowDialog(Dialog[24]);
                GlobalFlags.WallJumpTutorialContInteractions++;
            }
            else if (GlobalFlags.WallJumpTutorialContInteractions == 4)
            {
                yield return ShowDialog(Dialog[25]);
                yield return ShowDialog(Dialog[26]);
                yield return ShowDialog(Dialog[27]);
                yield return ShowDialog(Dialog[28]);
                yield return ShowDialog(Dialog[29]);
                yield return ShowDialog(Dialog[30]);
                GlobalFlags.WallJumpTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[29]);
                yield return ShowDialog(Dialog[30]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
