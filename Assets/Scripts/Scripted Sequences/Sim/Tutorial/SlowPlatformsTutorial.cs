﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SlowPlatformsTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorialPlatforms12")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.SlowPlatformTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.SlowPlatformTutorialInteractions++;
            }
            else if (GlobalFlags.SlowPlatformTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.SlowPlatformTutorialInteractions++;
            }
            else if (GlobalFlags.SlowPlatformTutorialInteractions == 2)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                GlobalFlags.SlowPlatformTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
