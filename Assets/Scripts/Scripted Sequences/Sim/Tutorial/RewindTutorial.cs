﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class RewindTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial14"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial15"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial16"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial17"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial18"),
                SerializedStrings.GetString("Sim/SimTutorial", "RewindTutorial19")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.RewindTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.RewindTutorialInteractions++;
            }
            else if (GlobalFlags.RewindTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                GlobalFlags.RewindTutorialInteractions++;
            }
            else if (GlobalFlags.RewindTutorialInteractions == 2)
            {
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                GlobalFlags.RewindTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[16]);
                yield return ShowDialog(Dialog[17]);
                yield return ShowDialog(Dialog[18]);
                yield return ShowDialog(Dialog[19]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
