﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SwitchTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorial12"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.SwitchTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                GlobalFlags.SwitchTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
