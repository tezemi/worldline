﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SavePointTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override Func<bool>[] CancelSequenceActions => new Func<bool>[] { () => GlobalFlags.PlayerHasMadeGregSoMadHeQuit };
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SavePointTutorial3"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            CloseDialog();
            Active = false;
        }
    }
}
