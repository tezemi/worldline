﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class PauseMasherTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers0"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers1"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers2"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers3"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers4"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers5"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers6"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers7"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers8"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers9"),
                SerializedStrings.GetString("Sim/SimTutorial", "PauseTutorialMashers10"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            bool originalFlip = GetComponent<SpriteRenderer>().flipX;
            GetComponent<SpriteRenderer>().flipX = Player.transform.position.x < transform.position.x;

            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.PauseMasherTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.PauseMasherTutorialInteractions++;
            }
            else if (GlobalFlags.PauseMasherTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                GlobalFlags.PauseMasherTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
            }

            GetComponent<SpriteRenderer>().flipX = originalFlip;
            CloseDialog();
            Active = false;
        }
    }
}
