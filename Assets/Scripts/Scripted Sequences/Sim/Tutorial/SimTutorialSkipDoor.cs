﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using MEC;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SimTutorialSkipDoor : SimDoor
    {
        public override IEnumerator<float> Fire()
        {
            GlobalFlags.PlayerSkippedTutorial = true;
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(base.Fire()));
        }
    }
}
