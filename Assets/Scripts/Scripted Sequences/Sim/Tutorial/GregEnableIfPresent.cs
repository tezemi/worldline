﻿using System.Collections.Generic;
using Worldline.ScriptedSequences;
using Worldline.ScriptedSequences.Generic;
using MEC;

namespace Worldline.Sim.Tutorial
{
    public class GregEnableIfPresent : EnableObjects
    {
        public override IEnumerator<float> Fire()
        {
            if (!GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                yield return Timing.WaitUntilDone(base.Fire());
            }
        }
    }
}
