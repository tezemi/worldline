﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class DashTutorialCont : ScriptedSequence
    {
        public Sprite WynnePortrait;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont12"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont13"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont14"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont15"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont16"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont17"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont18"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont19"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont20"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont21"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont22"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont23"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorialCont24")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.DashTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                //yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                GlobalFlags.DashTutorialContInteractions++;
            }
            else if (GlobalFlags.DashTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                //yield return ShowDialog(Dialog[11]);
                //yield return ShowResponses(new string[] { Dialog[12], Dialog[13] }, WynnePortrait);
                //if (QuestionSelection == 1)
                //{
                //    yield return ShowDialog(Dialog[14]);
                //    yield return ShowDialog(Dialog[15]);
                //    yield return ShowDialog(Dialog[16]);
                //    yield return ShowDialog(Dialog[17]);
                //    yield return ShowDialog(Dialog[18]);
                //    yield return ShowResponses(new string[] { Dialog[19], Dialog[20] }, WynnePortrait);
                //    yield return ShowDialog(Dialog[21]);
                //    GlobalFlags.DashTutorialContInteractions++;
                //}

                GlobalFlags.DashTutorialContInteractions++;
            }
            else if (GlobalFlags.DashTutorialContInteractions == 2)
            {
                yield return ShowDialog(Dialog[22]);
                yield return ShowDialog(Dialog[23]);
                yield return ShowDialog(Dialog[24]);
            }
            else
            {
                yield return ShowDialog(Dialog[21]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
