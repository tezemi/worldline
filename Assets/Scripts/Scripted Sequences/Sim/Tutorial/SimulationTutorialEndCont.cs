﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SimulationTutorialEndCont : ScriptedSequence
    {
        public AudioClip PartyBlower;
        public ConfettiSpawnPoint ConfettiSpawnPoint;
        public StringNode[] Dialog { get; private set; }
        public StringNode[] DialogAlt { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndCont12")
            };

            DialogAlt = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt12"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialEndContAlt13")
            };
        }

        public override IEnumerator<float> Fire()
        {
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                if (GlobalFlags.SimulationTutorialEndContInteractions == 0)
                {
                    yield return ShowDialog(DialogAlt[0]);
                    yield return ShowDialog(DialogAlt[1]);
                    yield return ShowDialog(DialogAlt[2]);
                    yield return ShowDialog(DialogAlt[3]);
                    yield return ShowDialog(DialogAlt[4]);
                    CloseDialog();
                    ConfettiSpawnPoint.ItsPartyTime();
                    SoundEffect.PlaySound(PartyBlower);
                    yield return Timing.WaitForSeconds(4f);
                    yield return ShowDialog(DialogAlt[5]);
                    GlobalFlags.SimulationTutorialEndContInteractions++;
                }
                else if (GlobalFlags.SimulationTutorialEndContInteractions == 1)
                {
                    yield return ShowDialog(DialogAlt[6]);
                    yield return ShowDialog(DialogAlt[7]);
                    yield return ShowDialog(DialogAlt[8]);
                    yield return ShowResponses(new string[] { DialogAlt[9], DialogAlt[10] });
                    if (QuestionSelection == 0)
                    {
                        yield return ShowDialog(DialogAlt[11]);
                        yield return ShowDialog(DialogAlt[12]);
                        yield return ShowDialog(DialogAlt[13]);
                        GlobalFlags.SimulationTutorialEndContInteractions++;
                    }
                    else
                    {
                        GlobalFlags.SimulationTutorialEndContInteractions += 2;
                    }
                }
                else if (GlobalFlags.SimulationTutorialEndContInteractions == 2)
                {
                    yield return ShowDialog(Dialog[11]);
                    yield return ShowDialog(Dialog[12]);
                }
                else
                {
                    yield return ShowDialog(DialogAlt[0]);
                }

                CloseDialog();
                Active = false;
            }
            else
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                if (GlobalFlags.SimulationTutorialEndContInteractions == 0)
                {
                    yield return ShowDialog(Dialog[0]);
                    yield return ShowDialog(Dialog[1]);
                    yield return ShowDialog(Dialog[2]);
                    GlobalFlags.SimulationTutorialEndContInteractions++;
                    GlobalFlags.SimulationTutorialEndContInteractions++;
                }
                else if (GlobalFlags.SimulationTutorialEndContInteractions == 1)
                {
                    yield return ShowDialog(Dialog[3]);
                    yield return ShowDialog(Dialog[4]);
                    yield return ShowDialog(Dialog[5]);
                }
                else if (GlobalFlags.SimulationTutorialEndContInteractions == 2)
                {
                    yield return ShowDialog(Dialog[6]);
                    yield return ShowDialog(Dialog[7]);
                    yield return ShowDialog(Dialog[8]);
                    yield return ShowDialog(Dialog[9]);
                    yield return ShowDialog(Dialog[10]);
                }
                else
                {
                    yield return ShowDialog(Dialog[11]);
                    yield return ShowDialog(Dialog[12]);
                }

                CloseDialog();
                Active = false;
            }
        }
    }
}
