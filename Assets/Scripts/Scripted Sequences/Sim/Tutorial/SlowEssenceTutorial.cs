﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SlowEssenceTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "SlowTutorial14")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.SlowTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                GlobalFlags.SlowTutorialInteractions++;
            }
            else if (GlobalFlags.SlowTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                GlobalFlags.SlowTutorialInteractions++;
            }
            else if (GlobalFlags.SlowTutorialInteractions == 2)
            {
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                GlobalFlags.SlowTutorialInteractions++;
            }
            else if (GlobalFlags.SlowTutorialInteractions == 3)
            {
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                GlobalFlags.SlowTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
