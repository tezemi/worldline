﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class HealingTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override Func<bool>[] CancelSequenceActions => new Func<bool>[] { () => GlobalFlags.PlayerHasMadeGregSoMadHeQuit };
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorial5")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            yield return ShowDialog(Dialog[4]);
            yield return ShowDialog(Dialog[5]);
            CloseDialog();
            Active = false;
        }
    }
}
