﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SwitchTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "SwitchTutorialCont9")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.SwitchTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                GlobalFlags.SwitchTutorialContInteractions++;
            }
            else if (GlobalFlags.SwitchTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.SwitchTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
