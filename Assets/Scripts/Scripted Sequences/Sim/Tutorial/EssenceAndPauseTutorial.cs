﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class EssenceAndPauseTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAndPauseTutorial9")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.EssenceAndPauseTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.EssenceAndPauseTutorialInteractions++;
            }
            else if (GlobalFlags.EssenceAndPauseTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.EssenceAndPauseTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
