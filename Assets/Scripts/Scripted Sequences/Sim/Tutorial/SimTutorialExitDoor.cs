﻿// Copyright (c) 2019 Destin Hebner

using System;
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using Worldline.Player.Upgrades;
using MEC;
using UnityEngine;
using Worldline.Player;
using Worldline.TimeEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SimTutorialExitDoor : ScriptedSequence
    {
        public AudioClip Fanfare;
        public GameCharacter Greg;
        public GameObject SimTutorialDoor;
        public Upgrade[] Upgrades;
        public Weapon[] Weapons;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialSkipped0"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialSkipped1"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialSkipped2"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialSkipped3"),
                SerializedStrings.GetString("Sim/SimTutorial", "SimulationTutorialSkipped4")
            };

            if (GlobalFlags.PlayerSkippedTutorial)
            {
                SimTutorialDoor.SetActive(false);
                if (!Fired)
                {
                    Greg.gameObject.SetActive(true);
                }
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            TextColor = Color.white;
            yield return ShowDialogWithAudioClip(Dialog[4], Fanfare);
            CloseDialog();
            yield return Timing.WaitForSeconds(0.25f);
            while (Greg.transform.localScale.x > 0f)
            {
                yield return Timing.WaitForOneFrame;
                Greg.transform.localScale += new Vector3(-0.15f, 0.025f, 0f);
            }

            Greg.gameObject.SetActive(false);
            Greg.FireID();
            foreach (Upgrade upgrade in Upgrades)
            {
                Player.GiveUpgrade(upgrade);
            }

            foreach (Weapon weapon in Weapons)
            {
                PlayerWeaponOperator.Main.GiveWeapon(weapon);
            }

            Player.MaxHealth += 5;
            Player.Health = Player.MaxHealth;
            foreach (TimeEffect timeEffect in Enum.GetValues(typeof(TimeEffect)))
            {
                if (timeEffect == TimeEffect.None)
                {
                    continue;
                }

                EssenceOperator.Main.EssenceAmounts[timeEffect] = EssenceOperator.Main.MaxEssences[timeEffect];
            }

            PlayerWeaponOperator.Main.gameObject.SetActive(true);
            Active = false;
        }
    }
}
