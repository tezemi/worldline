﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class FastForwardDoorTutorial : ScriptedSequence
    {
        public Sprite WynnePortrait;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor0"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor1"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor2"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor3"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor4"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor5"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor6"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor7"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor8"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor9"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor10"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor11"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor12"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor13"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor14"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor15"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor16"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor17"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor19"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor20"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor21"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor22"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor23"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor24"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor25"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor26"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor27"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor28"),
                SerializedStrings.GetString("Sim/SimTutorial", "FastForwardTutorialDoor29")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.FastForwardDoorTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.FastForwardDoorTutorialInteractions++;
            }
            else if (GlobalFlags.FastForwardDoorTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowResponses(new string[] { Dialog[6], Dialog[7], Dialog[8] }, WynnePortrait);
                if (QuestionSelection == 0)
                {
                    yield return ShowDialog(Dialog[9]);
                    yield return ShowDialog(Dialog[10]);
                    yield return ShowDialog(Dialog[11]);
                    yield return ShowDialog(Dialog[12]);
                    yield return ShowDialog(Dialog[13]);
                    yield return ShowDialog(Dialog[14]);
                    yield return ShowDialog(Dialog[15]);
                }
                else if (QuestionSelection == 1)
                {
                    yield return ShowDialog(Dialog[16]);
                    yield return ShowDialog(Dialog[17]);
                    yield return ShowDialog(Dialog[18]);
                    yield return ShowDialog(Dialog[19]);
                    yield return ShowDialog(Dialog[20]);
                    yield return ShowDialog(Dialog[21]);
                    yield return ShowDialog(Dialog[22]);
                    yield return ShowDialog(Dialog[23]);
                    yield return ShowDialog(Dialog[24]);
                }
                else
                {
                    yield return ShowDialog(Dialog[25]);
                    yield return ShowDialog(Dialog[26]);
                    yield return ShowDialog(Dialog[27]);
                }

                GlobalFlags.FastForwardDoorTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[28]);
                yield return ShowDialog(Dialog[29]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
