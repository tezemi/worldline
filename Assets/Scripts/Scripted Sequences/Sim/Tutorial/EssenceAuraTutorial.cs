﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class EssenceAuraTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "EssenceAuraTutorial10")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.EssenceAuraTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                GlobalFlags.EssenceAuraTutorialInteractions++;
            }
            else if (GlobalFlags.EssenceAuraTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.EssenceAuraTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
