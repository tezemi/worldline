﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class ImbuementTutorial : ScriptedSequence
    {
        public Flockling Flockling;
        public Vector3 FlocklingOnScreenPosition;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial14"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial15"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial16"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial17"),
                SerializedStrings.GetString("Sim/SimTutorial", "ImbuementTutorial18")
            };
        }

        public override IEnumerator<float> Fire()
        {
            if ((!Flockling.gameObject.activeSelf || Flockling == null) && 
            GlobalFlags.ImbuementTutorialInteractions == 0)
            {
                GlobalFlags.ImbuementTutorialInteractions++;
            }

            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.ImbuementTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                CloseDialog();
                Vector3 flockingOriginalPosition = Flockling.transform.position;
                Flockling.DisableAggression = true;
                Flockling.TargetPoint = FlocklingOnScreenPosition;
                yield return Timing.WaitUntilDone
                (
                    () => Vector2.Distance(Flockling.transform.position, FlocklingOnScreenPosition) < 1.5f
                );

                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                Flockling.TargetPoint = flockingOriginalPosition;
                yield return ShowDialog(Dialog[5]);
                yield return Timing.WaitForSeconds(3f);
                Flockling.DisableAggression = false;
                Flockling.TargetPoint = null;
                GlobalFlags.ImbuementTutorialInteractions++;
            }
            else if (GlobalFlags.ImbuementTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                GlobalFlags.ImbuementTutorialInteractions++;
            }
            else if(GlobalFlags.ImbuementTutorialInteractions == 2)
            {
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                yield return ShowResponses(new string[] { Dialog[14], Dialog[15] });
                if (QuestionSelection == 1)
                {
                    yield return ShowDialog(Dialog[16]);
                    GlobalFlags.ImbuementTutorialInteractions++;
                }

                GlobalFlags.ImbuementTutorialInteractions++;
            }
            else if (GlobalFlags.ImbuementTutorialInteractions == 3)
            {
                yield return ShowDialog(Dialog[17]);
            }
            else
            {
                yield return ShowDialog(Dialog[16]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
