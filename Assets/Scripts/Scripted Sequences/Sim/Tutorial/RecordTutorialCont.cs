﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class RecordTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {                
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "RecordTutorialCont9")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.RecordTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.RecordTutorialContInteractions++;
            }
            else if (GlobalFlags.RecordTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.RecordTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
