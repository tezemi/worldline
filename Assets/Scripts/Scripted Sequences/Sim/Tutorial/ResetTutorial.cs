﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class ResetTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial8"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial9"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial10"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial11"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial12"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial13"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetTutorial14")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.ResetTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                GlobalFlags.ResetTutorialInteractions++;
            }
            else if (GlobalFlags.ResetTutorialInteractions == 1)
            {
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                GlobalFlags.ResetTutorialInteractions++;
            }
            else if (GlobalFlags.ResetTutorialInteractions == 2)
            {
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
            }
            else
            {
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
