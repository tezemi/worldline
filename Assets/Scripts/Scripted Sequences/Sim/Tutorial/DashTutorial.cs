﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.Player.Upgrades;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class DashTutorial : ScriptedSequence
    {
        public AudioClip UpgradeFanfare;
        public DashUpgrade DashUpgrade;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial5"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial6"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial7"),
                SerializedStrings.GetString("Sim/SimTutorial", "DashTutorial8")
            };
        }

        public override IEnumerator<float> Fire()
        {
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                yield return Timing.WaitForSeconds(1f);
                yield return ShowDialogWithAudioClip(Dialog[5], UpgradeFanfare);
                Player.GiveUpgrade(DashUpgrade);
                CloseDialog();
                Active = false;
            }
            else
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[5], UpgradeFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                Player.GiveUpgrade(DashUpgrade);
                CloseDialog();
                Active = false;
            }
        }
    }
}
