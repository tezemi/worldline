﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class HealingTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont12"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont13"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont14"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont15"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont16"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont17"),
                SerializedStrings.GetString("Sim/SimTutorial", "HealingTutorialCont18")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.HealingTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                GlobalFlags.HealingTutorialContInteractions++;
            }
            else if (GlobalFlags.HealingTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.HealingTutorialContInteractions++;
            }
            else if (GlobalFlags.HealingTutorialContInteractions == 2)
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                GlobalFlags.HealingTutorialContInteractions++;
            }
            else if (GlobalFlags.HealingTutorialContInteractions == 3)
            {
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                yield return ShowDialog(Dialog[16]);
                GlobalFlags.HealingTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[17]);
                yield return ShowDialog(Dialog[18]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
