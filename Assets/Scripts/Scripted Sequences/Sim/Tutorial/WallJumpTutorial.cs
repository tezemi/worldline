﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.Player.Upgrades;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class WallJumpTutorial : ScriptedSequence
    {
        public WallJumpUpgrade WallJumpUpgrade;
        public AudioClip UpgradeGetFanfare;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial0"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial1"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial2"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial3"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial4"),
                SerializedStrings.GetString("Sim/SimTutorial", "WallJumpTutorial5")
            };
        }

        public override IEnumerator<float> Fire()
        {
            if (GlobalFlags.PlayerHasMadeGregSoMadHeQuit)
            {
                yield return Timing.WaitForSeconds(1f);
                yield return ShowDialogWithAudioClip(Dialog[5], UpgradeGetFanfare);
                Player.GiveUpgrade(WallJumpUpgrade);
                CloseDialog();
                Active = false;
            }
            else
            {
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[5], UpgradeGetFanfare);
                Player.GiveUpgrade(WallJumpUpgrade);
                CloseDialog();
                Active = false;
            }
        }
    }
}
