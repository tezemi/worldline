﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class CombatTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont12"),
                SerializedStrings.GetString("Sim/SimTutorial", "CombatTutorialCont13")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.CombatTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.CombatTutorialContInteractions++;
            }
            else if (GlobalFlags.CombatTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                GlobalFlags.CombatTutorialContInteractions++;
            }
            else if (GlobalFlags.CombatTutorialContInteractions == 2)
            {
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                GlobalFlags.CombatTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
