﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;
using Worldline.Visuals;
using Worldline.Utilities;
using MEC;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class Intro : ScriptedSequence
    {
        public SimDoor DoorA;
        public SimDoor DoorB;
        public TutorialPoint InteractA;
        public TutorialPoint InteractB;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "Intro0"),
                SerializedStrings.GetString("Sim/SimTutorial", "Intro1"),
                SerializedStrings.GetString("Sim/SimTutorial", "Intro2"),
                SerializedStrings.GetString("Sim/SimTutorial", "Intro3"),
                SerializedStrings.GetString("Sim/SimTutorial", "Intro4"),
                SerializedStrings.GetString("Sim/SimTutorial", "Intro5")
            };

            if (Fired)
            {
                DoorA.gameObject.SetActive(true);
                DoorB.gameObject.SetActive(true);
            }
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = DoorA.gameObject;
            yield return ShowDialog(Dialog[4]);
            DoorA.gameObject.SetActive(true);
            yield return Timing.WaitForSeconds(1f);
            CameraOperator.ActiveCamera.Track = DoorB.gameObject;
            yield return ShowDialog(Dialog[5]);
            DoorB.gameObject.SetActive(true);
            yield return Timing.WaitForSeconds(1f);
            CloseDialog();
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            yield return Timing.WaitForSeconds(1.5f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            Active = false;
            InteractA.enabled = true;
            InteractB.enabled = true;
        }
    }
}
