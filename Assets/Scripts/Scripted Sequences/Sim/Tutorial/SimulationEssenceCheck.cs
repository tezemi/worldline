﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.TimeEngine;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class SimulationEssenceCheck : ScriptedSequence
    {
        public TimeEffect EssenceToCheckFor;
        public GameObject Greg;
        public StringNode[] DialogA { get; private set; }
        public StringNode[] DialogB { get; private set; }
        public StringNode[] DialogC { get; private set; }
        public StringNode[] DialogD { get; private set; }
        public override bool HoldPlayerOnStart => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;
        private bool _playerUsedIntended;
        private bool _playerUsedAny;

        protected override void Awake()
        {
            base.Awake();
            DialogA = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence0"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence1"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence2"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence3"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence4"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence5"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence6"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssence7")
            };

            DialogB = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceB0"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceB1"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceB2"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceB3")
            };

            DialogC = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceC0"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceC1"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceC2"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceC3")
            };

            DialogD = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD0"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD1"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD2"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD3"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD4"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD5"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD6"),
                SerializedStrings.GetString("Sim/SimTutorial", "PlayerDoesntEssenceD7")
            };
        }

        protected override void Update()
        {
            base.Update();
            if (EssenceOperator.Main.CurrentEffectActive == EssenceToCheckFor && !_playerUsedIntended)
            {
                _playerUsedIntended = true;
            }

            if (EssenceOperator.Main.CurrentEffectActive != TimeEffect.None)
            {
                _playerUsedAny = true;
            }
        }

        public override IEnumerator<float> Fire()
        {
            if (_playerUsedIntended || GlobalFlags.SkipCheckingEssenceForPuzzles)
            {
                Active = false;
                yield break;
            }

            yield return Timing.WaitForSeconds(0.35f);
            Player.Hold = true;
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = null;
            if (Vector2.Distance(Greg.transform.position, Player.transform.position) < 15f)
            {
                CameraOperator.ActiveCamera.Focus =
                Player.transform.position + (Greg.transform.position - Player.transform.position) / 2;
            }
            else
            {
                CameraOperator.ActiveCamera.Focus = Greg.transform.position;
            }

            Player.GetComponent<SpriteRenderer>().flipX = Greg.transform.position.x < Player.transform.position.x;
            yield return Timing.WaitForSeconds(1f);
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly == 0)
            {
                yield return ShowDialog(DialogA[0]);
                if (!_playerUsedAny)
                {
                    yield return ShowDialog(DialogA[1]);
                }
                else
                {
                    yield return ShowDialog(DialogA[2]);
                }
                
                yield return ShowDialog(DialogA[3]);
                yield return ShowDialog(DialogA[4]);
                yield return ShowDialog(DialogA[5]);
                yield return ShowDialog(DialogA[6]);
                GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly++;
            }
            else if (GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly == 1)
            {
                yield return ShowDialog(DialogB[0]);
                if (!_playerUsedAny)
                {
                    yield return ShowDialog(DialogB[1]);
                }
                else
                {
                    yield return ShowDialog(DialogB[2]);
                }
                
                yield return ShowDialog(DialogB[3]);
                GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly++;
            }
            else if (GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly == 2)
            {
                yield return ShowDialog(DialogC[0]);
                yield return ShowDialog(DialogC[1]);
                yield return ShowDialog(DialogC[2]);
                yield return ShowDialog(DialogC[3]);
                GlobalFlags.TimesPlayerHasSolvedPuzzleIncorrectly++;
            }
            else
            {
                yield return ShowDialog(DialogD[0]);
                yield return ShowDialog(DialogD[1]);
                yield return ShowDialog(DialogD[2]);
                yield return ShowDialog(DialogD[3]);
                yield return ShowDialog(DialogD[4]);
                yield return ShowDialog(DialogD[5]);
                yield return ShowDialog(DialogD[6]);
                yield return ShowDialog(DialogD[7]);
                GlobalFlags.PlayerHasMadeGregSoMadHeQuit = true;
                GlobalFlags.SkipCheckingEssenceForPuzzles = true;
                CloseDialog();
                yield return Timing.WaitForSeconds(0.5f);
                while (Greg.transform.localScale.x > 0f)
                {
                    yield return Timing.WaitForOneFrame;
                    Greg.transform.localScale += new Vector3(-0.15f, 0.025f, 0f);
                }

                Greg.SetActive(false);
                yield return Timing.WaitForSeconds(1f);
            }

            CloseDialog();
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            yield return Timing.WaitForSeconds(1f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            Player.Hold = false;
            Active = false;
        }
    }
}
