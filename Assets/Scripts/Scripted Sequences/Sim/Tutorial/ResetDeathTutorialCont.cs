﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class ResetDeathTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "ResetDeathTutorialCont7"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.ResetDeathTutorialContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.ResetDeathTutorialContInteractions++;
            }
            else if (GlobalFlags.ResetDeathTutorialContInteractions == 1)
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                GlobalFlags.ResetDeathTutorialContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
