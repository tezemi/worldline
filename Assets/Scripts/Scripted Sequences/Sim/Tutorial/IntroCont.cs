﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Tutorial
{
    public class IntroCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont0"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont1"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont2"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont3"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont4"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont5"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont6"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont7"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont8"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont9"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont10"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont11"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont12"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont13"),
                SerializedStrings.GetString("Sim/SimTutorial", "IntroCont14")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.IntroContInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                GlobalFlags.IntroContInteractions++;
            }
            else if (GlobalFlags.IntroContInteractions == 1)
            {
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                GlobalFlags.IntroContInteractions++;
            }
            else if (GlobalFlags.IntroContInteractions == 2)
            {
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                GlobalFlags.IntroContInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[14]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
