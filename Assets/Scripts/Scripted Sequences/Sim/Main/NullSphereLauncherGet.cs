﻿using System.Collections.Generic;
using UnityEngine;
using Worldline.Utilities;
using Worldline.Player.Weapons;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class NullSphereLauncherGet : ScriptedSequence
    {
        public AudioClip WeaponFanfare;
        public Weapon WeaponToGive;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher0"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher1"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher2"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher3"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher4"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher5"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher6"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher7"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher8"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher9"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher10"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher11"),
                SerializedStrings.GetString("Sim/SimMain", "NullSphereLauncher12")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.NullSphereLauncherGetInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[3], WeaponFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                GlobalFlags.NullSphereLauncherGetInteractions++;
                PlayerWeaponOperator.Main.GiveWeapon(WeaponToGive);
            }
            else
            {
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
