﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.Level;
using Worldline.Visuals;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class DemoFinished : ScriptedSequence
    {
        public Sprite EmptyPortrait;
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "DemoFinished0"),
                SerializedStrings.GetString("Sim/SimMain", "DemoFinished1"),
                SerializedStrings.GetString("Sim/SimMain", "DemoFinished2"),
                SerializedStrings.GetString("Sim/SimMain", "DemoFinished3"),
                SerializedStrings.GetString("Sim/SimMain", "DemoFinished4")
            };
        }

        protected override void Update()
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.K))
            {
                Application.OpenURL(URLs.Kickstarter);
            }
        }

        public override IEnumerator<float> Fire()
        {
            Player.gameObject.SetActive(false);
            LevelLoader.Main.Transition.StartTransition();
            LevelLoader.Main.Transition.SafeToLoad = false;
            yield return Timing.WaitForOneFrame;
            yield return Timing.WaitUntilDone(() => LevelLoader.Main.Transition.SafeToLoad);
            yield return Timing.WaitForOneFrame;
            var parent = PersistentCanvasController.Main.Overlay.transform.parent;
            PersistentCanvasController.Main.Overlay.transform.SetParent(MainCanvasController.Main.transform);
            PersistentCanvasController.Main.Overlay.transform.SetAsFirstSibling();
            yield return Timing.WaitForOneFrame;
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            showResponses:
            yield return ShowResponses(new string[] { Dialog[3], Dialog[4] }, EmptyPortrait);
            if (QuestionSelection == 0)
            {
                Application.OpenURL(URLs.Kickstarter);
                goto showResponses;
            }
            else
            {
                CloseDialog();
                PersistentCanvasController.Main.Overlay.transform.SetParent(parent);
                SceneTransitionOperator.Main.LoadSceneTransitionEnd("TitleScreen");
            }
        }
    }
}
