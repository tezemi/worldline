﻿using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class ResetImbuementTutorial : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial0"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial1"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial2"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial3"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial4"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial5"),
                SerializedStrings.GetString("Sim/SimMain", "ResetImbuementTutorial6")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.ResetImbuementTutorialInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                GlobalFlags.ResetImbuementTutorialInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
