﻿using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine.Tilemaps;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BossEpilouge : ScriptedSequence
    {
        public Tilemap PathBlockerTiles;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge0"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge1"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge2"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge3"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge4"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge5"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge6"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge7"),
                SerializedStrings.GetString("Sim/SimMain", "BossEpilouge8")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.BossEpilougeInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                GlobalFlags.BossEpilougeInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
            }

            PathBlockerTiles.gameObject.SetActive(false);
            CloseDialog();
            Active = false;
        }
    }
}
