﻿using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Corruption;
using Worldline.Level.Obstacles;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class LesionDestroyed : ScriptedSequence
    {
        public Lesion Lesion;
        public GameObject Door;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        protected override void Update()
        {
            base.Update();
            if ((Lesion == null || Lesion.Dead) && !Active)
            {
                Activate();
            }
        }

        public override IEnumerator<float> Fire()
        {
            CameraOperator.ActiveCamera.Speed = 0.24f;
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = Door;
            while (Corrupter.Intensity > 0f)
            {
                Corrupter.Intensity -= 0.02f;
                yield return Timing.WaitForOneFrame;
            }

            yield return Timing.WaitForSeconds(2f);

            Door.SetActive(true);
            yield return Timing.WaitForSeconds(2f);
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            yield return Timing.WaitForSeconds(3f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            Active = false;
        }
    }
}
