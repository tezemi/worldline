﻿using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.Player.Weapons;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class NoBurningSunWarning : ScriptedSequence
    {
        public Sprite WynnePortrait;
        public AudioClip WeaponGetFanfare;
        public Weapon WeaponToGive;
        public GameCharacter Greg;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun0"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun1"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun2"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun3"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun4"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun5"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun6"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun7"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun8"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun9"),
                SerializedStrings.GetString("Sim/SimMain", "NoBurningSun10")
            };
        }

        protected virtual void OnEnable()
        {
            if (PlayerWeaponOperator.Main != null && PlayerWeaponOperator.Main.HasWeapon<BurningSun>() || GlobalFlags.GregIsShowingPlayerWhereBurningSunIs)
            {
                gameObject.SetActive(false);
            }
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (!PlayerWeaponOperator.Main.HasWeapon<BurningSun>())
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                if (SaveFile.LoadedSaveFile.Difficulty == Difficulty.Normal)
                {
                    yield return ShowDialog(Dialog[2]);
                    yield return ShowResponses(new string[] { Dialog[3], Dialog[4] }, WynnePortrait);
                    if (QuestionSelection == 0)
                    {
                        yield return ShowDialog(Dialog[5]);
                    }
                    else
                    {
                        yield return ShowDialog(Dialog[6]);
                        enabled = false;
                        Greg.GetComponent<MovementOperator>().Speed = new Vector2(-4f, 0f);
                        GlobalFlags.GregIsShowingPlayerWhereBurningSunIs = true;
                    }
                }
                else if (SaveFile.LoadedSaveFile.Difficulty == Difficulty.Easy)
                {
                    yield return ShowDialog(Dialog[7]);
                    TextColor = Color.white;
                    yield return ShowDialogWithAudioClip(Dialog[8], WeaponGetFanfare);
                    PlayerWeaponOperator.Main.GiveWeapon(WeaponToGive);
                }
                else
                {
                    yield return ShowDialog(Dialog[9]);
                }
            }
            else
            {
                yield return ShowDialog(Dialog[10]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
