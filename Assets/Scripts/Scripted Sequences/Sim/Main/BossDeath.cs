﻿using System.Collections.Generic;
using Worldline.Level;
using Worldline.Combat;
using Worldline.NPCs.Enemies.Tears;
using MEC;
using Worldline.Combat.Projectiles;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BossDeath : ScriptedSequence
    {
        public Boss Boss;
        public override bool HoldPlayerOnStart => false;
        public override bool IsNeverActive => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;
        
        public override IEnumerator<float> Fire()
        {
            foreach (Enemy enemy in FindObjectsOfType<Enemy>())
            {
                if (!enemy.isActiveAndEnabled) continue;

                if (enemy != Boss)
                {
                    enemy.Die();
                }
            }

            foreach (Projectile projectile in FindObjectsOfType<Projectile>())
            {
                if (!projectile.isActiveAndEnabled) continue;

                projectile.Die();
            }

            BackgroundMusicOperator.Main.AudioClip = null;

            yield return Timing.WaitForSeconds(12f);
            LevelController.Main.PhysicsTilemap.gameObject.SetActive(false);
        }
    }
}
