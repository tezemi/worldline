﻿using System.Collections.Generic;
using Worldline.Utilities;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BurningSunCourse : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse0"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse1"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse2"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse3"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse4"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourse5")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.BurningSunInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                GlobalFlags.BurningSunInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                yield return ShowDialog(Dialog[5]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
