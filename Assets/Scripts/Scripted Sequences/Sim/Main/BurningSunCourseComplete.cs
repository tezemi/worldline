﻿using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BurningSunCourseComplete : ScriptedSequence
    {
        public Weapon BurningSun;
        public AudioClip WeaponGetFanfare;
        public StringNode[] Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete0"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete1"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete2"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete3"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete4"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete5"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete6"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete7"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete8"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete9"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete10"),
                SerializedStrings.GetString("Sim/SimMain", "BurningSunCourseComplete11")
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.BurningSunCompleteInteractions == 0)
            {
                yield return ShowDialog(Dialog[0]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[1], WeaponGetFanfare);
                TextColor = CharacterColors[WorldlineCharacter.Greg];
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.BurningSunCompleteInteractions++;
                PlayerWeaponOperator.Main.GiveWeapon(BurningSun);
            }
            else if (GlobalFlags.BurningSunCompleteInteractions == 1)
            {
                yield return ShowDialog(Dialog[5]);
                yield return ShowDialog(Dialog[6]);
                yield return ShowDialog(Dialog[7]);
                yield return ShowDialog(Dialog[8]);
                GlobalFlags.BurningSunCompleteInteractions++;
            }
            else
            {
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
            }

            CloseDialog();
            Active = false;
        }
    }
}
