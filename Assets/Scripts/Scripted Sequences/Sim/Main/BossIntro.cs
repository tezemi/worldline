﻿using System.Collections.Generic;
using MEC;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BossIntro : ScriptedSequence
    {
        public AudioClip RingingSound;
        public ScriptedSequence BossAppearance;
        public List<StringNode> Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new List<StringNode>();
            for (int i = 0; i < 7; i++)
            {
                Dialog.Add(SerializedStrings.GetString("Sim/SimMain", $"BossIntro{i}"));
            }
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            Player.OverrideAnimations = true;
            yield return WatchCall(RingingSound);
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            yield return ShowDialog(Dialog[4]);
            yield return ShowDialog(Dialog[5]);
            yield return ShowDialog(Dialog[6]);
            Player.OverrideAnimations = false;
            CloseDialog();
            Active = false;

            yield return Timing.WaitForSeconds(5f);
            BossAppearance.Activate();
        }
    }
}
