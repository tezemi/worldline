﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
using UnityEngine.Tilemaps;
using Worldline.Corruption;
using Worldline.Corruption.TextCorruption;
using Worldline.Level.Obstacles;
using Worldline.NPCs;
using Worldline.Utilities;
using Worldline.Visuals;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class LesionIntro : ScriptedSequence
    {
        public Lesion Lesion;
        public Tilemap Tilemap;
        public AnimatedCharacter Greg;
        public GameObject SavePointGameObject;
        public Sprite[] GregLookingUp;
        public List<StringNode> Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new List<StringNode>();
            for (int i = 0; i < 7; i++)
            {
                Dialog.Add(SerializedStrings.GetString("Sim/SimMain", $"LesionIntro{i}"));
            }

            Lesion.Invulnerable = true;
        }

        public override IEnumerator<float> Fire()
        {
            TextCorrupter.MainTextCorrupter.enabled = false;
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            Greg.AlwaysFacePlayer = false;
            Greg.GetComponent<SpriteRenderer>().flipX = false;
            FocusCameraOn(Lesion.gameObject);
            yield return Timing.WaitForSeconds(0.5f);
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            Greg.AlwaysFacePlayer = true;
            yield return ShowDialog(Dialog[3]);
            Greg.AlwaysFacePlayer = false;
            Greg.GetComponent<SpriteRenderer>().flipX = false;
            CloseDialog();
            yield return Timing.WaitForSeconds(1f);
            Corrupter.Intensity = 1f;
            Lesion.HoverPosition = Lesion.HoverPosition + new Vector3(0f, 100f, 0f);
            yield return Timing.WaitForSeconds(4f);
            SavePointGameObject.SetActive(false);
            Greg.OverrideDefaultAnimations = true;
            Greg.SetAnimation(GregLookingUp);
            yield return Timing.WaitForSeconds(3f);
            Corrupter.Intensity = 0f;
            yield return ShowDialog(Dialog[4]);
            Greg.AlwaysFacePlayer = true;
            DefocusCamera();
            yield return ShowDialog(Dialog[5]);
            Greg.OverrideDefaultAnimations = false;
            yield return ShowDialog(Dialog[6]);
            while (Greg.transform.localScale.x > 0f)
            {
                yield return Timing.WaitForOneFrame;
                Greg.transform.localScale += new Vector3(-0.15f, 0.025f, 0f);
            }

            Greg.gameObject.SetActive(false);
            Greg.FireID();
            CloseDialog();
            Player.Hold = false;
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            CameraOperator.ActiveCamera.DisableHorizontalOffset = false;
            yield return Timing.WaitForSeconds(6f);
            Corrupter.Intensity = 0.5f;
            yield return Timing.WaitForSeconds(1f);
            Corrupter.Intensity = 1f;
            yield return Timing.WaitForSeconds(3f);
            Tilemap.gameObject.SetActive(true);
            yield return Timing.WaitForSeconds(2f);
            Destroy(Lesion.gameObject);
            Corrupter.Intensity = 0f;
            Active = false;
            TextCorrupter.MainTextCorrupter.enabled = true;
        }
    }
}
