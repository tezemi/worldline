﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Level;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class DemoEndDoor : SimDoor
    {
        public override IEnumerator<float> Fire()
        {
            //LevelLoader.Main.Transition.StartTransition();
            yield return 0;
        }
    }
}
