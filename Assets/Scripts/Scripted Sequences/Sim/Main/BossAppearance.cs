﻿using System.Collections.Generic;
using Worldline.Level;
using Worldline.Visuals;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class BossAppearance : ScriptedSequence
    {
        public Vector3 RightSide;
        public Vector3 LeftSide;
        public GameObject Boss;
        public GameObject[] BossGameObjects;
        public AudioClip BossBattleMusic;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        public override IEnumerator<float> Fire()
        {
            if (Player.transform.position.x > 236f)
            {
                foreach (GameObject obj in BossGameObjects)
                {
                    obj.transform.position = new Vector3(LeftSide.x, LeftSide.y, obj.transform.position.z);
                }
            }
            else
            {
                foreach (GameObject obj in BossGameObjects)
                {
                    obj.transform.position = new Vector3(RightSide.x, RightSide.y, obj.transform.position.z);
                }
            }

            foreach (GameObject obj in BossGameObjects)
            {
                obj.SetActive(true);
            }

            CameraOperator.ActiveCamera.Speed = 0.15f;
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = Boss;
            yield return Timing.WaitForSeconds(2f);
            BackgroundMusicOperator.Main.AudioClip = BossBattleMusic;
            yield return Timing.WaitForSeconds(6f);
            CameraOperator.ActiveCamera.Speed = 1.5f;
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            yield return Timing.WaitForSeconds(1f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;

            Active = false;
        }
    }
}
