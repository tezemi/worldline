﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Utilities;
using Worldline.Player.Weapons;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class ShootboxTutorialCont : ScriptedSequence
    {
        public StringNode[] Dialog { get; private set; }
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            if (PlayerWeaponOperator.Main != null && PlayerWeaponOperator.Main.HasWeapon<BurningSun>())
            {
                gameObject.SetActive(false);
                return;
            }

            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorialCont0"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorialCont1"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorialCont2"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorialCont3"),
            };
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            yield return ShowDialog(Dialog[3]);
            CloseDialog();
            Active = false;
        }
    }
}
