﻿using System.Collections.Generic;
using Worldline.Utilities;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class MainIntro : ScriptedSequence
    {
        public Sprite WynnePortrait;
        public AudioClip UpgradeGetFanfare;
        public List<StringNode> Dialog { get; private set; }
        public override bool UseSequenceID => false;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new List<StringNode>();
            for (int i = 0; i < 111; i++)
            {
                Dialog.Add(SerializedStrings.GetString("Sim/SimMain", $"Intro{i}"));
            }
        }

        protected virtual void OnDisable()
        {
            FireID(); // Don't spawn again after player leaves level
        }

        public override IEnumerator<float> Fire()
        {
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            if (GlobalFlags.GregQuestionsInitDialog == 0)
            {
                if (!GlobalFlags.PlayerSkippedTutorial)
                {
                    yield return ShowDialog(Dialog[0]);
                }

                yield return ShowDialog(Dialog[1]);
                yield return ShowDialog(Dialog[2]);
                yield return ShowDialog(Dialog[3]);
                yield return ShowDialog(Dialog[4]);
                GlobalFlags.GregQuestionsInitDialog++;
            }

            showQuestions:
            yield return ShowResponses(new string[] { Dialog[5], Dialog[6], Dialog[7], Dialog[8] }, WynnePortrait);
            if (QuestionSelection == 0)
            {
                yield return ShowDialog(Dialog[9]);
                yield return ShowDialog(Dialog[10]);
                yield return ShowDialog(Dialog[11]);
                yield return ShowDialog(Dialog[12]);
                yield return ShowDialog(Dialog[13]);
                yield return ShowDialog(Dialog[14]);
                yield return ShowDialog(Dialog[15]);
                yield return ShowDialog(Dialog[16]);
                yield return ShowDialog(Dialog[17]);
                yield return ShowDialog(Dialog[18]);
                GlobalFlags.GregDialogListen1 = true;
                goto showQuestions;
            }

            if (QuestionSelection == 1)
            {
                yield return ShowResponses(new string[] { Dialog[19], Dialog[20], Dialog[21] }, WynnePortrait);
                if (QuestionSelection == 0)
                {
                    yield return ShowDialog(Dialog[22]);
                    yield return ShowDialog(Dialog[23]);
                    yield return ShowDialog(Dialog[24]);
                    yield return ShowDialog(Dialog[25]);
                    yield return ShowDialog(Dialog[26]);
                    yield return ShowDialog(Dialog[27]);
                    GlobalFlags.GregDialogListen2 = true;
                }
                else if (QuestionSelection == 1)
                {
                    yield return ShowDialog(Dialog[28]);
                    yield return ShowDialog(Dialog[29]);
                    yield return ShowDialog(Dialog[30]);
                    yield return ShowDialog(Dialog[31]);
                    yield return ShowDialog(Dialog[32]);
                    yield return ShowDialog(Dialog[33]);
                    yield return ShowDialog(Dialog[34]);
                    yield return ShowDialog(Dialog[35]);
                    yield return ShowDialog(Dialog[36]);
                    yield return ShowDialog(Dialog[37]);
                    yield return ShowDialog(Dialog[38]);
                    GlobalFlags.GregDialogListen3 = true;
                }
                else if (QuestionSelection == 2)
                {
                    yield return ShowDialog(Dialog[39]);
                    yield return ShowDialog(Dialog[40]);
                    yield return ShowDialog(Dialog[41]);
                    yield return ShowDialog(Dialog[42]);
                    GlobalFlags.GregDialogListen4 = true;
                }

                goto showQuestions;
            }

            if (QuestionSelection == 2)
            {
                yield return ShowResponses(new string[] { Dialog[43], Dialog[44] }, WynnePortrait);
                if (QuestionSelection == 0)
                {
                    yield return ShowDialog(Dialog[45]);
                    yield return ShowDialog(Dialog[46]);
                    yield return ShowDialog(Dialog[47]);
                    yield return ShowDialog(Dialog[48]);
                    yield return ShowDialog(Dialog[49]);
                    yield return ShowDialog(Dialog[50]);
                    yield return ShowDialog(Dialog[51]);
                    yield return ShowResponses(new string[] { Dialog[52] }, WynnePortrait);
                    if (QuestionSelection == 0)
                    {
                        yield return ShowDialog(Dialog[54]);
                        yield return ShowDialog(Dialog[55]);
                        yield return ShowDialog(Dialog[56]);
                        yield return ShowDialog(Dialog[57]);
                        yield return ShowDialog(Dialog[58]);
                        yield return ShowDialog(Dialog[59]);
                        yield return ShowDialog(Dialog[60]);
                        yield return ShowDialog(Dialog[61]);
                        yield return ShowDialog(Dialog[62]);
                        yield return ShowDialog(Dialog[63]);
                        yield return ShowDialog(Dialog[64]);
                        yield return ShowDialog(Dialog[65]);
                        yield return ShowDialog(Dialog[66]);
                        yield return ShowDialog(Dialog[67]);
                        yield return ShowDialog(Dialog[68]);
                        yield return ShowDialog(Dialog[69]);
                        yield return ShowDialog(Dialog[70]);
                        GlobalFlags.GregDialogListen5 = true;
                    }
                }
                else if (QuestionSelection == 1)
                {
                    yield return ShowDialog(Dialog[87]);
                    yield return ShowDialog(Dialog[88]);
                    yield return ShowDialog(Dialog[89]);
                    yield return ShowDialog(Dialog[90]);
                    yield return ShowDialog(Dialog[91]);
                    yield return ShowDialog(Dialog[92]);
                    yield return ShowDialog(Dialog[93]);
                    yield return ShowDialog(Dialog[94]);
                    yield return ShowDialog(Dialog[95]);
                    yield return ShowDialog(Dialog[96]);
                    GlobalFlags.GregDialogListen6 = true;
                }

                goto showQuestions;
            }

            if (Player.MaxHealth < 20 && GlobalFlags.GregDialogListen1 && GlobalFlags.GregDialogListen2 && GlobalFlags.GregDialogListen3 && GlobalFlags.GregDialogListen4 && GlobalFlags.GregDialogListen5 && GlobalFlags.GregDialogListen6)
            {
                yield return ShowDialog(Dialog[106]);
                yield return ShowDialog(Dialog[107]);
                yield return ShowDialog(Dialog[108]);
                TextColor = Color.white;
                yield return ShowDialogWithAudioClip(Dialog[109], UpgradeGetFanfare);
                yield return ShowDialog(Dialog[110]);
                Player.MaxHealth += 5;
                Player.Health = Player.MaxHealth;
            }

            CloseDialog();
            Active = false;
        }
    }
}
