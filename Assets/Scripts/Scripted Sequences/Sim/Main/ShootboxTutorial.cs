﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Visuals;
using Worldline.Utilities;
using Worldline.Player.Weapons;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Sim.Main
{
    public class ShootboxTutorial : ScriptedSequence
    {
        public AudioClip WeaponGetFanfare;
        public Weapon WeaponToGive;
        public GameObject Greg;
        public GameObject Shootbox;
        public StringNode[] Dialog { get; private set; }
        public override bool UseGlobalID => true;
        public override bool SpawnObjectOnce => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;
        public override Func<bool>[] CancelSequenceActions => new Func<bool>[] { () => PlayerWeaponOperator.Main.HasWeapon<BurningSun>() };

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial0"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial1"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial2"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial3"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial4"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial5"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial6"),
                SerializedStrings.GetString("Sim/SimMain", "ShootboxTutorial7")
            };
        }

        public override IEnumerator<float> Fire()
        {
            GlobalFlags.GregIsShowingPlayerWhereBurningSunIs = false;
            FocusCameraOn(Greg);
            yield return Timing.WaitForSeconds(1f);
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[0]);
            FocusCameraOn(Shootbox);
            yield return Timing.WaitForSeconds(1f);
            yield return ShowDialog(Dialog[1]);
            yield return ShowDialog(Dialog[2]);
            FocusCameraOn(Greg);
            yield return ShowDialog(Dialog[3]);
            TextColor = Color.white;
            yield return ShowDialogWithAudioClip(Dialog[4], WeaponGetFanfare);
            TextColor = CharacterColors[WorldlineCharacter.Greg];
            yield return ShowDialog(Dialog[5]);
            yield return ShowDialog(Dialog[6]);
            yield return ShowDialog(Dialog[7]);
            PlayerWeaponOperator.Main.GiveWeapon(WeaponToGive);
            CloseDialog();
            DefocusCamera();
            yield return Timing.WaitForSeconds(1f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            Active = false;
        }
    }
}
