﻿using System.Collections.Generic;
using Worldline.Corruption;

namespace Worldline.ScriptedSequences.Generic
{
    public class SetCorruption : SimpleTrigger
    {
        public bool Additive;
        public float Amount;

        public override IEnumerator<float> Fire()
        {
            if (Additive)
            {
                Corrupter.Intensity += Amount;
            }
            else
            {
                Corrupter.Intensity = Amount;
            }

            yield return 0;
        }
    }
}
