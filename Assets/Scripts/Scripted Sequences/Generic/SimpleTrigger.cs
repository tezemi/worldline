﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.ScriptedSequences.Generic
{
    public abstract class SimpleTrigger : ScriptedSequence
    {
        public override bool HoldPlayerOnStart => false;
        public override bool IsNeverActive => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;
    }
}
