// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Player;
using MEC;
using UnityEngine;
using UnityEngine.SceneManagement;
using Worldline.Level;
using Worldline.Visuals;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// A generic reusable Event that will change the level using ExitLevel
    /// and ExitPosition when the player interacts. If ExitLevel is set to
    /// go to the same level, the player will be teleported to the
    /// ExitPosition with a nice animation.
    /// </summary>
    public class Door : ScriptedSequence
    {
        public string ExitLevel;
        public Vector3 ExitPosition;
        public ScriptedSequence[] FireOnLeave;
        public bool Teleporting { get; protected set; }
        public override bool UseSequenceID => false;
        public override bool IsNeverActive => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;
        public const float CamMoveDelay = 0.75f;

        protected virtual void OnDisable()
        {
            if (Teleporting && MainPlayerOperator.MainPlayerObject != null)
            {
                CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
                CameraOperator.ActiveCamera.Track = MainPlayerOperator.MainPlayerObject;
                MainPlayerOperator.MainPlayerObject.SetActive(true);
                MainPlayerOperator.MainPlayerComponent.Hurt(MainPlayerOperator.MainPlayerComponent.Health / 2, this);
                Teleporting = false;
            }
        }

        public override IEnumerator<float> Fire()
        {
            Active = false;
            if (ExitLevel == SceneManager.GetActiveScene().name)
            {
                Timing.RunCoroutine(Teleport(), Segment.FixedUpdate);

                IEnumerator<float> Teleport()
                {
                    Teleporting = true;
                    CameraOperator.ActiveCamera.Mode = CameraMode.Float;
                    CameraOperator.ActiveCamera.Track = null;
                    Player.gameObject.SetActive(false);
                    Player.gameObject.transform.position = new Vector3
                    (
                        ExitPosition.x,
                        ExitPosition.y,
                        Player.gameObject.transform.position.z
                    );

                    yield return Timing.WaitForSeconds(CamMoveDelay);
                    CameraOperator.ActiveCamera.Track = Player.gameObject;
                    bool moving = true;
                    Vector3? lastPoint = null;
                    while (moving)
                    {
                        yield return Timing.WaitForSeconds(0.05f);
                        if (lastPoint != null)
                        {
                            if (Math.Abs(Vector2.Distance((Vector2)lastPoint, CameraOperator.ActiveCamera.transform.position)) < 0.05f)
                            {
                                moving = false;
                            }
                        }

                        lastPoint = CameraOperator.ActiveCamera.transform.position;
                    }

                    yield return Timing.WaitForSeconds(CamMoveDelay);
                    CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
                    MainPlayerOperator.MainPlayerObject.SetActive(true);
                    Teleporting = false;
                }
            }
            else if (!LevelLoader.Main.SwitchingLevels) // Avoids warning
            {
                foreach (ScriptedSequence e in FireOnLeave)
                {
                    e.FireID();
                }

                LevelLoader.Main.PlayerStartPosition = ExitPosition;
                LevelLoader.Main.LoadLevel(ExitLevel, true);
            }

            yield break;
        }
    }
}
