﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.GUI;
using MEC;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// A great Event to inherit from when writing dialog using the
    /// ShowSubtleDialog() method. Events that inherit from this class
    /// can only be fired once, but do not hold the player, and are
    /// Overrideable.
    /// </summary>
    public class SubtleBase : ScriptedSequence
    {
        public override bool Overrideable => true;
        public override bool HoldPlayerOnStart => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        public override void Deactivate()
        {
            if (ActiveSpeechBubble != null)
            {
                Timing.RunCoroutine(ActiveSpeechBubble.Die(), Segment.FixedUpdate);
            }

            base.Deactivate();
        }

        public override IEnumerator<float> Fire()
        {
            yield return 0;
        }
    }
}
