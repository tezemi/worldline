﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;

namespace Worldline.ScriptedSequences.Generic
{ 
    /// <summary>
    /// A generic Event that will enable any provided GameObjects when the 
    /// player enters the Event area. Useful for spawning Tears using
    /// Worldline.Visuals.CorruptionSpawn. Only occurs once.
    /// </summary>
    public class EnableObjects : ScriptedSequence
    {
        public GameObject[] Objects;
        public override bool HoldPlayerOnStart => false;
        public override bool IsNeverActive => true;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        public override IEnumerator<float> Fire()
        {
            yield return 0;
            foreach (GameObject obj in Objects)
            {
                obj.SetActive(!obj.activeSelf);
            }
        }
    }
}
