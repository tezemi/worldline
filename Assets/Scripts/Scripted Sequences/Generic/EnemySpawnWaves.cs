﻿// Copyright (c) 2019 Destin Hebner
using System.Linq;
using System.Collections.Generic;
using Worldline.Level;
using MEC;
using UnityEngine;
using Worldline.Combat;
using Worldline.Level.Puzzles;

namespace Worldline.ScriptedSequences.Generic
{
    public class EnemySpawnWaves : ScriptedSequence
    {
        public float TimeBetweenSpawns = 1f;
        public PuzzleCompleteCameraTransition PuzzleCompleteCameraTransition;
        public List<SlidingDoor> Doors;
        public List<EnemySubList> EnemyWaves;
        public override bool IsNeverActive => true;
        public override bool HoldPlayerOnStart => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnEnter;

        public override IEnumerator<float> Fire()
        {
            foreach (SlidingDoor door in Doors)
            {
                door.Open = !door.Open;
            }

            List<Enemy> enemiesSpawnedThisSave = new List<Enemy>();
            foreach (EnemySubList enemyWaves in EnemyWaves)
            {
                enemiesSpawnedThisSave.Clear();
                foreach (Enemy enemy in enemyWaves.Enemies)
                {
                    enemy.gameObject.SetActive(true);
                    enemiesSpawnedThisSave.Add(enemy);
                }

                yield return Timing.WaitUntilDone
                (
                    new WaitUntil(() => enemiesSpawnedThisSave.All(e => e == null || e.Dead))
                );

                yield return Timing.WaitForSeconds(TimeBetweenSpawns);
            }

            if (PuzzleCompleteCameraTransition != null)
            {
                PuzzleCompleteCameraTransition.Activate();
            }

            foreach (SlidingDoor door in Doors)
            {
                door.Open = !door.Open;
            }
        }
    }
}
