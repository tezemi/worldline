﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.NPCs;
using Worldline.Physics;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// Simple Event that moves the specified GameCharacter UnitsToMove away 
    /// from their start position.
    /// </summary>
    public class SimpleMoveGameCharacter : SimpleTrigger
    {
        [Tooltip("The Speed the GameCharacter should move.")]
        public Vector3 Speed;
        [Tooltip("How far away to move the GameCharacter from their start position.")]
        public Vector3 UnitsToMove;
        [Tooltip("The GameCharacter to be moved.")]
        public GameCharacter Actor;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;
        
        public override IEnumerator<float> Fire()
        {
            Vector3 actorStartPosition = Actor.transform.position;
            while (Vector2.Distance(Actor.transform.position, actorStartPosition + UnitsToMove) > 1f)
            {
                yield return Timing.WaitForOneFrame;
                Actor.GetComponent<MovementOperator>().Speed = Speed;
            }

            Actor.GetComponent<MovementOperator>().Speed = Vector2.zero;
        }
    }
}
