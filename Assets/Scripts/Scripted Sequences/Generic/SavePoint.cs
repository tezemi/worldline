﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.GameState;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// A generic Event that allows the player to optionally save the game.
    /// </summary>
    public class SavePoint : ScriptedSequence
    {
        public Sprite WynnePortrait;
        public AudioClip SaveSound;
        public StringNode[] Dialog { get; protected set; }
        public override bool UseSequenceID => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.OnInteract;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("General/VeryGeneric", "SavePoint1"),
                SerializedStrings.GetString("General/VeryGeneric", "SavePoint2"),
                SerializedStrings.GetString("General/VeryGeneric", "SavePoint3"),
                SerializedStrings.GetString("General/VeryGeneric", "Yes"),
                SerializedStrings.GetString("General/VeryGeneric", "No")
            };
        }

        protected virtual IEnumerator<float> FadeOverlay(float speed, Color color)
        {
            while (CameraOperator.ActiveCamera.Overlay.color != color)
            {
                yield return Timing.WaitForOneFrame;
                CameraOperator.ActiveCamera.Overlay.color = Vector4.MoveTowards(CameraOperator.ActiveCamera.Overlay.color, color, speed);
            }
        }

        public override IEnumerator<float> Fire()
        {
            yield return ShowDialog(Dialog[0]);
            yield return ShowDialog(Dialog[1]);
            yield return ShowResponses(new string[] { Dialog[3], Dialog[4] }, WynnePortrait);
            if (QuestionSelection == 0)
            {
                CloseDialog();
                SaveFile.LoadedSaveFile.Save();
                SoundEffect.PlaySound(SaveSound);
                EssenceOperator.Main.enabled = false;

                CameraOperator.ActiveCamera.Overlay.color = new Color(1f, 1f, 1f, 0f);
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(FadeOverlay(0.08f, Color.white), Segment.FixedUpdate));

                yield return Timing.WaitForSeconds(0.5f);
                Player.Health = Player.MaxHealth;

                yield return Timing.WaitUntilDone(Timing.RunCoroutine(FadeOverlay(0.02f, Color.clear), Segment.FixedUpdate));

                EssenceOperator.Main.enabled = true;
                yield return ShowDialog(Dialog[2]);
            }

            CloseDialog();
            Active = false;
        }        
    }
}

