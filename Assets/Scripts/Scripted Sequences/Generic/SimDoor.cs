﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using Worldline.TimeEngine;
using Worldline.ScriptedSequences.Generic;
using MEC;
using UnityEngine;

namespace Worldline.Level
{
    /// <summary>
    /// A door Event designed for the doors in the simulation part of the
    /// game. Event GameObject should have a SpriteRenderer attached to
    /// it.
    /// </summary>
    public class SimDoor : Door
    {
        public bool Deactivated;
        public bool StayOpen;
        public Sprite StartupFrame;
        public float TimeAlive { get; set; }
        public const int FlashFrames = 15;
        public const float StartupTime = 0.05f;
        public const float RotateSpeed = 0.5f;
        protected SpriteRenderer SpriteRenderer { get; set; }
        protected TimeOperator TimeOperator { get; set; }
        private int _flashedFrames;

        protected override void Awake()
        {
            base.Awake();
            SpriteRenderer = GetComponent<SpriteRenderer>();
            TimeOperator = GetComponent<TimeOperator>();
        }

        protected virtual void OnEnable()
        {
            Sprite normalSprite = SpriteRenderer.sprite;
            SpriteRenderer.sprite = StartupFrame;
            CoroutineTimer coroutineTimer = new CoroutineTimer(TimeOperator, Startup);
            coroutineTimer.StartTimer(StartupTime, 1);

            void Startup()
            {
                SpriteRenderer.sprite = normalSprite;
            }
        }

        protected virtual void FixedUpdate()
        {
            if (TimeOperator.ShouldHold) return;

            base.Update();
            transform.Rotate(new Vector3(0f, 0f, TimeOperator.GetScaledValue(RotateSpeed)));
            if (Deactivated && _flashedFrames++ > FlashFrames)
            {
                SpriteRenderer.color = new Color(1f, 1f, 1f, Random.Range(0.25f, 0.5f));
            }
            else if (SpriteRenderer.color != Color.white)
            {
                SpriteRenderer.color = Color.white;
            }
        }

        protected override void OnTriggerEnter2D(Collider2D boxCollider2D)
        {
            if (!Deactivated)
            {
                base.OnTriggerEnter2D(boxCollider2D);
            }
        }

        public override IEnumerator<float> Fire()
        {
            if (!Deactivated)
            {
                yield return Timing.WaitUntilDone(Timing.RunCoroutine(base.Fire()));
            }
        }
    }
}
