﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.Combat;

namespace Worldline.ScriptedSequences.Generic
{
    [Serializable]
    public class EnemySubList
    {
        public List<Enemy> Enemies;
    }
}
