﻿// Copyright (c) 2019 Destin Hebner
using System.Collections.Generic;
using UnityEngine;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// Simple Event that will flip the enabled property of all specified
    /// MonoBehaviours when fired.
    /// </summary>
    public class ComponentEnabledFlipper : SimpleTrigger
    {
        public MonoBehaviour[] MonoBehaviours;

        public override IEnumerator<float> Fire()
        {
            yield return 0;
            foreach (MonoBehaviour m in MonoBehaviours)
            {
                m.enabled = !m.enabled;
            }
        }
    }
}
