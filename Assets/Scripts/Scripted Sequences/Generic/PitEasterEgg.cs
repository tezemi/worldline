﻿using System.Collections.Generic;
using Worldline.Level;
using Worldline.Player;
using Worldline.Utilities;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Generic
{
    public class PitEasterEgg : ScriptedSequence
    {
        public StringNode[] Dialog { get; protected set; }
        public override bool UseSequenceID => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        protected override void Awake()
        {
            base.Awake();
            Dialog = new[]
            {
                SerializedStrings.GetString("General/VeryGeneric", "Password")
            };
        }

        public override IEnumerator<float> Fire()
        {
            Player.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;
            BackgroundMusicOperator.Main.Volume = 0f;
            yield return Timing.WaitForSeconds(1f);
            yield return ShowDialog(SerializedStrings.GetString("General/VeryGeneric", "Password"));
            CloseDialog();
            Player.Die();
            DeadPlayer.Main.GetComponent<SpriteRenderer>().enabled = false;
            Active = false;
        }
    }
}
