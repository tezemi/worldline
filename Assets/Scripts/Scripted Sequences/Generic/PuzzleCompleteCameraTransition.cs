﻿// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;
using Worldline.Visuals;

namespace Worldline.ScriptedSequences.Generic
{
    /// <summary>
    /// Attach this to a valid Puzzle Driver, and on the completion of the 
    /// puzzle, this will move the camera to the specified position in the
    /// world (in order to indicate that the puzzle completion results in
    /// something happening elsewhere), and then move the camera back.
    /// </summary>
    public class PuzzleCompleteCameraTransition : ScriptedSequence
    {
        public float StartDelay = 1f;
        public float CompleteActionDelay = 1f;
        public float ReturnControlDelay = 1f;
        public Action PuzzleCompleteAction { get; set; }
        public override bool UseSequenceID => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        public override IEnumerator<float> Fire()
        {
            Timing.WaitForSeconds(StartDelay);
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = null;
            CameraOperator.ActiveCamera.Focus = transform.position;
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => CameraOperator.ActiveCamera.OnScreen(transform.position))
            );

            yield return Timing.WaitForSeconds(CompleteActionDelay);
            PuzzleCompleteAction.Invoke();
            yield return Timing.WaitForSeconds(ReturnControlDelay);
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            yield return Timing.WaitUntilDone
            (
                new WaitUntil(() => CameraOperator.ActiveCamera.OnScreen(Player.gameObject))
            );

            yield return Timing.WaitForSeconds(1f);
            Active = false;
        }
    }
}
