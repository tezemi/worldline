﻿using System.Collections.Generic;
using Worldline.Player;
using MEC;
using UnityEngine;
using Worldline.Physics;

namespace Worldline.ScriptedSequences
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class DelayedSequenceFire : MonoBehaviour
    {
        [Tooltip("If true, the ScriptedSequence(s) will only be fired if the player is inside the attached BoxCollider2D.")]
        public bool OnlyFireIfInside;
        [Tooltip("The delay before the specified ScriptedSequence(s) are fired.")]
        public float Delay;
        [Tooltip("Any specified ScriptedSequences will get fired after the specified delay when the player enters the BoxCollider2D.")]
        public ScriptedSequence[] ScriptedSequences;
        public bool PlayerIsInside { get; protected set; }

        protected virtual void OnDisable()
        {
            Timing.KillCoroutines(GetInstanceID().ToString());   
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if (other == MainPlayerOperator.MainPlayerComponent.GetComponent<Collider2D>())
            {
                Debug.Log("Counting down to event...");
                PlayerIsInside = true;
                Timing.RunCoroutine(Countdown(), Segment.FixedUpdate, GetInstanceID().ToString());
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D other)
        {
            if (other == MainPlayerOperator.MainPlayerComponent.GetComponent<Collider2D>())
            {
                Debug.Log("Cancelling count down...");
                PlayerIsInside = false;
                if (OnlyFireIfInside)
                {
                    Timing.KillCoroutines(GetInstanceID().ToString());
                }
            }
        }

        protected IEnumerator<float> Countdown()
        {
            yield return Timing.WaitForSeconds(Delay);
            if (OnlyFireIfInside && PlayerIsInside || !OnlyFireIfInside)
            {
                foreach (ScriptedSequence scriptedSequence in ScriptedSequences)
                {
                    if (scriptedSequence.Fired || scriptedSequence.Active) continue;

                    Debug.Log("Firing event!");
                    scriptedSequence.Activate();
                }
            }
        }
    }
}
