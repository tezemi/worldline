﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// Represents the three different ways a ScriptedSequence be fired, 
    /// either when the event's collision box is entered, when the event 
    /// is interacted with, or when the event is dependent on another 
    /// event or piece of code to fire it.
    /// </summary>
    public enum ScriptedSequenceStartMode
    {
        /// <summary>
        /// Indicates a ScriptedSequence should be fired when the player enters a 
        /// BoxCollider trigger attached with the ScriptedSequence.
        /// </summary>
        OnEnter,
        /// <summary>
        /// Indicates a ScriptedSequence should be fired when the player presses
        /// their interact button while inside the ScriptedSequence's BoxCollider
        /// trigger.
        /// </summary>
        OnInteract,
        /// <summary>
        /// Indicates a ScriptedSequence should not be fired normally, and should
        /// only be fired when the Activate method is called.
        /// </summary>
        Dependent
    }
}
