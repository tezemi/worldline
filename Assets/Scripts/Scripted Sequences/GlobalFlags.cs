﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// Properties inside this class represent global variables or states that 
    /// usually pertain to game-play or story elements. Conceptually, these are
    /// just global variables that can be accessed at any time, get loaded
    /// when the save file is loaded, and reset when a new save file is created.
    /// Value types only please, unless you setup your shit to be serialized
    /// on its own.
    /// </summary>
    public static class GlobalFlags
    {
        #region Hub
        public static int BedInteractions = 0;
        public static int ComputerInteractions = 0;
        #endregion

        #region DemoHub
        public static int ArchdeaconHubDemoTimesTalkedTo { get; set; }
        public static int DerkHubDemoTimesTalkedTo { get; set; }
        public static int KalezosHubDemoTimesTalkedTo { get; set; }
        public static int SurtesaHubDemoTimesTalkedTo { get; set; }
        public static int GodrickHubDemoTimesTalkedTo { get; set; }
        public static int VerieceHubDemoTimesTalkedTo { get; set; }
        public static int HarzixHubDemoTimesTalkedTo { get; set; }
        public static int PizzaPeteHubDemoTimesTalkedTo { get; set; }
        public static int ZekeHubDemoTimesTalkedTo { get; set; }
        public static int RupertIntroTimesTalkedTo { get; set; }
        public static int GregIntroTimesTalkedTo { get; set; }
        public static int PhamerahDemoTimesTalkedTo { get; set; }
        #endregion

        #region NewSim
        public static int IntroContInteractions { get; set; }
        public static int EssenceAndPauseTutorialInteractions { get; set; }
        public static int TimesPlayerHasSolvedPuzzleIncorrectly { get; set; }
        public static bool PlayerHasMadeGregSoMadHeQuit { get; set; }
        public static bool GregHasExplainedHeCantHelpThePlayer { get; set; }
        public static int PauseMasherTutorialInteractions { get; set; }
        public static int SlowTutorialInteractions { get; set; }
        public static int SlowPlatformTutorialInteractions { get; set; }
        public static int RewindTutorialInteractions { get; set; }
        public static int RewindPlatformsTutorialInteractions { get; set; }
        public static int SwitchTutorialInteractions { get; set; }
        public static int SwitchTutorialContInteractions { get; set; }
        public static int FastForwardEssenceTutorialInteractions { get; set; }
        public static int FastForwardDoorTutorialInteractions { get; set; }
        public static int RecordTutorialInteractions { get; set; }
        public static int RecordTutorialContInteractions { get; set; }
        public static int ResetTutorialInteractions { get; set; }
        public static int ResetDeathTutorialContInteractions { get; set; }
        public static int SavePointTutorialContInteractions { get; set; }
        public static int HealingTutorialContInteractions { get; set; }
        public static int CombatTutorialContInteractions { get; set; }
        public static int EssenceAuraTutorialInteractions { get; set; }
        public static int ImbuementTutorialInteractions { get; set; }
        public static int WallJumpTutorialContInteractions { get; set; }
        public static int DashTutorialContInteractions { get; set; }
        public static int SimulationTutorialEndContInteractions { get; set; }
        public static bool PlayerSkippedTutorial { get; set; }
        public static int BurningSunInteractions { get; set; }
        public static int BurningSunCompleteInteractions { get; set; }
        public static int ResetImbuementTutorialInteractions { get; set; }
        public static int NullSphereLauncherGetInteractions { get; set; }
        public static bool GregIsShowingPlayerWhereBurningSunIs { get; set; }
        public static int GregQuestionsInitDialog { get; set; }
        public static bool GregDialogListen1 { get; set; }
        public static bool GregDialogListen2 { get; set; }
        public static bool GregDialogListen3 { get; set; }
        public static bool GregDialogListen4 { get; set; }
        public static bool GregDialogListen5 { get; set; }
        public static bool GregDialogListen6 { get; set; }
        public static int BossEpilougeInteractions { get; set; }
        public static bool SkipCheckingEssenceForPuzzles { get; set; }
        #endregion
    }
}
