﻿using System.Collections.Generic;
using Worldline.Visuals;
using MEC;
using UnityEngine;

namespace Worldline.ScriptedSequences.Trailer
{
    public class PanToSky : ScriptedSequence
    {
        public Sprite[] LookUpSprites;
        public override bool DisableCameraOffset => false;
        public override bool SetIDFiredOnStart => false;
        public override ScriptedSequenceStartMode ScriptedSequenceStartMode => ScriptedSequenceStartMode.Dependent;

        protected override void Update()
        {
            base.Update();
            if (Input.GetKeyDown(KeyCode.U))
            {
                Activate();
            }
        }

        public override IEnumerator<float> Fire()
        {
            Player.OverrideAnimations = true;
            Player.SetAnimation(LookUpSprites);
            yield return Timing.WaitForSeconds(2f);
            foreach (BackgroundController backgroundController in FindObjectsOfType<BackgroundController>())
            {
                backgroundController.LockVerticalPosition = true;
            }

            CameraOperator.ActiveCamera.Speed = 0f;
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = null;
            CameraOperator.ActiveCamera.Focus = new Vector3(Player.transform.position.x, Player.transform.position.y + 20f, CameraOperator.ActiveCamera.transform.position.z);
            while (CameraOperator.ActiveCamera.Speed < 1.25f)
            {
                yield return Timing.WaitForOneFrame;
                CameraOperator.ActiveCamera.Speed += 0.005f;
            }

            yield return Timing.WaitForSeconds(8f);
            CameraOperator.ActiveCamera.Mode = CameraMode.Lock;
            CameraOperator.ActiveCamera.Track = Player.gameObject;
            Player.OverrideAnimations = false;
            Active = false;
        }
    }
}
