// Copyright (c) 2019 Destin Hebner
using System;
using System.Collections.Generic;
using Worldline.GUI;
using Worldline.NPCs;
using Worldline.Level;
using Worldline.Player;
using Worldline.Visuals;
using Worldline.Physics;
using Worldline.Utilities;
using Worldline.GameState;
using Worldline.GUI.Printing;
using Worldline.Player.Weapons;
using MEC;
using TeamUtility.IO;
using JetBrains.Annotations;
using UnityEngine;

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// A ScriptedSequence is a series of code that can be executed with precise 
    /// timing and control flow. ScriptedSequences can be activated via the player's 
    /// interaction with an attached BoxCollider2D, or manually using the static 
    /// Run() method. By default, most sequences pause the player and disallow the 
    /// activation of other sequences. Sequences also set an SequencesID, which will 
    /// be added to a list of all fired IDs, preventing the sequence from being fired 
    /// in the future. All of this behavior can be overridden in inheriting sequences 
    /// classes. The most common use for sequences is for cut-scenes.
    /// </summary>
    [HasDefaultState]
    public abstract class ScriptedSequence : TrackedMonoBehaviour
    {
        /// <summary>
        /// The the currently active events that game is running.
        /// </summary>
        public static List<ScriptedSequence> ActiveScriptedSequences { get; set; } = new List<ScriptedSequence>(1);
        [Tooltip("The portraits that this ScriptedSequence will use.")]
        public PortraitBundle[] PortraitBundles;
        /// <summary>
        /// If true, this event is currently prompting the player for an answer.
        /// </summary>
        public bool QuestionOpen { get; private set; }
        /// <summary>
        /// If this is false, this event will not fire or check any ID, effectively 
        /// making it run whenever activated, even if it has been run before.
        /// </summary>
        public virtual bool UseSequenceID { get; } = true;
        /// <summary>
        /// If this is false, this event will not fire it's event ID normally, 
        /// and it must be done manually.
        /// </summary>
        public virtual bool SetIDFiredOnStart { get; } = true;
        /// <summary>
        /// If this is true, then this event will deactivate when another event 
        /// is activated, essentially making it a bitch.
        /// </summary>
        public virtual bool Overrideable { get; } = false;
        /// <summary>
        /// If this is false, this event will not hold the player is place when 
        /// it is activated, giving the player the freedom to move around while
        /// the event runs.
        /// </summary>
        public virtual bool HoldPlayerOnStart { get; } = true;
        /// <summary>
        /// If this is true, this event will never be considered an active event, 
        /// and therefore will not deactivate other events, and will not prevent
        /// the firing of other events. Other checks that ensure if an event is
        /// running will also return false.
        /// </summary>
        public virtual bool IsNeverActive { get; } = false;
        /// <summary>
        /// If this is true, the GameObject that this event is attached to will 
        /// spawn once and then never spawn again assuming the event's ID gets 
        /// fired. Good for NPCs that need to only spawn once.
        /// </summary>
        public virtual bool SpawnObjectOnce { get; } = false;
        /// <summary>
        /// When the player does not have any weapons, the camera normally 
        /// extends a little further in the direction they last moved. By
        /// default, events will focus the camera on the player be getting
        /// rid of this offset.
        /// </summary>
        public virtual bool DisableCameraOffset { get; } = true;
        /// <summary>
        /// Optionally, any actions specified in this array will cancel the 
        /// sequence if they return true. For example, 
        /// () => GlobalFlags.PlayerLikesPiss, would cause the event to not 
        /// run if PlayerLikesPiss was true.
        /// </summary>
        public virtual Func<bool>[] CancelSequenceActions { get; } = null;
        public abstract ScriptedSequenceStartMode ScriptedSequenceStartMode { get; }
        public const float DoubleClickDelay = 0.25f;
        public const float FanfareSuppressionAmount = 1f;
        protected MainPlayerOperator Player { get; set; }
        protected SlidingDialogBox MainDialogBox { get; set; }
        protected SpeechBubble ActiveSpeechBubble { get; set; }
        private bool _active;
        private bool _primaryDown;
        private bool _primaryHold;
        private bool _specialHold;
        private bool _clickedOnce;
        private bool _skippingText;
        private bool _insideSequenceArea;
        private bool _deactivatedWeapon;
        private bool _primaryDownOrSpecialHold;
        private int _questionSelection;
        private string[] _questions;
        private Color _textColor;
        private CoroutineHandle _fireCoroutineHandle;
        private CoroutineHandle _textSkipCoroutineHandle;
        
        public static readonly Dictionary<WorldlineCharacter, Color> CharacterColors = new Dictionary<WorldlineCharacter, Color>
        {
            { WorldlineCharacter.Greg, new Color(1f, 0.75882f, 0.8451f, 1f) },
            { WorldlineCharacter.Rupert, new Color(0.098f, 0.455f, 0.824f, 1f) },
            { WorldlineCharacter.Archdeacon, new Color(0.729f, 0.600f, 0.788f, 1f) },
            { WorldlineCharacter.Derk, new Color(0.329f, 0.055f, 0.459f, 1f) },
            { WorldlineCharacter.Harzix, new Color(0.886f, 0.886f, 0.843f, 1f) },
            { WorldlineCharacter.PizzaPete, new Color(0.737f, 0.196f, 0.196f, 1f) },
            { WorldlineCharacter.ZekeVernie, new Color(1f, 0.196f, 0f, 1f) },
            { WorldlineCharacter.ZekeMaldra, new Color(1f, 0.741f, 0.408f, 1f) },
            { WorldlineCharacter.Phamerah, new Color(0.220f, 0.220f, 0.718f, 1f) },
            { WorldlineCharacter.Aaggie, new Color(0f, 0.5f, 0.5f, 1f) }
        };
        
        public bool Active
        {
            get => _active;
            protected set
            {
                if (_active == value && value)
                {
                    return; // Don't do anything if this is being changed from true to true
                }

                if (value)
                {
                    TextColor = Color.white;
                }

                if (_deactivatedWeapon)
                {
                    PlayerWeaponOperator.Main.Disabled = false;
                    _deactivatedWeapon = false;
                }

                if (HoldPlayerOnStart)
                {
                    Player.Hold = value;
                }

                if (!value)
                {
                    SendMessageUpwards("OnEventDeactivate", SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    SendMessageUpwards("OnEventActivate", SendMessageOptions.DontRequireReceiver);
                }

                if (UseSequenceID && SetIDFiredOnStart && !Fired)
                {
                    FireID();
                }

                if (!IsNeverActive)
                {
                    if (value)
                    {
                        if (HoldPlayerOnStart)
                        {
                            EssenceOperator.Main.EndEffect();
                        }

                        Debug.Log($"{gameObject.name} is activating!");
                        ActiveScriptedSequences.Add(this);
                        for (int i = 0; i < ActiveScriptedSequences.Count; i++)
                        {
                            ScriptedSequence e = ActiveScriptedSequences[i];
                            if (e != this && e.Overrideable)
                            {
                                e.Deactivate();
                            }
                            else if (e != this)
                            {
                                Debug.LogError
                                (
                                    $"Event already active that can't be overridden, {e.GetType().FullName}, please check to make sure two events can't be fired at once.",
                                    e.gameObject
                                );
                            }
                        }
                    }
                    else
                    {
                        Debug.Log($"{gameObject.name} is deactivating!");
                        ActiveScriptedSequences.Remove(this);
                    }

                    Player.InEvent = value;
                    Player.InImportantEvent = value && !Overrideable;
                    CameraOperator.ActiveCamera.DisableHorizontalOffset = DisableCameraOffset && value;
                    _active = value;
                }
            }
        }

        protected bool ShouldContinueDialog => _primaryDownOrSpecialHold &&
                                                !_skippingText && 
                                                !MainDialogBox.Transitioning &&
                                                !MainDialogBox.TextPrinter.Printing;

        protected int QuestionSelection
        {
            get
            {
                if (InputManager.GetButtonDown("pause"))
                {
                    return -1;   
                }

                return _questionSelection;
            }
            private set
            {
                if (value > _questions.Length - 1)
                {
                    value--;
                }

                if (value < 0)
                {
                    value = 0;
                }

                int i = 0;
                string final = "";
                foreach (string s in _questions)
                {
                    if (i == value)
                    {
                        final += $">\t{s}{(i == _questions.Length - 1 ? "" : Environment.NewLine)}";
                    }
                    else
                    {
                        final += "\t" + s + (i == _questions.Length - 1 ? "" : Environment.NewLine);
                    }

                    i++;
                }

                MainDialogBox.QuestionText.text = final;
                _questionSelection = value;
            }
        }
        
        protected Color TextColor
        {
            get => _textColor;
            set
            {
                DialogController.Main.MainDialogBox.NormalText.Color = value;
                DialogController.Main.MainDialogBox.PortraitText.Color = value;
                _textColor = value;
            }
        }

        protected virtual void Awake()
        {
            Player = MainPlayerOperator.MainPlayerComponent;
            if (SpawnObjectOnce)
            {
                if (Fired)
                {
                    gameObject.SetActive(false);
                }
            }
        }

        protected virtual void Start()
        {
            MainDialogBox = DialogController.Main.MainDialogBox;
        }

        protected virtual void Update()
        {
            if (Player == null)
            {
                Player = MainPlayerOperator.MainPlayerComponent;
            }

            if (MainDialogBox == null)
            {
                MainDialogBox = DialogController.Main.MainDialogBox;
            }

            _skippingText = false;
            _primaryDown = InputManager.GetButtonDown("primary");
            _primaryHold = InputManager.GetButton("primary");
            _specialHold = InputManager.GetButton("dash");
            _primaryDownOrSpecialHold = _primaryDown || _specialHold;
            CheckTextSkip();
            DoQuestionBehavior();
            CheckInteract();
            MainDialogBox.TextPrinter.SpeedUpPrinting = _primaryHold;
            if (MainDialogBox.TextPrinter.Printing)
            {
                if (_specialHold)
                {
                    MainDialogBox.TextPrinter.FinishPrinting();
                }

                if (MainDialogBox.TextPrinter.AwaitingInput && _primaryDown)
                {
                    MainDialogBox.TextPrinter.ContinuePrinting();
                }
            }
        }

        protected virtual void OnTriggerEnter2D(Collider2D col)
        {
            if (col.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                _insideSequenceArea = true;
            }

            if (ScriptedSequenceStartMode == ScriptedSequenceStartMode.OnEnter &&
            col.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                Activate();
            }
        }

        protected virtual void OnTriggerExit2D(Collider2D col)
        {
            if (col.gameObject == MainPlayerOperator.MainPlayerObject)
            {
                _insideSequenceArea = false;
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (GetComponent<BoxCollider2D>() != null)
            {
                Bounds bounds = GetComponent<BoxCollider2D>().bounds;
                Gizmos.color = new Color(1f, 0.654f, 0.078f);
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(0f, bounds.size.y));
                Gizmos.DrawLine(bounds.min, bounds.max - new Vector3(bounds.size.x, 0f));
                Gizmos.DrawLine(bounds.max, bounds.min + new Vector3(bounds.size.x, 0f));
            }
        }
        
        /// <summary>
        /// This gets called in the Update() and it checks to see if the player
        /// is trying to progress through dialog, or just skip it.
        /// </summary>
        private void CheckTextSkip()
        {
            if (MainDialogBox == null) return;

            if (MainDialogBox.isActiveAndEnabled && MainDialogBox.TextPrinter.Printing)
            {
                if (_primaryDown && Time.timeScale > 0)
                {
                    if (!_clickedOnce)
                    {
                        _clickedOnce = true;
                        _textSkipCoroutineHandle = Timing.RunCoroutine(DisableClick(), Segment.Update);

                        IEnumerator<float> DisableClick()
                        {
                            yield return Timing.WaitForSeconds(DoubleClickDelay);
                            _clickedOnce = false;
                        }
                    }
                    else
                    {
                        _clickedOnce = false;
                        _skippingText = true;
                        MainDialogBox.TextPrinter.FinishPrinting();
                    }
                }
            }
        }

        /// <summary>
        /// This gets called in the Update() and checks to see if the player is
        /// navigating or selecting anything from a list of responses to a
        /// question.
        /// </summary>
        private void DoQuestionBehavior()
        {
            if (QuestionOpen && _primaryDown)
            {
                QuestionOpen = false;
            }

            if (QuestionOpen)
            {
                if (InputManager.GetButtonDown("down") && QuestionSelection < _questions.Length)
                {
                    QuestionSelection++;
                }

                if (InputManager.GetButtonDown("up") && QuestionSelection > 0)
                {
                    QuestionSelection--;
                }
            }
        }

        /// <summary>
        /// This gets called in the Update() and checks to see if the player is
        /// inside an Event's BoxCollider2D, and is also trying to interact with
        /// the Event.
        /// </summary>
        private void CheckInteract()
        {
            if (_insideSequenceArea && 
            ScriptedSequenceStartMode == ScriptedSequenceStartMode.OnInteract && 
            InputManager.GetButtonDown("down") &&
            GetClosestEvent() == this &&
            Math.Abs(MainPlayerOperator.MainPlayerObject.GetComponent<Rigidbody2D>().velocity.y) < .02f)
            {
                Activate();
            }
        }

        /// <summary>
        /// Extends the dialog and prints the specified message.
        /// </summary>
        /// <param name="message">The message to be printed.</param>
        private void ExtendAndPrint(string message)
        {
            MainDialogBox.NormalText.SetMessage(string.Empty);
            MainDialogBox.PortraitText.SetMessage(string.Empty);
            if (MainDialogBox.isActiveAndEnabled && MainDialogBox.Extended)
            {
                _clickedOnce = false;
                MainDialogBox.TextPrinter.Print(message);
                return;
            }

            MainDialogBox.gameObject.SetActive(true);
            MainDialogBox.ExtendBox();
            Timing.RunCoroutine(ExtendBox(), Segment.FixedUpdate);

            IEnumerator<float> ExtendBox()
            {
                yield return Timing.WaitUntilDone
                (
                    new WaitUntil(() => !DialogController.Main.MainDialogBox.Transitioning)
                );

                MainDialogBox.TextPrinter.Print(message);
            }
        }
        
        /// <summary>
        /// This returns the best matching Event to be activated. Although rare,
        /// if the player is standing on two or more Events, this will decide which
        /// of those Events is the one that should be called, and is based on the
        /// player's proximity to the Event, as well as how much space the Event
        /// takes up.
        /// </summary>
        /// <returns>The best Event to be fired.</returns>
        private ScriptedSequence GetClosestEvent()
        {
            if (MainPlayerOperator.MainPlayerComponent.EventsTouching.Count <= 1)
            {
                return this;
            }

            float? closestPos = null;
            ScriptedSequence closestScriptedSequence = null;
            Player.EventsTouching.RemoveAll(n => n == null);
            foreach (ScriptedSequence e in Player.EventsTouching)
            {
                float dist = Vector2.Distance(e.transform.position, Player.transform.position);
                if (closestPos == null)
                {
                    closestScriptedSequence = e;
                    closestPos = dist;
                }
                else if (dist < closestPos)
                {
                    closestScriptedSequence = e;
                    closestPos = dist;
                }
            }

            return closestScriptedSequence;
        }

        /// <summary>
        /// Gets a set of portraits by their name. These should always be 
        /// supplied in the PortraitBundles array. Normally the name is
        /// specified in the XML dialog files, and is stored in a StringNode
        /// object.
        /// </summary>
        /// <param name="bundleName">The name of the PortraitBundle to load.</param>
        /// <returns>The PortraitBundle if it is attached to the ScriptedSequence.</returns>
        private PortraitBundle GetPortraitBundleByName(string bundleName)
        {
            foreach (PortraitBundle portraitBundle in PortraitBundles)
            {
                if (portraitBundle.name == bundleName)
                {
                    return portraitBundle;
                }
            }

            if (PortraitBundles.Length > 0)
            {
                return PortraitBundles[0];
            }

            return null;
        }

        /// <summary>
        /// Closes all dialog boxes and kills all speech bubbles.
        /// </summary>
        protected void CloseDialog()
        {
            MainDialogBox.RetractBox();
            Timing.RunCoroutine(WaitUntilClose(), Segment.FixedUpdate, GetInstanceID().ToString());

            IEnumerator<float> WaitUntilClose()
            {
                yield return Timing.WaitUntilDone
                (
                    new WaitUntil(() => !MainDialogBox.Transitioning)
                );

                if (!MainDialogBox.Extended)
                {
                    MainDialogBox.gameObject.SetActive(false);
                }
            }

            if (ActiveSpeechBubble != null)
            {
                ActiveSpeechBubble.StartCoroutine(ActiveSpeechBubble.Die());
            }
        }

        /// <summary>
        /// Shows a series of possible responses the player can choose from.
        /// The index of what response the player picked gets stored in
        /// <see cref="QuestionSelection"/>. You can pass an array of 
        /// <see cref="StringNode"/>s into here, as they will cast implicitly.
        /// </summary>
        /// <param name="options">The responses the player can pick from.</param>
        /// <param name="portrait">The portrait to use. Leave null for already set portrait.</param>
        /// <returns>Waits until the player has picked a response.</returns>
        protected float ShowResponses(string[] options, Sprite portrait = null)
        {
            if (!MainDialogBox.Extended)
            {
                MainDialogBox.ExtendBox();
            }

            if (!MainDialogBox.gameObject.activeSelf)
            {
                MainDialogBox.gameObject.SetActive(true);
            }

            MainDialogBox.SetupQuestionDialog();
            MainDialogBox.Portrait.sprite = portrait;

            _questions = options;
            QuestionSelection = 0;
            QuestionOpen = true;
            bool timeElapsed = false;
            Timing.RunCoroutine(Wait(), Segment.FixedUpdate);
            return Timing.WaitUntilDone
            (
                new WaitUntil(() => 
                    (_primaryDown || InputManager.GetButtonDown("pause")) && 
                    timeElapsed && 
                    !MainDialogBox.Transitioning
                )
            );

            IEnumerator<float> Wait()
            {
                yield return Timing.WaitForOneFrame;
                timeElapsed = true;
            }
        }

        /// <summary>
        /// Prints dialog using specified options supplied in the <see cref="StringNode"/>.
        /// </summary>
        /// <param name="dialog">The <see cref="StringNode"/> to print.</param>
        /// <returns>The amount of time to wait to be used by MEC.</returns>
        protected float ShowDialog(StringNode dialog)
        {
            if (string.IsNullOrEmpty(dialog.Portrait))
            {
                MainDialogBox.SetupNormalDialog();
            }
            else
            {
                PortraitBundle portraitBundle = GetPortraitBundleByName(dialog.Bundle);
                if (portraitBundle != null)
                {
                    MainDialogBox.SetupPortraitDialog();
                    MainDialogBox.Portrait.sprite = portraitBundle.GetPortrait(dialog.Portrait);
                }
                else
                {
                    Debug.LogWarning($"Line of dialog '{dialog.Name}' specified a portrait, '{dialog.Bundle}:{dialog.Portrait}', but it could not be loaded.");
                    MainDialogBox.SetupNormalDialog();
                }
            }

            Timing.KillCoroutines(_textSkipCoroutineHandle);
            ExtendAndPrint(dialog.Value);

            return Timing.WaitUntilDone(new WaitUntil(() => ShouldContinueDialog));
        }

        /// <summary>
        /// Prints dialog using the options from the specified <see cref="StringNode"/>,
        /// but the dialog will automatically continue after the specified number of
        /// seconds.
        /// </summary>
        /// <param name="dialog">The <see cref="StringNode"/> to print.</param>
        /// <param name="delay">The amount of time to wait before continuing automatically.</param>
        /// <param name="canProgressNormally">Whether or not the player can continue 
        /// before the time has ran out.</param>
        /// <returns></returns>
        protected float ShowDialogWithDelay(StringNode dialog, float delay, bool canProgressNormally)
        {
            bool timeElapsed = false;
            ShowDialog(dialog);
            Timing.RunCoroutine(Wait(), Segment.FixedUpdate);
            return Timing.WaitUntilDone(new WaitUntil
            (
                () => (canProgressNormally && ShouldContinueDialog) || timeElapsed)
            );

            IEnumerator<float> Wait()
            {
                yield return Timing.WaitForSeconds(delay);
                timeElapsed = true;
            }
        }

        /// <summary>
        /// Prints dialog using the options from the specified <see cref="StringNode"/>, 
        /// and also plays the specified <see cref="AudioClip"/> as a 
        /// <see cref="SoundEffect"/>. This will also wait for the <see cref="AudioClip"/>
        /// to finish playing before allowing the player to continue.
        /// </summary>
        /// <param name="dialog">The <see cref="StringNode"/> to display.</param>
        /// <param name="clip">The <see cref="AudioClip"/> to play.</param>
        /// <param name="suppressBGM">Whether or not the background music's volume should be 
        /// lowered. Good for fanfares and other musical sounds.</param>
        /// <returns>A time to wait to be used by MEC.</returns>
        protected float ShowDialogWithAudioClip(StringNode dialog, AudioClip clip, bool suppressBGM = true)
        {
            ShowDialog(dialog);
            SoundEffect sfx = SoundEffect.PlaySound(clip, false, suppressBGM, FanfareSuppressionAmount, 1f);

            return Timing.WaitUntilDone(new WaitUntil
            (
                () => ShouldContinueDialog && (sfx == null || !sfx.IsPlaying))
            );
        }

        /// <summary>
        /// Shows a speech bubble coming from speaker, which prints out message.
        /// </summary>
        /// <param name="speaker">The object that is speaking.</param>
        /// <param name="message">The message to print.</param>
        /// <param name="spriteType">The type of speech bubble.</param>
        /// <returns>Waits for the text to be finished printing.</returns>
        protected IEnumerator<float> ShowSpeechBubble(GameObject speaker, string message, SpeechBubbleSpriteType spriteType = SpeechBubbleSpriteType.Normal)
        {
            if (ActiveSpeechBubble == null)
            {
                ActiveSpeechBubble = SpeechBubbleController.Main.Say(speaker, message);
            }
            else
            {
                SpeechBubbleController.Main.Say(ActiveSpeechBubble, message);
            }
            
            ActiveSpeechBubble.TextColor = TextColor;
            ActiveSpeechBubble.SpriteType = spriteType;
            yield return Timing.WaitForSeconds(SpeechBubble.InitTime);
            yield return Timing.WaitUntilDone(new WaitUntil(() => !ActiveSpeechBubble.TextPrinter.Printing));
            yield return Timing.WaitForSeconds(ActiveSpeechBubble.TimeDisplayed);
        }

        /// <summary>
        /// Shows a white bubble next to speaker and prints out message. The
        /// bubble goes away after a bit. Mostly used in the tutorial segments
        /// of the game.
        /// </summary>
        /// <param name="speaker">Where to have the bubble float.</param>
        /// <param name="message">What the bubble should say.</param>
        /// <param name="spriteType">The bubble type, the tutorial bubble by default.</param>
        /// <returns>Waits for the text to be finished printing, as well as few seconds.</returns>
        protected float ShowTutorialBubble(GameObject speaker, string message, SpeechBubbleSpriteType spriteType = SpeechBubbleSpriteType.Info)
        {
            if (ActiveSpeechBubble == null)
            {
                ActiveSpeechBubble = SpeechBubbleController.Main.Say(speaker, message, Player.transform.position - new Vector3(0f, 10f, 0f));
            }
            else
            {
                SpeechBubbleController.Main.Say(ActiveSpeechBubble, message);
            }

            ActiveSpeechBubble.FancyDeath = true;
            ActiveSpeechBubble.TimeDisplayed = 3f;
            ActiveSpeechBubble.LerpSpeed = 0.04f;
            ActiveSpeechBubble.TextColor = TextColor;
            ActiveSpeechBubble.SpriteType = spriteType;

            return Timing.WaitUntilDone(Timing.RunCoroutine(RunCoroutines(), Segment.FixedUpdate));

            IEnumerator<float> RunCoroutines()
            {
                yield return Timing.WaitForSeconds(SpeechBubble.InitTime);
                yield return Timing.WaitUntilDone(new WaitUntil(() => ActiveSpeechBubble == null || Vector2.Distance(Player.transform.position, ActiveSpeechBubble.transform.position) < 3f));
                yield return Timing.WaitUntilDone(new WaitUntil(() => ActiveSpeechBubble == null || !ActiveSpeechBubble.TextPrinter.Printing));
                if (ActiveSpeechBubble != null)
                {
                    yield return Timing.WaitForSeconds(ActiveSpeechBubble.TimeDisplayed);
                }
            }
        }

        /// <summary>
        /// Plays a sound of a phone ringing, then shows the animation of the
        /// player looking at their arm.
        /// </summary>
        protected float WatchCall(AudioClip ringingSound)
        {
            return Timing.WaitUntilDone(Timing.RunCoroutine(RunCoroutines(), Segment.FixedUpdate));

            IEnumerator<float> RunCoroutines()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() => MainPlayerOperator.MainPlayerObject.GetComponent<PlayerMovementOperator>().Grounded));
                SoundEffect sfx = SoundEffect.PlaySound(ringingSound);
                SetPlayerSprite(Player.PlayerSpriteBundle.Idle[0]);
                yield return Timing.WaitUntilDone(new WaitUntil(() => !sfx.IsPlaying));
                PlayerWeaponOperator.Main.Disabled = true;
                _deactivatedWeapon = true;
                SetPlayerSprite(Player.PlayerSpriteBundle.LookingAtArmBand[1]);
                yield return Timing.WaitForSeconds(0.75f);
            }
        }

        /// <summary>
        /// Plays a sound of a button, then shows the animation of the player looking at their arm.
        /// </summary>
        protected float ButtonCall(AudioClip callSound)
        {
            return Timing.WaitUntilDone(Timing.RunCoroutine(RunCoroutines(), Segment.FixedUpdate));

            IEnumerator<float> RunCoroutines()
            {
                yield return Timing.WaitUntilDone(new WaitUntil(() => MainPlayerOperator.MainPlayerObject.GetComponent<PlayerMovementOperator>().Grounded));
                SoundEffect sfx = SoundEffect.PlaySound(callSound);
                SetPlayerSprite(Player.PlayerSpriteBundle.Idle[0]);
                yield return Timing.WaitUntilDone(new WaitUntil(() => !sfx.IsPlaying));
                PlayerWeaponOperator.Main.Disabled = true;
                _deactivatedWeapon = true;
                SetPlayerSprite(Player.PlayerSpriteBundle.LookingAtArmBand[0]);
                yield return Timing.WaitForSeconds(0.75f);
            }
        }

        /// <summary>
        /// Causes the camera to focus on both the <see cref="MainPlayerOperator"/> and 
        /// some other <see cref="GameObject"/>. If the other <see cref="GameObject"/>
        /// is too far away, the camera will focus solely on that 
        /// <see cref="GameObject"/>.
        /// </summary>
        /// <param name="obj">The <see cref="GameObject"/> to focus on.</param>
        /// <returns>An amount of time to be waited on.</returns>
        protected void FocusCameraOn(GameObject obj)
        {
            CameraOperator.ActiveCamera.Mode = CameraMode.Float;
            CameraOperator.ActiveCamera.Track = null;
            if (Vector2.Distance(obj.transform.position, Player.transform.position) < 15f)
            {
                CameraOperator.ActiveCamera.Focus =
                Player.transform.position + (obj.transform.position - Player.transform.position) / 2;
            }
            else
            {
                CameraOperator.ActiveCamera.Focus = obj.transform.position;
            }
        }

        /// <summary>
        /// Undoes the effects of the <see cref="FocusCameraOn"/> method.
        /// </summary>
        protected void DefocusCamera()
        {
            CameraOperator.ActiveCamera.Track = Player.gameObject;
        }

        protected IEnumerator<float> MoveCharacter(GameCharacter actor, Vector3 unitsToMove, Vector2 speed)
        {
            Vector3 actorStartPosition = actor.transform.position;
            while (Vector2.Distance(actor.transform.position, actorStartPosition + unitsToMove) > 1f)
            {
                yield return Timing.WaitForOneFrame;
                actor.GetComponent<MovementOperator>().Speed = speed;
            }

            actor.GetComponent<MovementOperator>().Speed = Vector2.zero;
        }

        /// <summary>
        /// Sets the player's sprite and override's the player's animator.
        /// </summary>
        protected void SetPlayerSprite(Sprite sprite)
        {
            PlayerWeaponOperator.Main.Disabled = true;
            _deactivatedWeapon = true;
            Player.GetComponent<AnimationOperator>().Sprites = new[] { sprite };
        }

        /// <summary>
        /// Ends the event if it is running. This normally gets called when an event
        /// can be overridden.
        /// </summary>
        public virtual void Deactivate()
        {
            Timing.KillCoroutines(_fireCoroutineHandle);
            Active = false;
        }

        /// <summary>
        /// Checks to see that an event can be fired, sets up some default values
        /// for all events, and then fires the event code.
        /// </summary>
        public void Activate()
        {
            if (Fired && UseSequenceID || Active || LevelController.Main.Paused || !Overrideable && ActiveScriptedSequences.Count > 0)
            {
                return;
            }

            if (CancelSequenceActions != null)
            {
                foreach (Func<bool> cancelFunction in CancelSequenceActions)
                {
                    if (cancelFunction.Invoke())
                    {
                        return;
                    }
                }
            }

            Active = true;

            try
            {
                _fireCoroutineHandle = Timing.RunCoroutine(Fire(), Segment.Update, GetInstanceID().ToString());
            }
            catch (Exception e)
            {
                Active = false;
                Debug.LogWarning($"An exception was thrown during the scripted sequence '{GetType().Name}'. The scripted sequence was deactivated.{Environment.NewLine}{e}");

                throw e;
            }            
        }

        /// <summary>
        /// The implementation of Fire() should be where the event code goes.
        /// </summary>
        /// <returns>IEnumerator invoked as coroutine.</returns>
        public abstract IEnumerator<float> Fire();

        /// <summary>
        /// Adds an event of type T to a new game object and runs it if startNow
        /// is true. The game object does not get cleaned up after the event is
        /// complete, so this must be done manually.
        /// </summary>
        /// <typeparam name="T">The event to run.</typeparam>
        /// <param name="startNow">Should this event run right now?</param>
        /// <returns></returns>
        public static ScriptedSequence Run<T>(bool startNow = true) where T : ScriptedSequence
        {
            GameObject g = new GameObject("Event", typeof(T));
            ScriptedSequence e = g.GetComponent<T>();
            if (startNow)
            {
                e.Activate();
            }

            return e;
        }

        /// <summary>
        /// Ends all active events and calls their Deactivate() implementations.
        /// </summary>
        public static void EndAllEvents()
        {
            for (int i = 0; i < ActiveScriptedSequences.Count; i++)
            {
                ScriptedSequence e = ActiveScriptedSequences[i];
                e.Deactivate();
            }
        }

        /// <summary>
        /// Sets the list of active events to zero.
        /// </summary>
        [SetsUpDefaults]
        [UsedImplicitly]
        private static void SetupDefaults()
        {
            ActiveScriptedSequences = new List<ScriptedSequence>(1);
        }
    }
}