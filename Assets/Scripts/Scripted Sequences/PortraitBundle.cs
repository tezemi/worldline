﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// A PortraitBundle is a ScriptableObject which holds references to 
    /// the names of portraits, and the sprites. A PortraitBundle should
    /// usually be named after the character in the bundle. For example,
    /// you might have a PortraitBundle called "Greg," which holds names
    /// like "Happy," "Sad," or "Angry." Each name is then assosiated 
    /// with a sprite.
    /// </summary>
    [CreateAssetMenu(fileName = "New Portrait Bundle", menuName = "Data/Portrait Bundle")]
    public class PortraitBundle : ScriptableObject
    {
        [Tooltip("Each portrait needs a name, usually an emotion, and then a corresponding sprite.")]
        public int Portraits;
        [HideInInspector]
        public string[] SerializedNames;
        [HideInInspector]
        public Sprite[] SerialziedSprites;
        public const string PortraitBundleAssetBundle = "portrait_bundles.bun";

        /// <summary>
        /// Gets a Sprite from this bundle via its corresponding name. If the 
        /// name does not exist in the bundle, this will return the first sprite.
        /// </summary>
        /// <param name="portraitName">The name of the portrait, usually an emotion.</param>
        /// <returns>A Sprite to be used as a portrait.</returns>
        public Sprite GetPortrait(string portraitName)
        {
            int indexOf = 0;
            for (int i = 0; i < SerializedNames.Length; i++)
            {
                if (SerializedNames[i] == portraitName)
                {
                    indexOf = i;
                    break;
                }
            }

            return SerialziedSprites[indexOf];
        }
    }
}
