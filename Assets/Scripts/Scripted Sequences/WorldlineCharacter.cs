﻿// Copyright (c) 2019 Destin Hebner

namespace Worldline.ScriptedSequences
{
    /// <summary>
    /// This enum represents a prominent character in Worldline. This is
    /// mainly used by the CharacterColors dictionary in the Event class,
    /// which will return a Color specified for a certain character.
    /// </summary>
    public enum WorldlineCharacter
    {
        Greg,
        Aaggie,
        Archdeacon,
        Derk,
        Harzix,
        PizzaPete,
        ZekeVernie,
        ZekeMaldra,
        Phamerah,
        Rupert
    }
}
