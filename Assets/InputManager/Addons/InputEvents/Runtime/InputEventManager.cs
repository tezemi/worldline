﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Callbacks;
#endif
using System.Collections.Generic;

namespace TeamUtility.IO
{
	public class InputEventManager : MonoBehaviour
	{
		[SerializeField]
        #pragma warning disable 0649
        private List<InputEvent> _inputEvents;
        #pragma warning restore 0649

        private Dictionary<string, InputEvent> _eventLookup;

		public int EventCount
        {
            get
			{
				return _inputEvents.Count;
			}
        }
		
		private void Awake()
		{
			Initialize();
		}

		private void Initialize()
		{
			_eventLookup = new Dictionary<string, InputEvent>();
			foreach(var evt in _inputEvents)
			{
				if(!_eventLookup.ContainsKey(evt.name))
				{
					_eventLookup.Add(evt.name, evt);
				}
				else
				{
					Debug.LogWarning(string.Format("An input event named \'{0}\' already exists in the lookup table", evt.name), this);
				}
			}
		}
		
		private void Update()
		{
			for(int i = 0; i < _inputEvents.Count; i++)
				_inputEvents[i].Evaluate();
		}
		
		public InputEvent CreateAxisEvent(string name, string axisName, PlayerID playerID = PlayerID.One)
		{
			if(!_eventLookup.ContainsKey(name))
			{
				InputEvent evt = new InputEvent(name);
				evt.axisName = axisName;
				evt.eventType = InputEventType.Axis;
                evt.playerID = playerID;

				_inputEvents.Add(evt);
				_eventLookup.Add(name, evt);
				return evt;
			}
			else
			{
				Debug.LogError(string.Format("An input event named {0} already exists", name), this);
				return null;
			}
		}
		
		public InputEvent CreateButtonEvent(string name, string buttonName, InputState inputState, PlayerID playerID = PlayerID.One)
		{
			if(!_eventLookup.ContainsKey(name))
			{
				InputEvent evt = new InputEvent(name);
				evt.buttonName = buttonName;
				evt.eventType = InputEventType.Button;
				evt.inputState = inputState;
                evt.playerID = playerID;

				_inputEvents.Add(evt);
				_eventLookup.Add(name, evt);
				return evt;
			}
			else
			{
				Debug.LogError(string.Format("An input event named {0} already exists", name), this);
				return null;
			}
		}
		
		public InputEvent CreateKeyEvent(string name, KeyCode key, InputState inputState)
		{
			if(!_eventLookup.ContainsKey(name))
			{
				InputEvent evt = new InputEvent(name);
				evt.keyCode = key;
				evt.eventType = InputEventType.Key;
				evt.inputState = inputState;

				_inputEvents.Add(evt);
				_eventLookup.Add(name, evt);
				return evt;
			}
			else
			{
				Debug.LogError(string.Format("An input event named {0} already exists", name), this);
				return null;
			}
		}
		
		public InputEvent CreateEmptyEvent(string name)
		{
			if(!_eventLookup.ContainsKey(name))
			{
				InputEvent evt = new InputEvent(name);
				_inputEvents.Add(evt);
				_eventLookup.Add(name, evt);
				return evt;
			}
			else
			{
				Debug.LogError(string.Format("An input event named {0} already exists", name), this);
				return null;
			}
		}
		
		public void DeleteEvent(string name)
		{
			InputEvent evt = null;
			if(_eventLookup.TryGetValue(name, out evt))
			{
				_eventLookup.Remove(name);
				_inputEvents.Remove(evt);
			}
		}
		
		/// <summary>
		/// Searches for an event based on the specified name. If an event can't be found the return value will be null.
		/// </summary>
		public InputEvent GetEvent(string name)
		{
			InputEvent evt = null;
			if(_eventLookup.TryGetValue(name, out evt))
				return evt;
			
			return null;
		}
		
		/// <summary>
		/// Gets the event at the specified index. If the index is out of range the return value will be null.
		/// </summary>
		public InputEvent GetEvent(int index)
		{
			if(index >= 0 && index < _inputEvents.Count)
				return _inputEvents[index];
			else
				return null;
		}

		public void OnInitializeAfterScriptReload()
		{
			Initialize();
		}

#if UNITY_EDITOR
		[DidReloadScripts(1)]
		private static void OnScriptReload()
		{
			if(EditorApplication.isPlaying)
			{
				InputEventManager[] inputEventManagers = FindObjectsOfType<InputEventManager>();
				for(int i = 0; i < inputEventManagers.Length; i++)
				{
					inputEventManagers[i].OnInitializeAfterScriptReload();
				}
			}
		}
#endif
	}
}