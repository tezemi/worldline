﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using UnityEngine.Events;
using System;
using System.Collections;

namespace TeamUtility.IO
{
	[Serializable]
	public class InputEvent
	{
		[Serializable]
		public class AxisEvent : UnityEvent<float> { }
		
		[Serializable]
		public class ActionEvent : UnityEvent { }

		/// <summary>
		/// Do not change the name of an event at runtime because it will invalidate the lookup table.
		/// </summary>
		public string name;
		public string axisName;
		public string buttonName;
		public KeyCode keyCode = KeyCode.None;
		public InputEventType eventType = InputEventType.Button;
		public InputState inputState = InputState.Pressed;
        public PlayerID playerID = PlayerID.One;
		public ActionEvent onAction;
		public AxisEvent onAxis;

		public InputEvent() :
			this("New Event") { }

		public InputEvent(string name)
		{
			this.name = name;
			axisName = "";
			buttonName = "";
			keyCode = KeyCode.None;
			eventType = InputEventType.Key;
			inputState = InputState.Pressed;
            playerID = PlayerID.One;
			onAxis = new AxisEvent();
			onAction = new ActionEvent();
		}

		public void Evaluate()
		{
			switch(eventType)
			{
			case InputEventType.Axis:
				EvaluateAxis();
				break;
			case InputEventType.Button:
				EvaluateButton();
				break;
			case InputEventType.Key:
				EvaluateKey();
				break;
			}
		}

		private void EvaluateAxis()
		{
			onAxis.Invoke(InputManager.GetAxis(axisName, playerID));
		}

		private void EvaluateButton()
		{
			switch(inputState) 
			{
			case InputState.Pressed:
				if(InputManager.GetButtonDown(buttonName, playerID))
					onAction.Invoke();
				break;
			case InputState.Released:
				if(InputManager.GetButtonUp(buttonName, playerID))
					onAction.Invoke();
				break;
			case InputState.Held:
				if(InputManager.GetButton(buttonName, playerID))
					onAction.Invoke();
				break;
			}
		}

		private void EvaluateKey()
		{
			switch(inputState) 
			{
			case InputState.Pressed:
				if(InputManager.GetKeyDown(keyCode))
					onAction.Invoke();
				break;
			case InputState.Released:
				if(InputManager.GetKeyUp(keyCode))
					onAction.Invoke();
				break;
			case InputState.Held:
				if(InputManager.GetKey(keyCode))
					onAction.Invoke();
				break;
			}
		}
	}
}