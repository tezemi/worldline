﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using System;
using System.Collections;

namespace TeamUtility.IO
{
	public enum InputType
	{
		Button, 
		MouseAxis, 
		DigitalAxis, 
		AnalogAxis,
		RemoteAxis,
		RemoteButton,
		AnalogButton
	}
}