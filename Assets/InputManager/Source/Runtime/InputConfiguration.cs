﻿// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace TeamUtility.IO
{
	[Serializable]
	public sealed class InputConfiguration
	{
		/// <summary>
		/// Do not change the name of an input configuration at runtime because it will invalidate the lookup tables.
		/// </summary>
		public string name;
		public List<AxisConfiguration> axes;
		public bool isExpanded;
		
		public InputConfiguration() :
			this("New Configuration") { }
		
		public InputConfiguration(string name)
		{
			axes = new List<AxisConfiguration>();
			this.name = name;
			isExpanded = false;
		}
		
		public static InputConfiguration Duplicate(InputConfiguration source)
		{
			InputConfiguration inputConfig = new InputConfiguration();
			inputConfig.name = source.name;
			
			inputConfig.axes = new List<AxisConfiguration>(source.axes.Count);
			for(int i = 0; i < source.axes.Count; i++)
			{
				inputConfig.axes.Add(AxisConfiguration.Duplicate(source.axes[i]));
			}
			
			return inputConfig;
		}
	}
}