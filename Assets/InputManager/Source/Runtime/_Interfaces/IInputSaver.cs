// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using System;
using System.Collections.Generic;

namespace TeamUtility.IO
{
	public interface IInputSaver
	{
		void Save(SaveLoadParameters parameters);
	}
}
