// Copyright (c) 2019 Destin Hebner
using UnityEngine;
using System;
using System.Collections.Generic;

namespace TeamUtility.IO
{
	public interface IInputLoader
	{
		SaveLoadParameters Load();
		InputConfiguration LoadSelective(string inputConfigName);
	}
}
