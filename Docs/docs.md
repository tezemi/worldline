# Worldline Documentation #

## Last Updated: 10/12/2018 ##

## Formatting Standards ##
- All code files should start with a copyright header.
- Using statements should be in the order of:
	- System namespaces (System, Linq, ect.)
	- Worldline namespaces (GUI, Weapons, ect.)
	- Any other libraries (ProtoBuf, Chronos, ect.)
	- Unity namespaces (UnityEngine, UI, ect.)
- After that, using statements should be in order of length, shortest to longest.
- All declarations are _explicit_. Make sure all classes have public or internal explicitly set. All methods need an access modifier explicitly set.
- Properties should be used when data does not to be serialized.
- Field and property declarations go as follows:
	- Static always comes first.
		- Public
		- Protected
		- Private
			- Fields
			- Properties
				- Type based on simplicity:
				- bool
				- int
				- float
				- Vector3
				- AudioClip
				- Sprite[]
					- { get; set; }
					- { get; protected set; }
					- { get; private set; }
					- { get; }
						- readonly
						- const

- Non-auto-properties should be implemented first, the backing field should be declared directly above the property **unless** the property has documentation (
`<summary>` tags), in which case the backing field should be placed with the rest of the private fields accordingly.
- If a non-auto-property's get and set simply return and set the backing field, it should be converted to an auto-property and moved accordingly.
- If a property's get simply returns a backing field, but the setter has additional code, the get should be converted to an expression body (`get => _value`.)
- Collections that initialize with a large number of objects should be declared above the properties and below the fields.
- When declaring and implementing methods, Unity callback function always come first. After that, the order is Public, Protected, Private.
- Unity callback functions should be called in the supposed order that they occur: Awake, Start, OnEnabled, Update, OnDisabled, OnDestroy.
- Unity callback functions should not have header documentation.
- **All** Unity callback functions should be `protected virtual` unless it is necessary to declare them otherwise. (For example, a method could be public if it needs to be called outside of normal circumstances, or a method could be private if it should not be overridden.)
- Sub classes, which should be used sparingly, if at all, should be declared at the bottom of the class.
- Classes, enumerations, interfaces, and structs should all be placed in separate files.
- Do not use the var keyword, all variables should have explicitly set types.
- Use standard C# naming conventions for everything else.

## MEC Coroutines ##
First, read up on [Unity coroutines](https://docs.unity3d.com/Manual/Coroutines.html), and then read up on [MEC coroutines](http://trinary.tech/category/mec/). MEC coroutines should *always* be used in place of standard Unity coroutines. This is mostly because they are (as the name implies) more efficient, but because Worldline requires, in some cases, incredibly precise timing that can't be achieved with regular coroutines. Due to the fact that MEC coroutines can be somewhat unwieldy, it's important to use a particular practice when working with them. With normal Unity coroutines, a coroutine runs on a MonoBehavior, and therefor, if the MonoBehavior is disabled, destroyed, or the GameObject the MonoBehavior is on is disabled or destroyed, the coroutine will stop. This is both a blessing and a curse. MEC coroutines on the other hand, run on a singleton GameObject that is always present, even in between scene changes, which is a blessing 90% of the time, and really only a curse if you're acting like an idiot. Sometimes it is desirable to have a coroutine run on a MonoBehavior, or even a GameObject. For example, if an enemy runs a coroutine that causes it to jump around with some delay, when that enemy dies, we would want the coroutine to stop running. If it doesn't, then it could create bizarre null reference exceptions (`this == null` would return true, normally impossible.) 

To simulate running a coroutine on a GameObject, what we can do is call `Timing.RunCoroutine(JumpAround())` as usual, but specify a tag that is the MonoBehavior (or GameObject's) instance ID. This would instead become `Timing.RunCoroutine(JumpAround(), GetInstanceID().ToString())`. Finally, in the OnDisable message in the same MonoBehavior, we would put `Timing.KillCoroutines(GetInstanceID().ToString())`. This will ensure the coroutine is killed when the MonoBehavior is disabled (or destroyed), and is better than using coroutine handles, because it means that we could (if we really wanted) run the same coroutine on the MonoBehavior, two, ten, or nine-million fucking times, and they would all stop when the MonoBehavior is disabled.

## Additional Messages ##
These can be used like Unity callback functions, and should be placed "in order" alongside Unity callback functions.

- **OnLevelLoaded()** Gets called on every Game Object in the scene after a level has loaded completely, and all level loading operations are finished.

- **LevelLoadStarted()** Is called right before a level starts to load.

- **OnFireWeaponPrimary(GameObject bullet)** Gets called when the player fires their primary weapon. The bullet object is the resultant projectile they have fired. Good for weapon upgrade modules to use.

- **OnFireWeaponSecondary(GameObject bullet)** Same as above but with secondary fire.

- **OnReplaySnapshot()** Gets called when a recorder replays a snapshot, going either forward or backward, or if the method is called externally.