# Worldline Events Documentation #

## Last Updated: 10/16/2017 ##

## The Event Class ##

As Bowen, probably the only person who's going to be helping me with writing, you might be interested in working on your own Events. An "Event" is simply a series of actions the game performs, one after the other, in a choreographed branching fashion. Think: 

- The player is asleep. 
- Aaggie breaks down their door and rush in. 
- She screams "WAKEY WAKEY OR YOU'LL MISS THE CAKEY" referring to the cake in the kitchen. 
- Then leaves. 
- And like that, the event ends.

Okay, so let's go over the code that comprises an event.

First, we'll create a new event. Event should be placed in the `Assets\Scripts\Events\Scripts` directory, and can be placed in sub folders in the directory. The current format is generally by location, but deviation is alright if applicable. Let's start a new event for when Rupert talks to the player while they're in the simulation.

![](\img\Capture14.png)

Class names are arbitrary, but I like them somewhat lengthy and descriptive, but not crazy. Normally event classes are not needed outside setting them in Tiled, so there is no need for brevity.

![](\img\Capture15.png)

Here is a template I set up for events. You don't need the template but this is the base that all events will need. The event should be in the `Worldline.Events` namespace, will need to inherit from Event, and require the Fire() method. The Fire() method is the method that gets called, well, when the event is fired. This could be something as simple as the player interacting with a box, or an NPC, or walking into a trigger. Or something more complex, like an Event that fires after a boss fight ends. If you haven't, you'll need to read about the [IEnumerator interface](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/yield), and also [Unity co-routines](https://docs.unity3d.com/Manual/Coroutines.html). You could probably get away without that though. Here's the down low. Methods that return IEnumerator don't `return`, but `yield return` or `yield break` in place of returning nothing. In the context of Unity co-routines, Unity provides several methods for pausing or delaying the execution of these methods. For example, the line `yield return new WaitForSeconds(5);` would delay the execution of the method for 5 seconds. Other important ones are,

- WaitForSeconds(int seconds);		// Waits for specified time.
- WaitForEndOfFrame();				// Waits for one frame to pass.
- WaitForFixedUpdate();				// Waits for one tick to pass.
- WaitUntil(Func<bool\> predicate);	// Waits until predicate method specified returns true.
- WaitWhile(Func<bool\> predicate);	// Waits while predicate method specified returns true.

These are great for pausing the execution of our methods, but the real cream of the crop is the methods the Event class provides. You'll find them all documented at the bottom of this document. We'll use one right now, `ShowDialog()`.

![](\img\Capture16.png)

Show dialog shows some dialog. Wow. This is a functioning event! If the player activates this event, this will work. The ShowDialog method returns WaitUntil, and guess what, it *waits until* the dialog is no longer needed. You will also, however, notice two new things. The `CloseDialog()` method, and the `Active = false`. The CloseDialog() method should be called after dialog if finished, otherwise the box just sticks around. `Active = false` tells the game we are done with this event. It's important because it unfreezes the player, who freezes when an event starts. Many methods like ShowDialog() and part of the event class, and many events have overloads, so be sure to watch out for that. Let's make our event more complex...

![](\img\Capture17.png)

Let's go through this set by step.

1. The `Event` class has a `TextColor` property, you can use [Unity Markup](https://docs.unity3d.com/Manual/StyledText.html), but if you're planning on changing the color of all the text, you should just use the `TextColor` property. (Here, am I using an array I created called "CharacterColors." Pass in the string of a character's name and get their color out. It may or may not have every character, so you might want to remind me if you get errors, or just do it yourself, bitch.)
2. We show some dialog.
3. We do it again, but we use the overload to specify a `TextEffect`. Specifically `Shake`, which makes the text, shake.
4. We ask the player a question, but just as normal dialog.
5. Then we actually display responses using the `ShowQuestion()` method. The method takes a string array, so we pass in a new string array with "Yeah." and "HELL YEAH." as the options.
6. We then access the `QuestionSelection` property. It contains the player's response to the last question in an integer format. So 0 for "Yeah." and 1 for "Hell Yeah." You can have up to four questions, so 0 through 3 is your is the normal response range.
7. If the player said "Yeah.", we show some dialog.
8. But if the player said "Hell Yeah.", we show different dialog.
9. Then we set the text color back to white, or it will show up as Rupert's navy next event.
10. We tell the event we're done.

Nice. Awesome! Great event, right? **Wrong, asshole.** You fucked up big time. **We CAN NOT use string literals in the C# code.** At least for dialog.

This shit:

![](\img\Capture18.png)

Means that people who want to support other languages, will have to completely rebuild the game. I'm not even sure how that would work. It would be a mess. Like you would have to have a different version of the game for each language? You _could_ ask Steam users upon startup, but that's annoying. Also it means that every day bilingual users can't add their own languages. Which is why...

## XML Dialog and Text ##

We have to use XML! Inside the `Assets\Data` directory you'll find all the XML for the game. Anything that the end user sees, Dialog, UI stuff, the HUD, gets stored here. Let's write some for this event we have here...

![](\img\Capture19.png)

I created a new file in the Sim folder called RupertLines. The organization here seems alright, Area/Character. Inside the file, I have my XML. The header is important, as well as the `<CultureStrings>` tags, and the `<Strings>` tags. What you add yourself is the `<String>` tag. Each `<String>` tag needs to attributes, name and lang. We specify an arbitrary name so we can reference it in the C# code, and then use `lang="US_EN"` because we speak English and we're from the United States (wow).

Here's an important side not about text markup. If you do this...

![](\img\Capture20.png)

The XML serializer gets confused. It thinks your **bold** `<b>` tags are part of the XML. To combat this, we have to HTML encode all inner markup tags. Just search notpade++ html encoder, and you'll find the plugin I'm using. Once the plugin is installed, you can highlight the text and press Ctrl+E to encode it. It will look like this,

![](\img\Capture21.png)

Now your bold tags will work!

## Inserting XML Text in Game ##

Finally, let's go back to our event, and tell it to use our new XML strings. For this we use the static `CultureString` class. The class isn't namespaced and is static, so you can access it anywhere without a using. The format looks like this `CultureString.Get("[folder]/[file]")["[string name]"];`. So our finished Event looks like this:

![](\img\Capture22.png)